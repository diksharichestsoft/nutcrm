<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Contracts\Validation\Validator;
use Log;
class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
	    $validation_rules = [
		    'company_id'    => 'required|numeric|exists:companies,id',
                    'name'          => 'required|string|max:255',
                    'phone'         => 'nullable|string|max:25',
                    'gst_number'    => 'nullable|string|max:255',
		    'bank_details'  => 'nullable|string|max:5000',
		    'tax_type'      => 'nullable|exists:tax_types,id',
		    'image'         => 'nullable|image',
           ];

	    return $validation_rules;
    }

    public function failedValidation(Validator $validator)
    {
	    $errors = $validator->getMessageBag()->toArray();
	    return response()->json( compact('errors'), 422 );
    }

}
