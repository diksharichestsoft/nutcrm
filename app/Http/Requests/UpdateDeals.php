<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateDeals extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title"=>'required|string|max:20',
            "company"=>'required|string|max:50',
            "reffer"=>'required|string|max:150',
            "estimated_hour"=>'required',
            "url"=>'required|url',
            "price"=>'required|regex:/^\d+(\.\d{1,2})?$/',
            "platform"=>'required|string|max:20',
            "department"=>'required|string|max:50',
            "client_name"=>'required|string|max:20',
            "client_email"=>'required|email',
            "client_phone"=>'required',
            "job_desc"=>'required|string|max:2000',
        ];
    }
}
