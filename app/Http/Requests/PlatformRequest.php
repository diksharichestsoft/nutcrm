<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Contracts\Validation\Validator;

class PlatformRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
	    $validation_rules = [
		    'name' => 'required|string|max:255',
	    ];

	    return $validation_rules;
    }

    public function failedValidation(Validator $validator)
    {
	    $errors = $validator->getMessageBag()->toArray();
	    return response()->json( compact('errors'), 422 );
    }
}
