<?php

namespace App\Http\Controllers;

use App\Models\EmployeeDsr;
use App\Models\TotalDsr;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EmployeeDsrController extends Controller
{
    public function viewDsrData(Request $request)
    {
        return view('admin.employeeDsr.main');
    }
    public function refreshDsrData(Request $request)
    {
        $todayDate = date('Y-m-d');
        $isDsrExist = TotalDsr::where('user_id', Auth::user()->id)->whereDate('created_at', today())->count();
        if(Auth::user()->can('all_development_dsr_view')){
            $dsrs =  TotalDsr::select('total_dsrs.*')
            ->join('employee_dsrs', 'employee_dsrs.parent_dsr_id', 'total_dsrs.id')
            ->join('projects', 'employee_dsrs.project_id', 'projects.id');
            $dsrs->where('projects.project_type' ,'!=', '2');

        }else if(Auth::user()->can('all_digital_marketing_dsr_view')){
            $dsrs =  TotalDsr::select('total_dsrs.*')
            ->join('employee_dsrs', 'employee_dsrs.parent_dsr_id', 'total_dsrs.id')
            ->join('projects', 'employee_dsrs.project_id', 'projects.id');
            $dsrs->where('projects.project_type' ,'=', '2');
        }else if((Auth::user()->can('all_development_dsr_view')) && Auth::user()->can('all_digital_marketing_dsr_view')){
            $dsrs = TotalDsr::select('total_dsrs.*');
        }else{
            $dsrs = TotalDsr::where('user_id', Auth::user()->id);

        }
        $dsrs = $dsrs->orderby('id', 'desc')->paginate(15);
        $projects = Auth::user()->projects()->get();
        $taskTimerData = Auth::user()->getTodayTimerTime();
        $taskDsrTimerData = Auth::user()->getDsrTodayTimerTime();

        // $quickTaskTimerData = Auth::user()->getTodayQuickTaskTime();

        return view('admin.employeeDsr.includes.view', compact('dsrs', 'projects', 'isDsrExist','taskDsrTimerData', 'taskTimerData'))->render();
    }
    public function dsrSearch(Request $r)
    {

        $isDsrExist = TotalDsr::where('user_id', Auth::user()->id)->whereDate('created_at', today())->count();
        if(Auth::user()->can('all_development_dsr_view')){
            $dsrs = TotalDsr::select('total_dsrs.*','users.name as user_name')
            ->join('users','total_dsrs.user_id','users.id')
            ->join('employee_dsrs', 'employee_dsrs.parent_dsr_id', 'total_dsrs.id')
            ->join('projects', 'employee_dsrs.project_id', 'projects.id');
            $dsrs->where('projects.project_type' ,'!=', '2');
        }else if(Auth::user()->can('all_digital_marketing_dsr_view')){
            $dsrs = TotalDsr::select('total_dsrs.*','users.name as user_name')
            ->join('users','total_dsrs.user_id','users.id')
            ->join('employee_dsrs', 'employee_dsrs.parent_dsr_id', 'total_dsrs.id')
            ->join('projects', 'employee_dsrs.project_id', 'projects.id');
            $dsrs = $dsrs->where('projects.project_type' ,'=', '2');
        }else if((Auth::user()->can('all_development_dsr_view')) && Auth::user()->can('all_digital_marketing_dsr_view')){
            $dsrs = TotalDsr::select('total_dsrs.*','users.name as user_name')
            ->join('users','total_dsrs.user_id','users.id');
        }else{
            $dsrs = TotalDsr::select('total_dsrs.*','users.name as user_name')
            ->join('users','total_dsrs.user_id','users.id');
            $dsrs = $dsrs->where('total_dsrs.user_id', Auth::user()->id);
        }
        if(!empty($r->start_date) && !empty($r->end_date)){
        $start = date('Y-m-d H:i:s', strtotime($r->start_date));
        $end = date('Y-m-d H:i:s', strtotime($r->end_date . ' +1 day'));
        $dsrs->whereBetween('total_dsrs.created_at',[$start,$end]);
        }
        if(!empty($r->search)){
            $search = $r->search;
            $dsrs->where(function ($q) use ($search) {
                $q->orWhere('users.name','like',"%$search%");
            });
        }
        $dsrs->distinct();
        $dsrs = $dsrs->orderBy('id','desc')->paginate(15);
        $projects = Auth::user()->projects()->get();
        $taskTimerData = Auth::user()->getTodayTimerTime();
        // $quickTaskTimerData = Auth::user()->getTodayQuickTaskTime();
        $taskDsrTimerData = Auth::user()->getDsrTodayTimerTime();
        return view('admin.employeeDsr.includes.view', compact('dsrs', 'projects', 'isDsrExist','taskDsrTimerData', 'taskTimerData'))->render();
    }
    public function employeeDsrStore(Request $request)
    {
        if(count($request->all()) < 1){
            return response()->json(['message'=>'success', "status"=>false],500);
        }
        if(TotalDsr::where('user_id', Auth::user()->id)->whereDate('created_at', today())->count() > 0){
            return response()->json(['message'=>'Dsr already Exists, you cannot add dsr more than 1 time a day', "status"=>false],200);
        }
        DB::beginTransaction();
        $haveQuickTask = Auth::user()->quickTasks()->whereIn('child_status_id', [1, 2])->where('active', '!=', 3)->count();
        if($haveQuickTask > 0){
            return response()->json(['message'=>'You have Quick tasks in queue or in progress, please complete them first', "status" =>false]);
        }
        $totalDsr = TotalDsr::create(['user_id' => Auth::user()->id, "date" => date("Y-m-d")]);
        foreach ($request->data as $key => $value) {
            $value["time"] = $value["time_hours"].":".$value["time_min"];
            unset( $value["time_hours"]);
            unset( $value["time_min"]);
            $value["parent_dsr_id"] = $totalDsr->id;
            $isCreated = EmployeeDsr::create($value);
        }
        DB::commit();
        if($isCreated){
            return response()->json(['message'=>'success', "status"=>true]);
        }
    }
    public function toggleReview(Request $request)
    {
        $reviewStatus = TotalDsr::whereId($request->id)->first('status')->status;
        if($reviewStatus == 1){
            $isToggled = true;
            return response()->json(['success'=> $isToggled]);
        }
        if($reviewStatus == 0){
            $isToggled=  TotalDsr::whereId($request->id)->update(["status" => 1, "approve_by" => Auth::user()->id]);
        }


        return response()->json(['success'=> $isToggled]);

    }

    public function viewData($id)
    {
        $thisDsr = TotalDsr::whereId($id)->first();
        $employeeName = User::whereId($thisDsr->user_id)->first('name');
        return view('admin.employeeDsr.detailView', compact('thisDsr', 'employeeName'));
    }

    public function all()
    {
        return view('admin.allEmployeeForDsr.main');
    }
    public function allSearch(Request $request)
    {
        $qry = User::select('users.*')->where('login_toggle', 1)
        ->leftjoin('user_department','user_department.user_id', 'users.id');
        if($request->active == 1){
            $qry->whereNotIn('user_department.department_id', [5, 19, 18, 15, 10]);
        }else{
            $qry->whereIn('user_department.department_id', [5, 19, 18, 15, 10]);
        }
        if(!empty($request->search)){
            $search = $request->search;
            $qry->where(function ($q) use ($search) {
                $q->orWhere('users.name','like',"%$search%");
            });
        }
        $users = $qry->distinct('users.id')->paginate(15);
        return view('admin.allEmployeeForDsr.includes.view', compact('users'));
    }



}
