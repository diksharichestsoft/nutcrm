<?php

namespace App\Http\Controllers;

use App\Models\ProjectTaskTiming;
use App\Models\Task;
use App\Models\TaskFeed;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Throwable;

class ProjectTaskTimingController extends Controller
{
    public function startOrPause(Request $request)
    {
        // Getting the task on which timer is related
        $thisTask = Task::with(['latestTimer'])->whereId($request->id)->first(['id', 'project_id', 'total_timer_time']);
        $haveTimerOnATask = Auth::user()->haveTimerOnTask();
        if($haveTimerOnATask[0] AND $haveTimerOnATask[1] != $request->id ){
            return response()->json(['message'=>'Your Other Task Have a Timer Running', 'status'=>0]);
        }
        $latestTimer = ProjectTaskTiming::where('task_id', $thisTask->id)->where('created_by', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($latestTimer != null){
            if($latestTimer->stop_time == 0){
                $taskTotalTimerTime =  $thisTask->pauseTimer($latestTimer);
                $isRunning = false;
            }else{
                $taskTotalTimerTime = $thisTask->startTimer();
                $isRunning = true;
            }
        }else{
            $taskTotalTimerTime = $thisTask->startTimer();
            $isRunning = true;
        }

        $feedTitle = ($isRunning) ? "Started timer on this task" : "Paused timer on this task ";
        TaskFeed::create([
            'title'=>$feedTitle,
            'comment'=>$request->comment??'',
            'ip_address'=>request()->ip(),
            'task_id'=>$thisTask->id,
            // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
            'branch_id'=>1,
            'status'=>1,
            'type'=>2,
            'created_by'=>auth()->user()->id,
        ]);
        return response()->json(['message'=>'success','isRunning'=>$isRunning,'status'=>1, 'seconds'=>$taskTotalTimerTime]);
    }
}
