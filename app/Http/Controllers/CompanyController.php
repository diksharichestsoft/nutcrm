<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Models\Company;
use \App\Models\TaxType;

use \App\Http\Requests\CompanyRequest;
use App\Models\Setting;
use Illuminate\Support\Facades\DB;
use Validator;
class CompanyController extends Controller
{
   public function viewCompanies( Request $req )
   {
	   $companies = Company::orderBy('id', 'DESC')->paginate(10);
	   $tax_types = TaxType::all();
	   return view('admin.companies.main', compact('companies','tax_types') );
   }


   public function searchCompanies( Request $req )
   {


	   if ( $req->filled('searched_keyword') ){

		   $companies = Company::select('companies.*', 'tax_types.name as tax_type_name')
			   ->LeftJoin('tax_types', 'tax_types.id', 'companies.tax_type')
			   ->where('companies.name','like',"%$req->searched_keyword%")
			   ->orWhere('companies.phone','like', "%$req->searched_keyword%")
			   ->orWhere('tax_types.name','like',"%$req->searched_keyword%")
			   ->orderBy('id', 'DESC')->paginate(10);
	   } else {

		   $companies = Company::select('companies.*','tax_types.name as tax_type_name')
			   	->leftJoin('tax_types', 'tax_types.id', 'companies.tax_type')
				->orderBy('id', 'DESC')->paginate(10);
	   }

	   return view('admin.companies.includes.view', compact('companies') )->render();
   }

   public function storeCompany( CompanyRequest $req )
   {
	   $validated = $req->validated();
	   $company = new Company;
	   $company->name         = $req->name;
	   $company->phone        = $req->phone;
	   $company->gst_number   = $req->gst_number;
	   $company->bank_details = $req->bank_details;
	   $company->tax_type     = $req->tax_type;
       $company->company_address     = $req->company_address;
       $company->account_name     = $req->account_name;
	   $company->account_number     = $req->account_number;
	   $company->swift     = $req->swift;
	   $company->ifsc_code     = $req->ifsc_code;
	   $company->bank_name     = $req->bank_name;
	   $company->bank_branch     = $req->bank_branch;

	   if ( $req->hasFile('logo') ){
		   $imagePath = saveCompanyLogo( $req->file('logo') );
		   $company->logo = $imagePath;
	   }

	   $isSaved = $company->save();

       if($isSaved){
           Setting::createInvoiceNumber($company->id, 1001, 'D1001', 'N1001');
       }

	   return response()->json( ['success' => true], 200);
   }

   public function editCompany( Request $req )
   {

	   $company   = Company::find(  $req->companyId );
	   $tax_types = TaxType::all();
	   return view('admin.companies.includes.update', compact('company','tax_types') );
   }

   public function updateCompany( CompanyRequest $req )
   {
	   $validated = $req->validated();

           $company = Company::find( $req->company_id );
	   $company->name         = $req->name;
	   $company->phone        = $req->phone;
	   $company->gst_number   = $req->gst_number;
	   $company->bank_details = $req->bank_details;
	   $company->tax_type     = $req->tax_type;
       $company->company_address     = $req->company_address;
       $company->account_name     = $req->account_name;
       $company->account_number     = $req->account_number;
	   $company->swift     = $req->swift;
	   $company->ifsc_code     = $req->ifsc_code;
	   $company->bank_name     = $req->bank_name;
	   $company->bank_branch     = $req->bank_branch;

	   if ( $req->hasFile('logo') ){
		   $imagePath = saveCompanyLogo( $req->file('logo') );
		   $deleteOldLogo = $company->logo == null ?  '' : deleteCompanyLogo( $company->logo );

		   $company->logo = $imagePath;
	   }

	   $company->save();

	   return response()->json( ['success' => true], 200);
   }

   public function deleteCompany( Request $req )
   {

	   $company = Company::find( $req->companyId );
       if($company->logo){
           deleteCompanyLogo( $company->logo );
       }

	   $company->delete();

	   return response()->json( ['success' => true], 200);
   }

   public function viewCompanyDetails( Request $req, $company_id )
   {

	   $company = Company::select('companies.*', 'tax_types.name as tax_type_name')
		   ->leftJoin('tax_types', 'companies.tax_type', '=', 'tax_types.id')
	   	   ->where('companies.id',  $company_id )->first();

	   return view('admin.companies.details', compact('company') )->render();
   }
}
