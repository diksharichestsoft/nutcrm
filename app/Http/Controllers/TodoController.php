<?php

namespace App\Http\Controllers;

use App\Models\EmployeeTodo;
use App\Models\Portfolio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TodoController extends Controller
{
    public function index()
    {
        return view('admin.todos.main');
    }
    public function search(Request $request)
    {
        $data = EmployeeTodo::where('user_id', Auth::user()->id);
        $priorityArr = array(
                1 => "highest",
                2 => "medium",
                3 => "low",
                4 => "lowest",
            );
        if(!empty($request->search)){
            $search = $request->search;
            $index = array_search($search, $priorityArr);
            $data->where(function ($q) use ($search, $index) {
                $q->orWhere('title','like',"%$search%");
                $q->orWhere('description','like',"%$search%");
                $q->orWhere('priority_level','like',"%$index%");
               });
        }
        $data = $data->orderBy('priority_level')->paginate(50);
        return view('admin.todos.includes.view', compact('data'));

    }
    public function todoCreatePage()
    {
        return view('admin.todos.create');
    }
    public function todoStore(Request $request)
    {

        $this->validate($request, [
            'title' => 'required',
            'priority_level' => 'required',
        ]);

        $data = $request->input();
        array_shift($data);
        unset($data["id"]);
        unset($data["reqType"]);
        if($request->reqType == 0){
            $isCreated = EmployeeTodo::create($data);
        }else{
            $isCreated = EmployeeTodo::whereId($request->id)->update($data);
        }
        if($isCreated){
            return response()->json(['message'=>'success'],200);
        }else{
            return response()->json(['message'=>"error"], 400);
        }


    }
    public function todoEditPage(Request $request)
    {
        $thisTodo = EmployeeTodo::whereId($request->id)->first();
        return view('admin.todos.update', compact('thisTodo'));
    }
    public function todoRemove(Request $request)
    {
        $isDeleted = EmployeeTodo::whereId($request->id)->delete();
        return response()->json(['message'=>'success'],200);
    }

    public function todoDetail(Request $request)
    {
        $thisTodo = EmployeeTodo::whereId($request->id)->first();
        return view('admin.todos.includes.detail', compact('thisTodo'));
    }

}
