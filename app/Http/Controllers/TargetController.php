<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\ProjectAddon;
use App\Models\Target;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TargetController extends Controller
{
    public function index(Request $request)
    {
        // $employees = User::get(['id', 'name']);
        $employees = User::whereHas("roles", function($q){
            $q->where("name", "BDE");
            $q->orWhere("name", "BDE_digital_marketing");
            $q->orWhere("name", "BDE_development");
            $q->orWhere("name", "BDE_development");
            $q->orWhere("name", "BDM_development");
            $q->orWhere("name", "BDM_digital_marketing");
            $q->orWhere("name", "BDM");
        })->get(['id', 'name']);
        return view('admin.targets.main', compact('employees'));
    }
    public function filter(Request $r)
    {
        $qry = Target::with(['project:id,title', 'convertedByUser:id,name', 'createdByUser:id,name'])->where('status', 1);
        if(!Auth::user()->can('all_user_targets')){
            $qry->where('created_by', Auth::user()->id);
        }
        if($r->created_by_id != null && $r->created_by_id != 0  ){
            $qry->where('created_by', $r->created_by_id);
        }
        if($r->converted_by_id != null && $r->converted_by_id != 0  ){
            $qry->where('converted_by', $r->converted_by_id);
        }
        if($r->employee_id != null && $r->employee_id != 0  ){
            $qry->where('created_by', $r->employee_id);
        }
        if(auth()->user()->can('targets_show_calendar')){
            if(!empty($r->start_date) && !empty($r->end_date)){
                $start = date('Y-m-d H:i:s', strtotime($r->start_date.'-1 day'));
                $end = date('Y-m-d H:i:s', strtotime($r->end_date));
                $qry->whereBetween('targets.target_date',[$start,$end]);
            }
        }else{
            $start = new Carbon('first day of last month');
            $end = new Carbon('last day of this month');
            $qry->whereBetween('targets.target_date',[$start->subdays(1),$end]);
        }
        if(!empty($r->search)){
            $search = $r->search;
            $qry->where(function ($q) use ($search) {
                $q->orWhere('targets.description','like',"%$search%");
            });
        }
        $targets = $qry->orderBy('id','desc')->paginate(20);
        $total_deal_amount = null;
        $total_received_amount = null;

        $name = null;
        if(($r->created_by_id != null && $r->created_by_id != 0)){
            $name = User::whereId($r->created_by_id)->first(['name'])->name ?? '';
        }
        if(($r->converted_by_id != null && $r->converted_by_id != 0)){
            $name = User::whereId($r->converted_by_id)->first(['name'])->name ?? '';
        }
        // if(($r->created_by_id != null && $r->created_by_id != 0) OR ($r->converted_by_id != null && $r->converted_by_id != 0) ){
            $total_deal_amount = 0;
            $arr = $targets->map(function($item) {
                $amount = $item->deal_amount;
                $amount =  (int)filter_var($amount, FILTER_SANITIZE_NUMBER_INT);
                return $amount;
            });
            foreach ($arr as $key => $value) {
                $total_deal_amount += $value;
            }
            $total_received_amount = 0;
            $arr2 = $targets->map(function($item) {
                $amount = $item->received_amount;
                $amount =  (int)filter_var($amount, FILTER_SANITIZE_NUMBER_INT);
                return $amount;
            });
            foreach ($arr2 as $key => $value) {
                $total_received_amount += $value;
            }
        // }
        return view('admin.targets.includes.view', compact('targets', 'total_deal_amount','total_received_amount', 'name'))->render();
    }
    public function add(Request $request)
    {
        $users = User::get(['id', 'name']);
        $projects = Project::select('projects.id', 'projects.title')
                    ->leftJoin('deals', 'deals.id', 'projects.deal_id')

                    // Get projects with addons added within last 2 months
                    ->leftJoin('project_addons', function ($join){
                        $join->on('project_addons.project_id', '=', 'projects.id')
                            ->whereBetween('project_addons.updated_at', [today()->subMonths(2), today()->addDay()]);
                    })
                    ->where('projects.created_by', Auth::user()->id)
                    ->orWhere('projects.accepted_by', Auth::user()->id)
                    ->orWhere('deals.created_by', Auth::user()->id)
                    ->orWhere('project_addons.created_by', Auth::user()->id)
                    ->distinct('projects.id')
                    ->get(['id', 'title']);
        return  view('admin.targets.includes.addform',compact('users', 'projects'))->render();
    }

    public function edit(Request $request)
    {
        $thisTarget = Target::whereId($request->id)->first();
        $users = User::get(['id', 'name']);
        $projects = Project::where('created_by', Auth::user()->id)
        ->orWhere('accepted_by', Auth::user()->id)
        ->get(['id', 'title']);

        return  view('admin.targets.includes.edit',compact('users', 'projects','thisTarget'))->render();
    }
    public function remove(Request $request)
    {
        $isRemoved = Target::whereId($request->id)->update(["status" => 0]);
        if($isRemoved){
            return response()->json(['message'=>'Removed Successfuly'], 200);
        }else{
            return response()->json(['message'=>'Failed to remove'], 500);
        }
    }
    public function viewDetail(Request $request)
    {
        $thisTarget = Target::with(['project:id,title', 'convertedByUser:id,name', 'createdByUser:id,name'])->whereId($request->id)->first();
        return view('admin.targets.includes.detail', compact('thisTarget'));
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'project_id' => 'required',
            'converted_by' => 'required',
            'target_date' => 'required'
        ]);
        $data = [
            "project_id" => $request->project_id ?? "",
            "deal_amount" => $request->deal_amount ?? "",
            "received_amount" => $request->received_amount ?? "",
            "type" => $request->type ?? "",
            "created_by" => Auth::user()->id,
            "converted_by" => $request->converted_by ?? "",
            "target_date" => $request->target_date ?? "",
            "description" => $request->description ?? "",
        ];
        if($request->id != null){
            // Update the Task
            $thisTask = Target::whereId($request->id)->first();
            $isCreated = $thisTask->update($data);
            if($isCreated){

            }

        }else{
            $isCreated = Target::create($data);
            if($isCreated){
            }
        }
        if($isCreated){
            return response()->json(['data'=>$isCreated,'message'=>'Success']);
        }

    }}
