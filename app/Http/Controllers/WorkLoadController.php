<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Project;
use App\Models\ProjectTeam;
use App\Models\Task;
use App\Models\TaskUser;
use App\Models\TeamLead;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class WorkLoadController extends Controller
{
    public function index(Request $request)
    {
        $departments = Department::all();
        $type = $request->type;
        return view('admin.workload.main', compact('departments', 'type'));
    }

    public function filter(Request $r)
    {
        $active = $r->active;
        DB::statement("SET SQL_MODE=''");
        $mySubQuery = Project::where('deal_status', 2);
        $qry = User::where('login_toggle', 1)
        ->leftJoin("project_team", function($join){
            $join->on("users.id", "=", "project_team.user_id");
        })
        ->leftJoin('projects', 'project_team.project_id', 'projects.id')
        ->select("users.*",'projects.deal_status' , DB::raw('
        SUM(CASE WHEN projects.deal_status = 2 AND projects.status != 0 AND project_team.status = 1 THEN project_team.work_hours ELSE 0 END) as total_work_hours'),
        DB::raw('(select count(task_users.user_id) as task_users_count from task_users where task_users.user_id=users.id) as task_users_count'),
        )
        ->groupBy("project_team.user_id")
        ->LeftJoin('user_department', 'user_department.user_id', 'users.id')
        ->distinct('user_department.user_id')
        ->groupBy("departments.id")
        ->LeftJoin('departments', 'departments.id', 'user_department.department_id');
        if($r->type == 1){
            // Get all without Seo
            $qry = $qry->whereNotIn('departments.id',  [5, 19, 18, 15, 10])->orderBy('task_users_count', 'DESC');
        }else{
            // Get all only Seo
            $qry->whereIn('departments.id',  [5, 19, 18, 15, 10])->orderBy('total_work_hours');
        }
        if(!empty($r->active) && $r->active!=-1 ){
            $qry->where('departments.id',$r->active);
        }
        if(!empty($r->search)){
            $search = $r->search;
            $qry->where(function ($q) use ($search) {
                $q->orWhere('users.id','like',"%$search%");
                $q->orWhere('users.name','like',"%$search%");
                $q->orWhere('users.designation','like',"%$search%");
                $q->orWhere('departments.name','like',"%$search%");

            });
        }
        // dd($qry->first());
        if(!empty($r->hours_search)){
            $hours_search = $r->hours_search;
            $time = explode(':', $hours_search);
            $minutes =((int)($time[0])*60) + ((int) ($time[1] ?? 0));
            $qry->where('total_work_hours', 1);
        }
        $hotdeals_status_ID=$r->active;
        $departments=Department::get();
        $data = $qry->paginate(10);
        $isDev = false;
        if($r->type == 1){
            $isDev = true;
        }

        return view('admin.workload.includes.view', compact('data','departments','hotdeals_status_ID','active', 'isDev'))->render();
    }

    public function viewData($id)
    {
        $thisUser = User::whereId($id)->first();
    //  $data=TaskUser::with('tasks')->with('users')->where('user_id',$id)->get();
    //     // foreach ($thisUser->projects()->get() as $key => $value) {
    //     //     dd($value->tasks()->first()->employees()->get());
    //     // }
    $taskOfThisUser = $thisUser->tasksAssigned()->get();
        return view('admin.workload.detail', compact('thisUser', 'taskOfThisUser'));
    }
}
