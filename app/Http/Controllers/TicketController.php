<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use App\Models\Ticket;
use App\Models\User;

class TicketController extends Controller
{
    public function index()
    {
        return view('admin.tickets.main');
    }

    public function search(Request $request)
    {
        // $users = User::get(['id', 'name']);
        $departments = DB::table('departments')->whereIn('id',[6,14])->get();
        if ($request->ajax()) {

            $qry = Ticket::select('tickets.*')->where('status','!=',0);

            if((Auth::user()->isHr() AND Auth::user()->isLinuxServerAdmin())){
            }else{
                if(Auth::user()->isLinuxServerAdmin()){
                    $qry = $qry->where('department_id', 14);
                }elseif(Auth::user()->isHr()){
                    $qry = $qry->where('department_id', 6);
                }elseif(!(Auth::user()->can('all_user_tickets'))){
                    $qry = $qry->where('created_by', Auth::user()->id);
                }
            }

            if(!empty($request->search)){
               $search = $request->search;
                $qry->where(function ($q) use ($search) {
                    $q->orWhere('tickets.id','like',"%$search%");
                    $q->orWhere('tickets.title','like',"%$search%");
                    $q->orWhere('tickets.description','like',"%$search%");
                });
            }
            $tickets = $qry->orderBy('priority', 'asc')->paginate();
            return view('admin.tickets.includes.view', compact('departments','tickets'))->render();
        }
    }

    public function ticketStore(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'department_id' => 'required',
            'priority' => 'required',
        ]);

        $data = $request->input();
        $data['created_by'] = Auth::user()->id;
        array_shift($data);
        unset($data["id"]);
        unset($data["reqType"]);
        $userId = DB::table('user_department')->where('department_id',$request->department_id)->get();
        if($request->reqType == 0){
            $isCreated = Ticket::create($data);
        }else{
            $isCreated = Ticket::whereId($request->id)->update($data);
        }
        if($isCreated){
            foreach ($userId as $key => $value) {
                $message =  "a new ticket is created, https://nutcrm.com/admin/tickets/view/".$isCreated->id;
                notifyUserByUserId(Auth::user()->name, $value->user_id,$message);
            }
            return response()->json(['message'=>'success'],200);
        }else{
            return response()->json(['message'=>"error"], 400);
        }
    }

    public function ticketDetail(Request $request)
    {
        $thisTicket = Ticket::with(['createdByUser:id,name'])->whereId($request->id)->first();
        return view('admin.tickets.includes.detail', compact('thisTicket'));
    }

    public function ticketEditPage(Request $request)
    {   $thisTicket = Ticket::whereId($request->id)->first();
        $departments = DB::table('departments')->whereIn('id',[6,14])->get(['id', 'name']);
        return view('admin.tickets.update', compact('thisTicket', 'departments'));
    }

    public function ticketRemove(Request $request)
    {
        $isDeleted = Ticket::whereId($request->id)->first();
        $isDeleted->status = 0;
        $isDeleted->save();
        return response()->json(['message'=>'success'],200);
    }
}
