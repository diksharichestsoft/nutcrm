<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\ProjectStatus;
use App\Models\Task;
use App\Models\TaskStatus;
use App\Models\TaskType;
use App\Http\Traits\ImageUpload;
use App\Models\TaskFeed;
use App\Models\User;
use App\Notifications\TaskNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use stdClass;
use App\Models\SeoTask;
use Carbon\Carbon;
class TaskController extends Controller
{
    use ImageUpload;

    // Main Index Function
    public function index($id, Request $request)
    {

        $request->id = $id;
        if($request->id == 'all'){
            $project_id = null;
            $thisProjectTitleOnly = null;

        }else{
            $project_id = $id;
            $thisProjectTitleOnly = Project::whereId($project_id)->first('title');
        }
        if($request->id == "all"){
            $taskStatus = TaskStatus::where('id', '!=', 1)->get();
        }else{
            $taskStatus = TaskStatus::get();
        }
        $qry = DB::table('tasks')
        ->select('task_status_id', DB::raw('count(*) as total'))
        ->groupBy('task_status_id');
        if($id != "all"){
            $qry->where('project_id', $id);
        }else{
            $qry->LeftJoin('task_users' , 'tasks.id', 'task_users.task_id')
            ->where('task_users.user_id', Auth::user()->id);
        }
        $tasksCount = $qry->where('active', 1)->pluck('total','task_status_id')->all();
        // dd($tasksCount);

        $orderForTaskStatus = TaskStatus::ORDER_FOR_TASK_STATUS;
        $taskStatus = $taskStatus->sortBy(function($model) use ($orderForTaskStatus) {
            return array_search($model->getKey(), $orderForTaskStatus);
        });
        return view('admin.tasks.main', compact('taskStatus', 'project_id', 'tasksCount', 'thisProjectTitleOnly'));
    }

    // Called on AJAX call to refresh, filter and search data
    public function filter(Request $r)
    {
        $active = $r->active;
        $qry = Task::with(['latestTimer','project:id,title','thisTaskStatus:id,title','assignedBy:id,name'])->select('tasks.*');

        if(!empty($r->start_date) && !empty($r->end_date)){
            $start = date('Y-m-d H:i:s', strtotime($r->start_date));
            $end = date('Y-m-d H:i:s', strtotime($r->end_date . ' +1 day'));
            $qry->whereBetween('tasks.created_at',[$start,$end]);
        }
        $qry->where('tasks.active',1);
        if($r->project_id != null){
            $qry->LeftJoin('task_users' , 'tasks.id', 'task_users.task_id')
            ->LeftJoin('users', 'task_users.user_id', 'users.id');
            $qry->where('project_id', $r->project_id);
        }else{
            $qry->LeftJoin('task_users' , 'tasks.id', 'task_users.task_id')
            ->LeftJoin('users', 'task_users.user_id', 'users.id');
            if($r->active != -2){
                $qry->where('task_users.user_id' , Auth::user()->id);
            }
        }
        if(!empty($r->search)){
            $search = $r->search;
            $qry->where(function ($q) use ($search) {
                $q->orWhere('tasks.id','like',"%$search%");
                $q->orWhere('tasks.title','like',"%$search%");
                $q->orWhere('users.name','like',"%$search%");
                $q->orWhere('tasks.description','like',"%$search%");
            });
        }

        $countArr = array();
        for ($i=1; $i < 9; $i++) {
            $countQry = clone $qry;
            $countArr[$i] = $countQry->where('tasks.task_status_id', $i)->count();
        }
        if(!empty($r->active) && $r->active!=-1 && $r->active != -2)
        $qry->where('tasks.task_status_id',$r->active);

        $data = $qry->distinct('tasks.id')->orderBy('id','DESC')->paginate(20);
        $hotdeals_status_ID=$r->active;
        if($r->project_id == null){
            $taskStatus = TaskStatus::where('id', '!=', 1)->get();
        }else{

            $taskStatus=TaskStatus::all();
        }
        $project_id = $r->project_id ?? null;

        $orderForTaskStatus = TaskStatus::ORDER_FOR_TASK_STATUS;
        $taskStatus = $taskStatus->sortBy(function($model) use ($orderForTaskStatus) {
            return array_search($model->getKey(), $orderForTaskStatus);
        });
        $taskTypes = TaskType::get();

        return view('admin.tasks.includes.view', compact('data','taskTypes','countArr','taskStatus','hotdeals_status_ID','active', 'project_id'))->render();
    }

    // Return data for add Task Form
    public function add(Request $request)
    {
            $taskTypes = TaskType::get();
            $project_id = $request->id;
            if($request->type == 3){
                $seoEmp =  SeoTask::all()->where('status', 1);
                return  view('admin.tasks.includes.seotaskform',compact('seoEmp','taskTypes', 'project_id'))->render();
            }else{
                return  view('admin.tasks.includes.addform',compact('taskTypes', 'project_id'))->render();
            }

    }
    public function seoStoreTask(Request $request){
        $tasks = $request->except( 'formKey' );
        $tasks = array_map(function ($task){
            if(count($task) > 3){
                if($task['quantity'] > 0){
                    return $task;
                }
            }else{
                return $task;
            }
        }, $tasks);
        $tasks = array_filter( $tasks, function( $v ) { return !( is_null( $v) or '' === $v ); } );
        $idArr = [];
        $arr3 = [];
        foreach ($tasks as $key => $value) {
            array_push($arr3, $value);

        }
        $tasks = $arr3;
        for($i = 0; $i+1 < count($tasks); $i++) {
            $value = $tasks[$i];
                array_push( $idArr, $value['id'] );
        }
        $allSeoTasks = SeoTask::whereIn('id', $idArr)->get()->keyBy('id');
        $seo_task = [];
        $timestamp = Carbon::now();
            for($i = 0; $i+1 < count($tasks); $i++){
                $task = $tasks[$i];
                $demo = [];
                $demo['title'] = $task['title'];
                $demo['description'] = $allSeoTasks[$task['id']]->description;
                $demo['project_id'] = $task['project_id'];
                $demo['time'] = 00;
                $demo['priority_level'] = 4;
                $demo['task_type_id'] = 3;
                $demo['quantity'] = $task['quantity'];
                $demo["priority_points"] = 0;
                $demo["given_by"] = Auth::user()->id;
                $demo['start_date'] = $tasks[count($tasks) -1]['start_date'];
                $demo['end_date'] = $tasks[count($tasks) -1]['end_date'];
                $demo['created_at'] = $timestamp;
                $demo['updated_at'] = $timestamp;
              array_push( $seo_task, $demo );
            }
            $data = Task::insert($seo_task);
            $ids = Task::latest()->limit(count($seo_task))->get(['id' ,'title'])->keyBy('id');
            foreach ( $ids as $id ){
            TaskFeed::create([
                'title'=>' Edited Task '.$id->title ?? '',
                'comment'=>$request->comment??'',
                'ip_address'=>request()->ip(),
               'task_id'=>$id->id,
                // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
                'branch_id'=>1,
                'status'=>1,
                'type'=>0,
                'created_by'=>auth()->user()->id,
            ]);
        }
        return response()->json(['data' => $data, 'message' => 'Added successfully.'], 200);
    }


    // Store/Create the task
    public function store(Request $request)
    {
        $this->validate($request, [
            // 'title' => 'required|unique:tasks,title,except,id',
            'title'=>'required|unique:tasks,title,NULL,id,project_id,'.$request->project_id,
            'priority_level' => 'required',
            'project_id' => 'required'
        ]);
// dd($request->all());
        // If there is image, upload the image in public/taskUploadFiles
        if($request->hasFile('file')){
            $image_name=$this->UploadImage($request->file('file'),'taskUploadFiles');

        }
        $time = $request->time_hours.":".$request->time_min;
        $data = [
            "title" => $request->title,
            "project_id" => $request->project_id ?? '',
            "time" => $time,
            "description" => $request->description ?? '',
            "summary" => $request->summary ?? '',
            "priority_level" => $request->priority_level,
            "task_type_id" => $request->task_type_id,
            "start_date" => $request->start_date,
            "end_date" => $request->end_date,
            "priority_points" => $request->priority_points,
            "given_by" => Auth::user()->id,
        ];
        if(isset($image_name)){
            $data["file"] = $image_name;
        }
        if($request->id != null){
            // Update the Task
            $thisTask = Task::whereId($request->id)->first();
            $old_file = $thisTask->file;
            $isCreated = $thisTask->update($data);
            if($isCreated){
                $old_files =   deleteCompanyLogo('taskUploadFiles/'.$old_file);
                TaskFeed::create([
                    'title'=>' Edited Task '.$request->title ?? '',
                    'comment'=>$request->comment??'',
                    'ip_address'=>request()->ip(),
                    'task_id'=>$thisTask->id,
                    // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
                    'branch_id'=>1,
                    'status'=>1,
                    'type'=>0,
                    'created_by'=>auth()->user()->id,
                ]);
            }

        }else{
            $data["user_id"] = Auth::user()->id; //Created By
            $isCreated = Task::create($data);
            if($isCreated){
                $selfAssigned = false;
                // If task is created from all tasks section, assign to the person who created
                if($request->assign_myself != null AND $request->assign_myself == "true"){
                    $isCreated->employees()->sync([Auth::user()->id]);
                    $isCreated->update(["task_status_id"=> 2]);
                    $selfAssigned = true;
                }

                TaskFeed::create([
                    'title'=>' Created Task title '.($request->title ?? '').($selfAssigned ? ", and assigned himself" : ""),
                    'comment'=>$request->comment??'',
                    'ip_address'=>request()->ip(),
                    'task_id'=>$isCreated->id,
                    // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
                    'branch_id'=>1,
                    'status'=>1,
                    'type'=>0,
                    'created_by'=>auth()->user()->id,
                ]);
            }
        }
        if($isCreated){
            return response()->json(['data'=>$isCreated,'message'=>'Success']);
        }

    }
    public function taskComment(Request $request)
    {
        $this->validate($request, [
            'comment' =>'required'
        ]);
        $data =  TaskFeed::create([
        'title'=> "Comment",
        'comment'=>$request->comment,
        'ip_address'=>request()->ip(),
        'task_id'=>$request->deal_id,
        'type'=>1,
        'created_by'=>auth()->user()->id,
        ]);
        if($data){
            $thisTask = Task::with(['employees:id,name'])->whereId($request->deal_id)->first();
            $message =  "Added Comment - ".$thisTask->title." http://localhost/portal/public/admin/tasks/detail/".$request->deal_id;
            foreach ($thisTask->employees as $key => $value) {
            notifyUserByUserId(Auth::user()->name, $value->id,$message);
            }
        }
        return response()->json(['data'=>$data,'message'=>'Comment Added Sucessfully']);
    }

    // Remove the task
    public function removeTask(Request $request)
    {
        $thisTask = Task::where('id',$request->id)->first();
        if($thisTask->haveTimerRunningByAnyUser()){
            return  response()->json(['message'=>'This Task Have Timer Running', 'status'=>0], 200);

        }
        $isRemoved = Task::where('id',$request->id)->update(['active'=>0]);

        if($isRemoved){
            $data =  TaskFeed::create([
                'title'=>' Deleted Task',
                    'comment'=>$request->comment??'',
                    'ip_address'=>request()->ip(),
                    'task_id'=>$request->deal_id,
                    // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
                    'branch_id'=>1,
                    'status'=>1,
                    'type'=>0,
                    'created_by'=>auth()->user()->id,
                ]);
            return response()->json(['message'=>'Delete Sucessfully', 'status'=>1]);
        }else{
            return response()->json(['message' => "Failed To delete", 'status'=>2], 500);
        }
    }

    // Change TaskStatus
    public function changeStatus(Request $request)
    {
        $thisTask = Task::where('id',$request->deal_id)->first();
        if($thisTask->haveTimerRunningByAnyUser()){
            return  response()->json(['message'=>'This Task Have Timer Running', 'status'=>0], 200);
        }

        if($thisTask){
            $isChanged = Task::where('id',$request->deal_id)->update(['task_status_id'=>$request->status]);
            if($isChanged){
                TaskFeed::create([
                    'title'=>' Changed Status to '.TaskStatus::whereId($request->status)->first('title')->title ?? '',
                    'comment'=>$request->comment??'',
                    'ip_address'=>request()->ip(),
                    'task_id'=>$request->deal_id,
                    // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
                    'branch_id'=>1,
                    'status'=>1,
                    'type'=>0,
                    'created_by'=>auth()->user()->id,
                ]);
                return  response()->json(['message'=>'Status Changed Sucessfully', 'status'=>1]);
            }else{
                return  response()->json(['message'=>'Failed to change status'], 500);
            }

        }else{
            return response()->json(["message" => 'Wrong Request'], 500);
        }


    }

    // Retrun data for editing the task

    public function updateData(Request $request)
    {
        $thisTask=Task::where('id',$request->id)->first();
        $taskTypes = TaskType::get();
        return  view('admin.tasks.includes.update',compact('thisTask','taskTypes'))->render();
    }
    public function getEmployeeList(Request $request){
        $reasons = Project::whereId($request->projectId)->first()->users()->get();
        return response()->json(['reasons'=>$reasons]);
    }
    public function assignTask(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required'
        ],
        [
            'user_id.required' => 'Minimum one employee is required',
        ]
    );
        $thisTask = Task::where('id',$request->task_id)->first();
        if($thisTask){
            $thisTask->employees()->sync($request->user_id);
            if(!$thisTask->haveTimerRunningByAnyUser()){
                if($thisTask->task_type_id == 2){
                    $isChanged = Task::where('id',$request->task_id)->update(['task_status_id'=>5]);
                }else{
                    $isChanged = Task::where('id',$request->task_id)->update(['task_status_id'=>2]);
                }
            }else{
                $isChanged = Task::where('id',$request->task_id)->update(['task_status_id'=>3]);
            }
            if($isChanged){
                // Sending notifications
                foreach ($request->user_id as $key => $value) {
                    $message =  "You have new task assigned, https://nutcrm.com/admin/tasks/detail/".$thisTask->id;
                    notifyUserByUserId(Auth::user()->name, $value,$message);
                }
                // Getting array to feed
                $name=User::select('name')->whereIn('id',$request->user_id)->get();
                 $name_array=[];
                 foreach($name as $key=>$value){
                       array_push($name_array,$value->name);
                 }

                $comment =TaskFeed::create([
                    'title'=>' Assigned Employees to Task '.$request->title ?? '',
                    'comment'=>implode(",",$name_array),
                    'ip_address'=>request()->ip(),
                    'task_id'=>$thisTask->id,
                    // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
                    'branch_id'=>1,
                    'status'=>1,
                    'type'=>0,
                    'created_by'=>auth()->user()->id,
                ]);


                return  response()->json(['message'=>'Status Changed Sucessfully']);
            }else{
                return  response()->json(['message'=>'Failed to change status'], 500);
            }
        }else{
            return response()->json(["message" => 'Wrong Request'], 500);
        }
    }
    public function viewDetails(Request $request)
    {
        $thisTask = Task::with(['project'])->whereId($request->id)->first();
        $currentStatus = TaskStatus::whereId($thisTask->task_status_id)->first('title');
        $hotdeals_status_ID=$request->active;
        // Get the user who created this task through task feed

        $thisTaskWasCreatedBy = User::whereId($thisTask->user_id)->first('name');
        if($request->project_id == null){
            $taskStatus = TaskStatus::where('id', '!=', 1)->get();
        }else{

            $taskStatus=TaskStatus::all();
        }
        $getcomment=TaskFeed::select('task_feeds.*','users.name')
        ->leftJoin('users','task_feeds.created_by','users.id')
        ->where('task_id',$request->id)
        ->orderBy('id','DESC')
        ->paginate(10);
        $priorities = array(
                "Highest",
                "Medium",
                "Low",
                "Lowest",
        );
        return view('admin.tasks.detail', compact('priorities','thisTask','thisTaskWasCreatedBy' ,'currentStatus', 'getcomment','hotdeals_status_ID','taskStatus'));
    }


    public function getValue($array){
        $value_array=[];
        foreach($array as $key=>$value){
              array_push($value_array,$value);
        }
        dd($value_array);
        return $value_array;
    }

    public function getTimeline(Request $request)
    {
        $thisTask = Task::whereId($request->id)->first();
        $getcomment=TaskFeed::select('task_feeds.*','users.name')
        ->leftJoin('users','task_feeds.created_by','users.id')
        ->where('task_id',$request->id)
        ->orderBy('id','DESC')
        ->paginate(20);
        return view('admin.tasks.includes.timeline', compact('thisTask', 'getcomment'))->render();
    }
}
