<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Discount;
use App\Models\Project;
use App\Models\ProjectTransaction;
use App\Models\ProjectTransactionItem;
use App\Models\Setting;
use App\Models\TaxType;
use Faker\Provider\ar_SA\Payment;
use Hamcrest\Core\Set;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PDF;

use Invoice;
use Illuminate\Support\Facades\Session;
class ProjectTransactionController extends Controller
{
    public function index($id){
        $thisProject = Project::whereId($id)->first(['id', 'title']);
        if($thisProject){
            return view('admin.projectTransactions.main', compact('thisProject'));
        }else{
            return abort(404);
        }
    }

    public function search(Request $request)
    {
        $thisProject = Project::whereId($request->id)->first(['id', 'title']);
        $data = ProjectTransaction::with(['project:id,title', 'company:id,name', 'taxType:id,name'])->select('project_transactions.*')->where('project_id',$request->id)
        ->join('users as created_by_user', 'project_transactions.created_by', 'created_by_user.id')
        ->leftJoin('users as updated_by_user', 'project_transactions.updated_by', 'updated_by_user.id');
        if(!empty($request->start_date) && !empty($request->end_date)){
            $start = date('Y-m-d H:i:s', strtotime($request->start_date));
            $end = date('Y-m-d H:i:s', strtotime($request->end_date . ' +1 day'));
            $data->whereBetween('project_transactions.created_at',[$start,$end]);
        }
        if(!empty($request->search)){
            $search = $request->search;
            $data->where(function ($q) use ($search) {
                $q->orWhere('details','like',"%$search%");
                $q->orWhere('terms','like',"%$search%");
                $q->orWhere('notes','like',"%$search%");
                $q->orWhere('created_by_user.name','like',"%$search%");
                $q->orWhere('updated_by_user.name','like',"%$search%");
               });
        }

        $transactions = $data->orderBy('project_transactions.created_at', 'DESC')->paginate(50);
        // dd($transactions);
        return view('admin.projectTransactions.includes.view', compact('transactions', 'thisProject'));

    }
    public function transationCreatePage(Request $request)
    {
        $currencies = Setting::getSetting(Setting::CURRENCY);
        $companies = Company::get(['id', 'name', 'company_address']);
        $taxTypes = TaxType::get(['id', 'name','value']);
        $allProjects = Project::get(['id', 'title', 'client_name', 'client_shipping_address']);
        $thisProject = Project::whereId($request->id)->first(['id', 'title', 'client_name']);
        return view('admin.projectTransactions.includes.create', compact('currencies','thisProject', 'companies', 'taxTypes','allProjects'));
    }
    public function transationUpdatePage(Request $request)
    {
        $currencies = Setting::getSetting(Setting::CURRENCY);
        $companies = Company::get(['id', 'name', 'company_address']);
        $taxTypes = TaxType::get(['id', 'name','value']);
        $allProjects = Project::get(['id', 'title', 'client_name', 'client_shipping_address']);
        $thisTransaction = ProjectTransaction::with(['items', 'project:id,title,client_name'])->whereId($request->id)->first();
        $thisProject = $thisTransaction->project;
        return view('admin.projectTransactions.includes.update', compact('currencies','thisProject', 'companies', 'taxTypes','allProjects','thisTransaction'));
    }

    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'project_id' => 'required',
                'date' => 'required',
                'currency' => 'required',
                'client_name' => 'required',
                'tax_type_id' => 'required',
                'company_id' => 'required',
            ]);
            // Defining Variable for Calculation of amoutn
            $totalAmount = 0;
            $totalItemAmount = 0;
            $totalDiscountAmount = 0;
            $totalTaxAmount = 0;

            // Tax Value and id Was stored in string separated by ",", so exploding it
            $taxTypeIdFromRequest = explode(",", $request->tax_type_id)[0];
            $taxValueFromRequest = explode(",", $request->tax_type_id)[1];
            if($taxValueFromRequest == 9){$taxValueFromRequest *=2;}
            // Creating the Transaction Row in DB
            if($taxTypeIdFromRequest == 8){
                $thisTransactionType = Setting::INTERNATIONAL;
            }else if($taxTypeIdFromRequest == 5){
                $thisTransactionType = Setting::NOGST;
            }else{
                $thisTransactionType = Setting::DOMESTIC;
            }
            $invoiceNumber = Setting::getInvoiceNumber($request->company_id, $thisTransactionType );

            DB::beginTransaction();
            $transactionParent = new ProjectTransaction();
            $transactionParent->project_id = $request->project_id;
            $transactionParent->client_name = $request->client_name ?? '';
            $transactionParent->date = $request->date;
            $transactionParent->currency = $request->currency;
            $transactionParent->notes = $request->notes ?? '';
            $transactionParent->details = $request->details ?? '';
            $transactionParent->terms = $request->terms ?? '';
            $transactionParent->from_address = $request->from_address ?? '';
            $transactionParent->bill_to_address = $request->bill_to_address ?? '';
            $transactionParent->ship_to_address = $request->ship_to_address ?? '';
            $transactionParent->company_id = $request->company_id ?? '';
            $transactionParent->tax_type_id = $taxTypeIdFromRequest ?? '';
            $transactionParent->discount_type = $request->discount_type ?? '';
            $transactionParent->discount_value = $request->discount_value ?? '';
            $transactionParent->invoice_number = $invoiceNumber;
            $transactionParent->created_by = auth()->user()->id;
            $transactionParent->save();

            // Increamenting Invoice number for next time
            $isInvoiceNumberIncremented = Setting::increamentInvoiceNumber($request->company_id, $thisTransactionType);
            if(!$isInvoiceNumberIncremented){
                DB::rollBack();
                return response()->json(['success' => false, 'message'=>'problem in invoice number generation'], 500 );
            }
            // Making the array of items to insert and calculating total amount
            $itemsArr = [];
            foreach ( $request->items as $item ){
                // Computing the total amount
                $totalItemAmount += (int)$item['quantity'] * (int)$item['rate'];

                //Adding id, created_at, updated_at
                $item[ 'project_transaction_id' ] = $transactionParent->id;
                $item['created_at'] = now();
                $item['updated_at'] = now();
                array_push( $itemsArr, $item );
            }

            // Making the array of discount to insert and calculating discount Amount
            $discountArr = [];
            foreach ($request->discounts as $key => $discount) {

                // If value of discount is 0 then don't store it and go to next
                if($discount["value"] == 0) continue;

                // Calculating the discount amount(Total)
                if($discount["type"] == "%"){
                    $totalDiscountAmount += $totalItemAmount * ((int)$discount["value"]/100);
                }
                if($discount["type"] == "FLAT"){
                    $totalDiscountAmount += (int)$discount["value"];
                }

                // Adding needed properties to dicount when its inserted
                $discount["type"] = ($discount["type"] == "FLAT") ? 1 : 2;
                $discount['model_type'] = "App\Models\ProjectTransaction";
                $discount['model_id'] = $transactionParent->id;
                $discount['created_by'] = auth()->user()->id;
                $discount['created_at'] = now();
                $discount['updated_at'] = now();
                array_push( $discountArr, $discount );
            }

            // Total Amount Before Tax, Deducting discount
            $totalAmount = ($totalItemAmount - $totalDiscountAmount);

            // Final Total Amount after Deduting discount, deducting Tax
            $totalTaxAmount = $totalAmount * $taxValueFromRequest/100;
            $totalAmount += $totalTaxAmount;

            // Updateing TotalAmount, Tax amount and discount amount in the transation row
            $dataToUpdate = [
                "total_amount" =>$totalAmount,
                "total_tax_amount" =>$totalTaxAmount,
                "total_discount_amount" =>$totalDiscountAmount,
            ];
            $transactionParent->update($dataToUpdate);

            // Inserting the data to disocunt and Items
            $isCreatedAllItems = ProjectTransactionItem::insert($itemsArr);
            $isCreatedAllDiscounts = Discount::insert($discountArr);
            DB::commit();
            if($isCreatedAllItems){
                $message = 'Created new sales Invoice '.route('project-transaction-invoice', $transactionParent->id);
                notifyUserOnEvent('on_new_purchase_invoice', $message);
                return response()->json(['success' => true, 'id'=>$transactionParent->id], 200 );
            }
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;

        }

    }
    public function udpate(Request $request)
    {
        try {
            $this->validate($request, [
                'id' => 'required',
                'project_id' => 'required',
                'date' => 'required',
                'currency' => 'required',
                'client_name' => 'required',
                'tax_type_id' => 'required',
                'company_id' => 'required',
            ]);
            // Defining Variable for Calculation of amoutn
            $totalAmount = 0;
            $totalItemAmount = 0;
            $totalDiscountAmount = 0;
            $totalTaxAmount = 0;

            // Tax Value and id Was stored in string separated by ",", so exploding it
            $taxTypeIdFromRequest = explode(",", $request->tax_type_id)[0];
            $taxValueFromRequest = explode(",", $request->tax_type_id)[1];
            if($taxValueFromRequest == 9){$taxValueFromRequest *=2;}


            DB::beginTransaction();
            $transactionParent = ProjectTransaction::whereId($request->id)->first();
            $transactionParent->project_id = $request->project_id;
            $transactionParent->client_name = $request->client_name ?? '';
            $transactionParent->date = $request->date;
            $transactionParent->currency = $request->currency;
            $transactionParent->notes = $request->notes ?? '';
            $transactionParent->details = $request->details ?? '';
            $transactionParent->terms = $request->terms ?? '';
            $transactionParent->from_address = $request->from_address ?? '';
            $transactionParent->bill_to_address = $request->bill_to_address ?? '';
            $transactionParent->ship_to_address = $request->ship_to_address ?? '';
            $transactionParent->company_id = $request->company_id ?? '';
            $transactionParent->tax_type_id = $taxTypeIdFromRequest ?? '';
            $transactionParent->discount_type = $request->discount_type ?? '';
            $transactionParent->discount_value = $request->discount_value ?? '';
            $transactionParent->updated_by = auth()->user()->id;
            $transactionParent->save();

            // Making the array of items to insert and calculating total amount
            $itemsArr = [];
            foreach ( $request->items as $item ){
                // Computing the total amount
                $totalItemAmount += (int)$item['quantity'] * (int)$item['rate'];

                //Adding id, created_at, updated_at
                $item[ 'project_transaction_id' ] = $transactionParent->id;
                $item['created_at'] = now();
                $item['updated_at'] = now();
                array_push( $itemsArr, $item );
            }

            // Making the array of discount to insert and calculating discount Amount
            $discountArr = [];
            foreach ($request->discounts as $key => $discount) {

                // If value of discount is 0 then don't store it and go to next
                if($discount["value"] == 0) continue;

                // Calculating the discount amount(Total)
                if($discount["type"] == "%"){
                    $totalDiscountAmount += $totalItemAmount * ((int)$discount["value"]/100);
                }
                if($discount["type"] == "FLAT"){
                    $totalDiscountAmount += (int)$discount["value"];
                }

                // Adding needed properties to dicount when its inserted
                $discount["type"] = ($discount["type"] == "FLAT") ? 1 : 2;
                $discount['model_type'] = "App\Models\ProjectTransaction";
                $discount['model_id'] = $transactionParent->id;
                $discount['created_by'] = auth()->user()->id;
                $discount['created_at'] = now();
                $discount['updated_at'] = now();
                array_push( $discountArr, $discount );
            }

            // Total Amount Before Tax, Deducting discount
            $totalAmount = ($totalItemAmount - $totalDiscountAmount);

            // Final Total Amount after Deduting discount, deducting Tax
            $totalTaxAmount = $totalAmount * $taxValueFromRequest/100;
            $totalAmount += $totalTaxAmount;

            // Updateing TotalAmount, Tax amount and discount amount in the transation row
            $dataToUpdate = [
                "total_amount" =>$totalAmount,
                "total_tax_amount" =>$totalTaxAmount,
                "total_discount_amount" =>$totalDiscountAmount,
            ];
            $transactionParent->update($dataToUpdate);

            // Deleteing previous items
            $deleteAllItems = ProjectTransactionItem::where('project_transaction_id', $request->id)->delete();

            // Inserting the data to disocunt and Items
            $isCreatedAllItems = ProjectTransactionItem::insert($itemsArr);
            $isCreatedAllDiscounts = Discount::insert($discountArr);
            DB::commit();
            if($isCreatedAllItems){
                return response()->json(['success' => true, 'id'=>$transactionParent->id], 200 );
            }
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;

        }

    }
    public function invoice(Request $request)
    {
        // $data = [
        //     'title' => 'Welcome to ItSolutionStuff.com',
        //     'date' => date('m/d/Y')
        // ];
        // $thisTransaction = ProjectTransaction::with(['project:id,title,client_name,client_email','items', 'company:id,name', 'taxType:id,name'])->whereId($request->id)->first();
        // // dd($thisTransaction);
        // $pdf = PDF::loadView('admin.projectTransactions.includes.invoice',compact('thisTransaction'));

        // return $pdf->stream('invoice.pdf');
        // return view('vendor.invoices.templates.default');
        $data = Invoice::makeParty([
            'title' => 'Welcome to ItSolutionStuff.com',
            'date' => date('m/d/Y')
        ]);

        Session::put('invoice_id', $request->id);
        $thisTransaction = ProjectTransaction::with(['project:id,title,client_name,client_email','items', 'company:id,name', 'taxType:id,name'])->whereId($request->id)->first()->toArray();
        $item = Invoice::makeItem('abc', $thisTransaction)->pricePerUnit(9.99);
        $Invoice=Invoice::make()->buyer($data)->addItem($item)->logo('https://richestsoft.com/wp-content/uploads/2018/02/logo.png');
        return $Invoice->stream();
    }
}

