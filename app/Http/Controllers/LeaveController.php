<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Leave;
use App\Models\User;
use App\Models\LeaveItem;
use Illuminate\Support\Facades\DB;
use App\Models\LeaveFeed;



class LeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.leaves.main');
    }

    public function search(Request $r)
    {
        $users = User::get(['id', 'name']);
        if ($r->ajax()) {
            $qry = Leave::with(['createdByUser:id,name', 'approveByUser:id,name', 'leaveItems'])->where('status', '!=', 0);
            if(!empty($r->search)){
               $search = $r->search;
                $qry->where(function ($q) use ($search) {
                    $q->orWhere('leaves.id','like',"%$search%");
                    $q->orWhere('leaves.subject','like',"%$search%");
                    $q->orWhere('leaves.description','like',"%$search%");
                    $q->orWhere('leaves.created_at','like',"%$search%");
                });
            }
            if(!Auth::user()->can('all_user_leaves')){
                $qry->where('created_by', Auth::user()->id);
            }
            $leaves = $qry->where('status', '!=', 0)->orderBy('leave_date','DESC')->paginate(15);
            return view('admin.leaves.includes.view', compact('leaves'))->render();

        }

    }

    public function leaveStore(Request $req)
    {
        $reqData = $req->all();
        if($reqData["leaves"]["subject"] == null){
            return response()->json(['success' => false, "message"=> "Subject is required" ], 400 );
        }
        if($reqData["leaves"]["description"] == null){
            return response()->json(['success' => false, "message"=> "Description is required" ], 400 );
        }
        DB::beginTransaction();
        if(array_key_exists("id",$reqData["leaves"])){
            $leave_data = Leave::find($reqData["leaves"]["id"]);
            $deleteItems = $leave_data->leaveItems()->delete();
        }else{
            $leave_data = new Leave;
        }
        $leave_data['created_by'] = Auth::user()->id;
        $leave_data['approved_by_user_id'] = Auth::user()->id;
        $leave_data->subject= $reqData["leaves"]["subject"];
	    $leave_data->description = $reqData["leaves"]["description"];
	    $leave_data->save();

        $leaves = $req->except( 'leaves' );
	    $leave_items_with_parent_id = [];
	    foreach ( $leaves as $leave ){
            if($leave['leave_date'] == null){
                DB::rollBack();
                return response()->json(['success' => false, "message"=> "Leave Date is required" ], 400 );
            }
            if($leave['leave_type'] == null){
                DB::rollBack();
                return response()->json(['success' => false, "message"=> "Leave Type is required" ], 400 );
            }
		    $leave[ 'leave_id' ] = $leave_data->id;
		    $leave[ 'created_by' ] = auth()->user()->id;
          array_push( $leave_items_with_parent_id, $leave );
	    }
        $leave_items = LeaveItem::insert( $leave_items_with_parent_id );
        if($leave_items){
            DB::commit();
            return response()->json(['success' => true ], 200 );
        }else{
            DB::rollBack();
            return response()->json(['success' => false ], 500 );
        }
    }
    public function leaveDetail(Request $request)
    {
        $thisLeave = Leave::with(['createdByUser:id,name'])->whereId($request->id)->first();
        $leaveTypes = Leave::LEAVE_TYPE_ARR;
        $getcomment=LeaveFeed::select('leave_feeds.*','users.name')
        ->leftJoin('users','leave_feeds.created_by','users.id')
        ->where('leave_id',$request->id)
        ->orderBy('id','DESC')
        ->paginate(10);
        return view('admin.leaves.includes.detail', compact('thisLeave', 'leaveTypes' , 'getcomment'));
    }

    public function leaveRemove(Request $request)
    {
        $isDeleted = Leave::whereId($request->id)->first();
        $isDeleted->status = 0;
        $isDeleted->save();
        return response()->json(['message'=>'success'],200);
    }

    public function leaveEditPage(Request $request)
    {   $thisLeave = Leave::with(['leaveItems'])->whereId($request->id)->first();
        $users = User::get(['id', 'name']);
        $leaveTypes = Leave::LEAVE_TYPE_ARR;

        return view('admin.leaves.update', compact('thisLeave', 'users', 'leaveTypes'));
    }

    public function toggleReview(Request $request)
    {
        if($request->id == 0){
            if($request->leave_id == 0 OR $request->leave_id == null){
                return response()->json(['success'=>false, 'message'=> "Opps Something Went wrong"],500);
            }
            $isToggled =  LeaveItem::whereLeaveId($request->leave_id)
                    ->update(["status" => $request->status,"approved_by_user_id" => Auth::user()->id]);
        }else{
            $isToggled =  LeaveItem::whereId($request->id)
                    ->update(["status" => $request->status,"approved_by_user_id" => Auth::user()->id]);
                    if($isToggled)
                    {
                     $leave = LeaveItem::whereId($request->id)->first();
                       if($leave->status == 2){
                           $message =  "Your leave is accepted by ".Auth::user()->name;
                           $resMessage = "Leave Accepted Successfully";
                       }else if($leave->status == 3){
                           $resMessage = "Leave Rejected Successfully";
                           $message =  "Your leave is rejected by ".Auth::user()->name;
                       }
                       notifyUserByUserId(Auth::user()->name, $leave->created_by,$message);
                       return response()->json(['success'=>true, 'message'=> $resMessage],200);
                   }else{
                        return response()->json(['success'=>false, 'message'=> "Opps Something Went wrong"],500);
                    }
        }
    }
    public function leaveItemRefresh($id)
    {
        $thisLeave = Leave::with(['createdByUser:id,name'])->whereId($id)->first();
        $leaveTypes = Leave::LEAVE_TYPE_ARR;
        return view('admin.leaves.includes.leaveItems', compact('thisLeave', 'leaveTypes'));
    }


   public function leaveComment(Request $request)
    {
        $this->validate($request, [
            'comment' =>'required'
        ]);
        $data =  LeaveFeed::create([
        'title'=> "Comment",
        'comment'=>$request->comment,
        'ip_address'=>request()->ip(),
        'leave_id'=>$request->leave_id,
        'type'=>1,
        'created_by'=>auth()->user()->id,
        ]);
        if($data){
            $thisLeave = Leave::with(['employees:id,name'])->whereId($request->leave_id)->first();
            $message =  "Added Comment - ".$thisLeave->title." http://localhost/portal/public/admin/leave/detail/view/".$request->leave_id;
            foreach ($thisLeave->employees as $key => $value) {
            notifyUserByUserId(Auth::user()->name, $value->id,$message);
            }
        }
        return response()->json(['data'=>$data,'message'=>'Comment Added Sucessfully']);
    }



    public function getTimeline(Request $request)
    {
        $thisLeave = Leave::whereId($request->id)->first();
        $getcomment=LeaveFeed::select('leave_feeds.*','users.name')
        ->leftJoin('users','leave_feeds.created_by','users.id')
        ->where('leave_id',$request->id)
        ->orderBy('id','DESC')
        ->paginate(20);
        return view('admin.leaves.includes.timeline', compact('thisLeave', 'getcomment'))->render();
    }

}
