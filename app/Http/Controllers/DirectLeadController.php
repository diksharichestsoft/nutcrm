<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\DirectLead;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class DirectLeadController extends Controller
{
    public function viewDirectLeads()
    {
        $companies = Company::get(['id', 'name']);
        return view('admin.directLeads.main', compact('companies'));
    }
    public function store(Request $request)
    {
        $isCreated = DirectLead::create($request->all());
        return $isCreated;
    }
    public function index()
    {
        return view('admin.directLeadLocal.main');
    }

    public function showData()

    {
        $data = DirectLead::all();
        return view('admin.directLeadLocal.index',["data"=>$data]);
    }
    public function delete($id)
    {
        $isDeleted = DirectLead::whereId($id)->delete();
        if($isDeleted){
            return redirect()->route('DirectLeadLocal');
        }else{
            abort(500);
        }
    }

    public function search(Request $r)
    {

        // $qry = Target::with(['project:id,title', 'convertedByUser:id,name', 'createdByUser:id,name'])->where('status', 1);
        // $qry = Target::with(['directLead:id,title', 'convertedByUser:id,name', 'createdByUser:id,name'])->where('status', 1);

        // if(!Auth::user()->can('all_user_targets')){
        //     $qry->where('created_by', Auth::user()->id);
        // }
        $qry = DirectLead::orderBy('id', 'DESC');
        if(!empty($r->start_date) && !empty($r->end_date)){
            $start = date('Y-m-d H:i:s', strtotime($r->start_date));
            $end = date('Y-m-d H:i:s', strtotime($r->end_date . ' +1 day'));
            $qry->whereBetween('direct_leads.created_at',[$start,$end]);
        }
        if(!empty($r->search)){
            $search = $r->search;
            $qry->where(function ($q) use ($search) {
                $q->orWhere('direct_leads.fname','like',"%$search%");
            });
        }
        // $targets = $qry->orderBy('id','desc')->paginate(10);
        // return view('admin.targets.includes.view', compact('targets'))->render();
        $leaves = $qry->paginate(15);
            return view('admin.directLeadLocal.includes.view', compact('leaves'))->render();
    }
    public function storeDirectLead(Request $request)
    {
        // try {
            // dd($request->all());
            $isCreated = DirectLead::create($request->all());
                return  response()->json(['message' => 'Add successful.'], 200);
        // }catch (\Exception $e) {
        //         return  response()->json(['message' => 'Error.'], 400);
        // }
    }
}
