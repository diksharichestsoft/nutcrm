<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Project;
use App\Models\ProjectTransaction;
use App\Models\PurchaseInvoice;
use App\Models\Setting;
use App\Http\Traits\ImageUpload;
use Illuminate\Http\Request;

class InvoiceManagementController extends Controller
{
    use ImageUpload;
    public function index($type)
    {
        $companies = Company::get(['id', 'name']);
        if($type == 'purchase'){
            return view('admin.invoicesPurchases.main', compact('companies'));
        }
        if($type == 'sales'){
            return view('admin.invoicesSales.main', compact('companies'));
        }else{
            return abort(404);
        }
    }

    public function filterSales(Request $r)
    {
        $active = $r->active;
        $qry = ProjectTransaction::with(['project:id,title', 'createdBy:id,name']);
        if(!($r->active== -1)){
            $qry->where('company_id', $r->active);
        }
        if(!empty($r->start_date) && !empty($r->end_date)){
            $start = date('Y-m-d H:i:s', strtotime($r->start_date));
            $end = date('Y-m-d H:i:s', strtotime($r->end_date . ' +1 day'));
            $qry->whereBetween('project_transactions.created_at',[$start,$end]);
        }
        if(!empty($r->search)){
            $search = $r->search;
            $qry->where(function ($q) use ($search) {
                $q->orWhere('project_transations.title','like',"%$search%");
            });
        }
        $invoices = $qry->orderBy('id','DESC')->paginate(10);
        $company_id=$r->active;
        return view('admin.invoicesSales.includes.view', compact('invoices'))->render();
    }
    public function filterPurchase(Request $r)
    {
        $active = $r->active;
        $status = $r->status;
        $qry = PurchaseInvoice::select('purchase_invoices.*', 'users.name as created_by_user_name')
        ->join('users', 'users.id', 'purchase_invoices.created_by');
        if($status != 'all'){
            $qry->where('purchase_invoices.status', $status);
        }
        if(!($r->active== -1)){
            $qry->where('purchase_invoices.company_id', $r->active);
        }
        if(!empty($r->start_date) && !empty($r->end_date)){
            $start = date('Y-m-d H:i:s', strtotime($r->start_date));
            $end = date('Y-m-d H:i:s', strtotime($r->end_date . ' +1 day'));
            $qry->whereBetween('purchase_invoices.created_at',[$start,$end]);
        }
        if(!empty($r->search)){
            $search = $r->search;
            $qry->where(function ($q) use ($search) {
                $q->orWhere('purchase_invoices.title','like',"%$search%");
                $q->orWhere('purchase_invoices.details','like',"%$search%");
                $q->orWhere('purchase_invoices.total_amount','like',"%$search%");
                $q->orWhere('users.name','like',"%$search%");
            });
        }
        $invoices = $qry->orderBy('purchase_invoices.id','DESC')->paginate(10);
        $company_id=$r->active;
        return view('admin.invoicesPurchases.includes.view', compact('invoices'))->render();
    }
    public function addPurchase(Request $request)
    {
        $projects = Project::where('status', 1)->get(['id', 'title', 'client_name']);
        $currencies = Setting::getSetting(Setting::CURRENCY);
        $companies = Company::get(['id', 'name']);
        return view('admin.invoicesPurchases.includes.addform', compact('projects', 'currencies', 'companies'));
    }
    public function editPurchase(Request $request)
    {
        $thisInvoice = PurchaseInvoice::whereId($request->id)->first();
        $projects = Project::where('status', 1)->get(['id', 'title', 'client_name']);
        $currencies = Setting::getSetting(Setting::CURRENCY);
        $companies = Company::get(['id', 'name']);
        return view('admin.invoicesPurchases.includes.update', compact('projects', 'currencies', 'companies', 'thisInvoice'));
    }
    public function storePurchase(Request $request)
    {
        $this->validate($request, [
            'company_id' => 'required|integer',
            'file' => 'required|max:5000|mimes:doc,pdf,docx,txt,zip,jpeg,jpg,png,xlsx,xlsm,xlsb,xltx,xltm,xls,xlt,xls,xml,xlam,xla,xlw,xlr',
            'currency' => 'required',
            'total_amount' => 'required|integer|min:1',
            'date' => 'required',
            'warranty' => 'required',
        ],
        [
          'file.size' => 'File size should  be less than 5mb'
        ]
        );
        $file_name_after_upload=$this->UploadImage($request->file('file'),'purchaseInvoices');

        $data = [
            'created_by' => auth()->user()->id,
            'title' => $request->title ?? '',
            'details' => $request->details ?? '',
            'file' => $file_name_after_upload,
            'date' => $request->date,
            'warranty' => $request->warranty,
            'company_id' => $request->company_id,
            'currency' => $request->currency,
            'gst_amount' => $request->gst_amount ?? '',
            'total_amount' => $request->total_amount,
        ];
        $isCreated = PurchaseInvoice::create($data);
        if($isCreated){
            $message = 'Created new purchase Invoice '.route('invoices-purchase-detial-view', $isCreated->id);
            notifyUserOnEvent('on_new_purchase_invoice', $message);
            return response()->json(['success' => true], 200 );
        }else{
            return response()->json(['success' => false], 500 );
        }
    }
    public function updatePurchase(Request $request)
    {
        $this->validate($request, [
            'company_id' => 'required|integer',
            'file' => 'max:5000|mimes:doc,pdf,docx,txt,zip,jpeg,jpg,png,xlsx,xlsm,xlsb,xltx,xltm,xls,xlt,xls,xml,xlam,xla,xlw,xlr',
            'currency' => 'required',
            'total_amount' => 'required|integer|min:1',
            'date' => 'required',
            'warranty' => 'required',
        ],
        [
          'file.size' => 'File size should  be less than 5mb'
        ]
        );
        $data = [
            'title' => $request->title ?? '',
            'details' => $request->details ?? '',
            'date' => $request->date,
            'warranty' => $request->warranty,
            'company_id' => $request->company_id,
            'currency' => $request->currency,
            'gst_amount' => $request->gst_amount ?? '',
            'total_amount' => $request->total_amount,
        ];
        $thisInvoice = PurchaseInvoice::whereId($request->id)->first(['id', 'file']);
        if($request->hasFile('file')){
            $file_name_after_upload=$this->UploadImage($request->file('file'),'purchaseInvoices');
            $data['file'] = $file_name_after_upload;
            if($thisInvoice->file != null && $thisInvoice->file != null){
                $employeesImage = deleteCompanyLogo('purchaseInvoices/'.$thisInvoice->file);
            }
        }
        $isupdated = $thisInvoice->update($data);
        if($isupdated){
            return response()->json(['success' => true], 200 );
        }else{
            return response()->json(['success' => false], 500 );
        }

    }
    public function changeStatusPurchase(Request $request)
    {
        $isUpdated = PurchaseInvoice::whereId($request->id)->update(['status'=> $request->status]);
        if($isUpdated)
        {
            return response()->json(['success' => true], 200 );
        }else{
            return response()->json(['success' => false], 500 );
        }
    }
    public function viewDetails($id)
    {
        $thisInvoice = PurchaseInvoice::with(['createdBy:id,name', 'company:id,name'])->whereId($id)->first();
        return view('admin.invoicesPurchases.detail', compact('thisInvoice'));
    }


}
