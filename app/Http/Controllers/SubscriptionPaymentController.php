<?php

namespace App\Http\Controllers;

use App\Models\ProjectPayment;
use App\Models\Project;
use Illuminate\Http\Request;
use DB;
class SubscriptionPaymentController extends Controller
{
    public function index()
    {

        $getproject= Project::where('subscription_date','!=',null)->where('subscription_status',0)->where('subscription_amount','!=',null)->get();
        if(count($getproject)>0){
            foreach($getproject as $project){
                $entry = ProjectPayment::where('project_id', $project->id)->orderBy('due_date','DESC')->first();
                if(!empty($entry) && date('Ymd',strtotime($entry->due_date))<= date('Ymd')){
                    // echo $project->subscription_amount;
                    // echo $project->subscription_payment_type;
                    // echo $entry->due_date;
                    $modifydate = date('Y-m-d', strtotime("$project->subscription_payment_type", strtotime($entry->due_date))); //date('m-d-Y',strtotime())->modify()->format('Y-m-d');
                    $data = [
                        'project_id' => $project->id,
                        'total' => $project->subscription_amount,
                        'paid' => 0,
                        'desc'=>'',
                        'pending' =>$project->subscription_amount,
                        'parent_id'=>0,
                        'status'=> 0,
                        'due_date'=>$modifydate,
                    ];
                    $create = ProjectPayment::create($data);
                }
            }
       }

        // $allPayments = ProjectPayment::select(
        //     'project_payments.*',
        //     'projects.title as project_title',
        //     'projects.client_name',
        //     'deal_platform.name as project_platform',
        //     'projects.client_email',
        //     )
        // ->join('projects','project_payments.project_id','projects.id')
        // ->join('deal_platform','projects.platform','deal_platform.id')
        // ->where('project_payments.status',"0")
        // // ->where('due_date','<',date('Y-m-d'))
        // ->orderBy('project_payments.id',"DESC")->get();

        return view('admin.payments.main');
    }
    public function filter(Request $r)
    {
        $qry = ProjectPayment::with('project')->select(
            'project_payments.*',
            'projects.title as project_title',
            'projects.client_name',
            'deal_platform.name as project_platform',
            'projects.client_email',
            'projects.deal_status',
            )
        ->join('projects','project_payments.project_id','projects.id')
        ->join('deal_platform','projects.platform','deal_platform.id');
        if($r->id == "-1"){
            $qry->where('project_payments.status', 1);
        }else if($r->id == "1"){
            $qry->where('project_payments.status', 2);
        }else if($r->id!=0){
            $modifydate =  date('Y-m-d', strtotime($r->id));
            $qry->where('project_payments.status',"0")->whereDate('project_payments.due_date','<=',$modifydate)->whereDate('project_payments.due_date','>=',date('Y-m-d'));
        }
        else{
            $qry->where('project_payments.status',"0")->whereDate('project_payments.due_date','<=',date('Y-m-d'));
        }

        if(!empty($r->search)){
            $search = $r->search;
            $qry->where(function ($q) use ($search) {
                $q->orWhere('projects.title','like',"%$search%")
                ->orWhere('projects.client_name', 'like', "%$search%")
                ->orWhere('projects.client_email', 'like', "%$search%");
            });
        }
        $active = $r->id;
        // $modifydate =  date('Y-m-d', strtotime($r->id));
     //   $allPayments = $qry->orderBy('project_payments.due_date',"DESC")->get();
          $allPayments = $qry->orderBy(DB::raw("DATE_FORMAT(project_payments.due_date,'%d-%M-%Y')"), 'ASC')->get();
        $data = view('admin.payments.includes.view', compact('allPayments', 'active'));

        return response($data);
    }
    public function viewProjectSubscription($id)
    {
        $allPayments = ProjectPayment::where('project_id', $id)->get();
        return view('admin.payments.main', compact('allPayments'));
    }
    public function getPaymentDetails(Request $request)
    {
        $data = ProjectPayment::where('id',$request->id)->first();
        return response()->json(['data'=>$data,'message'=>'Comment Added Sucessfully']);

    }
    public function addSubscriptionPayment(Request $request)
    {
        // echo 'sdfsdf sdf sdf ';
        // dd($request->all());
        if(!empty($request->image) ){
            $extension = $request->file('image')->getClientOriginalExtension();
            $fileNameToStore = time().'.'.$extension;
            $request->image->move(public_path('subscription-images'), $fileNameToStore);
            // $data=Item::where('id',$request->id)->update(['image'=>$fileNameToStore]);

            // ImageUpload::DBimageUpload($file,$model,$id,$column,$filename="images");
        }
        $prev_payment = ProjectPayment::where('id', $request->parent_id)->first();
        $data = [
            'paid' => $prev_payment->total,
            'desc' => $request->comment,
            'method' => $request->payment_method,
            'image' => $fileNameToStore??'',
            'pending' =>0,
            'status'=> 1,
        ];
            $changeStatus = $prev_payment->update($data);
        return response()->json(['message'=>'Success']);
    }
    public function cancelSubscription(Request $request){
        $commentData = ProjectPayment::where('id',$request->project_id)->first();
        $data = [
            'cancel_reason'=>$request->cancel_reason,
            'status'=> 2,
        ];
        $changeStatus = $commentData->update($data);
         return response()->json(['message'=>'Success']);
    }
    public function viewSubscriptionsDetail($id)
    {
        $subDetails = ProjectPayment::where('project_id', $id)->get();
        $project_id = $id;
        return view('admin.subscriptionPayment.main', compact('project_id'));

    }
    public function fiterSubscriptions(Request $request)
    {
        $data = ProjectPayment::with('project')->select(
            'project_payments.*',
            'projects.title as project_title',
            'projects.client_name',
            'deal_platform.name as project_platform',
            'projects.client_email',
            'projects.deal_status',
            )
        ->join('projects','project_payments.project_id','projects.id')
        ->join('deal_platform','projects.platform','deal_platform.id')->where('project_id',$request->id);
        if(!empty($request->start_date) && !empty($request->end_date)){
                    $start = date('Y-m-d H:i:s', strtotime($request->start_date));
                    $end = date('Y-m-d H:i:s', strtotime($request->end_date . ' +1 day'));
                    $data->whereBetween('project_payments.created_at',[$start,$end]);
                }
                if(!empty($request->search)){
                    $search = $request->search;
                    $data->where(function ($q) use ($search) {
                        $q->orWhere('project_payments.total','like',"%$search%");
                        $q->orWhere('projects.title','like',"%$search%");
                    });
                }
                $subDetails = $data->orderBy(DB::raw("DATE_FORMAT(project_payments.due_date,'%d-%M-%Y')"), 'ASC')->paginate();
        return view('admin.subscriptionPayment.includes.view', compact('subDetails'));

    }

}
