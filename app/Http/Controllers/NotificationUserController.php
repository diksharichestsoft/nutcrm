<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class NotificationUserController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $notifications = Notification::with(['users:id,name'])->get();

        return view('admin.notificationUsers.index',compact('notifications'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $thisNotification = false;
        if($id != 0){
            $thisNotification = Notification::whereId($id)->first();

        }
        return view('admin.notificationUsers.create', compact('thisNotification'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "title" => "required",
        ]);

        if($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        }
        if(isset($request->id) && $request->id != null){
            $isCreated = Notification::whereId($request->id)->update(['title'=>$request->title]);
        }else{
            $isCreated = Notification::create(['title'=>$request->title]);
        }
        return redirect()->route('notification-users')->with('success','Notification created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //



    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $thisNotification = Notification::with(['users:id,name'])->whereId($id)->first();
        $allUsers = User::get(['id', 'name']);
        $thisNotificationUsers = $thisNotification->users()->pluck('users.id')->toarray();
        // dd($thisNotificationUsers);
        return view('admin.notificationUsers.update',compact('thisNotification', 'allUsers', 'thisNotificationUsers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'notification_id'=>'required|integer',
            'users'=>'required|array',
        ]);
        $thisNotification= Notification::findOrFail($request->notification_id);
        $thisNotification->users()->sync($request->users);

        return redirect()->route('notification-users')->with('success','Notifications has Sucessfully Assigned');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // Role::where('id',$id)->delete();
        // return redirect()->route('roles')->with('success','Deleted Sucessfully');

    }
}
