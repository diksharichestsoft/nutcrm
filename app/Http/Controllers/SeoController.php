<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\TaskType;
use App\Models\SeoTask;

use App\Models\Project;
use App\Models\SeoTaskType;
use Illuminate\Support\Facades\Auth;


class SeoController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.seo.main');
    }
    public function seoFilter(Request $r)
    {
        $seoEmployee = SeoTask::select('seo_tasks.*', 'seo_task_types.title as seo_task_type_title', 'seo_task_types.id as seo_task_type_id')
        ->where('status', 1)->join('seo_task_types','seo_tasks.type','seo_task_types.id');
        if(!empty($r->start_date) && !empty($r->end_date)){
            $start = date('Y-m-d H:i:s', strtotime($r->start_date));
            $end = date('Y-m-d H:i:s', strtotime($r->end_date . ' +1 day'));
            $seoEmployee->whereBetween('seo_tasks.created_at',[$start,$end]);
        }
        if(!empty($r->search)){
            $search = $r->search;
            $seoEmployee->where(function ($q) use ($search) {
                $q->orWhere('seo_tasks.title','like',"%$search%");
            });
        }
        $seoEmployees = $seoEmployee->paginate();
        return view('admin.seo.includes.view', compact('seoEmployees'))->render();
    }
    public function seoAdd(Request $request)
    {
            $taskTypes = SeoTaskType::get();
            return  view('admin.seo.includes.addform',compact('taskTypes'))->render();
    }
    public function seoStore(Request $request)
    {
        $this->validate($request, [

            'title' => 'required',
            'type' => 'required',
            'description' => 'required',
        ]);
        $data = [
            "title" => $request->title ?? "",
            "type" => $request->type ?? "",
            "description" => $request->description ?? "",
        ];
        if($request->id != null){
            // Update the Task
            $thisTask = SeoTask::whereId($request->id)->first();
            $isCreated = $thisTask->update($data);
            if($isCreated){

            }

        }else{
            $isCreated = SeoTask::create($data);
            if($isCreated){
            }
        }
        if($isCreated){
            return response()->json(['data'=>$isCreated,'message'=>'Success']);
        }

    }
    public function seoEdit(Request $request)
    {
        $thisTask = SeoTask::whereId($request->id)->first();
        $users = SeoTask::get(['id', 'title']);
        $taskTypes = SeoTaskType::get();
        return  view('admin.seo.includes.edit',compact('users','thisTask','taskTypes'))->render();
    }
    public function seoDetail(Request $request)
    {
        $thisTask = SeoTask::whereId($request->id)->first();
        return view('admin.seo.includes.detail', compact('thisTask'));
    }
    public function seoRemove(Request $request)
    {
        $isRemoved = SeoTask::whereId($request->id)->update(["status" => 0]);
        if($isRemoved){
            return response()->json(['message'=>'Removed Successfuly'], 200);
        }else{
            return response()->json(['message'=>'Failed to remove'], 500);
        }
    }
}
