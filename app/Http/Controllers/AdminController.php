<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use App\Models\User;
use App\Models\Management;
use App\Models\Permissions as PD;
use App\Models\Agency;
use App\Models\Admin;
use App\Models\Announcement;
use App\Models\CoinHistory;
use App\Models\FinanceData;
use App\Models\WithdrawHistory;
use App\Models\Streamer as St;
use App\Models\PermissionLevel;
use App\Models\Punchin;
use App\Models\PunchTiming;
use App\Models\Deals;
use App\Models\DealPlatform;
use App\Models\ProjectType;
use App\Models\Company;
use App\Models\Project;
use App\Models\DealStatus;
use App\Models\FeedModel;
use Carbon\Carbon;

use App\Http\Requests\permissions;
use App\Http\Requests\StaffRequest;
use App\Http\Requests\AgencyRequest;
use App\Http\Requests\Streamer;
use App\Http\Requests\AddDeals;
use App\Http\Requests\UpdateDeals;
use App\Models\Attendance;
use App\Models\Transaction;
use App\Traits\ImageUpload;
use App\Traits\Statuscheck;
use App\Traits\togglestatus;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Crypt;


use Validator;
use DB;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{
    use ImageUpload;
    use Statuscheck;
    use togglestatus;
    public function login(Request $request)
    {
        if ($request->isMethod('post')) {
            $validator = validator()->make($request->all(), [
                "email" => "required|email",
                "password" => "required",
            ]);
            if ($validator->fails()) {
                return back()->withInput($request->input())->with('error', $validator->errors());
            }
            $credentials = $request->only('email', 'password');
            $remember = true;
            $check = User::where('email', $request->email);
            if(!$check->first()){
                return back()->with('error', "Email do not exist.");
            }
            $check = $check->where('login_toggle', 1)->first();
            if ($check) {
                if (Auth::attempt($credentials, $remember)) {
                    Auth::logoutOtherDevices($request->password);
                    $Get_user_id=User::where('email',$request->email)->first();
                    $id=$Get_user_id->id;
                    Session::put('id', $id);
                    return redirect()->route("home");
                } else {
                    return back()->with('error', "Incorrect password.");
                }
            } elseif(!$check) {
                return back()->with('error', "Login is disabled for this account");
            }
        } else {
            return view('admin.login');
        }
    }
    public function Logout(Request $request)
    {
        Auth::logout();
        return redirect('/')->with('success', 'you Logged out');
    }

    public function changePassword(Request $request)
    {
        $thisUser = User::whereId(Auth::user()->id);
        $request->validate([
        'old_password' => 'required',
        'new_password' => 'required|min:4|max:20|different:password',
        'confirm_password' => 'required|required_with:new_password|same:new_password|max:20'
            ]);
        if (Hash::check($request->old_password, $thisUser->first()->password)) {
            $password = Hash::make($request->new_password);
            $thisUser->update(['password' => $password]);
            return response()->json(redirect()->back()->with('success', 'Password is Updated Successfully'));
        }
        else{
            return response()->json(redirect()->back()->with(['errors'=> 'Password did not matched']));
        }
    }
    public function changeProfileImage(Request $request)
    {
        if($request->hasFile('new_profile_image')){
            $image_name=$this->UploadImage($request->file('new_profile_image'),'images');
            $thisUser = Auth::user()->id;
            $isUpdated = User::whereId($thisUser)->update(['profile_image' => $image_name]);
            if($isUpdated){
                return response()->json(redirect()->back()->with('success', 'Image is Updated Successfully'));
                // $admin = Auth::user();
                // return view('admin.profile', compact('admin'))->render();
            }
            else{
                return response()->json(redirect()->back()->with('fail', 'Image Failed to update'));
            }
        }
    }


    //******************************************* Permissions *****************************************//
    public function viewpermission()
    {

        $data = $this->PermissionData();
        return view('admin.permissions.create', compact('data'));
    }
    public function ViewUpdatePermission(request $request)
    {
        $permission = PD::where('id', $request->id)->first();
        return view('admin.permissions.includes.update', compact('permission'))->render();
    }
    public function UpdatePermission(permissions $request)
    {
        $update = PD::where('id', $request->id)->update([
            "title" => $request->title,
            "description" => $request->description ?? " "
        ]);
        return response()->json(redirect()->back()->with('success', 'Permission is Updated Successfully'));
    }
    public function AddPermission(permissions $request)
    {
        $insert = new PD;
        $insert->title = $request->title;
        $insert->description = $request->description ?? " ";
        $insert->created_by_id = Auth::user()->id;
        $insert->save();
        return response()->json(redirect()->back()->with('success', 'New Permission is added Successfully'));
    }
    public function permissiondetailview(request $request)
    {
        $permission = PD::where('id', $request->id)->first();
        return view('admin.permissions.includes.detailview', compact('permission'))->render();
    }
    public function permissionback()
    {
        $data = $this->PermissionData();
        return view('admin.permissions.main', compact('data'))->render();
    }
    public function TogglePermissionSatus(request $request)
    {
        $check_status = PD::where('id', $request->id)->first();
        $status = $this->checkStatus($check_status->status);

        $get_Result = $this->toggleStatusDB($status, 'Permissions', $request->id);
        if ($request->input('search')) {
            $data = PD::where('title', 'like', '%' . $request->search . '%')->orderBy('id', 'DESC')->paginate(15);
            return view('admin.permissions.includes.view', compact('data'))->render();
        } else {
            $data = $this->PermissionData();
            return view('admin.permissions.includes.view', compact('data'))->render();
        }
    }
    public function fetchpermission(request $request)
    {
        if ($request->ajax()) {
            if ($request->input('search')) {
                $data = PD::where('title', 'like', '%' . $request->search . '%')->orderBy('id', 'DESC')->paginate(15);
                return view('admin.permissions.includes.view', compact('data'))->render();
            } else {
                $data = $this->PermissionData();
                return view('admin.permissions.includes.view', compact('data'))->render();
            }
        }
    }


    public function deletepermission(request $request)
    {
        $delete = PD::where('id', $request->id)->delete();
        $data = $this->PermissionData();
        return view('admin.permissions.includes.view', compact('data'))->render();
    }
    public function PermissionData()
    {
        return PD::orderBy('id', 'DESC')->paginate(15);
    }

    //********************************************Projects ********************************************* */



    public function viewdeals()
    {
        $deal_status=6;
        $status=1;
        $status_ID=1;
        $value = Session::get('id');
        $Dealstatus=DealStatus::get();
        $data=Deals::where('status',$status)->where('status',$status)->where('deal_status',$deal_status)->orderBy('title','DESC')->paginate(15);
        $this->addProject($data);
        // return view('admin.deals.create', compact('data','Dealstatus','status_ID'));
    }
    public function fetchdeals(Request $request)
    {
        // dd($request->active);
        if($request->active==1)
        {
        $status=1;
        $accept_status=0;
        $deal_status=1;
        $remove_status=1;
        $status_ID=1;
        if ($request->ajax()) {
            if ($request->input('search')) {
                $data = Project::where('title', 'like', '%' . $request->search . '%')->where('status',$status)->where('acceptStatus',$accept_status)->where('removestatus',$remove_status)->where('dealstatus',$deal_status)->orderBy('title', 'DESC')->paginate(15);
                return view('admin.deals.includes.view', compact('data','status_ID'))->render();
            } else {
                $data = Project::where('status',$status)->where('acceptStatus',$accept_status)->where('removestatus',$remove_status)->where('dealstatus',$deal_status)->orderBy('title', 'DESC')->paginate(15);
                return view('admin.deals.includes.view', compact('data','status_ID'))->render();
            }
        }
        }
        if($request->active==2)
        {
            if ($request->input('search')) {
                $acceptStatus=1;
                $dealstatus=2;
                $remove_status=1;
                $inproccess_status_ID=2;
                $Dealstatus=DealStatus::get();
                $data = Project::where('title', 'like', '%' . $request->search . '%')->where('acceptStatus',$acceptStatus)->where('dealstatus',$dealstatus)->where('removestatus',$remove_status)->orderBy('title', 'DESC')->paginate(15);
                return view('admin.deals.includes.view', compact('data','Dealstatus','inproccess_status_ID'))->render();
            } else {
                $acceptStatus=1;
                $dealstatus=2;
                $remove_status=1;
                $inproccess_status_ID=2;
                $Dealstatus=DealStatus::get();
                $data = Project::where('acceptStatus',$acceptStatus)->where('dealstatus',$dealstatus)->where('removestatus',$remove_status)->orderBy('title', 'DESC')->paginate(15);
                return view('admin.deals.includes.view', compact('data','Dealstatus','inproccess_status_ID'))->render();
            }
        }
        if($request->active==3)
        {
            if ($request->ajax()) {
                if ($request->input('search')) {
                    $acceptStatus=1;
                    $dealstatus=3;
                    $remove_status=1;
                    $meeting_status_ID=3;
                    $Dealstatus=DealStatus::get();
                    $data = Project::where('title', 'like', '%' . $request->search . '%')->where('acceptStatus',$acceptStatus)->where('dealstatus',$dealstatus)->where('removestatus',$remove_status)->orderBy('title', 'DESC')->paginate(15);
                    return view('admin.deals.includes.view', compact('data','Dealstatus','meeting_status_ID'))->render();
                } else {
                    $acceptStatus=1;
                    $dealstatus=3;
                    $remove_status=1;
                    $meeting_status_ID=3;
                    $Dealstatus=DealStatus::get();
                    $data = Project::where('acceptStatus',$acceptStatus)->where('dealstatus',$dealstatus)->where('removestatus',$remove_status)->orderBy('title', 'DESC')->paginate(15);
                    return view('admin.deals.includes.view', compact('data','Dealstatus','meeting_status_ID'))->render();
                }
        }

    }
    if($request->active==4)
    {
        if ($request->ajax()) {
            if ($request->input('search')) {
                $acceptStatus=1;
                $dealstatus=4;
                $remove_status=1;
                $frd_status_ID=4;
                $Dealstatus=DealStatus::get();
                $data = Project::where('title', 'like', '%' . $request->search . '%')->where('acceptStatus',$acceptStatus)->where('dealstatus',$dealstatus)->where('removestatus',$remove_status)->orderBy('title', 'DESC')->paginate(15);
                return view('admin.deals.includes.view', compact('data','Dealstatus','frd_status_ID'))->render();
            } else {
                $acceptStatus=1;
                $dealstatus=4;
                $remove_status=1;
                $frd_status_ID=4;
                $Dealstatus=DealStatus::get();
                $data = Project::where('acceptStatus',$acceptStatus)->where('dealstatus',$dealstatus)->where('removestatus',$remove_status)->orderBy('title', 'DESC')->paginate(15);
                return view('admin.deals.includes.view', compact('data','Dealstatus','frd_status_ID'))->render();
            }
    }

}
            if($request->active==5)
                {
                    if ($request->ajax()) {
                        if ($request->input('search')) {
                            $acceptStatus=1;
                            $dealstatus=5;
                            $remove_status=1;
                            $hotdeals_status_ID=5;
                            $Dealstatus=DealStatus::get();
                            $data = Project::where('title', 'like', '%' . $request->search . '%')->where('acceptStatus',$acceptStatus)->where('dealstatus',$dealstatus)->where('removestatus',$remove_status)->orderBy('title', 'DESC')->paginate(15);
                            return view('admin.deals.includes.view', compact('data','Dealstatus','hotdeals_status_ID'))->render();
                        } else {
                            $acceptStatus=1;
                            $dealstatus=5;
                            $remove_status=1;
                            $hotdeals_status_ID=5;
                            $Dealstatus=DealStatus::get();
                            $data = Project::where('acceptStatus',$acceptStatus)->where('dealstatus',$dealstatus)->where('removestatus',$remove_status)->orderBy('title', 'DESC')->paginate(15);
                            return view('admin.deals.includes.view', compact('data','Dealstatus','hotdeals_status_ID'))->render();
                        }
                }

            }
            if($request->active==6)
            {
                if ($request->ajax()) {
                    if ($request->input('search')) {
                        $acceptStatus=1;
                        $dealstatus=6;
                        $remove_status=1;
                        $won_status_ID=6;
                        $Dealstatus=DealStatus::get();
                        $data = Project::where('title', 'like', '%' . $request->search . '%')->where('acceptStatus',$acceptStatus)->where('dealstatus',$dealstatus)->where('removestatus',$remove_status)->orderBy('title', 'DESC')->paginate(15);
                        return view('admin.deals.includes.view', compact('data','Dealstatus','won_status_ID'))->render();
                    } else {
                        $acceptStatus=1;
                        $dealstatus=6;
                        $remove_status=1;
                        $won_status_ID=6;
                        $Dealstatus=DealStatus::get();
                        $data = Project::where('acceptStatus',$acceptStatus)->where('dealstatus',$dealstatus)->where('removestatus',$remove_status)->orderBy('title', 'DESC')->paginate(15);
                        return view('admin.deals.includes.view', compact('data','Dealstatus','won_status_ID'))->render();
                    }
            }

        }
        if($request->active==7)
        {
            if ($request->ajax()) {
                if ($request->input('search')) {
                    $acceptStatus=1;
                    $dealstatus=7;
                    $remove_status=1;
                    $lost_status_ID=7;
                    $Dealstatus=DealStatus::get();
                    $data = Project::where('title', 'like', '%' . $request->search . '%')->where('acceptStatus',$acceptStatus)->where('dealstatus',$dealstatus)->where('removestatus',$remove_status)->orderBy('title', 'DESC')->paginate(15);
                    return view('admin.deals.includes.view', compact('data','Dealstatus','lost_status_ID'))->render();
                } else {
                    $acceptStatus=1;
                    $dealstatus=7;
                    $remove_status=1;
                    $lost_status_ID=7;
                    $Dealstatus=DealStatus::get();
                    $data = Project::where('acceptStatus',$acceptStatus)->where('dealstatus',$dealstatus)->where('removestatus',$remove_status)->orderBy('title', 'DESC')->paginate(15);
                    return view('admin.deals.includes.view', compact('data','Dealstatus','lost_status_ID'))->render();
                }
        }

    }
    if($request->active==8)
    {
        if ($request->ajax()) {
            if ($request->input('search')) {

                $remove_status=1;
                $all_deal_status_ID=8;
                $Dealstatus=DealStatus::get();
                $data = Project::where('title', 'like', '%' . $request->search . '%')->where('removestatus',$remove_status)->orderBy('title', 'DESC')->paginate(15);
                return view('admin.deals.includes.view', compact('data','Dealstatus','all_deal_status_ID'))->render();
            } else {

                $remove_status=1;
                $all_deal_status_ID=8;
                $Dealstatus=DealStatus::get();
                $data = Project::where('removestatus',$remove_status)->orderBy('title', 'DESC')->paginate(15);
                return view('admin.deals.includes.view', compact('data','Dealstatus','all_deal_status_ID'))->render();
            }
    }

}
}
    public function dealsPage(Request $request)
    {
          $platform = DealPlatform::get();
          $project_type = ProjectType::get();
          $companies = Company::get();
          return  view('admin.deals.includes.addform',compact('platform','project_type','companies'))->render();
    }
    public function addProject($request)
    {
        dd($request);
        $id= Session::get('id');
        $platform_name=DealPlatform::where('id',$request->platform)->first();
        $project=new Project;
        foreach($request as $value)
        {
            $project->title=$value['title'];
            $project->client_name=$value['client_name'];
            $project->project_type=$value['project_type'];
            $project->referred_by=$value['referred_by'];
            $project->platform=$value['platform'];
            $project->platform_id=$value['platform_id'];
            $project->estimated_hours=$value['estimated_hours'];
            $project->job_descriprion=$value['job_descriprion'];
            $project->url=$value['url'];
            $project->client_email=$value['client_email'];
            $project->client_phone=$$value['client_phone'];
            $project->budget=$value['budget'];
            $project->planned_start_date=$value['planned_start_date']??'';
            $project->planned_end_date=$value['planned_end_date']??'';
            $project->actual_start_date=$value['job_descriprion']??'';
            $project->actual_end_date=$value['job_descriprion']??'';
            $project->company_id=$value['company_id'];
            $project->created_by=$value['created_by'];
            $success=$project->save();
        }
        if($success)
        {
            return response()->json(['message'=>'success']);
        }
    }
    public function viewData(Request $request)
    {
        $getProjectData=Project::where('id',$request->id)->first();
        return  view('admin.deals.includes.detailview',compact('getProjectData'))->render();
    }

    public function updateData(Request $request)
    {

        $platform = DealPlatform::get();
        $project_type = ProjectType::get();
        $companies = Company::get();
        $getProjectData=Project::where('id',$request->id)->first();
        return  view('admin.deals.includes.update',compact('getProjectData','companies','project_type','platform'))->render();
    }
    public function UpdateProjectData(UpdateDeals $request)
    {

        $platform_name=DealPlatform::where('id',$request['platform'])->first();
        $update=Project::where('id',$request['id'])->update([
            "title"=>$request['title'],
            "client_name"=>$request['client_name'],
            "project_type"=>$request['department'],
            "referred_by"=>$request['reffer'],
            "platform"=>$request['platform'],
            "platform_id"=>$platform_name['name'],
            "estimated_hours"=>$request['estimated_hour'],
            "job_descriprion"=>$request['job_desc'],
            "url"=>$request['url'],
            "client_email"=>$request['client_email'],
            "client_phone"=>$request['client_phone'],
            "budget"=>$request['price'],
            "planned_start_date"=>$request['start_date'],
            "planned_end_date"=>$request['end_date'],
            "actual_start_date"=>$request['actual_start_date'],
            "actual_end_date"=>$request['actual_end_date'],
            "company_id"=>$request['company']
        ]);
        if($update)
        {
            return response()->json(['message'=>'success']);
        }
        else{
            return response()->json(['message'=>'failed']);
        }
    }
    public function removeData(Request $request)
    {
        $remove_status=0;
        $delete_confirmation=Project::where('id',$request->id)->update(['removestatus'=>$remove_status]);
        if($delete_confirmation)
        {
            return response()->json(['message'=>'success']);
        }
        else{
            return response()->json(['message'=>'failed']);
        }
    }
    public function accept(Request $request)
    {
        $acceptStatus=1;
        $dealstatus=2;
        $toInproccess=Project::where('id',$request->id)->update(['acceptStatus'=>$acceptStatus,'dealstatus'=>$dealstatus]);
        if($toInproccess)
        {
            return response()->json(['message'=>'success']);
        }
        else{
            return response()->json(['message'=>'failed']);
        }
    }
    public function inproccess(Request $request)
    {
        $acceptStatus=1;
        $dealstatus=2;
        $remove_status=1;
        $inproccess_status_ID=2;
        $data=Project::where('acceptStatus',$acceptStatus)->where('dealstatus',$dealstatus)->where('removestatus',$remove_status)->paginate(15);
        $Dealstatus=DealStatus::get();
        return  view('admin.deals.includes.view',compact('data','Dealstatus','inproccess_status_ID'))->render();
    }
    public function inquee(Request $request)
    {
        $status_ID=$request->statusID;

        $status=1;
        $acceptStatus=0;
        $dealstatus=1;
        $remove_status=1;
        $data=Project::where('status',$status)->where('acceptStatus',$acceptStatus)->where('removestatus',$remove_status)->where('dealstatus',$dealstatus)->orderBy('title','DESC')->paginate(15);

        return view('admin.deals.includes.view', compact('data','status_ID'))->render();
    }
    public function fetchInproccess(Request $request)
    {

    if ($request->ajax()) {

        if ($request->input('search')) {
            $acceptStatus=1;
            $dealstatus=1;
            $remove_status=1;
            $ddealstatus=DealStatus::get();
            $inproccessProjectData = Project::where('title', 'like', '%' . $request->search . '%')->where('acceptStatus',$acceptStatus)->where('dealstatus',$dealstatus)->where('removestatus',$remove_status)->orderBy('title', 'DESC')->paginate(15);
            return view('admin.deals.includes.inproccessview', compact('inproccessProjectData','ddealstatus'))->render();
        } else {
            $acceptStatus=1;
            $dealstatus=1;
            $remove_status=1;
            $ddealstatus=DealStatus::get();
            $inproccessProjectData = Project::where('acceptStatus',$acceptStatus)->where('dealstatus',$dealstatus)->where('removestatus',$remove_status)->orderBy('title', 'DESC')->paginate(15);
            return view('admin.deals.includes.inproccessview', compact('inproccessProjectData','ddealstatus'))->render();
        }
    }
}
public function saveInproccess(Request $request)
{
    $d=$request->candidate_id;
    if($d==1)
    {
        $accept_status=0;
        $id= Session::get('id');
        $feed=new FeedModel ;
        $ip_address=request()->ip();
        $feed->comment=$request->comment;
        $feed->ip_address=$ip_address;
        $feed->model_type="App/Project";
        $feed->model_id=$request->status;
        $feed->created_by_id=$id;
        $inproccessCommentResult=$feed->save();
        if($inproccessCommentResult)
        {
            $storeDelasStatus=Project::where('id',$request->status)->update(['acceptStatus'=>$accept_status,'dealstatus'=>$request->candidate_id]);
            return response()->json(['message'=>'success']);
        }
        else{
            return response()->json(['message'=>'failed']);
        }
    }else{
    $id= Session::get('id');
    $feed=new FeedModel ;
    $ip_address=request()->ip();
    $feed->comment=$request->comment;
    $feed->ip_address=$ip_address;
    $feed->model_type="App/Project";
    $feed->model_id=$request->status;
    $feed->created_by_id=$id;
    $inproccessCommentResult=$feed->save();
    if($inproccessCommentResult)
    {
        $storeDelasStatus=Project::where('id',$request->status)->update(['dealstatus'=>$request->candidate_id]);
        return response()->json(['message'=>'success']);
    }
    else{
        return response()->json(['message'=>'failed']);
    }
    }
}
public function meetingData(Request $request)
{
    $acceptStatus=1;
    $dealstatus=3;
    $remove_status=1;
    $meeting_status_ID=3;
    $Dealstatus=DealStatus::get();
    $data=Project::where('acceptStatus',$acceptStatus)->where('dealstatus',$dealstatus)->where('removestatus',$remove_status)->paginate(15);
    return view('admin.deals.includes.view', compact('data','Dealstatus','meeting_status_ID'))->render();
}
public function hotDeals(Request $request)
{
    $acceptStatus=1;
    $dealstatus=5;
    $remove_status=1;
    $hotdeals_status_ID=5;
    $Dealstatus=DealStatus::get();
    $data=Project::where('acceptStatus',$acceptStatus)->where('dealstatus',$dealstatus)->where('removestatus',$remove_status)->paginate(15);
    return view('admin.deals.includes.view', compact('data','Dealstatus','hotdeals_status_ID'))->render();
}
public function frdData(Request $request)
{
    $acceptStatus=1;
    $dealstatus=4;
    $remove_status=1;
    $frd_status_ID=4;
    $Dealstatus=DealStatus::get();
    $data=Project::select('projects.*','dealstatus.id as dealstaus_id','dealstatus.dealname')->join('dealstatus','dealstatus.id','=','projects.dealstatus')->where('acceptStatus',$acceptStatus)->where('dealstatus',$dealstatus)->where('removestatus',$remove_status)->paginate(15);
    return view('admin.deals.includes.view', compact('data','Dealstatus','frd_status_ID'))->render();
}
public function wonDeal(Request $request)
{
    $acceptStatus=1;
    $dealstatus=6;
    $remove_status=1;
    $won_status_ID=6;
    $Dealstatus=DealStatus::get();
    $data=Project::select('projects.*','dealstatus.id as dealstaus_id','dealstatus.dealname')->join('dealstatus','dealstatus.id','=','projects.dealstatus')->where('acceptStatus',$acceptStatus)->where('dealstatus',$dealstatus)->where('removestatus',$remove_status)->paginate(15);
    return view('admin.deals.includes.view', compact('data','Dealstatus','won_status_ID'))->render();
}
public function lostDeal(Request $request)
{
    $acceptStatus=1;
    $dealstatus=7;
    $remove_status=1;
    $lost_status_ID=7;
    $Dealstatus=DealStatus::get();
    $data=Project::select('projects.*','dealstatus.id as dealstaus_id','dealstatus.dealname')->join('dealstatus','dealstatus.id','=','projects.dealstatus')->where('acceptStatus',$acceptStatus)->where('dealstatus',$dealstatus)->where('removestatus',$remove_status)->paginate(15);
    return view('admin.deals.includes.view', compact('data','Dealstatus','lost_status_ID'))->render();
}
public function allDeal(Request $request)
{
    $remove_status=1;
    $all_deal_status_ID=8;
    $Dealstatus=DealStatus::get();
    $data=Project::where('removestatus',$remove_status)->paginate(15);
    return view('admin.deals.includes.view', compact('data','Dealstatus','all_deal_status_ID'))->render();
}
public function fetchMeeting(Request $request)
{
    if ($request->ajax()) {
        if ($request->input('search')) {
            $acceptStatus=1;
            $dealstatus=2;
            $remove_status=1;
            $Dealstatus=DealStatus::get();
            $meetingData = Project::where('title', 'like', '%' . $request->search . '%')->where('acceptStatus',$acceptStatus)->where('dealstatus',$dealstatus)->where('removestatus',$remove_status)->orderBy('title', 'DESC')->paginate(15);
            return view('admin.deals.includes.meetingview', compact('meetingData','Dealstatus'))->render();
        } else {
            $acceptStatus=1;
            $dealstatus=2;
            $remove_status=1;
            $Dealstatus=DealStatus::get();
            $meetingData = Project::where('acceptStatus',$acceptStatus)->where('dealstatus',$dealstatus)->where('removestatus',$remove_status)->orderBy('title', 'DESC')->paginate(15);
            return view('admin.deals.includes.meetingview', compact('meetingData','Dealstatus'))->render();
        }
}
}
public function fetchHotDeals(Request $request)
{
    if ($request->ajax()) {
        if ($request->input('search')) {
            $acceptStatus=1;
            $dealstatus=7;
            $remove_status=1;
            $Dealstatus=DealStatus::get();
            $hotdealData = Project::where('title', 'like', '%' . $request->search . '%')->where('acceptStatus',$acceptStatus)->where('dealstatus',$dealstatus)->where('removestatus',$remove_status)->orderBy('title', 'DESC')->paginate(15);
            return view('admin.deals.includes.hotdeal', compact('hotdealData','Dealstatus'))->render();
        } else {
            $acceptStatus=1;
            $dealstatus=7;
            $remove_status=1;
            $Dealstatus=DealStatus::get();
            $hotdealData = Project::where('acceptStatus',$acceptStatus)->where('dealstatus',$dealstatus)->where('removestatus',$remove_status)->orderBy('title', 'DESC')->paginate(15);
            return view('admin.deals.includes.hotdeal', compact('hotdealData','Dealstatus'))->render();
        }
}
}
    //************************************ Permission End *********************************************//
    //******************************************************* Agency **************************************//
    public function ViewAgency()
    {
        $data = $this->AgencyData();
        return view('admin.agency.create', compact('data'));
    }
    public function ViewUpdateAgency(request $request)
    {
        $agency = Admin::where('id', $request->id)->where('role', 2)->first();
        return view('admin.agency.includes.update', compact('agency'))->render();
    }
    public function AgencyBack()
    {
        $data = $this->AgencyData();
        return view('admin.agency.main', compact('data'))->render();
    }
    public function AgencyDetailsView(request $request)
    {
        $agency = Admin::where('id', $request->id)->where('role', 2)->first();
        return view('admin.agency.includes.detailview', compact('agency'))->render();
    }
    public function DeleteAgency(request $request)
    {
        $delete = Admin::where('id', $request->id)->delete();
        $data = $this->AgencyData();
        return view('admin.agency.includes.view', compact('data'));
    }
    public function ToggleAgencySatus(request $request)
    {
        $check_status = Admin::where('id', $request->id)->first();
        $status = $this->checkStatus($check_status->status);

        $get_Result = $this->toggleStatusDB($status, 'Admin', $request->id);
        if ($request->input('search')) {
            $data = Admin::where('id', '!=', Auth::user()->id)->where('role', 2)->where('name', 'like', '%' . $request->search . '%')->orderBy('id', 'DESC')->paginate(15);
            return view('admin.agency.includes.view', compact('data'))->render();
        } else {
            $data = $this->AgencyData();
            return view('admin.agency.includes.view', compact('data'))->render();
        }
    }
    public function AddAgency(AgencyRequest $request)
    {
        $image_name = $this->UploadImage($request->file('image'), 'images');
        $insert = new Admin;
        $insert->agency_code = strtoupper(substr(uniqid(), -7));
        $insert->name = $request->name;
        $insert->email = $request->email;
        $insert->phone_number = $request->phone_number;
        $insert->country_code = $request->country_code;
        $insert->country = $request->country;
        $insert->state = $request->state;
        $insert->city = $request->city;
        $insert->address = $request->address;
        $insert->license_id = $request->license_id;
        $insert->license_file = $image_name;
        $insert->legal_advisor = $request->legal_advisor;
        $insert->business_type = $request->business_type;
        $insert->password = Hash::make($request->password);
        $insert->created_by_id = Auth::user()->id;
        $insert->role = 2;
        $insert->save();
        return response()->json(redirect()->back()->with('success', 'New Agency is added Successfully'));
    }
    public function UpdateAgency(AgencyRequest $request)
    {
        $image_name = $this->DBimageUpload($request->file('image'), 'Admin', $request->id, 'license_file', 'images');
        $update = Admin::where('id', $request->id)->where('role', 2)->update([
            'name' => $request->name,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'country_code' => $request->country_code,
            'country' => $request->country,
            'state' => $request->state,
            'city' => $request->city,
            'address' => $request->address,
            'license_id' => $request->license_id,
            'license_file' => $image_name,
            'legal_advisor' => $request->legal_advisor,
            'business_type' => $request->business_type,
        ]);
        return response()->json(redirect()->back()->with('success', 'Agency is Updated Successfully'));
    }

    public function FetchAgency(request $request)
    {
        if ($request->ajax()) {
            if ($request->input('search')) {
                $data = Admin::where('id', '!=', Auth::user()->id)->where('role', 2)->where('name', 'like', '%' . $request->search . '%')->orderBy('id', 'DESC')->paginate(15);
                return view('admin.agency.includes.view', compact('data'))->render();
            } else {
                $data = $this->AgencyData();
                return view('admin.agency.includes.view', compact('data'))->render();
            }
        }
    }
    public function AgencyData()
    {
        return Admin::where('id', '!=', Auth::user()->id)->where('role', 2)->orderBy('id', 'DESC')->paginate(15);
    }
    //****************************************** Agency End ****************************************//
    //******************************************Staff  **********************************************//
    public function RemoveStaff()
    {
        $delete = Admin::where('id', $request->id)->delete();
        $data = $this->StaffData();
        return view('admin.staff.includes.view', compact('data'))->render();
    }
    public function DetailViewStaff(request $request)
    {
        $staff = Admin::where('id', $request->id)->where('role', 1)->first();
        return view('admin.staff.includes.detailview', compact('staff'))->render();
    }
    public function ViewUpdateStaff(request $request)
    {
        $staff = Admin::where('id', $request->id)->where('role', 1)->first();
        return view('admin.staff.includes.update', compact('staff'))->render();
    }
    public function UpdateStaff(StaffRequest $request)
    {
        $update = Admin::where('id', $request->id)->where('role', 1)->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone_number' => $request->phone,
            'email' => $request->email,
            'country_code' => $request->country_code
        ]);
        return response()->json(redirect()->back()->with('success', 'Staff is Updated Successfully'));
    }
    public function StaffBack()
    {
        $data = $this->StaffData();
        $permissions = PD::where('status', 1)->select('id', 'title')->get();
        return view('admin.staff.main', compact('data', 'permissions'))->render();
    }
    public function StaffData()
    {
        return Admin::orderBy('id', 'DESC')->where('role', 1)->paginate(15);
    }

    //****************************************** Staff End *******************************************//
    //****************************************** Streamer *******************************************//
    public function DetailViewStreamer(request $request)
    {
        $streamer = User::where('id', $request->id)->where('role_id', 2)->first();
        return view('admin.streamers.includes.detailview1', compact('streamer'))->render();
    }

    public function streamerProfile(Request $request)
    {
        $streamer = St::where('id', $request->id)->where('role_id', 2)->first();
        return view('admin.revenue.includes.streamer_profile', compact('streamer'))->render();
    }

    public function StreamerBack()
    {
        if (Auth::user()->role != User::ROLE_ADMIN) {

            $agency = Agency::find(Auth::user()->id);
            $data =  $agency->streamers()->paginate(15);

        return view('admin.streamers.main', compact('data'));
        } else {
            $data = User::where('role_id', 2)->orderBy('id', 'DESC')->paginate(15);
            return view('admin.streamers.main', compact('data'))->render();
        }

    }
    public function adminProfile()
    {
        $admin = User::where('id', Auth::user()->id)->first();
        return view('admin.profile', compact('admin'));
    }

    public function staffList(Request $request)
    {

        $permissions = PermissionLevel::where('status', 1)->select('id', 'title')->get();
        $data = Admin::orderBy('id', 'DESC')->where('role', 1)->paginate(15);
        return view('admin.staff.create', compact('data', 'permissions'));
    }

    public function home()
    {
        $currentAnnouncement = Announcement::where('updated_at','>',Carbon::now()->subdays(1))->where('status',1)->count();
        // dd($currentAnnouncement);
        $getAnnouncements = Announcement::select('announcements.*','user_announcements.user_id')->where('announcements.status',1)
        ->leftJoin('user_announcements', 'announcements.id', 'user_announcements.announcement_id')
        ->where('user_announcements.user_id', null)
        ->orwhere('user_announcements.user_id', Auth::user()->id)
        ->get();
        // ->where('   ')
        $data = [];
        $getpunch = PunchTiming::where('user_id', auth()->user()->id)->whereDate('punch_in', Carbon::today())->sum('total_time');
        $latest_time = PunchTiming::where('user_id', auth()->user()->id)->whereDate('punch_in', Carbon::today())->where('punch_out',null)->orderBy('id','DESC')->first();
        $punched_in = PunchTiming::where('user_id', auth()->user()->id)->whereDate('punch_in', Carbon::today())->where('punch_out',null)->count();
        $currentsession = 0;
        if(!empty($latest_time)){
            $startTime = Carbon::parse(strtotime($latest_time->punch_in));
            $endTime = Carbon::now();
            $currentsession =  $startTime->diffInMinutes($endTime,false);
            $previous_time = (explode(" ", $latest_time->punch_in));
            $previous_time = new DateTime($previous_time[1]);
            $test = new DateTime(date("H:i:s"));
            $data['diffTime'] =$test->getTimestamp() - $previous_time->getTimestamp();
            $data['diffTime'] += $getpunch;

        }
        $getpunch  = $getpunch+$currentsession;

        $in_minuts = $getpunch;
        $getpunch  = date('H:i', mktime(0,$getpunch));
        // $data['currentsession'] = $currentsession;
        $data['in_minuts'] = $in_minuts??0;
        $data['punchings_sum'] = $getpunch??"0:0";
        $data['punched_in'] = $punched_in??0;
        $data['permission'] = PD::count();
        $currentUser = User::where('id', Auth::user()->id)
                        ->with([
                            'tasksAssigned' => function($qry){
                            $qry->where('task_status_id', 2);
                            $qry->orderBy('priority_level');
                            $qry->orderBy('updated_at', 'DESC');
                            },
                            'tasksAssigned.project:id,title',
                            'todos'=> function ($qry){
                                $qry->orderBy('priority_level');
                                $qry->orderBy('updated_at', 'DESC');
                            },
                            'quickTasks' => function($qry){
                                $qry->whereIn('child_status_id', [1, 2]);
                                $qry->orderBy('priority_level');
                                $qry->orderBy('updated_at', 'DESC');
                            }
                            ])->first('id');
        $currentDate = date('m-d');
        $usersWithBirthdayToday = User::where('date_of_birth', 'like', "%$currentDate%")->where('login_toggle', 1)->get(['id', 'name', 'date_of_birth']);
        return view('admin.home', compact('data', 'currentUser','getAnnouncements', 'usersWithBirthdayToday','currentAnnouncement'));
    }


    public function punchin_time(Request $request)
{
    $user_id = auth()->user()->id;
    $getpunch = PunchTiming::where('user_id', $user_id)->where('punch_out','=',null)->orderBy('id','DESC')->first();
    $attendanceModel= Attendance::where(["date"=>date("Y-m-d"),"created_by"=>$user_id])->first();
   
    if(!$getpunch){
        if(empty($attendanceModel)){
            $attendanceModel=new Attendance();
            $attendanceModel->date= Carbon::now()->toDateString();
            $attendanceModel->in_at=Carbon::now();
            $attendanceModel->total_time=0;
            $attendanceModel->created_by=$user_id;
            $attendanceModel->save();
        }
        PunchTiming::create([
            'user_id'    =>  $user_id,
            'attendance_id'    =>  $attendanceModel->id,
            'punch_in'    =>  Carbon::now(),
            'in_ip_address'=>request()->ip(),

        ]);
    }
   
    else{
        //older
        // $startTime = Carbon::parse(strtotime($getpunch->punch_in));
        // $endTime = Carbon::now();
        // $result =  $startTime->diffInMinutes($endTime,false);

        $startTime = strtotime($getpunch->punch_in);
        $getpunch->punch_out=Carbon::now();

        $endTime = strtotime($getpunch->punch_out);
        $result =  $endTime-$startTime;
        $getpunch->total_time=$result;
        $getpunch->out_ip_address=request()->ip();

        $getpunch->save();

      $attendanceModel= $getpunch->getAttendance;
      $attendanceModel->total_time+=$result;
      $attendanceModel->out_at=Carbon::now();
      $attendanceModel->save();

    }
    
    $getpunch = PunchTiming::where('user_id', $user_id)->whereDate('punch_in', Carbon::today())->sum('total_time');
    $punched_in = PunchTiming::where('user_id', auth()->user()->id)->whereDate('punch_in', Carbon::today())->where('punch_out',null)->count();
    $latest_time = PunchTiming::where('user_id', auth()->user()->id)->whereDate('punch_in', Carbon::today())->where('punch_out',null)->orderBy('id','DESC')->first();
    $currentsession =0;
    if(!empty($latest_time)){
            $startTime = Carbon::parse(strtotime($latest_time->punch_in));
            $endTime = Carbon::now();
            $currentsession =  $startTime->diffInMinutes($endTime,false);
        }
    $getpunch  = $getpunch+$currentsession;
    $in_minuts = $getpunch;
    if($getpunch>60){
        $getpunch  = date('H:i', mktime(0,$getpunch));
    }
    $isTimerStopped = false;
    if($punched_in == 0){
        $isTimerStopped = Auth::user()->stopAllTaskTimers();
    }
    return response()->json(['time'=>$getpunch,'in_minuts'=>$in_minuts, 'punch'=>$punched_in, 'isTimerStopped'=>$isTimerStopped]);
}

    public function updateProfile(Request $request)
    {
        // $admin = Admin::where('email', $request->email)->first();
        $admin = Admin::where('id', Auth::user()->id)->first();
        $validator = validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|unique:admin,email,' . $admin->id . 'id',
            'phone_number' => 'required|digits_between:10,15|unique:admin,phone_number,' . $admin->id . 'id',
            // 'password' => 'required|min:6|required_with:confirm_password',
            // 'confirm_password' => 'required_with:password|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ), 422);
        }
        $staff->first_name = $request->first_name ?? $admin->first_name;
        $staff->last_name = $request->last_name ?? $admin->last_name;
        $staff->email = $request->email ?? $admin->email;
        $staff->phone_number = $request->phone_number ?? $admin->phone_number;
        // $staff->password = Hash::make($request->password)??$staff->password;
        // $url ='admin/profile'.'#update_profile';
        if ($staff->save()) {

            return response()->json(redirect()->back()->with('success', 'Profile updated successfully.'));
        } else {

            return response()->json(redirect()->back()->with('error', "Some error occured! Please try again."));
        }
    }

    public function fetchStaffList(request $request)
    {
        if ($request->ajax()) {
            if ($request->input('search')) {
                $data = Admin::where('first_name', 'like', '%' . $request->search . '%')->where('role', 1)->orderBy('last_name', 'DESC')->paginate(15);
                return view('admin.staff.includes.view', compact('data'))->render();
            } else {
                $data = Admin::orderBy('id', 'DESC')->where('role', 1)->paginate(15);
                return view('admin.staff.includes.view', compact('data'))->render();
            }
        }
    }

    public function toggleStaffStatus(Request $request)
    {
        $check_status = Admin::where('id', $request->id)->where('role', 1)->first();
        $status = $this->checkStatus($check_status->status);
        $get_Result = $this->toggleStatusDB($status, 'Admin', $request->id);
        if ($request->input('search')) {
            $data = Admin::where('first_name', 'like', '%' . $request->search . '%')->where('role', 1)->orderBy('id', 'DESC')->paginate(15);
            return view('admin.staff.includes.view', compact('data'))->render();
        } else {
            $data = Admin::orderBy('id', 'DESC')->where('role', 1)->paginate(15);
            return view('admin.staff.includes.view', compact('data'))->render();
        }
    }

    public function searchStaff(Request $request)
    {
        if ($request->ajax()) {
            if ($request->input('search')) {
                $data = Admin::where('first_name', 'like', '%' . $request->search . '%')->where('role', 1)->orderBy('last_name', 'DESC')->paginate(15);
                return view('admin.staff.includes.view', compact('data'))->render();
            } else {
                $data = Admin::orderBy('id', 'DESC')->where('role', 1)->paginate(15);
                return view('admin.staff.includes.view', compact('data'))->render();
            }
        }
    }
    public function deleteStaff(request $request)
    {
        $delete = Admin::where('id', $request->id)->delete();
        $data = Admin::orderBy('id', 'DESC')->where('role', 1)->paginate(15);
        return view('admin.staff.includes.view', compact('data'))->render();
    }

    public function addStaff(StaffRequest $request)
    {
        $staff = new Admin;
        $staff->first_name = $request->first_name;
        $staff->last_name = $request->last_name;
        $staff->phone_number = $request->phone;
        $staff->country_code = $request->country_code;
        $staff->email = $request->email;
        $staff->role = 1;
        $staff->password = Hash::make('password');
        $staff->created_by_id = Auth::user()->id;
        if(is_array($request->user_permission)){
            $request->user_permission=implode(",",$request->user_permission);
        }
        $staff->permission_level = $request->user_permission;
        $staff->save();
        return redirect()->back()->with('success', 'Staff profile created succesfully.');
    }

    public function addStreamer(Request $request)
    {

        $image_name = $this->UploadImage($request->file('profile_image'), 'images');

        $streamer = new User;
        $streamer->firstName = $request->first_name;
        $streamer->lastName = $request->last_name;
        $streamer->phone = $request->phone;
        $streamer->gender = $request->gender;
        $streamer->gender = $request->email;
        $streamer->dob = $request->streamer_dob;
        $streamer->profilePics = $image_name;
        $streamer->latitude = "";
        $streamer->longitude = "";
        $streamer->streamer_code = strtoupper(substr(uniqid(), -7));
        $streamer->role_id = 2;
        $streamer->email = $request->email;
        $streamer->created_by_id = Auth::user()->id;
        $streamer->save();
        return redirect()->back()->with('success', 'Streamer profile created succesfully.');
        // response()->json(redirect()->back()->with('success', 'Streamer profile created succesfully.'));
    }

    public function streamerList()
    {
        $loggedInUser = Auth::user();
        if($loggedInUser->role == User::ROLE_ADMIN){
            $data = User::where('role_id', 2)->orderBy('id', 'DESC')->paginate(15);
        }else{
            $agency = Agency::find($loggedInUser->id);
            $data =  $agency->streamers()->paginate(15);
        }

        return view('admin.streamers.create', compact('data'));
    }

    public function FetchStreamerList(request $request)
    {

        if ($request->ajax()) {
            $loggedInUser = Auth::user();
            if ($request->input('search')) {
                if($loggedInUser->role == User::ROLE_ADMIN){
                $data = User::where('firstName', 'like', '%' . $request->search . '%')->where('role_id', 2)->orderBy('id', 'DESC')->paginate(15);
                }else{
                    $agency = Agency::find($loggedInUser->id);
                    $data =  $agency->streamers()->where('firstName', 'like', '%' . $request->search . '%')->paginate(15);
                }
                return view('admin.streamers.includes.view', compact('data'))->render();
            } else {
                if($loggedInUser->role == User::ROLE_ADMIN){

                $data = User::where('role_id', 2)->orderBy('id', 'DESC')->paginate(15);
                }else{
                    $agency = Agency::find($loggedInUser->id);
                    $data =  $agency->streamers()->paginate(15);
                }
                return view('admin.streamers.includes.view', compact('data'))->render();
            }
        }
    }


    public function toggleStreamerStatus(Request $request)
    {
        $check_status = User::where('id', $request->id)->first();
        $status = $this->checkStatus($check_status->enabled);
        $get_Result = $this->toggleStatusDB($status, 'User', $request->id, 'enabled');
        if ($request->input('search')) {
            $data = User::where('firstName', 'like', '%' . $request->search . '%')->where('role_id', 2)->orderBy('id', 'DESC')->paginate(15);
            return view('admin.streamers.includes.view', compact('data'))->render();
        } else {
            $data = User::where('role_id', 2)->orderBy('id', 'DESC')->paginate(15);
            return view('admin.streamers.includes.view', compact('data'))->render();
        }
    }

    public function deleteStreamer(request $request)
    {
        $delete = User::where('id', $request->id)->where('role_id', 2)->delete();
        $data = User::where('role_id', 2)->orderBy('id', 'DESC')->paginate(15);
        return view('admin.streamers.includes.view', compact('data'))->render();
    }

    public function ViewUpdateStreamer(request $request)
    {
        $streamer = User::where('id', $request->id)->where('role_id', 2)->first();
        // return $streamer;
        return view('admin.streamers.includes.update', compact('streamer'))->render();
    }
    public function UpdateStreamer(Request $request)
    {
        if ($request->hasFile('profile_image')) {
            $image_name = $this->UploadImage($request->file('profile_image'), 'images');
            $update =  User::where('id', $request->id)->where('role_id', 2)->update(['profilePics' => $image_name]);
        }

        $update =  User::where('id', $request->id)->where('role_id', 2)->update([
            'firstName' => $request->first_name,
            'lastName' => $request->last_name,
            'phone' => $request->phone,
            'email' => $request->email,
            'gender' => $request->gender,
            'dob' => $request->streamer_dob
        ]);
        return response()->json(redirect()->back()->with('success', 'Streamer profile Updated Successfully'));
    }
    // public function searchStreamer(Request $request){
    //     if($request->ajax())
    //     {
    //        if($request->input('search')){
    //           $data=User::where('first_name','like','%'.$request->search.'%')->orderBy('last_name','DESC')->paginate(15);
    //           return view('admin.streamers.includes.view', compact('data'))->render();
    //        }else{
    //           $data=User::where('role_id', 2)->orderBy('id','DESC')->paginate(15);
    //           return view('admin.streamers.includes.view', compact('data'))->render();
    //        }
    //     }
    // }

    public function agencyHome()
    {

        $agency = Agency::find(3);

        $streamers = User::where('role_id', 2)->orderBy('id', 'DESC')->paginate(7);
        return view('admin.agency.dashboard', compact('streamers', 'agency'));
    }

    public function agencyProfile(Request $request)
    {
        $agency = Agency::where('id', $request->id)->where('role', 2)->first();
        $data = St::where('role_id', 2)->where('agency_id', $request->id)->orderBy('id', 'DESC')->paginate(15);
        return view('admin.revenue.includes.agency_profile', compact('agency', 'data'))->render();
    }

    public function sidebarAgencyProfile(Request $request)
    {
        $agency = Agency::where('id', $request->id)->where('role', 2)->first();
        return view('admin.agency.includes.agency_profile', compact('agency'))->render();
    }

    public function adminRevenue(Request $request)
    {

        $streamersRevenue = CoinHistory::whereIn('type', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10])->where('transaction_type', 1)->sum('coins');

        $agencies = Agency::where('role', 2)->get();
        $agenciesRevenue = 0;
        foreach ($agencies as $agency) {
            $agenciesRevenue = $agenciesRevenue + $agency->agencyIncome();
        }
        // $agency = Agency::find(3);
        $streamers = User::where('role_id', 2)->orderBy('id', 'DESC')->paginate(7);
        return view('admin.revenue.dashboard', compact('streamers', 'streamersRevenue', 'agenciesRevenue'));
    }

    public function revenueStreamerList()
    {
        $streamers = St::where('role_id', 2)->orderBy('id', 'DESC')->paginate(7);
        return view('admin.revenue.includes.streamers', compact('streamers'))->render();
    }
    public function ajaxGridFatchData(request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->type)) {
                $datas = Transaction::where(['type' => $request->type, 'user_id' => $request->user_id])->orderBy('id', 'DESC')->paginate(5);
            } else {
                $datas = Transaction::where(['user_id' => $request->user_id])->orderBy('id', 'DESC')->paginate(5);
            }
            // /$datas = Transaction::where('type', $request->type)->orderBy('id', 'DESC')->paginate(5);
            return view('admin.revenue.includes._table_pagination', compact('datas'))->render();
        }
    }

    public function fetchRevenueStreamer(request $request)
    {
        if ($request->ajax()) {
            if ($request->input('search')) {
                $streamers = St::where('role_id', 2)->orderBy('id', 'DESC')->paginate(7);
                return view('admin.revenue.includes.streamers', compact('streamers'))->render();
            } else {
                $streamers = St::where('role_id', 2)->orderBy('id', 'DESC')->paginate(7);
                return view('admin.revenue.includes.streamers', compact('streamers'))->render();
            }
        }
    }

    public function revenueAgencyList()
    {

        $agencies = Agency::where('role', 2)->paginate(7);
        return view('admin.revenue.includes.agencies', compact('agencies'))->render();
    }

    public function TabsFilter(request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->type)) {
                $datas = Transaction::where(['type' => $request->type, 'user_id' => $request->user_id])->orderBy('id', 'DESC')->paginate(5);
            } else {
                $datas = Transaction::where(['user_id' => $request->user_id])->orderBy('id', 'DESC')->paginate(5);
            }
            return view('admin.revenue.includes._table_pagination', compact('datas'))->render();
        }
    }

    public function revenueAgencyStreamers(Request $request)
    {
        $data = St::where('role_id', 2)->where('agency_id', $request->id)->orderBy('id', 'DESC')->paginate(15);
        return view('admin.revenue.includes.agency_streamers', compact('data'));
    }

    public function withdrawList(){
        $data = WithdrawHistory::paginate(10);
        // return $data;
        return view('admin.withdraw.create', compact('data'));
    }

    public function withdrawListFilter(request $request)
    {
        if ($request->ajax()) {

            if (!empty($request->type)) {
                $data = WithdrawHistory::where('status',$request->type)->paginate(10);
            } else {
                $data = WithdrawHistory::paginate(10);
            }
            return view('admin.withdraw.includes._table_pagination', compact('data'))->render();
        }
    }

    public function toggleWithdrawStatus(Request $request)
    {

        $withdrawRequest = WithdrawHistory::where('id', $request->id)->update(['status'=> $request->status]);

            $data = WithdrawHistory::paginate(10);
            return view('admin.withdraw.includes._table_pagination', compact('data'))->render();

    }

    public function viewPermissionLevel()
    {

        $data = PermissionLevel::orderBy('id', 'DESC')->paginate(15);
        $permissions = PD::get();
        return view('admin.permissionLevel.create', compact('data', 'permissions'))->render();
    }

    public function addPermissionLevel(Request $request)
    {
        $permissions = "";
        $request->validate([
            'permission_level_title' => 'bail|required|string|max:255',
        ]);
        if($request->filled('permission_ids')){
        $permissions =  implode(",",$request->permission_ids);
        }
        $insert = new PermissionLevel;
        $insert->title = $request->permission_level_title;
        $insert->permission_ids = $permissions;
        $insert->save();
        return redirect()->back()->with('success', 'Record created successfully.');
    }

    public function ViewUpdatePermissionLevel(request $request)
    {
        $permission = PermissionLevel::where('id', $request->id)->first();
        return view('admin.permissionLevel.includes.update', compact('permission'))->render();
    }

    public function updatePermissionLevel(Request $request)
    {

        $request->validate([
            'permission_level_title' => 'bail|required|string|max:255',
        ]);
        $update = PermissionLevel::where('id', $request->id)->update([
            "title" => $request->permission_level_title,
        ]);
        return response()->json(redirect()->back()->with('success', 'Record Updated Successfully'));
    }
}
