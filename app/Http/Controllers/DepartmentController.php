<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Models\Department;
use Validator;
use Log;

class DepartmentController extends Controller
{
   public function viewDepartments( Request $req )
   {
	   $departments = Department::orderBy('id', 'DESC')->paginate(10);
	   return view('admin.departments.main', compact('departments') );
   }

   public function storeDepartment( Request $req )
   {

	   $validation_rules = [
		    'name'  => 'required|string|max:255',
	   ];

	   $validator = Validator::make( $req->all(), $validation_rules );
	   if ( $validator->fails() ){

		   $errors = $validator->getMessageBag()->toArray();
		   return response()->json( compact('errors'), 422 );
	   }

	   $department = new Department;
	   $department->name = $req->name;
	   $department->save();
	   return response()->json( ['success' => true], 200);
   }

   public function searchDepartments( Request $req )
   {


	   if ( $req->filled('searched_keyword') ){

		   $departments = Department::where('name','like',"%$req->searched_keyword%")->orderBy('id', 'DESC')->paginate(10);
	   } else {

		   $departments = Department::orderBy('id', 'DESC')->paginate(10);
	   }

	   return view('admin.departments.includes.view', compact('departments') )->render();
   }

   public function deleteDepartment( Request $req )
   {

	   $department = Department::find( $req->departmentId );
	   $department->delete();

	   return response()->json( ['success' => true], 200);
   }

   public function editDepartment( Request $req )
   {
	   $department = Department::find(  $req->departmentId );
	   return view('admin.departments.includes.update', compact('department') );
   }

   public function updateDepartment( Request $req )
   {
	   $validation_rules = [
		    'name'  => 'required|string|max:255',
	   ];

	   $validator = Validator::make( $req->all(), $validation_rules );
	   if ( $validator->fails() ){

		   $errors = $validator->getMessageBag()->toArray();
		   return response()->json( compact('errors'), 422 );
	   }

	   $department = Department::find( $req->department_id );
	   $department->name = $req->name;
	   $department->save();

	   return response()->json( ['success' => true ], 200 );
   }
}
