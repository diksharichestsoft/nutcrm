<?php

namespace App\Http\Controllers;

use App\Models\Announcement;
use App\Models\Portfolio;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
//use App\Jobs\Dispatch;

class AnnouncementController extends Controller
{
    public function index(){
        return view('admin.announcements.main');
    }

    public function announcementStore(Request $request)
    {

        $this->validate($request, [
            'title' => 'required',
            'type' => 'required',
            'user_id'=>'required'
        ]);

        $data = $request->input();
            //  print_r($data);
            // die("hhghjggg");
        array_shift($data);
        unset($data["id"]);
        unset($data["reqType"]);
        unset($data["user_id"]);


        if($request->reqType == 0){
            $isCreated = Announcement::create($data);
        }else{
            $isCreated = Announcement::whereId($request->id)->update($data);
            $isCreated = Announcement::whereId($request->id)->first();
        }
        if($isCreated){
            //Dispatch::dispatchAfterResponse();
            if(!(in_array(0, $request->user_id))){
                $isCreated->users()->sync($request->user_id);
            }else{
                $isCreated->users()->sync([]);
            }
            return response()->json(['message'=>'success'],200);
        }else{
            return response()->json(['message'=>"error"], 400);
        }
    }

    // public function announcementToggle(Request $request){

    //     Announcement::where('id','!=',$request->id)->update([
    //         'status'=> 0
    //     ]);

    //     $data=Announcement::where('id',$request->id)->first();
    //     if($data->status == 0){
    //         $data->status = 1;
    //         $data->save();
    //     }
    //     return response()->json(['message'=>'success'],200);
    // }

    public function announcementToggle(Request $request){
          $data=Announcement::whereId($request->id)->first();
          $enable =($data->status==2)?1:2;
          Announcement::whereId($request->id)->update(['status'=>$enable]);
          return response()->json(['message'=>'success'],200);
      }

      public function announcementEditPage(Request $request)
    {   $thisAnnouncement = Announcement::with(['users'])->whereId($request->id)->first();
        $users_name = array();
        foreach ($thisAnnouncement->users as $key => $value) {
            array_push($users_name, $value->id);
        }
        $users = User::get(['id', 'name']);

        return view('admin.announcements.includes.update', compact('thisAnnouncement', 'users', 'users_name'));
    }

      public function announcementRemove(Request $request)
    {
        $isDeleted = Announcement::whereId($request->id)->first();
        $isDeleted->status = 0;
        $isDeleted->save();
        return response()->json(['message'=>'success'],200);
    }

    public function announcementDetail(Request $request)
    {
        $thisAnnouncement = Announcement::whereId($request->id)->first();
        return view('admin.announcements.includes.detail', compact('thisAnnouncement'));
    }

    public function filter(request $request)
    {
        $users = User::get(['id', 'name']);
        if ($request->ajax()) {
            $qry = Announcement::where('status', '!=', 0);
            if(!empty($request->search)){
               $search = $request->search;
                $qry->where(function ($q) use ($search) {
                    $q->orWhere('announcements.id','like',"%$search%");
                    $q->orWhere('announcements.title','like',"%$search%");
                    $q->orWhere('announcements.description','like',"%$search%");
                });
            $annoncements = $qry->paginate(15);
            return view('admin.announcements.includes.view', compact('annoncements','users'))->render();
            }
            else {
                $annoncements = Announcement::where('status', '!=', 0)->paginate(15);
                return view('admin.announcements.includes.view', compact('annoncements', 'users'))->render();
            }
        }
    }

    // public function filter(Request $request)
    // {
    //     $annoncements = Announcement::where('created_by', Auth::user()->id);
    //     $deptArr = array(
    //             1 => "highest",
    //             2 => "medium",
    //         );
    //     if(!empty($request->search)){
    //         $search = $request->search;
    //         $index = array_search($search, $deptArr);
    //         $annoncements->where(function ($q) use ($search, $index) {
    //             $q->orWhere('title','like',"%$search%");
    //             $q->orWhere('description','like',"%$search%");
    //             $q->orWhere('type','like',"%$index%");
    //            });
    //     }
    //     $annoncements = $annoncements->orderBy('id')->paginate(50);
    //     return view('admin.announcements.includes.view', compact('annoncements'));

    // }
    }

