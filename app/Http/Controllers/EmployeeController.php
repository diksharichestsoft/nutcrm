<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Traits\ImageUpload;
use App\Models\Company;
use App\Models\Department;
use App\Models\ProjectType;
use App\Models\TeamLead;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class EmployeeController extends Controller
{
    use ImageUpload;
    public function viewEmployeesData(Request $request)
    {
        $users = User::orderBy('id','desc')->paginate(15);
        return view('admin.employees.main', compact('users'));
    }
    public function paginationData(Request $request)
    {
        $users = User::orderBy('id','desc')->paginate(15);
        return view('admin.employees.includes.view', compact('users'))->render();
        }
    public function employeeSearch(Request $request)
    {
        $search = $request->search;
        $users = User::where('name','like',"%$search%")->orderBy('id','desc')->paginate(15);
        return view('admin.employees.includes.view', compact('users'))->render();
    }
    public function employeesCreatePage()
    {
        $users = User::all();
        $roles = Role::all();
        $companies = Company::all();
        $departments = Department::all();
        return view('admin.employees.create', compact('users', 'roles', 'companies', 'departments'));
    }
    public function employeesEditPage(Request $request)
    {
        $users = User::all();
        $thisEmployee = User::whereId($request->id)->first();
        $roles = Role::all();
        $departments = Department::all();
	$companies = Company::all();

	$thisEmployeeDepartmentsIds = $thisEmployee->departments->pluck('id');
	$thisEmployeeTeamLeadIds = $thisEmployee->teamLead->pluck('id');

        return view('admin.employees.update', compact('users', 'roles', 'companies', 'departments', 'thisEmployee','thisEmployeeDepartmentsIds', 'thisEmployeeTeamLeadIds'));
    }
    public function employeeStore(Request $request)
    {
        if($request->hasFile('profile_image')){
            $image_name=$this->UploadImage($request->file('profile_image'),'images');
            // $request->input()['profile_image'] = $image_name;
        }
        ($request->password != null) ? $password = Hash::make($request->password) : '';
        $isUpdate = true;
        if($request->reqType == 0){
            $this->validate($request, [
		    'password' => 'required',
		    'email'    => 'required|unique:users',

            ]);

        }

	$this->validate($request,
        [
        'company_id'      => 'required',
        'name'            => 'required',
        'date_of_joining' => 'date',
		'email'           => "required|unique:users,email,".$request->id ?? '',
		'security_amount' => 'required|numeric',
		'experience'      => 'required|numeric',
		'qualification'   => 'required|max:255',
		'date_of_joining' => 'required',
		'phone'           => 'required',
		'phone_number_mother' => 'required',
		'phone_number_father' => 'required',
		'department_id'       => 'required',
		'roles'               => 'required',
		'address'             => 'required',
        ],
        [
            'company_id.required' => 'Company name is required',
        ]
            );
        $data = $request->input();
        $type = $data['reqType'];
        array_shift($data);
        unset($data['company']);
        unset($data['roles']);
        unset($data['reqType']);
        unset($data['team_lead']);
        unset($data['department_id']);
        (isset($image_name)) ? $data['profile_image'] = $image_name : '';
        if (isset($password)) $data['password'] = $password;
        if (!isset($password)) unset($data['password']);

        if($type == 0){
            $isCreated = User::create($data);
        }else{
            $isCreated = User::where('id', $request->id)->update($data);
            $isCreated = User::where('id', $request->id)->first();
        }
        $isCreated->roles()->sync($request->roles ?? []);
        $isCreated->teamLead()->sync($request->team_lead ?? []);
        $isCreated->departments()->sync($request->department_id ?? []);
        return response()->json(['success'=> 'success']);


    }

    public function employeeLoginToggle(Request $request)
    {
        $toggle = User::whereId($request->id)->first()->login_toggle;
        if($toggle == 1){
            $isToggled = User::whereId($request->id)->update(["login_toggle" => 0]);
            $isToggled = 0;
        }
        else if($toggle == 0){
            $isToggled=  User::whereId($request->id)->update(["login_toggle" => 1]);
        }
        return response()->json(['success'=> $isToggled]);

    }
    public function empPagination()
    {
            $users = User::all()->orderBy('id','DESC')
            ->paginate(5);
            return view('admin.employees.main', compact('users'));
    }
    public function employeesRemove(Request $request)
    {
        $isDeleted = User::whereId($request->id)->delete();
        if($isDeleted){
            return response()->json(['message'=>'Delete Sucessfully']);
        }
    }
    public function employeesDetail(Request $request)
    {
        $thisEmployee = User::whereId($request->id)->first();
        return view('admin.employees.includes.detail', compact('thisEmployee'));
    }
    public function employeeStoreMoreInfo(Request $request){

        $data = [];
        $arr = [];
        if($request->hasFile('profile_image')){
            $profile_image=$this->UploadImage($request->file('profile_image'),'images/employees');
            $data['profile_image'] = $profile_image;
            array_push($arr, 'profile_image');
        }
        if($request->hasFile('aadhar_card_front_image')){
            $aadhar_card_front_image=$this->UploadImage($request->file('aadhar_card_front_image'),'images/employees');
            $data['aadhar_card_front_image'] = $aadhar_card_front_image;
            array_push($arr, 'aadhar_card_front_image');
        }
        if($request->hasFile('aadhar_card_back_image')){
            $aadhar_card_back_image=$this->UploadImage($request->file('aadhar_card_back_image'),'images/employees');
            $data['aadhar_card_back_image'] = $aadhar_card_back_image;
            array_push($arr, 'aadhar_card_back_image');

        }
        if($request->hasFile('driver_license_image')){
            $driver_license_image=$this->UploadImage($request->file('driver_license_image'),'images/employees');
            $data['driver_license_image'] = $driver_license_image;
            array_push($arr, 'driver_license_image');

        }
        if($request->hasFile('pan_card_image')){
            $pan_card_image=$this->UploadImage($request->file('pan_card_image'),'images/employees');
            $data['pan_card_image'] = $pan_card_image;
            array_push($arr, 'pan_card_image');
        }
        if($request->hasFile('voter_id_image')){
            $voter_id_image=$this->UploadImage($request->file('voter_id_image'),'images/employees');
            $data['voter_id_image'] = $voter_id_image;
            array_push($arr, 'voter_id_image');
        }
        if($request->hasFile('passport_image')){
            $passport_image=$this->UploadImage($request->file('passport_image'),'images/employees');
            $data['passport_image'] = $passport_image;
            array_push($arr, 'passport_image');
        }
        if($request->hasFile('passport_back_image')){
            $passport_back_image=$this->UploadImage($request->file('passport_back_image'),'images/employees');
            $data['passport_back_image'] = $passport_back_image;
            array_push($arr, 'passport_back_image');
        }
        $thisUser = User::whereId($request->id)->first($arr)->toarray();
        foreach ($thisUser as $key => $value) {
            if($value != '' OR $value != null){
                $employeesImage = deleteCompanyLogo('images/employees/'.$value);
            }
        }
        $updateEmployee=User::where('id',$request->id)->update($data);
        return response()->json(['data' => $updateEmployee, 'message' => 'Updated successfully.'], 200);
    }
}
