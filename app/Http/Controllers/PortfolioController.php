<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Traits\ImageUpload;
use App\Models\Company;
use App\Models\Country;
use App\Models\Department;
use App\Models\Portfolio;
use App\Models\ProjectType;
use App\Models\Tags;
use App\Models\TeamLead;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class PortfolioController extends Controller
{
    use ImageUpload;
    public function viewPortfolioData(Request $request)
    {

        $portfolios = Portfolio::orderBy('id','desc')->paginate(10);
        return view('admin.portfolio.main', compact('portfolios'));
    }
    public function paginationData(Request $request)
    {
        $portfolios = Portfolio::orderBy('id','desc')->paginate(10);
        return view('admin.portfolio.includes.view', compact('portfolios'))->render();
    }
    public function filter(Request $r)
    {
        $qry = Portfolio::select('portfolios.*')
        ->LeftJoin('portfolio_tags', 'portfolios.id', 'portfolio_tags.portfolio_id')
        ->LeftJoin('tags', 'portfolio_tags.tag_id', 'tags.id');



        if(!empty($r->start_date) && !empty($r->end_date)){
            $start = date('Y-m-d H:i:s', strtotime($r->start_date));
            $end = date('Y-m-d H:i:s', strtotime($r->end_date . ' +1 day'));
            $qry->whereBetween('portfolios.created_at',[$start,$end]);
        }
        if(!empty($r->search)){
            $search = $r->search;
            $qry->where(function ($q) use ($search) {
                $q->orWhere('portfolios.project_name','like',"%$search%");
                $q->orWhere('tags.name','like',"%$search%");
                $q->orWhere('portfolios.web_url','like',"%$search%");
                $q->orWhere('portfolios.ios_url','like',"%$search%");
                $q->orWhere('portfolios.android_url','like',"%$search%");
                $q->orWhere('portfolios.admin_url','like',"%$search%");
                $q->orWhere('portfolios.description','like',"%$search%");
                $q->orWhere('portfolios.business_model','like',"%$search%");
            });
        }
        $portfolios = $qry->distinct('portfolios.id')->orderBy('id','desc')->paginate(10);
        return view('admin.portfolio.includes.view', compact('portfolios'))->render();
    }


    public function portfolioCreatePage()
    {
        $users = User::all();
        $roles = Role::all();
        $companies = Company::all();
        $countries = Country::all();
        $departments = ProjectType::all();
        $tags = Tags::all('name');
        $default_value=null;
        $array_value=$this->getValue($tags);
        return view('admin.portfolio.create', compact('users','default_value', 'countries', 'roles', 'companies', 'departments', 'tags','array_value'));
    }
    public function portfolioEditPage(Request $request)
    {
        $users = User::all();
        $thisPortfolio = Portfolio::whereId($request->id)->first();
        $roles = Role::all();
        $companies = Company::all();
        $countries = Country::all();
        $departments = Department::all();
        $tags = Tags::all('name');
        $array_value=$this->getValue($tags);
        $default_value=$thisPortfolio->tags;
        return view('admin.portfolio.includes.edit', compact('default_value', 'users','thisPortfolio','countries', 'roles', 'companies', 'departments', 'tags','array_value'))->render();
    }
    public function portfolioStore(Request $request)
    {
        $tags = $request->tags;
        $tags = strtolower($tags);
        $tags = str_replace(" ","_",$tags);
        $tags = explode(',', $tags);
        $this->validate($request,
            [
            'project_name' => 'required',
            ]
        );
        $data = $request->input();
        $type = $data['reqType'];
        array_shift($data);
        unset($data['reqType']);
        unset($data['tags']);
        if($type == 0){
            $tag_ids = [];
            $data["created_by"] = Auth::user()->id;
            $isCreated = Portfolio::create($data);
            foreach ($tags as $key => $value) {
                Tags::firstOrCreate(['name' => $value]);
                $tagId = Tags::where('name', $value)->first('id')->id;
                array_push($tag_ids, $tagId);
            }
            $isCreated->tags()->sync($tag_ids);
        }else{
            $tag_ids = [];
            $data["updated_by"] = Auth::user()->id;
            $isCreated = Portfolio::where('id', $request->id)->update($data);
            $isCreated = Portfolio::where('id', $request->id)->first();
            foreach ($tags as $key => $value) {
                Tags::firstOrCreate(['name' => $value]);
                $tagId = Tags::where('name', $value)->first('id')->id;
                array_push($tag_ids, $tagId);
            }
            $isCreated->tags()->sync($tag_ids);
        }
        return response()->json(['success'=> 'success']);


    }

    public function portfolioLoginToggle(Request $request)
    {
        $toggle = User::whereId($request->id)->first()->login_toggle;
        if($toggle == 1){
            $isToggled = User::whereId($request->id)->update(["login_toggle" => 0]);
            $isToggled = 0;
        }
        else if($toggle == 0){
            $isToggled=  User::whereId($request->id)->update(["login_toggle" => 1]);
        }
        return response()->json(['success'=> $isToggled]);

    }
    public function portfolioPagination()
    {
            $users = User::all()->orderBy('id','DESC')
            ->paginate(5);
            return view('admin.employees.main', compact('users'));
    }
    public function removeEmployee(Request $request)
    {
        dd($request->id);
    }
    public function viewDetail(Request $request)
    {
        $thisPortfolio = Portfolio::with(['createdByUser:id,name', 'updatedByUser:id,name'])->whereId($request->id)->first();
        return view('admin.portfolio.includes.detail', compact('thisPortfolio'));
    }
    public function portfolioRemove(Request $request)
    {
        $isDeleted = Portfolio::whereId($request->id)->delete();
        if($isDeleted){
            return response()->json(['message'=>'Delete Sucessfully']);
        }
    }

    public function getValue($array){
        $value_array=[];
        foreach($array as $key=>$value){
              array_push($value_array,$value->name);
        }
        return $value_array;
    }
}
