<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\MasterPswd;
use App\Models\User;

class MasterPswdController extends Controller
{
    public function index(){
        return view('admin.masterPswds.main');
    }

    public function filter(Request $r)
    {
        $users = User::get(['id', 'name']);
        // if ($r->ajax()) {
            $qry = MasterPswd::where('status', '!=', 0);
            if(!empty($r->search)){
               $search = $r->search;
                $qry->where(function ($q) use ($search) {
                    $q->orWhere('master_pswds.id','like',"%$search%");
                    $q->orWhere('master_pswds.title','like',"%$search%");
                });
            }
                // $qry->where('created_by', Auth::user()->id);

            $masterPswds = $qry->paginate(15);
            // dd($masterPswds);
            return view('admin.masterPswds.includes.view', compact('masterPswds'))->render();

        // }

    }

    public function masterPswdsStore(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $data = $request->input();
        $data['created_by'] = Auth::user()->id;
        if ($request->filled('password')) {
       $data['password']= Hash::make($data['password']);
        }
        else
        {
            $data['password']=MasterPswd::where('id',$request->id)->first('password')->password;
        }
        array_shift($data);
        unset($data["id"]);
        unset($data["reqType"]);
        if($request->reqType == 0){
            $isCreated = MasterPswd::create($data);
        }else{
            $isCreated = MasterPswd::whereId($request->id)->update($data);
        }
        if($isCreated){
            return response()->json(['message'=>'success'],200);
        }else{
            return response()->json(['message'=>"error"], 400);
        }


    }

    public function masterPswdsRemove(Request $request)
    {
        $isDeleted = MasterPswd::whereId($request->id)->delete();
        // $isDeleted->status = 0;
        // $isDeleted->save();
        if($isDeleted){

            return response()->json(['message'=>'success'],200);
        }else{
            return response()->json(['message'=>'failed'],500);
        }
    }

    public function masterPswdsEditPage(Request $request)
    {   $masterPswds = MasterPswd::whereId($request->id)->first();
        $users = User::get(['id', 'name']);

        return view('admin.masterPswds.update', compact('masterPswds', 'users'));
    }

    public function masterPswdToggle(Request $request){

        // MasterPswd::where('id','!=',$request->id)->update([
        //     'status'=> 2
        // ]);

        $data=MasterPswd::where('id',$request->id)->first();
        if($data->status == 2){
            $data->status = 1;
            $data->save();
        }
        return response()->json(['message'=>'success'],200);
    }

    }

