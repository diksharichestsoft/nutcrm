<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Requests\dynamicFormValidation;
use Illuminate\Support\Facades\Crypt;
use App\Models\DynamicForm as DF;

use DB;
class DynamicForm extends Controller
{
    public function dynamicForms(){
        $companies=Company::get(['id','name']);
        return view('admin.dynamicForms.formscreation',compact('companies'));
    }
    public function addForm(dynamicFormValidation $request){
           DB::beginTransaction();
           $FormCreated=new DF;
           $FormCreated->company_id=$request->company;
           $FormCreated->form_type=$request->form_type;
           $FormCreated->domain=$request->domain;
           $FormCreated->redirected_to_success=$request->redirected_to_success;
           if($FormCreated->save()){
            $form_type=$request->form_type;
            $id=Crypt::encryptString($FormCreated->id);
            $access=Crypt::encryptString($request->domain);
            $company=$request->company;
            $redirected_to_success=Crypt::encryptString($request->redirected_to_success);
            $script=DF::find($FormCreated->id);
            $script_code=view('admin.dynamicForms.include.script',compact('form_type','id','access','company','redirected_to_success'))->render();
            $script->script=$script_code;
            if($script->save()){
              DB::commit();
              return response()->json($script_code);
            }
           }
          }
    // public function nutCrmView(request $request){
    //   $formType=$request->formtype;
    //   return view('admin.dynamicForms.include.forms',compact('formType'));
    // }

    public function Form(request $request){
         $accessBy=parse_url($request->server('HTTP_REFERER'));
         $access=Crypt::decryptString($request->access);
         $accessTo = parse_url($access);
         if($accessTo['host'] ==  $accessBy['host']){
          $formType=$request->formtype;
          $company=$request->company;
          $id=Crypt::decryptString($request->form);
          $access=Crypt::decryptString($request->access);
        
               $check=DF::where('id',$id)->where('form_type', $formType)->where('domain',$access)->first();
               if($check){
                   $redirection="hello";
                 return view('admin.dynamicForms.include.forms',compact('formType','company','redirection'));
                }
         }
          return view('admin.dynamicForms.include.notvalidateform');
    }
    // public function formView(request $request){
    //   $formType=$request->formtype;
    //   $id=$request->form;
    //   $access=$request->access;
    //   $curl = curl_init();
    //   curl_setopt_array($curl, array(
    //   CURLOPT_URL => 'https://sagarwatertanks.com/public/api/check-is-valid?id='.$id.'&form_type='.$formType.'&access='.$access.'&accessBy='.$request->accessBy,
    //   CURLOPT_RETURNTRANSFER => true,
    //   CURLOPT_ENCODING => '',
    //   CURLOPT_MAXREDIRS => 10,
    //   CURLOPT_TIMEOUT => 0,
    //   CURLOPT_FOLLOWLOCATION => true,
    //   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    //   CURLOPT_CUSTOMREQUEST => 'GET',
    //   ));
    //   $response = curl_exec($curl);
    //   curl_close($curl);
    //   if(!json_decode($response)->success){
    //     return view('admin.dynamicForms.include.notvalidateform');
    //   }
    //   return view('admin.dynamicForms.include.forms',compact('formType'));
    // }

    public function index(Request $request)
    {
        return view('admin.dynamicForms.main');
    }
    public function filter(Request $r)
    {
       $qry = DF::orderBy('id','desc');
        if(!empty($r->start_date) && !empty($r->end_date)){
            $start = date('Y-m-d H:i:s', strtotime($r->start_date));
            $end = date('Y-m-d H:i:s', strtotime($r->end_date . ' +1 day'));
            $qry->whereBetween('dynamic_forms.created_at',[$start,$end]);
        }
        if(!empty($r->search)){
            $search = $r->search;
            $qry->where(function ($q) use ($search) {
                $q->orWhere('dynamic_forms.id','like',"%$search%");
                $q->orWhere('dynamic_forms.domain','like',"%$search%");
                $q->orWhere('dynamic_forms.company_id','like',"%$search%");
            });
        }
       $dynamicforms = $qry->paginate();
        return view('admin.dynamicForms.include.view', compact('dynamicforms'))->render();
    }
    public function viewDetail(Request $request)
    {
        $thisDetails = DF::whereId($request->id)->first();
        return view('admin.dynamicForms.include.detail', compact('thisDetails'));
    }

}
