<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\TaskManagementStatus;
use App\Models\TaskManagment;
use App\Models\TaskStatus;
use App\Models\TaskType;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class TaskManagmentController extends Controller
{
    public function index(Request $request)
    {
        $taskStatus = TaskManagementStatus::get();
        return view('admin.taskManagement.main', compact('taskStatus'));
    }
    public function filter(Request $r)
    {
        $active = $r->active;
        $qry = TaskManagment::with(['project:id,title'])->select('task_managments.*', 'users.name as send_user_name', 'projects.title as project_title')->LeftJoin('users', 'users.id', 'task_managments.send_to')->LeftJoin('projects', 'projects.id', 'task_managments.project_id');

        // TaskManagment::with(['latestTimer','project:id,title','assignedBy:id,name'])->select('task_managments.*', 'users.name as send_user_name', 'projects.title as project_title')
        // ->LeftJoin('users', 'users.id', 'task_managments.send_to')
        // ->LeftJoin('projects', 'projects.id', 'task_managments.project_id');


        // if(!empty($r->active) && $r->active!=-1 && $r->active != -2)
        //     $qry->where('task_managments.task_status_id',$r->active);
        if(!empty($r->active)){
            if($r->active == 1){
                $qry->where('task_managments.task_status_id', 4)->where('task_managments.active', 1)->where('task_managments.child_status_id', 1)->where('send_to', Auth::user()->id);
            }else if($r->active == 2){
                $qry->where('task_managments.task_status_id', 4)->where('task_managments.active', 2)->where('task_managments.child_status_id', 2)->where('send_to', Auth::user()->id);
            }else if($r->active == 3){
                $qry->where('task_managments.task_status_id', 5)->where('task_managments.child_status_id', 3)->where('send_to', Auth::user()->id);
            }else if($r->active == 4){
                $qry->where('task_managments.task_status_id', 4)->where('send_by', Auth::user()->id);
            }else if($r->active == 5){
                $qry->where('task_managments.task_status_id', 5)->where('task_managments.child_status_id', 3)->where('send_by', Auth::user()->id);
            }else if($r->active == 6){
                $qry->where('task_managments.task_status_id', 4)->where('task_managments.child_status_id', 1)->where('send_to', Auth::user()->id);
            }
        }
        if(!empty($r->start_date) && !empty($r->end_date)){
            $start = date('Y-m-d H:i:s', strtotime($r->start_date));
            $end = date('Y-m-d H:i:s', strtotime($r->end_date . ' +1 day'));
            $qry->whereBetween('task_managments.created_at',[$start,$end]);
        }
        $qry->where('task_managments.active', '!=', 0);
        if(!empty($r->search)){
            $search = $r->search;
            $qry->where(function ($q) use ($search) {
                $q->orWhere('task_managments.title','like',"%$search%");
                $q->orWhere('task_managments.summary','like',"%$search%");
                $q->orWhere('users.name','like',"%$search%");
                $q->orWhere('projects.title','like',"%$search%");
                $q->orWhere('task_managments.description','like',"%$search%");
            });
        }
        $data = $qry->orderBy('id','DESC')->paginate(10);
        $hotdeals_status_ID=$r->active;
        if($r->project_id == null){
            $taskStatus = TaskManagementStatus::where('id', '!=', 1)->get();
        }else{
            $taskStatus=TaskManagementStatus::all();
        }
        return view('admin.taskManagement.includes.view', compact('data','taskStatus','hotdeals_status_ID','active'))->render();
    }
    public function add(Request $request)
    {
            $employees = User::where('login_toggle', 1)->get(['id', 'name']);
            $projects = Project::where('deal_status', 2)->get(['id', 'title']);
            return  view('admin.taskManagement.includes.addform',compact('employees', 'projects'))->render();
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'priority_level' => 'required'
        ]);
        if($request->hasFile('file')){
            $image_name=$this->UploadImage($request->file('file'),'taskUploadFiles');
        }
        $time = $request->time_hours.":".$request->time_min;
        $data = [
            "title" => $request->title,
            "project_id" => $request->project_id ?? null,
            "send_to" => $request->send_to,
            "time" => $time,
            "description" => $request->description ?? '',
            "summary" => $request->summary ?? '',
            "priority_level" => $request->priority_level,
            "task_type_id" => $request->task_type_id,
            "task_status_id" => 4,
            "start_date" => $request->start_date,
            "end_date" => $request->end_date,
            "priority_points" => $request->priority_points,
            "file" => $image_name  ?? '',
            "send_by" => Auth::user()->id,
        ];
        if($request->id != null){
            $thisTask = TaskManagment::whereId($request->id)->first();
            $isCreated = $thisTask->update($data);
        }else{
            $isCreated = TaskManagment::create($data);
        }
        if($isCreated){
            $message = "Sent you a quick task, https://nutcrm.com/admin/manageTasks/detail/".$isCreated->id;
            notifyUserByUserId(Auth::user()->name, $request->send_to, $message);
            return response()->json(['data'=>$isCreated,'message'=>'Success']);
        }

    }

    public function accept(Request $request)
    {
        $thisTask = TaskManagment::whereId($request->id)->first();
        if($request->type == 1){
            // Active 2 = accepted
            $isAccepted = $thisTask->update(['child_status_id'=> 2, 'active' => 2]);
            $message = "Accepted your Quick Task, https://nutcrm.com/admin/manageTasks/detail/".$thisTask->id;
            notifyUserByUserId(Auth::user()->name, $thisTask->send_by, $message);
        }else{
            // Active 3 = Rejected
            $isAccepted = $thisTask->update(['active' => 3]);
            $message = "Rejected your Quick Task, https://nutcrm.com/admin/manageTasks/detail/".$thisTask->id;
            notifyUserByUserId(Auth::user()->name, $thisTask->send_by, $message);

        }
        if($isAccepted){
            return response()->json(['data'=>$isAccepted,'message'=>'Success'], 200);
        }
    }
    public function changeStatus(Request $request)
    {
        $thisTask = TaskManagment::whereId($request->deal_id)->first();
        $isChanged = false;
        if($request->status == 3){
            $isChanged = $thisTask->update(["task_status_id"=> 5, "child_status_id" => 3]);
        }
        if($isChanged){
            return response()->json(['data'=>$isChanged,'message'=>'Success'], 200);
        }
    }

    public function updateData(Request $request)
    {
        $employees = User::where('login_toggle', 1)->get(['id', 'name']);
        $projects = Project::where('deal_status', 2)->get(['id', 'title']);
        $thisTask=TaskManagment::where('id',$request->id)->first();
        return  view('admin.taskManagement.includes.update',compact('thisTask', 'employees', 'projects'))->render();
    }

    public function viewDetails(Request $request)
    {   $quickTaskStatus = DB::table('task_management_statuses')->get();
        $thisTask = TaskManagment::whereId($request->id)->first();
        $currentStatus = TaskStatus::whereId($thisTask->task_status_id)->first('title');
        return view('admin.taskManagement.detail', compact('quickTaskStatus','thisTask', 'currentStatus'));
    }
    public function resendTask(Request $request)
    {
        $isResend = TaskManagment::where('id',$request->id)->update(['active'=>1]);
        if($isResend){
            return response()->json(['message'=>'Delete Sucessfully']);
        }else{
            return response()->json(['message' => "Failed To delete"], 500);
        }
    }

        // Remove the task
        public function removeTask(Request $request)
        {
            $isRemoved = TaskManagment::where('id',$request->id)->update(['active'=>0]);

            if($isRemoved){
                return response()->json(['message'=>'Delete Sucessfully', 'status'=>1]);
            }else{
                return response()->json(['message' => "Failed To delete", 'status'=>2], 500);
            }
        }


}
