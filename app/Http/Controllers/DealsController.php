<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use App\Models\Deals;
use App\Models\DealStatus;
use App\Models\User;
use App\Models\Company;
use App\Models\ProjectType;
use App\Models\DealPlatform;
use App\Models\FeedModel;
use App\Models\DealComment;

use Illuminate\Http\Request;
use App\Http\Requests\CompanyDeal;
use App\Http\Requests\UpdateDeal;
use App\Http\Requests\DealCommentRequest;
use App\Models\Project;
use Illuminate\Support\Facades\Auth;
use App\Models\DealLostReason;
use App\Models\ProjectFeed;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Log;
class DealsController extends Controller
{

    public function companydeal()
    {
        // $Dealstatus=DealStatus::get();
        $array =[1,2,10,9,11,12,13,14,15,16,17,18,19,3,20,4,5,21,22,23,24,25,26,6,7,8];
        $Dealstatus=DealStatus::all();
       $Dealstatus =$Dealstatus->sortBy(function($model) use ($array) {
            return array_search($model->getKey(), $array);
        });
        $dealLostReasons = DealLostReason::all();
        $status_ID=1;
        $thisUser = Auth::user();
        $qry = DB::table('deals')->where('deals.status',1)
        ->select('deal_status', DB::raw('count(*) as total'))
        ->groupBy('deal_status');


        //Permissions Check ********
        if($thisUser->can('deals_all_leads_of_any_user')){
            if($thisUser->can('deals_digital_marketing') && $thisUser->can('deals_development')) {
                $qry = $qry;
            }
            else{
                if($thisUser->can('deals_digital_marketing')){
                    $qry = $qry->where('deals.project_type', 2);
                }
                if($thisUser->can('deals_development')){
                    $qry = $qry->where('deals.project_type','!=',2);
                }
            }
        }
        else{
            if($thisUser->can('deals_digital_marketing') &&  $thisUser->can('deals_development')){
                if($thisUser->can('deals_all_leads_of_any_user_inqueue')){
                    // Nothing
                }else{
                    $qry = $qry->where('deals.assign_to', $thisUser->id);
                }
            }else{
                if($thisUser->can('deals_digital_marketing')){
                    $qry = $qry->where('deals.project_type', 2);
                    if($thisUser->can('deals_all_leads_of_any_user_inqueue')){
                        // Nothing
                    }else{
                        $qry = $qry->where('deals.assign_to', $thisUser->id);
                    }
                }
                else{
                        if($thisUser->can('deals_development')){
                        $qry = $qry->where('deals.project_type','!=',2);
                        if($thisUser->can('deals_all_leads_of_any_user_inqueue')){
                            // Nothing
                        }else{
                            $qry = $qry->where('deals.assign_to', $thisUser->id);
                        }
                  }
                }
            }
        }

        if(Auth::user()->can('only_appsminder_deals')){
            $qry->where('deals.company_id', 4);
        }
        $dealsCount = $qry->pluck('total','deal_status')->all();
        //End Permissions Check

        return view('admin.companyDeal.main',compact('Dealstatus','status_ID','dealLostReasons','dealsCount'));
    }
    public function companyDealPage(Request $request)
    {
            $platform = DealPlatform::get();
            $project_type = ProjectType::get();
            $users=User::get();
            $companies = Company::get();
            return  view('admin.companyDeal.includes.addform',compact('platform','project_type','companies','users'))->render();
    }
    public function dealComment(DealCommentRequest $request)
    {
        // dd($request->input());
        $data =  FeedModel::create([
        'comment'=>$request->comment,
        'ip_address'=>request()->ip(),
        'model_type'=>"App/Deals",
        'model_id'=>$request->deal_id,
        'type'=>1,
        'created_by'=>auth()->user()->id,
        ]);
        return response()->json(['data'=>$data,'message'=>'Comment Added Sucessfully']);
    }
    public function addcompanyDeal(CompanyDeal $request)
    {
        $data = [
            'title'           =>  $request->deal_title??'',
            'client_name'     =>  $request->client_name??'',
            'project_type'    =>  $request->department??'',
            'platform'        =>  $request->platform??'',
            'platform_id'     =>  $request->platform_id??'',
            'job_descriprion' =>  $request->job??'',
            'url'             =>  $request->url??'',
            'client_email'    =>  $request->client_email??'',
            'client_phone'    =>  $request->client_phone??'',
            'budget'          =>  $request->price??'',
            'company_id'      =>  $request->company??'',
        ];
        if(isset($request->id)){
            $data['updated_by']=  auth()->user()->id;
            $deal_exist = Deals::find($request->id);
            if($deal_exist){
                $deal=$deal_exist->update($data);
                $msg ="update";
                $data =  FeedModel::create([
                    'title'=>' has Updated a deal '.$deal_exist->title,
                    'comment'=>$request->deal_title??'',
                    'ip_address'=>request()->ip(),
                    'model_type'=>"App/Deals",
                    'model_id'=>$deal_exist->id,
                    'type'=>0,
                    'created_by'=>auth()->user()->id,
                    ]);
            }
        }else{
            $data['created_by']=  auth()->user()->id;
            $data['assign_to']=  auth()->user()->id;
            $deal=Deals::create($data);
            $data =  FeedModel::create([
                'title'=>' has created a deal '.$deal->title,
                'comment'=>$request->deal_title??'',
                'ip_address'=>request()->ip(),
                'model_type'=>"App/Deals",
                'model_id'=>$deal->id,
                'type'=>0,
                'created_by'=>auth()->user()->id,
                ]);
            $msg ="create";

        }
        if($deal)
        {
            return response()->json(['data'=>$deal,'message'=>$msg]);
        }
    }
    public function fetchCompanyDeal(Request $r)
    {
        $toggle = $r->active_toggle;
        $active = $r->active;
        $qry = Deals::with(['platformData', 'companyData', 'projectTypeData'])
        ->select(
                'deals.*',
                'dealstatus.dealname as deal_status_title',
                'project_type.name as project_name',
                'deal_platform.name as platform_name',
                'created_by_users.name as created_by_user_name',
                'updated_by_users.name as updated_by_user_name',
                'assigned_to_users.name as assigned_to_user_name',
                )
        ->join('project_type','deals.project_type','project_type.id')
        ->leftjoin('users as created_by_users','deals.created_by','created_by_users.id')
        ->leftjoin('users as updated_by_users','deals.updated_by','updated_by_users.id')
        ->leftjoin('users as assigned_to_users','deals.assign_to','assigned_to_users.id')
        ->join('dealstatus', 'deals.deal_status', 'dealstatus.id')
        ->join('deal_platform','deals.platform','deal_platform.id');

        if(!empty($r->active) && $r->active!=-1 )
            $qry->where('deals.deal_status',$r->active);
        if(!empty($r->start_date) && !empty($r->end_date)){
            $start = date('Y-m-d H:i:s', strtotime($r->start_date));
            $end = date('Y-m-d H:i:s', strtotime($r->end_date . ' +1 day'));
            $qry->whereBetween('deals.created_at',[$start,$end]);
        }
        $qry->where('deals.status',1);
        // If status is meeting show only which they putted in meeting
        if($r->active == 3){
            $isBussinessEmployee = (Auth::user()->departments()->where('departments.id',8)->count() > 0);
            if($isBussinessEmployee){
                // dd($qry->where('deals.id',522)->get());
                // dd(Auth::user()->id);
                $qry->where('status_meeting_by_user_id', Auth::user()->id)->orWhere('deals.assign_to', Auth::user()->id);
            }
        }

        $thisUser = Auth::user();

        if($thisUser->can('deals_all_leads_of_any_user')){
            if($thisUser->can('deals_digital_marketing') && $thisUser->can('deals_development')) {
                $qry = $qry;
            }
            else{
                if($thisUser->can('deals_digital_marketing')){
                    $qry = $qry->where('deals.project_type', 2);
                }
                if($thisUser->can('deals_development')){
                    $qry = $qry->where('deals.project_type','!=',2);
                }
            }
        }
        else{
            if($thisUser->can('deals_digital_marketing') &&  $thisUser->can('deals_development')){
                if($thisUser->can('deals_all_leads_of_any_user_inqueue') && $r->active == 1){
                    // Nothing
                }else{
                    $qry = $qry->where('deals.assign_to', $thisUser->id);
                }
            }else{
                if($thisUser->can('deals_digital_marketing')){
                    $qry = $qry->where('deals.project_type', 2);
                    if($thisUser->can('deals_all_leads_of_any_user_inqueue') && $r->active == 1){
                        // Nothing
                    }else{
                        $qry = $qry->where('deals.assign_to', $thisUser->id);
                    }
                }
                else{

                        if($thisUser->can('deals_development')){
                        $qry = $qry->where('deals.project_type','!=',2);
                        if($thisUser->can('deals_all_leads_of_any_user_inqueue') && $r->active == 1){
                            // Nothing
                        }else{
                            $qry = $qry->where('deals.assign_to', $thisUser->id);
                        }

                  }
                }
            }
        }
        if(!empty($r->search)){
            $search = $r->search;

            $qry->where(function ($q) use ($search) {
                $q->orWhere('deals.id','like',"%$search%");
                $q->orWhere('deals.title','like',"%$search%");
                $q->orWhere('deals.client_name','like',"%$search%");
                $q->orWhere('project_type.name','like',"%$search%");
                $q->orWhere('created_by_users.name','like',"%$search%");
                $q->orWhere('updated_by_users.name','like',"%$search%");
                $q->orWhere('deal_platform.name','like',"%$search%");
                $q->orWhere('deals.platform_id','like',"%$search%");
                $q->orWhere('deals.job_descriprion','like',"%$search%");
                $q->orWhere('deals.url','like',"%$search%");
                $q->orWhere('deals.client_email','like',"%$search%");
                $q->orWhere('deals.client_phone','like',"%$search%");
                $q->orWhere('deals.budget','like',"%$search%");
            });
        }
        if(Auth::user()->can('only_appsminder_deals')){
            $qry->where('deals.company_id', 4);
        }
        if($toggle == 1){
            $qry->where('deals.assign_to', Auth::user()->id);
        }
        if($r->active != -1){
            $qry = $qry->where('deals.deal_status', $r->active);
        }
        $data = $qry->orderBy('updated_at','DESC')->paginate(20);
        $hotdeals_status_ID=$r->active;
        $array =[1,2,10,9,11,12,13,14,15,16,17,18,19,3,20,4,5,21,22,23,24,25,26,6,7,8];
        $Dealstatus=DealStatus::all();
       $Dealstatus =$Dealstatus->sortBy(function($model) use ($array) {
            return array_search($model->getKey(), $array);
        });
        // dd($Dealstatus);
        return view('admin.companyDeal.includes.view', compact('data','Dealstatus','hotdeals_status_ID','active'))->render();
    }
    public function accept(Request $request)
    {
        $dealstatus=2;
        $update=Deals::where('id',$request->id)->update(['deal_status'=>$dealstatus, 'accepted_by'=>Auth::user()->id, 'assign_to'=> Auth::user()->id]);
        $deal=Deals::where('id',$request->id)->first();
        if($update)
        {
            $data =  FeedModel::create([
                'title'=>' has accept the deal '.$deal->title,
                'comment'=>'Deal Accpted',
                'ip_address'=>request()->ip(),
                'model_type'=>"App/Deals",
                'model_id'=>$request->id,
                'type'=>0,
                'created_by'=>auth()->user()->id,
                ]);
            return response()->json(['message'=>'success']);
        }
        else{
            return response()->json(['message'=>'failed']);
        }
    }
    public function changeStatus(Request $request)
    {
	    Log::info('change status, data we got: ', $request->all() );
        $deal = Deals::where('id',$request->deal_id)->first();
	    if($deal){
            if($request->status == 3){
                $this->validate($request, [
                    'meeting_time' => 'required'
                ]);
                Deals::where('id',$request->deal_id)->update(['deal_status'=>$request->status, 'status_meeting_by_user_id' => Auth::user()->id, 'meeting_time'=> $request->meeting_time]);
            }
            $status =(int)$request->status;
            //dd($status);
            if(in_array($status, [9,10,11, 12,13,14,15,16,17,18,19])){
                $this->validate($request, [
                    'contact_email' => 'required',
                    'contact_phone' => 'required',
                ]);
                Deals::where('id',$request->deal_id)->update(['contact_email'=>$request->contact_email,'contact_phone'=>$request->contact_phone,'contact_description'=>$request->contact_description,'deal_status'=>$request->status]);
                }else{
                Deals::where('id',$request->deal_id)->update(['deal_status'=>$request->status]);
            }
            $status = DealStatus::find($request->status);

            if($request->status == 3){
                $feed_title = ' has changed the deal status to '.$status->dealname.', And Scheduled time for '.date('d-m-Y h:i a',strtotime($request->meeting_time));
            }else{
                $feed_title = ' has changed the deal status to '.$status->dealname;
            }

            if ( $request->deal_lost_reason_id ){
                $deal_lost_reason = DealLostReason::find( $request->deal_lost_reason_id );
                $feed_title .= ", Reason: $deal_lost_reason->title";
            }

            // Updateing the Deal status
            $data =  FeedModel::create([
                'title'=> $feed_title,
                'comment'=>$request->comment??'',
                'ip_address'=>request()->ip(),
                'model_type'=>"App/Deals",
                'model_id'=>$request->deal_id,
                'type'=>0,
                'deal_lost_reason_id' => $request->deal_lost_reason_id ?? null,
                'created_by'=>auth()->user()->id,
                ]);

            // Creating a project if status change in won(6)
            if($request->status == 6){
                $deal = Deals::where('id', $request->deal_id )->first();
                $data = [
                    'title'           =>  $deal->title??'',
                    'client_name'     =>  $deal->client_name??'',
                    'project_type'    =>  $deal->project_type??'',
                    'platform'        =>  $deal->platform??'',
                    'platform_id'     =>  $deal->platform_id??'',
                    'job_descriprion' =>  $deal->job_descriprion??'',
                    'url'             =>  $deal->url??'',
                    'client_email'    =>  $deal->client_email??'',
                    'client_phone'    =>  $deal->client_phone??'',
                    'budget'          =>  $deal->budget??'',
                    'company_id'      =>  $deal->company_id??'',
                    'created_by'      =>  auth()->user()->id,
                    'deal_id'      =>  $deal->id ?? $request->deal_id,
                ];
                $isCreated=Project::create($data);
                if($isCreated){
                    $data =  ProjectFeed::create([
                        'title'=>' has won a deal which turned to project titled '.$deal->title,
                        'comment'=>$request->deal_title??'',
                        'ip_address'=>request()->ip(),
                        'model_type'=>"App/Project",
                        // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
                        'branch_id'=> 1,
                        'model_id'=>$isCreated->id ,
                        'type'=>0,
                        'created_by'=>auth()->user()->id,
                        ]);
                    $departmentArr =[$deal->project_type];
                    $departmentArrWithType = [];
                    foreach ($departmentArr as $eachDepartment) {

                        //collect all inserted department IDs
                        $departmentArrWithType[$eachDepartment] = ['type' => '0'];
                    }
                    //Insert into selected_departemnt table
                    $isCreated->projectTypes()->sync($departmentArrWithType);

                    // Notify users

                    $message =  "Won the deal, ".route('companyDeal-view',$request->deal_id);
                    notifyUserOnEvent('on_deal_won', $message);

                }
                $data =  FeedModel::create([
                    'title'=>' has Won a Project '.$deal->title,
                    'comment'=>$request->comment??'',
                    'ip_address'=>request()->ip(),
                    'model_type'=>"App/Projects",
                    'model_id'=>$deal->id,
                    'type'=>0,
                    'created_by'=>auth()->user()->id,
                    ]);
                $msg ="create";
            }
        }
        return  response()->json(['message'=>'Status Changed Sucessfully']);
    }

    public function viewData($id)
    {
        // Session::put('view_id', $id);
        $dealStatus = DB::table('dealstatus')->get();
        $getProjectData=Deals::select(
            'deals.*',
            'project_type.name as project_name',
            'deal_platform.name as platform_name',
            'created_by_users.name as created_by_user_name',
            'updated_by_users.name as updated_by_user_name',
            'assigned_to_users.name as assigned_to_user_name',
            )
        ->join('project_type','deals.project_type','project_type.id')
        ->leftjoin('users as created_by_users','deals.created_by','created_by_users.id')
        ->leftjoin('users as updated_by_users','deals.updated_by','updated_by_users.id')
        ->leftjoin('users as assigned_to_users','deals.assign_to','assigned_to_users.id')
        ->join('deal_platform','deals.platform','deal_platform.id')
        ->where('deals.id',$id)
        ->where('deals.status', '!=', '0')
        ->first();
        if(empty($getProjectData)){
            return abort(404);
        }
        $getcomment=FeedModel::select('feed.*','users.name')
        ->leftJoin('users','feed.created_by','users.id')
        ->where('model_id',$id)
        ->orderBy('id','DESC')
        ->paginate(10);
        $comments=DealComment::select('deal_comments.*','users.name')->leftJoin('users','deal_comments.created_by','users.id')->where('deal_id',$id)->paginate(5);
        return  view('admin.companyDeal.detail',compact('dealStatus','getProjectData','getcomment','comments'))->render();
    }

    public function updateData(Request $request)
    {
        $platform = DealPlatform::get();
        $project_type = ProjectType::get();
        $users=User::get();
        $companies = Company::get();
        $getProjectData=Deals::where('id',$request->id)->first();
        // dd($getProjectData);
        return  view('admin.companyDeal.includes.update',compact('getProjectData','companies','project_type','platform','users'))->render();
    }
    public function removeCompanyDealsData(Request $request)
    {
        Deals::where('id',$request->id)->update(['status'=>0]);
        return response()->json(['message'=>'Delete Sucessfully']);
    }

    public function getTimeline(Request $request)
    {

        $getProjectData=Deals::select('deals.*','project_type.name as project_name','users.name as user_name','deal_platform.name as platform_name')
        ->join('project_type','deals.project_type','project_type.id')
        ->join('users','deals.created_by','users.id')
        ->join('deal_platform','deals.platform','deal_platform.id')
        ->where('deals.id',$request->id)
        ->first();
        $getcomment=FeedModel::select('feed.*','users.name')
        ->leftJoin('users','feed.created_by','users.id')
        ->where('model_id',$request->id)
        ->orderBy('id','DESC')
        ->paginate(5);
        $comments=DealComment::select('deal_comments.*','users.name')->leftJoin('users','deal_comments.created_by','users.id')->where('deal_id',$request->id)->paginate(5);
        // echo '<pre>'.auth()->user()->id;print_r($comments);die();
        return  view('admin.companyDeal.includes.timeline',compact('getProjectData','getcomment','comments'))->render();

    }
    public function getEmployeeListForAssign(Request $request)
    {
        $users = User::where('login_toggle', 1)->get(['id', 'name']);
        // return response()->json(['reasons'=>$users]);
        return view('admin.companyDeal.includes.assigntoSelect', compact('users'));
    }

    public function assignDeal(Request $request)
    {
        $this->validate($request, [
            'deal_id' => 'required',
            'assign_to' => 'required'
        ]);
        $isUpdated = Deals::whereId($request->deal_id)->update(['assign_to' => $request->assign_to]);
        if($isUpdated){
            $assignedToUserName = User::whereId($request->assign_to)->first(['name']);
            $data =  FeedModel::create([
                'title'=>' has assigned this deal to '.$assignedToUserName->name,
                'comment'=>$request->comment ?? '',
                'ip_address'=>request()->ip(),
                'model_type'=>"App/Deals",
                'model_id'=>$request->deal_id,
                'type'=>0,
                'created_by'=>auth()->user()->id,
                ]);
            $message = "Has assigned you deal ".route('companyDeal-view',$request->deal_id);
            $isnotifited = notifyUserByUserId(Auth::user()->name, $request->assign_to, $message);
            if(!$isnotifited){
                return response()->json(['message'=>'Failed to assign'], 500);
            }
            return response()->json(['message'=>'Assigned Successfully']);
        }else{
            return response()->json(['message'=>'Failed to assign'], 500);
        }
    }

}
