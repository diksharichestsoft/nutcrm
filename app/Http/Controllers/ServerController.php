<?php

namespace App\Http\Controllers;
use \App\Models\ProjectDemoServerDetail;
use \App\Models\ProjectCredentials;
use Illuminate\Http\Request;
use App\Models\ProjectFeed;
use App\Models\Project;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;



class ServerController extends Controller
{
	public function index(){

	}

    public function renderProjectServersView( Request $req )
    {
	    $project_servers = ProjectDemoServerDetail::where('is_server_details',1)->get()->sortByDesc('id');
        $thisProject = Project::first(['id', 'title']);
	    $server_credential_types = getProjectCredentialTypes('server');

	   

	    return view('admin.servers.main', compact('project_servers', 'server_credential_types' ,'thisProject' ) );
    }

    public function storeProjectServer( Request $req )
    {


	    // 'formKey' is added to upload file with ajax in frontend it means nothing else

	    // saving pem file
	    if ( isset( $req->formKey['pem_keys'] ) ){
		    $file          = $req->formKey['pem_keys'];
		    $fileName      = $this->UploadImage( $file,'project_keys');
		    $pem_file_path = "/project_keys/$fileName";
	    }


	    $project_detail = new ProjectDemoServerDetail;

	    $project_detail->project_id  =1;
	    $project_detail->user_id     = $req->user()->id;
	    $project_detail->description = $req->input('servers_description');
	    $project_detail->is_server_details = 1;
	    $project_detail->keys = $pem_file_path ?? null;
	    $project_detail->save();


	    // parsing server credentials
	    $servers = $req->except( 'formKey' );
	    $servers_credentials_with_parent_id = [];
	    foreach ( $servers as $server ){
		    $server[ 'parent_id' ] = $project_detail->id;
            $server['password']=Crypt::encryptString($server['password']);
          array_push( $servers_credentials_with_parent_id, $server );
	    }


	    $project_credentials = ProjectCredentials::insert( $servers_credentials_with_parent_id );
        $data =  ProjectFeed::create([
		    'title'      => ' has added Server ',
		    'comment'    => '',
		    'ip_address' => request()->ip(),
		    'model_type' => "App/Project",
		    'branch_id'  => 1,             // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
		    'model_id'   => $project_detail->project_id,
		    'type'       => 0,
		    'created_by'=> auth()->user()->id,
	    ]);

	    return response()->json(['success' => true ], 200 );
    }

    public function deleteProjectServer( Request $req )
    {

	    $project_server = ProjectDemoServerDetail::find( $req->server_id );
	    $project_server->credentials()->delete();
	    $project_server->delete();

	    $data =  ProjectFeed::create([
		    'title'      => ' has deleted Server ',
		    'comment'    => '',
		    'ip_address' => request()->ip(),
		    'model_type' => "App/Project",
		    'branch_id'  => 1,             // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
		    'model_id'   => $project_server->id,
		    'type'       => 0,
		    'created_by'=> auth()->user()->id,
	    ]);

	    return response()->json( ['success' => true ], 200 );
    }

    public function renderEditProjectServerModal( Request $req  )
    {

	    $project_server = ProjectDemoServerDetail::find( $req->server_id );
	    $server_credential_types = getProjectCredentialTypes('server');

	    $project_id = $project_server->project_id;

	    return view('admin.projects.includes.index', compact('project_server', 'server_credential_types','project_id') );
    }

    public function updateProjectServer( Request $req )
    {
        	    // saving pem file
	    if ( isset( $req->formKey['pem_keys'] ) ){
		    $file          = $req->formKey['pem_keys'];
		    $fileName      = $this->UploadImage( $file,'project_keys');
		    $pem_file_path = "/project_keys/$fileName";
            // $old_file = $pem_file_path->keys;
            // $old_files =   deleteCompanyLogo('project_keys/'.$old_file);
	    }

	    $project_server = ProjectDemoServerDetail::find( $req->formKey["server_id"]);
	    if ( $req->formKey["description"]){
		    $project_server->description = $req->formKey["description"];
            if(isset($pem_file_path)) $project_server->keys = $pem_file_path;
		    $project_server->save();
	    }

	    $servers_credentials_with_parent_id = [];
	    // foreach ( $req->servers as $server ){

		//     $server[ 'parent_id' ] = $project_server->id;
		//     array_push( $servers_credentials_with_parent_id, $server );
	    // }

	    // parsing server credentials
	    $servers = $req->except( 'formKey' );
	    $servers_credentials_with_parent_id = [];
	    foreach ( $servers as $server ){
		    $server[ 'parent_id' ] = $project_server->id;
            $server['password']=Crypt::encryptString($server['password']);
		    array_push( $servers_credentials_with_parent_id, $server );
	    }

	    $project_server->credentials()->delete(); // deleting old
	    $project_credentials = ProjectCredentials::insert( $servers_credentials_with_parent_id );

	    $data =  ProjectFeed::create([
		    'title'      => ' has edited Server ',
		    'comment'    => '',
		    'ip_address' => request()->ip(),
		    'model_type' => "App/Project",
		    'branch_id'  => 1,             // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
		    'model_id'   => $project_server->project_id,
		    'type'       => 0,
		    'created_by'=> auth()->user()->id,
	    ]);

	    return response()->json( ['success' => true ], 200 );
    }


}
