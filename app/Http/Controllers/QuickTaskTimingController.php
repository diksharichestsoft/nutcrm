<?php

namespace App\Http\Controllers;

use App\Models\QuickTaskTiming;
use App\Models\TaskManagment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QuickTaskTimingController extends Controller
{
    public function startOrPause(Request $request)
    {
        // Getting the task on which timer is related
        $thisQuickTask = TaskManagment::with(['latestTimer'])->whereId($request->id)->first(['id','total_timer_time', 'project_id']);
        $haveTimerOnATask = Auth::user()->haveTimerOnQuickTask();
        if($haveTimerOnATask[0] AND $haveTimerOnATask[1] != $request->id ){
            return response()->json(['message'=>'Your Other Task Have a Timer Running', 'status'=>0]);
        }
        $latestTimer = QuickTaskTiming::where('task_managment_id', $thisQuickTask->id)->where('created_by', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($latestTimer != null){
            if($latestTimer->stop_time == 0){
                $taskTotalTimerTime =  $thisQuickTask->pauseTimer($latestTimer);
                $isRunning = false;
            }else{
                $taskTotalTimerTime = $thisQuickTask->startTimer();
                $isRunning = true;
            }
        }else{
            $taskTotalTimerTime = $thisQuickTask->startTimer();
            $isRunning = true;
        }
        return response()->json(['message'=>'success','isRunning'=>$isRunning,'status'=>1, 'seconds'=>$taskTotalTimerTime]);
    }

}
