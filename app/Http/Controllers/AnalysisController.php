<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\ProjectStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AnalysisController extends Controller
{
    public function index($type, Request $request)
    {
        $statusCounts=DB::table('projectstatus')
            ->select('projectstatus.id', 'projectstatus.dealname',DB::raw("(SELECT COUNT(*) as project_count FROM projects where projects.status = 1 AND projects.deal_status = projectstatus.id) as count"))
            ->get()->keyBy('id');
        return view('admin.analysis.main', compact('statusCounts'));
    }
    public function filterComparisan(Request $request)
    {
        $qry=DB::table('projectstatus')
        ->select('projectstatus.id', 'projectstatus.dealname',DB::raw("(SELECT COUNT(*) as project_count FROM projects where projects.status = 1 AND projects.deal_status = projectstatus.id) as count"))
        ->leftJoin('projects', 'projects.deal_status', 'projectstatus.id');
        $start = date('Y-m-d H:i:s', strtotime($request->start_date));
        $end = date('Y-m-d H:i:s', strtotime($request->end_date . ' +1 day'));
        $qry->whereBetween('projects.created_at',[$start,$end]);
        $statusCounts = $qry->get()->keyBy('id');
        return view('admin.analysis.components.status', compact('statusCounts'));

    }
}
