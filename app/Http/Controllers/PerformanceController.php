<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\ProjectAddon;
use App\Models\Target;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
class PerformanceController extends Controller
{
    public function performanceIndex(Request $request)
    {
        return view('admin.performance.main');
    }
    public function performanceFilter(Request $r)
    {

         $qry = User::withCount(['departments','projects','projectPositiveCompleted','projectNegtiveCompleted','projectMovedIn','projectMovedOut'])
         ->leftJoin("project_team", function($join){
            $join->on("users.id", "=", "project_team.user_id");
        })
        ->leftJoin('projects', 'project_team.project_id', 'projects.id','status',1)
         ->LeftJoin('user_department', 'user_department.user_id', 'users.id')
         ->LeftJoin('departments', 'departments.id', 'user_department.department_id');
         if($r->active_users == 0){
            $qry = $qry->where('users.login_toggle', 1);
        }


        if(!empty($r->start_date) && !empty($r->end_date)){
            $start = date('Y-m-d H:i:s', strtotime($r->start_date));
            $end = date('Y-m-d H:i:s', strtotime($r->end_date . ' +1 day'));
            $qry->whereBetween('users.created_at',[$start,$end]);
        }
        if(!empty($r->search)){
            $search = $r->search;
            $qry->where(function ($q) use ($search) {
                $q->orWhere('users.name','like',"%$search%");
                $q->orWhere('departments.name','like',"%$search%");
                $q->orWhere('projects.title','like',"%$search%");
                $q->where('project_team.status','!=',0);
            });

        }
        $employees = $qry->distinct('users.id')->paginate();
        return view('admin.performance.includes.view', compact('employees'))->render();
    }
    public function viewPerformanceDetail(Request $request)
    {
        $thisPerformance = User::withCount(['departments','projects','projectPositiveCompleted','projectNegtiveCompleted','projectMovedIn','projectMovedOut'])->whereId($request->id)->first();
        return view('admin.performance.includes.detail', compact('thisPerformance'));
    }
}
