<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Setting;
use App\Models\TrainingSession;

class TrainingSessionController extends Controller
{
    public function index(){
        return view('admin.trainingSession.main');
    }

    public function trainingSessionStore(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'user_id'=>'required'
        ]);

        $data = $request->input();
        $data['created_by'] = Auth::user()->id;
        array_shift($data);
        unset($data["id"]);
        unset($data["reqType"]);
        unset($data["user_id"]);

        if($request->reqType == 0){
            $isCreated = TrainingSession::create($data);
        }else{
            $isCreated = TrainingSession::whereId($request->id)->update($data);
            $isCreated = TrainingSession::whereId($request->id)->first();
        }
        if($isCreated){
            if(!(in_array(0, $request->user_id))){
                $isCreated->users()->sync($request->user_id);
            }else{
                $isCreated->users()->sync([]);
            }
            foreach ($request->user_id as $key => $value) {
                $message =  "You have new training session assigned, https://nutcrm.com/admin/training-sessions/".$isCreated->id;
                notifyUserByUserId(Auth::user()->name, $value,$message);
            }
            return response()->json(['message'=>'success'],200);
        }else{
            return response()->json(['message'=>"error"], 400);
        }
}
    
    public function trainingSessionToggle(Request $request){

        TrainingSession::where('id','!=',$request->id)->update([
            'status'=> 2
        ]);

        $data=TrainingSession::where('id',$request->id)->first();
        if($data->status == 2){
            $data->status = 1;
            $data->save();
        }
        return response()->json(['message'=>'success'],200);
    }
    
    // public function announcementToggle(Request $request){
    //       $data=Announcement::whereId($request->id)->first();
    //       $enable =($data->status==2)?1:2;  
    //       Announcement::whereId($request->id)->update(['status'=>$enable]);
    //       return response()->json(['message'=>'success'],200);    
    //   }

      public function trainingSessionEditPage(Request $request)
      {   $training_types = Setting::getSetting(Setting::TRAINING_SESSION_TYPE);
        $thisTrainingSession = TrainingSession::with(['users'])->whereId($request->id)->first();
        $users_name = array();
        foreach ($thisTrainingSession->users as $key => $value) {
            array_push($users_name, $value->id);
        }
        $users = User::get(['id', 'name']);
     
        return view('admin.trainingSession.includes.update', compact('thisTrainingSession', 'users', 'users_name','training_types'));
    }

      public function trainingSessionRemove(Request $request)
    {
        $isDeleted = TrainingSession::whereId($request->id)->first();
        $isDeleted->status = 0;
        $isDeleted->save();
        return response()->json(['message'=>'success'],200);
    }

    public function viewTrainingSessionDetail(Request $request)
    {
        $thisTrainingSession = TrainingSession::whereId($request->id)->first();
        return view('admin.trainingSession.includes.detail', compact('thisTrainingSession'));
    }

    public function filter(request $request)
    {
        $training_types = Setting::getSetting(Setting::TRAINING_SESSION_TYPE);
        $users = User::get(['id', 'name']);
        if ($request->ajax()) {

            $qry=TrainingSession::where('status','!=',0);
            // $qry = TrainingSession::select('training_sessions.*','user_training_session.user_id')->where('training_sessions.status',1)
            // ->leftJoin('user_training_session', 'training_sessions.id', 'user_training_session.training_session_id')
            // ->where('user_training_session.user_id', null)
            // ->orwhere('user_training_session.user_id', Auth::user()->id);

            if(!empty($request->search)){
               $search = $request->search;
                $qry->where(function ($q) use ($search) {
                    $q->orWhere('training_sessions.id','like',"%$search%");
                    $q->orWhere('training_sessions.title','like',"%$search%");
                    $q->orWhere('training_sessions.description','like',"%$search%");
                });
            }
            $trainingSession = $qry->paginate(15);
            return view('admin.trainingSession.includes.view', compact('trainingSession','users', 'training_types'))->render();
        }
    }
}
