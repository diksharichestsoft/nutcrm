<?php

namespace App\Http\Controllers;

use App\Models\Report;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use App\Models\User;

class ReportController extends Controller
{
    public function viewReportData(Request $request)
    {
        if(Auth::user()->can('all_report_view')){
            $reports   = Report::orderBy('id','desc')->paginate(15);
        }else{
            $reports   = Report::where('created_by', Auth::user()->id)->orderBy('id','desc')->paginate(15);
        }
        $status = Report::STATUS;
        return view('admin.report.main', compact('reports','status'));
    }

    public function refreshReportData(Request $request)
    {
        if(Auth::user()->can('all_report_view')){
            $reports   = Report::orderBy('id','desc')->paginate(15);
        }else{
            $reports   = Report::where('created_by', Auth::user()->id)->orderBy('id','desc')->paginate(15);
        }
        $status = Report::STATUS;
        return view('admin.report.includes.view', compact('reports','status'))->render();
    }
    public function reportSearch(Request $r)
    {
        $qry = Report::select('reports.*','users.name as user_name')
        ->join('users','reports.created_by','users.id');
        if(!(Auth::user()->can('all_report_view'))){
            $qry->where('reports.created_by', Auth::user()->id);
        }
            if(!empty($r->start_date) && !empty($r->end_date)){
            $start = date('Y-m-d H:i:s', strtotime($r->start_date));
            $end = date('Y-m-d H:i:s', strtotime($r->end_date . ' +1 day'));
            $qry->whereBetween('reports.created_at',[$start,$end]);
        }
        if(!empty($r->search)){
            $search = $r->search;
            $qry->where(function ($q) use ($search) {
                $q->orWhere('users.name','like',"%$search%");
                $q->orWhere('reports.title','like',"%$search%");
                $q->orWhere('reports.description','like',"%$search%");
            });
        }
        $reports = $qry->orderBy('id', 'DESC')->paginate(15);
        $status = Report::STATUS;
        return view('admin.report.includes.view', compact('reports', 'status'))->render();
    }
    public function reportStore(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);
        $data = $request->all();
        unset($data["_token"]);
        unset($data["reqType"]);
        unset($data["report_id"]);
        if($request->reqType == 1){
            $thisReport = Report::whereId($request->report_id);
            $id = $request->report_id;
            $isCreated = $thisReport->update($data);
        }else{
            $data["created_by"] = Auth::user()->id;
            $isCreated = Report::create($data);
            $id = $isCreated->id;
        }
        if($isCreated){
            $constant_id = Config::get('constants.notifications.on_new_report');
        $message =  "please check this bug, https://nutcrm.com/admin/report/view/".$id;
        notifyUserByUserId(Auth::user()->name, $constant_id,$message);
            return response()->json(['message'=>'success']);
        }else{
            return response()->json(['message'=>'Faild'], 404);
        }
    }

    public function reportEdit(Request $request)
    {
        $thisReport = Report::whereId($request->id)->first();
        return response()->json(['data'=>$thisReport]);
    }

    public function reportRemove(Request $request)
    {
        $isDeleted = Report::whereId($request->id)->delete();
        if($isDeleted){
            return response()->json(['message'=>'success']);
        }else{
            return response()->json(['message'=>'Faild'], 404);
        }
    }

    
    public function changeStatus(Request $request)
    {
       
        $isUpdated = Report::whereId($request->id)->update(['status'=> $request->status]);
        if($isUpdated)
        {
            $byUser=Auth::user()->name;
            $userId=Report::whereId($request->id)->first()->created_by;
            $status=Report::STATUS;
            $message="Action on your report -  ".$status[$request->status].".   Added Comment  -  ".$request->input('comment')."  https://nutcrm.com/admin/reports/detail/".$request->id;
            notifyUserByUserId($byUser, $userId, $message);
            return response()->json(['success' => true], 200 );
        }else{
            return response()->json(['success' => false], 500 );
        }
    }


    public function viewReportDetail(Request $request)
    {
        $thisReport = Report::whereId($request->id)->first();
        return view('admin.report.includes.detail', compact('thisReport'));
    }

}
