<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\DealPlatform;

use \App\Http\Requests\PlatformRequest;

use Validator;
use Log;

class PlatformController extends Controller
{
   public function viewPlatforms( Request $req )
   {
	   $platforms = DealPlatform::orderBy('id','DESC')->paginate(10);
	   return view('admin.platforms.main', compact('platforms') );
   }

   public function searchPlatforms( Request $req )
   {

	   if ( $req->filled('searched_keyword') ){

		   $platforms = DealPlatform::where('name','like',"%$req->searched_keyword%")->orderBy('id','DESC')->paginate(10);
	   } else {

		   $platforms = DealPlatform::orderBy('id','DESC')->paginate(10);
	   }

	   return view('admin.platforms.includes.view', compact('platforms') )->render();
   }

   public function storePlatform( PlatformRequest $req )
   {
	   $validated = $req->validated();

	   $platform = new DealPlatform;
	   $platform->name = $req->name;
	   $platform->save();
	   return response()->json( ['success' => true], 200);
   }

   public function editPlatform( Request $req )
   {
	   $platform = DealPlatform::find(  $req->platformId );
	   return view('admin.platforms.includes.update', compact('platform') );
   }

   public function updatePlatform( PlatformRequest $req )
   {

	   $validated = $req->validated();

	   $platform = DealPlatform::find( $req->platform_id );
	   $platform->name = $req->name;
	   $platform->save();

	   return response()->json( ['success' => true ], 200 );
   }

   public function deletePlatform( Request $req )
   {

	   $platform = DealPlatform::find( $req->platformId );
	   $platform->delete();

	   return response()->json( ['success' => true], 200);
   }

}
