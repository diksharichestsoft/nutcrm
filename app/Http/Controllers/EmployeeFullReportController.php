<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\TaskStatus;
use App\Models\User;
use App\Models\TotalDsr;

use Illuminate\Http\Request;

class EmployeeFullReportController extends Controller
{
    public function index(Request $request)
    {
        $userId = $request->id;
        return view('admin.employeeReport.main', compact('userId'));
    }
    public function filter(Request $request)
    {
        $qry = TotalDsr::with('users')->select(
            'total_dsrs.*','users.name as user_name')
            ->join('users','total_dsrs.user_id','users.id')
           ->where('id',$request->id);

     //  where('id',$request->id);
        if(!empty($request->search)){
            $search = $request->search;
            $qry->where(function ($q) use ($search) {
                $q->orWhere('users.name','like',"%$search%");
            });
        }
     //   $users = $qry->paginate(15);
        return view('admin.employeeReport.includes.view', compact('qry'))->render();

    }
}
