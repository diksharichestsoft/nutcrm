<?php
namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Models\Deals;
use App\Models\Project;
use App\Models\DealStatus;
use App\Models\ProjectStatus;
use App\Models\User;
use App\Models\Company;
use App\Models\ProjectType;
use App\Models\DealPlatform;
use App\Models\ProjectFeed;
use App\Models\DealComment;
use App\Models\PunchTiming;

use Illuminate\Http\Request;
use App\Http\Requests\storeProjectRequest;
use App\Http\Requests\UpdateDeal;
use App\Http\Requests\DealCommentRequest;
use App\Models\ProjectsClosedReason;

class AttendenceController extends Controller
{

    public function index()
    {
        $ProjectStatus=ProjectStatus::get();
        // $data=Project::where('deal_status',1)->where('status',1)->orderBy('id','DESC')->paginate(15);
        $data=User::with(['punchTiming' => function($q)  {
            $q->whereDate('created_at',date('Y-m-d'));
            $q->orderBy('id','DESC');
        }])->orderBy('name','ASC')->paginate(15);
        //return view('admin.attendance.attendance',compact('data','ProjectStatus'));

        return view('admin.attendance.main',compact('data','ProjectStatus'));
    }

    public function changeAttendenceMonth(Request $request){
        dd($request->month);
    }
    public function add(Request $request)
    {
            $platform = DealPlatform::get();
            $project_type = ProjectType::get();
            $users=User::get();
            $companies = Company::get();
            return  view('admin.projects.includes.addform',compact('platform','project_type','companies','users'))->render();
    }
    public function dealComment(DealCommentRequest $request)
    {
        $data =  ProjectFeed::create([
        'comment'=>$request->comment,
        'ip_address'=>request()->ip(),
        'model_type'=>"App/Project",
        'model_id'=>$request->deal_id,
        // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
        'branch_id'=> 1,
        'type'=>1,
        'created_by'=>auth()->user()->id,
        ]);
        return response()->json(['data'=>$data,'message'=>'Commnet Added Sucessfully']);
    }
    public function store(storeProjectRequest $request)
    {
        $data = [
            'title'           =>  $request->project_title??'',
            'client_name'     =>  $request->client_name??'',
            'project_type'    =>  $request->department??'',
            'platform'        =>  $request->platform??'',
            'platform_id'     =>  $request->platform_id??'',
            'job_descriprion' =>  $request->job??'',
            'url'             =>  $request->url??'',
            'client_email'    =>  $request->client_email??'',
            'client_phone'    =>  $request->client_phone??'',
            'budget'          =>  $request->price??'',
            'company_id'      =>  $request->company??'',
            'created_by'      =>  auth()->user()->id,
        ];
        if(isset($request->id)){
            $deal_exist = Project::find($request->id);
            if($deal_exist){
                $deal=$deal_exist->update($data);
                $msg ="update";
            }
        }else{
            $deal=Project::create($data);
            $data =  ProjectFeed::create([
                'title'=>' has created a Project '.$deal->title,
                'comment'=>$request->deal_title??'',
                'ip_address'=>request()->ip(),
                'model_type'=>"App/Project",
                // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
                'branch_id'=> 1,
                'model_id'=>$deal->id,
                'type'=>0,
                'created_by'=>auth()->user()->id,
                ]);
            $msg ="create";

        }
        if($deal)
        {
            return response()->json(['data'=>$deal,'message'=>$msg]);
        }
    }
    public function filter(Request $r)
    {

        // $active = $r->active;
        // $qry = Project::select('projects.*','project_type.name as project_name','users.name as user_name','deal_platform.name as platform_name')
        // ->join('project_type','projects.project_type','project_type.id')
        // ->join('users','projects.created_by','users.id')
        // ->join('deal_platform','projects.platform','deal_platform.id');


        // if(!empty($r->active) && $r->active!=-1 )
        //     // $qry->where('projects.deal_status',$r->active);
        //     $qry->where('projects.deal_status',$r->active);
        // if(!empty($r->start_date) && !empty($r->end_date)){
        //     $start = date('Y-m-d H:i:s', strtotime($r->start_date));
        //     $end = date('Y-m-d H:i:s', strtotime($r->end_date . ' +1 day'));
        //     $qry->whereBetween('projects.created_at',[$start,$end]);
        // }
        // $qry->where('projects.status',1);
        // if(!empty($r->search)){
        //     $search = $r->search;
        //     $qry->where(function ($q) use ($search) {
        //         $q->orWhere('projects.id','like',"%$search%");
        //         $q->orWhere('projects.title','like',"%$search%");
        //         $q->orWhere('projects.client_name','like',"%$search%");
        //         $q->orWhere('project_type.name','like',"%$search%");
        //         $q->orWhere('users.name','like',"%$search%");
        //         $q->orWhere('deal_platform.name','like',"%$search%");
        //         $q->orWhere('projects.platform_id','like',"%$search%");
        //         $q->orWhere('projects.job_descriprion','like',"%$search%");
        //         $q->orWhere('projects.url','like',"%$search%");
        //         $q->orWhere('projects.client_email','like',"%$search%");
        //         $q->orWhere('projects.client_phone','like',"%$search%");
        //         $q->orWhere('projects.budget','like',"%$search%");
        //     });
        // }
        // $data = $qry->orderBy('id','DESC')->paginate(15);
        // $hotdeals_status_ID=$r->active;
        // $ProjectStatus=ProjectStatus::all();
        $ProjectStatus=ProjectStatus::get();
        // $data=Project::where('deal_status',1)->where('status',1)->orderBy('id','DESC')->paginate(15);
        $data=User::with(['punchTiming' => function($q)  {
            // $q->whereDate('created_at',<,date('Y-m-d'));
            $q->orderBy('id','DESC');
        }])->orderBy('name','ASC')->paginate(15);
        // echo '<pre>';
        // dd($data);

        return view('admin.attendance.includes.view', compact('data','ProjectStatus'))->render();
    }
    public function accept(Request $request)
    {
        $dealstatus=2;
        $update=Project::where('id',$request->id)->update(['deal_status'=>$dealstatus]);
        $deal=Project::where('id',$request->id)->first();
        if($update)
        {
            $data =  ProjectFeed::create([
                'title'=>' has accept the Project '.$deal->title,
                'comment'=>'Project Accpted',
                'ip_address'=>request()->ip(),
                'model_type'=>"App/Project",
                'model_id'=>$request->id,
                // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
                'branch_id'=> 1,
                'type'=>0,
                'created_by'=>auth()->user()->id,
                ]);
            return response()->json(['message'=>'success']);
        }
        else{
            return response()->json(['message'=>'failed']);
        }
    }
    public function changeStatus(Request $request)
    {
        // dd($request->input());
        $deal = Project::where('id',$request->deal_id)->first();
        if($deal){
            Project::where('id',$request->deal_id)->update(['deal_status'=>$request->status]);
            $status = ProjectStatus::find($request->status);
            // if(!empty($request->comment)){
                $data =  ProjectFeed::create([
                    'title'=>' has changed the Project status to '.$status->dealname,
                    'comment'=>$request->comment??'',
                    'ip_address'=>request()->ip(),
                    'model_type'=>"App/Project",
                    'model_id'=>$request->deal_id,
                    'type'=>0,
                    // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
                    'branch_id'=>1,
                    'created_by'=>auth()->user()->id,
                ]);
            // }
        }
        return  response()->json(['message'=>'Status Changed Sucessfully']);
    }

    public function viewData($id)
    {
        // Session::put('view_id', $id);

        $getProjectData= $qry = Project::select('projects.*','project_type.name as project_name','users.name as user_name','deal_platform.name as platform_name')
        ->join('project_type','projects.project_type','project_type.id')
        ->join('users','projects.created_by','users.id')
        ->join('deal_platform','projects.platform','deal_platform.id')
        ->where('projects.id',$id)
        ->first();

        $getcomment=ProjectFeed::select('project_feed.*','users.name')
        ->leftJoin('users','project_feed.created_by','users.id')
        ->where('model_id',$id)
        ->orderBy('id','DESC')
        ->paginate(5);
        $seocomment = array();
        $devcomment = array();
        foreach ($getcomment as $key => $value) {
            if($value->branch_id == 0){
                array_push($seocomment, $value);
            }
            else if($value->branch_id == 1){
                array_push($devcomment, $value);
            }
        }
        $comments=DealComment::select('deal_comments.*','users.name')->leftJoin('users','deal_comments.created_by','users.id')->where('deal_id',$id)->paginate(5);
        return  view('admin.projects.detail',compact('getProjectData','getcomment','devcomment','seocomment','comments'))->render();
    }

    public function updateData(Request $request)
    {
        $platform = DealPlatform::get();
        $project_type = ProjectType::get();
        $users=User::get();
        $companies = Company::get();
        $getProjectData=Project::where('id',$request->id)->first();
        // dd($getProjectData);
        return  view('admin.projects.includes.update',compact('getProjectData','companies','project_type','platform','users'))->render();
    }
    public function removeCompanyDealsData(Request $request)
    {
        Project::where('id',$request->id)->update(['status'=>0]);
        return response()->json(['message'=>'Delete Sucessfully']);
    }

    public function getStatusReason(Request $request){
        $reasons = ProjectsClosedReason::all();
        return response()->json(['reasons'=>$reasons]);

    }


}
