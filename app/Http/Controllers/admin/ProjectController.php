<?php
namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use App\Models\Deals;
use App\Models\Project;
use App\Models\DealStatus;
use App\Models\ProjectStatus;
use App\Models\User;
use App\Models\Company;
use App\Models\ProjectType;
use App\Models\DealPlatform;
use App\Models\ProjectFeed;
use App\Models\DealComment;
use App\Models\MasterPswd;

use Illuminate\Http\Request;
use App\Http\Requests\storeProjectRequest;
use App\Http\Requests\UpdateDeal;
use App\Http\Requests\DealCommentRequest;
use App\Models\ProjectPayment;
use App\Models\ProjectsClosedReason;
use App\Models\ProjectTeam;

use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

use \App\Models\ProjectSheet;
use \App\Models\ProjectCredentials;
use \App\Models\ProjectDemoServerDetail;
use App\Http\Traits\ImageUpload;
use App\Models\ProjectAddon;
use Log;

class ProjectController extends Controller
{
	use ImageUpload;

    public function index()
    {
        $ProjectStatus=ProjectStatus::get();

        //Permissions checks for count
        $qry = DB::table('projects')->where('projects.status', 1)
        ->select('deal_status', DB::raw('count(*) as total'))
        ->join('selected_departments', 'selected_departments.model_id', 'projects.id')
        ->groupBy('deal_status');

        $thisUser = Auth::user();
        if($thisUser->can('projects_all_leads_of_any_user')){
            if($thisUser->can('projects_digital_marketing') && $thisUser->can('projects_development')) {
                $qry = $qry;
                // dd($qry->get());
            }
            else{
                if($thisUser->can('projects_digital_marketing')){
                    $qry = $qry->where('selected_departments.department_id', 2);
                }
                if($thisUser->can('projects_development')){
                    $qry = $qry->where('selected_departments.department_id','!=',2);
                }
            }
        }
        else{
            if($thisUser->can('projects_digital_marketing') &&  $thisUser->can('projects_development')){
                $qry = $qry;
                if($thisUser->can('projects_assigned')){
                    $qry =$qry->join('project_team', 'project_team.project_id', '=', 'projects.id')->where('project_team.user_id', '=' , Auth::user()->id);
                }else{
                    $qry = $qry->where('projects.created_by', $thisUser->id);
                }
            }else{
                if($thisUser->can('projects_digital_marketing')){
                    // $qry = $qry->where('projects.project_type', 2);
                    $qry = $qry->where('selected_departments.department_id', 2);
                    if($thisUser->can('projects_assigned')){
                        $qry =$qry->join('project_team', 'project_team.project_id', '=', 'projects.id')->where('project_team.user_id', '=' , Auth::user()->id);
                    }else{
                        $qry = $qry->where('projects.created_by', $thisUser->id);
                    }
                }
                else{
                        if($thisUser->can('projects_development')){
                        // $qry = $qry->where('project_type','!=',2);
                        $qry = $qry->where('selected_departments.department_id','!=',2);
                        if($thisUser->can('projects_assigned')){
                            $qry =$qry->join('project_team', 'project_team.project_id', '=', 'projects.id')->where('project_team.user_id', '=' , Auth::user()->id);
                        }else{
                            $qry = $qry->where('projects.created_by', $thisUser->id);
                            }
                  }
                }
            }
        }
        // end permissions check for count
        $projectsCount = $qry->pluck('total','deal_status')->all();
        // $projectsCount = [0];



        return view('admin.projects.main',compact('ProjectStatus', 'projectsCount'));
    }
    public function add(Request $request)
    {
            $platform = DealPlatform::get();
            $project_type = ProjectType::get();
            $users=User::get();
            $companies = Company::get();
            return  view('admin.projects.includes.addform',compact('platform','project_type','companies','users'))->render();
    }
    public function dealComment(DealCommentRequest $request)
    {
        $data =  ProjectFeed::create([
        'title'=>' has Comment on this project',
        'comment'=>$request->comment,
        'ip_address'=>request()->ip(),
        'model_type'=>"App/Project",
        'model_id'=>$request->deal_id,
        // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
        'branch_id'=> $request->branch_id,
        'type'=>1,
        'created_by'=>auth()->user()->id,
        ]);
        if($data){
            return response()->json(['data'=>$data,'message'=>'Comment Added Sucessfully']);
        }else{
            return response()->json(["message" => "error"], 500);
        }
    }
    public function store(storeProjectRequest $request)
    {
        $data = [
            'title'           =>  $request->project_title??'',
            'client_name'     =>  $request->client_name??'',
            'project_type'    =>  $request->department??'1',
            'platform'        =>  $request->platform??'',
            'platform_id'     =>  $request->platform_id??'',
            'job_descriprion' =>  $request->job??'',
            'url'             =>  $request->url??'',
            'client_email'    =>  $request->client_email??'',
            'client_phone'    =>  $request->client_phone??'',
            'budget'          =>  $request->price??'',
            'company_id'      =>  $request->company??'',
            'client_shipping_address' => $request->client_shipping_address ?? '',
            'client_whatsapp_number' => $request->client_whatsapp_number ?? '',
            'client_skype' => $request->client_skype ?? '',
            'client_slack_link' => $request->client_slack_link ?? '',
            'client_slack_username' => $request->client_slack_username ?? '',
            'client_slack_password' => $request->client_slack_password ?? '',
            'client_telegram_number' => $request->client_telegram_number ?? '',
        ];
        if(isset($request->id)){
            $deal_exist = Project::find($request->id);

            if($deal_exist){
                $data["updated_by"] = Auth::user()->id;
                $deal=$deal_exist->update($data);
                $thisProject = Project::find($request->id);
                if($deal){
                    $data =  ProjectFeed::create([
                        'title'=>' has updated Project '.$thisProject->title,
                        'comment'=>$request->deal_title??'',
                        'ip_address'=>request()->ip(),
                        'model_type'=>"App/Project",
                        // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
                        'branch_id'=> 1,
                        'model_id'=>$thisProject->id,
                        'type'=>0,
                        'created_by'=>auth()->user()->id,
                        ]);
                }
                $departmentArr =$request->department_id;
                $departmentArrWithType = [];
                foreach ($departmentArr as $eachDepartment) {

                    //collect all inserted department IDs
                    $departmentArrWithType[$eachDepartment] = ['type' => '0'];
                    $deal_exist->projectTypes()->sync($departmentArrWithType);
                }
                //Insert into selected_departemnt table
                $msg ="update";
            }
        }else{

            $data["created_by"] = auth()->user()->id;
            $deal=Project::create($data);
            if($deal){
                $departmentArr =$request->department_id;
                $departmentArrWithType = [];
                foreach ($departmentArr as $eachDepartment) {

                    //collect all inserted department IDs
                    $departmentArrWithType[$eachDepartment] = ['type' => '0'];
                }
                //Insert into selected_departemnt table
                $deal->projectTypes()->sync($departmentArrWithType);
            }
            $data =  ProjectFeed::create([
                'title'=>' has created a Project '.$deal->title,
                'comment'=>$request->deal_title??'',
                'ip_address'=>request()->ip(),
                'model_type'=>"App/Project",
                // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
                'branch_id'=> 1,
                'model_id'=>$deal->id,
                'type'=>0,
                'created_by'=>auth()->user()->id,
                ]);
            $msg ="create";

        }
        if($deal)
        {
            return response()->json(['data'=>$deal,'message'=>$msg]);
        }
    }
    public function filter(Request $r)
    {
        $active = $r->active;
        // dd(Project::get());
        DB::statement("SET SQL_MODE=''");
        $qry = Project::select(
            'projects.*',
            'projectstatus.dealname as project_status_name',
            'project_type.name as project_name',
            'deal_platform.name as platform_name',
            'created_by_users.name as created_by_user_name',
            'updated_by_users.name as updated_by_user_name',
            'project_feed.created_at as project_feed_created_at '
            )
        ->leftjoin('users as created_by_users','projects.created_by','created_by_users.id')
        ->leftjoin('users as updated_by_users','projects.updated_by','updated_by_users.id')
        ->join('deal_platform','projects.platform','deal_platform.id')
        ->join('projectstatus', 'projects.deal_status', 'projectstatus.id')
        ->leftjoin('selected_departments', 'selected_departments.model_id', 'projects.id')
        ->leftjoin('project_feed', function($join) {
            $join->on('project_feed.model_id', '=', 'projects.id')
            ->on('project_feed.id', '=', DB::raw("(select max(id) from project_feed WHERE project_feed.model_id = projects.id)"));
        })
        ->leftjoin('project_type','selected_departments.department_id','project_type.id');

        if(!empty($r->active) && $r->active!=-1 && $r->active != -2 )
            $qry->where('projects.deal_status',$r->active);
        if(!empty($r->start_date) && !empty($r->end_date)){
            $start = date('Y-m-d H:i:s', strtotime($r->start_date. '-1 day'));
            $end = date('Y-m-d H:i:s', strtotime($r->end_date . ' +1 day'));
            $qry->whereBetween('projects.created_at',[$start,$end]);
        }
        $qry->where('projects.status',1);

        // If status is meeting show only which they putted in meeting
        if($r->active == 3){
            $isBussinessEmployee = (Auth::user()->departments()->where('departments.id',8)->count() > 0);
            if($isBussinessEmployee){
                $qry->where('status_meeting_by_user_id', Auth::user()->id);
            }
        }

        $thisUser = Auth::user();
        if($thisUser->can('projects_all_leads_of_any_user')){
            if($thisUser->can('projects_digital_marketing') && $thisUser->can('projects_development')) {
                $qry = $qry;
                // dd($qry->get());
            }
            else{
                if($thisUser->can('projects_digital_marketing')){
                    $qry = $qry->where('selected_departments.department_id', 2);
                }
                if($thisUser->can('projects_development')){
                    $qry = $qry->where('selected_departments.department_id','!=',2);
                }
            }
        }
        else{
            if($thisUser->can('projects_digital_marketing') &&  $thisUser->can('projects_development')){
                $qry = $qry;
                if($thisUser->can('projects_assigned')){
                    $qry =$qry->join('project_team', 'project_team.project_id', '=', 'projects.id')->where('project_team.user_id', '=' , Auth::user()->id);
                }else{
                    $qry = $qry->where('projects.created_by', $thisUser->id);
                }
            }else{
                if($thisUser->can('projects_digital_marketing')){
                    // $qry = $qry->where('projects.project_type', 2);
                    $qry = $qry->where('selected_departments.department_id', 2);
                    if($thisUser->can('projects_assigned')){
                        $qry =$qry->join('project_team', 'project_team.project_id', '=', 'projects.id')->where('project_team.user_id', '=' , Auth::user()->id);
                    }else{
                        $qry = $qry->where('projects.created_by', $thisUser->id);
                    }
                }
                else{
                        if($thisUser->can('projects_development')){
                        // $qry = $qry->where('project_type','!=',2);
                        $qry = $qry->where('selected_departments.department_id','!=',2);
                        if($thisUser->can('projects_assigned')){
                            $qry =$qry->join('project_team', 'project_team.project_id', '=', 'projects.id')->where('project_team.user_id', '=' , Auth::user()->id);
                        }else{
                            $qry = $qry->where('projects.created_by', $thisUser->id);
                            }
                  }
                }
            }
        }
        if(!empty($r->search)){
            $search = $r->search;
            $qry->where(function ($q) use ($search) {
                $q->orWhere('projects.id','like',"%$search%");
                $q->orWhere('projects.title','like',"%$search%");
                $q->orWhere('project_type.name','like',"%$search%");
                $q->orWhere('projects.client_name','like',"%$search%");
                $q->orWhere('created_by_users.name','like',"%$search%");
                $q->orWhere('updated_by_users.name','like',"%$search%");
                $q->orWhere('deal_platform.name','like',"%$search%");
                $q->orWhere('projects.platform_id','like',"%$search%");
                $q->orWhere('projects.job_descriprion','like',"%$search%");
                $q->orWhere('projects.url','like',"%$search%");
                $q->orWhere('projects.client_email','like',"%$search%");
                $q->orWhere('projects.client_phone','like',"%$search%");
                $q->orWhere('projects.budget','like',"%$search%");
            });
        }
        if($active == -2){
        $data = $qry->groupBy('projects.id')->orderBy('project_feed.created_at','DESC')->paginate(20);
        }else
        {
         $data = $qry->groupBy('projects.id')->orderBy('id','DESC')->paginate(20);
        }
        $hotdeals_status_ID=$r->active;
        $ProjectStatus=ProjectStatus::all();
        return view('admin.projects.includes.view', compact('data','ProjectStatus','hotdeals_status_ID','active'))->render();
    }
    public function accept(Request $request)
    {
        $dealstatus=2;
        $update=Project::where('id',$request->id)->update(['deal_status'=>$dealstatus, 'accepted_by'=>Auth::user()->id]);
        $deal=Project::where('id',$request->id)->first();
        if($update)
        {
            $data =  ProjectFeed::create([
                'title'=>' has accept the Project '.$deal->title,
                'comment'=>'Project Accpted',
                'ip_address'=>request()->ip(),
                'model_type'=>"App/Project",
                'model_id'=>$request->id,
                // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
                'branch_id'=> 1,
                'type'=>0,
                'created_by'=>auth()->user()->id,
                ]);
            return response()->json(['message'=>'success']);
        }
        else{
            return response()->json(['message'=>'failed']);
        }
    }
    public function changeStatus(Request $request)
    {
        $deal = Project::where('id',$request->deal_id)->first();
        if($deal){
            if($request->status == 3){
                $this->validate($request, [
                    'meeting_time' => 'required'
                ]);
                Project::where('id',$request->deal_id)->update(['deal_status'=>$request->status, 'status_meeting_by_user_id' => Auth::user()->id, 'meeting_time'=> $request->meeting_time]);
            }else{
                Project::where('id',$request->deal_id)->update(['deal_status'=>$request->status]);
            }

            $status = ProjectStatus::find($request->status);
            if($request->status == 3){
                $feed_title = ' has changed the deal status to '.$status->dealname.', And Scheduled time for '.date('d-m-Y h:i a',strtotime($request->meeting_time));
            }else{
                $feed_title = ' has changed the deal status to '.$status->dealname;
            }
                $data =  ProjectFeed::create([
                    'title'=>$feed_title,
                    'comment'=>$request->comment??'',
                    'ip_address'=>request()->ip(),
                    'model_type'=>"App/Project",
                    'model_id'=>$request->deal_id,
                    'type'=>0,
                    // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
                    'branch_id'=>1,
                    'projects_closed_reason_id'=>$request->reason_id,
                    'created_by'=>auth()->user()->id,
                ]);
        }
        $constant_id = Config::get('constants.notifications.on_change_project_status');
        $message =  "Project status changed, https://nutcrm.com/admin/projects/viewData/".$request->deal_id;
        notifyUserByUserId(Auth::user()->name, $constant_id,$message);
        return  response()->json(['message'=>'Status Changed Sucessfully']);
    }

    public function viewData(Request $request)
    {
        $id = $request->id;
        $getProjectData= Project::select('projects.*','project_type.name as project_name','users.name as user_name','deal_platform.name as platform_name', 'projectstatus.dealname as project_status_name')
        ->leftjoin('selected_departments', 'projects.id', 'selected_departments.model_id')
        ->leftjoin('project_type','selected_departments.department_id','=','project_type.id')
        ->leftjoin('users','projects.created_by','users.id')
        ->join('projectstatus', 'projects.deal_status', 'projectstatus.id')
        ->leftjoin('deal_platform','projects.platform','deal_platform.id')
        ->where('projects.id',$id)
        ->first();

        $subscription_date = $getProjectData->subscription_date;
        $currentDate = $date = date('Y-m-d');
        $devcomment=ProjectFeed::select('project_feed.*','users.name')
            ->leftJoin('users','project_feed.created_by','users.id')
            ->where('model_id',$id)
            ->where('branch_id',$request->branch_id)
            ->orderBy('id','DESC')
            ->paginate(10);
        // $seocomment = array();
        // $devcomment = array();
        // foreach ($getcomment as $key => $value) {
        //     if($value->branch_id == 0){
        //         array_push($seocomment, $value);
        //     }
        //     else if($value->branch_id == 1){
        //         array_push($devcomment, $value);
        //     }
        // }

        $ProjectStatus = DB::table('projectstatus')->get();
        // dd($ProjectStatus);
        $allTeamMembers = User::get(["id" , "name"]);
        $comments=DealComment::select('deal_comments.*','users.name')->leftJoin('users','deal_comments.created_by','users.id')->where('deal_id',$id)->paginate(5);
        $subscriptionData = ProjectPayment::where('project_id', $getProjectData->id)->first();
        $branch_id = 1; //Add active class in timeline
        return  view('admin.projects.detail',compact('ProjectStatus','getProjectData', 'allTeamMembers','subscriptionData','devcomment','comments','branch_id'))->render();
    }

    public function updateData(Request $request)
    {
        $platform = DealPlatform::get();
        $project_type = ProjectType::get();
        $users=User::get();
        $companies = Company::get();
        $getProjectData=Project::where('id',$request->id)->first();
        return  view('admin.projects.includes.update',compact('getProjectData','companies','project_type','platform','users'))->render();
    }
    public function removeCompanyDealsData(Request $request)
    {
        Project::where('id',$request->id)->update(['status'=>0]);
        return response()->json(['message'=>'Delete Sucessfully']);
    }

    public function getStatusReason(Request $request){
        $reasons = ProjectsClosedReason::all();
        return response()->json(['reasons'=>$reasons]);
    }
    public function addSubscription(Request $request){
        $updateData = $request->all();
        array_shift($updateData);
        array_shift($updateData);
        $update = Project::whereId($request->subscriptionProjectId)->update($updateData);
        $thisProject = Project::where('id',$request->subscriptionProjectId)->first();
        $modifydate = date('Y-m-d', strtotime("$thisProject->subscription_payment_type", strtotime($thisProject->subscription_date))); //date('m-d-Y',strtotime())->modify()->format('Y-m-d');
        $getcount = ProjectPayment::where('project_id',$request->subscriptionProjectId)->get()->count();
        if($getcount<1){
            $data = [
                'project_id' => $request->subscriptionProjectId,
                'total' => $thisProject->subscription_amount,
                'paid' => 0,
                'pending' =>  ($thisProject->subscription_amount),
                'parent_id'=> 0,
                'due_date'=>$modifydate,
                'status'=> 0,
            ];
            $create = ProjectPayment::create($data);
        }
        return response()->json(['message'=>'Subscription Added Successfully']);
    }
    public function addTeamMember(Request $request)
    {
        $request->validate([
            'project_hou'=>'required',
            'project_min' => 'required'
        ]);
        $request["project_hours"] = $request->project_hou.":".$request->project_min;
        unset( $request["project_hou"]);
        unset( $request["project_min"]);
        if($request->project_hours!= null){
            $time = $request->project_hours;
            $time = explode(':', $time);
            $time =  ($time[0]*60) + ($time[1]);
        }
        $team_user_id = explode(" ", $request->team_user_id)[0];
        $team_user_name = explode(" ", $request->team_user_id)[1];
        $data = [
            "project_id" => $request->project_id,
            "user_id" => $team_user_id,
            "created_by" => auth()->id(),
            "work_hours" => $time ?? '0',
            "status" => '1',
        ];
        $message = "Added you to project https://nutcrm.com/admin/projects/viewData/".$request->project_id;
        notifyUserByUserId(Auth::user()->name, $data["user_id"], $message);

        ProjectTeam::updateOrCreate(["project_id"=>$request->project_id,"user_id"=>$request->team_user_id],$data);

        ProjectFeed::create([
            'title'=>' added member to team '.$team_user_name,
            'comment'=>$request->deal_title??'',
            'ip_address'=>request()->ip(),
            'model_type'=>"App/Project",
            // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
            'branch_id'=> 1,
            'model_id'=>$request->project_id,
            'type'=>0,
            'created_by'=>auth()->user()->id,
            ]);
        return response()->json(['message'=>'Team Added Successfully']);
    }

    public function removeTeamMember(Request $request)
    {
        $removedTeamMemberData =DB::table('project_team')
        ->join('users', 'project_team.user_id', '=', 'users.id')
        ->join('projects', 'project_team.project_id', '=', 'projects.id')
        ->where('project_team.id', '=', $request->id)
        ->select('users.name', 'project_team.project_id')
        ->get();
        $isDeleted = ProjectTeam::where('id', $request->id)->update(['status'=>0]);

        ProjectFeed::create([
            'title'=>' removed member from team '.$removedTeamMemberData[0]->name,
            'comment'=>$request->deal_title??'',
            'ip_address'=>request()->ip(),
            'model_type'=>"App/Project",
            // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
            'branch_id'=> 1,
            'model_id'=>$removedTeamMemberData[0]->project_id,
            'type'=>0,
            'created_by'=>auth()->user()->id,

        ]);
        if($isDeleted){
            return response()->json(['message'=>'Team Deleted Successfully']);
        }
        else{
            return response()->json(['message'=>'Error']);
        }

    }

    public function viewTeamManagement(Request $request)
    {
        $getProjectData = Project::where('id',$request->id)->first();
        $allTeamMembers = User::all();
        return view('admin.projects.includes.addteam', compact('getProjectData', 'allTeamMembers'))->render();
    }

    public function getTimeline(Request $request)
    {
        $id = $request->id;
        $getProjectData= $qry = Project::select('projects.*','project_type.name as project_name','users.name as user_name','deal_platform.name as platform_name')
        ->join('project_type','projects.project_type','=','project_type.id')
        ->join('users','projects.created_by','users.id')
        ->join('deal_platform','projects.platform','deal_platform.id')
        ->where('projects.id',$id)
        ->first();

            $devcomment=ProjectFeed::select('project_feed.*','users.name')
            ->leftJoin('users','project_feed.created_by','users.id')
            ->where('model_id',$id)
            ->where('branch_id',$request->branch_id)
            ->orderBy('id','DESC')
            ->paginate(20);
         //   dd($seocomment);

            // $devcomment=ProjectFeed::select('project_feed.*','users.name')
            // ->leftJoin('users','project_feed.created_by','users.id')
            // ->where('model_id',$id)
            // ->where('branch_id',1)
            // ->orderBy('id','DESC')
            // ->paginate(20);



        $comments=DealComment::select('deal_comments.*','users.name')->leftJoin('users','deal_comments.created_by','users.id')->where('deal_id',$id)->paginate();


        $branch_id = $request->branch_id;
        return view('admin.projects.includes.timeline', compact('getProjectData', 'devcomment', 'branch_id'))->render();
    }
    public function renderProjectSheetsView( Request $req )
    {

	    $project_sheets = ProjectDemoServerDetail::where('is_server_details',2)->where('project_id', $req->project_id)->get()->sortByDesc('id');
	    $project_credential_types = getProjectCredentialTypes('demo');

	    $project_id = $req->project_id;

	    return view('admin.projects.includes.sheets', compact('project_sheets', 'project_credential_types', 'project_id' ) );
    }

    public function storeProjectSheet( Request $req )
    {

	    $project_detail = new ProjectDemoServerDetail;

	    $project_detail->project_id  = $req->project_id;
	    $project_detail->user_id     = $req->user()->id;
        $project_detail->is_server_details = 2;
	    $project_detail->description = $req->description;
	    $project_detail->save();


	    $demos_credentials_with_parent_id = [];
	    foreach ( $req->demos as $demo ){

		    $demo[ 'parent_id' ] = $project_detail->id;
		    array_push( $demos_credentials_with_parent_id, $demo );
	    }

	    $project_credentials = ProjectCredentials::insert( $demos_credentials_with_parent_id );

	    $data =  ProjectFeed::create([
		    'title'      => ' has added sheet ',
		    'comment'    => '',
		    'ip_address' => request()->ip(),
		    'model_type' => "App/Project",
		    'branch_id'  => 1,             // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
		    'model_id'   => $project_detail->project_id,
		    'type'       => 0,
		    'created_by'=> auth()->user()->id,
	    ]);

	    return response()->json(['success' => true ], 200 );
    }

    public function renderEditProjectSheetModal( Request $req )
    {
	    $project_sheet = ProjectDemoServerDetail::find( $req->demo_id );
	    $project_id = $project_sheet->project_id;

	    return view('admin.projects.includes.editSheet', compact('project_sheet','project_id') );
    }

    public function updateProjectSheet( Request $req )
    {
	    $project_sheet = ProjectDemoServerDetail::find( $req->demo_id );

	    if ( $req->filled('description') ){
		    $project_sheet->description = $req->description;
		    $project_sheet->save();
	    }

	    $demos_credentials_with_parent_id = [];
	    foreach ( $req->demos as $demo ){
		    $demo[ 'parent_id' ] = $project_sheet->id;
		    array_push( $demos_credentials_with_parent_id, $demo );
	    }


	    $project_sheet->credentials()->delete(); // deleting old
	    $project_credentials = ProjectCredentials::insert( $demos_credentials_with_parent_id );


	    $data =  ProjectFeed::create([
		    'title'      => ' has edited Sheet ',
		    'comment'    => $req->deal_title??'',
		    'ip_address' => request()->ip(),
		    'model_type' => "App/Project",
		    'branch_id'  => 1,             // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
		    'model_id'   => $project_sheet->project_id,
		    'type'       => 0,
		    'created_by'=> auth()->user()->id,
	    ]);

	    return response()->json(['success' => true]);
    }

    public function deleteProjectSheet( Request $req )
    {
	    $project_sheet = ProjectDemoServerDetail::find( $req->demo_id );
	    $project_sheet->credentials()->delete();
	    $project_sheet->delete();

	    $data =  ProjectFeed::create([
		    'title'      => ' has deleted Sheet ',
		    'comment'    => $req->deal_title??'',
		    'ip_address' => request()->ip(),
		    'model_type' => "App/Project",
		    'branch_id'  => 1,             // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
		    'model_id'   => $project_sheet->project_id,
		    'type'       => 0,
		    'created_by'=> auth()->user()->id,
	    ]);

	    return response()->json(['succes' => true, 200 ]);
    }


    public function renderProjectDemosView( Request $req )
    {
	    $project_demos            = ProjectDemoServerDetail::where('is_server_details',0)->where('project_id', $req->project_id)->get()->sortByDesc('id');
	    $project_credential_types = getProjectCredentialTypes('demo');

	    $project_id = $req->project_id;

	    return view('admin.projects.includes.demo_info', compact('project_demos', 'project_credential_types', 'project_id' ) );
    }

    public function storeProjectDemos( Request $req )
    {

	    $project_detail = new ProjectDemoServerDetail;

	    $project_detail->project_id  = $req->project_id;
	    $project_detail->user_id     = $req->user()->id;
	    $project_detail->description = $req->description;
	    $project_detail->save();


	    $demos_credentials_with_parent_id = [];
	    foreach ( $req->demos as $demo ){

		    $demo[ 'parent_id' ] = $project_detail->id;
            $demo['password']=Crypt::encryptString($demo['password']);
		    array_push( $demos_credentials_with_parent_id, $demo );
	    }


	    $project_credentials = ProjectCredentials::insert( $demos_credentials_with_parent_id );

	    $data =  ProjectFeed::create([
		    'title'      => ' has added Demo ',
		    'comment'    => '',
		    'ip_address' => request()->ip(),
		    'model_type' => "App/Project",
		    'branch_id'  => 1,             // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
		    'model_id'   => $project_detail->project_id,
		    'type'       => 0,
		    'created_by'=> auth()->user()->id,
	    ]);

	    return response()->json(['success' => true ], 200 );
    }

    public function deleteProjectDemos( Request $req )
    {

	    $project_demo = ProjectDemoServerDetail::find( $req->demo_id );
	    $project_demo->credentials()->delete();
	    $project_demo->delete();

	    $data =  ProjectFeed::create([
		    'title'      => ' has deleted Demo ',
		    'comment'    => '',
		    'ip_address' => request()->ip(),
		    'model_type' => "App/Project",
		    'branch_id'  => 1,             // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
		    'model_id'   => $project_demo->project_id,
		    'type'       => 0,
		    'created_by'=> auth()->user()->id,
	    ]);

	    return response()->json( ['success' => true ], 200 );
    }

    public function renderEditProjectDemoModal( Request $req  )
    {

	    $project_demo = ProjectDemoServerDetail::find( $req->demo_id );
	    $project_credential_types = getProjectCredentialTypes('demo');
	    $project_id = $project_demo->project_id;

	    return view('admin.projects.includes.editDemo', compact('project_demo', 'project_credential_types','project_id') );
    }

    public function updateProjectDemo( Request $req )
    {

	    $project_demo = ProjectDemoServerDetail::find( $req->demo_id );

	    if ( $req->filled('description') ){
		    $project_demo->description = $req->description;
		    $project_demo->save();
	    }

	    $demos_credentials_with_parent_id = [];
	    foreach ( $req->demos as $demo ){

		    $demo[ 'parent_id' ] = $project_demo->id;
            $demo['password']=Crypt::encryptString($demo['password']);
		    array_push( $demos_credentials_with_parent_id, $demo );
	    }


	    $project_demo->credentials()->delete(); // deleting old
	    $project_credentials = ProjectCredentials::insert( $demos_credentials_with_parent_id );

	    $data =  ProjectFeed::create([
		    'title'      => ' has edited Demo ',
		    'comment'    => '',
		    'ip_address' => request()->ip(),
		    'model_type' => "App/Project",
		    'branch_id'  => 1,             // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
		    'model_id'   => $project_demo->project_id,
		    'type'       => 0,
		    'created_by'=> auth()->user()->id,
	    ]);

	    return response()->json( ['success' => true ], 200 );
    }

    public function renderProjectServersView( Request $req )
    {
	    $project_servers = ProjectDemoServerDetail::where('is_server_details',1)->where('project_id', $req->project_id)->get()->sortByDesc('id');
        $thisProject = Project::whereId($req->project_id)->first(['id', 'title']);
	    $server_credential_types = getProjectCredentialTypes('server');

	    $project_id = $req->project_id;

	    return view('admin.projects.includes.servers', compact('project_servers', 'server_credential_types', 'project_id' ,'thisProject' ) );
    }

    public function storeProjectServer( Request $req )
    {


	    // 'formKey' is added to upload file with ajax in frontend it means nothing else

	    // saving pem file
	    if ( isset( $req->formKey['pem_keys'] ) ){
		    $file          = $req->formKey['pem_keys'];
		    $fileName      = $this->UploadImage( $file,'project_keys');
		    $pem_file_path = "/project_keys/$fileName";
	    }


	    $project_detail = new ProjectDemoServerDetail;

	    $project_detail->project_id  = $req->formKey['project_id'];
	    $project_detail->user_id     = $req->user()->id;
	    $project_detail->description = $req->formKey['description'];
	    $project_detail->is_server_details = 1;
	    $project_detail->keys        = $pem_file_path ?? null;
	    $project_detail->save();


	    // parsing server credentials
	    $servers = $req->except( 'formKey' );
	    $servers_credentials_with_parent_id = [];
	    foreach ( $servers as $server ){
		    $server[ 'parent_id' ] = $project_detail->id;
            $server['password']=Crypt::encryptString($server['password']);
          array_push( $servers_credentials_with_parent_id, $server );
	    }


	    $project_credentials = ProjectCredentials::insert( $servers_credentials_with_parent_id );
        $data =  ProjectFeed::create([
		    'title'      => ' has added Server ',
		    'comment'    => '',
		    'ip_address' => request()->ip(),
		    'model_type' => "App/Project",
		    'branch_id'  => 1,             // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
		    'model_id'   => $project_detail->project_id,
		    'type'       => 0,
		    'created_by'=> auth()->user()->id,
	    ]);

	    return response()->json(['success' => true ], 200 );
    }

    public function deleteProjectServer( Request $req )
    {

	    $project_server = ProjectDemoServerDetail::find( $req->server_id );
	    $project_server->credentials()->delete();
	    $project_server->delete();

	    $data =  ProjectFeed::create([
		    'title'      => ' has deleted Server ',
		    'comment'    => '',
		    'ip_address' => request()->ip(),
		    'model_type' => "App/Project",
		    'branch_id'  => 1,             // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
		    'model_id'   => $project_server->project_id,
		    'type'       => 0,
		    'created_by'=> auth()->user()->id,
	    ]);

	    return response()->json( ['success' => true ], 200 );
    }

    public function renderEditProjectServerModal( Request $req  )
    {

	    $project_server           = ProjectDemoServerDetail::find( $req->server_id );
	    $server_credential_types = getProjectCredentialTypes('server');

	    $project_id = $project_server->project_id;

	    return view('admin.projects.includes.editServer', compact('project_server', 'server_credential_types','project_id') );
    }

    public function updateProjectServer( Request $req )
    {
        	    // saving pem file
	    if ( isset( $req->formKey['pem_keys'] ) ){
		    $file          = $req->formKey['pem_keys'];
		    $fileName      = $this->UploadImage( $file,'project_keys');
		    $pem_file_path = "/project_keys/$fileName";
            // $old_file = $pem_file_path->keys;
            // $old_files =   deleteCompanyLogo('project_keys/'.$old_file);
	    }

	    $project_server = ProjectDemoServerDetail::find( $req->formKey["server_id"]);
	    if ( $req->formKey["description"]){
		    $project_server->description = $req->formKey["description"];
            if(isset($pem_file_path)) $project_server->keys = $pem_file_path;
		    $project_server->save();
	    }

	    $servers_credentials_with_parent_id = [];
	    // foreach ( $req->servers as $server ){

		//     $server[ 'parent_id' ] = $project_server->id;
		//     array_push( $servers_credentials_with_parent_id, $server );
	    // }

	    // parsing server credentials
	    $servers = $req->except( 'formKey' );
	    $servers_credentials_with_parent_id = [];
	    foreach ( $servers as $server ){
		    $server[ 'parent_id' ] = $project_server->id;
            $server['password']=Crypt::encryptString($server['password']);
		    array_push( $servers_credentials_with_parent_id, $server );
	    }

	    $project_server->credentials()->delete(); // deleting old
	    $project_credentials = ProjectCredentials::insert( $servers_credentials_with_parent_id );

	    $data =  ProjectFeed::create([
		    'title'      => ' has edited Server ',
		    'comment'    => '',
		    'ip_address' => request()->ip(),
		    'model_type' => "App/Project",
		    'branch_id'  => 1,             // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
		    'model_id'   => $project_server->project_id,
		    'type'       => 0,
		    'created_by'=> auth()->user()->id,
	    ]);

	    return response()->json( ['success' => true ], 200 );
    }


    public function renderProjectAddonsView( Request $req )
    {
        $project_addons= ProjectAddon::with('user:id,name')->where('project_id', $req->project_id)->get();
        $project_id = $req->project_id;
        return view('admin.projects.includes.addons', compact('project_addons', 'project_id' ) );
    }

    public function storeProjectAddon( Request $request )
    {
        $thisProject = Project::whereId($request->project_id)->first(['id', 'title']);
        $data = $request->all();
        $data["created_by"] = auth()->user()->id;
        unset($data["_token"]);
        if($request->id != null){
            unset($data["id"]);
            $isCreatedAddon = ProjectAddon::whereId($request->id)->update($data);
            $data =  ProjectFeed::create([
                'title'      => ' has updated addon ',
                'comment'    => '',
                'ip_address' => request()->ip(),
                'model_type' => "App/Project",
                'branch_id'  => 1,             // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
                'model_id'   => $thisProject->id,
                'type'       => 0,
                'created_by'=> auth()->user()->id,
            ]);
        }else{
            $isCreatedAddon = ProjectAddon::create($data);
            $data =  ProjectFeed::create([
                'title'      => ' has added addon ',
                'comment'    => '',
                'ip_address' => request()->ip(),
                'model_type' => "App/Project",
                'branch_id'  => 1,             // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
                'model_id'   => $thisProject->id,
                'type'       => 0,
                'created_by'=> auth()->user()->id,
            ]);
        }

	    return response()->json(['success' => true ], 200 );
    }
    public function renderEditProjectAddonModal( Request $req  )
    {
        $thisAddon = ProjectAddon::find( $req->addon_id );
	    $project_id = $thisAddon->project_id;
	    return view('admin.projects.includes.editAddon', compact('thisAddon','project_id') );
    }
    public function deleteProjectAddon(Request $request)
    {
        $thisAddon = ProjectAddon::whereId($request->id)->first(['id', 'project_id']);
        $isDeleted = $thisAddon->delete();
        if($isDeleted){
            $data =  ProjectFeed::create([
                'title'      => ' has deleted addon ',
                'comment'    => '',
                'ip_address' => request()->ip(),
                'model_type' => "App/Project",
                'branch_id'  => 1,             // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
                'model_id'   => $thisAddon->project_id,
                'type'       => 0,
                'created_by'=> auth()->user()->id,
            ]);
            return response()->json( ['success' => true ], 200 );
        }else{
            return response()->json( ['success' => false ], 500 );

        }
    }
    public function changeAddonStatus(Request $request)
    {
        $thisAddon = ProjectAddon::whereId($request->id);
        $isUpdated = $thisAddon->update(['status'=>$request->status]);
        $thisAddon = $thisAddon->first(['id', 'title','project_id']);
        $addonStatus = [2=>"Approved", 3=>"Rejected"];
        if($isUpdated){
            $msg = 'has changed status of addon- '.$thisAddon->title.', to '.$addonStatus[$request->status];
            $data =  ProjectFeed::create([
                'title'      => $msg,
                'comment'    => '',
                'ip_address' => request()->ip(),
                'model_type' => "App/Project",
                'branch_id'  => 1,             // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
                'model_id'   => $thisAddon->project_id,
                'type'       => 0,
                'created_by'=> auth()->user()->id,
            ]);
            return response()->json( ['success' => true ], 200 );
        }else{
            return response()->json( ['success' => false ], 500 );
        }

    }

   public function viewServerPswd(Request $request) {
       $filledPswd = $request->masterPassword;
       $server = ProjectCredentials::where('id',$request->id)->first();
       $password =  Crypt::decryptString($server->password);
    //    $secretPswd = MasterPswd::where('status', 1)->first();
        $passwords = MasterPswd::where('status', 1)->get();

        foreach ($passwords as $key => $value) {
            if (Hash::check($filledPswd, $value->password)){
                return response()->json(['message'=>'success','isValidated'=>true,'password'=>$password],200);
            }
        }
            return response()->json(['message'=>'failed','isValidated'=>false, 'password'=>"password didn't matched"],200);

 }
 public function subscriptionsStatus(Request $request)
 {
    //  dd($request->all());
     $toggle = Project::whereId($request->id)->first()->subscription_status;
     if($toggle == 1){
         $isToggled = Project::whereId($request->id)->update(["subscription_status" => 0]);
         $isToggled = 0;
     }
     else if($toggle == 0){
         $isToggled=  Project::whereId($request->id)->update(["subscription_status" => 1]);
     }
     return response()->json(['success'=> $isToggled]);

 }
}
