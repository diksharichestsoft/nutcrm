<?php
namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\ProjectStatus;
use App\Models\User;
use App\Models\Company;
use App\Models\ProjectType;
use App\Models\DealPlatform;
use App\Models\ProjectFeed;
use App\Models\DealComment;

use Illuminate\Http\Request;
use App\Http\Requests\storeProjectRequest;
use App\Http\Requests\DealCommentRequest;
use App\Models\CandidateStatus;
use App\Models\Department;
use App\Models\ManageCandidate;
use App\Models\ProjectPayment;
use App\Models\ProjectsClosedReason;
use App\Models\ProjectTeam;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Traits\ImageUpload;
use App\Models\CandidateFeed;

class CandidateController extends Controller
{
    use ImageUpload;

    public function index()
    {
        $candidateStatus=CandidateStatus::get();
        $qry = DB::table('manage_candidates')->where('status', 1)
        ->select('candidate_status', DB::raw('count(*) as total'))
        ->groupBy('candidate_status');
        if(!(Auth::user()->can('candidate_all_candidate_of_any_user'))){
            $qry->where('hr_id', Auth::user()->id);
        }
        $candidatesCount = $qry->pluck('total','candidate_status')->all();

        // $data=ManageCandidate::where('status', 1)->orderBy('id','DESC')->paginate(15);


        return view('admin.candidates.main',compact('candidateStatus', 'candidatesCount'));
    }
    public function  add(Request $request)
    {
            $platform = DealPlatform::get();
            $project_type = ProjectType::get();
            $users=User::get();
            $departments = Department::get();
            $departments = Department::get();
            return  view('admin.candidates.includes.addform',compact('platform','project_type','departments','users'))->render();
    }
    public function addComment(Request $request)
    {

        $data =   CandidateFeed::create([
            'title'=>' Commented ',
            'comment'=>$request->comment??'',
            'ip_address'=>request()->ip(),
            'candidate_id' =>$request->deal_id,
            // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
            'branch_id'=> 1,
            'created_by'=>auth()->user()->id,
        ]);
        return response()->json(['data'=>$data,'message'=>'Commnet Added Sucessfully']);
    }
    public function store(Request $request)
    {
        if($request->phone != null){
            $this->validate($request, [
                'phone' => 'numeric|digits:10',
            ]);
        }
        $rules=[
            'name' => 'required',
            'department' => 'required',
            'resume' => 'max:5000',
            'phone' => 'unique:manage_candidates,phone,'.$request->id ?? '',

        ];
        if($request->email){
            $rules['email'] = 'email|unique:manage_candidates,email,'.$request->id ?? '';
        }
        $this->validate($request,
           $rules,
            [
                'resume.max' => "Resume File should be less than 5mb"
            ]
        );

        if($request->hasFile('resume')){
            $image_name=$this->UploadImage($request->file('resume'),'images');
        }


        $data = $request->input();
        array_shift($data);
        array_shift($data);
        $data += ["hr_id" => Auth::user()->id];
        (isset($image_name)) ? $data += ["resume" => $image_name] : '';

        if($request->req_type == 'create'){
            $isCreated = ManageCandidate::create($data);
            $msg ="create";
            if($isCreated){
                CandidateFeed::create([
                    'title'=>' Added Candidate '.$isCreated->name,
                    'comment'=>$request->deal_title??'',
                    'ip_address'=>request()->ip(),
                    'candidate_id' =>$isCreated->id,
                    // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
                    'branch_id'=> 1,
                    'created_by'=>auth()->user()->id,
                ]);
            }
        }
        else{
            $isCreated = ManageCandidate::where('id',$request->id)->first();
            $old_file = $isCreated->resume;
            $update = $isCreated->update($data);
            $msg ="update";
            if($update){
                $old_files =   deleteCompanyLogo('images/'.$old_file);
                CandidateFeed::create([
                    'title'=>' Updated Candidate '.$isCreated->name,
                    'comment'=>$request->deal_title??'',
                    'ip_address'=>request()->ip(),
                    'candidate_id' =>$isCreated->id,
                    // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
                    'branch_id'=> 1,
                    'created_by'=>auth()->user()->id,
                ]);
            }
        }
        $isCreated->departments()->sync($request->department ?? []);
        if($isCreated)
        {
            return response()->json(['data'=>$isCreated,'message'=>$msg]);
        }
    }
    public function filter(Request $r)
    {
        $active = $r->active;
        $qry = ManageCandidate::select('manage_candidates.*','candidate_status.name as candidate_status_name', 'departments.name as department_name', 'users.name as user_name')
            ->join('departments', 'manage_candidates.department', 'departments.id')
            ->join('candidate_status', 'manage_candidates.candidate_status', 'candidate_status.id')
            ->join('users','manage_candidates.hr_id','users.id');
        if(!empty($r->active) && $r->active!=-1 )
            $qry->where('manage_candidates.candidate_status',$r->active);
        if(!empty($r->start_date) && !empty($r->end_date)){
            $start = date('Y-m-d H:i:s', strtotime($r->start_date));
            $end = date('Y-m-d H:i:s', strtotime($r->end_date . ' +1 day'));
            $qry->whereBetween('manage_candidates.created_at',[$start,$end]);
        }
        $qry->where('manage_candidates.status',1);

        if(!empty($r->search)){
            $search = $r->search;
            $qry->where(function ($q) use ($search) {
                $q->orWhere('manage_candidates.id','like',"%$search%");
                $q->orWhere('manage_candidates.name','like',"%$search%");
                $q->orWhere('manage_candidates.email','like',"%$search%");
                $q->orWhere('manage_candidates.phone','like',"%$search%");
                $q->orWhere('manage_candidates.current_salary','like',"%$search%");
                $q->orWhere('manage_candidates.expected_salary','like',"%$search%");
                $q->orWhere('manage_candidates.notice','like',"%$search%");
                $q->orWhere('manage_candidates.total_experience','like',"%$search%");
                $q->orWhere('manage_candidates.current_company','like',"%$search%");
                $q->orWhere('departments.name','like',"%$search%");
                $q->orWhere('users.name','like',"%$search%");
            });
        }
        // ($r->active != -1) ? $data = ManageCandidate::where('candidate_status', $r->active)->where('status', 1)->orderBy('id','DESC')->paginate(15) : $data = ManageCandidate::where('status', 1)->orderBy('id','DESC')->paginate(15);
        if(Auth::user()->can('candidate_all_candidate_of_any_user')){
            $data = $qry->orderBy('id','DESC')->paginate(15);
        }else{
            $data = $qry->where('hr_id', Auth::user()->id)->orderBy('id','DESC')->paginate(15);
        }
        $hotdeals_status_ID=$r->active;
        $candidateStatus=CandidateStatus::get();

        return view('admin.candidates.includes.view', compact('data','candidateStatus','hotdeals_status_ID','active'))->render();
    }
    public function accept(Request $request)
    {
        $thisCandidate =ManageCandidate::where('id',$request->id);
        $update= $thisCandidate->update(['status'=>1, 'candidate_status' => 2]);
        if($update){
            CandidateFeed::create([
                'title'=>' Accepted Candidate '.$thisCandidate->first()->name,
                'comment'=>$request->comment??'',
                'ip_address'=>request()->ip(),
                'candidate_id' =>$thisCandidate->first()->id,
                // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
                'branch_id'=> 1,
                'created_by'=>auth()->user()->id,
            ]);
            return response()->json(['message'=>'success']);
        }

    }
    public function changeStatus(Request $request)
    {
        if($request->schedule_time != null){
            $timeArr = explode("T",$request->schedule_time);
            $timestamp = strtotime($timeArr[0]);
            $new_date = date("d-m-Y", $timestamp);
            $sTime = strtotime($timeArr[1]);
            $sTime = date('h:i a');
        }
        if($request->joining_date != null){
            $timeArr = explode("T",$request->joining_date);
            $timestamp = strtotime($timeArr[0]);
            $new_date = date("d-m-Y", $timestamp);
            $sTime = strtotime($timeArr[1]);
            $sTime = date('h:i a');
        }

        $thisCandidate = ManageCandidate::where('id',$request->deal_id)->first();
        if($thisCandidate){
            $isSuccess = ManageCandidate::where('id',$request->deal_id)->update(['candidate_status'=>$request->candidate_status, 'time_schedule' => ($request->schedule_time), 'joining_date' => ($request->joining_date)]);
            if($isSuccess){
                $status_name = CandidateStatus::whereId($request->candidate_status)->first()->name;
                if($request->schedule_time != null){
                    CandidateFeed::create([
                        'title'=>' Changed Status of '.$thisCandidate->name.' to '.$status_name.(", Scheduled time for: ".($new_date." ".$sTime) ?? ''),
                        'comment'=>$request->comment?? '',
                        'ip_address'=>request()->ip(),
                        'candidate_id' =>$thisCandidate->id,
                        // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
                        'branch_id'=> 1,
                        'created_by'=>auth()->user()->id,
                    ]);
                }
                else if($request->joining_date != null){
                    CandidateFeed::create([
                        'title'=>' Changed Status of '.$thisCandidate->name.' to '.$status_name.(", Selected Joining date of: ".($new_date." ".$sTime) ?? ''),
                        'comment'=>$request->comment?? '',
                        'ip_address'=>request()->ip(),
                        'candidate_id' =>$thisCandidate->id,
                        // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
                        'branch_id'=> 1,
                        'created_by'=>auth()->user()->id,
                    ]);
                }
                else{
                    CandidateFeed::create([
                    'title'=>' Changed Status of '.$thisCandidate->name.' to '.$status_name,
                    'comment'=>$request->comment ?? '',
                    'ip_address'=>request()->ip(),
                    'candidate_id' =>$thisCandidate->id,
                    // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
                    'branch_id'=> 1,
                    'created_by'=>auth()->user()->id,
                ]);
                }
            }
        }
        return  response()->json(['message'=>'Status Changed Sucessfully']);
    }

    public function viewData($id)
    {   $getCandidateData= ManageCandidate::whereId($id)->first();

        // $getCandidateData= $qry = Project::select('projects.*','project_type.name as project_name','users.name as user_name','deal_platform.name as platform_name')
        // ->join('project_type','projects.project_type','project_type.id')
        // ->join('users','projects.created_by','users.id')
        // ->join('deal_platform','projects.platform','deal_platform.id')
        // ->where('projects.id',$id)
        // ->first();
        $currentDate = $date = date('Y-m-d');

        $getcomment=CandidateFeed::select('candidate_feeds.*','users.name')
        ->leftJoin('users','candidate_feeds.created_by','users.id')
        ->where('candidate_id',$id)
        ->orderBy('id','DESC')
        ->paginate(10);
        $seocomment = array();
        $devcomment = array();
        foreach ($getcomment as $key => $value) {
            if($value->branch_id == 0){
                array_push($seocomment, $value);
            }
            else if($value->branch_id == 1){
                array_push($devcomment, $value);
            }
        }
        $comments=DealComment::select('deal_comments.*','users.name')->leftJoin('users','deal_comments.created_by','users.id')->where('deal_id',$id)->paginate(5);
        return  view('admin.candidates.detail',compact('getCandidateData','getcomment','devcomment','seocomment','comments'))->render();
    }

    public function updateData(Request $request)
    {
        $departments=Department::get();
        $thisCandidate=ManageCandidate::where('id',$request->id)->first();
        return  view('admin.candidates.includes.update',compact('thisCandidate','departments'))->render();
    }
    public function removeCandidate(Request $request)
    {
        $thisCandidate = ManageCandidate::where('id',$request->id);
        $remove = $thisCandidate->update(['status'=>0]);
        CandidateFeed::create([
            'title'=>' removed candidate'.$thisCandidate->select('name')->first()->name,
            'comment'=>$request->deal_title??'',
            'ip_address'=>request()->ip(),
            'candidate_id' => $request->id,
            // PLEASE CHANGE THE BRANCH_ID LATER THIS IS NOW HARDCODED
            'branch_id'=> 1,
            'created_by'=>auth()->user()->id,
        ]);
        return response()->json(['message'=>'Delete Sucessfully']);
    }
    public function getTimeline(Request $request)
    {
        $getCandidateData= ManageCandidate::whereId($request->id)->first();
        $getcomment=CandidateFeed::select('candidate_feeds.*','users.name')
        ->leftJoin('users','candidate_feeds.created_by','users.id')
        ->where('candidate_id',$request->id)
        ->orderBy('id','DESC')
        ->paginate(5);
        $seocomment = array();
        $devcomment = array();
        foreach ($getcomment as $key => $value) {
            if($value->branch_id == 0){
                array_push($seocomment, $value);
            }
            else if($value->branch_id == 1){
                array_push($devcomment, $value);
            }
        }
        $comments=DealComment::select('deal_comments.*','users.name')->leftJoin('users','deal_comments.created_by','users.id')->where('deal_id',$request->id)->paginate(5);

        return  view('admin.candidates.includes.timeline',compact('getCandidateData','getcomment','devcomment','seocomment','comments'))->render();    }


}
