<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AttendenceController extends Controller
{
    public function index()
    {
        return view('admin.attendance.main');
    }

    public function filter(Request $r)
    {
        $att_start = $r->start_date;
        $att_end = $r->end_date;
        // $arr = array($att_start,$att_end);
        $start = date('Y-m-d H:i:s', strtotime($r->start_date));
        $end = date('Y-m-d H:i:s', strtotime($r->end_date . ' +1 day'));
        $arr2 = array($start, $end);
        DB::statement("SET SQL_MODE=''");
        $qry=User::select('users.*')
        ->with(['punchTiming' => function($q) use($r) {
            $q->orderBy('id','DESC');
            // $q->where('created_at', 'like', "%$r->start_date%");
        },'attendanceForCount'=> function ($qry) use($arr2) {
            $qry->whereBetween('created_at', $arr2);
        }]);

        if($r->active_users == 0){
            $qry = $qry->where('users.login_toggle', 1);
        }
            if($att_start == $att_end){
                if($r->active == 1){

                    $qry->select('users.*','punch_timing.created_at')->join('punch_timing', 'punch_timing.user_id', 'users.id');
                    $qry->where('punch_timing.created_at', 'like', "%$r->start_date%");
                    // $qry->whereDate('punch_timing.created_at','=', "%$r->start_date%");
                }else if($r->active == 2){
                    $qry->leftJoin('punch_timing', function($join) {

                        $join->on('punch_timing.user_id','users.id')

                        ->on('punch_timing.id', '=', DB::raw("(SELECT max(punch_timing.id) from punch_timing WHERE punch_timing.user_id = users.id)"));

                    });

                    // dd($qry->count());
                    $qry->where('punch_timing.created_at','not like', "%$r->start_date%");
                    // $qry->whereDate('punch_timing.created_at','!=', "%$r->start_date%");

                }else{
                    $qry->distinct();
                }
            }
            // dd($qry->get());
        if(!empty($r->search)){
            $search = $r->search;
            $qry->where(function ($q) use ($search) {
                $q->orWhere('users.name','like',"%$search%");
            });
        }
        $data = $qry->distinct()->paginate(50);
        return view('admin.attendance.includes.view', compact('data','att_start', 'att_end'))->render();
    }

    public function viewDetails(Request $request){
        $thisUser = User::where('id', $request->id)->first();
        $res =  view('admin.attendance.includes.detail', compact('thisUser'))->render();
        return $res;
    }

}
