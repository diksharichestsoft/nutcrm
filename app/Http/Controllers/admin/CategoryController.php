<?php

// namespace App\Http\Controllers;
namespace App\Http\Controllers\admin;

use App\Http\Requests\CategoryValidate as CV;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;
use App\Models\Category;
use Auth;
use DB;
class CategoryController extends Controller
{
    public function Category(){
        $data = $this->CategoryData();
        return view('admin.category.main', compact('data'));
    }
    public function AddCategory(CV $request){
        try{
            $insert=new Category;
            $insert->title=$request->title;
            $insert->created_by=Auth::id();
            $insert->save();
            $success="Category is added Successfully";
            return view('admin.message.success',compact('success'))->render();
        }catch (Exception $e) {
            $error=$e->getMessage();
            // $error="Getting Some Issue...";
            return view('admin.message.error',compact('error'))->render();
        }
    }
    public function CategoryData(){
        return Category::paginate(15);
    }
}
