<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Notifications\TaskNotification;
use App\Models\User;

class TaskComplete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'AssigTask:Task';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Assign task to slack user ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        User::first()->notify(new TaskNotification());
    }
}
