<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class Dispatch implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
     
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $testarray=array(21,20,77,23,22);
        foreach($testarray as $test){
        notifyUserByUserId('test',$test,'hlo1');
        }
        foreach($testarray as $test){
            notifyUserByUserId('test',$test,'hlo2');
            }
        foreach($testarray as $test){
         notifyUserByUserId('test',$test,'hlo3');
         }
         foreach($testarray as $test){
           notifyUserByUserId('test',$test,'hlo4');
            }
    }
   
}
