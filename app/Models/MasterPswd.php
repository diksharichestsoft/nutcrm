<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterPswd extends Model
{
    use HasFactory;
    protected $table="master_pswds";
    protected $fillable = ['title','password','status','type','created_by'];
}
