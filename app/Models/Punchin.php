<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Punchin extends Model
{
    use HasFactory;
    protected $table="today_punch";
}
