<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectPayment extends Model
{
    use HasFactory;
    protected $table = 'project_payments';
    protected $fillable = [
        'project_id',
        'total',
        'paid',
        'pending',
        'type',
        'parent_id',
        'status',
        'cancel_reason',
        'desc',
        'method',
        'image',
        'due_date',
    ];
    public function project(){
        return $this->hasOne(Project::class, 'id', 'project_id');
    }

}
