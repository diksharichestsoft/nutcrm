<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    use HasFactory;
    protected $table = "announcements";
    protected $fillable=['title','description','status','type','color', 'created_by'];

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_announcements', 'announcement_id', 'user_id');
    }
}
