<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectTaskTiming extends Model
{
    use HasFactory;
    protected $table = "project_task_timing";
    protected $guarded =[];

    public function getTask()
    {
        return $this->belongsTo(Task::class, 'task_id', 'id');
    }
    public function getProject()
    {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }
}
