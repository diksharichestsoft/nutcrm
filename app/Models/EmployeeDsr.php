<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeDsr extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function dsrProject()
    {
        return $this->hasOne(Project::class, 'id', 'project_id');
    }
}
