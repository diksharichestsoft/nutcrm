<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectTeam extends Model
{
    use HasFactory;
    protected $table = 'project_team';
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_NEW = 2;
    public function tasks(){
        return $this->hasOne(Task::class,'project_id','project_id');
    }
    protected $fillable = [
        'project_id',
        'user_id',
        'created_by',
        'work_hours',
        'status',
    ];

}
