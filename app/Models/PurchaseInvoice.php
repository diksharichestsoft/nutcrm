<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchaseInvoice extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function project(){
        return $this->hasOne(Project::class, 'id', 'project_id');
    }
    public function company(){
        return $this->hasOne(Company::class, 'id', 'company_id');
    }
    public function createdBy(){
        return $this->hasOne(User::class, 'id', 'created_by');
    }

}
