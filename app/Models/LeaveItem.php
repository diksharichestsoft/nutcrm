<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeaveItem extends Model
{
    use HasFactory;
    protected $table = "leave_items";
    protected $fillable = ['leave_date','leave_type','leave_id','created_by','status','type','approved_by_user_id'];

    public function leave()
    {
        return $this->belongsTo(Leave::class, 'id', 'leave_id');
    }
}
