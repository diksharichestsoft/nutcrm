<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FeedModel extends Model
{
    use HasFactory;
    protected $table="feed";
    protected $fillable = [
    'title',
    'comment',
    'ip_address',
    'model_type',
    'model_id',
    'type',
    'status',
    'created_by',
    'deal_lost_reason_id'
    ];
}
