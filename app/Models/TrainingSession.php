<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrainingSession extends Model
{
    use HasFactory;
    protected $table ="training_sessions";
    protected $fillable=['title','description','status','type','created_by'];

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_training_session', 'training_session_id', 'user_id');
    }
}
