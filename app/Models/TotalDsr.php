<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TotalDsr extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function childDsrs()
    {
        return $this->hasMany(EmployeeDsr::class, 'parent_dsr_id', 'id');
    }
    public function approveByUser()
    {
        return $this->hasOne(User::class, 'id', 'approve_by');
    }
    public function employee()
    {
        return $this->hasOne(User::class,'id', 'user_id');
    }
}
