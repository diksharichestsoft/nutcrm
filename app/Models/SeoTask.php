<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeoTask extends Model
{
    use HasFactory;
    protected $table = 'seo_tasks';
    protected $guarded = [];
}
