<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaskStatus extends Model
{
    use HasFactory;
    protected $table = 'task_status';
    protected $fillable = ['title'];
    const ORDER_FOR_TASK_STATUS = [1,2,3,9,4,5,6,7,8];

}
