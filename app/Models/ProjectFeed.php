<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectFeed extends Model
{
    use HasFactory;
    protected $table="project_feed";
    protected $fillable = [
    'title',
    'comment',
    'ip_address',
    'model_type',
    'model_id',
    'type',
    'branch_id',
    'projects_closed_reason_id',
    'status',
    'created_by',
    ];

    public function reasons()
    {
        return $this->belongsTo(ProjectsClosedReason::class, 'projects_closed_reason_id');
    }
}
