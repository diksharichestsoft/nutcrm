<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Task extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function employees()
    {
        return $this->belongsToMany(User::class, 'task_users', 'task_id', 'user_id');
    }
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }
    public function assignedBy()
    {
        return $this->belongsTo(User::class, 'given_by', 'id');
    }
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'given_by', 'id');
    }
    public function thisTaskStatus()
    {
        return $this->belongsTo(TaskStatus::class, 'task_status_id', 'id');
    }
    public function latestTimer()
    {
        return $this->hasMany(ProjectTaskTiming::class)->where('created_by', Auth::user()->id)->orderBy('id', 'DESC');
    }
    public function latestTimerOfAnyUser()
    {
        return $this->hasMany(ProjectTaskTiming::class)->orderBy('id', 'DESC');
    }



    public function haveTimerRunning(){

        if(count($this->latestTimer) > 0){
            $timer =  $this->latestTimer[0]->stop_time;
        }else{
            return false;
        }
        return ($timer == 0) ? true : false;

    }

    public function haveTimerRunningByAnyUser()
    {
        if(count($this->latestTimerOfAnyUser) > 0){
            $timer =  $this->latestTimerOfAnyUser[0]->stop_time;
        }else{
            return false;
        }
        return ($timer == 0) ? true : false;

    }

    public function startTimer(){
        $dataToStoreInTaskTiming = [
            'start_time'=> now(),
            'stop_time'=> "0",
            'total_time'=>0,
            "task_id" => $this->id,
            "project_id" => $this->project_id,
            "created_by" => Auth::user()->id
        ];
        ProjectTaskTiming::create($dataToStoreInTaskTiming);
        $secs =$this->authUserTimerOnThisTask();
        return $secs;
    }

    public function pauseTimer($latestTimer){
            $diffTimer = Carbon::parse($latestTimer->start_time)->diffInSeconds(now());
            $isStopped = $latestTimer->update(['stop_time'=>now(), 'total_time' => $diffTimer]);
            $taskTotalTimerTime = (int)($this->total_timer_time) + $diffTimer;
            $this->update(['total_timer_time'=> $taskTotalTimerTime]);
            $secs =$this->authUserTimerOnThisTask();
            return $secs;
    }
    public function authUserTimerOnThisTask()
    {
        $totalTimeByThisUser = ProjectTaskTiming::select(DB::raw('sum(total_time) as total_time_by_this_user'))
        ->where('task_id', $this->id)
        ->where('created_by',Auth::user()->id)->first()->total_time_by_this_user;
        // $this->totalTimerByThisUser = $totalTimeByThisUser->total_time_by_this_user ?? 0;
        return $totalTimeByThisUser;
    }
    public function userTimerData()
    {
        $data = DB::table('project_task_timing')
        ->select('project_task_timing.created_by','users.name',
                DB::raw('SUM(project_task_timing.total_time) as total_calculated_time'),
                DB::raw('MIN(stop_time) as min_stop_time'))
        ->where('task_id', $this->id)
        ->join('users', 'users.id', 'project_task_timing.created_by')
        ->groupBy('created_by', 'users.name')->get();
        return $data;
    }

    public function thisUserTimer()
    {
        return $this->hasMany(ProjectTaskTiming::class);
    }

}
