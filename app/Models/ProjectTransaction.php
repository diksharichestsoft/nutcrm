<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectTransaction extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function project(){
        return $this->hasOne(Project::class, 'id', 'project_id');
    }
    public function company(){
        return $this->hasOne(Company::class, 'id', 'company_id');
    }
    public function taxType(){
        return $this->hasOne(TaxType::class, 'id', 'tax_type_id');
    }
    public function items()
    {
        return $this->hasMany(ProjectTransactionItem::class);
    }
    public function createdBy(){
        return $this->hasOne(User::class, 'id', 'created_by');
    }

}
