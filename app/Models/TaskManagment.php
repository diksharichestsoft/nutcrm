<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TaskManagment extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function project(){
        return $this->belongsTo(Project::class);
    }
    public function assignedBy()
    {
        return $this->belongsTo(User::class, 'send_by', 'id');
    }


    // public function latestTimer()
    // {
    //     return $this->hasMany(QuickTaskTiming::class)->where('created_by', Auth::user()->id)->orderBy('id', 'DESC');
    // }
    // public function latestTimerOfAnyUser()
    // {
    //     return $this->hasMany(QuickTaskTiming::class)->orderBy('id', 'DESC');
    // }



    // public function haveTimerRunning(){

    //     if(count($this->latestTimer) > 0){
    //         $timer =  $this->latestTimer[0]->stop_time;
    //     }else{
    //         return false;
    //     }
    //     return ($timer == 0) ? true : false;

    // }

    // public function haveTimerRunningByAnyUser()
    // {
    //     if(count($this->latestTimerOfAnyUser) > 0){
    //         $timer =  $this->latestTimerOfAnyUser[0]->stop_time;
    //     }else{
    //         return false;
    //     }
    //     return ($timer == 0) ? true : false;

    // }

    // public function startTimer(){
    //     $dataToStoreInTaskTiming = [
    //         'start_time'=> now(),
    //         'stop_time'=> "0",
    //         'total_time'=>0,
    //         'project_id'=>$this->project_id,
    //         "task_managment_id" => $this->id,
    //         "created_by" => Auth::user()->id
    //     ];
    //     QuickTaskTiming::create($dataToStoreInTaskTiming);
    //     $secs =$this->authUserTimerOnThisTask();
    //     return $secs;
    // }

    // public function pauseTimer($latestTimer){
    //         $diffTimer = Carbon::parse($latestTimer->start_time)->diffInSeconds(now());
    //         $isStopped = $latestTimer->update(['stop_time'=>now(), 'total_time' => $diffTimer]);
    //         $taskTotalTimerTime = (int)($this->total_timer_time) + $diffTimer;
    //         $this->update(['total_timer_time'=> $taskTotalTimerTime]);
    //         $secs =$this->authUserTimerOnThisTask();
    //         return $secs;
    // }
    // public function authUserTimerOnThisTask()
    // {
    //     $totalTimeByThisUser = QuickTaskTiming::select(DB::raw('sum(total_time) as total_time_by_this_user'))
    //     ->where('task_managment_id', $this->id)
    //     ->where('created_by',Auth::user()->id)->first()->total_time_by_this_user;
    //     // $this->totalTimerByThisUser = $totalTimeByThisUser->total_time_by_this_user ?? 0;
    //     return $totalTimeByThisUser;
    // }
    // public function userTimerData()
    // {
    //     $data = DB::table('project_task_timing')
    //     ->select('project_task_timing.created_by','users.name',
    //             DB::raw('SUM(project_task_timing.total_time) as total_calculated_time'),
    //             DB::raw('MIN(stop_time) as min_stop_time'))
    //     ->where('task_managment_id', $this->id)
    //     ->join('users', 'users.id', 'project_task_timing.created_by')
    //     ->groupBy('created_by', 'users.name')->get();
    //     return $data;
    // }

}
