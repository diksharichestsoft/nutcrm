<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeamLead extends Model
{
    use HasFactory;
    protected $table = 'team_lead';
    protected $fillable = [
        'user_id',
        'team_lead_id'
    ];
}
