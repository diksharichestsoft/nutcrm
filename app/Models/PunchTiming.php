<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
class PunchTiming extends Model
{
    use HasFactory;
    protected $table="punch_timing";
    protected $fillable = [
                'punch_in',
                'punch_out',
                'user_id',
                'total_time',
                'attendance_id',
                'in_ip_address',
                'out_ip_address',

    ];



    public function getAttendance()
    {
        return $this->hasOne(Attendance::class, 'id', 'attendance_id');
    }




    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

}
