<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    CONST NO_ACTION_TAKEN = 1;
    CONST FIXED = 2;
    CONST NOT_FIXED=3;
    CONST REJECTED=4;
    CONST IN_PROCESS=5;
    
    const STATUS = [
        1 =>'No Action Taken' ,2 =>'Fixed' , 3 =>'Not Fixed' , 4 =>'Rejected' , 5 =>'In Process'
    ];

    use HasFactory;
    protected $fillable = [
        'title',
        'created_by',
        'description'
    ];
    public function users()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }
}
