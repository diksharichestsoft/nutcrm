<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
    use HasFactory;

    protected $table="leaves";
    protected $fillable =['subject','description','status','leave_date','leave_type','created_by','approved_by_user_id'];

    const LEAVE_TYPE_ARR =[
        1=>"Short",
        2=>"Half",
        3=>"Full"
    ];

    public function employees()
    {
        return $this->belongsToMany(User::class, 'leave_items', 'leave_id','approved_by_user_id');
    }

    public function approveByUser()
    {
        return $this->hasOne(User::class, 'id', 'approved_by_user_id');
    }
    public function createdByUser()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }
    public function leaveItems()
    {
        return $this->hasMany(LeaveItem::class, 'leave_id','id');
    }
}
