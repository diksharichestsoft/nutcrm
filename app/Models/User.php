<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Sanctum\HasApiTokens;
use App\Notifications\TaskNotification;

use App\Models\PunchTiming;
use App\Models\Announcement;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use SebastianBergmann\CodeCoverage\Report\Xml\Totals;
use Throwable;

use function PHPUnit\Framework\returnArgument;

class User extends Authenticatable
{
    use HasRoles,HasApiTokens,HasFactory, Notifiable;

    const ROLE_ADMIN=0;
    const ROLE_STAFF=1;
    const ROLE_AGENCY=2;
    const ROLE_STREAMER=3;
    const ROLE_USER=4;

    const MALE=1;
    const FEMALE=2;
    const UPLOAD_PICTURE_PATH = "/public/images";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'roles'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];



    public function punchTiming()
    {
        return $this->hasMany(PunchTiming::class, 'user_id','id');
    }
    public function attendance()
    {
        return $this->hasMany(Attendance::class, 'created_by','id');
    }
    public function attendanceForCount()
    {
        return $this->hasMany(Attendance::class, 'created_by','id');
    }
    public function teamLead()
    {
        return $this->belongsToMany(User::class, 'team_lead', 'user_id', 'team_lead_id');
    }
    public function departments()
    {
        return $this->belongsToMany(Department::class, 'user_department', 'user_id', 'department_id');
        // return $this->name;
    }
    public function projects()
    {
        return $this->belongsToMany(Project::class, 'project_team', 'user_id', 'project_id')->where('project_team.status', 1);
    }

    public function projectPositiveCompleted()
    {
        return $this->belongsToMany(Project::class, 'project_team', 'user_id', 'project_id')->where('projects.deal_status',7);
    }


    public function projectNegtiveCompleted()
    {
        return $this->belongsToMany(Project::class, 'project_team', 'user_id', 'project_id')->where('projects.deal_status',8);
    }

    public function projectMovedIn()
    {
        return $this->belongsToMany(Project::class, 'project_team', 'user_id', 'project_id')->where('project_team.status',1);
    }


    public function projectMovedOut()
    {
        return $this->belongsToMany(Project::class, 'project_team', 'user_id', 'project_id')->where('project_team.status',0);
    }


    public function tasksAssigned()
    {
        return $this->belongsToMany(Task::class, 'task_users', 'user_id', 'task_id');

    }
    public function announcements()
    {
        return $this->belongsToMany(Announcement::class, 'user_announcements', 'user_id', 'announcement_id');

    }

    public function trainingSession()
    {
        return $this->belongsToMany(TrainingSession::class, 'user_training_session', 'user_id', 'training_session_id');

    }

    public function quickTasks()
    {
        return $this->hasMany(TaskManagment::class, 'send_to', 'id');
    }

    public function todos(){
        return $this->hasMany(EmployeeTodo::class, 'user_id' ,'id');
    }
    public function routeNotificationForSlack($notification)
    {
        //return $this->SLACK_HOOK ;
        //return $this->slack_webhook_url;
        //return config('app.SLACK_HOOK');
        $ifNoHookInEnv = 'https://hooks.slack.com/services/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';
        $hook = env('SLACK_HOOK') ?? $ifNoHookInEnv;
       return $hook;
       // return $this->slack_webhook_url;
    }
    public function notifyThisUser()
    {
        try{
            if($this->slack_id == null){
                return "Invalid Slack ID(null)";
            }
            if($this->messageToSend == null){
                return "MessageToSend is not set";
            }
            $this->notify(new TaskNotification());
            return true;


        }catch(Throwable $error){
            return $error;
        }


    }
    public function haveTimerOnTask()
    {
        $thisUserTasks = $this->tasksAssigned()->get();
        $haveTimer = false;
        $task_id = 0;
        foreach ($thisUserTasks as $key => $value) {
            if($value->haveTimerRunning()){
                $haveTimer = true;
                $task_id = $value->id;
                break;
            }
        }
        return [$haveTimer, $task_id];
    }

    public function haveTimerOnQuickTask()
    {
        $thisUserTasks = $this->quickTasks()->get();
        $haveTimer = false;
        $task_id = 0;
        foreach ($thisUserTasks as $key => $value) {
            if($value->haveTimerRunning()){
                $haveTimer = true;
                $task_id = $value->id;
                break;
            }
        }
        return [$haveTimer, $task_id];
    }

    public function missedDsrs()
    {
        $thisMonthStart = Carbon::now()->startOfMonth();
        $thisMonthEnd = Carbon::now()->endOfMonth();
        $att = Attendance::where('created_by', $this->id)->whereBetween('created_at', [$thisMonthStart, $thisMonthEnd])->whereDate('created_at', '<', today())->count();
        $dsrs = TotalDsr::where('user_id', $this->id)->whereBetween('created_at', [$thisMonthStart, $thisMonthEnd])->count();
        return $att - $dsrs;
    }
    public function missedDsrsAll()
    {
        $att = Attendance::where('created_by', $this->id)->whereDate('created_at', '>', Carbon::parse('2021-11-01'))->whereDate('created_at', '<', today())->count();
        $dsrs = TotalDsr::where('user_id', $this->id)->whereDate('created_at', '>', Carbon::parse('2021-11-01'))->whereDate('created_at', '<', today())->count();
        return $att - $dsrs;
    }
    public function missedDsrsAllDates()
    {
        $dsrs = TotalDsr::where('user_id', $this->id)->whereDate('created_at', '>', Carbon::parse('2021-11-01'))->get(['created_at'])->toArray();

        $dsr = array_unique(array_map("parseCreatedAtDate", $dsrs));

        $att =  Attendance::where('created_by',$this->id)->whereDate('created_at', '>', Carbon::parse('2021-11-01'))->get(['created_at'])->toArray();
        $attDates = array_unique(array_map("parseCreatedAtDate", $att));
        // dd($dsr, $attDates);
        $diff =array_diff($attDates, $dsr);
      // dd($diff);
      return $diff;

    }

    public function haveBirthdayToday()
    {
        if($this->date_of_birth != null){
            $bdate =  date("m-d", strtotime($this->date_of_birth));
            if($bdate == date('m-d')){
                return true;
            }else{
                return false;
            }

        }else{
            return false;
        }

    }
    public function getTodayTimerTime()
    {
        $time = ProjectTaskTiming::with(['getProject:id,title', 'getProject.getTodayActiveTask'])
            ->select('project_id',DB::raw('SUM(total_time) as today_total_time'))
            ->where('created_by', $this->id)
            ->whereDate('updated_at', today())
            ->groupby('project_id')
            ->get();
        return $time;
    }
    public function getDsrTodayTimerTime()
    {
        $time = ProjectTaskTiming::with(['getProject:id,title', 'getProject.getTodayActiveTask'])
            ->select(DB::raw('SUM(total_time) as today_total_time'),'total_time','task_id')
            ->where('created_by', $this->id)
            ->whereDate('updated_at', today())
            ->groupby('task_id','total_time')
            ->get();
        return $time;
    }
    public function getTodayQuickTaskTime()
    {
        $time = QuickTaskTiming::select(DB::raw('SUM(total_time) as today_total_time'))
            ->where('created_by', $this->id)
            ->whereDate('updated_at', today())
            ->groupby('project_id')
            ->get();
        return $time;
    }
    public function isSeoEmployee()
    {
        $data = $this->departments()->whereIn('department_id',  [5, 19, 18, 15, 10])->count();
        return $data;
    }

    public function stopAllTaskTimers()
    {
        try {
            $task_id = $this->haveTimerOnTask()[1];
            $thisTask = Task::with(['latestTimer'])->whereId($task_id)->first(['id', 'project_id', 'total_timer_time']);
            $latestTimer = ProjectTaskTiming::where('task_id', $thisTask->id)->where('created_by', Auth::user()->id)->orderBy('id', 'DESC')->first();
            $thisTask->pauseTimer($latestTimer);
            return true;
        } catch (\Throwable $th) {
            return false;
        }

    }

    public static function AssignedAnnouncementToUser(){
        $announcement=Announcement::with('users')->get();
        return $announcement;
    }

    public static function AssignedTrainingSessionToUser(){
        $trainingSession=TrainingSession::with('users')->get();
        return $trainingSession;
    }
    public function countChangedIp()
    {
        // $start = date('Y-m-d H:i:s', strtotime($start_date));
        // $end = date('Y-m-d H:i:s', strtotime($end_date . ' +1 day'));

        $count = PunchTiming::whereRaw('in_ip_address != out_ip_address')
        ->where('user_id', $this->id)
        // ->whereBetween('projects.created_at',[$start,$end])
        ->count();
                return $count;
    }

    public function leaveTypes($leave_type){
        $count = Leave::where('leave_type', $leave_type)
        ->where('created_by', $this->id)->count();
                return $count;
    }

    public function isHr()
    {
        $countDepartment = $this->departments()->where('departments.id', 6)->count();
        return ($countDepartment > 0);
    }
    public function isLinuxServerAdmin()
    {
        $countDepartment = $this->departments()->where('departments.id', 14)->count();
        return ($countDepartment > 0);
    }
    public function isBusinessSales()
    {
        $countDepartment = $this->departments()->where('departments.id', 8)->count();
        return ($countDepartment > 0);
    }
    public function test()
    {

        // Attendance::where('created_by', 75)->leftjoin('total_dsrs', function($join){
        //     $join->on('total_dsrs.user_id','attendance.created_by');
        //     $join->on('total_dsrs.created_at', '!=', 'attendance.created_at')
        // })->whereRaw("`attendance`.`created_at` not LIKE CONCAT('%',`total_dsrs`.`created_at`,'%')")->count();
        // ->where(DB::raw('attendance.created_at') ,'NOT LIKE',DB::raw('total_dsrs.created_at'))->count();
        $dsrs = TotalDsr::where('user_id', 75)->get(['created_at'])->toArray();

    }

}
