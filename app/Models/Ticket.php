<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;
    protected $table='tickets';
    protected $fillable=['title','description','priority','status','type','created_by','department_id','created_at','updated_at'];

    public function departments()
    {
        return $this->hasOne(Department::class, 'id', 'department_id');
    }

    public function createdByUser()
    {
        return $this->hasOne(User::class, 'id', 'created_by');
    }
}
