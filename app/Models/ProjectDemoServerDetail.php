<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use \App\Models\ProjectCredentials;
class ProjectDemoServerDetail extends Model
{
	use HasFactory;

	public function credentials()
	{
		return $this->hasMany( ProjectCredentials::class, 'parent_id' );
	}

	public function user()
	{
		return $this->belongsTo( User::class, 'user_id', 'id' );
	}
}
