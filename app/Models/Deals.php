<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Deals extends Model
{
    use HasFactory;
    protected $table="deals";
    protected $fillable = [
        'title',
        'client_name',
        'project_type',
        'platform',
        'platform_id',
        'job_descriprion',
        'url',
        'client_email',
        'client_phone',
        'meeting_time',
        'status_meeting_by_user_id',
        'budget',
        'contact_email',
        'contact_phone',
        'contact_description',
        'company_id',
        'created_by',
        'updated_by',
        'accepted_by',
        'assign_to',
    ];

    function platformData(){
        return $this->belongsTo(DealPlatform::class,'platform' );
    }

    function companyData(){
        return $this->belongsTo(Company::class,'company_id' );
    }
    function projectTypeData(){
        return $this->belongsTo(ProjectType::class,'project_type' );
    }
    function AssignedTo(){
        return $this->belongsTo(User::class,'assign_to' );
    }
}
