<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;

    // NOTE: Setting Key should be in Capital letters, No space(use underscore [ '_' ] ) and should be singular
    const CURRENCY = "CURRENCY";
    const TRAINING_SESSION_TYPE = "TRAINING_SESSION_TYPE";
    const TAX = "TAX";
    const INTERNATIONAL = 0;
    const DOMESTIC = 1;
    const NOGST = 2;

    // count 164
    const  CURRENCY_CODES = array(
        "AFA" => "؋",
        "ALL" => "Lek",
        "DZD" => "دج",
        "AOA" => "Kz",
        "ARS" => "$",
        "AMD" => "֏",
        "AWG" => "ƒ",
        "AUD" => "$",
        "AZN" => "m",
        "BSD" => "B$",
        "BHD" => ".د.ب",
        "BDT" => "৳",
        "BBD" => "Bds$",
        "BYR" => "Br",
        "BEF" => "fr",
        "BZD" => "$",
        "BMD" => "$",
        "BTN" => "Nu.",
        "BTC" => "฿",
        "BOB" => "Bs.",
        "BAM" => "KM",
        "BWP" => "P",
        "BRL" => "R$",
        "GBP" => "£",
        "BND" => "B$",
        "BGN" => "Лв.",
        "BIF" => "FBu",
        "KHR" => "KHR",
        "CAD" => "$",
        "CVE" => "$",
        "KYD" => "$",
        "XOF" => "CFA",
        "XAF" => "FCFA",
        "XPF" => "₣",
        "CLP" => "$",
        "CNY" => "¥",
        "COP" => "$",
        "KMF" => "CF",
        "CDF" => "FC",
        "CRC" => "₡",
        "HRK" => "kn",
        "CUC" => "$, CUC",
        "CZK" => "Kč",
        "DKK" => "Kr.",
        "DJF" => "Fdj",
        "DOP" => "$",
        "XCD" => "$",
        "EGP" => "ج.م",
        "ERN" => "Nfk",
        "EEK" => "kr",
        "ETB" => "Nkf",
        "EUR" => "€",
        "FKP" => "£",
        "FJD" => "FJ$",
        "GMD" => "D",
        "GEL" => "ლ",
        "DEM" => "DM",
        "GHS" => "GH₵",
        "GIP" => "£",
        "GRD" => "₯, Δρχ, Δρ",
        "GTQ" => "Q",
        "GNF" => "FG",
        "GYD" => "$",
        "HTG" => "G",
        "HNL" => "L",
        "HKD" => "$",
        "HUF" => "Ft",
        "ISK" => "kr",
        "INR" => "₹",
        "IDR" => "Rp",
        "IRR" => "﷼",
        "IQD" => "د.ع",
        "ILS" => "₪",
        "ITL" => "L,£",
        "JMD" => "J$",
        "JPY" => "¥",
        "JOD" => "ا.د",
        "KZT" => "лв",
        "KES" => "KSh",
        "KWD" => "ك.د",
        "KGS" => "лв",
        "LAK" => "₭",
        "LVL" => "Ls",
        "LBP" => "£",
        "LSL" => "L",
        "LRD" => "$",
        "LYD" => "د.ل",
        "LTL" => "Lt",
        "MOP" => "$",
        "MKD" => "ден",
        "MGA" => "Ar",
        "MWK" => "MK",
        "MYR" => "RM",
        "MVR" => "Rf",
        "MRO" => "MRU",
        "MUR" => "₨",
        "MXN" => "$",
        "MDL" => "L",
        "MNT" => "₮",
        "MAD" => "MAD",
        "MZM" => "MT",
        "MMK" => "K",
        "NAD" => "$",
        "NPR" => "₨",
        "ANG" => "ƒ",
        "TWD" => "$",
        "NZD" => "$",
        "NIO" => "C$",
        "NGN" => "₦",
        "KPW" => "₩",
        "NOK" => "kr",
        "OMR" => ".ع.ر",
        "PKR" => "₨",
        "PAB" => "B/.",
        "PGK" => "K",
        "PYG" => "₲",
        "PEN" => "S/.",
        "PHP" => "₱",
        "PLN" => "zł",
        "QAR" => "ق.ر",
        "RON" => "lei",
        "RUB" => "₽",
        "RWF" => "FRw",
        "SVC" => "₡",
        "WST" => "SAT",
        "SAR" => "﷼",
        "RSD" => "din",
        "SCR" => "SRe",
        "SLL" => "Le",
        "SGD" => "$",
        "SKK" => "Sk",
        "SBD" => "Si$",
        "SOS" => "Sh.so.",
        "ZAR" => "R",
        "KRW" => "₩",
        "XDR" => "SDR",
        "LKR" => "Rs",
        "SHP" => "£",
        "SDG" => ".س.ج",
        "SRD" => "$",
        "SZL" => "E",
        "SEK" => "kr",
        "CHF" => "CHf",
        "SYP" => "LS",
        "STD" => "Db",
        "TJS" => "SM",
        "TZS" => "TSh",
        "THB" => "฿",
        "TOP" => "$",
        "TTD" => "$",
        "TND" => "ت.د",
        "TRY" => "₺",
        "TMT" => "T",
        "UGX" => "USh",
        "UAH" => "₴",
        "AED" => "إ.د",
        "UYU" => "$",
        "USD" => "$",
        "UZS" => "лв",
        "VUV" => "VT",
        "VEF" => "Bs",
        "VND" => "₫",
        "YER" => "﷼",
        "ZMK" => "ZK"
    );

    protected $fillable = [
        "key",
        "value"
    ];
    public static function storeSetting($key, $valueArr)
    {
        $value = json_encode($valueArr);
        $isCreated = self::create(["key"=>$key, "value"=>$value]);
        return $isCreated;
    }
    public static function updateSetting($key,$valueArr)
    {
        $value = json_encode($valueArr);
        $isCreated = self::where('key', $key)->update(["value"=>$value]);
        return $isCreated;
    }

    public static function getSetting($key)
    {
        $data = self::where("key", $key)->pluck('value')->first();
        $valueArr = json_decode($data, true);
        return $valueArr;
    }

    public static function createInvoiceNumber($company_id, $international,$domestic, $nogst)
    {
        try {
            $invoice_numberArr = [$international, $domestic, $nogst];
            $key = 'INVOICE_NO_'.$company_id;
            self::create(["key"=>$key,"value"=> json_encode($invoice_numberArr)]);
            return true;
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    public static function getInvoiceNumber($company_id, $type)
    {
        $key = 'INVOICE_NO_'.$company_id;
        $value = self::where("key", $key)->first()->value;
        $valArr = json_decode($value);
        if($type == self::INTERNATIONAL){
            return $valArr[0];
        }
        if($type == self::DOMESTIC){
            return $valArr[1];
        }
        if($type == self::NOGST){
            return $valArr[2];
        }
    }
    public static function updateInvoiceNumber($company_id, $type,$invoice_number)
    {
        try {
            //code...
            $key = 'INVOICE_NO_'.$company_id;
            $value = self::where('key', $key)->first();
            $arr = json_decode($value->value);
            $arr[$type] = $invoice_number;
            $invoice_numberArr = json_encode($arr);
            self::where("key", $key)->update(["value"=> $invoice_numberArr]);
            return true;
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public static function increamentInvoiceNumber($company_id, $type){
        try {
            $invoice_number = self::getInvoiceNumber($company_id, $type);
            if($type == self::DOMESTIC){
                $invoice_number = str_replace('D', '', $invoice_number);
                $increament = (int)($invoice_number)+1;
                $increament = 'D'.$increament;
            }else if($type == self::NOGST){
                $invoice_number = str_replace('N', '', $invoice_number);
                $increament = (int)($invoice_number)+1;
                $increament = 'N'.$increament;
            }
            else{
                $increament = (int)($invoice_number)+1;
            }
            return self::updateInvoiceNumber($company_id, $type, $increament);
        } catch (\Throwable $th) {
            return false;
        }


    }
}
