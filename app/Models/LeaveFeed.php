<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeaveFeed extends Model
{
    use HasFactory;
    protected $table="leave_feeds";
    protected $fillable = [
        'title',
        'comment',
        'ip_address',
        //'task_id',
        'leave_id',
        'status',
        'type',
        'branch_id',
        'created_by',
        ];
    
   
 
}
