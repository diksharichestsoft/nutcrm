<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    use HasFactory;
    protected $fillable = [
        'project_name',
        'country',
        'department',
        'web_url',
        'ios_url',
        'android_url',
        'admin_url',
        'business_model',
        'description',
        'created_by',
        'updated_by',
    ];
    public function tags()
    {
        return $this->belongsToMany(Tags::class, 'portfolio_tags', 'portfolio_id', 'tag_id');
    }

    public function createdByUser()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
    public function updatedByUser()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
}
