<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Project extends Model
{
    use HasFactory;
    protected $table="projects";
    protected $fillable = [
        'title',
        'client_name',
        'project_type',
        'platform',
        'platform_id',
        'job_descriprion',
        'url',
        'client_email',
        'client_shipping_address',
        'client_phone',
        'client_whatsapp_number',
        'client_skype',
        'client_slack_link',
        'client_slack_username',
        'client_slack_password',
        'client_telegram_number',
        'budget',
        'company_id',
        'created_by',
        'updated_by',
        'accepted_by',
        'meeting_time',
        'status_meeting_by_user_id',
        'deal_id',
        'subscription_status',
        'subscription_date',
        'subscription_amount',
        'subscription_payment_type',
        'subscription_payment_method',
        'subscription_details'
    ];

    public function platform_name()
    {
        return $this->belongsTo(DealPlatform::class);
    }
    public function users()
    {
        return $this->belongsToMany(User::class, 'project_team')->withPivot('work_hours','id')->where('project_team.status',1);
    }
    public function tasks()
    {
        return $this->hasMany(Task::class);
    }
    public function projectTypes()
    {
        return $this->belongsToMany(ProjectType::class, 'selected_departments', 'model_id', 'department_id');

    }
    function companyData(){
        return $this->belongsTo(Company::class,'company_id' );
    }

    public function getTodayActiveTask()
    {
        return $this->hasMany(ProjectTaskTiming::class)->select('task_id')->whereDate('updated_at', today())->where('created_by', Auth::user()->id)->groupBy('task_id');
        // return ProjectTaskTiming::where(['project_id'=>$this->id])->select('task_id')->groupby('task_id')->get();
    }

}
