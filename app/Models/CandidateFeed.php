<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CandidateFeed extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'comment',
        'ip_address',
        'candidate_id',
        'branch_id',
        'created_by',
    ];
}
