<?php

use App\Models\Attendance;
use App\Models\FinanceData;
use App\Models\Notification;
use App\Models\Setting;
use App\Models\TotalDsr;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

if (! function_exists('getCoinsToDiamond')) {
    function getCoinsToDiamond($coins)
    {
       $conversion_rate = FinanceData::value('diamond_per_100_coin');
       return ($conversion_rate/100)*$coins;
    }
   }

   if (! function_exists('getDiamondsToIncome')) {
    function getDiamondsToIncome($diamonds)
    {
       $conversion_rate = FinanceData::value('income_per_100_diamonds');
       return ($conversion_rate/100)*$diamonds;
    }
   }

   if (! function_exists('getCoinsToIncome')) {
      function getCoinsToIncome($coins)
      {
         $coin_rate = FinanceData::value('diamond_per_100_coin');
         $diamond_rate = FinanceData::value('income_per_100_diamonds');
         return ($diamond_rate/100)*(($coin_rate/100)*$coins);
      }
     }

     if (! function_exists('getIncomeByRating')) {
      function getIncomeByRating($coins,$rating)
      {
         $coin_rate = FinanceData::value('diamond_per_100_coin');
         $diamond_rate = FinanceData::value('income_per_100_diamonds');
         $TotalIncome = ($diamond_rate/100)*(($coin_rate/100)*$coins);
         switch ($rating) {
            case "A":
               $income = (20/100)*$TotalIncome;
               return $income;
              break;
            case "B":
               $income = (15/100)*$TotalIncome;
               return $income;
              break;
            case "C":
               $income = (10/100)*$TotalIncome;
               return $income;
              break;
            default:
              $income = 0;
              return $income;
          }

      }
     }

     function deleteCompanyLogo( $logoPath )
     {
	     $imageFullPath = public_path( $logoPath );

	     if ( file_exists( $imageFullPath ) ){

		     unlink( $imageFullPath );

            }else{
                return false;
            }

        return true;
     }

     function saveCompanyLogo( $file )
     {
	     $imageName = time().".".$file->getClientOriginalExtension();
	     $file->move( public_path('/images/companies'), $imageName );
	     return "/images/companies/$imageName";
     }

     function getProjectCredentialTypes( $section = "demo")
     {
	     $demo_types_with_their_display_name = [
		     'ios'      => "IOS",
		     'android'  => "Android",
		     'admin'    => "Admin",
		     'web'      => "Web",
	     ];

	     $server_types_with_their_display_name = [
            'hosting'  => "Hosting",
            'root'     => "Root Access",
            'ip'       => "Hosting IP",
            'domain_purchased' => "Domain Purchased",
            'ftp'              => "FTP",
            'php_my_admin'     => "Php My Admin",
            'facebook'     => "Facebook",
            'pinterest'     => "Pinterest",
            'instagram'    => "Instagram",
            'twitter'		=> "Twitter",
            'linkedin'  => "Linkedin",
            "tiktok" => "Tiktok",
            "snapchat" => "Snapchat",
            "website_access"  => "Website Access",
            "admin_access" => "Admin Access",
            "contact_details"=> "Contact Details",
            "recovery_email"       => "Recovery Email",
            "recovery_number"=> "Recovery Number",
            "google_playstore_account"=> "Google(playstore Account)",
            "apple_developer_account"=> "Apple Developer Account",
            "plivio_text_sms_details"=> "Plivio/Text Sms details",
            "keystore_details"=> "Keystore details",
            "payment_gateway_details"=> "Payment gateway details",
            "firebase_details"=> "Firebase details",
            "bitbucket_github_details"=> "Bitbucket/ Github details",
            "apple_certificate_password"=> "Apple certificate password",
            "bug_tracker"=> "Bug Tracker",
            "other"=> "Other",

	     ];

	     $return_types = $section == "server" ? $server_types_with_their_display_name : $demo_types_with_their_display_name;

	     return $return_types;
     }

     function getDisplayNameOfProjectCredentailsType( $type )
     {
	     $server_types = getProjectCredentialTypes( 'server');
	     $demo_types   = getProjectCredentialTypes( 'demo' );
	     $all_types    = array_merge( $server_types, $demo_types );
	     return $all_types[ $type ];
     }

    function timestampToDateAndTimeObj($timestamp, $timeFormat = 12){
        try{
            $timestampArr = explode(' ', $timestamp);
            $date = date('d-m-Y', strtotime($timestampArr[0]));
            if($timeFormat == 24){
                $time = date('h:i:s', strtotime(($timestampArr[1])));
            }else{
                $time = date('h:i:s a', strtotime(($timestampArr[1])));
            }
            $dateTime = new stdClass();
            $dateTime->time = $time;
            $dateTime->date = $date;
            return $dateTime;
        }catch(\Throwable $th){
            $dateTime = new stdClass();
            $dateTime->time = $th->getMessage();
            $dateTime->date = $th->getMessage();
            return $dateTime;
        }
    }
function dealsBadgeClass($key){
    try{
        $arry =  array("badge badge-primary", "badge badge-success", "badge badge-info", "badge badge-warning", "badge badge-danger", "badge badge-light-green", "badge badge-purple", "badge badge-red-yellow-gradient", "badge badge-blue-yellow-gradient");
        return $arry[$key];
    }catch(Throwable $error){
        return "";
    }
}

function dealsStatusColors($key){
    try{
        $array = [
            '1' => "#FFBF00;",
            '2' => "#03C03C;",
            '10' => "#00EEEE;",
            '9' => "#DC143C;",
            '11' => "#FF0800;",
            '12' => "#0000EE;",
            '13' => "#00008B;",
            '14' => "#00A693;",
            '15' => "#548B54;",
            '16' => "#682860;",
            '17' => "#4A0000;",
            '18' => "#8B5A00;",
            '19' => "#8B8682;",
            '3' => "#007474;",
            '20' => "#6C7B8B;",
            '5' => "#E86100;",
            '21' => "#CD853F;",
            '22' => "#8B5A2B;",
            '23' => "#8B7B8B;",
            '24' => "#DEAA88;",
            '25' => "#8A496B;",
            // '26' => "#EE82EE;",
            // '6' => ;
            // '7' => ;
            // '8' => ;
        ];

        return $array[$key];
    }catch(Throwable $error){
        return "";
    }
}
function notifyUserByUserId($byUser, $userId, $message)
{
    try {
        $userToNofity = User::whereId($userId)->first(['id', 'slack_id']);
        if($userToNofity){
            $userToNofity->messageToSend = $message;
            $userToNofity->messageSentBy = $byUser;
            return $userToNofity->notifyThisUser();
        }else{
            return "User Not Found";
        }
    } catch (\Throwable $th) {
        throw $th;
    }
}
 function notifyUserOnEvent($title, $message){
    $users = Notification::where('title', $title)->first(['id'])->users()->pluck('users.id');
    $authUserName = Auth::user()->name;
    foreach ($users as $key => $value) {
        notifyUserByUserId($authUserName, $value, $message);
    }
}

function secToHoursHelper($secs, $wantSecondsToo = false)
{
    try{
        $hours = floor($secs / 3600);
        $minutes = floor(($secs / 60) % 60);
        $seconds = $secs % 60;
        if($minutes < 10) $minutes='0'.$minutes;
        if($hours < 10) $hours='0'.$hours;
        if($seconds < 10) $seconds='0'.$seconds;
        if($wantSecondsToo) return "$hours:$minutes:$seconds";
        return "$hours:$minutes";
    }catch (\Throwable $th) {
        throw $th;
    }
}
function parseCreatedAtDate($element)
{
    return Carbon::parse($element["created_at"])->format('Y-m-d');
}
function getCurrencySymbol($code)
{
    try {
        $symbol = (Setting::CURRENCY_CODES)[$code];
        return $symbol;
    } catch (\Throwable $th) {
        return $code;
    }
}
