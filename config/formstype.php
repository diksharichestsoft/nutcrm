<?php
return [
    'forms' => [
        "0"=>"quote",
        "2"=>"common",
        "2"=>"popup",
        "3"=>"newsletter"
    ],
    //company secrets keys available in sagarwatertanks.com using this only we are able to generate dynamic forms
    'secret' => [
        "1"=>"3643hdhew879483dohr93497",
        "2"=>"6362gukasduguw8379732hasdh",
        "3"=>"7348hahd72840hsldhfilw884",
        "4"=>"1638hajd7329hah6482hlslg83",
        "5"=>"7348hsdfhe98349fjwk948bk",
        "6"=>"384gsgd73ugdewgui73ghdsfh",
        "7"=>"gjghsgygyew73hewbhw8jd9389",
        "8"=>"gjdgygewhsgheyghhbygebhhbhc"
    ]

];