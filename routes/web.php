<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\DealsController;
use App\Models\Admin;

use App\Http\Controllers\admin\permissionsController;
use App\Http\Controllers\admin\RolesController;
use App\Http\Controllers\admin\RolesPermissionsController;
use App\Http\Controllers\admin\UserController;
use App\Http\Controllers\admin\ProjectController;

use App\Http\Controllers\admin\AttendenceController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\SubscriptionPaymentController;
use App\Http\Controllers\TrainingSessionController;

use App\Http\Controllers\admin\CandidateController;
use App\Http\Controllers\AnalysisController;
use App\Http\Controllers\DirectLeadController;
use App\Http\Controllers\EmployeeDsrController;
use App\Http\Controllers\PortfolioController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\TodoController;
use App\Http\Controllers\AnnouncementController;
use App\Http\Controllers\WorkLoadController;
use App\Http\Controllers\TicketController;
use App\Http\Controllers\MasterPswdController;
use App\Models\EmployeeDsr;
use App\Models\Task;
use App\Http\Controllers\ServerController;



use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\TaskManagmentController;
use App\Models\TaskManagment;

use App\Http\Controllers\CompanyController;
use App\Http\Controllers\DynamicForm;
use App\Http\Controllers\EmployeeFullReportController;
use App\Http\Controllers\InvoiceManagementController;
use App\Http\Controllers\PlatformController;
use App\Http\Controllers\ProjectTaskTimingController;
use App\Http\Controllers\ProjectTransactionController;
use App\Http\Controllers\LeaveController;
use App\Http\Controllers\NotificationUserController;
use App\Http\Controllers\TargetController;
use App\Models\Attendance;
use App\Models\Target;
use App\Http\Controllers\PerformanceController;
use App\Http\Controllers\QuickTaskTimingController;
use App\Http\Controllers\SeoController;

/*
|--------------------------------------------------------------------------
| Web Routes
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AdminController::class,'Login'])->middleware(["CustomAuthCheck"]);
Route::get('/login',[AdminController::class,'Login'])->middleware(["CustomAuthCheck"]);
Route::post('/login',[AdminController::class,'Login'])->middleware(["CustomAuthCheck"]);

Route::post('/punchin_time',[AdminController::class,'punchin_time'])->name('punchin');
Route::get('/agency',[AdminController::class,'AgencyLogin']);
Route::post('/agency',[AdminController::class,'AgencyLogin']);
Route::group(['prefix'=>'admin'], function() {
Route::middleware([
    'prefix'=>'AuthCheck'
])->group(function(){
          Route::get('/dashboard', function () {
            return view('admin.dashboard');
        });


        Route::get('/home', [AdminController::class,'home'])->name('home');
        Route::get('/profile', [AdminController::class,'adminProfile']);
        Route::get('/staff', [AdminController::class,'staffList']);
        Route::get('/revenue', [AdminController::class,'adminRevenue']);
        Route::get('/staffList/fetch_data', [AdminController::class,'fetchStaffList']);
        Route::post('/staff/toggleStatus',[AdminController::class,'toggleStaffStatus']);
        Route::get('/staffList/search',[AdminController::class,'fetchStaffList']);
        Route::post('/staff/add',[AdminController::class,'addStaff']);
        Route::post('/save/staff',[AdminController::class,'saveStaff'] );


        Route::post('/updateProfile', [AdminController::class,'updateProfile'] )->name('admin.updateProfile');

        // 04-10-2021 by Shivam - Routes for profile: change password and change pic
        Route::post('/profile/changePassword',[AdminController::class,'changePassword'])->name('users-change-password');
        Route::post('/profile/changeProfileImage',[AdminController::class,'changeProfileImage'])->name('users-change-image');



        Route::get('/dashboard', function () {
        return view('admin.dashboard');
    });



    Route::get('/dark', function () {
        return view('dashboard.dashboard_dark');
    });

    Route::get('/light', function () {
        return view('dashboard.dashboard_light');
    });

    Route::get('/custom', function () {
        return view('dashboard.dashboard_custom');
    });

    Route::get('/logout',[AdminController::class,'Logout']);


// Route::group(['middleware' => ['permission:User_Management']], function () {

    //******************************************Permissions*********************************************//

    Route::get('premission/create', [permissionsController::class,'create'])->name('premissions-create');
    Route::post('premission/store', [permissionsController::class,'store'])->name('premission-store');
    Route::get('premissions/edit/{id}', [permissionsController::class,'edit'])->name('premissions-edit');
    Route::get('premissions/destroy/{id}', [permissionsController::class,'destroy'])->name('premissions-destroy');
    Route::post('premissions/update/{id}', [permissionsController::class,'update'])->name('premissions-update');
    Route::get('premissions', [permissionsController::class,'index'])->name('premissions');
    // role
    Route::get('role/create', [RolesController::class,'create'])->name('roles-create');
    Route::post('role/store', [RolesController::class,'store'])->name('role-store');
    Route::get('roles/edit/{id}', [RolesController::class,'edit'])->name('roles-edit');
    Route::get('roles/destroy/{id}', [RolesController::class,'destroy'])->name('roles-destroy');
    Route::post('roles/update/{id}', [RolesController::class,'update'])->name('roles-update');
    Route::get('roles', [RolesController::class,'index'])->name('roles');

    // roles-permissions
    Route::get('roles-permissions/create', [RolesPermissionsController::class,'create'])->name('roles-permissions-create');
    Route::post('roles-permissions/store', [RolesPermissionsController::class,'store'])->name('roles-permissions-store');
    Route::get('roles-permissions/edit/{id}', [RolesPermissionsController::class,'edit'])->name('roles-permissions-edit');
    Route::get('roles-permissions/destroy/{id}', [RolesPermissionsController::class,'destroy'])->name('roles-permissions-destroy');
    Route::post('roles-permissions/update/{id}', [RolesPermissionsController::class,'update'])->name('roles-permissions-update');
    Route::get('roles-permissions', [RolesPermissionsController::class,'index'])->name('roles-permissions');
    // manage-users
    Route::get('manage-users/create', [UserController::class,'create'])->name('manage-users-create');
    Route::post('manage-users/store', [UserController::class,'store'])->name('manage-users-store');
    Route::get('manage-users/edit/{id}', [UserController::class,'edit'])->name('manage-users-edit');
    Route::get('manage-users/destroy/{id}', [UserController::class,'destroy'])->name('manage-users-destroy');
    Route::post('manage-users/update/{id}', [UserController::class,'update'])->name('manage-users-update');
    Route::get('manage-users', [UserController::class,'index'])->name('manage-users');

    // });
    /**********************************************Deals*************************************** */
    Route::get('/companydeal',[DealsController::class,'companydeal'])->name('deals')->middleware(['permission:deals_view']);
    Route::get('/companyDeal/search',[DealsController::class,'fetchCompanyDeal'])->name('deals-filter')->middleware(['permission:deals_view']);
    Route::get('/companyDeal/companyDealPage',[DealsController::class,'companyDealPage'])->name('deal-add')->middleware(['permission:deals_add']);
    Route::post('/deals/deals-change-status',[DealsController::class,'changeStatus'])->name('deals-change-status')->middleware(['permission:deals_change_status']);
    Route::get('/companyDeal/viewData/{id}',[DealsController::class,'viewData'])->name('companyDeal-view')->middleware(['permission:deals_view']);
    Route::post('/companyDeal/addcompanyDeal',[DealsController::class,'addcompanyDeal'])->name('deals-add-company')->middleware(['permission:deals_add']);
    Route::post('/companyDeal/add-Comment',[DealsController::class,'dealComment'])->name('deals-add-Comment');
    Route::get('/companyDeal/get-timeline',[DealsController::class,'getTimeline'])->name('deals-get-timeline');
    Route::post('/companyDeal/accept',[DealsController::class,'accept'])->name('deal-accept');
    Route::get('/companyDeal/updateData',[DealsController::class,'updateData'])->name('deal-edit');
    Route::post('/companyDeal/removeCompanyDealsData',[DealsController::class,'removeCompanyDealsData'])->name('deal-remove-data')->middleware(['permission:deals_delete']);
    Route::get('/companyDeal/getEmployeeList/',[DealsController::class,'getEmployeeListForAssign'])->name('get-deals-employee-list');
    Route::post('/companyDeal/assign-deal',[DealsController::class,'assignDeal'])->name('deals-assign')->middleware(['permission:deals_assign']);


    /**********************************************projects*************************************** */
    Route::get('/projects',[ProjectController::class,'index'])->name('projects')->middleware(['permission:projects_view']);
    Route::get('/projects/search',[ProjectController::class,'filter'])->name('projects-filter');
    Route::get('/projects/companyDealPage',[ProjectController::class,'add'])->name('projects-add')->middleware(['permission:projects_add']);
    Route::post('/projects/change-status',[ProjectController::class,'changeStatus'])->name('projects-change-status')->middleware(['permission:projects_change_status']);
    Route::get('/projects/viewData/{id}',[ProjectController::class,'viewData'])->name('projects-view')->middleware(['permission:projects_view']);
    Route::post('/projects/store',[ProjectController::class,'store'])->name('projects-store')->middleware(['permission:projects_add']);
    Route::post('/projects/add-Comment',[ProjectController::class,'dealComment'])->name('projects-Comment');
    Route::get('/projects/get-timeline',[ProjectController::class,'getTimeline'])->name('projects-get-timeline');
    Route::post('/projects/accept',[ProjectController::class,'accept'])->name('projects-accept');
    Route::get('/projects/updateData',[ProjectController::class,'updateData'])->name('projects-edit');
    Route::post('/projects/removeCompanyDealsData',[ProjectController::class,'removeCompanyDealsData'])->name('projects-remove-data');
    Route::get('/projects/getstatusreason',[ProjectController::class,'getStatusReason'])->name('get-status-reason');
    Route::post('/projects/addSubscription',[ProjectController::class,'addSubscription'])->name('add-subscription');
    Route::post('/subscriptions/addTeamMember',[ProjectController::class,'addTeamMember'])->name('add-team-member');
    Route::post('/subscriptions/removeTeamMember',[ProjectController::class,'removeTeamMember'])->name('remove-team-member');
    Route::get('/projects/viewTeamManagement',[ProjectController::class,'viewTeamManagement'])->name('view-team-management');
    Route::get('/projects/analysis/view/{type}',[AnalysisController::class,'index'])->name('project-analysis');
    Route::get('/projects/analysis/status',[AnalysisController::class,'filterComparisan'])->name('project-analysis-status-filter');
    Route::post('/projects/viewData',[ProjectController::class,'viewServerPswd'])->name('view-server-pswd');

    Route::get('/projects/sheets',[ ProjectController::class, 'renderProjectSheetsView' ] )
    ->name('project-sheets')->middleware(['permission:project_sheets_view']);

    Route::post('/projects/sheets/store',[ ProjectController::class, 'storeProjectSheet' ] )
    ->name('project-sheets-store')->middleware(['permission:project_sheets_create']);

    Route::get('/projects/sheets/edit',[ ProjectController::class, 'renderEditProjectSheetModal' ] )
    ->name('project-sheets-edit')->middleware(['permission:project_sheets_edit']);

    Route::post('/projects/sheets/update',[ ProjectController::class, 'updateProjectSheet' ] )
    ->name('project-sheets-update')->middleware(['permission:project_sheets_edit']);

    Route::post('/projects/sheets/delete',[ ProjectController::class, 'deleteProjectSheet' ] )
    ->name('project-sheets-delete')->middleware(['permission:project_sheets_delete']);

    Route::get('/servers',[ServerController::class,'renderProjectServersView'])->name('servers');

    Route::get('/projects/demos', [ ProjectController::class, 'renderProjectDemosView'] )
    ->name('project-demos')->middleware(['permission:project_demos_view']);

    Route::post('/projects/demos/store', [ ProjectController::class, 'storeProjectDemos' ] )
    ->name('project-demos-store')->middleware(['permission:project_demos_create']);

    Route::post('/projects/demos/delete',[ ProjectController::class, 'deleteProjectDemos'         ] )
    ->name('project-demos-delete')->middleware(['permission:project_demos_delete']);

    Route::get('/projects/demos/edit',   [ ProjectController::class, 'renderEditProjectDemoModal'] )
    ->name('project-demos-edit')->middleware(['permission:project_demos_edit']);

    Route::post('/projects/demos/update',[ ProjectController::class, 'updateProjectDemo'] )
    ->name('project-demos-update')->middleware(['permission:project_demos_edit']);


    Route::get('/projects/servers',[ ProjectController::class, 'renderProjectServersView' ] )
    ->name('project-servers')->middleware(['permission:project_servers_view']);

    Route::post('/projects/servers/store', [ ProjectController::class, 'storeProjectServer' ] )
    ->name('project-servers-store')->middleware(['permission:project_servers_create']);



    Route::post('/servers/store', [ ServerController::class, 'storeProjectServer' ] )
    ->name('project-servers-store')->middleware(['permission:project_servers_create']);

    Route::post('/projects/servers/delete',[ ProjectController::class, 'deleteProjectServer'   ] )
    ->name('project-servers-delete')->middleware(['permission:project_servers_delete']);

    Route::get('/projects/servers/edit',   [ ProjectController::class, 'renderEditProjectServerModal' ] )
    ->name('project-servers-edit')->middleware(['permission:project_servers_create']);

    Route::post('/projects/servers/update',[ ProjectController::class, 'updateProjectServer'] )
    ->name('project-servers-update');

    Route::get('/projects/addons',[ ProjectController::class, 'renderProjectAddonsView' ] )
    ->name('project-addons')->middleware(['permission:project_addons_view']);

    Route::post('/projects/addons/store', [ ProjectController::class, 'storeProjectAddon' ] )
    ->name('project-addons-store')->middleware(['permission:project_addons_create']);

    Route::get('/projects/addons/edit',   [ ProjectController::class, 'renderEditProjectAddonModal' ] )
    ->name('project-addons-edit')->middleware(['permission:project_addons_edit']);

    Route::post('/projects/addons/delete',[ ProjectController::class, 'deleteProjectAddon'   ] )
    ->name('project-addons-delete')->middleware(['permission:project_addons_delete']);

    Route::post('/projects/addons/changeStatus',[ ProjectController::class, 'changeAddonStatus'   ] )
    ->name('project-addons-change-status')->middleware(['permission:project_addons_change_status']);



    Route::post('/subscriptions/status',[ProjectController::class,'subscriptionsStatus'])->name('subscriptions-status');
    /**********************************************Attendence*************************************** */
    Route::get('/attendance',[AttendenceController::class,'index'])->name('attendence')->middleware(['permission:attendance_view']);
    Route::get('/attendance/search',[AttendenceController::class,'filter'])->name('attendance-filter');
    // Route::get('/attendence/search',[AttendenceController::class,'filter'])->name('attendence-filter')->middleware(['permission:attendance_view']);
    Route::get('/attendence/viewdetails',[AttendenceController::class,'viewDetails'])->name('attendence.viewdetails');


    /**********************************************Subscription*************************************** */
    Route::get('/subscriptions',[SubscriptionPaymentController::class,'index'])->name('subscriptions')->middleware(['permission:subscriptions_payment_accept']);
    Route::get('/subscriptions/viewProjectSubscription/{id}',[SubscriptionPaymentController::class,'viewProjectSubscription'])->name('view-project-subscription')->middleware(['permission:subscriptions_payment_accept']);
    Route::get('/subscriptions/getPaymentDetails',[SubscriptionPaymentController::class,'getPaymentDetails'])->name('get-payment-details')->middleware(['permission:subscriptions_payment_accept']);
    Route::post('/subscriptions/addSubscriptionPayment',[SubscriptionPaymentController::class,'addSubscriptionPayment'])->name('add-subscription-payment')->middleware(['permission:subscriptions_payment_accept']);
    Route::get('/subscriptions/filter',[SubscriptionPaymentController::class,'filter'])->name('subscription-filter')->middleware(['permission:subscriptions_payment_accept']);

    Route::get('/index/subscriptions/{id}',[SubscriptionPaymentController::class,'viewSubscriptionsDetail'])->name('subscriptions-detail');
    Route::get('/fiterSubscriptions/payment',[SubscriptionPaymentController::class,'fiterSubscriptions'])->name('subscription-fiterSubscriptions');

    Route::post('/subscriptions/cancel',[SubscriptionPaymentController::class,'cancelSubscription'])->name('cancel-subscriptions');
    /**********************************************Employees*************************************** */
    Route::get('/employees',[EmployeeController::class,'viewEmployeesData'])->middleware(['permission:employees_view'])->name('employees');
    Route::get('/employees/page',[EmployeeController::class,'paginationData'])->middleware(['permission:employees_view'])->name('employees-paginate');
    Route::get('/employees/search',[EmployeeController::class,'employeeSearch'])->middleware(['permission:employees_view'])->name('employees-paginate');
    Route::get('/employees/create',[EmployeeController::class,'employeesCreatePage'])->name('employees-create')->middleware(['permission:employees_add']);
    Route::post('/employees/store',[EmployeeController::class,'employeeStore'])->name('employee-store')->middleware(['permission:employees_add']);
    Route::get('/employees/edit/{id}',[EmployeeController::class,'employeesEditPage'])->name('employee-edit')->middleware(['permission:employees_edit']);
    Route::post('/employees/remove',[EmployeeController::class,'employeesRemove'])->name('employee-remove-data')->middleware(['permission:employees_delete']);
    Route::post('/employees/toggleLogin',[EmployeeController::class,'employeeLoginToggle'])->name('employee-login-toggle')->middleware(['permission:employees_view']);
    Route::get('/employees/paginate',[EmployeeController::class,'empPagination'])->name('employee-pagination')->middleware(['permission:employees_view']);
    Route::get('/employee/view/{id}',[EmployeeController::class,'employeesDetail'])->name('employee-detail');
    Route::post('/employees/store/moreinfo',[EmployeeController::class,'employeeStoreMoreInfo'])->name('employee-store-moreinfo');

    /**********************************************PortFolio*************************************** */
    Route::get('/portfolio',[PortfolioController::class,'viewPortfolioData'])->name('portfolio');
    Route::get('/portfolio/view/{id}',[PortfolioController::class,'viewDetail'])->name('portfolio-view');
    Route::get('/portfolio/page',[PortfolioController::class,'filter'])->name('portfolio-paginate');
    Route::get('/portfolio/create',[PortfolioController::class,'portfolioCreatePage'])->name('portfolio-create');
    Route::post('/portfolio/store',[PortfolioController::class,'portfolioStore'])->name('portfolio-store');
    Route::get('/portfolio/edit',[PortfolioController::class,'portfolioEditPage'])->name('portfolio-edit');
    Route::post('/portfolio/remove',[PortfolioController::class,'portfolioRemove'])->name('portfolio-remove-data');
    Route::post('/portfolio/toggleLogin',[PortfolioController::class,'portfolioLoginToggle'])->name('portfolio-login-toggle');
    Route::get('/portfolio/paginate',[PortfolioController::class,'portfolioPagination'])->name('portfolio-pagination');
    Route::get('/portfolio/tags',[PortfolioController::class,'portfolioTagsView'])->name('portfolio-tags-view');

    /**********************************************Candidates*************************************** */
    Route::get('/candidates',[CandidateController::class,'index'])->name('candidates');
    Route::get('/candidates/search',[CandidateController::class,'filter'])->name('candidates-filter');
    Route::get('/candidates/companyDealPage',[CandidateController::class,'add'])->name('candidates-add');
    Route::post('/candidates/change-status',[CandidateController::class,'changeStatus'])->name('candidates-change-status');
    Route::get('/candidates/viewData/{id}',[CandidateController::class,'viewData'])->name('candidates-view');
    Route::post('/candidates/store',[CandidateController::class,'store'])->name('candidates-store');
    Route::post('/candidates/add-Comment',[CandidateController::class,'addComment'])->name('candidates-Comment');
    Route::get('/candidates/get-timeline',[CandidateController::class,'getTimeline'])->name('candidates-get-timeline');
    Route::post('/candidates/accept',[CandidateController::class,'accept'])->name('candidates-accept');
    Route::get('/candidates/updateData',[CandidateController::class,'updateData'])->name('candidates-edit');
    Route::post('/candidates/removeCandidate',[CandidateController::class,'removeCandidate'])->name('candidates-remove-data');

    /**********************************************DSR*************************************** */
    Route::get('/dsr', [EmployeeDsrController::class, 'viewDsrData'])->name('employee-dsr');
    Route::get('/dsr/refresh', [EmployeeDsrController::class, 'refreshDsrData'])->name('employee-dsr-refresh');
    Route::post('/dsr/store',[EmployeeDsrController::class,'employeeDsrStore'])->name('employee-dsr-store');
    Route::post('/dsr/toggleReview',[EmployeeDsrController::class,'toggleReview'])->name('employee-dsr-review')->middleware(['permission:employee_dsr_review']);
    Route::get('/dsr/viewData/{id}',[EmployeeDsrController::class,'viewData'])->name('employee-dsr-view');
    Route::get('/dsr/search',[EmployeeDsrController::class,'dsrSearch'])->name('dsr-paginate');

    /********************************************** Departments *************************************** */
    Route::get('/departments', [ DepartmentController::class, 'viewDepartments'])
    ->name('departments')->middleware(['permission:departments_view']);

    Route::post('/departments/store', [ DepartmentController::class, 'storeDepartment'])
    ->name('store-department')->middleware(['permission:departments_create']);

    Route::get('/departments/edit', [ DepartmentController::class, 'editDepartment'])
    ->name('edit-department')->middleware(['permission:departments_edit']);

    Route::post('/departments/update', [ DepartmentController::class, 'updateDepartment'])
    ->name('update-department')->middleware(['permission:departments_edit']);

    Route::post('/departments/delete', [ DepartmentController::class, 'deleteDepartment'])
    ->name('delete-department')->middleware(['permission:departments_delete']);

    Route::get('/departments/search', [ DepartmentController::class, 'searchDepartments'])
    ->name('search-departments')->middleware(['permission:departments_view']);


    /********************************************** Companies *************************************** */

    Route::get('/companies', [ CompanyController::class, 'viewCompanies'])
    ->name('companies')->middleware(['permission:companies_view']);

    Route::get('/companies/search', [ CompanyController::class, 'searchCompanies'])
    ->name('search-companies')->middleware(['permission:companies_view']);

    Route::get('/companies/detail/{company_id}', [ CompanyController::class, 'viewCompanyDetails'])
    ->name('company-detail')->middleware(['permission:companies_view']);

    Route::post('/companies/store', [ CompanyController::class, 'storeCompany'])
    ->name('store-company')->middleware(['permission:companies_create']);

    Route::get('/companies/edit', [ CompanyController::class, 'editCompany'])
    ->name('edit-company')->middleware(['permission:companies_edit']);

    Route::post('/companies/update', [ CompanyController::class, 'updateCompany'])
    ->name('update-company')->middleware(['permission:companies_edit']);

    Route::post('/companies/delete', [ CompanyController::class, 'deleteCompany'])
    ->name('delete-company')->middleware(['permission:companies_delete']);

    /********************************************** Platforms *************************************** */

    Route::get('/platforms', [ PlatformController::class, 'viewPlatforms'])
    ->name('platforms')->middleware(['permission:platforms_view']);

    Route::get('/platforms/search', [ PlatformController::class, 'searchPlatforms'])
    ->name('search-platforms')->middleware(['permission:platforms_view']);

    Route::post('/platforms/store', [ PlatformController::class, 'storePlatform'])
    ->name('store-platform')->middleware(['permission:platforms_create']);

    Route::get('/platforms/edit', [ PlatformController::class, 'editPlatform'])
    ->name('edit-platform')->middleware(['permission:platforms_edit']);

    Route::post('/platforms/update', [ PlatformController::class, 'updatePlatform'])
    ->name('update-platform')->middleware(['permission:platforms_edit']);

    Route::post('/platforms/delete', [ PlatformController::class, 'deletePlatform'])
    ->name('delete-platform')->middleware(['permission:platforms_delete']);


    /**********************************************Report*************************************** */
    Route::get('/report', [ReportController::class, 'viewReportData'])->name('report');
    Route::get('/report/refresh', [ReportController::class, 'refreshReportData'])->name('report-refresh');
    Route::get('/report/view/{id}',[ReportController::class,'viewReportDetail'])->name('report-detail');
    Route::post('/report/store',[ReportController::class,'reportStore'])->name('report-store');
    Route::get('/report/edit', [ReportController::class, 'reportEdit'])->name('report-edit');
    Route::post('/report/remove', [ReportController::class, 'reportRemove'])->name('report-remove-data');
    Route::get('/report/search',[ReportController::class,'reportSearch'])->name('dsr-paginate');
    Route::post('/report/change-status',[ReportController::class,'changeStatus'])->name('report-change-status');
    /**********************************************DirectLeads*************************************** */
    Route::get('/directLeads', [DirectLeadController::class, 'viewDirectLeads'])->name('directLeads')->middleware(['permission:direct_leads_digital_view|direct_leads_development_view']);
    Route::get('/report/refresh', [ReportController::class, 'refreshReportData'])->name('report-refresh');
    Route::get('/direacLeads/add',[DealsController::class,'companyDealPage'])->name('directlead-deal-add');
    
    /**********************************************Tasks*************************************** */
    Route::get('/tasks/view/{id}',[TaskController::class,'index'])->name('tasks');
    Route::get('/tasks/search',[TaskController::class,'filter'])->name('tasks-filter');
    Route::get('/tasks/add',[TaskController::class,'add'])->name('tasks-add');
    Route::post('/tasks/store',[TaskController::class,'store'])->name('tasks-store');

    Route::post('/seoTasks/store',[TaskController::class,'seoStoreTask'])->name('seoTasks-store');


    Route::post('/tasks/removeTask',[TaskController::class,'removeTask'])->name('tasks-remove-data');
    Route::get('/tasks/updateData',[TaskController::class,'updateData'])->name('tasks-edit');
    Route::post('/tasks/change-status',[TaskController::class,'changeStatus'])->name('tasks-change-status');
    Route::get('/tasks/assignEmployee/',[TaskController::class,'getEmployeeList'])->name('get-employee-list');
    Route::post('/tasks/assignTask',[TaskController::class,'assignTask'])->name('tasks-assign-to-employee');
    Route::get('/tasks/detail/{id}',[TaskController::class,'viewDetails'])->name('task-detial-view');
    Route::post('/tasks/add-Comment',[TaskController::class,'taskComment'])->name('tasks-add-Comment');
    Route::get('/tasks/get-timeline',[TaskController::class,'getTimeline'])->name('tasks-get-timeline');

    /**********************************************TaskTimings*************************************** */
    Route::post('/tasks/timer/start',[ProjectTaskTimingController::class, 'startOrPause'])->name('task-timer-start');
    // Route::post('/tasks/timer/pause',[ProjectTaskTimingController::class, 'pause'])->name('task-timer-pause');


    /**********************************************TaskManagement*************************************** */
    Route::get('/manageTasks/view',[TaskManagmentController::class,'index'])->name('manageTasks')->middleware(['permission:task_management_view']);
    Route::get('/manageTasks/search',[TaskManagmentController::class,'filter'])->name('manageTasks-filter')->middleware(['permission:task_management_view']);
    Route::get('/manageTasks/add',[TaskManagmentController::class,'add'])->name('manageTasks-add')->middleware(['permission:task_management_add']);
    Route::post('/manageTasks/accept',[TaskManagmentController::class,'accept'])->name('manageTasks-accept');
    Route::post('/manageTasks/store',[TaskManagmentController::class,'store'])->name('manageTasks-store')->middleware(['permission:task_management_add']);
    Route::post('/manageTasks/removeTask',[TaskManagmentController::class,'removeTask'])->name('manageTasks-remove-data');
    Route::post('/manageTasks/resendTask',[TaskManagmentController::class,'resendTask'])->name('manageTasks-resendTask');
    Route::get('/manageTasks/updateData',[TaskManagmentController::class,'updateData'])->name('manageTasks-edit')->middleware(['permission:task_management_edit']);
    Route::post('/manageTasks/change-status',[TaskManagmentController::class,'changeStatus'])->name('manageTasks-change-status');
    Route::get('/manageTasks/detail/{id}',[TaskManagmentController::class,'viewDetails'])->name('manageTasks-detial-view')->middleware(['permission:task_management_view']);
    Route::get('/manageTasks/assignEmployee',[TaskManagmentController::class,'getEmployeeList'])->name('get-all-employee-list');
    Route::post('/manageTasks/assignTask',[TaskManagmentController::class,'assignTask'])->name('manageTasks-assign-to-employee');
    Route::post('/manageTasks/add-Comment',[TaskManagmentController::class,'taskComment'])->name('manageTasks-add-Comment');

    Route::post('/manageTasks/timer/start',[QuickTaskTimingController::class, 'startOrPause'])->name('quick-task-timer-start');

    /*****************************************workload********************************************/
    Route::get('/workload/view/{type}',[WorkLoadController::class,'index'])->name('workload');
    Route::get('/workload/search',[WorkLoadController::class,'filter'])->name('workload-filter');
    Route::get('/workload/employee/{id}',[WorkLoadController::class,'viewData'])->name('workload-employee-view');


    /*****************************************Todos********************************************/
    Route::get('/todo',[TodoController::class,'index'])->name('todo');
    Route::get('/todo/search',[TodoController::class,'search'])->name('search');
    Route::get('/todo/create',[TodoController::class,'todoCreatePage'])->name('todo-create');
    Route::post('/todo/store',[TodoController::class,'todoStore'])->name('todo-store');
    Route::get('/todo/edit',[TodoController::class,'todoEditPage'])->name('todo-edit');
    Route::get('/todo/detail',[TodoController::class,'todoDetail'])->name('todo-detail-view');
    Route::post('/todo/remove',[TodoController::class,'todoRemove'])->name('todo-remove-data');


    /*****************************************leaves********************************************/
    Route::get('/leaves',[LeaveController::class,'index'])->name('leaves');
    // ->middleware(['permission:leaves_view']);
    Route::get('/leave/search',[LeaveController::class,'search'])->name('search');
    // ->middleware(['permission:leaves_view']);
    Route::post('/leave/create',[LeaveController::class,'leaveStore'])->name('leave-store');
    Route::get('/leave/detail/view/{id}',[LeaveController::class,'leaveDetail'])->name('leave-detail-view')->middleware(['permission:leaves_view']);
    Route::post('/leave/toggleReview',[LeaveController::class,'toggleReview'])->name('leave-review')->middleware(['permission:leaves_approve_reject']);
    Route::get('/leave/edit',[LeaveController::class,'leaveEditPage'])->name('leave-edit')->middleware(['permission:leaves_edit']);
    Route::post('/leave/remove',[LeaveController::class,'leaveRemove'])->name('remove-leave')->middleware(['permission:leaves_delete']);
    Route::post('/leave/items/{id}',[LeaveController::class,'leaveItemRefresh'])->name('leave-item-refresh')->middleware(['permission:leaves_delete']);
    Route::post('/leave/add-Comment',[LeaveController::class,'leaveComment'])->name('leave-add-Comment');
    Route::get('/leave/get-timeline',[LeaveController::class,'getTimeline'])->name('leave-get-timeline');

    /*****************************************Announcements********************************************/
    Route::get('/announcements',[AnnouncementController::class,'index'])->name('announcements')->middleware(['permission:announcements_view']);
    Route::get('/announcements/search',[AnnouncementController::class,'filter'])->name('search')->middleware(['permission:announcements_view']);
    Route::get('/announcements/detail',[AnnouncementController::class,'announcementDetail'])->name('announcement-detail-view')->middleware(['permission:announcements_view']);
    Route::post('/announcements/store',[AnnouncementController::class,'announcementStore'])->name('announcement-store');
    Route::post('/announcements/toggle',[AnnouncementController::class,'announcementToggle'])->name('announcement-toggle')->middleware(['permission:announcements_toggle_status']);
    Route::get('/announcement/edit',[AnnouncementController::class,'announcementEditPage'])->name('announcement-edit')->middleware(['permission:announcements_edit']);
    Route::post('/announcement/remove',[AnnouncementController::class,'announcementRemove'])->name('remove-announcement')->middleware(['permission:announcements_delete']);

    /*****************************************MasterPswds********************************************/
    Route::get('/masterPswds',[MasterPswdController::class,'index'])->name('masterPswd')->middleware(['permission:master_password_view']);
    Route::get('/masterPswds/search',[MasterPswdController::class,'filter'])->name('search')->middleware(['permission:master_password_view']);
    Route::post('/masterPswds/store',[MasterPswdController::class,'masterPswdsStore'])->name('masterPswds-store');
    Route::get('/masterPswds/edit',[MasterPswdController::class,'masterPswdsEditPage'])->name('masterPswds-edit')->middleware(['permission:master_password_edit']);
    Route::post('/masterPswds/remove',[MasterPswdController::class,'masterPswdsRemove'])->name('remove-masterPswds')->middleware(['permission:master_password_delete']);
    Route::post('/masterPswds/toggle',[MasterPswdController::class,'masterPswdToggle'])->name('masterPswd-toggle')->middleware(['permission:master_password_toggle_status']);

    /*****************************************Training Session********************************************/
    Route::get('/training-sessions',[TrainingSessionController::class,'index'])->name('training-sessions')->middleware(['permission:training_session_view']);
    Route::get('/training-sessions/search',[TrainingSessionController::class,'filter'])->name('search')->middleware(['permission:training_session_view']);
    Route::get('/training-sessions/view/{id}',[TrainingSessionController::class,'viewTrainingSessionDetail'])->name('training-sessions-detail');
    Route::post('/training-sessions/store',[TrainingSessionController::class,'trainingSessionStore'])->name('training-sessions-store');
    Route::post('/training-sessions/toggle',[TrainingSessionController::class,'trainingSessionToggle'])->name('training-sessions-toggle')->middleware(['permission:training_session_toggle_status']);
    Route::get('/training-sessions/edit',[TrainingSessionController::class,'trainingSessionEditPage'])->name('training-sessions-edit')->middleware(['permission:training_session_edit']);
    Route::post('/training-sessions/remove',[TrainingSessionController::class,'trainingSessionRemove'])->name('remove-training-sessions')->middleware(['permission:training_session_delete']);

    /*****************************************tickets********************************************/
    Route::get('/tickets',[TicketController::class,'index'])->name('tickets')->middleware(['permission:tickets_view']);
    Route::get('/tickets/search',[TicketController::class,'search'])->name('search')->middleware(['permission:tickets_view']);
    Route::post('/tickets/create',[TicketController::class,'ticketStore'])->name('ticket-store')->middleware(['permission:tickets_create']);
    Route::get('/tickets/detail/view/{id}',[TicketController::class,'ticketDetail'])->name('ticket-detail-view')->middleware(['permission:tickets_view']);
    Route::get('/tickets/edit',[TicketController::class,'ticketEditPage'])->name('ticket-edit')->middleware(['permission:tickets_edit']);
    Route::post('/tickets/remove',[TicketController::class,'ticketRemove'])->name('remove-ticket')->middleware(['permission:tickets_delete']);


    /*****************************************Project Transctions********************************************/
    Route::get('/transactions/view/{id}',[ProjectTransactionController::class,'index'])->name('project-transaction')->middleware(['permission:project_invoices_view']);
    Route::get('/transactions/search',[ProjectTransactionController::class,'search'])->name('project-transaction-search');
    Route::get('/transaction/create/{id}',[ProjectTransactionController::class,'transationCreatePage'])->name('project-transaction-create');
    Route::post('/transaction/store',[ProjectTransactionController::class,'store'])->name('project-transaction-store');
    Route::get('/transaction/edit/{id}',[ProjectTransactionController::class,'transationUpdatePage'])->name('project-transaction-edit');
    Route::post('/transaction/update',[ProjectTransactionController::class,'udpate'])->name('project-transaction-update');
    Route::get('/transaction/invoice/download/{id}',[ProjectTransactionController::class,'invoice'])->name('project-transaction-invoice');

    // Route::get('/todo/edit',[TodoController::class,'todoEditPage'])->name('todo-edit');
    // Route::get('/todo/detail',[TodoController::class,'todoDetail'])->name('todo-detail-view');
    // Route::post('/todo/remove',[TodoController::class,'todoRemove'])->name('todo-remove-data');


    /**********************************************PortFolio*************************************** */
    Route::get('/targets',[TargetController::class,'index'])->name('targets')->middleware(['permission:targets_view']);
    Route::get('/targets/view/{id}',[TargetController::class,'viewDetail'])->name('targets-detail')->middleware(['permission:targets_view']);
    Route::get('/targets/search',[TargetController::class,'filter'])->name('targets-search')->middleware(['permission:targets_view']);
    Route::get('/targets/add',[TargetController::class,'add'])->name('targets-add')->middleware(['permission:targets_add']);
    Route::post('/targets/store',[TargetController::class,'store'])->name('targets-store')->middleware(['permission:targets_add']);
    Route::get('/targets/edit',[TargetController::class,'edit'])->name('targets-edit')->middleware(['permission:targets_edit']);
    Route::post('/targets/remove',[TargetController::class,'remove'])->name('targets-remove')->middleware(['permission:targets_delete']);


    /****************************************************Dynamic Form*********************************************************** */
    Route::get('/dynamicform/index',[DynamicForm::class,'index'])->name('dynamic-form-index');
    Route::get('/dynamicform/search',[DynamicForm::class,'filter'])->name('dynamicform-search');
    Route::get('/dynamicform/view/{id}',[DynamicForm::class,'viewDetail'])->name('dynamicform-detail');


    /**********************************************AllDsrs*************************************** */
    Route::get('/allDsrs',[EmployeeDsrController::class,'all'])->name('allDsrs');
    Route::get('/allDsrs/search',[EmployeeDsrController::class,'allSearch'])->name('allDsrs-search');
    Route::get('/allDsrs/employee/{id}',[EmployeeDsrController::class,'singeEmployeeDsrs'])->name('allDsrs-employee');


    /**********************************************AllDsrs*************************************** */
    Route::get('/full-report/view/{id}',[EmployeeFullReportController::class,'index'])->name('employee-full-report');
    Route::get('/full-report/search',[EmployeeFullReportController::class,'filter'])->name('employee-full-report-search');
    // Route::get('/allDsrs/employee/{id}',[EmployeeDsrController::class,'singeEmployeeDsrs'])->name('allDsrs-employee');



    /**********************************************Performance*************************************** */
    Route::get('/performance',[PerformanceController::class,'performanceIndex'])->name('performance');
    Route::get('/performance/search',[PerformanceController::class,'performanceFilter'])->name('performance-search');
    Route::get('/performance/view/{id}',[PerformanceController::class,'viewPerformanceDetail'])->name('performance-detail');

    /**********************************************SEO*************************************** */

    Route::get('/seo/index',[SeoController::class,'index'])->name('seo.index');
    Route::get('/seo/search',[SeoController::class,'seoFilter'])->name('seo-search');
    Route::get('/seo/add',[SeoController::class,'seoAdd'])->name('seo-add');
    Route::post('/seo/store',[SeoController::class,'seoStore'])->name('seo-store');
    Route::get('/seo/edit',[SeoController::class,'seoEdit'])->name('seo-edit');
    Route::get('/seo/view/{id}',[SeoController::class,'seoDetail'])->name('seo-detail');
    Route::post('/seo/remove',[SeoController::class,'seoRemove'])->name('seo-remove');


    /**********************************************Dynamic Forms *********************************/
    Route::get('/dynamic-form',[DynamicForm::class,'dynamicForms'])->name('dynamic-form');
    Route::post('/dynamic-form/add',[DynamicForm::class,'addForm'])->name('dynamic-form.add');

    /**********************************************Invoice Management *********************************/
    Route::get('/invoices/view/{type}', [InvoiceManagementController::class, 'index'])->name('invoices')->middleware(['permission:invoices_view']);
    // Sales

    Route::get('/invoices/sales/search',[InvoiceManagementController::class,'filterSales'])->name('invoices-sales-filter')->middleware(['permission:invoices_view']);

    // Purchase
    Route::get('/invoices/purchase/search',[InvoiceManagementController::class,'filterPurchase'])->name('invoices-purchase-filter')->middleware(['permission:invoices_view']);
    Route::get('/invoices/purchase/add',[InvoiceManagementController::class,'addPurchase'])->name('invoices-purchase-add')->middleware(['permission:invoices_purchase_create']);
    Route::get('/invoices/purchase/edit',[InvoiceManagementController::class,'editPurchase'])->name('invoices-purchase-edit')->middleware(['permission:invoices_purchase_update']);
    Route::post('/invoices/purchase/store',[InvoiceManagementController::class,'storePurchase'])->name('invoices-purchase-store')->middleware(['permission:invoices_purchase_create']);
    Route::post('/invoices/purchase/update',[InvoiceManagementController::class,'updatePurchase'])->name('invoices-purchase-update')->middleware(['permission:invoices_purchase_update']);
    Route::post('/invoices/purchase/change-status',[InvoiceManagementController::class,'changeStatusPurchase'])->name('invoices-purchase-change-status')->middleware(['permission:invoices_change_status']);
    Route::get('/invoices/detail/{id}',[InvoiceManagementController::class,'viewDetails'])->name('invoices-purchase-detial-view')->middleware(['permission:invoices_view']);

    /*****************************************directLeadLocal********************************************/
    Route::get('/directLeadLocal',[DirectLeadController::class,'index'])->name('DirectLeadLocal')->middleware(['permission:direct_lead_local_view']);
    Route::get('/directLeadLocal/search',[DirectLeadController::class,'search'])->name('search')->middleware(['permission:direct_lead_local_view']);


    /*****************************************NotificationUsers********************************************/
    Route::get('notification-users', [NotificationUserController::class,'index'])->name('notification-users')->middleware(['permission:notification_users_view']);
    Route::get('notification-users/create/{id}', [NotificationUserController::class,'create'])->name('notification-users-create')->middleware(['permission:notification_users_view']);
    Route::get('notification-users/edit/{id}', [NotificationUserController::class,'edit'])->name('notification-users-edit')->middleware(['permission:notification_users_view']);
    Route::post('notification-users/update', [NotificationUserController::class,'update'])->name('notification-users-update')->middleware(['permission:notification_users_view']);
    Route::post('notification-users/store', [NotificationUserController::class,'store'])->name('notification-users-store')->middleware(['permission:notification_users_view']);
    Route::get('notification-users/destroy/{id}', [RolesPermissionsController::class,'destroy'])->name('notification-users-destroy')->middleware(['permission:notification_users_view']);





    // ->middleware(['permission:leaves_view']);
    Route::get('/directLeadLocal/search',[DirectLeadController::class,'search'])->name('search');
    Route::get('/directLeadLocal/DirectLeadLocalDelete/{id}',[DirectLeadController::class,'delete'])->name('DirectLeadLocal-delete');
    // ->middleware(['permission:leaves_view']);

});
});
// Route::get('/form',[DynamicForm::class,'Form']);
// Route::get('/dynamic-form/form-view',[DynamicForm::class,'nutCrmView'])->name('dynamic-form.form-view');
Route::get('/test',function (){
    return view('admin.layout.includes.test');
});

