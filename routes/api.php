<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\DirectLeadController;
use App\Http\Controllers\DynamicForm;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('register',[UserController::class, 'register']);
Route::post('login',[UserController::class, 'login']);
Route::post('forgot-password',[UserController::class, 'forgotPassword']);
Route::post('change-password',[UserController::class, 'changePassword']);

//to show error when user not logged in --- used in middleware(Authenticate)
Route::get('login-check',[UserController::class, 'loginCheck'])->name('login');

Route::post('store-direct-leads', [DirectLeadController::class, 'store']);


Route::get('/form',[DynamicForm::class,'Form']);


Route::middleware('auth:sanctum')->group( function () {

});
Route::post('direct-leads', [DirectLeadController::class, 'storeDirectLead']);
