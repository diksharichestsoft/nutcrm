<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feed', function (Blueprint $table) {
            $table->id();
            $table->longText('title');
            $table->longText('comment');
            $table->string('ip_address');
	        $table->string('model_type');
	        $table->integer('deal_lost_reason_id');
            $table->integer('model_id');
            $table->integer('status')->default(1);
            $table->integer('type')->default(0);
            $table->foreignId('created_by')->constrained('users')->onDelete('cascade');
            $table->index([ 'model_type','model_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feed');
    }
}
