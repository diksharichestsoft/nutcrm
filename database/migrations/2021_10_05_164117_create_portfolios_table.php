<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortfoliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolios', function (Blueprint $table) {
            $table->id();
            $table->string('project_name');
            $table->integer('country')->nullable();
            $table->integer('department')->nullable();
            $table->string('web_url')->nullable();
            $table->string('ios_url')->nullable();
            $table->string('android_url')->nullable();
            $table->string('admin_url')->nullable();
            $table->longText('description')->nullable();
            $table->longText('business_model')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolios');
    }
}
