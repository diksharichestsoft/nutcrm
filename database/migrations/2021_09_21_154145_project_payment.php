<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProjectPayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('project_payments', function (Blueprint $table) {
            $table->id();
            $table->string('total')->nullable();
            $table->string('paid')->nullable();
            $table->string('pending')->nullable();
            $table->longText('desc')->nullable();
            $table->string('method')->nullable();
            $table->string('image')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->tinyInteger('type')->nullable();
            $table->string('cancel_reason')->nullable();
            $table->foreignId('project_id')->constrained('projects')->onDelete('cascade');
            $table->timestamps();
            $table->bigInteger('parent_id');
            $table->date('due_date');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('project_payments');
    }
}
