<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectServerDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_server_details', function (Blueprint $table) {
            $table->id();
            $table->string('hosting_url');
            $table->string('root_access');
            $table->string('hosting_id');
            $table->string('hosting_cpanel');
            $table->string('domain_purchased');
            $table->string('ftp_details');
            $table->string('php_my_admin');
            $table->string('public_private_key');
            $table->integer('user_id');
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_server_details');
    }
}
