<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->unique();
            $table->string('phone')->nullable();
            $table->string('profile_image')->nullable();
            $table->string('cover_image')->nullable();
            $table->string('address')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->integer('pincode')->nullable();
            $table->integer('age')->nullable();
            $table->integer('role_id')->default(0);
			$table->integer('is_notification')->default(1);
            $table->integer('status')->default(1);

            // New Columns
            $table->integer('company_id')->nullable();
            $table->integer('salary')->nullable();
            $table->integer('security_amount')->nullable();
            $table->string('employee_code')->nullable();
            $table->string('experience', 5)->nullable();
            $table->string('qualification')->nullable();
            $table->string('specialization')->nullable();
            $table->string('date_of_joining')->nullable();
            $table->string('date_of_birth')->nullable();
            $table->string('phone_number_2')->nullable();
            $table->string('phone_number_mother')->nullable();
            $table->string('phone_number_father')->nullable();
            $table->string('phone_number_sibling')->nullable();
            $table->string('monthly_target')->nullable();
            $table->string('designation')->nullable();
            $table->boolean('login_toggle')->default(1);
            $table->integer('team_lead')->nullable();
            $table->integer('department')->nullable();
            $table->string('description')->nullable();
            $table->string('slack_id')->nullable();
            $table->string('aadhar_card_front_image')->nullable();
            $table->string('aadhar_card_back_image')->nullable();
            $table->string('driver_license_image')->nullable();
            $table->string('pan_card_image')->nullable();
            $table->string('voter_id_image')->nullable();
            $table->string('passport_image')->nullable();
            $table->string('passport_back_image')->nullable();
            // \end new Columns

            $table->timestamp('contact_verified_at')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
