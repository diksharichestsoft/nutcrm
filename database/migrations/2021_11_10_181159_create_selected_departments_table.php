<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSelectedDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('selected_departments', function (Blueprint $table) {
            $table->id();
            $table->integer('model_id')->comment('Id of the model which have multiple departments');
            $table->integer('department_id');
            $table->integer('type')->comment('0 = project, 1= deal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('selected_departments');
    }
}
