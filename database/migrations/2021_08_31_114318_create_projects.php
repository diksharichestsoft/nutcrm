<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->longText('title');
            $table->string('client_name');
            $table->integer('project_type');
            $table->integer('referred_by')->nullable();
            $table->integer('platform');
            $table->string('platform_id')->nullable();
            $table->string('estimated_hours')->default(0);
            $table->longText('job_descriprion')->nullable();
            $table->string('url')->nullable();
            $table->string('client_email')->nullable();
            $table->string('client_phone')->nullable();
            $table->text('client_shipping_address')->nullable();
            $table->string('client_whatsapp_number')->nullable();
            $table->string('client_skype')->nullable();
            $table->string('client_slack_link')->nullable();
            $table->string('client_slack_username')->nullable();
            $table->string('client_slack_password')->nullable();
            $table->string('client_telegram_number')->nullable();
            $table->string('deal_status')->nullable()->default(1);
            $table->string('budget')->nullable();
            $table->timestamp('planned_start_date')->nullable();
            $table->timestamp('planned_end_date')->nullable();
            $table->timestamp('actual_start_date')->nullable();
            $table->timestamp('actual_end_date')->nullable();
            $table->integer('status')->default(1);
            $table->integer('subscription_status')->default(1);
            $table->integer('type')->default(0);
            $table->integer('company_id');
            $table->foreignId('created_by')->constrained('users')->onDelete('cascade');
            $table->integer('deal_id')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('accepted_by')->nullable();
            $table->string('meeting_time')->nullable();
            $table->integer('status_meeting_by_user_id')->nullable();
            $table->timestamps();
            $table->date('subscription_date')->nullable();
            $table->decimal('subscription_amount', $precision = 19, $scale = 4)->nullable();
            $table->string('subscription_payment_type')->nullable();
            $table->string('subscription_payment_method')->nullable();
            $table->mediumText('subscription_details')->nullable();
            $table->date('subscription_due_date')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
