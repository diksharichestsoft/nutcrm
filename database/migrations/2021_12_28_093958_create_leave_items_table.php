<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeaveItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_items', function (Blueprint $table) {
            $table->id();
            $table->string('leave_date');
            $table->integer('leave_type')->comment('1-short,2-half,3-full');
            $table->foreignId('leave_id')->constrained('leaves')->onDelete('cascade');
            $table->foreignId('created_by')->constrained('users')->onDelete('cascade');
            $table->string('status')->default(1);
            $table->string('type')->default(0);
            $table->foreignId('approved_by_user_id')->constrained('users')->onDelete('SET NULL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_items');
    }
}
