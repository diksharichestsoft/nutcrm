<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCandidateFeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidate_feeds', function (Blueprint $table) {
            $table->id();
            $table->string('title', 300);
            $table->longText('comment');
            $table->string('ip_address');
            $table->integer('candidate_id');
            $table->integer('status')->default(1);
            $table->integer('type')->default(0);
            $table->integer('branch_id');
            $table->integer('candidate_removed_reason_id')->nullable();
            $table->foreignId('created_by')->constrained('users')->onDelete('cascade');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidate_feeds');
    }
}
