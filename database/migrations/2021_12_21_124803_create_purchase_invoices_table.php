<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_invoices', function (Blueprint $table) {
            $table->id();
            $table->integer('created_by');
            $table->string('title')->nullable();
            $table->text('details')->nullable();
            $table->string('file')->nullable();
            $table->string('date')->nullable();
            $table->integer('company_id');
            $table->integer('warranty');
            $table->string('currency')->nullable();
            $table->double('total_amount', 10, 2)->nullable();
            $table->double('gst_amount', 10, 2)->nullable();
            $table->integer('type')->default(0);
            $table->integer('status')->default(1)->comment('1 = not checked, 2 = checked');
            $table->index(['created_by','company_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_invoices');
    }
}
