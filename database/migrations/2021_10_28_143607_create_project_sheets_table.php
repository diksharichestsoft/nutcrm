<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectSheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('project_sheets', function (Blueprint $table) {

		$table->id();
		$table->string('client_link')->nullable();
		$table->string('developer_link')->nullable();
		$table->string('sales_team_link')->nullable();
		$table->string('client_bugs_link')->nullable();
		$table->string('description', 5000 )->nullable();

		$table->integer('user_id');
		$table->integer('project_id');
		$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_sheets');
    }
}
