<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManageCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manage_candidates', function (Blueprint $table) {
            $table->id();
            $table->integer('hr_id');
            $table->integer('edited_by')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('current_salary')->nullable();
            $table->string('expected_salary')->nullable();
            $table->string('notice')->nullable();
            $table->string('total_experience')->nullable();
            $table->string('reference')->nullable();
            $table->string('current_company')->nullable();
            $table->string('department')->nullable();
            $table->string('sub_department')->nullable();
            $table->string('resume')->nullable();
            $table->integer('status')->default(1);
            $table->integer('candidate_status')->default(1);
            $table->boolean('favorite')->nullable();
            $table->string('time_schedule')->nullable();
            $table->string('joining_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manage_candidates');
    }
}
