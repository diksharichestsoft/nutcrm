<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deals', function (Blueprint $table) {
            $table->id();
            $table->longText('title');
            $table->string('client_name');
            $table->integer('project_type');
            $table->integer('platform');
            $table->string('platform_id')->nullable();
            $table->longText('job_descriprion')->nullable();
            $table->string('url')->nullable();
            $table->string('client_email')->nullable();
            $table->string('client_phone')->nullable();
            $table->string('contact_email')->nullable();
            $table->string('contact_phone')->nullable();
            $table->string('contact_description')->nullable();
            $table->string('deal_status')->default(1);
            $table->string('budget')->nullable();
            $table->integer('status')->default(1);
            $table->integer('type')->default(0);
            $table->string('meeting_time')->nullable();
            $table->integer('status_meeting_by_user_id')->nullable();
            $table->integer('company_id');
            $table->integer('updated_by')->nullable();
            $table->integer('accepted_by')->nullable();
            $table->foreignId('created_by')->constrained('users')->onDelete('cascade');
            $table->foreignId('assign_to')->nullable()->constrained('users')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deals');
    }
}
