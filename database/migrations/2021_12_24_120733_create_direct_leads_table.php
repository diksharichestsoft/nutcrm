<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDirectLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direct_leads', function (Blueprint $table) {
            $table->id();
            $table->integer('company_id')->default(4);
            $table->string('fname')->nullable();
            $table->string('website_url')->nullable();
            $table->string('phone')->nullable();
            $table->string('email');
            $table->string('budget')->nullable();
            $table->string('company_name')->nullable();
            $table->text('message')->nullable();
            $table->string('link')->nullable();
            $table->string('interested')->nullable();
            $table->string('skype_whatsapp')->nullable();
            $table->text('token');
            $table->string('status')->default(0);
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('ip')->nullable();
            $table->string('utm_campaign')->nullable();
            $table->string('utm_medium')->nullable();
            $table->string('utm_source')->nullable();
            $table->string('utm_term')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direct_leads');
    }
}
