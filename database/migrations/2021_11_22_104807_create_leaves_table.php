<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leaves', function (Blueprint $table) {
            $table->id();
            $table->string('subject');
            $table->longText('description')->nullable();
            $table->integer('status')->default(1)->comment('0-deleted,1-no action,2-approved,3-rejected');
            $table->string('leave_date')->nullable();
            $table->integer('leave_type')->comment('1-short,2-half,3-full')->nullable();
            $table->foreignId('created_by')->constrained('users')->onDelete('cascade');
            $table->foreignId('approved_by_user_id')->constrained('users')->onDelete('cascade');
            $table->index(['created_by']);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leaves');
    }
}
