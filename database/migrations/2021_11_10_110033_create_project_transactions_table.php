<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_transactions', function (Blueprint $table) {
            $table->id();
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->string('client_name')->nullable();
            $table->text('details')->nullable();
            $table->string('account')->nullable();
            $table->string('file')->nullable();
            $table->string('date')->nullable();
            $table->integer('company_id')->nullable();
            $table->string('from_address')->nullable();
            $table->string('bill_to_address')->nullable();
            $table->string('ship_to_address')->nullable();
            $table->string('currency')->nullable();
            $table->integer('tax_type_id')->nullable();
            $table->text('notes')->nullable();
            $table->text('terms')->nullable();
            $table->double('total_amount', 10, 2)->nullable()->comment("Amount after calculation of tax and discount");
            $table->double('total_tax_amount', 10, 2)->nullable()->comment("deducted after discount");
            $table->double('total_discount_amount', 10, 2)->nullable()->comment("might be multipe discounts form discounts table");
            $table->string('invoice_number');
            $table->string('discount_type')->nullable();
            $table->string('discount_value')->nullable();

            $table->index(['created_by', 'project_id']);
            $table->foreignId('project_id')->constrained('projects')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('project_transactions');
    }
}
