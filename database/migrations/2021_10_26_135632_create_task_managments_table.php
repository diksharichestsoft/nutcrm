<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaskManagmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_managments', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->longText('summary')->nullable();
            $table->longText('description')->nullable();
            $table->mediumText('file')->nullable();
            $table->string('time')->nullable();
            $table->integer('send_to')->nullable();
            $table->integer('project_id')->nullable();
            $table->integer('send_by')->nullable();
            $table->integer('task_status_id')->default(4);
            $table->integer('child_status_id')->default(1);
            $table->integer('task_type_id')->nullable();
            $table->string('total_timer_time')->nullable();
            $table->integer('priority_level')->nullable();
            $table->integer('priority_points')->nullable();
            $table->integer('active')->default(1);
            $table->string('start_date')->nullable();
            $table->string('end_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_managments');
    }
}
