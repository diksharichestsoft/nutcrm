<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectDemoServerDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_demo_server_details', function (Blueprint $table) {
		$table->id();
		$table->integer('project_id');
		$table->integer('user_id');
		$table->string('description', 10000)->nullable();
		$table->string('keys')->nullable();
		$table->integer('is_server_details')->default(0);
		$table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_demo_server_details');
    }
}
