<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectTransactionItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_transaction_items', function (Blueprint $table) {
            $table->id();
            $table->text('description')->nullable();
            $table->double('quantity', 10, 2)->nullable();
            $table->double('rate', 10, 2)->nullable();
            $table->double('amount', 10, 2)->nullable();
            $table->foreignId('project_transaction_id')->constrained('project_transactions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_transaction_items');
    }
}
