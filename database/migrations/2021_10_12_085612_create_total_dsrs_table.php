<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTotalDsrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('total_dsrs', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('total_time')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('description')->nullable();
            $table->string('date')->nullable();
            $table->integer('status')->default(0);
            $table->integer('approve_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('total_dsrs');
    }
}
