<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->longText('summary')->nullable();
            $table->longText('description')->nullable();
            $table->longText('file')->nullable();
            $table->string('time')->nullable();
            $table->string('quantity')->nullable();
            $table->integer('given_by');
            $table->integer('project_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('task_status_id')->default(1);
            $table->string('task_type_id')->default(0);
            $table->string('total_timer_time')->default(0);
            $table->integer('priority_level')->nullable();
            $table->integer('priority_points')->nullable();
            $table->integer('active')->default(1);
            $table->string('start_date')->nullable();
            $table->string('end_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
