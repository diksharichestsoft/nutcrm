<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaskFeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_feeds', function (Blueprint $table) {
            $table->id();
            $table->longText('title');
            $table->longText('comment');
            $table->string('ip_address');
            $table->integer('task_id')->nullable();
            $table->integer('status')->default(1);
            $table->integer('type')->default(0);
            $table->integer('branch_id')->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_feeds');
    }
}
