<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuickTaskTimingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quick_task_timings', function (Blueprint $table) {
            $table->id();
            $table->string('start_time');
            $table->string('stop_time');
            $table->integer('status')->default(0);
            $table->string('total_time')->default(0);
            $table->foreignId('task_managment_id')->constrained('task_managments')->onDelete('cascade');
            $table->foreignId('project_id')->constrained('projects')->onDelete('cascade');
            $table->foreignId('created_by')->constrained('users')->onDelete('cascade');
            $table->index(['created_by', 'task_managment_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quick_task_timings');
    }
}
