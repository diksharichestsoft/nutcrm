<?php

namespace Database\Seeders;

use App\Models\ProjectType;
use Illuminate\Database\Seeder;

class ProjectTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $projectType = array(
            [
                'id' => 1,
                'name'=>'Web'
                ],
                [
                'name'=>'Digital Marketing (SEO, PPC, SMM, ORM)'
                ],
                [
                'name'=>'Mobile app'
                ],
                [
                'name'=>'Web and Mobile app'
                ],
                [
                'name'=>'Others'
                ],

        );
        foreach ($projectType as $key => $value) {
            ProjectType::create($value);
        }
    }
}
