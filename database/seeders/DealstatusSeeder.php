<?php

namespace Database\Seeders;

use App\Models\DealStatus;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DealstatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = array(
            [
                'id' => 1,
                'dealname'=>'In Queue'
                ],
                [
                'dealname'=>'In Proccess'
                ],
                [
                'dealname'=>'Meeting'
                ],
                [
                'dealname'=>'FRD'
                ],
                [
                'dealname'=>'Hot Deals'
                ],
                [
                'dealname'=>'Won'
                ],
                [
                'dealname'=>'Lost'
                ],
                [
                'dealname'=>'Fake'
                ],
        );
        foreach ($status as $key => $value) {
            DealStatus::create($value);
        }
    }
}
