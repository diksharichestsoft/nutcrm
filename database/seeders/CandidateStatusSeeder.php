<?php

namespace Database\Seeders;

use App\Models\CandidateStatus;
use Illuminate\Database\Seeder;

class CandidateStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = array(
            [
                'id' => 1,
                'name'=>'In Queue'
                ],
                [
                'id' => 2,
                'name'=>'In Processing'
                ],
                [
                'id' => 3,
                'name'=>'Favorite'
                ],
                [
                'id' => 4,
                'name'=>'Line up'
                ],
                [
                'id' => 5,
                'name'=>'On hold'
                ],
                [
                'id' => 6,
                'name'=>'Up coming'
                ],
                [
                'id' => 7,
                'name'=>'Recruited'
                ],
                [
                'id' => 8,
                'name'=>'Interested'
                ],
                [
                'id' => 9,
                'name'=>'Need to try'
                ],
                [
                'id' => 10,
                'name'=>'Not looking for change'
                ],
                [
                'id' => 11,
                'name'=>'Do not pic call/accept the request'
                ],
                [
                'id' => 12,
                'name'=>'Changed his stream'
                ],
                [
                'id' => 13,
                'name'=>'Invalid number'
                ],
                [
                'id' => 15,
                'name'=>'Not suitable'
                ],
                [
                'id' => 16,
                'name'=>'Not hiring currently'
                ],
                [
                'id' => 17,
                'name'=>'Location issue'
                ],
        );
        foreach ($status as $key => $value) {
            CandidateStatus::create($value);
        }    }
}
