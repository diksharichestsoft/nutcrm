<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use \App\Models\DealLostReason;

class DealLostReasonsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $reasons = [ 
		    [ "title" => "High Cost/Budget Issue" ],
		    [ "title" => "Skill Set" ],
		    [ "title" => "We don't deal in required technology" ],
		    [ "title" => "Not relevant references of the project" ],
		    [ "title" => "Not relevant client references" ],
		    [ "title" => "Lack in Communication" ],
		    [ "title" => "Profile Feedback History" ],
		    [ "title" => "Preferred to work with individual" ],
		    [ "title" => "Other" ],
	    ];

	   $response =  DealLostReason::insert( $reasons );
    }
}
