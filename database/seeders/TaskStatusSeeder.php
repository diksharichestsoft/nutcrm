<?php

namespace Database\Seeders;

use App\Models\TaskStatus;
use Illuminate\Database\Seeder;

class TaskStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $taskStatus = array(
            [
                'id' => 1,
                'title'=>'New tasks'
                ],
                [
                'title'=>'To Do'
                ],
                [
                'title'=>'In progress'
                ],
                [
                'title'=>'In Testing'
                ],
                [
                'title'=>'Bug'
                ],
                [
                'title'=>'Bug Resloved'
                ],
                [
                'title'=>'Bug Closed'
                ],
                [
                'title'=>'Final Complete'
                ],
                [
                'title'=>'Paused'
                ],

        );
        foreach ($taskStatus as $key => $value) {
            TaskStatus::create($value);

        }
    }
}
