<?php

namespace Database\Seeders;

use App\Models\Company;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = array(
            [
                'id' => 1,
                'name'=>'Richestsoft'
                ],
                [
                'id' => 2,
                'name'=>'IndeedSEO'
                ],
                [
                'id' => 3,
                'name'=>'SeoCurrent'
                ],
                [
                'name'=>'AppsMinder'
                ],
                [
                'name'=>'TopxListing'
                ],
                [
                'name'=>'VisaFirms'
                ],
                [
                'name'=>'Richest Technologies Private Limited'
                ],
                [
                'name'=>'Another'
                ],
        );
        foreach ($company as $key => $value) {
            Company::create($value);
        }
    }
}
