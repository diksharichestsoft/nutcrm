<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $country = array(
            [
                'name'=>'India'
                ],
                [
                'name'=>'USA'
                ],
                [
                'name'=>'UK'
                ],
                [
                'name'=>'Australia'
                ],
                [
                'name'=>'Canada'
                ],
                [
                'name'=>'UAE'
                ],
                [
                'name'=>'Singapore'
                ],
                [
                'name'=>'Global'
                ],
        );
        foreach ($country as $key => $value) {
            Country::create($value);
        }
    }
}
