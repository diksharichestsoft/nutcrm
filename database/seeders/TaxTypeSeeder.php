<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use \App\Models\TaxType;

class TaxTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $tax_types = [
		    ['name' => 'nogst', 'value' => 0 ],
		    ['name' => 'cgst', 'value' => 9 ],
		    ['name' => 'igst', 'value' => 9 ],
		    ['name' => 'international', 'value' => 0 ],

	    ];

	    TaxType::insert( $tax_types );

    }
}
