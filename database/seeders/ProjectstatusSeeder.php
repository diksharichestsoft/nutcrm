<?php

namespace Database\Seeders;

use App\Models\ProjectStatus;
use Illuminate\Database\Seeder;

class ProjectstatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = array(
            [
                'id' => 1,
                'dealname'=>'In Queue'
                ],
                [
                'dealname'=>'In Proccess'
                ],
                [
                'dealname'=>'Meeting'
                ],
                [
                'dealname'=>'FRD'
                ],
                [
                'dealname'=>'Hot Deals'
                ],
                [
                'dealname'=>'Won'
                ],
                [
                'dealname'=>'Lost'
                ],
                [
                'dealname'=>'Fake'
                ],
        );
        foreach ($status as $key => $value) {
            ProjectStatus::create($value);
        }
    }
}
