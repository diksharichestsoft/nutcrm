<?php

namespace Database\Seeders;

use App\Models\TaskType;
use Illuminate\Database\Seeder;

class TaskTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $taskTypes = array(
            [
                'id' => 1,
                'title'=>'Dev Task'
                ],
                [
                'title'=>'Bug'
                ],
                [
                'title'=>'SEO Task'
                ],
                [
                'title'=>'SMM Task'
                ],
                [
                'title'=>'Other'
                ],


        );
        foreach ($taskTypes as $key => $value) {
            TaskType::create($value);

        }
    }
}
