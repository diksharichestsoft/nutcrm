<?php

namespace Database\Seeders;

use App\Models\DealPlatform;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(AdminSeeder::class);
        $this->call(CompanySeeder::class);
        $this->call(DealPlatformSeeder::class);
        $this->call(DealstatusSeeder::class);
        $this->call(ProjectClosedReasonsSeeder::class);
        $this->call(ProjectstatusSeeder::class);
        $this->call(ProjectTypeSeeder::class);
        $this->call(DepartmentSeeder::class);
        $this->call(CandidateStatusSeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(TaskStatusSeeder::class);
        $this->call(TaskTypeSeeder::class);
        $this->call(DealLostReasonsSeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(PermissionRolesSeeder::class);
        $this->call(SeoTaskTypeSeeder::class);



    }
}
