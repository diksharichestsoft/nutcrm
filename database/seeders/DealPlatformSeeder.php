<?php

namespace Database\Seeders;

use App\Models\DealPlatform;
use Illuminate\Database\Seeder;

class DealPlatformSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $platform = array(
            [
                'id' => 1,
                'name'=>'Up-work'
                ],
                [
                'name'=>'Direct'
                ],
                [
                'name'=>'Fiverr'
                ],
                [
                'name'=>'PPH'
                ],
                [
                'name'=>'Old Client'
                ],
                [
                'name'=>'Up-Sale'
                ],

        );
        foreach ($platform as $key => $value) {
            DealPlatform::create($value);
        }
    }
}
