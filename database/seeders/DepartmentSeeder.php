<?php

namespace Database\Seeders;

use App\Models\Department;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departments = array(
            [
            'name'=>'Php Developer'
            ],
            [
            'name'=>'React JS Developer'
            ],
            [
            'name'=>'Android Developer'
            ],
            [
            'name'=>'IOS Developer'
            ],
            [
            'name'=>'SEO'
            ],
            [
            'name'=>'Human Resource'
            ],
            [
            'name'=>'React Native Developer'
            ],
            [
            'name'=>'Business Developer'
            ],
            [
            'name'=>'Project Management'
            ],
            [
            'name'=>'Content Writer'
            ],
            [
            'name'=>'Flutter Developer'
            ],
            [
            'name'=>'Graphic Designer'
            ],
            [
            'name'=>'Web Designer'
            ],
            [
            'name'=>'Linux Server Admin'
            ],
            [
            'name'=>'PPC'
            ],
            [
            'name'=>'Project'
            ],
            [
            'name'=>'Software Tester'
            ],
            [
            'name'=>'Social Media Marketing'
            ],
            [
            'name'=>'Digital Marketing'
            ],
            [
            'name'=>'BDE'
            ],
            [
            'name'=>'Telecaller/Counselor'
            ],
            [
            'name'=>'Quality Analyst BPO'
            ],
            [
            'name'=>'Business Analyst'
            ],

        );
        foreach ($departments as $key => $value) {
            Department::create($value);
        }
    }
}
