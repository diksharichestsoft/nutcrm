<?php

namespace Database\Seeders;

use App\Models\TaskManagementStatus;
use Illuminate\Database\Seeder;

class TaskManagementStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $taskStatus = array(
            [
                'id' => 1,
                'title'=>'In Queue'
                ],
                [
                'title'=>'In process '
                ],
                [
                'title'=>'Completed'
                ],
                [
                'title'=>'Assigned by me'
                ],
                [
                'title'=>'Completed by another user '
                ]
        );
        foreach ($taskStatus as $key => $value) {
            TaskManagementStatus::create($value);

        }
    }

}
