<?php

namespace Database\Seeders;

use App\Models\User;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Database\Seeder;

class PermissionRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Clear chached permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // All permissions to create
        $permissions = [
            "user_management",
            "employees_view",
            "employees_edit",
            "employees_delete",
            "employees_disable",
            "attendance_view",
            "deals_view",
            "deals_edit",
            "deals_delete",
            "deals_all_leads_tab",
            "deal_details_clientinfo_hide",
            "projects_view",
            "projects_edit",
            "projects_delete",
            "projects_add",
            "deals_add",
            "employees_add",
            "subscriptions_view",
            "subscriptions_payment_accept",
            "deals_accept",
            "deals_assign",
            "projects_accept",
            "deals_digital_marketing",
            "projects_digital_marketing",
            "deals_development",
            "projects_development",
            "projects_assigned",
            "candidate_view",
            "portfolio_view",
            "deals_comment",
            "projects_all_leads_tab",
            "projects_comment",
            "candidate_edit",
            "candidate_delete",
            "portfolio_edit",
            "portfolio_delete",
            "projects_add_team",
            "projects_remove_team",
            "candidate_comment",
            "candidate_add",
            "portfolio_add",
            "candidate_accept",
            "projects_Show_Client_data",
            "projects_show_sales_tab",
            "projects_show_subscription_tab",
            "deals_all_leads_of_any_user",
            "projects_all_leads_of_any_user",
            "candidate_all_candidate_tab",
            "candidate_all_candidate_of_any_user",
            "employee_dsr_review",
            "employee_dsr_view",
            "all_dsr_view",
            "deals_change_status",
            "projects_change_status",
            "all_report_view",
            "direct_leads_development_view",
            "direct_leads_digital_view",
            "direct_leads_delete",
            "direct_leads_add",
            "employee_development_dsr_review",
            "employee_digital_marketing_dsr_review",
            "all_development_dsr_view",
            "all_digital_marketing_dsr_view",
            "tasks_view",
            "tasks_assign",
            "tasks_delete",
            "tasks_edit",
            "tasks_comment",
            "tasks_change_status",
            "workload_view",
            "all_user_all_task_tab",
            "departments_view",
            "departments_edit",
            "departments_delete",
            "departments_create",
            "task_management_view",
            "task_management_add",
            "task_management_edit",
            "companies_view",
            "companies_edit",
            "companies_delete",
            "companies_create",
            "platforms_create",
            "platforms_view",
            "platforms_edit",
            "platforms_delete",
            "project_sheets_create",
            "project_sheets_edit",
            "project_sheets_delete",
            "project_sheets_view",
            "project_demos_view",
            "project_demos_create",
            "project_demos_edit",
            "project_demos_delete",
            "project_servers_view",
            "project_servers_create",
            "project_servers_edit",
            "projects_budget_view",
            "targets_view",
            "targets_add",
            "targets_edit",
            "targets_delete",
            "tickets_create",
            "tickets_edit",
            "tickets_delete",
            "tickets_view",
            "all_user_tickets",
            "training_session_view",
            "training_session_add",
            "training_session_edit",
            "training_session_delete",
            "training_session_toggle_status",
            "projects_ordered_by_updates",
            "announcements_add",
            "announcements_view",
            "announcements_edit",
            "announcements_delete",
            "announcements_toggle_status",
            "leaves_view",
            "leaves_approve_reject",
            "leaves_add",
            "leaves_edit",
            "leaves_delete",
            "all_user_leaves",
            "master_password_view",
            "master_password_add",
            "master_password_edit",
            "master_password_delete",
            "master_password_toggle_status",
            "seo_tasks_view",
            "seo_tasks_add",
            "seo_tasks_edit",
            "seo_tasks_remove",
            "project_servers_delete",
            "project_addons_view",
            "project_addons_create",
            "project_addons_edit",
            "project_addons_delete",
            "project_addons_change_status",
            "can_add_dsr_item",
            "performance_view",
            "task_management_delete",
            "all_user_targets",
            'deal_details_clientinfo_hide',
            'only_appsminder_deals',
            'only_assigned_deals_toggle',
            'invoices_view',
            'invoices_purchase_create',
            'invoices_change_status',
        ];
        $roles = [
            "admin",
            "HR",
            "Project_coordinator",
            "BDE",
            "HR_head",
            "Employee_Digital_Marketing",
            "Employee_Development",
            "BDE_development",
            "BDE_digital_marketing",
            "BDM_development",
            "BDM_digital_marketing",
            "BDM",
            "Project_coordinator_digital_marketing",
            "Project_coordinator_development",
            "BDE_Visafirms",
            "temporary_for_arvind_sir",
            "QA_development",
        ];
        $permissions = collect($permissions)->map(function ($permission) {
            return ['name'=>$permission, 'guard_name' => 'web'];
        });
        $roles = collect($roles)->map(function ($permission) {
            return ['name'=>$permission, 'guard_name' => 'web'];
        });

        $createPerimssions = Permission::insert($permissions->toArray());
        $createRoles = Role::insert($roles->toArray());

        $adminUser = User::find(1);
        $adminRole = Role::find(1);
        $adminUser->assignRole($adminRole);
        $adminRole->givePermissionTo(Permission::where('name', '!=', 'only_appsminder_deals' )->get());
    }
}
