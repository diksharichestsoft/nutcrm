<?php

namespace Database\Seeders;

use App\Models\SeoTaskType;
use Illuminate\Database\Seeder;

class SeoTaskTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seoTaskTypes = array(
            [
                'id' => 1,
                'title'=>'SEO'
                ],
                [
                'title'=>'SMM'
                ],
                [
                'title'=>'PPC'
                ]
        );
        foreach ($seoTaskTypes as $key => $value) {
            SeoTaskType::create($value);
        }

    }
}
