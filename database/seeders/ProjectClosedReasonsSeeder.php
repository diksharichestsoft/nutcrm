<?php

namespace Database\Seeders;

use App\Models\ProjectsClosedReason;
use Illuminate\Database\Seeder;

class ProjectClosedReasonsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reasons = array(
            [
                'id' => 1,
                'name'=>'Ranking Issues'
                ],
                [
                'name'=>'Communication Issues'
                ],
                [
                'name'=>'OnPage Issues'
                ],
                [
                'name'=>'Blog content Issues'
                ],
                [
                'name'=>'Off Page content Issues'
                ],
                [
                'name'=>'Site Optimization'
                ],
                [
                'name'=>'Traffic Issues'
                ],
                [
                'name'=>'Sales Issue'
                ],
                [
                'name'=>'Quality Backlinks Issues'
                ],
                [
                'name'=>'Off page profile images issues'
                ],
                [
                'name'=>'Quality of whole SEO'
                ],
                [
                'name'=>'Budget'
                ],
                [
                'name'=>'Development Side Issues'
                ],
                [
                'name'=>"New add on's"
                ],
                [
                'name'=>"Changes"
                ],
        );
        foreach ($reasons as $key => $value) {
            ProjectsClosedReason::create($value);
        }
    }
}
