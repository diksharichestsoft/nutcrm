// var base_url=$('meta[name="base-url"]').attr('content');
function deals()
{
  $.ajax({
    url:base_url + "/admin/deals/dealsPage",
    type:"get",
    success:function(data)
    {
      $('#card_head').hide();
     $('#view').empty().html(data);
      $('select').select2();
    }
  })
}
function submit(){
    var data = new FormData();
      
      data.append('title',document.querySelector("#title_test").value);
      data.append('company',document.querySelector("#company").value);
      data.append('reffer',document.querySelector("#reffer").value);
      data.append('estimated_hour',document.querySelector("#estimated_hour").value);
      data.append('url',document.querySelector("#url").value);
      data.append('price',document.querySelector("#price").value);
      data.append('platform',document.querySelector("#platform").value);
      data.append('department',document.querySelector("#department").value);
      // data.append('upwork_id',document.querySelector("#upwork_id").value);
      data.append('client_name',document.querySelector("#client_name").value);
      data.append('client_email',document.querySelector("#client_email").value);
      data.append('client_phone',document.querySelector("#client_phone").value);
      data.append('start_date',document.querySelector("#start_date").value);
      data.append('end_date',document.querySelector("#end_date").value);
      data.append('actual_start_date',document.querySelector("#actual_start_date").value);
      data.append('actual_end_date',document.querySelector("#actual_end_date").value);
      data.append('job_desc',document.querySelector("#job_desc").value);
      data.append('_token',$('meta[name="csrf-token"]').attr('content'));
      $.ajax({
          type:'post',
          url:base_url+"/admin/deals/addProject",
          dataType: "JSON",       
         xhr: function() {
               myXhr = $.ajaxSettings.xhr();
               return myXhr;
         },
         cache: false,
         contentType: false,
         processData: false,
          data:data,
          success:function(data){
              location.reload();
          },
          error:function(data){
              errormessage('error_title',data.responseJSON.errors.title);
              errormessage('error_company',data.responseJSON.errors.company);
              errormessage('error_refer',data.responseJSON.errors.reffer);
              errormessage('error_estimated_hour',data.responseJSON.errors.estimated_hour);
              errormessage('error_url',data.responseJSON.errors.url);
              errormessage('error_price',data.responseJSON.errors.price);
              errormessage('error_platform',data.responseJSON.errors.platform);
              errormessage('error_department',data.responseJSON.errors.department);
              errormessage('error_client_name',data.responseJSON.errors.client_name);
              errormessage('error_client_email',data.responseJSON.errors.client_email);
              errormessage('error_client_phone',data.responseJSON.errors.client_phone);
              errormessage('error_job',data.responseJSON.errors.job_desc);
          }
        });
  } 
//   function viewProject(e)
// {
//   var id=e.getAttribute('data-id');
//   $.ajax({
//     url:base_url+"/admin/deals/viewData",
//     type:"get",
//     data:{id:id},
//     success:function(data)
//     {
//       $('#card_head').hide();
//       $('#view').empty().html(data);
//     }
//   })
// }
// function update(e)
// {
//   var id=e.getAttribute('data-id');
//   $.ajax({
//     url:base_url+"/admin/deals/updateData",
//     type:"get",
//     data:{id:id},
//     success:function(data)
//     {
//       $('#card_head').hide();
//       $('#view').empty().html(data);
//     }
//   })
// }
// function remove(e)
// {
//   var id=e.getAttribute('data-id');
//   swal({
//   title: "Oops....",
//   text: "Are You Sure You want to delete!",
//   icon: "error",
//   buttons: [
//     'NO',
//     'YES'
//   ],
// }).then(function(isConfirm) {
//   if (isConfirm) {
//     $.ajax({
//       url:base_url+"/admin/deals/removeData",
//       type:"post",
//       data:{id:id},
//       headers: {
//           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//           },
//       success:function(data){
//         location.reload();
//       }
//     })
//   } else {
    
//   }
// });
// }
// function updateProject()
// {
//   var data = new FormData();
//     data.append('id',document.querySelector("#update_id").value);
//     data.append('title',document.querySelector("#utitle_test").value);
//     data.append('company',document.querySelector("#ucompany").value);
//     data.append('reffer',document.querySelector("#ureffer").value);
//     data.append('estimated_hour',document.querySelector("#uestimated_hour").value);
//     data.append('url',document.querySelector("#uurl").value);
//     data.append('price',document.querySelector("#uprice").value);
//     data.append('platform',document.querySelector("#uplatform").value);
//     data.append('department',document.querySelector("#udepartment").value);
//     data.append('client_name',document.querySelector("#uclient_name").value);
//     data.append('client_email',document.querySelector("#uclient_email").value);
//     data.append('client_phone',document.querySelector("#uclient_phone").value);
//     data.append('start_date',document.querySelector("#ustart_date").value);
//     data.append('end_date',document.querySelector("#uend_date").value);
//     data.append('actual_start_date',document.querySelector("#uactual_start_date").value);
//     data.append('actual_end_date',document.querySelector("#uactual_end_date").value);
//     data.append('job_desc',document.querySelector("#ujob_desc").value);
//     data.append('_token',$('meta[name="csrf-token"]').attr('content'));
//     $.ajax({
//         type:'post',
//         url:base_url+"/admin/deals/UpdateProjectData",
//         dataType: "JSON",       
//        xhr: function() {
//              myXhr = $.ajaxSettings.xhr();
//              return myXhr;
//        },
//        cache: false,
//        contentType: false,
//        processData: false,
//         data:data,
//         success:function(data){
//           // console.log(data);
//             location.reload();
//         },
//         error:function(data){
//             errormessage('error_title',data.responseJSON.errors.title);
//             errormessage('error_company',data.responseJSON.errors.company);
//             errormessage('error_refer',data.responseJSON.errors.reffer);
//             errormessage('error_estimated_hour',data.responseJSON.errors.estimated_hour);
//             errormessage('error_url',data.responseJSON.errors.url);
//             errormessage('error_price',data.responseJSON.errors.price);
//             errormessage('error_platform',data.responseJSON.errors.platform);
//             errormessage('error_department',data.responseJSON.errors.department);
//             errormessage('error_client_name',data.responseJSON.errors.client_name);
//             errormessage('error_client_email',data.responseJSON.errors.client_email);
//             errormessage('error_client_phone',data.responseJSON.errors.client_phone);
//             errormessage('error_job',data.responseJSON.errors.job_desc);
//         }
//       });
// }
// function accept(e)
// {
//   var id=e.getAttribute('data-id');
//   $.ajax({
//     url:base_url+"/admin/deals/accept",
//     type:"post",
//     data:{id:id},
//     headers: {
//           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//           },
//     success:function(data)
//     {
//       location.reload();
//     }
//   })
// }
// function inproccess(e)
// {
//   alert('Hii');
//   var inproccess=e.getAttribute('data-id');
//   if(inproccess==2)
//   {
//         $.ajax({
//           url:base_url+"/admin/deals/inproccess",
//           type:"get",
//           success:function(data)
//           {
//             $('#view').empty().html(data);
//           }
//         })
//   }
// }
// function inquee(e)
// {
//   var inproccess=e.getAttribute('data-id');
//   if(inproccess==1)
//   {
//         $.ajax({
//           url:base_url+"/admin/deals/inquee",
//           type:"get",
//           success:function(data)
//           {
//             $('#view').empty().html(data);
//           }
//         })
//   }
// }
// function meeting(e)
// {
//     var meeting=e.getAttribute('data-id');
//     if(meeting==3)
//     {
//           $.ajax({
//             url:base_url+"/admin/deals/meeting",
//             type:"get",
//             success:function(data)
//             {
//               $('#view').empty().html(data);
//             }
//           })
//     }
// }


// inproccess



// $('.deals_status').change(function(){
//     var status = $(this).val();
//     var candidate_id = $(this).find(':selected').attr('data-id');
//       if(candidate_id==1)
//       {
//         $('#exampleModal').modal('show');
//        save_data(status,candidate_id);
//       }
//       else if(candidate_id==2)
//       {
//         $('#exampleModal1').modal('show');
//         save_data1(status,candidate_id);
//       }
//       else if(candidate_id==3)
//       {
//         $('#exampleModal2').modal('show');
//         save_data2(status,candidate_id);
//       }
//       else if(candidate_id==4)
//       {
//         $('#exampleModal3').modal('show');
//         save_data3(status,candidate_id);
//       }
//       else if(candidate_id==5)
//       {
//         $('#exampleModal4').modal('show');
//         save_data4(status,candidate_id);
//       }
//       else if(candidate_id==6)
//       {
//         $('#exampleModal5').modal('show');
//         save_data5(status,candidate_id);
//       }
//   })
//   function save_data(status,candidate_id){
//   $('#save_deal').click(function(){
//       let comment=$('#inproc').val();
      
//             $.ajax({
//                url:base_url+"/admin/deals/saveInproccess",
//                type:"post",
//                data:{comment:comment,status:status,candidate_id:candidate_id},
//                headers: {
//       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//       },
//                success:function(data){
//                    location.reload();
//                }
//             });
//         })
//     }
//     function save_data1(status,candidate_id){
//        $('#save_deal1').click(function(){
//           let comment=$('#inproc1').val();
          
//                 $.ajax({
//                    url:base_url+"/admin/deals/saveInproccess",
//                    type:"post",
//                    data:{comment:comment,status:status,candidate_id:candidate_id},
//                    headers: {
//           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//           },
//                    success:function(data){
//                        location.reload();
//                    }
//                 });
//             })
//         }
//         function save_data2(status,candidate_id){
      
//       $('#save_deal2').click(function(){
//           let comment=$('#inproc2').val();
          
//                 $.ajax({
//                    url:base_url+"/admin/deals/saveInproccess",
//                    type:"post",
//                    data:{comment:comment,status:status,candidate_id:candidate_id},
//                    headers: {
//           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//           },
//                    success:function(data){
//                        location.reload();
//                    }
//                 });
//             })
//         }
//         function save_data3(status,candidate_id){
      
//       $('#save_deal3').click(function(){
//           let comment=$('#inproc3').val();
          
//                 $.ajax({
//                    url:base_url+"/admin/deals/saveInproccess",
//                    type:"post",
//                    data:{comment:comment,status:status,candidate_id:candidate_id},
//                    headers: {
//           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//           },
//                    success:function(data){
//                        location.reload();
//                    }
//                 });
//             })
//         }
//         function save_data4(status,candidate_id){
      
//       $('#save_deal4').click(function(){
//           let comment=$('#inproc4').val();
          
//                 $.ajax({
//                    url:base_url+"/admin/deals/saveInproccess",
//                    type:"post",
//                    data:{comment:comment,status:status,candidate_id:candidate_id},
//                    headers: {
//           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//           },
//                    success:function(data){
//                        location.reload();
//                    }
//                 });
//             })
//         }
//         function save_data5(status,candidate_id){
//        $('#save_deal5').click(function(){
//           let comment=$('#inproc5').val();
          
//                 $.ajax({
//                    url:base_url+"/admin/deals/saveInproccess",
//                    type:"post",
//                    data:{comment:comment,status:status,candidate_id:candidate_id},
//                    headers: {
//           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//           },
//                    success:function(data){
//                        location.reload();
//                    }
//                 });
//             })
//         }