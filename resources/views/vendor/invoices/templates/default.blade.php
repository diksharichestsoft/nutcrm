<?php
$id = Session::get('invoice_id');
$firc = Session::get('firc');

if ($id == null) {
    $id = DB::table('project_transactions')
        ->get()
        ->last()->id;
}

$fetch = \App\Models\ProjectTransaction::with(['items', 'taxType'])
    ->where('id', $id)
    ->first();
$comp = \DB::table('companies')
    ->where('id', $fetch->company_id)
    ->first();
$comp_add = \DB::table('project_transactions')
    ->where('id', $fetch->id)
    ->first();

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>{{ $invoice->name }}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <style type="text/css" media="screen">
        * {
            font-family: Arial, Helvetica, sans-serif;
            box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
        }

        html,
        body {
            margin: 0;
            padding: 0;
        }

        .party-header {
            font-size: 1.5rem;
            font-weight: 400;
        }

        .total-amount {
            font-size: 12px;
            font-weight: 700;
        }

    </style>
</head>

<body style="padding:20px; box-sizing: border-box; -webkit-box-sizing: border-box;-moz-box-sizing: border-box;">
    {{-- Header --}}


    <table cellspacing="0" cellpadding="0" style="width:100%;max-width:100%;margin:0 auto;">
        <tr>
            <td valign="top">
                <table cellpadding="0" cellspacing="0" width="100%;">
                    <tr>
                        <td style="width:50%;">
                            <a href="javascript:void(0)">
                                <img style="width:150px;max-width: 150px;"
                                    src="{{ $invoice->getLogo() }}" alt="{{url('/')}}/img/logo.png">
                            </a>
                        </td>
                        <td style="width:50%;" align="right">
                            <h1
                                style="font-family: Arial, Helvetica, sans-serif;color:#333;font-size: 15px;text-transform:uppercase;margin-bottom: 5px;">
                                Original for recipient</h1>
                            <?php
                            // if($fetch->tax_type == "1"){
                            ?>
                            @if ($fetch->tax_type_id == 8 )
                            <h1
                            style="font-family: Arial, Helvetica, sans-serif;color:#333;font-size: 15px;margin-top:8px;text-transform:uppercase;margin-bottom: 5px;">
                            Export Invoice</h1>
                            @endif
                            <?php //}
                            ?>
                            <span style="font-family: Arial, Helvetica, sans-serif;color:#333;font-size: 14px;">Invoice No {{ ($fetch->invoice_number) ?? ''}}</span>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td style="width: 100%;">
                <table cellpadding="0" cellspacing="0" width="100%;" style="margin-top: 40px;">
                    <tr>
                        <td style="width: 60%;">
                            <h2
                                style="font-family: Arial, Helvetica, sans-serif;color:#333;font-size: 15px;margin-top: 0;">
                                GSTIN:{{$comp->gst_number}} </h2>
                            <h2
                                style="font-family: Arial, Helvetica, sans-serif;color:#333;font-size: 18px;margin-top: 0;margin-bottom: 5px; !important;">
                                {{ $comp->name ? $comp->name : '' }}</h2>
                            <div
                                style="font-family: Arial, Helvetica, sans-serif;color:#333;margin-top:0;font-size: 14px;line-height: 20px;">


                                {{--Jugad --}}
                                   {{--  There were some issues, if the address have more than one <p> tags
                                        then it was giving error of 'division by zero', to fix this we putted
                                        whole in <p> tag, it fixed the issue if there are more than one <p> tage
                                        but new issue arrived if there is only one or no <p> tag it was giving the error
                                        so we did a conditional statement,
                                        if
                                        there are less than two <p> tags we will simply show it(It will not give any error)
                                        else
                                        there are more than 1 <p> tags, we will put all the <p> tags in a single <p> tag
                                        this fixed the error

                                    --}}
                                    @if (substr_count($fetch->from_address, '<p>')<2)
                                        {!!$fetch->from_address!!}
                                        @else
                                        <p>
                                            {!!$fetch->from_address!!}
                                        </p>
                                    @endif

                                {{--Jugad --}}

                                <!-- F-34, 2nd Floor, Phase 8, Industrial Area, SahibzadaAjit Singh Nagar, Punjab 160071, India -->
                            </div>
                            <table cellpadding="0" cellspacing="0" width="100%;">
                                <tr>
                                    <td style="width: 50%; padding-right: 20px;" valign="top">
                                        <h3
                                            style="font-family: Arial, Helvetica, sans-serif;color:#333;font-size: 15px;color:#afafaf;text-transform:uppercase;margin-bottom: 0;">
                                            Bill to:</h3>
                                                <div
                                                    style="font-family: Arial, Helvetica, sans-serif;color:#333;font-size: 14px;line-height: 18px;">
                                                    <p>
                                                    <?php
                                                    $string = htmlentities($fetch->bill_to_address, null, 'utf-8');
                                                    $content = str_replace('&nbsp;', '', $string);
                                                    echo $content = html_entity_decode($content);
                                                    ?>
                                                    </p>
                                                </div>

                                    </td>

                                    <td style="width: 50%;padding-left: 20px;" valign="top">
                                        <p>
                                            {{-- <?php
                                             $string = str_replace('<p>&nbsp;</p>','',$fetch->ship_to_address);
                                             if($string!=""){ ?>
                                            <h3
                                                style="font-family: Arial, Helvetica, sans-serif;color:#333;font-size: 15px;color:#afafaf;text-transform:uppercase;margin-bottom: 0;">
                                                Ship to:</h3>
                                            <div
                                                style="font-family: Arial, Helvetica, sans-serif;color:#333;font-size: 14px;line-height: 18px;">
                                                <?php
                                                $string = htmlentities($fetch->ship_to_address, null, 'utf-8');
                                                $content = str_replace('&nbsp;', '', $string);
                                                echo $content = html_entity_decode($content);
                                                ?>
                                            </div>
                                            <?php }?> --}}
                                        </p>
                                    </td>

                                </tr>
                            </table>
                        </td>
                        <td width="50%" valign="top">
                            <table cellpadding="0" cellspacing="0" width="60%;" align="right">
                                <tr>
                                    <td>
                                        <h3
                                            style="font-family: Arial, Helvetica, sans-serif;color:#333;font-size: 14px;text-transform:capitalize;line-height:20px;text-align: right;margin-bottom: 0;margin-top: 0;font-weight: normal;">
                                            Invoice Date:{{ date('d-m-Y', strtotime($fetch->created_at)) }}</h3>
                                    </td>
                                    <td>
                                        <span
                                            style="font-family: Arial, Helvetica, sans-serif;color:#333;font-size: 14px;line-height:20px;display: block;text-align: right;font-weight: normal;"></span>
                                    </td>
                                </tr>

                                <tr>

                                    <td>
                                        <span
                                            style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height:20px;display: block;text-align: right;"></span>
                                    </td>
                                </tr>
                                <?php
                                // if($fetch->tax_type == "1"){
                                ?>
                                @if ($fetch->tax_type_id == 8 )
                                    <tr>
                                        <td colspan="2">
                                            <p
                                                style="font-family: Arial, Helvetica, sans-serif;font-size: 14px;line-height:20px;display: block;text-align: right;color:#333;">
                                                Place of supply: Outside India</p>
                                        </td>
                                    </tr>
                                @endif
                                <?php// } ?> ?>

                            </table>
                        </td>

                    </tr>

                </table>
            </td>

        </tr>

        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" width="100%;" style="margin-top: 30px;">
                    <thead>
                        <tr>
                            <th scope="col" class="border-0"
                                style="text-align:left;background-color: #333;font-family: Arial, Helvetica, sans-serif;font-size: 15px;color: #fff;padding: 10px;">
                                {{ __('invoices::invoice.description') }}</th>
                            @if ($invoice->hasItemUnits)
                                <th scope="col" class="text-center border-0"
                                    style="background-color: #333;font-family: Arial, Helvetica, sans-serif;font-size: 15px;color: #fff;padding: 10px;">
                                    {{ __('invoices::invoice.units') }}</th>
                            @endif
                            <th scope="col" class="text-center border-0"
                                style="background-color: #333;font-family: Arial, Helvetica, sans-serif;font-size: 15px;color: #fff;padding: 10px;">
                                {{ __('invoices::invoice.quantity') }}</th>
                            <th scope="col" class="text-right border-0"
                                style="background-color: #333;font-family: Arial, Helvetica, sans-serif;font-size: 15px;color: #fff;padding: 10px;">
                                Rate</th>
                            @if ($invoice->hasItemDiscount)
                                <th scope="col" class="text-right border-0"
                                    style="background-color: #333;font-family: Arial, Helvetica, sans-serif;font-size: 15px;color: #fff;padding: 10px;">
                                    {{ __('invoices::invoice.discount') }}</th>
                            @endif
                            @if ($invoice->hasItemTax)
                                <th scope="col" class="text-right border-0"
                                    style="background-color: #333;font-family: Arial, Helvetica, sans-serif;font-size: 15px;color: #fff;padding: 10px;">
                                    {{ __('invoices::invoice.tax') }}</th>
                            @endif
                            <th scope="col" class="text-right border-0"
                                style="background-color: #333;font-family: Arial, Helvetica, sans-serif;font-size: 15px;color: #fff;padding: 10px;text-align: right;">
                                Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- Table Items --}}
                        @foreach ($fetch->items as $item)

                            <tr>
                                <td
                                    style="font-family: Arial, Helvetica, sans-serif;font-size: 16px;color: #333;padding: 10px;">
                                    {{ $item['description'] }}</td>

                                <td class="text-center"
                                    style="font-family: Arial, Helvetica, sans-serif;font-size: 16px;color: #333;padding: 10px;text-align: center;">
                                    {{ $item['quantity'] }}</td>
                                <td class="text-right"
                                    style="font-family: Arial, Helvetica, sans-serif;font-size: 16px;color: #333;padding: 10px;text-align: center;">
                                    <span style="font-family: DejaVu Sans;">{{ getCurrencySymbol($fetch->currency) }}</span> {{ $item['rate'] }}

                                </td>
                                <td class="text-right"
                                    style="text-align:center;font-family: Arial, Helvetica, sans-serif;font-size: 16px;color: #333;padding: 10px;text-align:right;">
                                    <span style="font-family: DejaVu Sans;">{{ getCurrencySymbol($fetch->currency) }}</span> {{ $item['amount'] }}
                                </td>
                                @if ($invoice->hasItemTax)
                                    <td class="text-right"
                                        style="font-family: Arial, Helvetica, sans-serif;font-size: 16px;color: #333;padding: 10px;">
                                    </td>
                                @endif

                                {{-- <td class="text-right"
                                    style="font-family: Arial, Helvetica, sans-serif;font-size: 16px;color: #333;padding: 10px;text-align: right;">
                                </td> --}}
                            </tr>
                        @endforeach
                        {{-- Summary --}}
                        @if ($invoice->hasItemOrInvoiceDiscount())
                            <tr>
                                <td colspan="{{ $invoice->table_columns - 2 }}" class="border-0"></td>
                                <td class="text-right pl-0">{{ __('invoices::invoice.total_discount') }}</td>
                                <td class="text-right pr-0">
                                    {{-- {{ $invoice->formatCurrency($invoice->total_discount) }} --}}
                                </td>
                            </tr>
                        @endif
                        @if ($invoice->taxable_amount)
                            <tr>
                                <td colspan="{{ $invoice->table_columns - 2 }}" class="border-0"></td>
                                <td class="text-right"
                                    style="font-family: Arial, Helvetica, sans-serif;font-size: 16px;color: #333;padding: 10px;">
                                    {{ __('invoices::invoice.taxable_amount') }}</td>
                                <td class="text-right"
                                    style="font-family: Arial, Helvetica, sans-serif;font-size: 16px;color: #333;padding: 10px;">
                                    {{-- {{ $invoice->formatCurrency($invoice->taxable_amount) }} --}}
                                </td>
                            </tr>
                        @endif
                        @if ($invoice->tax_rate)
                            <tr>
                                <td colspan="{{ $invoice->table_columns - 2 }}" class="border-0"></td>
                                <td class="text-right pl-0">{{ __('invoices::invoice.tax_rate') }}</td>
                                <td class="text-right pr-0">
                                    {{ $invoice->tax_rate }}%
                                </td>
                            </tr>
                        @endif
                        @if ($invoice->hasItemOrInvoiceTax())
                            <tr>
                                <td colspan="{{ $invoice->table_columns - 2 }}" class="border-0"></td>
                                <td class="text-right pl-0">{{ __('invoices::invoice.total_taxes') }}</td>
                                <td class="text-right pr-0">
                                    {{-- {{ $invoice->formatCurrency($invoice->total_taxes) }} --}}
                                </td>
                            </tr>
                        @endif
                        @if ($invoice->shipping_amount)
                            <tr>
                                <td colspan="{{ $invoice->table_columns - 2 }}" class="border-0"></td>
                                <td class="text-right pl-0">{{ __('invoices::invoice.shipping') }}</td>
                                <td class="text-right pr-0" style="text-align: right;">
                                    {{-- {{ $invoice->formatCurrency($invoice->shipping_amount) }} --}}
                                </td>
                            </tr>
                        @endif

                    </tbody>
                </table>
            </td>
        </tr>

    </table>

    <table cellspacing="0" cellpadding="0" style="width:100%;max-width:100%;margin:30px auto 0 auto;">

        <tr>

            <td align="right" valign="top">
                <table cellpadding="0" cellspacing="0" width="50%;" align="right">
                    <tr>
                        <td>
                            <h3
                                style="font-family: Arial, Helvetica, sans-serif;color:#333;font-size: 15px;color:#afafaf;text-align: right;margin-bottom: 0;margin-top: 0;">
                                SubTotal:</h3>
                        </td>
                        <td>
                            <span
                                style="font-family: Arial, Helvetica, sans-serif;color:#333;font-size: 14px;line-height:20px;display: block;text-align: right;padding-right: 10px;">
                                <span style="font-family: DejaVu Sans;">{{ getCurrencySymbol($fetch->currency) }}</span>
                                {{ $fetch->total_amount - $fetch->total_tax_amount + $fetch->total_discount_amount}}</span>
                        </td>
                    </tr>

                    <tr>
                        <td style="height: 5px;"></td>
                        <td style="height: 5px;"></td>
                    </tr>

                    <?php //if($fetch->tax_type!="1"){
                    ?>
                    @if($fetch->total_tax_amount != 0)
                    {{-- if Tax is cgst or sgst --}}
                    @if ($fetch->taxType->id == 6)
                    <tr>

                        <td valign="top">
                            <h3
                                style="font-family: Arial, Helvetica, sans-serif;color:#333;font-size: 15px;color:#afafaf;text-align: right;margin-bottom: 0;margin-top: 0;">
                                Tax: CGST({{$fetch->taxType->value}}%)</h3>
                            </td>
                            <td valign="top">
                                <span
                                style="font-family: Arial, Helvetica, sans-serif;color:#333;font-size: 14px;line-height:20px;display: block;text-align: right;padding-right: 10px;">
                                <span style="font-family: DejaVu Sans;">{{ getCurrencySymbol($fetch->currency) }}</span> {{ ($fetch->total_tax_amount)/2 }}
                            </span>
                        </td>
                    </tr>
                    <tr>

                        <td valign="top">
                            <h3
                                style="font-family: Arial, Helvetica, sans-serif;color:#333;font-size: 15px;color:#afafaf;text-align: right;margin-bottom: 0;margin-top: 0;">
                                Tax: SGST({{$fetch->taxType->value}}%)</h3>
                            </td>
                            <td valign="top">
                                <span
                                style="font-family: Arial, Helvetica, sans-serif;color:#333;font-size: 14px;line-height:20px;display: block;text-align: right;padding-right: 10px;">
                                <span style="font-family: DejaVu Sans;">{{ getCurrencySymbol($fetch->currency) }}</span> {{( $fetch->total_tax_amount)/2 }}
                            </span>
                        </td>
                    </tr>

                    @else
                    <tr>

                        <td valign="top">
                            <h3
                                style="font-family: Arial, Helvetica, sans-serif;color:#333;font-size: 15px;color:#afafaf;text-align: right;margin-bottom: 0;margin-top: 0;">
                                Tax: {{strtoupper($fetch->taxType->name)}}({{($fetch->taxType->id == 7 ? $fetch->taxType->value *2 : $fetch->taxType->value  )}}%)</h3>
                            </td>
                            <td valign="top">
                                <span
                                style="font-family: Arial, Helvetica, sans-serif;color:#333;font-size: 14px;line-height:20px;display: block;text-align: right;padding-right: 10px;">
                                <span style="font-family: DejaVu Sans;">{{ getCurrencySymbol($fetch->currency) }}</span> {{ $fetch->total_tax_amount }}
                            </span>
                        </td>
                    </tr>
                    @endif
                    @endif

                    <tr>
                        <td style="height: 5px;"></td>
                        <td style="height: 5px;"></td>
                    </tr>
                    @if($fetch->total_discount_amount != 0)
                        <tr>
                            <td valign="top">

                                <h3
                                style="font-family: Arial, Helvetica, sans-serif;color:#333;font-size: 15px;color:#afafaf;text-align: right;margin-bottom: 0;margin-top: 0;">
                                Discount ({{($fetch->discount_type == '%') ? $fetch->discount_value : ''}} {{$fetch->discount_type ?? ''}})</h3>
                            </td>
                            <td valign="top">
                                <span
                                    style="font-family: Arial, Helvetica, sans-serif;color:#333;font-size: 14px;line-height:20px;display: block;text-align: right;padding-right: 10px;">
                                    <?php //echo $currency_sign;
                                    ?>
                                    <span style="font-family: DejaVu Sans;">{{ getCurrencySymbol($fetch->currency) }}</span> {{ $fetch->total_discount_amount }}

                                </span>
                            </td>
                        </tr>
                    @endif
                    <?php //}
                    ?>
                    {{-- @if ($fetch->discount > 0) --}}


                    <tr>
                        <td style="height: 5px;"></td>
                        <td style="height: 5px;"></td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <h3
                                style="font-family: Arial, Helvetica, sans-serif;color:#333;font-size: 15px;color:#afafaf;text-align: right;margin-bottom: 0;margin-top: 0;">
                                Total:</h3>
                        </td>
                        <td valign="top">
                            <span
                                style="font-family: Arial, Helvetica, sans-serif;color:#333;font-size: 14px;line-height:20px;display: block;text-align: right;padding-right: 10px;">
                                <span style="font-family: DejaVu Sans;">{{ getCurrencySymbol($fetch->currency) }}</span> {{ $fetch->total_amount }}</span>
                        </td>
                    </tr>


                </table>
            </td>


        </tr>

        <tr>
            <td style="width: 100%;height: 80px;"></td>
        </tr>
        <tr>
            <td>
                <table cellspacing="0" cellpadding="0" style="width:100%;max-width:100%;margin:30px auto 0 auto;">

                    <tr>
                        <td valign="top">
                            <?php ?>
                            <h3
                                style="font-family: Arial, Helvetica, sans-serif;color:#afafaf;font-size: 15px;margin-top:0;margin-bottom: 0;">
                                Bank Details</h3>
                            <?php //}
                            ?>
                            <table cellpadding="0" cellspacing="0" width="100%;">
                                <tr>
                                    <td valign="top">
                                        <div
                                            style="font-family: Arial, Helvetica, sans-serif;color:#333;font-size: 14px;line-height:20px;margin-bottom: 0;margin-top: 0;">


                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>

                <p style="font-size: 15px;line-height:25px;margin:0px;">A/C Name : {{$comp->account_name ?? ''}}</p>
                <P style="font-size: 15px;line-height:25px;margin:0px;">A/C number: {{$comp->account_number ?? ''}}</P>
                <p style="font-size: 15px;line-height:25px;margin:0px;text-transform: uppercase;">swift:{{$comp->swift ?? ''}}</p>
                <p style="font-size: 15px;line-height:25px;margin:0px;">IFSC Code: {{$comp->ifsc_code ?? ''}}</p>
                <p style="font-size: 15px;margin:0px;">Bank Name:{{$comp->bank_name ?? ''}}</p>
                <p style="font-size: 15px;line-height:25px;margin:0px;"> Branch name: {{$comp->branch_name ?? ''}}</p>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <table cellspacing="0" cellpadding="0" style="width:100%;max-width:100%;margin:80px auto 0 auto;">
                    <tr>
                        <td>
                            <?php
                            // $string = str_replace('<p>&nbsp;</p>','',$fetch->notes);
                            // if($string!=""){
                            ?>
                            <p style="font-family: Arial, Helvetica, sans-serif;font-size: 15px;color: #333;">
                                @if ($fetch->tax_type_id == 8)
                                    Note: Supply meant for export of services without payment of tax under LUT.
                                @endif
                                {{-- {{ trans('invoices::invoice.notes') }}: --}}
                                <?php
                                // $string = htmlentities($fetch->notes, null, 'utf-8');
                                // $notes = str_replace("&nbsp;", "", $string);
                                // echo $notes = html_entity_decode($notes);
                                ?>
                            </p>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        @if($fetch->notes)
        <tr>
            <td>
                <div style="page-break-before:always;"></div>
                <b>Notes</b>
            </td>

        </tr>
        <tr>
            <td>
                {!!$fetch->notes ?? ''!!}
            </td>

        </tr>
        @endif
        @if($fetch->details)
        <tr>
            <td>
                <b>Details</b>
            </td>

        </tr>
        <tr>
            <td>
                {!!$fetch->details ?? ''!!}
            </td>

        </tr>
        @endif
        @if($fetch->terms)
        <tr>
            <td>
                <b>Terms</b>
            </td>

        </tr>
        <tr>
            <td>
                {!!$fetch->terms ?? ''!!}
            </td>

        </tr>
        @endif



    </table>

</body>

</html>
