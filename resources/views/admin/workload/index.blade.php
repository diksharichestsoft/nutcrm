<style>
    .daterangepicker .ranges li {font-size: 12px;padding: 8px 12px;cursor: pointer;background: #000000b0;}
  </style>
  <div class="card" id="data">
                <div class="card-header p-2" id="card_head">
                  <ul class="nav nav-pills">

                    @foreach($departments as $status)
                    <li class="nav-item"><a class="nav-link services" href="#view" onclick="fetch_data(1,this.getAttribute('data-id'))" data-id="{{$status->id}}" data-toggle="tab">{{$status->name}}</a></li>&nbsp;
                    @endforeach
                    <li class="nav-item"><a class="nav-link services" href="#view" onclick="fetch_data(1,this.getAttribute('data-id'))" data-id="-1" data-toggle="tab">All Candidates</a></li>&nbsp;
                    <div id="reportrange" style="background: transparent; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                    <i class="fa fa-calendar"></i>&nbsp;
                    <span class="ab"></span> <i class="fa fa-caret-down"></i>
                   </div>
                   <input type="hidden" id="start_date_range" >
                   <input type="hidden" id="end_date_range">
                    <li class="nav-item search-right">
                      <div class="search_bar">
                          <input class="form-control form-control-sidebar" id="hours-search" type="search" placeholder="Search" aria-label="Search">
                      </div>
                     <div class="search_bar">
                        <div class="input-group" data-widget="sidebar-search">
                         <input class="form-control form-control-sidebar" id="search" type="search" placeholder="Search" aria-label="Search">
                        </div>
                     </div>
                    </li>
                  </ul>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <div class="tab-content">
                    <div class="tab-pane" id="profile">
                       {{-- @include('admin.candidates.includes.addform') --}}
                    </div>

                    <div>
                      @if(!isset($data))
                      <h5>Add Candidates</h5>
                      @endif
                    </div>
                    <div class="active tab-pane" id="view">
                       {{-- @include('admin.candidates.includes.view')--}}
                      </div>
                    <!-- /.tab-pane -->
                  </div>
                  <!-- /.tab-content -->
                </div><!-- /.card-body -->
        </div>


      {{-- Modal for change status, when candidate is on LineUp --}}
  <div class="modal fade" id="changestatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel1">Change Status of Candidate</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="changeStatuSbody">
          <form action="" name="changestatusform" id="changestatusform">
              @csrf

              <input type="hidden" name='candidate_status' id="changeStatus-status" value="">
              <input type="hidden" name='deal_id' id="changeStatus-deal_id" value="">
              <div class="value">

              <div class="form-group row" id="schedule_time">
                <label for="schedule_time" class="col-sm-2 col-form-label">Schedule Time</label>
                <div class="col-sm-10">
                  <input type="datetime-local" class="form-control" name='schedule_time' id="sched_time">
                  <div class="error" id="error_comment"></div>
                </div>
              </div>
              <div class="form-group row" id="joining_date">
                <label for="joining_date" class="col-sm-2 col-form-label">Joining Date</label>
                <div class="col-sm-10">
                  <input type="datetime-local" class="form-control" name='joining_date' id="join_date">
                  <div class="error" id="error_comment"></div>
                </div>
              </div>
              <div class="form-group row">
                <label for="comment" class="col-sm-2 col-form-label">Comment</label>
                <div class="col-sm-10">
                  <textarea name="comment"  cols="30" rows="10" class="form-control"></textarea>
                  <div class="error" id="error_comment"></div>
                </div>
              </div>
              <div class="form-group row">
                <div class="offset-sm-2 col-sm-10">
                  <button type="submit" class="btn btn-success">Submit</button>
                </div>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="createDeal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Candidates</h5>
          <button type="button" class="close" id='createDealHide' data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body " id="createDealBody">
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="addComment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Comment</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body " id="addCommentBody">
        <form action="" name="addCommentForm" id="addCommentForm">
            @csrf
            <input type="hidden" name='deal_id' id="deal_id" value="">
          <div class="value">
            <div class="form-group row">
              <label for="comment" class="col-sm-2 col-form-label">Comment</label>
              <div class="col-sm-10">
                <textarea name="comment" id="comment" cols="30" rows="10" class="form-control"></textarea>
                <div class="error" id="error_comment"></div>
              </div>
            </div>
            <div class="form-group row">
              <div class="offset-sm-2 col-sm-10">
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </div>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script>


    $(document).ready(function(){
      $(".services").eq(0).addClass("active");
    });
    $(document).on('click','.comment',function(){
      var id = $(this).attr('data-id');
      // alert(id);
      $('#deal_id').val(id);
      $('#comment_title').val('');
      $('#comment').val('');
    });


  function updateCompanyProject(e)
  {
    $('#page-loader').show();

    var id=e.getAttribute('data-id');
    $.ajax({
      url:"{{route('candidates-edit')}}",
      type:"get",
      data:{id:id},
      success:function(data)
      {
        $('#page-loader').hide();
        $('#createDealBody').empty().html(data);
      },
      error:function(){
        $('#page-loader').hide();
      }
    })
  }

  $(document).on('submit','#addCompany',function(e){
    e.preventDefault();
    var data = new FormData(this);
      $.ajax({
          type:'post',
          url:"{{route('candidates-store')}}",
          dataType: "JSON",
         xhr: function() {
               myXhr = $.ajaxSettings.xhr();
               return myXhr;
         },
         cache: false,
         contentType: false,
         processData: false,
          data:data,
          success:function(data){
              $(".services:first").trigger( "click" );
              $('#createDeal').modal('hide');
          },
          error:function(data){
            $.each(data.responseJSON.errors, function(id,msg){
              $('#error_'+id).html(msg);
            })
          }
        });
      });


  $(document).on('submit','#addCommentForm',function(e){
    e.preventDefault();
    var data = new FormData(this);
      $.ajax({
        type:'post',
        url:"{{route('candidates-Comment')}}",
        dataType: "JSON",
        xhr: function() {
              myXhr = $.ajaxSettings.xhr();
              return myXhr;
        },
        cache: false,
        contentType: false,
        processData: false,
        data:data,
          success:function(data){
              $('#addComment').modal('hide');
              fetch_data(1,'');
          },
          error:function(data){
            $.each(data.responseJSON.errors, function(id,msg){
              $('.error#error_'+id).html(msg);
            })
          }
        });
      });


    $(document).on('change','#selectsatus',function(){

      $("#schedule_time").hide();
      $("#sched_time").attr('name' , 'schd_time');
      $("#joining_date").hide();
      $("#join_date").attr('name' , 'join_date');
      var status =$(this).val();
      var deal_id = $(this).attr('data-id');
      $('#changeStatus-status').val(status);
      $('#changeStatus-deal_id').val(deal_id);
      $('#changestatusform textarea').val('');
      if(status == 4){
          $("#schedule_time").show();
          $("#sched_time").attr('name' , 'schedule_time');

      }
      if(status == 18){
          $("#joining_date").show();
          $("#join_date").attr('name' ,'joining_date');
      }
      $('#changestatus').modal('show');

    });
    $(document).on('submit','#changestatusform',function(e){
        e.preventDefault();
          var data = new FormData(this);
      $.ajax({
          type:'post',
          url:"{{route('candidates-change-status')}}",
          dataType: "JSON",
         xhr: function() {
               myXhr = $.ajaxSettings.xhr();
               return myXhr;
         },
         cache: false,
         contentType: false,
         processData: false,
          data:data,
          success:function(data){
              $('#changestatus').modal('hide');
              fetch_data(1,'');
          },
          error:function(data){
            $.each(data.responseJSON.errors, function(id,msg){
              $('#error_'+id).html(msg);
            })
          }
        });
  });

  function removeCompanyProject(e)
  {

    var id=e.getAttribute('data-id');
    console.log(id);
    swal({
    title: "Oops....",
    text: "Are You Sure You want to delete!",
    icon: "error",
    buttons: [
      'NO',
      'YES'
    ],
  }).then(function(isConfirm) {
    if (isConfirm) {
      $.ajax({
        url:"{{route('candidates-remove-data')}}",
        type:"post",
        data:{id:id},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        success:function(data){
          $('#view').empty().html(data);
          fetch_data(1,'');
        }
      })
    } else {

    }
  });
  }

  function accept(e)
  {

    var id=e.getAttribute('data-id');
    $.ajax({
      url:"{{route('candidates-accept')}}",
      type:"post",
      data:{id:id},
      headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
      success:function(data)
      {
      fetch_data(1,'');
      }
    })
  }

    $(document).on('submit','#updateCompany',function(e){
      e.preventDefault();
      var data = new FormData(this);
      $.ajax({
          type:'post',
          url:"{{route('candidates-store')}}",
          dataType: "JSON",
         xhr: function() {
               myXhr = $.ajaxSettings.xhr();
               return myXhr;
         },
         cache: false,
         contentType: false,
         processData: false,
          data:data,
          success:function(data){
            fetch_data(1,'');
            $('#createDeal').modal('hide');
          },
          error:function(data){
            $.each(data.responseJSON.errors, function(id,msg){
              $('#error_'+id).html(msg);
            });
          }
      });
  });
  function addcompanydeals()
  {
    $('#page-loader').show();

    $.ajax({
      url:"{{route('candidates-add')}}",
      type:"get",
      success:function(data)
      {
        $('#page-loader').hide();
        $('#createDealBody').empty().html(data);
      },
      error:function(error){
        $('#page-loader').hide();
      }
    })
  }

  $(function() {
  var start = moment().subtract(29, 'days');
  var end = moment();
  function cb(start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      $('#start_date_range').val(start.format('YYYY-MM-DD'));
      $('#end_date_range').val(end.format('YYYY-MM-DD'));
      fetch_data(1,'');
  }

  $('#reportrange').daterangepicker({
      startDate: start,
      endDate: end,
      ranges: {
         'Today': [moment(), moment()],
         'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
         'Last 7 Days': [moment().subtract(6, 'days'), moment()],
         'Last 30 Days': [moment().subtract(29, 'days'), moment()],
         'This Month': [moment().startOf('month'), moment().endOf('month')],
         'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
  }, cb);
  cb(start, end);

  });

  $(document).on('click', '.pagination a', function(event){
    event.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    var sendurl=$(this).attr('href');
    fetch_data(page,'','');
  });
  document.querySelector("#search").addEventListener("keyup",(e)=>{
      // var active=$('a.active.services').attr('data-id');
      fetch_data(1,'');
  });
  document.querySelector("#hours-search").addEventListener("keyup",(e)=>{
      // var active=$('a.active.services').attr('data-id');
      fetch_data(1,'');
  });

  function fetch_data(page,active)
  {
    $('#page-loader').show();
    var start_date=document.querySelector('#start_date_range').value;
    var end_date=document.querySelector('#end_date_range').value;
    if(active==''){
      var active=document.getElementsByClassName('services active')[0].getAttribute('data-id');
    }

  //   page = (page='')?'1':page;
    var search=document.querySelector("#search").value;
    var hours_search=document.querySelector("#hours-search").value;
    var type = "{{$type}}";
    var data={search:search,type:type ,active:active,start_date:start_date,end_date:end_date, hours_search: hours_search};
    var make_url= `{{url('/')}}/admin/workload/search?page=`+page;
    $.ajax({
      url:make_url,
      data:data,
      success:function(data)
      {
          $('#view').empty().html(data);
        $('#page-loader').hide();

      },
      error:function(error){
        $('#page-loader').hide();

      }
    });
    }
  </script>

