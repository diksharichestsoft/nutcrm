<style>
    select#selectsatus {
      width: 100%;
  }
  </style>

  <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Work Load</h3>
                  <h6 class="results text-right">Total Work Load : {{$data->total()}}</h6>
                </div>
                <!-- /.card-header -->
               <div class="card-body">
                  <table class="table table-bordered">
                    @if(count($data)>0)
                    <thead>
                      <tr>
                        <th style="width: 10px">Action</th>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Department</th>
                        <th>Designation</th>
                        @if (!$isDev)
                        <th>Experience</th>
                        @endif
                        <th>Total Projects Assigned</th>
                        @if (!$isDev)
                        <th>Total Hours Assigned</th>
                        <th>Total Tasks</th>
                        @else
                        <th>Todo</th>
                        <th>In progress</th>
                        <th>Bugs</th>
                        @endif

                      </tr>
                    </thead>
                    @endif
                    <tbody>
                          @forelse($data as $key=>$value)
                             <tr>
                                 <td>
                                    <a href="{{route('workload-employee-view',$value->id)}}" target="_blank" class="btn btn-outline-success btn-xs  view"><i class="fas fa-eye"></i></a>
                                 </td>
                                 <td>{{$value->id}}</td>
                                 <td>{{$value->name}}</td>
                                 <td>
                                     @if ($value->departments())
                                     @foreach ($value->departments()->get() as $thisUserDepartment)
                                     {{$thisUserDepartment->name}}
                                     @endforeach
                                     @endif
                                 </td>
                                 <td>
                                     {{$value->designation}}


                                 </td>
                                 @if (!$isDev)
                                 <td>
                                    {{$value->experience}}
                                </td>
                                @endif
                                 <td>
                                     {{DB::table('project_team')->where('user_id', $value->id)->where('project_team.status', 1)->count()}}
                                 </td>
                                 @if (!$isDev)

                                 <td>
                                     @php
                                     $total_hours = $value->total_work_hours ?? 00;
                                     echo "<script>console.log(".$total_hours.")</script>";
                                     $hours = floor($total_hours / 60);
                                     $minutes = ($total_hours % 60);
                                     if($hours < 10){
                                         $hours = "0".$hours;
                                        }
                                        if($minutes < 10){
                                            $minutes = "0".$minutes;
                                        }
                                        @endphp
                                 {{$hours.":".$minutes}}
                                </td>

                                <td>
                                    {{$value->tasksAssigned()->count()}}
                                </td>
                                @else
                                <td>
                                    {{$value->tasksAssigned()->where('task_status_id', 2)->count()}}
                                </td>
                                <td>
                                    {{$value->tasksAssigned()->where('task_status_id', 3)->count()}}
                                </td>
                                <td>
                                    {{$value->tasksAssigned()->where('task_status_id', 5)->count()}}
                                </td>
                                @endif
                             </tr>
                          @empty
                            <center> <h3> No Data Available </h3> </center>
                          @endforelse
                    </tbody>
                  </table>
                </div>
                {{$data->links()}}
                {{--
                @if($data->previousPageUrl() != null)
                   <a href="{{$data->previousPageUrl()}}" class="pagination prev_btn pull-left"><i class="fa fa-chevron-left"></i> Previous</a>
                @endif

                @if($data->nextPageUrl() != null)
                    <a href="{{$data->nextPageUrl()}}" class="pagination prev_btn pull-right">Next <i class="fa fa-chevron-right"></i> </a>
                @endif
                --}}
                <!-- /.card-body -->
              </div>

