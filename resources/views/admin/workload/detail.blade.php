@extends('admin.layout.template')
@section('contents')
<div class="accordion" id="accordionExample">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h2 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Employee Status of {{$thisUser->name}}
        </button>
      </h2>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="card-body">

        <table class="table table-sm">
          <thead>
            <tr>
              <th style="width: 10px">#</th>
              <th>Header</th>
              <th>Label</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1.</td>
              <td>Total Project In</td>
              <td>
                  <h5>
                      <span class="badge bg-info">
                        {{$thisUser->projects()->count()}}
                      </span>
                  </h5>
              </td>
            </tr>
            <tr>
              <td>2.</td>
              <td>Total Tasks Assigned</td>
              <td>
                  <h5>
                      <span class="badge bg-info">
                        {{$thisUser->tasksAssigned()->count()}}
                      </span>
                  </h5>
              </td>
            </tr>
            <tr>
              <td>2.</td>
              <td>Total Project Hours</td>
              <td>
                @php
                $total_hours = 0;
                $total_hours = DB::table('project_team')->select('project_team.*', 'projects.deal_status')->LeftJoin('projects', 'projects.id', 'project_team.project_id')->where('projects.deal_status', 2)->where('user_id', $thisUser->id)->where('project_team.status', 1)->sum('work_hours');
                $hours = floor($total_hours / 60);
                $minutes = ($total_hours % 60);
                @endphp
                <h5>
                    <span class="badge bg-info">
                    {{$hours.":".$minutes}}
                    </span>
                </h5>
              </td>
            </tr>
          </tbody>
        </table>

      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Tasks
        </button>
      </h2>
    </div>
    <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="card-body">
        <div class="card">
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
            <table class="table table-hover text-nowrap">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Title</th>
                  <th>Project</th>
                  <th>Work Hours</th>
                  <th>Current Status</th>
                  <th>Summary</th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($taskOfThisUser as $key=>$task)
                  <tr>
                  <td>{{$key + 1}}</td>
                  <td>
                      <a href="{{route('task-detial-view',$task->id)}}" target="_blank">{{$task->title}}</a>
                  </td>
                  <td>
                      <a href="{{route('projects-view',$task->project()->first('id')->id)}}" target="_blank">  {{$task->project()->first('title')->title}}</a>
                  </td>
                  <td>{{$task->time}}</td>
                  <td><span class="tag tag-success">{{$task->thisTaskStatus()->first('title')->title}}</span></td>
                  <td>{{$task->summary}}</td>
                  </tr>
                  @endforeach
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseTwo">
          Projects In
        </button>
      </h2>
    </div>
    <div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
        <div class="card">
          <!-- /.card-header -->
          <div class="card-body table-responsive p-0">
            <table class="table table-hover text-nowrap">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Title</th>
                  <th>Work Hours In this Project</th>
                  <th>Total Team Member</th>
                  <th>Have Task?(Unfinished)</th>
                  <th>Remove From Project</th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($thisUser->projects()->where('deal_status', 2)->get() as $project)
                  @php
                    $projectUser=$project->users()->where('user_id', $thisUser->id)->first();
                   if (empty($projectUser)) {
                      continue;
                  }
                  @endphp
                  <tr>
                  <td>{{$project->id}}</td>
                  <td>
                        <a href="{{route('projects-view',$project->id)}}" target="_blank">{{$project->title}}</a>
                  </td>
                  @php

                  $min_in_team = $project->users()->where('user_id', $thisUser->id)->first()->pivot->work_hours;
                  $hours = floor($min_in_team/ 60);
                  $minutes = ($min_in_team % 60);
                  if($hours < 10) $hours = '0'.$hours;
                  if($minutes < 10) $minutes = '0'.$minutes;
                    @endphp
                  <td>{{$hours.":".$minutes}}</td>
                  <td>{{$project->users()->count()}}</td>
                  @php
                    $thisUserTasks = DB::table('task_users')->where('task_users.user_id', $thisUser->id)->join('tasks', 'tasks.id', 'task_users.task_id')->where('tasks.project_id', $project->id)->where('tasks.task_status_id','!=', 8)->count();
                  @endphp
                  <td>{{$thisUserTasks}}</td>
                  <td>
                      @if ($thisUserTasks == 0)
                      {{-- {{dd($project->users()->where('user_id', $thisUser->id)->first()->pivot->id)}} --}}
                      <button type="button" data-id="{{$project->users()->where('user_id', $thisUser->id)->first()->pivot->id}}" onclick="removeteamember(this)" class="btn btn-danger remove_modal_data"  data-toggle="modal" data-target="#">
                        Remove
                    </button>
                    @else
                      <button type="button" class="btn btn-warning swalDefaultWarning" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
                          Can't Delete <i class="fa fa-info-circle" aria-hidden="true"></i>
                      </button>
                      @endif
                  </td>
                  </tr>
                  @endforeach
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
      </div>
    </div>
  </div>

</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>

$(".swalDefaultWarning").on('click', function () {
    swal({
  title: "Employee is working on tasks",
  text: "In order to remove employee from a project, the employee should'nt have any task(In Progress) in the project",
  icon: "warning",
  dangerMode: true,
})
 })
    function removeteamember(e)
    {

        var id=e.getAttribute('data-id');
        swal({
            title: "Remove From team",
            text: "Are You Sure You want to remove!",
            icon: "error",
            buttons: [
                'NO',
                'YES'
            ],
        }).then(function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url:"{{route('remove-team-member')}}",
                    type:"post",
                    data:{id:id},
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success:function(data){
                    e.parentElement.parentElement.remove();
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Remove Successfully',
                        showConfirmButton: false,
                        timer: 1500
                    })
            }
            })
        } else {

        }
        });
    }
</script>
@endsection
