   <!-- Main content -->
   <div class="modal fade" id="addAnnouncementModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Announcement</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="add_dsr_modal_body" class="modal-body">
            @include('admin.announcements.create')
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

   <div class="modal fade" id="editAnnouncementModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Announcement</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="edit_dsr_modal_body" class="modal-body">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>



    {{-- <a href="#" id="addTodoButton" class="btn btn-outline-success btn-lg" data-toggle="modal" data-target="#addTodoModal">
        <i class="fa fa-plus" aria-hidden="true"></i>New Todo
    </a> --}}
    <div class="modal fade" id="createDeal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Todos</h5>
              <button type="button" class="close" id='createDealHide' data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body " id="todoBody">
            </div>
          </div>
        </div>
      </div>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!--  -->
                <div class="card-body">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width: 10px">#</th>
                          <th>Action</th>
                          <th>Title</th>
                          <th>Description</th>
                          <th>Department</th>
                          <th>Announcement</th>
                          <th>Created At</th>
                          <th>Updated At</th>
                        </tr>
                      </thead>
                      <tbody>
                          @forelse ($annoncements as $key=> $value )
                          <tr>
                              <td>
                                {{$key+1}}
                              </td>
                              <td>
                                <div class="btn-group">
                                    @can('announcements_edit')
                                    <button data-id="{{$value->id}}" onclick="updateAnnouncement(this)" class="btn btn-outline-success btn-xs  update" ><i class="fas fa-pencil-alt"></i></button>
                                    @endcan

                                    @can('announcements_view')
                                    <button data-id="{{$value->id}}" class="btn btn-outline-success btn-xs view" onclick="viewAnnouncementDetail(this)"><i class="fas fa-eye"></i></button>
                                    @endcan

                                    @can('announcements_delete')
                                    <button data-id="{{$value->id}}" onclick="removeAnnouncement(this)" class="btn btn-danger btn-xs  remove"><i class="fas fa-times"></i></button>
                                    @endcan

                                </div>
                              </td>
                              <td>
                                  {{$value->title}}
                                </td>
                                <td>
                                {!!$value->description!!}
                            </td>
                            <td>
                              {{($value->type == 1) ? "Development" : ''}}
                              {{($value->type == 2) ? "SEO" : ''}}
                              {{($value->type == 3) ? "Both SEO and Dev" : ''}}
                          </td>
                          @can('announcements_toggle_status')

                            <td>@if($value->status==1)
                                <button data-id="{{$value->id}}" class="disable_enable btn btn-success btn-xs" onclick="toggleDisableEnable(this)">Enable</button>
                                @else
                                <button data-id="{{$value->id}}" class="disable_enable btn btn-danger btn-xs" onclick="toggleDisableEnable(this)">Disable</button>
                                @endif
                            </td>
                          @endcan

                          <td>{{ $value->created_at->format('d/m/Y')}}</td>
                          <td>{{ $value->updated_at->format('d/m/Y')}}</td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="9">
                                <h4  align="center">No Data Found</h4>
                            </td>
                        </tr>
                        @endforelse
                      </tbody>
                    </table>
                </div>

                <!--  -->
            </div>
        </div>
        {{$annoncements->links()}}
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
    <script>




    $("#addAnnouncementButton").on('click', function (){
      document.querySelector("#error_title").innerText="";
      document.querySelector("#error_type").innerText="";
    // $("#view").html($("#add_dsr_modal_body").html())
    // initializeCreateForm()
    $("#add_announcement_form").on('submit', function (e){
        e.preventDefault();
        // var descData = ($("#editor").html());
        // $("#description").val(descData);
        var data = new FormData(this);
        console.log(data);
        $.ajax({
            type:'post',
            url:"{{route('announcement-store')}}",
            cache: false,
                contentType: false,
                processData: false,
                dataType: "JSON",
            data : {data: data},
        headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data:data,
            success:function(data){
                $('#addAnnouncementModal').modal('hide');
                $(".modal-backdrop").hide();
                $("body").removeClass("modal-open");
                fetchAnnouncementData(1);
                Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Added Successfully',
                showConfirmButton: false,
                timer: 1500
                })

            },
            error:function(data){
            $.each(data.responseJSON.errors, function(id,msg){
                $('#error_'+id).html(msg);

            })
            }
        });
        })

})

function removeAnnouncement(e)
{

  var id=e.getAttribute('data-id');
  swal({
  title: "Oops....",
  text: "Are You Sure You want to delete!",
  icon: "error",
  buttons: [
    'NO',
    'YES'
  ],
}).then(function(isConfirm) {
  if (isConfirm) {
    $.ajax({
      url:"{{route('remove-announcement')}}",
      type:"post",
      data:{id:id},
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
      success:function(data){
        Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Remove Successfully',
                showConfirmButton: false,
                timer: 1500
                })
        fetchAnnouncementData(1);
        }
    })
  } else {

  }
});
}


function updateAnnouncement(e)
{
  $('#page-loader').show();

  var id=e.getAttribute('data-id');
  $.ajax({
    url:"{{route('announcement-edit')}}",
    type:"get",
    data:{id:id},
    success:function(data)
    {
       // toastr.success(response.message)
      $('#page-loader').hide();
      $('#edit_dsr_modal_body').empty().html(data);
    $("#editAnnouncementModal").modal('show');
      intitializeUpdate();
    },
    error:function(){
      $('#page-loader').hide();
    }
  })
}
function intitializeUpdate(){

// initializeCreateForm()
    $("#update_announcement_form").on('submit', function (e){
        e.preventDefault();
        // var descData = ($("#editorupdate").html());
        // console.log(descData)
        // $("#update-description").val(descData);

        var data = new FormData(this);
        console.log(data);
        $.ajax({
            type:'post',
            url:"{{route('announcement-store')}}",
            cache: false,
                contentType: false,
                processData: false,
                dataType: "JSON",
            data : {data: data},
        headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data:data,
            success:function(data){
                $('#addAnnouncementModal').modal('hide');
                $(".modal-backdrop").hide();
                fetchAnnouncementData(1);
                Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Updated Successfully',
                showConfirmButton: false,
                timer: 1500
                })
            },
            error:function(data){
            $.each(data.responseJSON.errors, function(id,msg){
                $('#error_'+id).html(msg);

            })
            }
        });
        })

}

function viewAnnouncementDetail (e){
    let id = $(e).data('id')
    $.ajax({
    url:"{{route('announcement-detail-view')}}",
    type:"get",
    data:{id:id},
    success:function(data)
    {
       // toastr.success(response.message)
      $('#page-loader').hide();
      $('#edit_dsr_modal_body').empty().html(data);
    $("#editAnnouncementModal").modal('show');
      intitializeUpdate();
    },
    error:function(){
      $('#page-loader').hide();
    }
  })
}
</script>
