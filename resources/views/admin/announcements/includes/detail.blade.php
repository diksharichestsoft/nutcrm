
<table class="table">
    <tbody>
      <tr>
        <th scope="row">Title</th>
        <td>{{$thisAnnouncement->title}}</td>
      </tr>
      <tr>
        <th scope="row">Description</th>
        <td>{!!$thisAnnouncement->description!!}</td>
      </tr>
      <tr>
        <th scope="row">Created At</th>
        <td>{{timestampToDateAndTimeObj($thisAnnouncement->created_at)->time}}</td>
      </tr>
      <tr>
        <th scope="row">Created On</th>
        <td>{{timestampToDateAndTimeObj($thisAnnouncement->created_at)->date}}</td>
      </tr>
      <tr>
        <th scope="row">Department</th>
        <td>
            {{($thisAnnouncement->type == 1) ? "Development" : ''}}
            {{($thisAnnouncement->type == 2) ? "SEO" : ''}}
            {{($thisAnnouncement->type == 3) ? "Both SEO and Dev" : ''}}

        </td>
      </tr>
      <tr>
        <th scope="row">Users</th>
        <td>
          @foreach($thisAnnouncement->users as $user)
          {{$user->name}}
          <br>
          @endforeach
        </td>
      </tr>

    </tbody>
  </table>

