<!-- Button trigger modal -->
  <!-- Modal -->
  <style>
    #user + span {
      width: 100% !important;
    }
    </style>
  <form id="add_announcement_form" action="" method="POST">
    <div id="dsr_item_container">
        @csrf
        <div class="dsr_items">
            <div class="form-group">
                <label for="project_id">Title</label>
                <br>
                <input type="text" required class="form-control" name="title">
                <input type="hidden" class="form-control" name="created_by" value="{{Auth::user()->id}}">
                <input type="hidden" name="reqType" value="0">
                <div class="error" id="error_title"></div>
            </div>
            <div class="form-group">
                <label for="description" class="col col-form-label">Description</label>
                      <textarea id="description" rows="3" class="form-control" name="description"></textarea>
                      {{-- <div id="editor" style="border: 1px solid #ffffff42;"></div> --}}
              </div>
              <div class="form-group">
                <label for="department_type" class="col col-form-label">Department</label>
                <div class="col-sm-10">
                  <select name="type" required id="type" class="form-control select2">
                    <option disabled selected >Select Department</option>
                      <option value="1">Development</option>
                      <option value="2">SEO</option>
                      <option value="3">Both SEO and Dev</option>
                  </select>
                  <div class="error" id="error_type"></div>
                </div>
              </div>

              <div class="form-group">
                <label for="user" class="col col-form-label">Users</label><br>
                <div class="col-sm-10">
                    <select name="user_id[]" id="user" class="form-control select2" multiple required>
                      <option value="" selected>All Users</option>
                      @foreach($users as $user)
                      <option value="{{$user->id}}">{{$user->name}}</option>
                      @endforeach
                    </select>
                </div>
                @error('user_id')
                    <div class="error error-msg-red"></div>
                @enderror
            </div>
            <div class="form-group">
                <label for="color" class="col col-form-label">Color</label>
                <div class="col-sm-10">
                    <input type="color" id="favcolor" name="color" required value="#3f6791">
                </div>
            </div>

        </div>
    </div>
    <br>
        {{-- <button type="submit" class="btn btn-primary">Save</button> --}}
        <button type="button" class="btn btn-primary" onclick="$('#add_announcement_form').submit()"> Save</button>

    </form>
    <script>
      $('#user').select2({
        dropdownParent: $('#addAnnouncementModal')
    });
    </script>
    <script>
      // var myEditor;
      // InlineEditor
      // .create( document.querySelector( `#editor` ) )
      // .then( editor => {
      //     console.log( 'Editor was initialized', editor );
      //     myEditor = editor;
      // } )
      // .catch( err => {
      //     console.error( err.stack );
      // } );
      // $(document).ready(function (){
      // // Hiding default extrack file and dropdown buttons
      // $(".ck-file-dialog-button").hide();
      // $(".ck-dropdown__button").hide();
      // })
  </script>
