<form action="" name="addCompany" id="addCompany">
    @csrf
    <input type="hidden" name="req_type" value="update">
    <input type="hidden" name="id" value="{{$thisCandidate->id}}">
  <div class="value">
    <div class="form-group row">
      <label for="deal_title" class="col-sm-2 col-form-label">Name of Candidate</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="title"  name="name" value="{{$thisCandidate->name}}" >
        <div class="error" id="error_name"></div>
      </div>
    </div>
    <div class="form-group row">
      <label for="email" class="col-sm-2 col-form-label">Email</label>
      <div class="col-sm-10">
        <input type="email" class="form-control" id="email"  name="email" value="{{$thisCandidate->email}}" >
        <div class="error" id="error_email"></div>
      </div>
    </div>
    <div class="form-group row">
      <label for="phone" class="col-sm-2 col-form-label">Phone Number</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="phone"  name="phone" value="{{$thisCandidate->phone}}" >
        <div class="error" id="error_phone"></div>
      </div>
    </div>
    <div class="form-group row">
      <label for="current_salary" class="col-sm-2 col-form-label">Current Salary</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="current_salary"  name="current_salary" value="{{$thisCandidate->current_salary}}" >
        <div class="error" id="error_current_salary"></div>
      </div>
    </div>
    <div class="form-group row">
      <label for="expected_salary" class="col-sm-2 col-form-label">Expected Salary</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="expected_salary"  name="expected_salary" value="{{$thisCandidate->expected_salary}}" >
        <div class="error" id="error_expected_salary"></div>
      </div>
    </div>
    <div class="form-group row">
      <label for="notice" class="col-sm-2 col-form-label">Notice Period</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="notice"  name="notice" value="{{$thisCandidate->notice}}" >
        <div class="error" id="error_notice"></div>
      </div>
    </div>
    <div class="form-group row">
      <label for="total_experience" class="col-sm-2 col-form-label">Total Experience</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="total_experience"  name="total_experience" value="{{$thisCandidate->total_experience}}" >
        <div class="error" id="error_total_experience"></div>
      </div>
    </div>
    <div class="form-group row">
      <label for="reference" class="col-sm-2 col-form-label">Reference</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="reference"  name="reference" value="{{$thisCandidate->reference}}" >
        <div class="error" id="error_reference"></div>
      </div>
    </div>
    <div class="form-group row">
      <label for="current_company" class="col-sm-2 col-form-label">Current Company Name</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="current_company"  name="current_company" value="{{$thisCandidate->current_company}}" >
        <div class="error" id="error_current_company"></div>
      </div>
    </div>

    <div class="form-group row">
        <label for="resume" class="col-sm-2 col-form-label">Resume</label>
        <div class="col-sm-10">
          <input type="file" class="form-control" id="resume" name="resume">
          <div class="error" id="error_project_title"></div>
        </div>
      </div>
    <div class="form-group row">
      <label for="department" class="col-sm-2 col-form-label">Department</label>
      <div class="col-sm-10">
        <select name="department" id="department" class="form-control select2">
          <option value="">Select Company</option>
          @foreach($departments as $department)
            <option value="{{ $department->id }}" {{ $thisCandidate->departments()->first()->id == $department->id ? 'selected' : '' }}>{{ $department->name }}</option>
          @endforeach
        </select>
        <div class="error" id="error_department"></div>
      </div>
    </div>

    <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-success">Save Deal</button>
        <button type="reset" class="btn btn-danger">Reset</button>
      </div>
    </div>
  </div>

  </form>


