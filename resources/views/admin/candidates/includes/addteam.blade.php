{{-- **********************remove team Modal ****************************** --}}
  {{-- Modal --}}
  <div class="modal fade" id="removeTeamMember" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="" id="remove_team_member_form">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Remove Member from Team</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
                @csrf
                <input type="hidden" name="user_project_team_id" id="user_project_team_id" value="">
                <input type="hidden" name="member_name" id="member_name" value="">
                <input type="hidden" name="project_id" id="this_project_id" value="{{$getProjectData->id}}">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Submit</button>
            </div>
        </form>
      </div>
    </div>
  </div>


<div>

      <button type="button" id="addTeamModalToggle" class="btn btn-success mb-4" data-toggle="modal" data-target="#addTeamModal">
        Add Member
      </button>


      <table class="table table-dark">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">id</th>
                <th scope="col">Name</th>
                <th scope="col">Work Hours</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @php
                $i = 1
            @endphp
            @forelse ($getProjectData->users as $projectData )
            <tr>

                <td>{{$i}}</td>
                <td>{{$projectData->id}}</td>
                <td>{{$projectData->name}}</td>
                <td>{{date('H:i', mktime(0,$projectData->pivot->work_hours))}}</td>
                <td>
                    <button type="button" data-id="{{$projectData->pivot->id}}" onclick="removeteamember(this)" data-name="{{$projectData->name}}" class="btn btn-primary remove_modal_data"  data-toggle="modal" data-target="#">
                        Remove
                      </button>
                </td>
            </tr>
                @php
                    $i++
                @endphp
        @empty
        <tr>
            <td colspan="3" class="text-center text-muted">No members working on this project</td>
        </tr>
        @endforelse
        </tbody>
      </table>


</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>

$(".dropdown-toggle").click();
    $(".remove_modal_data").on("click", function (e){
        id = $(this).data("id");

        $("#user_project_team_id").val(id);
        name = $(this).data("name");
        $("#member_name").val(name);
    })
    $("#remove_team_member_form").on('submit', function (e){
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            type:'post',
            url:"{{route('remove-team-member')}}",
            dataType: "JSON",
        xhr: function() {
                myXhr = $.ajaxSettings.xhr();
                return myXhr;
        },
        cache: false,
        contentType: false,
        processData: false,
            data:data,
            success:function(data){
                $("#removeTeamMember").modal('hide');
                addTeamview();

            },
            error:function(data){
            }
        });
    });


function removeteamember(e)
    {
        var id=e.getAttribute('data-id');
        swal({
        title: "Remove From team",
        text: "Are You Sure You want to remove!",
        icon: "error",
        buttons: [
            'NO',
            'YES'
        ],
        }).then(function(isConfirm) {
        if (isConfirm) {
            $.ajax({
            url:"{{route('remove-team-member')}}",
            type:"post",
            data:{id:id},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            success:function(data){
                addTeamview();
            }
            })
        } else {

        }
        });
    }

</script>


