{{-- <div class="card">
              <div class="card-header">
                <h3 class="card-title">Permissions</h3>
                @include('admin.permissions.includes.back')
              </div>
              <!-- /.card-header -->
             <div class="card-body">

                      <div class="form-group row">
                        <label for="first_name" class="col-sm-2 col-form-label">Title</label>
                        <div class="col-sm-10">
                          <input type="text" value="{{$permission->title}}" class="form-control" id="title" readonly name="title" placeholder="Permission Title">
                        </div>
                      </div>

                </div>
</div>--}}


 <section class="content">

<!-- Default box -->
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Project Detail</h3>

    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
        <i class="fas fa-times"></i>
      </button>
    </div>
  </div>
  <div class="card-body">
    <div class="row">
      <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
        <div class="row">
          <div class="col-12 col-sm-4">
            <div class="info-box bg-light">
              <div class="info-box-content">
                <span class="info-box-text text-center text-muted">Estimated budget</span>
                <span class="info-box-number text-center text-muted mb-0">{{$getProjectData->budget??''}}</span>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-4">
            <div class="info-box bg-light">
              <div class="info-box-content">
                <span class="info-box-text text-center text-muted">Total amount spent</span>
                <span class="info-box-number text-center text-muted mb-0">{{$getProjectData->budget??''}}</span>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-4">
            <div class="info-box bg-light">
              <div class="info-box-content">
                <span class="info-box-text text-center text-muted">Estimated project duration</span>
                <span class="info-box-number text-center text-muted mb-0">{{$getProjectData->budget??''}}</span>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <h4>Recent Activity</h4>
              <div class="post">
                <div class="user-block">
                  <img class="img-circle img-bordered-sm" src="../../dist/img/user1-128x128.jpg" alt="user image">
                  <span class="username">
                    <a href="#">Jonathan Burke Jr.</a>
                  </span>
                  <span class="description">Shared publicly - 7:45 PM today</span>
                </div>
                <!-- /.user-block -->
                <p>
                  Lorem ipsum represents a long-held tradition for designers,
                  typographers and the like. Some people hate it and argue for
                  its demise, but others ignore.
                </p>

                <p>
                  <a href="#" class="link-black text-sm"><i class="fas fa-link mr-1"></i> Demo File 1 v2</a>
                </p>
              </div>

              <div class="post clearfix">
                <div class="user-block">
                  <img class="img-circle img-bordered-sm" src="../../dist/img/user7-128x128.jpg" alt="User Image">
                  <span class="username">
                    <a href="#">Sarah Ross</a>
                  </span>
                  <span class="description">Sent you a message - 3 days ago</span>
                </div>
                <!-- /.user-block -->
                <p>
                  Lorem ipsum represents a long-held tradition for designers,
                  typographers and the like. Some people hate it and argue for
                  its demise, but others ignore.
                </p>
                <p>
                  <a href="#" class="link-black text-sm"><i class="fas fa-link mr-1"></i> Demo File 2</a>
                </p>
              </div>

              <div class="post">
                <div class="user-block">
                  <img class="img-circle img-bordered-sm" src="https://adminlte.io/themes/v3/dist/img/user1-128x128.jpg" alt="user image">
                  <span class="username">
                    <a href="#">Jonathan Burke Jr.</a>
                  </span>
                  <span class="description">Shared publicly - 5 days ago</span>
                </div>
                <!-- /.user-block -->
                <p>
                  Lorem ipsum represents a long-held tradition for designers,
                  typographers and the like. Some people hate it and argue for
                  its demise, but others ignore.
                </p>

                <p>
                  <a href="#" class="link-black text-sm"><i class="fas fa-link mr-1"></i> Demo File 1 v1</a>
                </p>
              </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-12 col-lg-4 order-1 order-md-2">
        <h3 class="text-primary"><i class="fas fa-paint-brush"></i>Client Information</h3>
        <p class="text-muted">{{$getProjectData->job_descriprion??''}}</p>
        <br>
        <div class="text-muted">
          <p class="text-sm">Client Name
            <b class="d-block">{{$getProjectData->client_name??''}}</b>
          </p>
          <p class="text-sm">Client Email
            <b class="d-block">{{$getProjectData->client_email??''}}</b>
          </p>
          <p class="text-sm">Client Phone
            <b class="d-block">{{$getProjectData->client_phone??''}}</b>
          </p>
        </div>

        <h5 class="mt-5 text-muted">Project Details</h5>
        <ul class="list-unstyled">
          <li>
            <i class="far fa-fw fa-file-word"></i>Project Name:-{{$getProjectData->title??''}}
          </li>
          <li>
            <i class="far fa-fw fa-file-pdf"></i> Project Type:-{{DB::table('project_type')->where(['id' => $getProjectData->project_type])->pluck('name')->first() }}
          </li>
          <li>
            <i class="far fa-fw fa-envelope"></i>Reffred By:-{{DB::table('users')->where(['id' => $getProjectData->referred_by])->pluck('name')->first() }}
          </li>
          <li>
            <i class="far fa-fw fa-image "></i>Platform:-{{$getProjectData->platform_id??''}}
          </li>
          <!-- <li>
           <i class="far fa-fw fa-file-word"></i>Estimate Hour
          </li> -->

        </ul>
        <h5 class="mt-5 text-muted">Project Times Details</h5>
        <ul class="list-unstyled">
          <li>
            <i class="fas  fa-clock"></i>&nbsp;Planned Start Date:-{{$getProjectData->planned_start_date??''}}
          </li>
          <li>
            <i class="fas  fa-clock"></i>&nbsp;Planned End Date:-{{$getProjectData->planned_start_date??''}}
          </li>
          <li>
            <i class="fas  fa-clock"></i>&nbsp;Estimated Hour:-{{$getProjectData->estimated_hours??''}}
          </li>
          <li>
           <i class="far fa-fw fa-image"></i>&nbsp;Url:-{{$getProjectData->url??''}}
          </li>
        </ul>
        <!-- <div class="text-center mt-5 mb-3">
          <a href="#" class="btn btn-sm btn-primary">Add files</a>
          <a href="#" class="btn btn-sm btn-warning">Report contact</a>
        </div> -->
      </div>
    </div>
  </div>
  <!-- /.card-body -->
</div>
<!-- /.card -->

</section>
<!-- Content Wrapper. Contains page content -->
<!-- <div class="content-wrapper"> -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>TimeLine</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">

            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
                  <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">

                  <li class="nav-item"><a class=" active nav-link" href="#timeline" data-toggle="tab">Timeline</a></li>

                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                 <!-- /.tab-pane -->
                  <div class=" active tab-pane" id="timeline">
                    <!-- The timeline -->
                    <div class="timeline timeline-inverse">
                      <!-- timeline time label -->
                      <!-- <div class="time-label">
                        <span class="bg-danger">
                          10 Feb. 2014
                        </span>
                      </div> -->
                      <!-- /.timeline-label -->
                      <!-- timeline item -->
                      <!-- <div>
                        <i class="fas fa-envelope bg-primary"></i>

                        <div class="timeline-item">
                          <span class="time"><i class="far fa-clock"></i> 12:05</span>

                          <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>

                          <div class="timeline-body">
                            Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                            weebly ning heekya handango imeem plugg dopplr jibjab, movity
                            jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                            quora plaxo ideeli hulu weebly balihoo...
                          </div>
                          <div class="timeline-footer">
                            <a href="#" class="btn btn-primary btn-sm">Read more</a>
                            <a href="#" class="btn btn-danger btn-sm">Delete</a>
                          </div>
                        </div>
                      </div> -->
                      <!-- END timeline item -->
                      <!-- timeline item -->
                      @foreach($getcomment as $comment)
                      <div>
                        <i class="fas fa-user bg-info"></i>
                        <div class="timeline-item">
                          <?php
                          $comment_time=$comment->created_at;
                          $time=explode(' ',$comment_time);
                          $get_time_hour_minute_seconds=explode(':',$time[1]);
                          $hour=$get_time_hour_minute_seconds[0];
                          $minute=$get_time_hour_minute_seconds[1];
                          if($hour>12)
                          {
                            $get_hour=$hour-12;
                            ?>
                            <span class="time"><i class="far fa-clock"></i> {{$get_hour}}: {{$minute}} PM</span>
                            <?php
                          }
                          else
                          {
                            $get_hour=$hour;
                            ?>
                            <span class="time"><i class="far fa-clock"></i> {{$get_hour}}: {{$minute}} AM</span>
                            <?php
                          }
                          ?>

                          <h3 class="timeline-header border-0"><a href="#">{{DB::table('users')->where(['id' => $comment->created_by])->pluck('name')->first() }}</a> Changed your Status
                          </h3>
                        </div>
                      </div>
                      <!-- END timeline item -->
                      <!-- timeline item -->

                      <div>
                        <i class="fas fa-comments bg-warning"></i>

                        <div class="timeline-item">
                        <?php
                          $comment_time=$comment->created_at;
                          $time=explode(' ',$comment_time);
                          $get_time_hour_minute_seconds=explode(':',$time[1]);
                          $hour=$get_time_hour_minute_seconds[0];
                          $minute=$get_time_hour_minute_seconds[1];
                          if($hour>12)
                          {
                            $get_hour=$hour-12;
                            ?>
                            <span class="time"><i class="far fa-clock"></i> {{$get_hour}}: {{$minute}} PM</span>
                            <?php
                          }
                          else
                          {
                            $get_hour=$hour;
                            ?>
                            <span class="time"><i class="far fa-clock"></i> {{$get_hour}}: {{$minute}} AM</span>
                            <?php
                          }
                          ?>
                          <h3 class="timeline-header"><a href="#">{{DB::table('users')->where(['id' => $comment->created_by])->pluck('name')->first() }}</a> commented on your post</h3>

                          <div class="timeline-body">
                            {{$comment->comment}}
                          </div>
                          <!-- <div class="timeline-footer">
                            <a href="#" class="btn btn-warning btn-flat btn-sm">View comment</a>
                          </div> -->
                        </div>
                      </div>
                      @endforeach
                      {{$getcomment->links()}}
                      <!-- END timeline item -->
                      <!-- timeline time label -->
                      <!-- <div class="time-label">
                        <span class="bg-success">
                          3 Jan. 2014
                        </span>
                      </div> -->
                      <!-- /.timeline-label -->
                      <!-- timeline item -->
                      <!-- <div>
                        <i class="fas fa-camera bg-purple"></i>

                        <div class="timeline-item">
                          <span class="time"><i class="far fa-clock"></i> 2 days ago</span>

                          <h3 class="timeline-header"><a href="#">Mina Lee</a> uploaded new photos</h3>

                          <div class="timeline-body">
                            <img src="https://placehold.it/150x100" alt="...">
                            <img src="https://placehold.it/150x100" alt="...">
                            <img src="https://placehold.it/150x100" alt="...">
                            <img src="https://placehold.it/150x100" alt="...">
                          </div>
                        </div>
                      </div> -->
                      <!-- END timeline item -->
                      <!-- <div>
                        <i class="far fa-clock bg-gray"></i>
                      </div>
                    </div>
                  </div> -->
                  <!-- /.tab-pane -->


                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  <!-- </div> -->
  <!-- /.content-wrapper -->
  <script>

  </script>


