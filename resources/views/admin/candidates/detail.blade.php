@extends('admin.layout.template')
@section('contents')
{{-- Add comment Modal --}}
<div class="modal fade" id="addComment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Comment</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body " id="addCommentBody">
        <form action="" name="addCommentForm" id="addCommentForm">
            @csrf
            <input type="hidden" name='deal_id' id="deal_id" value="{{$getCandidateData->id}}">
          <div class="value">
            <div class="form-group row">
              <label for="comment" class="col-sm-2 col-form-label">Comment</label>
              <div class="col-sm-10">
                {{-- <textarea name="comment" id="comment" cols="30" rows="10" class="form-control"></textarea> --}}
                <input type="hidden" id="comment" class="form-control" name="comment">
                <div id="editor" style="border: 1px solid #ffffff42;"></div>

                <div class="error" id="error_comment"></div>
              </div>
            </div>
            <div class="form-group row">
              <div class="offset-sm-2 col-sm-10">
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </div>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>


<div id="permissions_data">


{{-- Modal --}}
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Candidate Detail</h3>

                <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                    <i class="fas fa-times"></i>
                </button>
                </div>
            </div>


            <div class="card-body">
                <div class="row pb-4">
                    <div class="col-6">
                        <h5 class="mt-5 ">Candidate Details</h5>
                        <ul class="list-unstyled list-group">
                            <li class="list-group-item">
                                <i class="far fa-fw fa-file-word"></i>Candidate Name:- {{$getCandidateData->name??''}}
                            </li>
                            <li class="list-group-item">
                                <i class="far fa-fw fa-file-pdf"></i>Email:- {{$getCandidateData->email??''}}
                            </li>
                            <li class="list-group-item">
                                <i class="far fa-fw fa-envelope"></i>Phone:- {{$getCandidateData->phone??''}}
                            </li>
                            <li class="list-group-item">
                                <i class="far fa-fw fa-image "></i>Current Salary:- {{$getCandidateData->current_salary??''}}
                            </li>
                            <li class="list-group-item">
                                <i class="far fa-fw fa-image "></i>Expected Salary:- {{$getCandidateData->expected_salary??''}}
                            </li>

                        </ul>
                    </div>
                    <div class="col-6">
                        <h5 class="mt-5 ">-</h5>

                        <ul class="list-unstyled list-group">
                            <li class="list-group-item">
                                <i class="far fa-fw fa-image "></i>Notive:- {{$getCandidateData->notice??''}}
                            </li>
                            <li class="list-group-item">
                                <i class="far fa-fw fa-image "></i>Total Experience:- {{$getCandidateData->total_experience??''}}
                            </li>
                            <li class="list-group-item">
                                <i class="far fa-fw fa-image "></i>Current Company:- {{$getCandidateData->current_company??''}}
                            </li>
                            <li class="list-group-item">
                                <i class="far fa-fw fa-image "></i>Departement:- {{$getCandidateData->departments()->first()->name??''}}
                            </li>
                            <li class="list-group-item">
                                @php
                                    $timeArr = explode("T",$getCandidateData->time_schedule);
                                    $timestamp = strtotime($timeArr[0]);
                                    $new_date = date("d-m-Y", $timestamp);
                                @endphp
                                <i class="far fa-fw fa-image "></i>Scheduled Date:- {{$new_date ?? ''}}
                            </li>
                            <li class="list-group-item">
                                <i class="far fa-fw fa-image "></i>Scheduled Time:- {{$timeArr[1] ?? ''}}
                            </li>


                        </ul>
                    </div>
                </div>

                <div class="row">
                <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
                    <div class="row">
                        <div class="col-12">
                          <br>


                        </div>
                    <div class="row">
                        <div class="col-12 mt-3 mb-3">

                        </div>
                    </div>
                    <div class="row">
                    <div class="col-12">
                        <h4>Recent Comments</h4>

                        @foreach($comments as $comment)
                          <div class="post">
                              <div class="user-block">
                              <img class="img-circle img-bordered-sm" src="../../dist/img/user1-128x128.jpg" alt="user image">
                              <span class="username">
                                  <a href="#">{{$comment->name}}</a>
                              </span>
                              <span class="description">{{$comment->title}} - 7:45 PM today</span>
                              </div>
                              <!-- /.user-block -->
                              <p>
                              {{$comment->comment}}
                              </p>

                              <p>
                              <a href="#" class="link-black text-sm"><i class="fas fa-link mr-1"></i> Demo File 1 v2</a>
                              </p>
                          </div>
                        @endforeach


                </div>

                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </section>
<!-- Content Wrapper. Contains page content -->
<!-- <div class="content-wrapper"> -->
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <div id="comment-section-view">
        @include('admin.candidates.includes.timeline')

    </div> <!--End comment-section-view -->

</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jscroll/2.4.1/jquery.jscroll.min.js"></script>
<script>
      $('ul.pagination').hide();
                $(function() {
                    $('.timeline-inverse').jscroll({
                        autoTrigger: true,
                        padding: 0,
                        nextSelector: '.pagination li.active + li a',
                        contentSelector: 'div.timeline-inverse',
                        callback: function() {
                            $('ul.pagination').remove();
                        }
                    });
                });
    $(document).on('submit','#addCommentForm',function(e){
        $('#page-loader').show();
        var commentData = ($("#editor").html());
        $("#comment").val(commentData);

e.preventDefault();
var data = new FormData(this);
$.ajax({
  type:'post',
  url:"{{route('candidates-Comment')}}",
  dataType: "JSON",
  xhr: function() {
        myXhr = $.ajaxSettings.xhr();
        return myXhr;
  },
  cache: false,
  contentType: false,
  processData: false,
  data:data,
    success:function(data){
        $('#addComment').modal('hide');
        getCommentData();
        $('#page-loader').hide();
	myEditor.setData('');
    },
    error:function(data){
      $.each(data.responseJSON.errors, function(id,msg){
        $('.error#error_'+id).html(msg);
        $('#page-loader').hide();

      })
    }
  });
});
function getCommentData(page = 1){
    $('#page-loader').show();
    $.ajax({
          url:"{{route('candidates-get-timeline')}}",
          data:{id:"{{$getCandidateData->id}}", page: page},
          success:function(data)
          {
              $('#comment-section-view').empty().html(data);
              $('#page-loader').hide();

          }
        });
}
$(document).on('click', '.pagination a', function(event){
  event.preventDefault();
  var page = $(this).attr('href').split('page=')[1];
  var sendurl=$(this).attr('href');
  getCommentData(page);
});
</script>
<script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/inline/ckeditor.js"></script>
<script>
    var myEditor;
    InlineEditor
    .create( document.querySelector( `#editor` ) )
    .then( editor => {
        console.log( 'Editor was initialized', editor );
        myEditor = editor;
    } )
    .catch( err => {
        console.error( err.stack );
    } );
    $(document).ready(function (){
    // Hiding default extrack file and dropdown buttons
    $(".ck-file-dialog-button").hide();
    $(".ck-dropdown__button").hide();
    })

</script>


@endsection
