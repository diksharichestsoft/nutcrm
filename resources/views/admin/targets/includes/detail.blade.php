@extends('admin.layout.template')

@section('contents')

<table class="table">
    <tbody>
      <tr>
        <th scope="row">Project Name</th>
        <td>{{$thisTarget->project->title ?? ""}}</td>
      </tr>

      <tr>
        <th scope="row">Deal Amount</th>
        <td>{{$thisTarget->deal_amount ?? ""}}</td>
      </tr>
      <tr>
        <th scope="row">Received Amount</th>
        <td>{{$thisTarget->received_amount ?? ""}}</td>
      </tr>
      <tr>
        <th scope="row">Converted By</th>
        <td>{{$thisTarget->convertedByUser->name ?? ""}}</td>
      </tr>
      <tr>
        <th scope="row">Description</th>
        <td>{!!$thisTarget->description!!}</td>
      </tr>
      <tr>
        <th scope="row">Created by</th>
        <td>{{$thisTarget->createdByUser->name ?? ""}}</td>
      </tr>
      <tr>
        <th scope="row">Target Date</th>
        <td>{{date_format(date_create($thisTarget->target_date), "d-m-Y") ?? ""}}</td>
      </tr>
      <tr>
        <th scope="row">Created at</th>
        <td>
            {{timestampToDateAndTimeObj($thisTarget->created_at)->date ?? ""}}
        </td>
      </tr>
    </tbody>
  </table>@endsection
