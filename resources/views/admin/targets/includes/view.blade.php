   <!-- Main content -->
   <div class="card">
    <div class="card-header ">
      {{-- <h3 class="card-title"></h3> --}}
      @if ($name)
      <h6 class="count text-left">Name : {{$name}}</h6>
      @endif
      <h6 class="count text-left">Total Deal Amount : {{$total_deal_amount}}</h6>
      <h6 class="count text-left">Total Received Amount : {{$total_received_amount}}</h6>
      @can('targets_show_calendar')
      <h6 class="count text-left">Total Targets : {{$targets->total()}}</h6>
      @endcan
    </div>
                <div class="card-body">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width: 10px">#</th>
                          <th>Action</th>
                          <th>Project Name</th>
                          <th>Deal Amount</th>
                          <th>Received Amount</th>
                          <th>Converted By</th>
                          <th>Created By</th>
                          <th>Target Date</th>
                          <th>Created At</th>
                        </tr>
                      </thead>
                      <tbody>

                        @forelse($targets as $key=>$target)
                          <tr>
                              <td>{{$key +1}}</td>
                              <td>
                                    <div class="btn-group">
                                      <a href="{{route('targets-detail',$target->id)}}" target="_blank" class="btn btn-outline-success btn-xs  view"><i class="fas fa-eye"></i></a>
                                      @can('targets_edit')
                                      <button data-id="{{$target->id}}" onclick="editTarget(this)" class="btn btn-outline-success btn-xs  update"><i class="fas fa-pencil-alt"></i></button>
                                      @endcan
                                      @can('targets_delete')
                                      <button data-id="{{$target->id}}" onclick="removeTarget(this)" class="btn btn-danger btn-xs  remove"><i class="fas fa-times"></i></button>
                                      @endcan
                                    </div>
                              </td>
                            <td>
                                <p><a href="{{route('projects-view', $target->project->id)}}" target="_blank"> {{$target->project->title  ?? 'NaN'}} </a></p>
                            </td>
                            <td>
                                {{$target->deal_amount}}
                            </td>
                            <td>
                                {{$target->received_amount}}
                            </td>
                            <td>
                                {{$target->convertedByUser->name}}
                            </td>
                            <td>
                                {{$target->createdByUser->name}}
                            </td>
                            <td>
                                {{date_format(date_create($target->target_date), "d-m-Y") ?? ""}}
                            </td>
                            <td>
                                {{timestampToDateAndTimeObj($target->created_at)->date ?? ""}}
                            </td>

                        </tr>
                        @empty
                        <tr>
                            <td colspan="9">
                                <p class="text-center h4">
                                    No Data Found
                                </p>
                            </td>
                        </tr>
                        @endforelse

                      </tbody>
                    </table>
                </div>

                <!--  -->

        {{$targets->links()}}
      </div><!--/. container-fluid -->
    <!-- /.content -->
    <script>


        $(".toggle_user_login").on('click', function (e){
            e.preventDefault();
            var id = $(this).attr('data-id');
            $.ajax({
                type: "post",
                url: "{{route('employee-login-toggle')}}",
                headers:
                {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {id:id},
                dataType: "JSON",
                success: function (response) {

                      document.querySelector(`#${e.target.id}`).checked= response.success;
                }
            });
        })
    $(document).on('click', '#post_pagination .pagination a', function(event){
 event.preventDefault();
 var page = $(this).attr('href').split('page=')[1];
 var filter=document.querySelector("#tabs_filter").value;
 ajax('get',`{{Route("employee-pagination")}}`,data={page:page},'#pagination_employee');
});


function removeCompanyProject(e)
{

  var id=e.getAttribute('data-id');
  swal({
  title: "Oops....",
  text: "Are You Sure You want to delete!",
  icon: "error",
  buttons: [
    'NO',
    'YES'
  ],
}).then(function(isConfirm) {
  if (isConfirm) {
    $.ajax({
      url:"{{route('portfolio-remove-data')}}",
      type:"post",
      data:{id:id},
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
      success:function(data){
        getEmployeeData({{$targets->currentPage()}});
        Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Remove Successfully',
                showConfirmButton: false,
                timer: 1500
                })
        }
    })
  } else {

  }
});
}
</script>
