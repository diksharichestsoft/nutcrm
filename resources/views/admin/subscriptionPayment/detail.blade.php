@extends('admin.layout.template')
@section('contents')
<style>
    .dark-mode .list-group-item {
    border-color: #343a40;
    background-color: #454d55;
}
</style>
<section>
    <h5>Task Details</h5>
    {{-- Started the main accordion --}}
    <div class="accordion" id="accordionExample">

        {{-- Card which contains everything(have darker background) --}}
        <div class="card">

            {{-- Card header --}}
          <div class="card-header" id="headingOne">
              <div class="h4">
                  Project:
                <a href="{{route('projects-view', $thisTask->project_id)}}" target="_blank">
                  {{$thisTask->project->title}}
                </a>
              </div>
          </div> {{--End card header--}}

          {{-- Start collapse --}}
          <div id="collapse1" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body">
                {{-- Start row --}}
                <div class="row">
                    {{-- Start Left side panned/div --}}
                    <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">


                        {{-- Start main table --}}
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <th scope="row">Task Title</th>
                                <td>
                                    {{$thisTask->title}}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Summary</th>
                                <td>{{$thisTask->summary}}</td>
                            </tr>
                            <tr>
                                <th scope="row">Description</th>
                                <td>{!!$thisTask->description!!}</td>
                            </tr>
                            <tr>
                            <th scope="row">File</th>
                            <td>
                                @if($thisTask->file != null)
                                <a class="btn btn-success" href="{{asset('taskUploadFiles/'.$thisTask->file)}}" download> File download </a>
                                @else
                                No Attached
                                @endif
                            </td>
                            </tr>
                        </tbody>
                    </table> {{--End main table--}}
                    <hr>

                    {{-- Start Timer details --}}
                    <div class="container">
                        <h3>Timer Details</h3>
                        <table class="table">
                            <thead class="thead-light">
                              <tr>
                                <th scope="col">#</th>
                                <th scope="col">Employee Name</th>
                                <th scope="col">Total Timer Run</th>
                                <th scope="col">Have Timer Active</th>
                              </tr>
                            </thead>
                            <tbody>
                                @forelse ($thisTask->userTimerData() as $key=>$eachTimerUser)
                                <tr>
                                  <th scope="row">{{$key + 1}}</th>
                                  <td>{{$eachTimerUser->name}}</td>
                                  {{-- <td> {{gmdate("H:i:s", $eachTimerUser->total_calculated_time)}}</td> --}}
                                  <td> {{secToHoursHelper($eachTimerUser->total_calculated_time, true)}}</td>
                                  <td>{{($eachTimerUser->min_stop_time == 0) ? "Yes" : "No"}}</td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="4" class="text-center">
                                        <h4>No Timer Data</h4>
                                    </td>
                                </tr>
                              @endforelse
                            </tbody>
                          </table>



                    </div>
                    <br>
                    <h3>Comments/Feed</h3>
                    {{-- Start commment box --}}
                    <div class="container card">
                        <form action="" name="addCommentForm" id="addCommentForm">
                          @csrf
                          <input type="hidden" name='deal_id' id="deal_id" value="{{$thisTask->id}}">
                          <div class="value">
                              <div class="form-group row"  style="border: 3px solid #ffffff42;">
                                  <div class="col">
                                      <input type="hidden" id="comment" class="form-control" name="comment">
                                      <div id="editor"></div>
                                      <div class="error" id="error_comment"></div>
                                  </div>
                              </div>
                              <div class="form-group align-items-end row">
                                  <div class="col">
                                      <button type="submit" class="btn btn-success float-right">Post Comment</button>
                                  </div>
                              </div>
                          </div>
                      </form>
                    </div> {{--End comment box--}}

                    {{-- Start comment section --}}
                    <div id="comment-section-view">
                        @include('admin.tasks.includes.timeline')
                    </div> {{--End Comment Section--}}

                    </div> {{--End Left side pannel/Div--}}

                    {{-- Start Right side pannel/div --}}
                    <div class="col-12 col-md-12 col-lg-4 order-1 order-md-2">
                        <h3>Other Details</h3>
                            {{-- Start Side table --}}
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <th scope="row">Status</th>
                                <td class="d-flex">
                                    <select name="sel" id="selectsatus" class="form-control deals_status" data-id="{{ $thisTask->id }}" >
                                    <option value="0">Select Status</option>
                                    @foreach($taskStatus as $status)
                                    <option value="{{$status->id}}"  {{($thisTask->task_status_id==$status->id)?"selected":""}}>{{$status->title}}</option>
                                    @endforeach
                                </select>
                            </td>
                              </tr>
                            <tr>
                                <th scope="row">Allowed Time</th>
                                <td>{{$thisTask->time}}</td>
                            </tr>
                            <tr>
                                <th scope="row">Priority</th>
                                <td>{{$priorities[($thisTask->priority_level -1)]}}</td>
                            </tr>

                            <tr>
                                <th scope="row">Assigned By</th>
                                <td>{{$thisTask->assignedBy->name}}</td>
                            </tr>
                            <tr>
                                <th scope="row">Assigned To</th>
                                <td>
                                    <button data-id="{{$thisTask->id}}" data-project-id="{{$thisTask->project_id}}" class="btn btn-primary btn-xs assignButton">ReAssign</button>
                                    @foreach ($thisTask->employees()->get() as $employee)
                                    {{$employee->name}}
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Created By</th>
                                <td>{{$thisTaskWasCreatedBy->name ?? ""}}</td>
                            </tr>

                            <tr>
                                <th scope="row">Start Date</th>
                                <td>{{$thisTask->start_date}}</td>
                            </tr>
                            <tr>
                                <th scope="row">End Date</th>
                                <td>{{$thisTask->end_date}}</td>
                            </tr>
                            <tr>
                                <th scope="row">Actions</th>
                                <td>
                                    @can('tasks_edit')
                                    <button data-id="{{$thisTask->id}}" onclick="editTask(this)" class="btn btn-outline-success btn-xs  update" ><i class="fas fa-pencil-alt"></i></button>
                                    @endcan

                                    @can('tasks_delete')
                                    <button data-id="{{$thisTask->id}}" onclick="removeTask(this)" class="btn btn-danger btn-xs  remove"><i class="fas fa-times"></i></button>
                                    @endcan

                                </td>
                            </tr>
                        </tbody>
                    </table> {{--End Side table--}}

                    </div> {{--End Right side pannel/div--}}
                </div> {{--End row--}}
            </div> {{--End card body--}}
          </div> {{--End collapse--}}
        </div>    {{-- End Card --}}

    </div>

</section>


<div class="modal fade" id="assingTaskModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel1">Assign Task</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="changeStatuSbody">
          <form action="" name="assignTaskForm" id="assignTaskForm">
              @csrf
              <input type="hidden" name='task_id' id="assignTask-task_id" value="">
              <div class="value">
                <div class="form-group row">
                    <label for="comment" class="col-sm-2 col-form-label">Assign Task To</label>
                    <div class="col-sm-10">
                        <select name="user_id[]" class="form-select select2" id="employees_select" aria-label="Default select example" multiple>
                            <option selected>Open this select menu</option>
                        </select>
                        <div class="error" id="error_user_id"></div>
                    </div>
                </div>
              <div class="form-group row">
                <label for="comment" class="col-sm-2 col-form-label">Comment</label>
                <div class="col-sm-10">
                  <textarea name="comment"  cols="30" rows="10" class="form-control"></textarea>
                  <div class="error" id="error_comment"></div>
                </div>
              </div>
              <div class="form-group row">
                <div class="offset-sm-2 col-sm-10">
                  <button type="submit" class="btn btn-success">Submit</button>
                </div>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
{{-- Change Status Modal --}}

<div class="modal fade" id="changestatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel1">Assign Task</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="changeStatuSbody">
          <form action="" name="changestatusform" id="changestatusform">
              @csrf
              <input type="hidden" name='status' id="changeStatus-status" value="">
              <input type="hidden" name='deal_id' id="changeStatus-deal_id" value="">
              <div class="value">
              <div class="form-group row">
                <label for="comment" class="col-sm-2 col-form-label">Comment</label>
                <div class="col-sm-10">
                  <textarea name="comment"  cols="30" rows="10" class="form-control"></textarea>
                  <div class="error" id="error_comment"></div>
                </div>
              </div>
              <div class="form-group row">
                <div class="offset-sm-2 col-sm-10">
                  <button type="submit" class="btn btn-success">Submit</button>
                </div>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="createDeal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Projects</h5>
          <button type="button" class="close" id='createDealHide' data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body " id="createDealBody">
        </div>
      </div>
    </div>
  </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jscroll/2.4.1/jquery.jscroll.min.js"></script>

<script>
  $('ul.pagination').hide();
                $(function() {
                    $('.timeline-inverse').jscroll({
                        autoTrigger: true,
                        padding: 0,
                        nextSelector: '.pagination li.active + li a',
                        contentSelector: 'div.timeline-inverse',
                        callback: function() {
                            $('ul.pagination').remove();
                        }
                    });
                });
    $(document).on('change','#selectsatus',function(){
      var status =$(this).val();
      var deal_id = $(this).attr('data-id');
      $('#changeStatus-status').val(status);
      $('#changeStatus-deal_id').val(deal_id);
      $('#changestatusform textarea').val('');
      $('#changestatus').modal('show');
    });
      $(document).ready(function(){
      $(".services").eq(0).addClass("active");
    });
    $(document).on('click','.comment',function(){
      var id = $(this).attr('data-id');
      $('#deal_id').val(id);
      $('#comment_title').val('');
      $('#comment').val('');

    });
    $(document).on('submit','#changestatusform',function(e){
        e.preventDefault();
          var data = new FormData(this);
      $.ajax({
          type:'post',
          url:"{{route('tasks-change-status')}}",
          dataType: "JSON",
         xhr: function() {
               myXhr = $.ajaxSettings.xhr();
               return myXhr;
         },
         cache: false,
         contentType: false,
         processData: false,
          data:data,
          success:function(data){
              if(data.status == 0){
                  $('#changestatus').modal('hide');
                  Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: data.message,
                        showConfirmButton: false,
                        timer: 7000
                    })
                    return
              }else{
                  Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: "Status Changed successfully",
                        showConfirmButton: false,
                        timer: 2000
                    })
              }
              $('#changestatus').modal('hide');
              getCommentData(1,'');
          },
          error:function(data){
            $.each(data.responseJSON.errors, function(id,msg){
              $('#error_'+id).html(msg);
            })
          }
        });
  });


$(document).on('submit','#addCommentForm',function(e){
    $('#page-loader').show();
    var commentData = ($("#editor").html());
    $("#comment").val(commentData);
    e.preventDefault();
    var data = new FormData(this);
    $.ajax({
    type:'post',
    url:"{{route('tasks-add-Comment')}}",
    dataType: "JSON",
    xhr: function() {
            myXhr = $.ajaxSettings.xhr();
            return myXhr;
    },
    cache: false,
    contentType: false,
    processData: false,
    data:data,
        success:function(data){
            $('#addComment').modal('hide');
            getCommentData();
            $('#page-loader').hide();
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Added Successfully',
                showConfirmButton: false,
                timer: 1500
	          })
        myEditor.setData('');
        },
        error:function(data){
        $.each(data.responseJSON.errors, function(id,msg){
            $('.error#error_'+id).html(msg);
            $('#page-loader').hide();

        })
        }
    });
});
function getCommentData(page = 1){
    $('#page-loader').show();
    $.ajax({
          url:"{{route('tasks-get-timeline')}}",
          data:{id:"{{$thisTask->id}}", page: page},
          success:function(data)
          {
              $('#comment-section-view').empty().html(data);
              $('#page-loader').hide();

          }
        });
}
$(document).on('click', '.pagination a', function(event){
  event.preventDefault();
  var page = $(this).attr('href').split('page=')[1];
  var sendurl=$(this).attr('href');
  getCommentData(page);
});



// Assign Task functionalities
$(document).on('click','.assignButton',function(){
        var task_id = $(this).attr('data-id');
        var project_id = $(this).attr('data-project-id');
        $('#assingTaskModal-status').val(status);
        $('#assignTask-task_id').val(task_id);
        $('#assingTaskModal textarea').val('');
        $
        $.ajax({
            type:'get',
            url:`{{route('get-employee-list')}}?projectId=${project_id}`,
            data: {"status":status},
            dataType: "JSON",
            xhr: function() {
                myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },
            cache: false,
            contentType: false,
            processData: false,
            success:function(data){

                $('#employees_select').empty();
                $.each(data.reasons, function(index, item) {
                $('#employees_select').append(`<option value="${item.id}">${item.name}</option>`)
            });
            },
            error:function(data){
            //   $.each(data.responseJSON.errors, function(id,msg){
            //     $('#error_'+id).html(msg);
            //   })
            }
            });
        $('#assingTaskModal').modal('show');
        });

$(document).on('submit','#assignTaskForm',function(e){
        e.preventDefault();
          var data = new FormData(this);
      $.ajax({
          type:'post',
          url:"{{route('tasks-assign-to-employee')}}",
          dataType: "JSON",
         xhr: function() {
               myXhr = $.ajaxSettings.xhr();
               return myXhr;
         },
         cache: false,
         contentType: false,
         processData: false,
          data:data,
          success:function(data){
              $('#assingTaskModal').modal('hide');
              Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Added Successfully',
                showConfirmButton: false,
                timer: 1500
	          })
          },
          error:function(data){
            $.each(data.responseJSON.errors, function(id,msg){
              $('#error_'+id).html(msg);
            })
          }
        });
  });

  function editTask(e)
  {
    $('#page-loader').show();

    var id=e.getAttribute('data-id');
    $.ajax({
      url:"{{route('tasks-edit')}}",
      type:"get",
      data:{id:id},
      success:function(data)
      {
        $("#createDeal").modal('show')
        $('#page-loader').hide();
        $('#createDealBody').empty().html(data);

      },
      error:function(){
        $('#page-loader').hide();
      }
    })
  }

//   Function to submit task
$(document).on('submit','#addTask',function(e){
    e.preventDefault();
    Swal.showLoading()
    var descData = (myEditor.getData());
    $("#description").val(descData);
    var data = new FormData(this);
      $.ajax({
          type:'post',
          url:"{{route('tasks-store')}}",
          dataType: "JSON",
         xhr: function() {
               myXhr = $.ajaxSettings.xhr();
               return myXhr;
         },
         cache: false,
         contentType: false,
         processData: false,
          data:data,
          success:function(data){
            Swal.close()
              $(".services:first").trigger( "click" );
              $('#createDeal').modal('hide');
              Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Added Successfully, Reload the page to see the changes',
                showConfirmButton: true
	          })
          },
          error:function(data){
            Swal.close()
            $.each(data.responseJSON.errors, function(id,msg){
              $('#error_'+id).html(msg);
            })
          }
        });
      });


  function removeTask(e)
  {
    var id=e.getAttribute('data-id');
    swal({
    title: "Oops....",
    text: "Are You Sure You want to delete!",
    icon: "error",
    buttons: [
      'NO',
      'YES'
    ],
  }).then(function(isConfirm) {
    if (isConfirm) {
      $.ajax({
        url:"{{route('tasks-remove-data')}}",
        type:"post",
        data:{id:id},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        success:function(data){
          $('#view').empty().html(data);
          Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Remove Successfully',
                showConfirmButton: true
                }).then(()=>{
                    window.location.replace("{{route('tasks' ,'all')}}");
                })
        }
      })
    } else {

    }
  });
  }



</script>
<script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/inline/ckeditor.js"></script>
<script>
    var myEditor;
    InlineEditor
    .create( document.querySelector( `#editor` ) )
    .then( editor => {
        console.log( 'Editor was initialized', editor );
        myEditor = editor;
    } )
    .catch( err => {
        console.error( err.stack );
    } );
    $(document).ready(function (){
    // Hiding default extrack file and dropdown buttons
    $(".ck-file-dialog-button").hide();
    $(".ck-dropdown__button").hide();
    })

</script>


@endsection
