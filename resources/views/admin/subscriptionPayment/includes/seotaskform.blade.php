<label for="title" class="col-sm-8 form-label">Title</label>
<label for="title" class="col-sm-3 form-label">Quantity</label>
<form method="post" name="addSeoTask" id="addSeoTask" enctype="multipart/form-data" autocomplete="off">
    @csrf
    @foreach ($seoEmp as $key => $value)

        <div class="value">
            <div class="row seotaskdata">
                <div class="col-md-8">
                    <input type="hidden" class="form-control" name="id" id="update_id" value="{{ $value->id }}">
                    <input type="hidden" class="form-control" name="project_id" id="project_id"
                        value="{{ $project_id }}">
                    <input type="text" class="form-control" id="title" name="title" value="{{ $value->title ?? '' }}"
                        disabled>
                </div>
                <div class="col-md-4">
                    <input type="number" class="form-control" id="quantity" name="quantity" value="0">
                    <div class="error" id="error_title"></div>
                </div>
            </div><br>
    @endforeach
    <div class="form-group row">
        <label for="task_time" class="col-sm-2 col-form-label">Select Start Date</label>
        <div class="col-sm-10">
            <input type="date" id="start_date" class="form-control" required value="{{ date('d-m-Y h:i') }}"
                name="start_date">
            <div class="error" id="error_start_date"></div>
        </div>
    </div>
    <div class="form-group row">
        <label for="task_time" class="col-sm-2 col-form-label">Select End Date</label>
        <div class="col-sm-10">
            <input type="date" id="end_date" class="form-control" name="end_date">
            <div class="error" id="error_start_date"></div>
        </div>
    </div>
    <div class="form-group row">
        <div class="offset-sm-2 col-sm-10">
            <button type="submit" class="btn btn-success">Add Task</button>
            <button type="reset" class="btn btn-danger">Reset</button>
        </div>
    </div>
</form>

</div>

</form>
<script>

</script>
