<style>
    select#selectsatus {
        width: 100%;
    }

</style>
<div class="card">
    <div class="card-header ">
        <h3 class="card-title">Subscription Payment</h3>
    </div> <!-- /.card-header -->
    <div class="card-body">
        <table class="table table-bordered">

            @if (count($subDetails) > 0)
                <thead>
                    <tr>
                        <th>Project ID</th>
                        <th> ID</th>
                        <th>Project Title</th>
                        <th>Total Payment</th>
                        <th>Paid Payment</th>
                        <th>Pending Payment</th>
                        <th>Payment Method</th>
                        <th>Cancel Reason</th>
                        <th>Image</th>
                        <th>Created At</th>
                        <th>Due Date</th>

                    </tr>
                </thead>
            @endif
            <tbody>
                @forelse($subDetails as $key=>$value)
                    <tr>
                        <td> {{ $value->project->id }} </td>
                        <td> {{ $value->id }} </td>
                        <td>{{ $value->project->title }}</td>
                        <td>{{ $value->total }}</td>
                        <td>{{ $value->paid }}</td>
                        <td>{{ $value->pending }}</td>
                        <td>{{ $value->method }}</td>
                        <td>{{ $value->cancel_reason }}</td>
                        <td><a class="btn btn-success" href="{{ asset('subscription-images/' . $value->image) }}"
                            download>Download </a></td>
                        <td>{{ $value->created_at}}</td>
                        <td>{{date('d-m-Y', strtotime($value->due_date)) ??''}}</td>
                    <tr>
                    @empty
                        <center><h3> No Data Available </h3></center>
                @endforelse
            </tbody>
        </table>
    </div>
    {{ $subDetails->links() }}
    <!-- /.card-body -->
</div>
