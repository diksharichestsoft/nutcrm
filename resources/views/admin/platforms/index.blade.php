@section('contents')
<div class="card" id="data">
    <div class="card-header p-2" id="card_head">
      <ul class="nav nav-pills">
            <li class="nav-item search-right">
             <div class="search_bar">
                <div class="input-group" data-widget="sidebar-search">
                 <input class="form-control form-control-sidebar" id="search" type="search" placeholder="Search" aria-label="Search">
                </div>
             </div>
            </li>
      </ul>
    </div><!-- /.card-header -->

    <div class="card-body">

	<div id="createPlatformModalBlock">
		@include('admin.platforms.includes.create')
	</div>

	<div id="platforms_view">
        </div>

	<div id="updatePlatformModalBlock">

	</div>
      <!-- /.tab-content -->
    </div><!-- /.card-body -->
</div>
<script>

function refreshPlatformsView( userRequestedPage = '' ){

	var searched_keyword = $("#search").val();
	console.log( `user requested page: ${userRequestedPage}` );
	if ( userRequestedPage != '' ){
		var page = userRequestedPage;
	} else {
		var page = $('#platforms_pagination').data('page');
	}
	$.ajax({

		url: '{{ route("search-platforms") }}' +`?page=${page}`,
		type:'get',
		data: { searched_keyword },
		xhr: function() {
			myXhr = $.ajaxSettings.xhr();
			return myXhr;
		},

		beforeSend:  function(){
			$("#page-loader").show();
		},


		success: function( platformsHtml ){
			console.log( "page refreshed ");
			window.jsnn = platformsHtml;
			$("#platforms_view").empty().html( platformsHtml );
		},

		error: function( errorResponse ){
			console.log("page not refreshed");
		},

		complete: function(){
			$("#page-loader").hide();
		},
});

}

function saveNewPlatform( platformData ){

	console.log("save new platform");
	$('#page-loader').show();

	$.ajax({

		url:'{{ route("store-platform") }}',
		type:'post',
		dataType: "JSON",
		xhr: function() {
			myXhr = $.ajaxSettings.xhr();
			return myXhr;
		},

		cache: false,
		contentType: false,
		processData: false,
		data: platformData,
		success:  function( successResponse ){
			$("#addPlatformModal").modal('hide');;
			$('#page-loader').hide();
			refreshPlatformsView();
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Added Successfully',
                showConfirmButton: false,
                timer: 1500
                })
		},

		error: function( errorResponse ){

			console.log("next is errors");
			console.log( errorResponse );
			for (const [key, value] of Object.entries( errorResponse.responseJSON.errors)) {
				console.log( `key: ${key}, value: ${value}` );
				$(`#errors_${key}`).html( value );			}
	  		$('#page-loader').hide();

		}
		});
  }

/* Save new platform's helper */
  $(document).on('submit', '#add_platform_form', function(e) {
	  e.preventDefault();

	  var platformData = new FormData(this);
	  saveNewPlatform( platformData );
  });


  function deletePlatform( deletePlatformBtn )
  {
	  var platformId= deletePlatformBtn.getAttribute('data-id');

	  swal({
	  	title: "Oops....",
		text: "Are You Sure You want to delete!",
		icon: "error",
		buttons: [ 'NO', 'YES' ],
	}).then( function(isConfirm) {
		if (isConfirm) {
			console.log("is confirmed");
			 $.ajax({
					url:"{{ route('delete-platform') }}",
					type:"post",
					data:{ platformId },
					headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
					success: function(data){
						refreshPlatformsView();
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Remove Successfully',
                            showConfirmButton: false,
                            timer: 1500
                            })
					}
			});
		} else {
			console.log("not confirmed");
		}
	});

  }



  function renderUpdatePlatformModal( updatePlatformBtn )
  {
	$('#page-loader').show();

	var platformId = updatePlatformBtn.getAttribute('data-id');
	console.log("this is platform id: "+ platformId );
	$.ajax({
		url: "{{ route('edit-platform') }}",
		type: "get",
		data: { platformId },
		success: function( successResponse ){
			$('#page-loader').hide();
			$('#updatePlatformModalBlock').empty().html( successResponse );
			$("#updatePlatformModal").modal('show');
		},
		error: function(){
			$('#page-loader').hide();
		}
	})
  }

function updatePlatform( platformData ){

	console.log("update new platform");
	$('#page-loader').show();

	$.ajax({

		url:'{{ route("update-platform") }}',
		type:'post',
		dataType: "JSON",
		xhr: function() {
			myXhr = $.ajaxSettings.xhr();
			return myXhr;
		},

		cache: false,
		contentType: false,
		processData: false,
		data: platformData,
		success:  function( successResponse ){
			$("#updatePlatformModal").modal('hide');
		//	$('.modal-backdrop').hide();
			$('#page-loader').hide();
			refreshPlatformsView();
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Updated Successfully',
                showConfirmButton: false,
                timer: 1500
                })
		},

		error: function( errorResponse ){

			console.log("next is errors");
			console.log( errorResponse );
			for (const [key, value] of Object.entries( errorResponse.responseJSON.errors)) {
				console.log( `key: ${key}, value: ${value}` );
				$(`#errors_update_${key}`).html( value );
				$('#page-loader').hide();
			}
		}
	});
  }

  $(document).on('submit', '#update_platform_form', function(e) {
	  e.preventDefault();

	  var platformData = new FormData(this);
	  updatePlatform( platformData );
  });

document.querySelector("#search").addEventListener("keyup",(e)=>{
	refreshPlatformsView( 1);
  });

  /* Pagination */

  $(document).on('click', '.pagination a', function(event){

	          event.preventDefault();
		  console.log('ok');
		  var page = $(this).attr('href').split('page=')[1];
		  var sendurl=$(this).attr('href');

		  console.log( `page number: ${page}` );
		  refreshPlatformsView(page);
  });
  refreshPlatformsView();
</script>
