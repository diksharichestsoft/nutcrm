
<!-- Main content -->
@can ('platforms_create')
	    <a href="#" id="addPlatformButton" class="btn btn-success ml-4 " data-toggle="modal" data-target="#addPlatformModal">Add New Platform</a>
@endcan

    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!--  -->
                <div class="card-body">
		    <table class="table table-bordered">
			@if ( count( $platforms ) > 0 )
                      <thead>
			<tr>
			@canany (['platforms_edit', 'platforms_delete'] )
	                          <th style="width: 170px">Actions</th>	
			@endcanany
                          <th style="width: 10px">ID</th>
                          <th>Name</th>
                        </tr>
		      </thead>
			@endif
		      <tbody>
				@forelse ( $platforms as $platform )
				<tr>
				@canany (['platforms_edit', 'platforms_delete'] )
					<td>
						<div class="btn-group">
					@can ('platforms_edit')
								<button type="button" id="updatePlatformBtn" class="btn btn-outline-success btn-sm" data-id="{{ $platform->id }}" onclick=" renderUpdatePlatformModal( this )"><i class="fas fa-pencil-alt"></i></button>
					@endcan

					@can ('platforms_delete')
					<button type="button" id="deletePlatform" data-id="{{ $platform->id }}" class="btn btn-danger btn-sm" onclick=" deletePlatform( this )"><i class="fas fa-times"></i></button>
					@endcan
						</div>
					</td>
				@endcanany

					<td> {{ $platform->id }}</td>
					<td> {{ $platform->name }}</td>
				</tr>
				@empty
					<center> <h3> No Data Available! </h3></center>
				@endforelse
                      </tbody>
                    </table>
                </div>
                <!--  -->
            </div>
        </div>
     	<div class="ml-4" id="platforms_pagination" data-page="{{ $platforms->currentPage() }}"> {{  $platforms->links() }} </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
