<!-- Button trigger modal -->
  <!-- Modal -->
  <div class="modal fade" id="addPlatformModal" tabindex="-1" role="dialog"  aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Platform</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closePlatformModalBtn">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="add_platform_modal_body" class="modal-body">
	    <form id="add_platform_form" action="#" method="POST">
		@csrf
		<div class="form-group">
			<input type="text" class="form-control" id="platformName" placeholder="Name" maxlength="255" required name="name">
			<small id="errors_name" class="form-text  font-weight-bold text-danger"></small>
		</div>
                <button type="submit" class="btn btn-success"> Save</button>
            </form>
	</div>
      </div>
    </div>
  </div>

