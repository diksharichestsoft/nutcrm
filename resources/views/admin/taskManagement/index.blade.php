<style>
    .daterangepicker .ranges li {font-size: 12px;padding: 8px 12px;cursor: pointer;background: #000000b0;}
  </style>
  <div class="card" id="data">
                <div class="card-header p-2" id="card_head">
                  <ul class="nav nav-pills">

                    @foreach($taskStatus as $eachTaskStatus)
                    <li class="nav-item"><a class="nav-link services" href="#view" onclick="fetch_data__data(1,this.getAttribute('data-id'))" data-id="{{$eachTaskStatus->id}}" data-toggle="tab">{{$eachTaskStatus->title}}</a></li>&nbsp;
                    @endforeach
                    <li class="nav-item"><a class="nav-link services" href="#view" onclick="fetch_data__data(1,this.getAttribute('data-id'))" data-id="6" data-toggle="tab">Rejected By me</a></li>&nbsp;
                    {{-- <li class="nav-item"><a class="nav-link services" href="#view" onclick="fetch_data__data(1,this.getAttribute('data-id'))" data-id="-1" data-toggle="tab">All Tasks</a></li>&nbsp; --}}
                    <div id="reportrange" style="background: transparent; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                    <i class="fa fa-calendar"></i>&nbsp;
                    <span class="ab"></span> <i class="fa fa-caret-down"></i>
                   </div>
                   <input type="hidden" id="start_date_range">
                   <input type="hidden" id="end_date_range">
                   <li style="align-self: center;">
                       <h3>
                       </h3>
                   </li>
                    <li class="nav-item search-right">
                        @can('task_management_add')

                        <div>
                            <a href="javascript:void(0)" onclick="addTasks()"  class="btn btn-primary" >ADD/Send Tasks</a>
                        </div>
                        @endcan
                     <div class="search_bar">
                        <div class="input-group" data-widget="sidebar-search">
                         <input class="form-control form-control-sidebar" id="search" type="search" placeholder="Search" aria-label="Search">
                        </div>
                     </div>
                    </li>
                  </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                  <div class="tab-content">
                    <div class="tab-pane" id="profile">
                       {{-- @include('admin.permissions.includes.addform') --}}
                    </div>

                    <div class="active tab-pane" id="view">
                      </div>
                    <!-- /.tab-pane -->
                  </div>
                  <!-- /.tab-content -->
                </div><!-- /.card-body -->
        </div>

  <div class="modal fade" id="changestatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel1">Assign Task</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="changeStatuSbody">
          <form action="" name="changestatusform" id="changestatusform">
              @csrf
              <input type="hidden" name='status' id="changeStatus-status" value="">
              <input type="hidden" name='deal_id' id="changeStatus-deal_id" value="">
              <div class="value">
              <div class="form-group row">
                <label for="comment" class="col-sm-2 col-form-label">Comment</label>
                <div class="col-sm-10">
                  <textarea name="comment"  cols="30" rows="10" class="form-control"></textarea>
                  <div class="error" id="error_comment"></div>
                </div>
              </div>
              <div class="form-group row">
                <div class="offset-sm-2 col-sm-10">
                  <button type="submit" class="btn btn-success">Submit</button>
                </div>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="assingTaskModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel1">Assign Task</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="changeStatuSbody">
          <form action="" name="assignTaskForm" id="assignTaskForm">
              @csrf
              <input type="hidden" name='task_id' id="assignTask-task_id" value="">
              <div class="value">
                <div class="form-group row">
                    <label for="comment" class="col-sm-2 col-form-label">Assign Task To</label>
                    <div class="col-sm-10">
                        <select name="user_id[]" class="form-select select2" id="employees_select" aria-label="Default select example" multiple>
                            <option selected>Open this select menu</option>
                        </select>
                        <div class="error" id="error_user_id"></div>
                    </div>
                </div>
              <div class="form-group row">
                <label for="comment" class="col-sm-2 col-form-label">Comment</label>
                <div class="col-sm-10">
                  <textarea name="comment"  cols="30" rows="10" class="form-control"></textarea>
                  <div class="error" id="error_comment"></div>
                </div>
              </div>
              <div class="form-group row">
                <div class="offset-sm-2 col-sm-10">
                  <button type="submit" class="btn btn-success">Submit</button>
                </div>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="createDeal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Projects</h5>
          <button type="button" class="close" id='createDealHide' data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body " id="createDealBody">
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="addComment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Comment</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body " id="addCommentBody">
        <form action="" name="addCommentForm" id="addCommentForm">
            @csrf
            <input type="hidden" name='deal_id' id="deal_id" value="">
          <div class="value">
            <div class="form-group row">
              <label for="comment" class="col-sm-2 col-form-label">Comment</label>
              <div class="col-sm-10">
                <textarea name="comment" id="comment" cols="30" rows="10" class="form-control"></textarea>
                <div class="error" id="error_comment"></div>
              </div>
            </div>
            <div class="form-group row">
              <div class="offset-sm-2 col-sm-10">
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </div>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script>
    $(document).ready(function(){
      $(".services").eq(0).addClass("active");
    });

    function accept(e)
    {
        var id=e.getAttribute('data-id');
        var type=e.getAttribute('data-type');
        $.ajax({
        url:"{{route('manageTasks-accept')}}",
        type:"post",
        data:{id:id,type:type},
        headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
        success:function(data)
        {
            fetch_data__data(1, '');
        }
        })
    }


    $(document).on('click','.comment',function(){
      var id = $(this).attr('data-id');
      // alert(id);
      $('#deal_id').val(id);
      $('#comment_title').val('');
      $('#comment').val('');

    });


  function updateCompanyProject(e)
  {
    $('#page-loader').show();

    var id=e.getAttribute('data-id');
    $.ajax({
      url:"{{route('manageTasks-edit')}}",
      type:"get",
      data:{id:id},
      success:function(data)
      {
          $("#createDeal").modal('show')
        $('#page-loader').hide();
          $("#createDealBody").html(data)
      },
      error:function(){
        $('#page-loader').hide();
      }
    })
  }
  $(document).on('submit','#addTask',function(e){
    e.preventDefault();
    Swal.showLoading()
    var descData = (myEditor.getData());
    $("#description").val(descData);
    var data = new FormData(this);
      $.ajax({
          type:'post',
          url:"{{route('manageTasks-store')}}",
          dataType: "JSON",
         xhr: function() {
               myXhr = $.ajaxSettings.xhr();
               return myXhr;
         },
         cache: false,
         contentType: false,
         processData: false,
          data:data,
          success:function(data){
            Swal.close()
              $('#createDeal').modal('hide');
              fetch_data__data(1, '');
            //   Swal.fire({
            //     position: 'top-end',
            //     icon: 'success',
            //     title: 'Added Successfully',
            //     showConfirmButton: false,
            //     timer: 1500
            //     })

          },
          error:function(data){
            Swal.close()
            $.each(data.responseJSON.errors, function(id,msg){
              $('#error_'+id).html(msg);
            })
          }
        });
      });

  $(document).on('submit','#addCommentForm',function(e){
    e.preventDefault();
    var data = new FormData(this);
      $.ajax({
        type:'post',
        url:"{{route('manageTasks-add-Comment')}}",
        dataType: "JSON",
        xhr: function() {
              myXhr = $.ajaxSettings.xhr();
              return myXhr;
        },
        cache: false,
        contentType: false,
        processData: false,
        data:data,
          success:function(data){
              $('#addComment').modal('hide');
	      fetch_data__data(1,'');
	      myEditor.setData('');
          },
          error:function(data){
            $.each(data.responseJSON.errors, function(id,msg){
              $('.error#error_'+id).html(msg);
            })
          }
        });
      });


    $(document).on('change','#selectsatus',function(){
      oldstatus = $(this).data('status')
      var status =$(this).val();
      $(this).val(oldstatus);
      var deal_id = $(this).attr('data-id');
      $('#changeStatus-status').val(status);
      $('#changeStatus-deal_id').val(deal_id);
      $('#changestatusform textarea').val('');
      $('#changestatus').modal('show');
    });

        $(document).on('click','.assignButton',function(){
        var task_id = $(this).attr('data-id');
        $('#assingTaskModal-status').val(status);
        $('#assignTask-task_id').val(task_id);
        $('#assingTaskModal textarea').val('');
        $
        $.ajax({
            type:'get',
            url:"{{route('get-all-employee-list')}}",
            data: {"status":status},
            dataType: "JSON",
            xhr: function() {
                myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },
            cache: false,
            contentType: false,
            processData: false,
            success:function(data){

                $('#employees_select').empty();
                $.each(data.reasons, function(index, item) {
                $('#employees_select').append(`<option value="${item.id}">${item.name}</option>`)
            });
            },
            error:function(data){
            //   $.each(data.responseJSON.errors, function(id,msg){
            //     $('#error_'+id).html(msg);
            //   })
            }
            });
        $('#assingTaskModal').modal('show');
        });

    $(document).on('submit','#changestatusform',function(e){
        e.preventDefault();
          var data = new FormData(this);
      $.ajax({
          type:'post',
          url:"{{route('manageTasks-change-status')}}",
          dataType: "JSON",
         xhr: function() {
               myXhr = $.ajaxSettings.xhr();
               return myXhr;
         },
         cache: false,
         contentType: false,
         processData: false,
          data:data,
          success:function(data){
              $('#changestatus').modal('hide');
              fetch_data__data(1, '');
          },
          error:function(data){
            $.each(data.responseJSON.errors, function(id,msg){
              $('#error_'+id).html(msg);
            })
          }
        });
  });
    $(document).on('submit','#assignTaskForm',function(e){
        e.preventDefault();
        Swal.showLoading()
          var data = new FormData(this);
      $.ajax({
          type:'post',
          url:"{{route('manageTasks-assign-to-employee')}}",
          dataType: "JSON",
         xhr: function() {
               myXhr = $.ajaxSettings.xhr();
               return myXhr;
         },
         cache: false,
         contentType: false,
         processData: false,
          data:data,
          success:function(data){
            Swal.close()
              $('#assingTaskModal').modal('hide');
              fetch_data__data(1,'');
          },
          error:function(data){
            Swal.close()
            $.each(data.responseJSON.errors, function(id,msg){
              $('#error_'+id).html(msg);
            })
          }
        });
  });

  function removeTask(e)
  {
    var id=e.getAttribute('data-id');
    swal({
    title: "Oops....",
    text: "Are You Sure You want to delete!",
    icon: "error",
    buttons: [
      'NO',
      'YES'
    ],
  }).then(function(isConfirm) {
    if (isConfirm) {
      $.ajax({
        url:"{{route('manageTasks-remove-data')}}",
        type:"post",
        data:{id:id},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        success:function(data){
          $('#view').empty().html(data);
          fetch_data__data(1,'');
        }
      })
    } else {

    }
  });
  }
  function resendTask(e)
  {
    var id=e.getAttribute('data-id');
    swal({
    title: "Resend Task?",
    text: "Are You Sure You want to Resend Task? If you want to send someone else, edit the task and choose the employee you want to send, then click this resend button",
    icon: "info",
    buttons: [
      'NO',
      'YES'
    ],
  }).then(function(isConfirm) {
    if (isConfirm) {
      $.ajax({
        url:"{{route('manageTasks-resendTask')}}",
        type:"post",
        data:{id:id},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        success:function(data){
          $('#view').empty().html(data);
          fetch_data__data(1,'');
        }
      })
    } else {

    }
  });
  }

  function addTasks()
  {
    $('#page-loader').show();

    $.ajax({
      url:"{{route('manageTasks-add')}}",
      type:"get",
      data: {id: "{{$project_id ?? ''}}"},
      success:function(data)
      {
          $("#createDeal").modal('show')
        $('#page-loader').hide();
        $('#createDealBody').empty().html(data);
      },
      error:function(error){
        $('#page-loader').hide();
      }
    })
  }


  // run on  change date start to end

  $(function() {
  var start = moment().subtract(29, 'days');
  var end = moment();
  function cb(start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      $('#start_date_range').val(start.format('YYYY-MM-DD'));
      $('#end_date_range').val(end.format('YYYY-MM-DD'));
      fetch_data__data(1,'');
  }

  $('#reportrange').daterangepicker({
      startDate: start,
      endDate: end,
      ranges: {
         'Today': [moment(), moment()],
         'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
         'Last 7 Days': [moment().subtract(6, 'days'), moment()],
         'Last 30 Days': [moment().subtract(29, 'days'), moment()],
         'This Month': [moment().startOf('month'), moment().endOf('month')],
         'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
  }, cb);
  cb(start, end);

  });

  $(document).on('click', '.pagination a', function(event){
    event.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    var sendurl=document.getElementsByClassName('services active')[0].getAttribute('data-id');
    fetch_data__data(page,sendurl,'');

  });
  document.querySelector("#search").addEventListener("keyup",(e)=>{
      // var active=$('a.active.services').attr('data-id');
      fetch_data__data(1,'');
  });

  function fetch_data__data(page,active)
  {
    $('#page-loader').show();

    var start_date=document.querySelector('#start_date_range').value;
    var end_date=document.querySelector('#end_date_range').value;
    if(active==''){
      var active=document.getElementsByClassName('services active')[0].getAttribute('data-id');
    }

    var search=document.querySelector("#search").value;
    var project_id = "{{$project_id ?? ''}}";
    var data={search:search,active:active,start_date:start_date,end_date:end_date, project_id: project_id};
    var make_url= "{{url('/')}}/admin/manageTasks/search?page="+page;
    $.ajax({
      url:make_url,
      data:data,
      success:function(data)
      {
          $('#view').empty().html(data);
        $('#page-loader').hide();

      },
      error:function(error){
        $('#page-loader').hide();
      }
    });
    }

    function leadingZeros(input) {
    if(!isNaN(input.value) && input.value.length === 1) {
      input.value = '0' + input.value;
    }
  }


//  Task timer funcitons
function startTimerAjax (thisButton,selector) {
    id = $(thisButton).data('id');
    seconds = $(thisButton).data('seconds')
    $.ajax({
      url:"{{route('quick-task-timer-start')}}",
      type: 'post',
      headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
      data:{id},
      success:function(data)
      {
        if(data.status == 0){
                  $('#changestatus').modal('hide');
                  Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: data.message,
                        showConfirmButton: false,
                        timer: 7000
                    })
                    return
              }else{
                  if(data.isRunning == 0){
                        Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: "Timer Stoped",
                                showConfirmButton: false,
                                timer: 3000
                            })
                        $(thisButton).html('START')
                        $(thisButton).addClass('btn-success')
                        $(thisButton).removeClass('btn-danger')
                    }else{
                        Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: "Timer Started",
                                showConfirmButton: false,
                                timer: 2000
                        });
                        $(thisButton).html('PAUSE')
                        $(thisButton).removeClass('btn-success')
                        $(thisButton).addClass('btn-danger')

                  }
              }
        running = (data.isRunning) ? 1 : 0
        myCounter(data.seconds,running,selector)
        // startPause(thisButton, data.seconds);
      },
      error:function(error){
        alert(error)
        console.log(error)
      }
    });

 }
let timer;
function myCounter(seconds,OnOff,selector) {
    timeHMS = seconds;
  if(OnOff){
    timer=setInterval(()=>{
    // document.querySelector(`${selector}`).innerHTML=new Date(seconds * 1000).toISOString().substr(11, 8);
    document.querySelector(`${selector}`).innerHTML= toHHMMSS(seconds);
    ++seconds;
  },1000);
  }else{
    clearInterval(timer);
    document.querySelector(`${selector}`).innerHTML=toHHMMSS(seconds);
    // document.querySelector(`${selector}`).innerHTML=new Date(seconds * 1000).toISOString().substr(11, 8);
    return ;
  }
}

const toHHMMSS = (secs) => {
    var sec_num = parseInt(secs, 10)
    var hours   = Math.floor(sec_num / 3600)
    var minutes = Math.floor(sec_num / 60) % 60
    var seconds = sec_num % 60

    return [hours,minutes,seconds]
        .map(v => v < 10 ? "0" + v : v)
        .filter((v,i) => v !== "00" || i > 0)
        .join(":")
}

  </script>
  <script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/inline/ckeditor.js"></script>

