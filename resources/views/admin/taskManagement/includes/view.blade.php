<style>
    select#selectsatus {
      width: 100%;
  }
  </style>

  <div class="card">
    <div class="card-header ">
      <h3 class="card-title">Your Quick Tasks</h3>
      <h6 class="count text-right">Total Quick Tasks : {{$data->total()}}</h6>
    </div>
                <!-- /.card-header -->
               <div class="card-body">
                  <table class="table table-bordered">
                    @if(count($data)>0)
                    <thead>
                      <tr>
                        <th style="width: 10px">Action</th>
                        <th>ID</th>
                        {{-- @if(isset($active) && $active==2)
                        <th>Timer</th>
                        @endif --}}
                        <th>Title</th>
                        <th>Summary</th>
                        <th>Project</th>
                        <th>Assigned By</th>
                        <th>Assigned To</th>
                        <th>Time</th>
                        <th>Created</th>
                        @if(isset($active) && $active==2)
                        @can('tasks_change_status')
                        <th>Action</th>
                        @endcan
                        @endif
                        @if(isset($active) && $active==4)
                        <th>Status</th>
                        @endif
                      </tr>
                    </thead>
                    @endif
                    <tbody>
                          @forelse($data as $key=>$value)
                             <tr>
                                  <td>
                                    <div class="btn-group">
                                        @if($value->child_status_id==1 && $value->send_to == Auth::user()->id)
                                            <button data-id="{{$value->id}}" data-type="1" onclick="accept(this)" class="btn btn-primary btn-xs remove">Accept</button>
                                        @endif
                                        @if($active != 6)
                                        @can('task_management_view')
                                        <a href="{{route('manageTasks-detial-view',$value->id)}}" target="_blank" class="btn btn-outline-success btn-xs  view"><i class="fas fa-eye"></i></a>
                                        @endcan
                                        @can('task_management_view')
                                        @endcan
                                        @if($value->send_by == Auth::user()->id)
                                        @can('task_management_edit')
                                        <button data-id="{{$value->id}}" onclick="updateCompanyProject(this, 0)" class="btn btn-outline-success btn-xs  update" ><i class="fas fa-pencil-alt"></i></button>
                                        @endcan
                                        @can('task_management_delete')
                                        @if($value->child_status_id == 1)
                                        <button data-id="{{$value->id}}" onclick="removeTask(this)" class="btn btn-danger btn-xs  remove"><i class="fas fa-times"></i></button>
                                        @endif
                                        @endcan
                                        @endif

                                        @if($value->child_status_id==1 && $value->send_to == Auth::user()->id)
                                            <button data-id="{{$value->id}}" data-type="0" onclick="accept(this)" class="btn btn-danger btn-xs remove">Reject</button>
                                        @endif
                                        @endif
                                    </div>
                                  </td>
                                  <td>{{$value->id??''}}</td>
                                  {{-- @if(isset($active) && $active==2)
                                  <td>
                                      @php
                                          $secs = (int)$value->authUserTimerOnThisTask();
                                          $diffMinSec = gmdate("i:s", $secs);
                                          $diffHours = (gmdate("d", $secs)-1)*24 + gmdate("H", $secs);
                                      @endphp

                                      <p class = "text-center time-output" id="time_output_{{$value->id}}">{{$diffHours.":".$diffMinSec}}</p>
                                      <div id = "controls" class = "text-center">
                                        <button class="btn {{($value->haveTimerRunning()) ?  "btn-danger" : "btn-success"}}" id="time_button_{{$value->id}}" data-seconds="{{$secs}}" data-id="{{$value->id}}" onclick = "startTimerAjax(this,'#time_output_{{$value->id}}')">{{($value->haveTimerRunning()) ?  "PAUSE" : "START"}}</button>
                                      </div>
                                      @php
                                          if($value->haveTimerRunning()){
                                              $diffTimer = Carbon\Carbon::parse($value->latestTimer[0]->start_time)->diffInSeconds(now());
                                              echo "<script>myCounter('".($secs+$diffTimer)."', true, '#time_output_".$value->id."')</script>";
                                          }
                                      @endphp
                                  </td>
                                  @endif --}}


                                  <td>{{$value->title??''}}</td>
                                  <td>{{$value->summary??''}}</td>
                                  <td>
                                      @if($value->project)
                                  <a href="{{route('projects-view',$value->project->id)}}" target="_blank">{{$value->project->title}}</a>

                                    @else
                                    <b>No Project!</b>
                                  @endif
                                  </td>
                                   <td>{{$value->assignedBy->name}}</td>
                                  <td>
                                    {{DB::table('users')->where('id', $value->send_to)->first('name')->name ?? ''}}
                                  </td>
                                  <td>{{$value->time}}</td>
                                  <td>
                                      {{timestampToDateAndTimeObj($value->created_at)->date}}<br>
                                      {{timestampToDateAndTimeObj($value->created_at)->time}}
                                    </td>
                                  @if($value->deal_status!=1)
                                  @can('tasks_change_status')

                                @if(isset($active) && $active==2)

                                  <td class="d-flex">
                                      <select name="sel" id="selectsatus" class="form-control deals_status" data-status="{{$value->deal_status}}" data-id="{{ $value->id }}" >
                                        <option value="0">Select Status</option>
                                        @foreach($taskStatus as $status)
                                        @if($status->id != 4 && $status->id != 5 )
                                        <option value="{{$status->id}}"  {{($value->deal_status==$status->id)?"selected":""}}>{{$status->title}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </td>
                                @endif
                                @endcan
                                @endif
                                @if(isset($active) && $active==4 && $value->active == 3)
                                <td><button class="btn btn-warning" data-id="{{$value->id}}" onclick="resendTask(this)">Rejected, Resend?</button></td>

                                @endif
                                @if(isset($active) && $active==4 && $value->active == 2)
                                <td>Accepted</td>
                                @endif
                                @if(isset($active) && $active==4 && $value->active == 1)
                                <td>In Queue</td>
                                @endif

                             </tr>
                          @empty
                            <center> <h3> No Data Available </h3> </center>
                          @endforelse
                    </tbody>
                  </table>
                </div>
                {{$data->links()}}
                <!-- /.card-body -->
              </div>







    <script>

      function save_deal_data()
      {
          var active=$('a.active.services').attr('data-id');
          var start_date=document.querySelector('#start_date_range').value;
          var end_date=document.querySelector('#end_date_range').value;
      }
    </script>
