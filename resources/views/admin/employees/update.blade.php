@extends('admin.layout.template')
@section('contents')

<style>
     .select2-selection{
         background-color:#343a40 !important;
     }
</style>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Update User</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <div id="validation-errors"></div>

                    <form method="post" action="" id="add_user_form" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">

                        <div class="form-group">
                            <label for="company">Company Name</label>
                            <select name="company_id" id="company" class="form-control" required>
                              <option value="">Select Team Lead</option>
                              @foreach($companies as $company)
                              <option value="{{$company->id}}" {{($company->id == $thisEmployee->company_id)? 'selected' : ''}}>{{$company->name}}</option>
                              @endforeach
                            </select>
                            @error('company_id')
                                <div class="error error-msg-red">{{ $message }}</div>
                            @enderror
                        </div>
                        <input type="hidden" name="reqType" value="1">
                        <input type="hidden" name="id" value="{{$thisEmployee->id}}">

                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" id="name" class="form-control" value="{{old('name') ?? $thisEmployee->name}}" name="name" required>
                        </div>

                        <div class="form-group">
                            <label for="profile_image">Image</label>
                            <input type="file" id="profile_image" class="form-control" name="profile_image" accept="image/*">
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" value="{{old('email') ?? $thisEmployee->email}}" required>
                        </div>

                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password">
                        </div>
                        <div class="form-group">
                            <label for="slack">slack ID</label>
                            <input type="text" class="form-control" name="slack_id" value="{{old('slack_id') ?? $thisEmployee->slack_id}}" required>
                        </div>

                        <div class="form-group">
                            <label for="salary">Salary</label>
                            <input type="number" class="form-control" name="salary" value="{{old('salary') ?? $thisEmployee->salary}}">
                        </div>

                        <div class="form-group">
                            <label for="security_amount">Security Amount</label>
                            <input type="number" class="form-control" value="{{old('security_amount') ?? $thisEmployee->security_amount}}" name="security_amount" required>
                        </div>

                        <div class="form-group">
                            <label for="experience">Experience(In years)</label>
                            <input type="number" class="form-control" value="{{old('experience') ?? $thisEmployee->experience}}" name="experience" required>
                        </div>

                        <div class="form-group">
                            <label for="qualification">Qualification</label>
                            <input type="text" class="form-control" value="{{old('qualification') ?? $thisEmployee->qualification}}" name="qualification" required>
                        </div>

                        <div class="form-group">
                            <label for="specialization">Specialization</label>
                            <input type="text" class="form-control" value="{{old('specialization') ?? $thisEmployee->specialization}}" name="specialization">
                        </div>

                        <div class="form-group">
                            <label for="date_of_joining">Date Of Joining</label>
                            <input type="date" class="form-control" value="{{old('date_of_joining') ?? $thisEmployee->date_of_joining}}" name="date_of_joining" required>
                        </div>
                        <div class="form-group">
                            <label for="date_of_birth">Date Of Birth</label>
                            <input type="date" class="form-control" name="date_of_birth" value="{{old('date_of_birth') ?? $thisEmployee->date_of_birth}}" required>
                        </div>

                        <div class="form-group">
                            <label for="phone">Phone Number</label>
                            <input type="text" class="form-control" value="{{old('phone') ?? $thisEmployee->phone}}" name="phone"  required>
                        </div>

                        <div class="form-group">
                            <label for="phone_number_2">Phone Number Alternate</label>
                            <input type="text" class="form-control" value="{{old('phone_number_2') ?? $thisEmployee->phone_number_2}}" name="phone_number_2" >
                        </div>

                        <div class="form-group">
                            <label for="phone_number_mother">Phone Number Mother</label>
                            <input type="text" class="form-control" value="{{old('phone_number_mother') ?? $thisEmployee->phone_number_mother}}" name="phone_number_mother" required>
                        </div>

                        <div class="form-group">
                            <label for="phone_number_father">Phone Number Father</label>
                            <input type="text" class="form-control" value="{{old('phone_number_father') ?? $thisEmployee->phone_number_father}}" name="phone_number_father" required>
                        </div>

                        <div class="form-group">
                            <label for="phone_number_sibling">Phone Number Brother/Sister</label>
                            <input type="text" class="form-control" value="{{old('phone_number_sibling') ?? $thisEmployee->phone_number_sibling}}" name="phone_number_sibling" >
                        </div>

                        <div class="form-group">
                            <label for="monthly_target">Monthly Target</label>
                            <input type="text" class="form-control" value="{{old('monthly_target') ?? $thisEmployee->monthly_target}}" name="monthly_target">
                        </div>

                        <div class="form-group">
                            <label for="designation">Designation</label>
                            <input type="text" class="form-control" value="{{old('designation') ?? $thisEmployee->designation}}" name="designation">
                        </div>

                        <div class="form-group">
                            <label for="login_toggle">Login</label>
                            <input type="hidden" class="form-control" value="0" name="login_toggle">
                            <input type="checkbox" class="form-control" value="1" style="width: 20px" name="login_toggle" checked>

                        </div>
                        <div class="form-group">
                            <label for="team_lead">Team Lead</label>
                            <select name="team_lead[]" id="team_lead" class="form-control select2" multiple>
                              <option disabled>Select Team Lead</option>
                              @foreach($users as $user)
                              <option value="{{$user->id}}" {{ $thisEmployeeTeamLeadIds->contains( $user->id ) ? 'selected' : '' }}>{{$user->name}}</option>
                              @endforeach
                            </select>
                            @error('team_lead')
                                <div class="error error-msg-red">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="team_lead">Department</label>
                            <select name="department_id[]" id="department" class="form-control select2" multiple required>
                              <option value="">Select Departement</option>
                              @foreach($departments as $department)
                              <option value="{{$department->id}}" {{ $thisEmployeeDepartmentsIds->contains( $department->id ) ? 'selected' : '' }}>{{$department->name}}</option>
                              @endforeach
                            </select>
                            @error('team_lead')
                                <div class="error error-msg-red">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="team_lead">Roles</label>
                            <select name="roles[]" id="permissions" class="form-control select2" multiple required>
                              <option value="">Select Roles</option>
                              @foreach($roles as $role)
                              <option value="{{$role->id}}"  {{ $thisEmployee->hasRole( $role->name )  ? 'selected' : ''}}> {{$role->name}}</option>
                              @endforeach
                            </select>
                            @error('permissions')
                                <div class="error error-msg-red">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="description">Description</label>
                            <input type="text" class="form-control" value="{{$thisEmployee->description}}" name="description">
                        </div>

                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" class="form-control" value="{{$thisEmployee->address}}" name="address" required>
                        </div>

                        <div class="form-group">
                            <label for="age">Age</label>
                            <input type="number" class="form-control" value="{{$thisEmployee->age}}" name="age">
                        </div>

                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!--  -->
            </div>
        </div>

      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
    <script>
function getFormWithImage(id) {
            let form = document.querySelector(`#${id}`);
            let data = new FormData(form);
            return data;
        }
        $("#add_user_form").on('submit', function (e) {
            e.preventDefault();
            Swal.showLoading()
            var data = getFormWithImage("add_user_form");

            $.ajax({
                url:"{{route('employee-store')}}",
                type:"post",
                cache: false,
                contentType: false,
                processData: false,
                dataType: "JSON",
                data:data,
                xhr: function() {
                    myXhr = $.ajaxSettings.xhr();
                    return myXhr;
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                success:function(data){
                    Swal.close()
                    window.location.href = "{{route('employees')}}"
                    // Swal.fire({
                    //     position: 'top-end',
                    //     icon: 'success',
                    //     title: 'Updated Successfully',
                    //     showConfirmButton: false,
                    //     timer: 1500
                    //     })
                },
                error:function(data){
                    Swal.close()
                    data =data.responseJSON.errors;
                    for (const [key, value] of Object.entries(data)) {
                        $('#validation-errors').append('<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Error: </strong>'+value+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                        }
                        $("html, body").animate({ scrollTop: 0 }, "slow");
                }

                })
         })

    </script>
@endsection

