@section('contents')


<div class="card" id="data">
    <div class="card-header p-2" id="card_head">
      <ul class="nav nav-pills">
       <input type="hidden" id="start_date_range" >
       <input type="hidden" id="end_date_range">
        <li class="nav-item search-right">

          <div>
          </div>
         <div class="search_bar">
            <div class="input-group" data-widget="sidebar-search">
             <input class="form-control form-control-sidebar" id="search" type="search" placeholder="Search" aria-label="Search">
            </div>
         </div>
        </li>
      </ul>
    </div><!-- /.card-header -->
    <div class="card-body">

        <div id="pagination_employee">
            @include('admin.employees.includes.view')
        </div>

      <!-- /.tab-content -->
    </div><!-- /.card-body -->
</div>








<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
document.querySelector("#search").addEventListener("keyup",(e)=>{
        var search = $("#search").val();
      filterEmpData(search);
  });


    $(document).on('click', '.pagination a', function(event){
  event.preventDefault();
  var page = $(this).attr('href').split('page=')[1];
  var sendurl=$(this).attr('href');
  getEmployeeData(page);
});
function getEmployeeData(page)
{
  $('#page-loader').show();
//   page = (page='')?'1':page;
  var data={};
  var make_url= "{{url('/')}}/admin/employees/page?page="+page;
  $.ajax({
    url:make_url,
    data:data,
    success:function(data)
    {
        $('#pagination_employee').empty().html(data);
      $('#page-loader').hide();

    },
    error:function(error){
      $('#page-loader').hide();

    }
  });
  }

function filterEmpData(search){
    $('#page-loader').show();
    var data={};
  var make_url= "{{url('/')}}/admin/employees/search?search="+search;
  $.ajax({
    url:make_url,
    data:data,
    success:function(data)
    {
        $('#pagination_employee').empty().html(data);
      $('#page-loader').hide();

    },
    error:function(error){
      $('#page-loader').hide();

    }
  });}
</script>
@endsection
