@extends('admin.layout.template')

@section('contents')
    <div class="card">
        <div class="card-header" id="headingOne">
            <div class="h4">
                Update More Details
                </a>
            </div>
        </div>
        <div class="card-body">
            <form action="" id="updateEmployeeForm" enctype="multipart/form-data">
                @csrf
                <input type="hidden" class="form-control" value="{{ $thisEmployee->id ?? '' }}" name="id">
                <div class="value">
                    <div class="form-group">
                        <label for="profile_image">Employee Image :</label>
                        <div class="row">
                            <div class="col-md-8">
                                <input type="file" id="profile_image" class="form-control"
                                    name="profile_image" accept="image/*" >
                            </div>
                            <div class="col-md-4">
                                <a class="btn btn-success"
                                    href="{{ asset('images/employees/'. $thisEmployee->profile_image)}}" download>
                                    Download </a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="aadhar_card_front_image">Aadhar card front Image:</label>
                        <div class="row">
                            <div class="col-md-8">
                                <input type="file" id="aadhar_card_front_image" class="form-control"
                                    name="aadhar_card_front_image" accept="image/*" >
                            </div>
                            <div class="col-md-4">
                                <a class="btn btn-success"
                                    href="{{ asset('images/employees/'. $thisEmployee->aadhar_card_front_image)}}" download>
                                    Download </a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="aadhar_card_back_image">Aadhar card back Image:</label>
                        <div class="row">
                            <div class="col-md-8">
                                <input type="file"
                                    id="aadhar_card_back_image" class="form-control" name="aadhar_card_back_image"
                                    accept="image/*" >
                            </div>
                            <div class="col-md-4">
                                <a class="btn btn-success"
                                    href="{{ asset('images/employees/' . $thisEmployee->aadhar_card_back_image) }}" download>
                                    Download </a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="driver_license_image">Driver license :</label>
                        <div class="row">
                            <div class="col-md-8">
                                <input type="file" id="driver_license_image"
                                 class="form-control"
                                    name="driver_license_image" accept="image/*" >
                            </div>
                            <div class="col-md-4">
                                <a class="btn btn-success"
                                    href="{{ asset('images/employees/' . $thisEmployee->driver_license_image) }}" download>
                                    Download </a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pan_card_image">Pan Card :</label>
                        <div class="row">
                            <div class="col-md-8">
                                <input type="file" id="pan_card_image"
                                    class="form-control" name="pan_card_image" accept="image/*" >
                            </div>
                            <div class="col-md-4">
                                <a class="btn btn-success"
                                    href="{{ asset('images/employees/' . $thisEmployee->pan_card_image) }}" download>
                                    Download </a>
                            </div>
                        </div>
                    </div>
                     <div class="form-group">
                        <label for="voter_id_image">Voter ID :</label>
                        <div class="row">
                            <div class="col-md-8">
                                <input type="file" id="voter_id_image"
                                    class="form-control" name="voter_id_image" accept="image/*" >
                            </div>
                            <div class="col-md-4">
                                <a class="btn btn-success"
                                    href="{{ asset('images/employees/' . $thisEmployee->voter_id_image) }}" download>
                                    Download </a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="passport_image">Passport Front Image :</label>
                        <div class="row">
                            <div class="col-md-8">
                                <input type="file" id="passport_image"class="form-control" name="passport_image" accept="image/*" >
                            </div>


                            <div class="col-md-4">
                                <a class="btn btn-success"
                                    href="{{ asset('images/employees/' . $thisEmployee->passport_image) }}" download>
                                    Download </a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="passport_back_image">Passport Back Image:</label>
                        <div class="row">
                            <div class="col-md-8">
                                <input type="file" id="passport_back_image"class="form-control" name="passport_back_image" accept="image/*" >
                            </div>


                            <div class="col-md-4">
                                <a class="btn btn-success"
                                    href="{{ asset('images/employees/' . $thisEmployee->passport_back_image) }}" download>
                                    Download </a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                            <button type="submit" class="btn btn-success">Update Detail</button>
                            <button type="reset" class="btn btn-danger">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>


        function getFormWithImage(id) {
            let form = document.querySelector(`#${id}`);
            let data = new FormData(form);
            return data;
        }
        $("#updateEmployeeForm").on('submit', function(e) {
            e.preventDefault();
            var data = getFormWithImage("updateEmployeeForm");
            $.ajax({
                url: "{{ route('employee-store-moreinfo') }}",
                type: "post",
                cache: false,
                contentType: false,
                processData: false,
                dataType: "JSON",
                data: data,
                xhr: function() {
                    myXhr = $.ajaxSettings.xhr();
                    return myXhr;
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                   //  window.location.href = "{{ route('employees') }}"
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Updated Successfully',
                        showConfirmButton: true,
                    })
                },
                error: function(data) {
                    data = data.responseJSON.errors;
                    for (const [key, value] of Object.entries(data)) {
                        $('#validation-errors').append(
                            '<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Error: </strong>' +
                            value +
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
                        );
                    }
                    $("html, body").animate({
                        scrollTop: 0
                    }, "slow");
                }

            })
        })
    </script>
@endsection
