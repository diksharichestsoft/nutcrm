
<style>
    .switch{
  opacity: 0;
  position: absolute;
  z-index: 1;
  width: 18px;
  height: 18px;
  cursor: pointer;
}
.switch + .lable{
  position: relative;
  display: inline-block;
  margin: 0;
  line-height: 20px;
  min-height: 18px;
  min-width: 18px;
  font-weight: normal;
  cursor: pointer;
}
.switch + .lable::before{
  cursor: pointer;
  font-weight: normal;
  font-size: 12px;
  color: #32a3ce;
  content: "\a0";
  background-color: #FAFAFA;
  border: 1px solid #c8c8c8;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
  border-radius: 0;
  display: inline-block;
  text-align: center;
  height: 16px;
  line-height: 14px;
  min-width: 16px;
  margin-right: 1px;
  position: relative;
  top: -1px;
}
.switch:checked + .lable::before {
  display: inline-block;
  content: '\f00c';
  background-color: #F5F8FC;
  border-color: #adb8c0;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
}
/* CSS3 on/off switches */
.switch + .lable {
  margin: 0 4px;
  min-height: 24px;
}
.switch + .lable::before {
  font-weight: normal;
  font-size: 11px;
  line-height: 17px;
  height: 20px;
  overflow: hidden;
  border-radius: 12px;
  background-color: #F5F5F5;
  -webkit-box-shadow: inset 0 1px 1px 0 rgba(0, 0, 0, 0.15);
  box-shadow: inset 0 1px 1px 0 rgba(0, 0, 0, 0.15);
  border: 1px solid #CCC;
  text-align: left;
  float: left;
  padding: 0;
  width: 52px;
  text-indent: -21px;
  margin-right: 0;
  -webkit-transition: text-indent .3s ease;
  -o-transition: text-indent .3s ease;
  transition: text-indent .3s ease;
  top: auto;
}
.switch.switch-bootstrap + .lable::before {
  font-family: FontAwesome;
  content: "\f00d";
  box-shadow: none;
  border-width: 0;
  font-size: 16px;
  background-color: #a9a9a9;
  color: #F2F2F2;
  width: 52px;
  height: 22px;
  line-height: 21px;
  text-indent: 32px;
  -webkit-transition: background 0.1s ease;
  -o-transition: background 0.1s ease;
  transition: background 0.1s ease;
}
.switch.switch-bootstrap + .lable::after {
  content: '';
  position: absolute;
  top: 2px;
  left: 3px;
  border-radius: 12px;
  box-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  width: 18px;
  height: 18px;
  text-align: center;
  background-color: #F2F2F2;
  border: 4px solid #F2F2F2;
  -webkit-transition: left 0.2s ease;
  -o-transition: left 0.2s ease;
  transition: left 0.2s ease;
}
.switch.switch-bootstrap:checked + .lable::before {
  content: "\f00c";
  text-indent: 6px;
  color: #FFF;
  border-color: #b7d3e5;

}
.switch-primary >.switch.switch-bootstrap:checked + .lable::before {
    background-color: #337ab7;
}
.switch-success >.switch.switch-bootstrap:checked + .lable::before {
    background-color: #5cb85c;
}
.switch-danger >.switch.switch-bootstrap:checked + .lable::before {
    background-color: #d9534f;
}
.switch-info >.switch.switch-bootstrap:checked + .lable::before {
    background-color: #5bc0de;
}
.switch-warning >.switch.switch-bootstrap:checked + .lable::before {
    background-color: #f0ad4e;
}
.switch.switch-bootstrap:checked + .lable::after {
  left: 32px;
  background-color: #FFF;
  border: 4px solid #FFF;
  text-shadow: 0 -1px 0 rgba(0, 200, 0, 0.25);
}
/* square */
.switch-square{
  opacity: 0;
  position: absolute;
  z-index: 1;
  width: 18px;
  height: 18px;
  cursor: pointer;
}
.switch-square + .lable{
  position: relative;
  display: inline-block;
  margin: 0;
  line-height: 20px;
  min-height: 18px;
  min-width: 18px;
  font-weight: normal;
  cursor: pointer;
}
.switch-square + .lable::before{
  cursor: pointer;
  font-family: fontAwesome;
  font-weight: normal;
  font-size: 12px;
  color: #32a3ce;
  content: "\a0";
  background-color: #FAFAFA;
  border: 1px solid #c8c8c8;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
  border-radius: 0;
  display: inline-block;
  text-align: center;
  height: 16px;
  line-height: 14px;
  min-width: 16px;
  margin-right: 1px;
  position: relative;
  top: -1px;
}
.switch-square:checked + .lable::before {
  display: inline-block;
  content: '\f00c';
  background-color: #F5F8FC;
  border-color: #adb8c0;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
}
/* CSS3 on/off switches */
.switch-square + .lable {
  margin: 0 4px;
  min-height: 24px;
}
.switch-square + .lable::before {
  font-weight: normal;
  font-size: 11px;
  line-height: 17px;
  height: 20px;
  overflow: hidden;
  border-radius: 2px;
  background-color: #F5F5F5;
  -webkit-box-shadow: inset 0 1px 1px 0 rgba(0, 0, 0, 0.15);
  box-shadow: inset 0 1px 1px 0 rgba(0, 0, 0, 0.15);
  border: 1px solid #CCC;
  text-align: left;
  float: left;
  padding: 0;
  width: 52px;
  text-indent: -21px;
  margin-right: 0;
  -webkit-transition: text-indent .3s ease;
  -o-transition: text-indent .3s ease;
  transition: text-indent .3s ease;
  top: auto;
}
.switch-square.switch-bootstrap + .lable::before {
  font-family: FontAwesome;
  content: "\f00d";
  box-shadow: none;
  border-width: 0;
  font-size: 16px;
  background-color: #a9a9a9;
  color: #F2F2F2;
  width: 52px;
  height: 22px;
  line-height: 21px;
  text-indent: 32px;
  -webkit-transition: background 0.1s ease;
  -o-transition: background 0.1s ease;
  transition: background 0.1s ease;
}
.switch-square.switch-bootstrap + .lable::after {
  content: '';
  position: absolute;
  top: 2px;
  left: 3px;
  border-radius: 12px;
  box-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  width: 18px;
  height: 18px;
  text-align: center;
  background-color: #F2F2F2;
  border: 4px solid #F2F2F2;
  -webkit-transition: left 0.2s ease;
  -o-transition: left 0.2s ease;
  transition: left 0.2s ease;
}
.switch-square.switch-bootstrap:checked + .lable::before {
  content: "\f00c";
  text-indent: 6px;
  color: #FFF;
  border-color: #b7d3e5;

}
.switch-primary >.switch-square.switch-bootstrap:checked + .lable::before {
    background-color: #337ab7;
}
.switch-success >.switch-square.switch-bootstrap:checked + .lable::before {
    background-color: #5cb85c;
}
.switch-danger >.switch-square.switch-bootstrap:checked + .lable::before {
    background-color: #d9534f;
}
.switch-info >.switch-square.switch-bootstrap:checked + .lable::before {
    background-color: #5bc0de;
}
.switch-warning >.switch-square.switch-bootstrap:checked + .lable::before {
    background-color: #f0ad4e;
}
.switch-square.switch-bootstrap:checked + .lable::after {
  left: 32px;
  background-color: #FFF;
  border: 4px solid #FFF;
  text-shadow: 0 -1px 0 rgba(0, 200, 0, 0.25);
}
.switch-square.switch-bootstrap + .lable::after {
	border-radius: 2px;
}
</style>

    <!-- Main content -->
    @can('employees_add')
   <div class="row">
     <div class="col-md-10">
    <a href="{{route('employees-create')}}" class="btn btn-outline-success btn-lg">Add Users</a>
     </div>
     <div class="col-md-2">
      <h6 class="count text-right">Total Dsrs : {{$users->total()}}</h6>
     </div>
   </div>


    @endcan
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!--  -->
                <div class="card-body">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width: 10px">#</th>
                          <th>Actions</th>
                          <th>User</th>
                          <th>User Info</th>
                          <th>User Role</th>
                          <th>Roles</th>
                          @can('employees_disable')
                          <th>Login</th>
                          @endcan
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($users as $key=>$user)
                          <tr>
                            <td>{{$user->id}}</td>
                            <td>
                                @can('employees_edit')
                                <a href="{{route('employee-edit',$user->id)}}" class="btn btn-outline-success btn-xs"><i class="fas fa-pencil-alt"></i></a>
                                @endcan
                                @can('employees_delete')
                                <button data-id="{{$user->id}}" onclick="removeCompanyProject(this)" class="btn btn-danger btn-xs  remove"><i class="fas fa-times"></i></button>
                                @endcan
                                <a href="{{route('employee-detail',$user->id)}}"  target="_blank" class="btn btn-outline-success btn-xs">Add More Details</a>
                            </td>
                            <td>
                                <img src="{{asset('/').('images/'.($user->profile_image ?? 'user_default.png'))}}" style="height: 100px; width: 100px"  alt="">
                                {{$user->name}}

                            </td>
                            <td>

                                <p> <b>Email: </b> {{$user->email ?? 'NaN'}} </p>
                                <p> <b>Phone: </b> {{$user->phone ?? 'NaN'}} </p>
                                <p> <b>Experience: </b> {{$user->experience ?? 'NaN'}} </p>
                                <p> <b>Qualification: </b> {{$user->qualification ?? 'NaN'}} </p>
                                <p> <b>Specialization: </b> {{$user->specialization ?? 'NaN'}} </p>
                                <p> <b>Date Of Joining: </b> {{$user->date_of_joining ?? 'NaN'}} </p>
                            </td>
                            <td>
                                <p>
                                    <b>Role: </b>
                                        @foreach($user->roles as $role)
                                        {{$role->name}}
                                        @endforeach
                                </p>
                                <p> <b>Company: </b> {{$user->company ?? 'NaN'}} </p>
                                <p>
                                    <b>Departments: </b>
                                        @foreach($user->departments()->get() as $department)
                                        {{$department->name}},
                                        @endforeach
                                </p>
                                <p>
                                    <b>Team Lead: </b>
                                        @foreach($user->teamLead()->get() as $tl)
                                        {{$tl->name}},
                                        @endforeach
                                </p>
                                <p> <b>Salary: </b> {{$user->salary ?? 'NaN'}} </p>
                                <p> <b>Monthly Target: </b> {{$user->monthly_target ?? 'NaN'}} </p>
                                <p> <b>Slack ID: </b> {{$user->slack_id ?? 'NaN'}} </p>

                            </td>
                            <td>

                            </td>
                            <td>

                                @can('employees_disable')
                                <label class="label-switch switch-success">
                                <input type="checkbox" id="toggle_check_{{$key}}"  class="switch-square switch-bootstrap status toggle_user_login" data-id="{{$user->id}}" name="" id="status"  {{($user->login_toggle == 1) ? 'checked' : ''}}>
                                <span class="lable">Login</span></label>
                                @endcan
                            </td>
                        </tr>
                        @endforeach

                      </tbody>
                    </table>
                </div>

                <!--  -->
            </div>
        </div>
        {{$users->links()}}
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
    <script>

        $(".toggle_user_login").on('click', function (e){
            e.preventDefault();
            var id = $(this).attr('data-id');
            $.ajax({
                type: "post",
                url: "{{route('employee-login-toggle')}}",
                headers:
                {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {id:id},
                dataType: "JSON",
                success: function (response) {

                      document.querySelector(`#${e.target.id}`).checked= response.success;
                }
            });
        })
    </script>

<script>
    $(document).on('click', '#post_pagination .pagination a', function(event){
 event.preventDefault();
 var page = $(this).attr('href').split('page=')[1];
 var filter=document.querySelector("#tabs_filter").value;
 ajax('get',`{{Route("employee-pagination")}}`,data={page:page},'#pagination_employee');
});


function removeCompanyProject(e)
{

  var id=e.getAttribute('data-id');
  swal({
  title: "Oops....",
  text: "Are You Sure You want to delete!",
  icon: "error",
  buttons: [
    'NO',
    'YES'
  ],
}).then(function(isConfirm) {
  if (isConfirm) {
    $.ajax({
      url:"{{route('employee-remove-data')}}",
      type:"post",
      data:{id:id},
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
      success:function(data){
        getEmployeeData({{$users->currentPage()}});
        Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Remove Successfully',
                showConfirmButton: false,
                timer: 1500
                })
        }
    })
  } else {

  }
});
}


</script>
