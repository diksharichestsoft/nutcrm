@extends('admin.layout.template')
@section('contents')

<style>
     .select2-selection{
         background-color:#343a40 !important;
     }
</style>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Add Employee</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <div id="validation-errors"></div>
                    <form method="post" id="add_user_form" action="" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">

                        <div class="form-group">
                            <label for="company">Company Name</label>
                            <select name="company_id" id="company" class="selectpicker" data-live-search="true">
                              <option value="">Select Team Lead</option>
                              @foreach($companies as $company)
                              <option value="{{$company->id}}">{{$company->name}}</option>
                              @endforeach
                            </select>
                            @error('company_id')
                                <div class="error error-msg-red">{{ $message }}</div>
                            @enderror
                        </div>

                        <input type="hidden" name="reqType" value="0">

                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" id="name" class="form-control" name="name" value="{{old('name')}}" required>
                        </div>

                        <div class="form-group">
                            <label for="profile_image">Image</label>
                            <input type="file" id="profile_image" class="form-control" name="profile_image" accept="image/*">
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" value="{{old('email')}}" required>
                        </div>


                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" value="{{old('password')}}" required>
                        </div>
                        <div class="form-group">
                            <label for="slack">slack ID</label>
                            <input type="text" class="form-control" name="slack_id" value="{{old('slack_id')}}" required>
                        </div>
                        <div class="form-group">
                            <label for="salary">Salary</label>
                            <input type="number" class="form-control" name="salary" value="{{old('salary')}}">
                        </div>

                        <div class="form-group">
                            <label for="security_amount">Security Amount</label>
                            <input type="number" class="form-control" name="security_amount" value="{{old('security_amount')}}" required>
                        </div>

                        <div class="form-group">
                            <label for="experience">Experience(In years)</label>
                            <input type="number" class="form-control" name="experience" value="{{old('experience')}}" required>
                        </div>

                        <div class="form-group">
                            <label for="qualification">Qualification</label>
                            <input type="text" class="form-control" name="qualification" value="{{old('qualification')}}" required>
                        </div>

                        <div class="form-group">
                            <label for="specialization">Specialization</label>
                            <input type="text" class="form-control" name="specialization" value="{{old('specialization')}}">
                        </div>

                        <div class="form-group">
                            <label for="date_of_joining">Date Of Joining</label>
                            <input type="date" class="form-control" name="date_of_joining" value="{{old('date_of_joining')}}" required>
                        </div>
                        <div class="form-group">
                            <label for="date_of_birth">Date Of Birth</label>
                            <input type="date" class="form-control" name="date_of_birth" value="{{old('date_of_birth')}}" required>
                        </div>

                        <div class="form-group">
                            <label for="phone">Phone Number</label>
                            <input type="text" class="form-control" name="phone"  value="{{old('phone')}}" required>
                        </div>

                        <div class="form-group">
                            <label for="phone_number_2">Phone Number Alternate</label>
                            <input type="text" class="form-control" name="phone_number_2"  value="{{old('phone_number_2')}}">
                        </div>

                        <div class="form-group">
                            <label for="phone_number_mother">Phone Number Mother</label>
                            <input type="text" class="form-control" name="phone_number_mother" value="{{old('phone_number_mother')}}" required>
                        </div>

                        <div class="form-group">
                            <label for="phone_number_father">Phone Number Father</label>
                            <input type="text" class="form-control" name="phone_number_father" value="{{old('phone_number_father')}}" required>
                        </div>

                        <div class="form-group">
                            <label for="phone_number_sibling">Phone Number Brother/Sister</label>
                            <input type="text" class="form-control" name="phone_number_sibling" value="{{old('phone_number_sibling')}}">
                        </div>

                        <div class="form-group">
                            <label for="monthly_target">Monthly Target</label>
                            <input type="text" class="form-control" name="monthly_target" value="{{old('monthly_target')}}">
                        </div>

                        <div class="form-group">
                            <label for="designation">Designation</label>
                            <input type="text" class="form-control" name="designation" value="{{old('designation')}}">
                        </div>

                        <div class="form-group">
                            <label for="login_toggle">Login</label>
                            <input type="hidden" class="form-control" value="0" name="login_toggle">
                            <input type="checkbox" class="form-control" value="1" style="width: 20px" name="login_toggle">

                        </div>
                        <div class="form-group">
                            <label for="team_lead">Team Lead</label>
                            <select name="team_lead[]" id="team_lead" class="selectpicker" data-live-search="true" multiple>
                              <option value="">Select Team Lead</option>
                              @foreach($users as $user)
                              <option value="{{$user->id}}">{{$user->name}}</option>
                              @endforeach
                            </select>
                            @error('team_lead')
                                <div class="error error-msg-red">{{ $message }}</div>
                            @enderror
                        </div>
{{--
                        <div class="form-group">
                            <label for="team_lead">Department</label>
                            <input type="text" class="form-control" name="department">
                        </div> --}}
                        <div class="form-group">
                            <label for="team_lead">Department</label>
                            <select name="department_id[]" id="department" class="selectpicker" data-live-search="true" multiple required>
                              <option value="">Select Departement</option>
                              @foreach($departments as $department)
                              <option value="{{$department->id}}">{{$department->name}}</option>
                              @endforeach
                            </select>
                            @error('team_lead')
                                <div class="error error-msg-red">{{ $message }}</div>
                            @enderror
                        </div>


                        <div class="form-group">
                            <label for="team_lead">Roles</label>
                            <select name="roles[]" id="permissions"  class="selectpicker" data-live-search="true"  multiple required>
                              <option value="">Select Roles</option>
                              @foreach($roles as $role)
                              <option value="{{$role->id}}">{{$role->name}}</option>
                              @endforeach
                            </select>
                            @error('permissions')
                                <div class="error error-msg-red">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="description">Description</label>
                            <input type="text" class="form-control" name="description" value="{{old('description')}}">
                        </div>

                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" class="form-control" name="address"  value="{{old('address')}}" required>
                        </div>

                        <div class="form-group">
                            <label for="age">Age</label>
                            <input type="number" class="form-control" name="age" value="{{old('age')}}">
                        </div>


                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!--  -->
            </div>
        </div>

      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
    <script>

    function getFormWithImage(id) {
            let form = document.querySelector(`#${id}`);
            let data = new FormData(form);
            return data;
        }
        $("#add_user_form").on('submit', function (e) {
            e.preventDefault();
            Swal.showLoading()
            var data = getFormWithImage("add_user_form");

            $.ajax({
                url:"{{route('employee-store')}}",
                type:"post",
                cache: false,
                contentType: false,
                processData: false,
                dataType: "JSON",
                data:data,
                xhr: function() {
                    myXhr = $.ajaxSettings.xhr();
                    return myXhr;
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                success:function(data){
                    Swal.close()
                    window.location.href = "{{route('employees')}}"
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Added Successfully',
                        showConfirmButton: false,
                        timer: 1500
                        })
                },
                error:function(data){
                    Swal.close()
                    data =data.responseJSON.errors;
                    for (const [key, value] of Object.entries(data)) {
                        $('#validation-errors').append('<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Error: </strong>'+value+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                        }
                        $("html, body").animate({ scrollTop: 0 }, "slow");
                }

                })
         })

    </script>
    <script>
        $(".selectpicker").selectpicker({
                "title": "Select Options"
            }).selectpicker("render");
        var myEditor;

        InlineEditor
        .create( document.querySelector( '#editor' ) )
        .then( editor => {
            console.log( 'Editor was initialized', editor );
            myEditor = editor;
        } )
        .catch( err => {
            console.error( err.stack );
        } );
        $(document).ready(function (){
        $(".ck-file-dialog-button").hide();
        $(".ck-dropdown__button").hide();
        })
    </script>
@endsection

