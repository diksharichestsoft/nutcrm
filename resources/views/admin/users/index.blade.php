@extends('admin.layout.template')
@section('contents')
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!--  -->
                <div class="card-body"> 
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width: 10px">#</th>
                          <th>User Name</th>
                          <th>Email</th>
                          <th>Roles</th>
                          <th>Action</th>
                          <!-- <th style="width: 40px">Label</th> -->
                        </tr>
                      </thead>
                      <tbody>
                        <?php $i =0 ?>
                        @foreach($users as $user)
                          <tr>
                            <td><?php  $i++; echo $i; ?> </td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>
                              @foreach($user->roles as $role)
                              <span class="badge bg-info">{{$role->name}}</span>
                              @endforeach
                            </td>

                            <td>
                            <a href="{{route('manage-users-edit',$user->id)}}" class="btn btn-outline-success btn-sm rounded-pill">Edit</a>
                            </td>
                        </tr>
                        @endforeach
                        
                      </tbody>
                    </table>
                </div>
                <!--  -->
            </div>
        </div>
        
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
@endsection