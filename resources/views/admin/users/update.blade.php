@extends('admin.layout.template')
@section('contents')
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!--  -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Update User Roles</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form method="post" action="{{route('manage-users-update',$user->id)}}" >
                        @csrf
                        <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Select Roles</label>
                            <input type="text" class="form-control" name="user" value="{{$user->name}}" id="user" placeholder="Enter Role Name" readonly >
                            @error('user')
                                <div class="error error-msg-red">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Select Permissions</label>
                            <!-- <input type="text" class="form-control" name="role" id="role" placeholder="Enter Role Name"> -->
                            <select name="roles[]" id="roles" class="form-control select2" multiple>
                              <option value="">Select Permissions</option>
                              @foreach($roles as $role)
                              <option value="{{$role->id}}" {{in_array ( $role->id, $rolesdata)?'selected':''}}>{{$role->name}}</option>
                              @endforeach
                            </select>
                            @error('roles')
                                <div class="error error-msg-red">{{ $message }}</div>
                            @enderror
                        </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!--  -->
            </div>
        </div>
        
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
@endsection
