@extends('admin.layout.template')
@section('contents')
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!--  -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Update Role</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form method="post" action="{{route('roles-update',$role->id)}}" >
                        @csrf
                        <div class="card-body">
                        
                        <div class="form-group">
                            <label for="exampleInputEmail1">Role Name</label>
                            <input type="text" class="form-control" name="role" value="{{$role->name??''}}" id="permission" placeholder="Enter Role Name">
                            @error('role')
                                <div class="error error-msg-red">{{ $message }}</div>
                            @enderror
                        </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!--  -->
            </div>
        </div>
        
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
@endsection