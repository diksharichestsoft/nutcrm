<style>
    .daterangepicker .ranges li {font-size: 12px;padding: 8px 12px;cursor: pointer;background: #000000b0;}
  </style>
  <div class="card" id="data">
                 <div class="card-header p-2" id="card_head">
                  <ul class="nav nav-pills">
                    <li class="nav-item"><a class="nav-link active services" href="javascript:void(0)" data-id="0" data-toggle="tab">Over Due</a></li>&nbsp;
                    <li class="nav-item"><a class="nav-link services" href="javascript:void(0)" data-id="+1 week" data-toggle="tab">One Week</a></li>&nbsp;
                    <li class="nav-item"><a class="nav-link services" href="javascript:void(0)" data-id="+2 week" data-toggle="tab">Two Week</a></li>&nbsp;
                    <li class="nav-item"><a class="nav-link services" href="javascript:void(0)" data-id="+3 week" data-toggle="tab">Three Week</a></li>&nbsp;
                    <li class="nav-item"><a class="nav-link services" href="javascript:void(0)" data-id="+1 month" data-toggle="tab">One Month</a></li>&nbsp;
                    <li class="nav-item"><a class="nav-link services" href="javascript:void(0)" data-id="+2 month" data-toggle="tab">Two Month</a></li>&nbsp;
                    <li class="nav-item"><a class="nav-link services" href="javascript:void(0)" data-id="+3 month" data-toggle="tab">Three Month</a></li>&nbsp;
                    <li class="nav-item"><a class="nav-link services" href="javascript:void(0)" data-id="+4 month" data-toggle="tab">Four Month</a></li>&nbsp;
                    <li class="nav-item"><a class="nav-link services" href="javascript:void(0)" data-id="-1" data-toggle="tab">All Paid</a></li>&nbsp;
                    <li class="nav-item"><a class="nav-link services" href="javascript:void(0)" data-id="1" data-toggle="tab">Cancel</a></li>&nbsp;
                  </ul>
                </div><!-- /.card-header -->
                <li class="nav-item search-right">
                <div class="search_bar" >
                    <div class="input-group" data-widget="sidebar-search" >
                     <input class="form-control form-control-sidebar" id="search" type="search" placeholder="Search" aria-label="Search">
                    </div>
                 </div>
                </li>
                <div class="card-body">
                  <div class="tab-content">
                    <div class="tab-pane" id="profile">
                      <!-- sadasdasdsad -->
                       @include('admin.payments.includes.addform')
                    </div>

                  <div>
                  </div>
                  <div class="active tab-pane" id="view-data">
                      {{-- @include('admin.payments.includes.view') --}}
                  </div>
                    <!-- /.tab-pane -->
                  </div>
                  <!-- /.tab-content -->
                </div><!-- /.card-body -->

        </div>

  <div class="modal fade" id="createDeal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Projects</h5>
          <button type="button" class="close" id='createDealHide' data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body " id="createDealBody">
        </div>
      </div>
    </div>
  </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>

    $(document).ready(()=>{
    fetch_subscription_data()
    })
  $(document).on('click','.services',function(){
    const id = $(this).data('id')
    console.log(id)
    fetch_subscription_data(id)

  });
  const fetch_subscription_data = (id)=>{
    $("#page-loader").show();
    // var id = $(this).data('id');
    var search=document.querySelector("#search").value;
    var id = id
    $.ajax({
            type:'get',
            url:"{{route('subscription-filter')}}",
            // dataType: "JSON",
        data:{id:id},
            success:function(data){

                $('#view-data').html(data);
                $("#page-loader").hide();
              //  getSubscriptions();
            },
            error:function(data){

                $("#page-loader").hide();
            }
        });
  }

  document.querySelector("#search").addEventListener("keyup",(e)=>{
      getSubscriptions(1);
  });

  $(document).on('click', '.pagination a', function(event){
  event.preventDefault();
  var page = $(this).attr('href').split('page=')[1];
  var sendurl = document.getElementsByClassName('services active')[0].getAttribute('data-id');
//  var sendurl=$(this).attr('href');
  getSubscriptions(page, sendurl, '');
});

function getSubscriptions(page, id)
{
  $('#page-loader').show();
//   page = (page='')?'1':page;
// console.log(document.getElementsByClassName('services active')[0].getAttribute('data-id')       )
var id = document.getElementsByClassName('services active')[0].getAttribute('data-id');
// if (active == '') {
//         }
    var search=document.querySelector("#search").value;
    var data={search:search,id: id};
    console.log(data);
  var make_url= "{{url('/')}}/admin/subscriptions/filter?page="+page;
  $.ajax({
    url:make_url,
    data:data,
    success:function(data)
    {
    $('#view-data').empty().html(data);
    $('#page-loader').hide();
    },
    error:function(error){
      $('#page-loader').hide();
    }
  });
  }
</script>
