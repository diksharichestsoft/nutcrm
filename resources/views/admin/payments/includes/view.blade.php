@php
    $total = 0;
    $pending = 0;
@endphp
@foreach($allPayments as $eachPayment)
    @php
        $total +=(int)($eachPayment->paid);
        $pending +=(int)($eachPayment->pending);
    @endphp

@endforeach
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Payments</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        Recevied Payment:{{$total}}&nbsp;
        Pending Payment:{{$pending}}
        <table class="table table-bordered">
            <thead>
                <tr>
                    <!-- <th>ID</th> -->
                    <th>Project ID</th>
                    <th>Project Title</th>
                    <th>Email</th>
                    <th>Platform</th>
                    <th>Client</th>
                    <th>Total</th>
                    @if ($active == "-1")
                    <th>Payment Date</th>
                    @endif
                    @if ($active == "1")
                    <th>Comment</th>
                    @endif
                    <!-- <th>Pending</th> -->
                    <!-- <th>Parent Id</th> -->
                    <th>Due Date</th>
                    <!-- <th>Time</th> -->
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($allPayments as $payment)
                <tr>
                    <!-- <td>{{$payment->id}}</td> -->
                    <td>{{$payment->project_id}}</td>
                    <td><a href="{{route('projects-view',$payment->project_id)}}" target="_blank">{{$payment->project->title}}</a></td>
                    <td>{{$payment->project->client_email}}</td>
                    <td>{{$payment->project_platform}}</td>
                    <td>{{$payment->project->client_name}}</td>
                    <td>{{floatval($payment->total)}}</td>
                    @if ($active == "-1")
                    <td>{{$payment->updated_at}}</td>
                    @endif
                    @if ($active == "1")
                    <td>{{$payment->cancel_reason}}</td>
                    @endif
                    <!-- <td>{{floatval($payment->pending)}}</td> -->
                    <!-- <td>{{$payment->parent_id}}</td> -->
                    <td>{{date('d-m-Y', strtotime($payment->due_date)) ??''}}</td>
                    <!-- <td>{{$payment->created_at->format('h:i:s')}}</td> -->
                    <td>
                        @if ($payment->status == 0 && $payment->pending != 0)
                        @can('subscriptions_payment_accept')
                        <?php
                            if($payment->project->deal_status == 2){
                        ?>
                        <button type="button" data-id ="{{$payment->id}}" class="btn btn-primary payment-modal-toggle" data-toggle="modal" data-target="#addPaymentModal" style="margin-bottom: 11px;">
                        Add Payment</button>
                        <button type="button" data-id ="{{$payment->id}}" class="btn btn-primary cancel-modal-toggle" data-toggle="modal" data-target="#exampleModal">
                           Cancel
                          </button>


                        <?php
                        }else{
                            ?>
                        <button type="button" data-id ="{{$payment->id}}" class="btn btn-primary payment-modal" data-toggle="modal" onclick="Swal.fire('You cant add payment of project which is not in progress','','info')" style="margin-bottom: 11px;">
                        Add Payment</button>
                        <button type="button" data-id ="{{$payment->id}}" class="btn btn-primary cancel-modal-toggle" data-toggle="modal" data-target="#exampleModal" >
                            Cancel
                           </button>
                        <?php
                        }
                        ?>
                        @endcan
                        @endif
                        @if ($payment->status == 1 || $payment->pending == 0 )
                        <a href="{{route('subscriptions-detail',$payment->project_id)}}" target="_blank" class="btn btn-outline-success btn-xs  view"><i class="fas fa-eye"></i></a>

                        <i class="fa fa-check-circle" style="font-size:40px;color:rgb(6, 224, 6)"></i>

                        @endif
                        @if ($active == "1")
                        <i class="fa fa-times-circle" style="font-size:40px;color:rgb(206, 5, 5)"></i>
                        @endif
                    </td>
                </tr>
                @endforeach
                @if(count($allPayments)<'1')
                <tr>
                    <td colspan="9">
                        <h4  align="center">No Data Found</h4>
                    </td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
</div>
<!-- Modal -->

<div class="modal fade" id="addPaymentModal" tabindex="-1" role="dialog" aria-labelledby="addPaymentModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Subscription Payment</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

            <form id="addSubscription"  method="POST" >
                @csrf
                <div class="form-group">
                     <input type="hidden" class="form-control" name="parent_id" id="parent_id">
                <div class="form-group">
                    <label for="exampleInputEmail1">Image</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" name="image"  id="exampleInputFile">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Payment Method</label>
                    <select class="w-100 form-select form-control" name="payment_method" id="subscriptionMethod" aria-label="Default method" required="">
                        <option selected="" value="">Select method</option>
                        <option value="Paypal">Paypal</option>
                        <option value="Payoneer">Payoneer</option>
                        <option value="Bank">Bank</option>
                        <option value="Others">Others</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Comment</label>
                    <textarea type="number" name="comment" class="form-control" id="comment" ></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>

        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div> -->
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Payment Cancel</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="cancelSubscription"  method="POST" >
                @csrf
                <div class="form-group">
                     <input type="hidden" class="form-control" name="project_id" id="project_id">
                <div class="form-group">
                    <label for="exampleInputEmail1">Comment</label>
                    <textarea type="text" name="cancel_reason" class="form-control" id="cancel_reason" ></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
        </div>

      </div>
    </div>
  </div>




<script>
    $(document).on('click','.payment-modal-toggle',function(e){
        e.preventDefault();
        $("#parent_id").val($(this).data('id'));
    });
$(document).on('submit','#addSubscription',function(e){
    e.preventDefault();
Swal.showLoading()
    var data = new FormData(this);
        $.ajax({
            type:'post',
            url:"{{route('add-subscription-payment')}}",
            dataType: "JSON",
        xhr: function() {
                myXhr = $.ajaxSettings.xhr();
                return myXhr;
        },
        cache: false,
        contentType: false,
        processData: false,
        data:data,
            success:function(data){
                Swal.close()
                $('.active.services').trigger('click');
                $("#addPaymentModal").hide();

                $(".modal-backdrop").remove();
                $("body").removeClass("modal-open");
                // window.location.reload();


            },
            error:function(data){
                Swal.close()
            }
        });
    });

    $(document).on('click','.cancel-modal-toggle',function(e){
        $("#project_id").val($(this).data('id'));
    });
    $(document).on('submit','#cancelSubscription',function(e){
    e.preventDefault();
    Swal.showLoading()
    var data = new FormData(this);
        $.ajax({
            type:'post',
            url:"{{route('cancel-subscriptions')}}",
            dataType: "JSON",
        xhr: function() {
                myXhr = $.ajaxSettings.xhr();
                return myXhr;
        },
        cache: false,
        contentType: false,
        processData: false,
        data:data,
            success:function(data){
                Swal.close()
                $('.active.services').trigger('click');
                $("#exampleModal").hide();

                $(".modal-backdrop").remove();
                $("body").removeClass("modal-open");
                // window.location.reload();


            },
            error:function(data){
                Swal.close()
            }
        });
    });
</script>

