<!-- Button trigger modal -->
  <!-- Modal -->
  <div class="modal fade" id="addReportModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Report</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="add_report_form" action="" method="POST">
                @csrf
                {{-- /// reqType 0 = Create, 1 = update --}}
              <input type="hidden" name="reqType" value="0">

                    <div class="form-group">
                      <label for="exampleInputEmail1">Title</label>
                      <div class="error" id="error_title"></div>
                      <input type="text" class="form-control" name="title" placeholder="Enter Title">

                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Description</label>
                      <div class="error" id="error_description"></div>
                      <textarea name="description" cols="30" rows="3" placeholder="Please Describe the issue/bug in detail" class="form-control"></textarea>

                    </div>
            <br>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="$('#add_report_form').submit()"> Save</button>
        </div>
      </div>
    </div>
  </div>



<script>
</script>
