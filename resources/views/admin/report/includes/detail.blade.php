@extends('admin.layout.template')

@section('contents')
<table class="table">
    <tbody>
      <tr>
        <th scope="row">Title</th>
        <td>{{$thisReport->title}}</td>
      </tr>
      <tr>
        <th scope="row">Description</th>
        <td>{!!$thisReport->description!!}</td>
      </tr>
      <tr>
        <th scope="row">Created By</th>
        <td>
          {{$thisReport->users()->first('name')->name}}
        </td>
      </tr>
      <tr>
        <th scope="row">Created At</th>
        <td>{{timestampToDateAndTimeObj($thisReport->created_at)->time}}</td>
      </tr>
      <tr>
        <th scope="row">Created On</th>
        <td>{{timestampToDateAndTimeObj($thisReport->created_at)->date}}</td>
      </tr>
    </tbody>
  </table>
  @endsection

