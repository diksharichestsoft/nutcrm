<style>
  select#selectsatus {
      width: 100%;
  }
  </style>
<!-- Main content -->
<a href="#" id="addDsrButton" class="btn btn-outline-success btn-lg" data-toggle="modal" data-target="#addReportModal">Add Report</a>

<section class="content">
  <div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <!--  -->
            <div class="card-body">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Actions</th>
                      <th>Title</th>
                      <th>Description</th>
                      <th>Created By</th>
                      <th>Created At</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                      @forelse( $reports as $key=>$report )
                      <tr>
                        <td>{{$report->id}}</td>
                        <td>
                            <div class="btn-group">
                                <button data-id="{{$report->id}}" onclick="editReport(this)" class="btn btn-outline-success btn-xs  update" data-toggle="modal" data-target="#editReportModal" ><i class="fas fa-pencil-alt"></i></button>
                                <a href=" {{route('report-detail',$report->id)}}" target="_blank" class="btn btn-outline-success btn-xs  view"><i class="fas fa-eye"></i></a>
                                <button data-id="{{$report->id}}" onclick="removeCompanyProject(this)" class="btn btn-danger btn-xs  remove"><i class="fas fa-times"></i></button>
                              </div>

                        </td>
                        <td>
                            {{$report->title}}
                        </td>
                        <td>
                            {{$report->description}}
                        </td>
                        <td>
                            {{$report->users()->first('name')->name}}
                        </td>
                        <td>
                            {{$report->created_at}}
                        </td>
                        <td class="d-flex">
                                    <select name="sel" id="selectstatus" onclick=showmodel() class="deals_status" data-status="{{$report->status}}" data-id="{{ $report->id }}" >
                                    <option value="0">Select Status</option>
                                    @foreach($status as $key => $eachstatus)
                                    <option value="{{$key}}"  {{($report->status==$key)?"selected":""}}>{{$eachstatus}}</option>
                                    @endforeach
                                </select>
                              </td>
                    </tr>

                    @empty
                    <tr>
                        <td colspan="9">
                            <h4  align="center">No Data Found</h4>
                        </td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
            </div>
            <!--  -->
        </div>
    </div>
    {{$reports->links()}}
  </div><!--/. container-fluid -->
</section>
<!-- /.content -->


<script>
$(document).on('click', '#post_pagination .pagination a', function(event){
event.preventDefault();
var page = $(this).attr('href').split('page=')[1];
var filter=document.querySelector("#tabs_filter").value;
ajax('get',`{{Route("employee-pagination")}}`,data={page:page},'#pagination_employee');
});


function editReport(e)
{
  $('#page-loader').show();

  var id=e.getAttribute('data-id');
  $.ajax({
    url:"{{route('report-edit')}}",
    type:"get",
    data:{id:id},
    success:function(data)
    {

      $('#page-loader').hide();
      $("#editReportModal").modal('show');
      $("#title").val(data.data.title);
      $("#description").val(data.data.description);
      $("#report_id").val(data.data.id);
    },
    error:function(){
      $('#page-loader').hide();
    }
  })
}




function removeCompanyProject(e)
{

var id=e.getAttribute('data-id');
swal({
title: "Oops....",
text: "Are You Sure You want to delete!",
icon: "error",
buttons: [
'NO',
'YES'
],
}).then(function(isConfirm) {
if (isConfirm) {
$.ajax({
  url:"{{route('report-remove-data')}}",
  type:"post",
  data:{id:id},
  headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
  success:function(data){
    refreshReportData("pagination_employee");
    }
})
} else {

}
});
}

$('body').on('click', '.editPlanSchedule', function() {
var Item_id = $(this).data('id');
$.get("/quotation/getEditPlanSchedule" + '/' + Item_id, function(data) {
    let options = "";
    data.product_plan.forEach(p => {
        options += `<option value="${p.productplanID}">${p.productplanID}</option>`;
    });
    $('#product_plan').val(options);
});
});


</script>

