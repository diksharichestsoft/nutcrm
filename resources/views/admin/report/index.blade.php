@section('contents')
@include('admin.report.includes.create')
@include('admin.report.includes.update')
<style>
    .daterangepicker .ranges li {font-size: 12px;padding: 8px 12px;cursor: pointer;background: #000000b0;}
  </style>
<div class="card" id="data">
    <div class="card-header p-2" id="card_head">
      <ul class="nav nav-pills">
        <div id="reportrange" style="background: transparent; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
            <i class="fa fa-calendar"></i>&nbsp;
            <span class="ab"></span> <i class="fa fa-caret-down"></i>
           </div>
           <input type="hidden" id="start_date_range" >
           <input type="hidden" id="end_date_range">
            <li class="nav-item search-right">
             <div class="search_bar">
                <div class="input-group" data-widget="sidebar-search">
                 <input class="form-control form-control-sidebar" id="search" type="search" placeholder="Search" aria-label="Search">
                </div>
             </div>
            </li>
      </ul>
  </div><!-- /.card-header -->
    
    <div class="card-body">

        <div id="pagination_employee">
            @include('admin.report.includes.view')
        </div>
      <!-- /.tab-content -->
    </div><!-- /.card-body -->
</div>



<div class="modal fade" id="addComment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Comment</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body " id="addCommentBody">
        <form action="" name="addCommentForm" id="addCommentForm">
            @csrf
            <input type="hidden" name='status' id="changeStatus-status" value="">
            <input type="hidden" name='id' id="id">
            
          <div class="value">
            <div class="form-group row">
              <label for="comment" class="col-sm-2 col-form-label">Comment</label>
              <div class="col-sm-10">
              <textarea name="comment" id="comment" cols="30" rows="10" class="form-control">
              
                </textarea>
                <div class="error" id="error_comment"></div>
              </div>
            </div>
            <div class="form-group row">
              <div class="offset-sm-2 col-sm-10">
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </div>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>



<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


<script>
$("#add_report_form").on('submit', function (e){
    e.preventDefault();
    reportStore("add_report_form", "addReportModal")
    })
$("#update_report_form").on('submit', function (e){
    e.preventDefault();
    reportStore("update_report_form", "editReportModal")
    })

    function reportStore(form_id, modal_id){
    const data=getformdata(form_id);
    $.ajax({
        type:'post',
        url:"{{route('report-store')}}",
        dataType: "JSON",
       xhr: function() {
             myXhr = $.ajaxSettings.xhr();
             return myXhr;
       },
       headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        data:data,
        success:function(data){
            $(`#${modal_id}`).modal('hide');
            refreshReportData("pagination_employee");
        },
        error:function(data){
          $.each(data.responseJSON.errors, function(id,msg){
            $('#error_'+id).html(msg);

          })
        }
      });
    }



function refreshReportData (change_html_id){
    $('#page-loader').show();
    $.ajax({
    url:"{{route('report-refresh')}}",
    success:function(data)
    {
        $(`#${change_html_id}`).empty().html(data);
      $('#page-loader').hide();

    },
    error:function(error){
      $('#page-loader').hide();

    }
  });
}
$(document).on('click', '.pagination a', function(event){
    event.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    fetch_data__data(page,'','');

  });

document.querySelector("#search").addEventListener("keyup",(e)=>{
        var search = $("#search").val();
      filterEmpData(search);
  });

  function filterEmpData(search){
    $('#page-loader').show();
    var data={};
  var make_url= "{{url('/')}}/admin/report/search?search="+search;
  $.ajax({
    url:make_url,
    data:data,
    success:function(data)
    {
        $('#pagination_employee').empty().html(data);
      $('#page-loader').hide();

    },
    error:function(error){
      $('#page-loader').hide();

    }
  });
  }

  $(function() {
  var start = moment().subtract(29, 'days');
  var end = moment();
  function cb(start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      $('#start_date_range').val(start.format('YYYY-MM-DD'));
      $('#end_date_range').val(end.format('YYYY-MM-DD'));
      fetch_data__data(1,'');
  }

  $('#reportrange').daterangepicker({
      startDate: start,
      endDate: end,
      ranges: {
         'Today': [moment(), moment()],
         'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
         'Last 7 Days': [moment().subtract(6, 'days'), moment()],
         'Last 30 Days': [moment().subtract(29, 'days'), moment()],
         'This Month': [moment().startOf('month'), moment().endOf('month')],
         'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
  }, cb);
  cb(start, end);

  });



  function fetch_data__data(page)
  {
    $('#page-loader').show();

    var start_date=document.querySelector('#start_date_range').value;
    var end_date=document.querySelector('#end_date_range').value;

    var search=document.querySelector("#search").value;
    var data={search:search,start_date:start_date,end_date:end_date};
    var make_url= "{{url('/')}}/admin/report/search?page="+page;
    $.ajax({
      url:make_url,
      data:data,
      success:function(data)
      {
          $('#pagination_employee').empty().html(data);
        $('#page-loader').hide();

      },
      error:function(error){
        $('#page-loader').hide();
      }
    });
    }



    // $(document).ready(function () {
      $('#selectstatus').on('change', function(event) {
                //event.preventDefault();
                //$('#addComment').modal('show');
                  
            });



            function showmodal(){
              $('#addComment').modal('show');

              
             
            }
        // });
      
     

    $(document).on('change','#selectstatus',function(){
      oldstatus = $(this).data('status')
      var status =$(this).val();
        $(this).val(oldstatus);
      var report_id = $(this).attr('data-id');
      $('#changeStatus-status').val(status);
      $('#id').val(report_id);
      $('#addCommentForm textarea').val('');
      $('#addComment').modal('show');
    });

    

    $(document).on('submit','#addCommentForm',function(e){
        e.preventDefault();
          var data = new FormData(this);
          
      $.ajax({
          type:'post',
          url:"{{route('report-change-status')}}",
          dataType: "JSON",
         xhr: function() {
               myXhr = $.ajaxSettings.xhr();
               return myXhr;
         },
         cache: false,
         contentType: false,
         processData: false,
          data:data,
          success:function(data){
              if(data.status == 0){
                  $('#addComment').modal('hide');
                  Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: data.message,
                        showConfirmButton: false,
                        timer: 7000
                    })
                    return
              }
              $('#addComment').modal('hide');
              fetch_data__data(1,'');
          
          }
        });
  });

     
  
               
</script>
