@extends('admin.layout.template')
@section('contents')
<div id="companies_data">

    <h5>Details</h5>
    <div class="accordion" id="accordionExample">
        <div class="card">
          <div class="card-header" id="headingOne">
            <h2 class="mb-0">
	      <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
		Details
              </button>
            </h2>
          </div>
          <div id="collapse1" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body">
                <table class="table">
                    <tbody>
                        <tr>
                            <th scope="row">Logo</th>
			    <td>
				@if ( $company->logo != null )
					<img src="{{ $company->logo }}" class="img-fluid thumbnail" style="max-height: 200px">
				@else
					Not Added
				@endif
			   </td>
                        </tr>
                        <tr>
                            <th scope="row">Name</th>
                            <td> {{ $company->name }} </td>
                        </tr>
                        <tr>
                            <th scope="row">Phone</th>
                            <td> {{ $company->phone ?? 'Not Added' }} </td>
                        </tr>
                        <tr>
                            <th scope="row">Company Address</th>
                            <td> {!! $company->company_address ?? 'Not Added' !!} </td>
                        </tr>
                        <tr>
                            <th scope="row">Bank Details</th>
                            <td> {{ $company->bank_details ?? 'Not Added' }} </td>
                        </tr>
                        <tr>
                            <th scope="row">Gst Number</th>
                            <td> {{ $company->gst_number ?? 'Not Added'}} </td>
                        </tr>
                        <tr>
                            <th scope="row">Account Name</th>
                            <td> {{ $company->account_name ?? 'Not Added' }} </td>
                        </tr>
                        <tr>
                            <th scope="row">Account Number</th>
                            <td> {{ $company->account_number ?? 'Not Added' }} </td>
                        </tr>
                        <tr>
                            <th scope="row">Swift</th>
                            <td> {{ $company->swift ?? 'Not Added' }} </td>
                        </tr>
                        <tr>
                            <th scope="row">Ifsc Code</th>
                            <td> {{ $company->ifsc_code ?? 'Not Added' }} </td>
                        </tr>
                        <tr>
                            <th scope="row">Branch</th>
                            <td> {{ $company->bank_name ?? 'Not Added' }} </td>
                        </tr>
                        <tr>
                            <th scope="row">Branch</th>
                            <td> {{ $company->bank_branch ?? 'Not Added' }} </td>
                        </tr>
                        <tr>
                            <th scope="row">Tax Type</th>
                            <td> {{ $company->tax_type_name ?? 'Not Added' }} </td>
                        </tr>
                    </tbody>
                </table>
                        </div>
          </div>
        </div>
      </div>

<!-- Content Wrapper. Contains page content -->

</div>
@endsection
