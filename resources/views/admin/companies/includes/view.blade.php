
<!-- Main content -->
@can ('companies_create')
	    <a href="#" id="addCompanyButton" class="btn btn-success ml-4 " data-toggle="modal" data-target="#addCompanyModal">Add New Company</a>
@endcan

    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!--  -->
                <div class="card-body">
		    <table class="table table-bordered">
			@if ( count( $companies ) > 0 )
                <thead>
                    <tr>
                        <th style="width: 170px">Actions</th>
                        <th style="width: 10px">ID</th>
                        <th>Name</th>
                        <th>Phone No.</th>
                        <th>GST No.</th>
                        <th>Tax Type</th>
                    </tr>
		      </thead>
			@endif
		      <tbody>
				@forelse ( $companies as $company )
				<tr>
					<td>
						<div class="btn-group text-center">
							@can ('companies_view')
								<a target="_blank" href="{{ route('company-detail', [ $company->id ] ) }}" id="viewCompanyDetailsBtn" class="btn btn-outline-success btn-sm" data-id="{{ $company->id }}" > <i class="fas fa-eye"></i> </a>
							@endcan

							@can ('companies_edit')
							<button type="button" id="updateCompanyBtn" class="btn btn-outline-success btn-sm" data-id="{{ $company->id }}" onclick=" renderUpdateCompanyModal( this )"> <i class="fas fa-pencil-alt"></i> </button>
							@endcan

							@can ('companies_delete')

								<button type="button" id="deleteCompany" data-id="{{ $company->id }}" class="btn btn-danger btn-sm" onclick=" deleteCompany( this )"> <i class="fas fa-times"></i> </button>
							@endcan
						</div>
					</td>
					<td> {{ $company->id }}</td>
					<td> {{ $company->name }}</td>
					<td> {{ $company->phone  ?? 'Not Added' }}</td>
					<td> {{ $company->gst_number ?? 'Not Added'}}</td>
					<td class="{{ $company->tax_type_name ? 'text-uppercase' : '' }}"> {{ $company->tax_type_name ?? 'Not Added' }}</td>
				</tr>
				@empty
					<center> <h3> No Data Available! </h3></center>
				@endforelse
                      </tbody>
                    </table>
                </div>
                <!--  -->
            </div>
        </div>
     	<div class="ml-4" id="companies_pagination" data-page="{{ $companies->currentPage() }}"> {{  $companies->links() }} </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
