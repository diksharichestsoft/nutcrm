<!-- Button trigger modal -->
  <!-- Modal -->
  <div class="modal fade" id="addCompanyModal" tabindex="-1" role="dialog"  aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Company</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeCompanyModalBtn">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="add_company_modal_body" class="modal-body">
	    <form id="add_company_form" action="#" method="POST" enctype="multipart/form-data">
		@csrf
		<div class="form-group">
            <label>Company Name:</label>


			<input type="text" class="form-control" id="companyName" placeholder="Name" maxlength="255" required name="name">
			<small id="errors_name" class="form-text  font-weight-bold text-danger"></small>
		</div>

		<div class="form-group">
            <label>Phone Number:</label>

			<input type="number" class="form-control" id="companyPhone" placeholder="Phone Number" maxlength="14"  name="phone">
			<small id="errors_phone" class="form-text  font-weight-bold text-danger"></small>
		</div>
		<div class="form-group">
            <label>GST Number:</label>

			<input type="text" class="form-control" id="companyGstNumber" placeholder="GST Number"  name="gst_number">
			<small id="errors_gst_number" class="form-text  font-weight-bold text-danger"></small>
		</div>

        <div class="form-group">
            <label>Bank Details:</label>
            <textarea class="form-control" id="companyBankDetails" placeholder="Bank Details" rows="3"
            name="bank_details"></textarea>
            <small id="errors_bank_details" class="form-text  font-weight-bold text-danger"></small>
        </div>

		<div class="form-group">
            <label>Company Address:</label>
			<input type="hidden" class="form-control" id="companyAddress" placeholder="Company Address"  name="company_address">
            <div id="companyAddressCreate" style="border: 1px solid #ffffff42;"></div>
			<small id="errors_company_address" class="form-text  font-weight-bold text-danger"></small>
		</div>
		<div class="form-group">
            <label>Account Name:</label>
			<input type="text" class="form-control" id="accountName" placeholder="Account Name"  name="account_name">
			<small id="errors_account_name" class="form-text  font-weight-bold text-danger"></small>
		</div>
		<div class="form-group">
            <label>Account Number:</label>
            <input type="text" class="form-control" id="account_number" placeholder="account_number"  name="account_number">
            <small id="errors_account_number" class="form-text  font-weight-bold text-danger"></small>
        </div>
        <div class="form-group">
            <label>Swift</label>

            <input type="text" class="form-control" id="swift" placeholder="swift"  name="swift">
            <small id="errors_swift" class="form-text  font-weight-bold text-danger"></small>
        </div>
        <div class="form-group">
            <label>IFSC Code:</label>
            <input type="text" class="form-control" id="ifsc_code" placeholder="ifsc_code"  name="ifsc_code">
            <small id="errors_ifsc_code" class="form-text  font-weight-bold text-danger"></small>
        </div>
        <div class="form-group">
            <label>Bank Name:</label>
            <input type="text" class="form-control" id="bank_name" placeholder="bank_name"  name="bank_name">
            <small id="errors_bank_name" class="form-text  font-weight-bold text-danger"></small>
        </div>
        <div class="form-group">
            <label>Bank Branch:</label>
            <input type="text" class="form-control" id="bank_branch" placeholder="bank_branch"  name="bank_branch">
            <small id="errors_bank_branch" class="form-text  font-weight-bold text-danger"></small>
        </div>
        <div class="form-group">
            <label>Tax Type:</label>
			<select class="form-control" id="companyTaxType" name="tax_type">
			      <option disabled selected> Select Tax Type</option>
				@foreach ( $tax_types as $tax_type )
				      <option class="text-uppercase" value="{{ $tax_type->id }}"> {{ $tax_type->name }}</option>
				@endforeach
		    </select>
			<small id="errors_tax_type" class="form-text  font-weight-bold text-danger"></small>
		</div>
		<div class="form-group">
			<input type="file" class="" id="companyLogo"   name="logo" accept="image/*">
			<small id="errors_logo" class="form-text  font-weight-bold text-danger"></small>
		</div>
                <button type="submit" class="btn btn-success"> Save</button>
            </form>
	</div>
      </div>
    </div>
  </div>
<script>
          var createCompanyAddressEditor;
      InlineEditor
      .create( document.querySelector( `#companyAddressCreate`) )
      .then( editor => {
          console.log( 'Editor was initialized', editor );
          createCompanyAddressEditor = editor;
      } )
      .catch( err => {
          console.error( err.stack );
	  } );

</script>
