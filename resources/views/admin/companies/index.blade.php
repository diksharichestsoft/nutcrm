@section('contents')
<script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/inline/ckeditor.js"></script>
<div class="card" id="data">
    <div class="card-header p-2" id="card_head">
      <ul class="nav nav-pills">
            <li class="nav-item search-right">
             <div class="search_bar">
                <div class="input-group" data-widget="sidebar-search">
                 <input class="form-control form-control-sidebar" id="search" type="search" placeholder="Search" aria-label="Search">
                </div>
             </div>
            </li>
      </ul>
    </div><!-- /.card-header -->

    <div class="card-body">

	<div id="createCompanyModalBlock">
		@include('admin.companies.includes.create', compact( 'tax_types' ) )
	</div>

	<div id="listCompaniesBlock">
        </div>

	<div id="updateCompanyModalBlock">

	</div>
      <!-- /.tab-content -->
    </div><!-- /.card-body -->
</div>


<script>


function refreshCompaniesView( userRequestedPage = null ){

	var searched_keyword = $("#search").val();
	console.log( `user requested page: ${userRequestedPage}` );
	var page = userRequestedPage ??  $('#companies_pagination').data('page');

	$.ajax({

		url: '{{ route("search-companies") }}' +`?page=${page}`,
		type:'get',
		data: { searched_keyword },
		xhr: function() {
			myXhr = $.ajaxSettings.xhr();
			return myXhr;
		},

		beforeSend:  function(){
			$("#page-loader").show();
		},

		success: function( companiesHtml ){
			console.log( "page refreshed ");
			window.jsnn = companiesHtml;
			$("#listCompaniesBlock").empty().html( companiesHtml );
		},

		error: function( errorResponse ){
			console.log("page not refreshed");
		},

		complete: function(){
			$("#page-loader").hide();
		},
});

}
refreshCompaniesView();

function saveNewCompany( companyData ){

	console.log("save new company");
	$('#page-loader').show();

	$.ajax({

		url:'{{ route("store-company") }}',
		type:'post',
		dataType: "JSON",
		xhr: function() {
			myXhr = $.ajaxSettings.xhr();
			return myXhr;
		},

		cache: false,
		contentType: false,
		processData: false,
		data: companyData,
		success:  function( successResponse ){
			Swal.close()
			$("#addCompanyModal").modal('hide');;
			$('#page-loader').hide();
			refreshCompaniesView();
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Added Successfully',
                showConfirmButton: false,
                timer: 1500
                })
		},

		error: function( errorResponse ){
			Swal.close()
			console.log("next is errors");
			console.log( errorResponse );
			for (const [key, value] of Object.entries( errorResponse.responseJSON.errors)) {
				console.log( `key: ${key}, value: ${value}` );
				$(`#errors_${key}`).html( value );			}
	  		$('#page-loader').hide();

		}
		});
  }

/* Save new company's helper */
  $(document).on('submit', '#add_company_form', function(e) {
	  e.preventDefault();
      $("#companyAddress").val($("#companyAddressCreate").html())
	  Swal.showLoading()
	  var companyData = new FormData(this);
	  saveNewCompany( companyData );
	   $(this)[0].reset();
  });


  function deleteCompany( deleteCompanyBtn )
  {
	  var companyId= deleteCompanyBtn.getAttribute('data-id');

	  swal({
	  	title: "Oops....",
		text: "Are You Sure You want to delete!",
		icon: "error",
		buttons: [ 'NO', 'YES' ],
	}).then( function(isConfirm) {
		if (isConfirm) {
			console.log("is confirmed");
			 $.ajax({
					url:"{{ route('delete-company') }}",
					type:"post",
					data:{ companyId },
					headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
					success: function(data){
						refreshCompaniesView();
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Remove Successfully',
                            showConfirmButton: false,
                            timer: 1500
                        })
					}
			});
		} else {
			console.log("not confirmed");
		}
	});

  }



  function renderUpdateCompanyModal( updateCompanyBtn )
  {
	$('#page-loader').show();

	var companyId = updateCompanyBtn.getAttribute('data-id');
	console.log("this is company id: "+ companyId );
	$.ajax({
		url: "{{ route('edit-company') }}",
		type: "get",
		data: { companyId },
		success: function( successResponse ){
			$('#page-loader').hide();
			$('#updateCompanyModalBlock').empty().html( successResponse );
			$("#updateCompanyModal").modal('show');

		},
		error: function(){
			$('#page-loader').hide();
		}
	})
  }

function updateCompany( companyData ){

	console.log("update new company");
	$('#page-loader').show();

	$.ajax({

		url:'{{ route("update-company") }}',
		type:'post',
		dataType: "JSON",
		xhr: function() {
			myXhr = $.ajaxSettings.xhr();
			return myXhr;
		},

		cache: false,
		contentType: false,
		processData: false,
		data: companyData,
		success:  function( successResponse ){
			Swal.close()
			$("#updateCompanyModal").modal('hide');
		//	$('.modal-backdrop').hide();
			$('#page-loader').hide();
			refreshCompaniesView();
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Updated Successfully',
                showConfirmButton: false,
                timer: 1500
                })
		},

		error: function( errorResponse ){
			Swal.close()
			console.log("next is errors");
			console.log( errorResponse );
			for (const [key, value] of Object.entries( errorResponse.responseJSON.errors)) {
				console.log( `key: ${key}, value: ${value}` );
				$(`#errors_update_${key}`).html( value );
				$('#page-loader').hide();
			}
		}
	});
  }

  $(document).on('submit', '#update_company_form', function(e) {
	  e.preventDefault();
        $("#updateCompanyAddress").val($("#companyAddressUpdate").html())
      Swal.showLoading()
	  var companyData = new FormData(this);
	  updateCompany( companyData );
  });

document.querySelector("#search").addEventListener("keyup",(e)=>{
	refreshCompaniesView( 1);
  });

  /* Pagination */

  $(document).on('click', '.pagination a', function(event){

	          event.preventDefault();
		  console.log('ok');
		  var page = $(this).attr('href').split('page=')[1];
		  var sendurl=$(this).attr('href');

		  console.log( `page number: ${page}` );
		  refreshCompaniesView(page);
  });
</script>
