   <!-- Main content -->
   <div class="modal fade" id="addTodoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Note</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="add_dsr_modal_body" class="modal-body">
            @include('admin.todos.create')
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

   <div class="modal fade" id="editTodoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add DSR</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="edit_dsr_modal_body" class="modal-body">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>



    {{-- <a href="#" id="addTodoButton" class="btn btn-outline-success btn-lg" data-toggle="modal" data-target="#addTodoModal">
        <i class="fa fa-plus" aria-hidden="true"></i>New Todo
    </a> --}}
    <div class="modal fade" id="createDeal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Todos</h5>
              <button type="button" class="close" id='createDealHide' data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body " id="todoBody">
            </div>
          </div>
        </div>
      </div>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!--  -->
                <div class="card-body">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width: 10px">#</th>
                          <th>Action</th>
                          <th>Title</th>
                          <th>Description</th>
                          <th>Priority</th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach ($data as $key=> $value )
                          <tr>
                              <td>
                                {{$key+1}}
                              </td>
                              <td>
                                <div class="btn-group">
                                    <button data-id="{{$value->id}}" onclick="updateTodo(this)" class="btn btn-outline-success btn-xs  update" ><i class="fas fa-pencil-alt"></i></button>
                                    <button data-id="{{$value->id}}" class="btn btn-outline-success btn-xs view" onclick="viewTodoDetail(this)"><i class="fas fa-eye"></i></button>
                                    <button data-id="{{$value->id}}" onclick="removeTodo(this)" class="btn btn-danger btn-xs  remove"><i class="fas fa-times"></i></button>

                                </div>
                              </td>
                              <td>
                                  {{$value->title}}
                                </td>
                                <td>
                                {!!$value->description!!}
                            </td>
                            <td>
                                {{($value->priority_level == 1) ? "Highest" : ''}}
                                {{($value->priority_level == 2) ? "Medium" : ''}}
                                {{($value->priority_level == 3) ? "Low" : ''}}
                                {{($value->priority_level == 4) ? "Lowest" : ''}}
                            </td>
                        </tr>
                        @endforeach

                      </tbody>
                    </table>
                </div>

                <!--  -->
            </div>
        </div>
        {{$data->links()}}
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
    <script>

$("#search").on("keyup",(e)=>{
    fetchTodoData(1);
  });


    $("#addTodoButton").on('click', function (){
      document.querySelector("#error_title").innerText="";
      document.querySelector("#error_priority_level").innerText="";
    // $("#view").html($("#add_dsr_modal_body").html())
    // initializeCreateForm()
    $("#add_todo_form").on('submit', function (e){
        e.preventDefault();
        var descData = ($("#editor").html());
        $("#description").val(descData);
        var data = new FormData(this);
        console.log(data);
        $.ajax({
            type:'post',
            url:"{{route('todo-store')}}",
            cache: false,
                contentType: false,
                processData: false,
                dataType: "JSON",
            data : {data: data},
        headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data:data,
            success:function(data){
                $('#addTodoModal').modal('hide');
                $(".modal-backdrop").hide();
                $("body").removeClass("modal-open");
                fetchTodoData(1);
                Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Added Successfully',
                showConfirmButton: false,
                timer: 1500
                })
            },
            error:function(data){
            $.each(data.responseJSON.errors, function(id,msg){
                $('#error_'+id).html(msg);

            })
            }
        });
        })

})
$(document).on('click', '#post_pagination .pagination a', function(event){
 event.preventDefault();
 var page = $(this).attr('href').split('page=')[1];
 var filter=document.querySelector("#tabs_filter").value;
 ajax('get',`{{Route("employee-pagination")}}`,data={page:page},'#pagination_employee');
});

function removeTodo(e)
{

  var id=e.getAttribute('data-id');
  swal({
  title: "Oops....",
  text: "Are You Sure You want to delete!",
  icon: "error",
  buttons: [
    'NO',
    'YES'
  ],
}).then(function(isConfirm) {
  if (isConfirm) {
    $.ajax({
      url:"{{route('todo-remove-data')}}",
      type:"post",
      data:{id:id},
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
      success:function(data){
        Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Remove Successfully',
                showConfirmButton: false,
                timer: 1500
                })
        fetchTodoData(1);
        }
    })
  } else {

  }
});
}


function updateTodo(e)
{
  $('#page-loader').show();

  var id=e.getAttribute('data-id');
  $.ajax({
    url:"{{route('todo-edit')}}",
    type:"get",
    data:{id:id},
    success:function(data)
    {
       // toastr.success(response.message)
      $('#page-loader').hide();
      $('#edit_dsr_modal_body').empty().html(data);
    $("#editTodoModal").modal('show');
      intitializeUpdate();
    },
    error:function(){
      $('#page-loader').hide();
    }
  })
}
function intitializeUpdate(){

// initializeCreateForm()
    $("#update_todo_form").on('submit', function (e){
        e.preventDefault();
        var descData = ($("#editorupdate").html());
        console.log(descData)
        $("#update-description").val(descData);

        var data = new FormData(this);
        console.log(data);
        $.ajax({
            type:'post',
            url:"{{route('todo-store')}}",
            cache: false,
                contentType: false,
                processData: false,
                dataType: "JSON",
            data : {data: data},
        headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data:data,
            success:function(data){
                $('#addTodoModal').modal('hide');
                $(".modal-backdrop").hide();
                fetchTodoData(1);
                Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Updated Successfully',
                showConfirmButton: false,
                timer: 1500
                })
            },
            error:function(data){
            $.each(data.responseJSON.errors, function(id,msg){
                $('#error_'+id).html(msg);

            })
            }
        });
        })

}

const viewTodoDetail = (e)=>{
    let id = $(e).data('id')
    $.ajax({
    url:"{{route('todo-detail-view')}}",
    type:"get",
    data:{id:id},
    success:function(data)
    {
       // toastr.success(response.message)
      $('#page-loader').hide();
      $('#edit_dsr_modal_body').empty().html(data);
    $("#editTodoModal").modal('show');
      intitializeUpdate();
    },
    error:function(){
      $('#page-loader').hide();
    }
  })
}
</script>
