
<table class="table">
    <tbody>
      <tr>
        <th scope="row">Title</th>
        <td>{{$thisTodo->title}}</td>
      </tr>
      <tr>
        <th scope="row">Description</th>
        <td>{!!$thisTodo->description!!}</td>
      </tr>
      <tr>
        <th scope="row">Created At</th>
        <td>{{timestampToDateAndTimeObj($thisTodo->created_at)->time}}</td>
      </tr>
      <tr>
        <th scope="row">Created On</th>
        <td>{{timestampToDateAndTimeObj($thisTodo->created_at)->date}}</td>
      </tr>
      <tr>
        <th scope="row">Priority</th>
        <td>
            {{($thisTodo->priority_level == 1) ? "Higest" : ''}}
            {{($thisTodo->priority_level == 2) ? "Medium" : ''}}
            {{($thisTodo->priority_level == 3) ? "Low" : ''}}
            {{($thisTodo->priority_level == 4) ? "Lowest" : ''}}

        </td>
      </tr>

    </tbody>
  </table>

