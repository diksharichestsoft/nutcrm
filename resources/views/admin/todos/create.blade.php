<!-- Button trigger modal -->
  <!-- Modal -->

            <form id="add_todo_form" action="" method="POST">
            <div id="dsr_item_container">
                @csrf
                <div class="dsr_items">
                    <div class="form-group">
                        <label for="project_id">Title</label>
                        <br>
                        <input type="text" class="form-control" name="title">
                        <input type="hidden" class="form-control" name="user_id" value="{{Auth::user()->id}}">
                        <input type="hidden" name="reqType" value="0">
                        <div class="error" id="error_title"></div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col col-form-label">Description</label>
                          <div class="col-sm-10">
                              <input type="hidden" id="description" class="form-control" name="description">
                              <div id="editor" style="border: 1px solid #ffffff42;"></div>
                          </div>
                      </div>
                      <div class="form-group">
                        <label for="priority_level" class="col-sm-2 col-form-label">Priority</label>
                        <div class="col-sm-10">
                          <select name="priority_level" id="priority_level" class="form-control select2">
                            <option disabled selected >Select Priority</option>
                              <option value="1">Highest</option>
                              <option value="2">Medium</option>
                              <option value="3">Low</option>
                              <option value="4">Lowest</option>
                          </select>
                          <div class="error" id="error_priority_level"></div>
                        </div>
                      </div>

                </div>
            </div>
            <br>
                {{-- <button type="submit" class="btn btn-primary">Save</button> --}}
                <button type="button" class="btn btn-primary" onclick="$('#add_todo_form').submit()"> Save</button>

            </form>


  <script>

</script>


<script>
    var myEditor;
    InlineEditor
    .create( document.querySelector( `#editor` ) )
    .then( editor => {
        console.log( 'Editor was initialized', editor );
        myEditor = editor;
    } )
    .catch( err => {
        console.error( err.stack );
    } );
    $(document).ready(function (){
    // Hiding default extrack file and dropdown buttons
    $(".ck-file-dialog-button").hide();
    $(".ck-dropdown__button").hide();
    })
</script>
