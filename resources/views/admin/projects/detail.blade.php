@extends('admin.layout.template')
@section('contents')
<style>
    .switch{
    opacity: 0;
    position: absolute;
    z-index: 1;
    width: 18px;
    height: 18px;
    cursor: pointer;
  }
  .switch + .lable{
    position: relative;
    display: inline-block;
    margin: 0;
    line-height: 20px;
    min-height: 18px;
    min-width: 18px;
    font-weight: normal;
    cursor: pointer;
  }
  .switch + .lable::before{
    cursor: pointer;
    font-weight: normal;
    font-size: 12px;
    color: #32a3ce;
    content: "\a0";
    background-color: #FAFAFA;
    border: 1px solid #c8c8c8;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
    border-radius: 0;
    display: inline-block;
    text-align: center;
    height: 16px;
    line-height: 14px;
    min-width: 16px;
    margin-right: 1px;
    position: relative;
    top: -1px;
  }
  .switch:checked + .lable::before {
    display: inline-block;
    content: '\f00c';
    background-color: #F5F8FC;
    border-color: #adb8c0;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
  }
  /* CSS3 on/off switches */
  .switch + .lable {
    margin: 0 4px;
    min-height: 24px;
  }
  .switch + .lable::before {
    font-weight: normal;
    font-size: 11px;
    line-height: 17px;
    height: 20px;
    overflow: hidden;
    border-radius: 12px;
    background-color: #F5F5F5;
    -webkit-box-shadow: inset 0 1px 1px 0 rgba(0, 0, 0, 0.15);
    box-shadow: inset 0 1px 1px 0 rgba(0, 0, 0, 0.15);
    border: 1px solid #CCC;
    text-align: left;
    float: left;
    padding: 0;
    width: 52px;
    text-indent: -21px;
    margin-right: 0;
    -webkit-transition: text-indent .3s ease;
    -o-transition: text-indent .3s ease;
    transition: text-indent .3s ease;
    top: auto;
  }
  .switch.switch-bootstrap + .lable::before {
    font-family: FontAwesome;
    content: "\f00d";
    box-shadow: none;
    border-width: 0;
    font-size: 16px;
    background-color: #a9a9a9;
    color: #F2F2F2;
    width: 52px;
    height: 22px;
    line-height: 21px;
    text-indent: 32px;
    -webkit-transition: background 0.1s ease;
    -o-transition: background 0.1s ease;
    transition: background 0.1s ease;
  }
  .switch.switch-bootstrap + .lable::after {
    content: '';
    position: absolute;
    top: 2px;
    left: 3px;
    border-radius: 12px;
    box-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
    width: 18px;
    height: 18px;
    text-align: center;
    background-color: #F2F2F2;
    border: 4px solid #F2F2F2;
    -webkit-transition: left 0.2s ease;
    -o-transition: left 0.2s ease;
    transition: left 0.2s ease;
  }
  .switch.switch-bootstrap:checked + .lable::before {
    content: "\f00c";
    text-indent: 6px;
    color: #FFF;
    border-color: #b7d3e5;

  }
  .switch-primary >.switch.switch-bootstrap:checked + .lable::before {
      background-color: #337ab7;
  }
  .switch-success >.switch.switch-bootstrap:checked + .lable::before {
      background-color: #5cb85c;
  }
  .switch-danger >.switch.switch-bootstrap:checked + .lable::before {
      background-color: #d9534f;
  }
  .switch-info >.switch.switch-bootstrap:checked + .lable::before {
      background-color: #5bc0de;
  }
  .switch-warning >.switch.switch-bootstrap:checked + .lable::before {
      background-color: #f0ad4e;
  }
  .switch.switch-bootstrap:checked + .lable::after {
    left: 32px;
    background-color: #FFF;
    border: 4px solid #FFF;
    text-shadow: 0 -1px 0 rgba(0, 200, 0, 0.25);
  }
  /* square */
  .switch-square{
    opacity: 0;
    position: absolute;
    z-index: 1;
    width: 18px;
    height: 18px;
    cursor: pointer;
  }
  .switch-square + .lable{
    position: relative;
    display: inline-block;
    margin: 0;
    line-height: 20px;
    min-height: 18px;
    min-width: 18px;
    font-weight: normal;
    cursor: pointer;
  }
  .switch-square + .lable::before{
    cursor: pointer;
    font-family: fontAwesome;
    font-weight: normal;
    font-size: 12px;
    color: #32a3ce;
    content: "\a0";
    background-color: #FAFAFA;
    border: 1px solid #c8c8c8;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
    border-radius: 0;
    display: inline-block;
    text-align: center;
    height: 16px;
    line-height: 14px;
    min-width: 16px;
    margin-right: 1px;
    position: relative;
    top: -1px;
  }
  .switch-square:checked + .lable::before {
    display: inline-block;
    content: '\f00c';
    background-color: #F5F8FC;
    border-color: #adb8c0;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
  }
  /* CSS3 on/off switches */
  .switch-square + .lable {
    margin: 0 4px;
    min-height: 24px;
  }
  .switch-square + .lable::before {
    font-weight: normal;
    font-size: 11px;
    line-height: 17px;
    height: 20px;
    overflow: hidden;
    border-radius: 2px;
    background-color: #F5F5F5;
    -webkit-box-shadow: inset 0 1px 1px 0 rgba(0, 0, 0, 0.15);
    box-shadow: inset 0 1px 1px 0 rgba(0, 0, 0, 0.15);
    border: 1px solid #CCC;
    text-align: left;
    float: left;
    padding: 0;
    width: 52px;
    text-indent: -21px;
    margin-right: 0;
    -webkit-transition: text-indent .3s ease;
    -o-transition: text-indent .3s ease;
    transition: text-indent .3s ease;
    top: auto;
  }
  .switch-square.switch-bootstrap + .lable::before {
    font-family: FontAwesome;
    content: "\f00d";
    box-shadow: none;
    border-width: 0;
    font-size: 16px;
    background-color: #a9a9a9;
    color: #F2F2F2;
    width: 52px;
    height: 22px;
    line-height: 21px;
    text-indent: 32px;
    -webkit-transition: background 0.1s ease;
    -o-transition: background 0.1s ease;
    transition: background 0.1s ease;
  }
  .switch-square.switch-bootstrap + .lable::after {
    content: '';
    position: absolute;
    top: 2px;
    left: 3px;
    border-radius: 12px;
    box-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
    width: 18px;
    height: 18px;
    text-align: center;
    background-color: #F2F2F2;
    border: 4px solid #F2F2F2;
    -webkit-transition: left 0.2s ease;
    -o-transition: left 0.2s ease;
    transition: left 0.2s ease;
  }
  .switch-square.switch-bootstrap:checked + .lable::before {
    content: "\f00c";
    text-indent: 6px;
    color: #FFF;
    border-color: #b7d3e5;

  }
  .switch-primary >.switch-square.switch-bootstrap:checked + .lable::before {
      background-color: #337ab7;
  }
  .switch-success >.switch-square.switch-bootstrap:checked + .lable::before {
      background-color: #5cb85c;
  }
  .switch-danger >.switch-square.switch-bootstrap:checked + .lable::before {
      background-color: #d9534f;
  }
  .switch-info >.switch-square.switch-bootstrap:checked + .lable::before {
      background-color: #5bc0de;
  }
  .switch-warning >.switch-square.switch-bootstrap:checked + .lable::before {
      background-color: #f0ad4e;
  }
  .switch-square.switch-bootstrap:checked + .lable::after {
    left: 32px;
    background-color: #FFF;
    border: 4px solid #FFF;
    text-shadow: 0 -1px 0 rgba(0, 200, 0, 0.25);
  }
  .switch-square.switch-bootstrap + .lable::after {
      border-radius: 2px;
  }
    .daterangepicker .ranges li {font-size: 12px;padding: 8px 12px;cursor: pointer;background: #000000b0;}
        /* .dark-mode .list-group-item {
      border-color: #454d55;
      background-color: #343a40;
    } */
        .icon {
            color: #007bff;
            margin-right: 10px;
        }

        .card-header {
            border-bottom: 1px solid #454d55;
        }

        .dark-mode .list-group-item {
            border-color: #343a40;
            background-color: #454d55;
        }

    </style>
    <div id="permissions_data">

        {{-- Modal --}}
        <div class="modal fade" id="addTeamModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Team Member</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="" id="add_team_form">
                            @csrf
                            <div class="mb-3">
                                <label for="input_team" class="form-label">Add Team Member</label>

                                <select class="selectpicker" name="team_user_id" data-live-search="true">
                                    @foreach ($allTeamMembers as $teamMember)
                                        <option value="{{ $teamMember->id }} {{ $teamMember->name }}"
                                            id="option_{{ $teamMember->id }}">{{ $teamMember->name }}</option>
                                    @endforeach
                                </select>

                                <input type="hidden" value="{{ $getProjectData->id }}" name="project_id"
                                    class="form-control" id="project_id">
                            </div>
                            <div class="mb-3">
                                <label for="project_hours" class="form-label">Select Hours</label>
                                {{-- <input type="number" class="form-control" name="project_hours" id="project_hours"> --}}
                                <input type="number" onchange="leadingZeros(this)" min="00" class="form-control"
                                    id="project_hours" name="project_hou" style="width: 150px; display: initial"><input
                                    type="number" onchange="leadingZeros(this)" min="00" id="project_minutes"
                                    class="form-control" style="width: 150px; display: initial" name="project_min">

                            </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    </form>

                </div>
            </div>
        </div>

        {{-- Add comment Modal --}}
        <div class="modal fade" id="addComment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Comment</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body " id="addCommentBody">
                        <form action="" name="addCommentForm" id="addCommentForm">
                            @csrf
                            <input type="hidden" name='deal_id' id="deal_id" value="{{ $getProjectData->id }}">
                            <div class="form-group row">
                                <label for="comment" class="col-sm-2 col-form-label">Type</label>
                                <div class="col-sm-10">
                                    <select name="branch_id" id="" class="form-control">
                                        <option value="1" selected>Dev</option>
                                        <option value="0">Sales</option>
                                    </select>
                                </div>
                            </div>
                            <div class="value">
                                <div class="form-group row">
                                    <label for="comment" class="col-sm-2 col-form-label">Comment</label>
                                    <div class="col-sm-10">
                                        {{-- <textarea name="comment" id="comment" cols="30" rows="10" class="form-control"></textarea> --}}
                                        <input type="hidden" id="comment" class="form-control" name="comment">
                                        <div id="editor" style="border: 1px solid #ffffff42;"></div>

                                        <div class="error" id="error_comment"></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-sm-2 col-sm-10">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="changestatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel1">Assign Task</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body" id="changeStatuSbody">
                  <form action="" name="changestatusform" id="changestatusform">
                      @csrf
                      <input type="hidden" name='status' id="changeStatus-status" value="">
                      <input type="hidden" name='deal_id' id="changeStatus-deal_id" value="{{$getProjectData->id}}">
                      <div class="value">
                      <div class="form-group row">
                        <label for="comment" class="col-sm-2 col-form-label">Comment</label>
                        <div class="col-sm-10">
                          <textarea name="comment"  cols="30" rows="10" class="form-control"></textarea>
                          <div class="error" id="error_comment"></div>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>

        <div class="modal fade" id="createDeal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Projects</h5>
                        <button type="button" class="close" id='createDealHide' data-dismiss="modal"
                            aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body " id="createDealBody">
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Projects Detail</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>


                <div class="card-body">
                    <div class="row pb-4">
                        <div class="col-4">
                            <h5 class="mt-5 ">Project Details</h5>
                            <ul class="list-unstyled list-group">
                                <li class="list-group-item">
                                    <i class="far fa-fw fa-file-word icon"></i><b>Project Name:-</b>
                                    {{ $getProjectData->title ?? '' }}
                                </li>
                                <li class="list-group-item">
                                    <i class="far fa-fw fa-file-pdf icon"></i><b> Project Type:-</b>
                                    @foreach ($getProjectData->projectTypes as $thisProjectType)
                                        {{ $thisProjectType->name }},
                                    @endforeach
                                </li>
                                <li class="list-group-item">
                                    <i class="far fa-fw fa-envelope icon"></i><b>Reffred By:-
                                    </b>{{ $getProjectData->user_name ?? '' }}
                                </li>
                                <li class="list-group-item">
                                    <i class="far fa-fw fa-image icon"></i><b>Platform:-</b>
                                    {{ $getProjectData->platform_name ?? '' }}
                                </li>
                                <li class="list-group-item">
                                    <i class="far fa-fw fa-image icon"></i><b>Platform ID:-</b>
                                    {{ $getProjectData->platform_id ?? '' }}
                                </li>
                                <li>
                                    <div class="row mt-4">
                                        <div class="col-md-3">
                                            Actions -
                                        </div>
                                        <div class="col-md-9">
                                            <div class="btn-group">
                                                @can('projects_edit')
                                                    <button data-id="{{ $getProjectData->id }}"
                                                        onclick="updateCompanyProject(this)"
                                                        class="btn btn-outline-success btn-xs  update" data-toggle="modal"
                                                        data-target="#createDeal"><i class="fas fa-pencil-alt"></i></button>
                                                @endcan
                                                @can('projects_delete')
                                                    <button data-id="{{ $getProjectData->id }}"
                                                        onclick="removeCompanyProject(this)"
                                                        class="btn btn-danger btn-xs  remove"><i
                                                            class="fas fa-times"></i></button>
                                                @endcan
                                                @can('project_invoices_view')
                                                <a href="{{route('project-transaction', $getProjectData->id)}}" class="btn btn-success btn-xs" target="_blank">Payments</a>

                                                @endcan
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        @can('projects_Show_Client_data')
                            <div class="col-4">
                                <h5 class="mt-5 text">Client Infromation</h5>
                                <ul class="list-unstyled">
                                    <li class="list-group-item">
                                        <i class="far fa-user-circle icon"></i>&nbsp;<b>Client Name:-
                                        </b>{{ $getProjectData->client_name ?? '' }}
                                    </li>
                                    <li class="list-group-item">
                                        <i class="fas fa-envelope-square icon"></i>&nbsp;<b>Client Email:-</b>
                                        {{ $getProjectData->client_email ?? '' }}
                                    </li>
                                    <li class="list-group-item">
                                        <i class="fas fa-phone-square-alt icon"></i>&nbsp;<b>Client Phone:-</b>
                                        {{ $getProjectData->client_phone ?? '' }}
                                    </li>

                                    @if($getProjectData->client_shipping_address != null)
                                    <li class="list-group-item">
                                        <i class="fas fa-phone-square-alt icon"></i>&nbsp;<b>Client Shipping AddressAddress:-</b>
                                        {!! $getProjectData->client_shipping_address ?? '' !!}
                                    </li>
                                    @endif

                                    @if ($getProjectData->client_whatsapp_number != null)
                                        <li class="list-group-item">
                                            <i class="fas fa-envelope-square icon"></i>&nbsp;<b>Whatsapp Number:-</b>
                                            {{ $getProjectData->client_whatsapp_number ?? '' }}
                                        </li>
                                    @endif
                                    @if ($getProjectData->client_skype != null)
                                        <li class="list-group-item">
                                            <i class="fas fa-envelope-square icon"></i>&nbsp;<b>Skype:-</b>
                                            {{ $getProjectData->client_skype ?? '' }}
                                        </li>
                                    @endif
                                    @if ($getProjectData->client_slack_link != null)
                                        <li class="list-group-item">
                                            <i class="fas fa-envelope-square icon"></i>&nbsp;<b>Slack Link:-</b>
                                            {{ $getProjectData->client_slack_link ?? '' }}
                                        </li>
                                    @endif
                                    @if ($getProjectData->client_slack_username != null)
                                        <li class="list-group-item">
                                            <i class="fas fa-envelope-square icon"></i>&nbsp;<b>Slack Username:-</b>
                                            {{ $getProjectData->client_slack_username ?? '' }}
                                        </li>
                                    @endif
                                    @if ($getProjectData->client_slack_password != null)
                                        <li class="list-group-item">
                                            <i class="fas fa-envelope-square icon"></i>&nbsp;<b>Slack Password:-</b>
                                            {{ $getProjectData->client_slack_password ?? '' }}
                                        </li>
                                    @endif
                                    @if ($getProjectData->client_telegram_number != null)
                                        <li class="list-group-item">
                                            <i class="fas fa-envelope-square icon"></i>&nbsp;<b>Telegram Number:-</b>
                                            {{ $getProjectData->client_telegram_number ?? '' }}
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        @endcan
                        <div class="col-4">
                            <h5 class="mt-5 text">Project Times Details</h5>
                            <ul class="list-unstyled">
                                @can('projects_change_status')
                                <li class="list-group-item">
                                    <div scope="row">Status</div>
                                    <div class="d-flex">
                                        <select name="sel" id="selectsatus" class="form-control deals_status" data-status="{{$getProjectData->deal_status}}" data-id="{{ $getProjectData->id }}">
                                            <option value="0">Select Status</option>
                                            @foreach ($ProjectStatus as $status)
                                                <option value="{{ $status->id }}"
                                                    {{ $getProjectData->deal_status == $status->id ? 'selected' : '' }}>
                                                    {{ $status->dealname }}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </li>
                                @endcan
                                @if ($getProjectData->deal_status == 3)
                                    <li class="list-group-item">
                                        <i class="fas fa-link icon icon"></i>&nbsp;<b>Meeting Time:-
                                        </b>{{ date('d-m-Y h:i a', strtotime($getProjectData->meeting_time)) ?? '' }}
                                    </li>
                                @endif
                                <li class="list-group-item">
                                    <i class="fas fa-stopwatch icon"></i>&nbsp;<b>Planned Start Date:-</b>
                                    {{ $getProjectData->planned_start_date ?? '' }}
                                </li>
                                <li class="list-group-item">
                                    <i class="fas fa-business-time icon"></i>&nbsp;<b>Planned End Date:-</b>
                                    {{ $getProjectData->planned_start_date ?? '' }}
                                </li>
                                <li class="list-group-item">
                                    <i class="fas fa-hourglass-half icon"></i>&nbsp;<b>Estimated Hour:-
                                    </b>{{ $getProjectData->estimated_hours ?? '' }}
                                </li>
                                <li class="list-group-item">
                                    <i class="fas fa-link icon icon"></i>&nbsp;<b>Url:- </b><a
                                        href="{{ $getProjectData->url ?? '' }}">{{ $getProjectData->url ?? '' }}</a>
                                </li>
                                <li class="list-group-item">
                                    <i class="fas fa-link icon icon"></i>&nbsp;<b>Created At:-
                                    </b>{{ $getProjectData->created_at->format('d-m-Y h:i a') ?? '' }}
                                </li>
                                <li class="list-group-item">
                                    <i class="fas fa-link icon icon"></i>&nbsp;<b>Updated At:-
                                    </b>{{ $getProjectData->updated_at->format('d-m-Y h:i a') ?? '' }}
                                </li>

                            </ul>
                        </div>

                    </div>


                   <div class="row">
                    <div class="card col-12" id="data">
                        <div class="card-header p-2" id="card_head">
                          <ul class="nav nav-pills">
                            <li class="nav-item"><a class="nav-link services active" href="#details" onclick="" data-id="" data-toggle="tab">Details</a></li>&nbsp;
                            @can('projects_add_team')
                            <li class="nav-item"><a class="nav-link services" href="#addTeam" onclick="addTeamview()" data-id="" data-toggle="tab">Add Team</a></li>&nbsp;
			                @endcan
				@can ('project_sheets_view')
				<li class="nav-item"><a class="nav-link services" href="#projectSheets" onclick="renderProjectSheetsView()" data-id="" data-toggle="tab">Sheets</a></li>&nbsp;
			    @endcan
                @can('project_addons_view')
				<li class="nav-item"><a class="nav-link services" href="#projectAddons" onclick="renderProjectAddonsView()" data-id="" data-toggle="tab">Addons</a></li>&nbsp;
                @endcan
				    @can ('project_demos_view')
				    <li class="nav-item"><a class="nav-link services" href="#projectDemos" onclick="renderProjectDemosView()" data-id="" data-toggle="tab">Demo Info</a></li>&nbsp;
			    		@endcan

						@can ('project_servers_view')
						<li class="nav-item"><a class="nav-link services" href="#projectServers" onclick="renderProjectServersView()" data-id="" data-toggle="tab">Server</a></li>
						@endcan
                        @can('projects_show_subscription_tab')

                        <li class="nav-item"><a class="nav-link services " href="#subscriptions" onclick="" data-id="" data-toggle="tab">Subscriptions</a></li>
                    </li>
                    @endcan
                          </ul>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                          <div class="tab-content">
                            <div class="active tab-pane" id="details">
                                <h3>Project Description :</h3>
                                <p>
                                    {{$getProjectData->job_descriprion}}
                                </p>
                            </div>
                            @can('projects_add_team')

                            <div class="tab-pane" id="addTeam">
                                {{-- @include('admin.projects.includes.addteam') --}}
                            </div>
			    @endcan

			    <div class="tab-pane" id="projectSheets">
			    </div>

			    <div class="tab-pane" id="projectDemos">
			    </div>

			    <div class="tab-pane" id="projectServers">
			    </div>
			    <div class="tab-pane" id="projectAddons">
			    </div>
                <div class="tab-pane" id="subscriptions">
                    @include('admin.projects.includes.subscriptions')
			    </div>
                            <!-- /.tab-pane -->
                          </div>
                          <!-- /.tab-content -->
                        </div><!-- /.card-body -->
                </div>


                    </div>


                    @can('projects_budget_view')

                        <div class="row">
                            <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
                                <div class="row">
                                    <div class="col-12 col-sm-12">
                                        <div class="info-box bg-light">
                                            <div class="info-box-content">
                                                <span class="info-box-text text-center text-muted">Estimated budget</span>
                                                <span
                                                    class="info-box-number text-center text-muted mb-0">{{ $getProjectData->budget ?? '' }}</span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            @endcan

                            <div class="row">
                                <div class="col-12">
                                    <br>


                                </div>
                                <div class="row">
                                    <div class="col-12 mt-3 mb-3">

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        @foreach ($comments as $comment)
                                            <div class="post">
                                                <div class="user-block">
                                                    <img class="img-circle img-bordered-sm"
                                                        src="../../dist/img/user1-128x128.jpg" alt="user image">
                                                    <span class="username">
                                                        <a href="#">{{ $comment->name }}</a>
                                                    </span>
                                                    <span class="description">{{ $comment->title }} - 7:45 PM
                                                        today</span>
                                                </div>
                                                <!-- /.user-block -->
                                                <p>
                                                    {{ $comment->comment }}
                                                </p>

                                                <p>
                                                    <a href="#" class="link-black text-sm"><i
                                                            class="fas fa-link mr-1"></i> Demo File 1 v2</a>
                                                </p>
                                            </div>
                                        @endforeach

                                        {{-- ****************************************** Subscriptions ****************************************** --}}





                                    </div>

                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
        </section>
        <!-- Content Wrapper. Contains page content -->
        <!-- <div class="content-wrapper"> -->
        <!-- Content Header (Page header) -->

        <!-- Main content -->
        <div id="comment-section-view">
            @include('admin.projects.includes.timeline')
        </div>
        <!--End comment-section-view -->
        <!-- /.content -->
        <!-- </div> -->
        <!-- /.content-wrapper -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jscroll/2.4.1/jquery.jscroll.min.js"></script>
        <script>

            $(document).on('submit', '#subscription_submit_form', function(e) {
                $('#subscription_submit_form ').submit(false);

                e.preventDefault();
                var data = new FormData(this);
                $.ajax({
                    type: 'post',
                    url: "{{ route('add-subscription') }}",
                    dataType: "JSON",
                    xhr: function() {
                        myXhr = $.ajaxSettings.xhr();
                        return myXhr;
                    },
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function(data) {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Added Successfully',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    },
                    error: function(data) {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'error',
                            title: 'Failed to add',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    }
                });
            });

            $(document).on('click', '#addTeamModalToggle', function(e) {
                const team_users = [];
                @foreach ($getProjectData->users as $projectData)
                    team_users.push("{{ $projectData->id }}")
                @endforeach
                // employees = {{ $getProjectData->users }};
                team_users.forEach(single => {
                    $(`#option_${single}`).attr('selected', true);

                });
            })

            function leadingZeros(input) {
                if (!isNaN(input.value) && input.value.length === 1) {
                    input.value = '0' + input.value;
                }
            }
        </script>
        <script>
            function removeCompanyProject(e) {
                var id = e.getAttribute('data-id');
                swal({
                    title: "Oops....",
                    text: "Are You Sure You want to delete!",
                    icon: "error",
                    buttons: [
                        'NO',
                        'YES'
                    ],
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: "{{ route('projects-remove-data') }}",
                            type: "post",
                            data: {
                                id: id
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(data) {
                                $('#view').empty().html(data);
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'Removed Successfully',
                                    showConfirmButton: true
                                }).then(() => {
                                    window.location.replace("{{ route('projects', 'all') }}");
                                })
                            }
                        })
                    } else {

                    }
                });
            }

            function updateCompanyProject(e) {
                $('#page-loader').show();

                var id = e.getAttribute('data-id');
                $.ajax({
                    url: "{{ route('projects-edit') }}",
                    type: "get",
                    data: {
                        id: id
                    },
                    success: function(data) {
                        $('#page-loader').hide();
                        $('#createDealBody').empty().html(data);
                    },
                    error: function() {
                        $('#page-loader').hide();
                    }
                })
            }

            $(document).on('submit', '#updateCompany', function(e) {
                $("#client_shipping_address").val($("#clientShippingAddressUpdate").html())
                e.preventDefault();
                var data = new FormData(this);
                $.ajax({
                    type: 'post',
                    url: "{{ route('projects-store') }}",
                    dataType: "JSON",
                    xhr: function() {
                        myXhr = $.ajaxSettings.xhr();
                        return myXhr;
                    },
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function(data) {
                        $(".services:first").trigger("click");
                        $('#createDeal').modal('hide');
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Added Successfully, Reload the page to see the changes',
                            showConfirmButton: true
                        })
                    },
                    error: function(data) {
                        $.each(data.responseJSON.errors, function(id, msg) {
                            $('#error_' + id).html(msg);
                        });
                    }
                });
            });

            $(document).on('submit', '#add_team_form', function(e) {
                // $('#add_team_form').submit(false);
                if (!$("#project_minutes").val()) {
                    $("#project_minutes").val("00")
                }
                if (!$("#project_hours").val()) {
                    $("#project_hours").val("00")
                }
                e.preventDefault();
                var data = new FormData(this);
                $.ajax({
                    type: 'post',
                    url: "{{ route('add-team-member') }}",
                    dataType: "JSON",
                    xhr: function() {
                        myXhr = $.ajaxSettings.xhr();
                        return myXhr;
                    },
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function(data) {
                        $("#addTeamModal").modal('hide');
                        addTeamview();
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Added Successfully',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    },
                    error: function(data) {}
                });
            });

            function addTeamview() {
                $.ajax({
                    type: 'get',
                    url: "{{ route('view-team-management') }}?id={{ $getProjectData->id }}",
                    cache: false,
                    contentType: false,
                    processData: false,
                    xhr: function() {
                        myXhr = $.ajaxSettings.xhr();
                        return myXhr;
                    },
                    success: function(data) {

                        $("#addTeam").empty().html(data);
                    }

                })

            }

            $(document).on('submit', '#addTask', function(e) {
                e.preventDefault();
                var data = new FormData(this);
                $.ajax({
                    type: 'post',
                    url: "{{ route('tasks-store') }}",
                    dataType: "JSON",
                    xhr: function() {
                        myXhr = $.ajaxSettings.xhr();
                        return myXhr;
                    },
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function(data) {
                        $(".services:first").trigger("click");
                        $('#createDeal').modal('hide');
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Added Successfully',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    },
                    error: function(data) {
                        $.each(data.responseJSON.errors, function(id, msg) {
                            $('#error_' + id).html(msg);
                        })
                    }
                });
            });




            $(document).on('submit', '#addCommentForm', function(e) {
                $('#page-loader').show();
                var commentData = ($("#editor").html());
                $("#comment").val(commentData);

                e.preventDefault();
                var data = new FormData(this);
                $.ajax({
                    type: 'post',
                    url: "{{ route('projects-Comment') }}",
                    dataType: "JSON",
                    xhr: function() {
                        myXhr = $.ajaxSettings.xhr();
                        return myXhr;
                    },
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function(data) {
                        $('#addComment').modal('hide');
                        getCommentData();
                        $('#page-loader').hide();
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Added Successfully',
                            showConfirmButton: false,
                            timer: 1500
                        });

                        myEditor.setData('');

                    },
                    error: function(data) {
                        $.each(data.responseJSON.errors, function(id, msg) {
                            $('.error#error_' + id).html(msg);
                            $('#page-loader').hide();

                        })
                    }
                });
            });

            function getCommentData(page = 1,branch_id = 1) {
                $('#page-loader').show();
                $.ajax({
                    url: "{{ route('projects-get-timeline') }}",
                    data: {
                        id: "{{ $getProjectData->id }}",
                        page: page,branch_id,
                    },

                    success: function(data) {
                        $('#comment-section-view').empty().html(data);
                        $('#page-loader').hide();

                    }
                });
            }
            getCommentData();

        </script>


        <script id="forProjectSheets">
            function renderProjectSheetsView() {
                var project_id = "{{ $getProjectData->id }}";
                $.ajax({
                    type: 'get',
                    url: "{{ route('project-sheets') }}",
                    data: {
                        'project_id': project_id
                    },
                    xhr: function() {
                        myXhr = $.ajaxSettings.xhr();
                        return myXhr;
                    },
                    success: function(data) {
                        $("#projectSheets").empty().html(data);
                        console.log("content added");
                        window.jsnn = data;
                    }

                })
            }

            function isAnyInputFilledInForm(form_id) {

                var elements = document.getElementById(form_id).getElementsByTagName('input');
                for (var i = 0; i < elements.length; i++) {

                    if (elements[i].value.length > 0 && elements[i].hidden == false && elements[i].name != "_token") {
                        console.log("this has value");
                        console.log(elements[i]);
                        return true;
                    }
                }

                var textareaElement = document.getElementById(form_id).getElementsByTagName('textarea')[0];
                if (textareaElement.value.length > 0) {
                    return true;
                }
                return false;
            }

            function showDangerAlertIn(alert_block_id) {
                var alertBlock = document.getElementById(alert_block_id);
                var dangerAlertHtml =
                    ' <div class="alert alert-danger alert-dismissible fade show"> <button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error</strong> Please fill any input field.</div>';
                alertBlock.innerHTML = dangerAlertHtml;
                console.log(`alert block displayed in: ${alert_block_id}`);
            }

            function saveProjectSheet(projectSheetData) {
                console.log("save project sheet");
                $('#page-loader').show();

                $.ajax({

                    url: '{{ route('project-sheets-store') }}',
                    type: 'post',
                    dataType: "JSON",
                    xhr: function() {
                        myXhr = $.ajaxSettings.xhr();
                        return myXhr;
                    },

                    cache: false,
                    contentType: false,
                    processData: false,
                    data: projectSheetData,

                    success: function(successResponse) {
                        $("#closeSheetModalBtn").trigger('click');
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();
                        $('#page-loader').hide();
                        renderProjectSheetsView();
                        getCommentData();

                    },

                    error: function(errorResponse) {

                        console.log("next is errors");
                        console.log(errorResponse);
                        for (const [key, value] of Object.entries(errorResponse.responseJSON.errors)) {
                            console.log(`key: ${key}, value: ${value}`);
                            $(`#errors_${key}`).html(value);
                        }
                        $('#page-loader').hide();
                    }
                });

            }

            // $(document).on('submit', '#add_sheet_form', function(e) {
            // 	e.preventDefault();

            // 	if ( isAnyInputFilledInForm('add_sheet_form') ){
            // 		var projectSheetData = new FormData(this);
            // 		saveProjectSheet( projectSheetData );
            // 	} else {
            // 		showDangerAlertIn( 'add_sheet_alert_block');
            // 	}
            // });

            function renderUpdateSheetModal(updateBtn) {
                var sheet_id = updateBtn.getAttribute("data-id");
                $.ajax({

                    type: 'get',
                    url: "{{ route('project-sheets-edit') }}",
                    data: {
                        sheet_id
                    },
                    xhr: function() {
                        myXhr = $.ajaxSettings.xhr();
                        return myXhr;
                    },

                    beforeSend: function() {
                        $("#page-loader").show();
                    },

                    complete: function() {
                        $("#page-loader").hide();
                    },

                    success: function(data) {
                        console.log("update modal fetched");
                        $("#updateSheetModalBlock").empty().html(data);
                        $("#updateSheetModal").modal('show');
                        window.jsnn = data;
                    }

                })

            }

            // function updateProjectSheet( projectSheetData )
            // {
            // 	$.ajax({

            // 		type: 'post',
            // 		url:  "{{ route('project-sheets-update') }}",
            //          	cache: false,
            // 	        contentType: false,
            // 		processData: false,
            // 		data: projectSheetData,
            // 		xhr: function() {
            // 			myXhr = $.ajaxSettings.xhr();
            // 			return myXhr;
            // 		},

            // 		beforeSend: function(){
            // 			$("#page-loader").show();
            // 		},

            // 		complete: function(){
            // 			$("#page-loader").hide();
            // 		},

            // 		success:function(data){

            // 			console.log("project sheet updated");
            // 			$("#updateSheetModal").modal('hide');
            // 			$('.modal-backdrop').remove();
            // 			$('body').removeClass('modal-open');
            // 			renderProjectSheetsView();
            // 			getCommentData();
            // 		}

            // 	})

            // }


            // $(document).on('submit', '#update_sheet_form', function(e) {
            // 	e.preventDefault();

            // 	if ( isAnyInputFilledInForm( 'update_sheet_form' ) ){

            // 		var projectSheetData = new FormData(this);
            // 		updateProjectSheet( projectSheetData );
            // 	} else {

            // 		showDangerAlertIn('update_sheet_alert_block');
            // 	}
            // });

            function deleteProjectSheet(deleteProjectSheetBtn) {
                var projectSheetId = deleteProjectSheetBtn.getAttribute('data-id');

                swal({
                    title: "Oops....",
                    text: "Are You Sure You want to delete!",
                    icon: "error",
                    buttons: ['NO', 'YES'],
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        console.log("is confirmed");
                        $.ajax({
                            url: "{{ route('project-sheets-delete') }}",
                            type: "post",
                            data: {
                                'sheet_id': projectSheetId
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(data) {
                                console.log("sheet deleted");
                                renderProjectSheetsView();
                                getCommentData();
                            }
                        });
                    } else {
                        console.log("not confirmed");
                    }
                });

            }
        </script>
        <script id="forProjectDemos">
            function renderProjectDemosView() {
                var project_id = "{{ $getProjectData->id }}";
                console.log(`rendering sheets of ${project_id}`);
                $.ajax({
                    type: 'get',
                    url: "{{ route('project-demos') }}",
                    data: {
                        project_id
                    },
                    xhr: function() {
                        myXhr = $.ajaxSettings.xhr();
                        return myXhr;
                    },
                    success: function(data) {
                        $("#projectDemos").empty().html(data);
                        console.log("content added");
                    }

                });
            }
            $(document).on('click', '.pagination a', function(event) {
                event.preventDefault();
                var page = $(this).attr('href').split('page=')[1];
                var sendurl = $(this).attr('href');
                getCommentData(page);
            });
        </script>
        <script id="forProjectServers">
            function renderProjectServersView() {
                var project_id = "{{ $getProjectData->id }}";
                $.ajax({
                    type: 'get',
                    url: "{{ route('project-servers') }}",
                    data: {
                        project_id
                    },
                    xhr: function() {
                        myXhr = $.ajaxSettings.xhr();
                        return myXhr;
                    },
                    success: function(data) {
                        $("#projectServers").empty().html(data);
                        console.log("content added");
                    }

                });
            }
        </script>
        <script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/inline/ckeditor.js"></script>
        <script>
            var myEditor;
            InlineEditor
                .create(document.querySelector(`#editor`))
                .then(editor => {
                    console.log('Editor was initialized', editor);
                    myEditor = editor;
                })
                .catch(err => {
                    console.error(err.stack);
                });
            $(document).ready(function() {
                // Hiding default extrack file and dropdown buttons
                $(".ck-file-dialog-button").hide();
                $(".ck-dropdown__button").hide();
            })
    //       }
    //     });
    //   });




//       $(document).on('submit','#addCommentForm',function(e){
//         $('#page-loader').show();
//         var commentData = ($("#editor").html());
//         $("#comment").val(commentData);

// e.preventDefault();
// var data = new FormData(this);
// $.ajax({
//   type:'post',
//   url:"{{route('projects-Comment')}}",
//   dataType: "JSON",
//   xhr: function() {
//         myXhr = $.ajaxSettings.xhr();
//         return myXhr;
//   },
//   cache: false,
//   contentType: false,
//   processData: false,
//   data:data,
//     success:function(data){
//         $('#addComment').modal('hide');
//         getCommentData();
//         $('#page-loader').hide();
//         Swal.fire({
//                 position: 'top-end',
//                 icon: 'success',
//                 title: 'Added Successfully',
//                 showConfirmButton: false,
//                 timer: 1500
// 	});

// 	myEditor.setData('');

//     },
//     error:function(data){
//       $.each(data.responseJSON.errors, function(id,msg){
//         $('.error#error_'+id).html(msg);
//         $('#page-loader').hide();

//       })
//     }
//   });
// });



  </script>


<script id="forProjectSheets">

function renderProjectSheetsView()
{
      var project_id = "{{ $getProjectData->id }}";
    $.ajax({
		type: 'get',
		url:  "{{ route('project-sheets') }}",
		data: { 'project_id': project_id },
		xhr: function() {
			myXhr = $.ajaxSettings.xhr();
			return myXhr;
		},
		success:function(data){
			$("#projectSheets").empty().html(data);
			console.log("content added");
			window.jsnn = data;
		}

    })
}

function isAnyInputFilledInForm( form_id ){

  var elements = document.getElementById( form_id ).getElementsByTagName('input');
  for(var i=0; i < elements.length; i++){

	  if ( elements[i].value.length > 0 && elements[i].hidden == false && elements[i].name != "_token" ){
		  console.log("this has value");
		  console.log( elements[i] );
		  return true;
	  }
  }

  var textareaElement =  document.getElementById( form_id ).getElementsByTagName('textarea')[0];
  if ( textareaElement.value.length > 0 ){
	  return true;
  }
  return false;
}

function showDangerAlertIn( alert_block_id )
{
	var alertBlock = document.getElementById( alert_block_id );
	var dangerAlertHtml = ' <div class="alert alert-danger alert-dismissible fade show"> <button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error</strong> Please fill any input field.</div>';
	alertBlock.innerHTML = dangerAlertHtml;
	console.log(`alert block displayed in: ${alert_block_id}`);
}

function saveProjectSheet( projectSheetData )
{
	console.log("save project sheet");
	$('#page-loader').show();

	$.ajax({

		url:'{{ route("project-sheets-store") }}',
		type:'post',
		dataType: "JSON",
		xhr: function() {
			myXhr = $.ajaxSettings.xhr();
			return myXhr;
		},

		cache: false,
		contentType: false,
		processData: false,
		data: projectSheetData,

		success:  function( successResponse ){
			$("#closeSheetModalBtn").trigger('click');
			$('body').removeClass('modal-open');
			$('.modal-backdrop').remove();
			$('#page-loader').hide();
			renderProjectSheetsView();
			getCommentData();

		},

		error: function( errorResponse ){

			console.log("next is errors");
			console.log( errorResponse );
			for (const [key, value] of Object.entries( errorResponse.responseJSON.errors)) {
				console.log( `key: ${key}, value: ${value}` );
				$(`#errors_${key}`).html( value );
			}
			$('#page-loader').hide();
		}
	});

}

// $(document).on('submit', '#add_sheet_form', function(e) {
// 	e.preventDefault();

// 	if ( isAnyInputFilledInForm('add_sheet_form') ){
// 		var projectSheetData = new FormData(this);
// 		saveProjectSheet( projectSheetData );
// 	} else {
// 		showDangerAlertIn( 'add_sheet_alert_block');
// 	}
// });

function renderUpdateSheetModal( updateBtn )
{
	var sheet_id = updateBtn.getAttribute("data-id");
	$.ajax({

		type: 'get',
		url:  "{{ route('project-sheets-edit') }}",
		data: { sheet_id },
		xhr: function() {
			myXhr = $.ajaxSettings.xhr();
			return myXhr;
		},

		beforeSend: function(){
			$("#page-loader").show();
		},

		complete: function(){
			$("#page-loader").hide();
		},

		success:function(data){
			console.log("update modal fetched");
			$("#updateSheetModalBlock").empty().html(data);
			$("#updateSheetModal").modal('show');
			window.jsnn = data;
		}

	})

}

// function updateProjectSheet( projectSheetData )
// {
// 	$.ajax({

// 		type: 'post',
// 		url:  "{{ route('project-sheets-update') }}",
//          	cache: false,
// 	        contentType: false,
// 		processData: false,
// 		data: projectSheetData,
// 		xhr: function() {
// 			myXhr = $.ajaxSettings.xhr();
// 			return myXhr;
// 		},

// 		beforeSend: function(){
// 			$("#page-loader").show();
// 		},

// 		complete: function(){
// 			$("#page-loader").hide();
// 		},

// 		success:function(data){

// 			console.log("project sheet updated");
// 			$("#updateSheetModal").modal('hide');
// 			$('.modal-backdrop').remove();
// 			$('body').removeClass('modal-open');
// 			renderProjectSheetsView();
// 			getCommentData();
// 		}

// 	})

// }


// $(document).on('submit', '#update_sheet_form', function(e) {
// 	e.preventDefault();

// 	if ( isAnyInputFilledInForm( 'update_sheet_form' ) ){

// 		var projectSheetData = new FormData(this);
// 		updateProjectSheet( projectSheetData );
// 	} else {

// 		showDangerAlertIn('update_sheet_alert_block');
// 	}
// });

function deleteProjectSheet( deleteProjectSheetBtn )
{
  var projectSheetId= deleteProjectSheetBtn.getAttribute('data-id');

  swal({
	title: "Oops....",
	text: "Are You Sure You want to delete!",
	icon: "error",
	buttons: [ 'NO', 'YES' ],
}).then( function(isConfirm) {
	if (isConfirm) {
		console.log("is confirmed");
		 $.ajax({
				url:"{{ route('project-sheets-delete') }}",
				type:"post",
				data:{ 'sheet_id': projectSheetId },
				headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
				success: function(data){
					console.log("sheet deleted");
					renderProjectSheetsView();
					getCommentData();
				}
		});
	} else {
		console.log("not confirmed");
	}
});

}
</script>
<script id="forProjectDemos">

function renderProjectDemosView()
{
      var project_id = "{{ $getProjectData->id }}";
      console.log( `rendering sheets of ${project_id}` );
    $.ajax({
		type: 'get',
		url:  "{{ route('project-demos') }}",
		data: { project_id },
		xhr: function() {
			myXhr = $.ajaxSettings.xhr();
			return myXhr;
		},
		success:function(data){
			$("#projectDemos").empty().html(data);
			console.log("content added");
		}

    });
}
$(document).on('click', '.pagination a', function(event){
  event.preventDefault();
  var page = $(this).attr('href').split('page=')[1];
  var sendurl=$(this).attr('href');
  getCommentData(page);
});
</script>
<script id="forProjectServers">

function renderProjectServersView()
{
    var project_id = "{{ $getProjectData->id }}";
    $.ajax({
		type: 'get',
		url:  "{{ route('project-servers') }}",
		data: { project_id },
		xhr: function() {
			myXhr = $.ajaxSettings.xhr();
			return myXhr;
		},
		success:function(data){
			$("#projectServers").empty().html(data);
			console.log("content added");
		}

    });
}


function renderProjectAddonsView(){
    let  project_id = "{{$getProjectData->id}}"
    $.ajax({
		type: 'get',
		url:  "{{ route('project-addons') }}",
		data: { project_id },
		xhr: function() {
			myXhr = $.ajaxSettings.xhr();
			return myXhr;
		},
		success:function(data){
			$("#projectAddons").empty().html(data);
		}

    });

}

    $(document).on('change','#selectsatus',function(){
        oldstatus = $(this).data('status')
      var status =$(this).val();
      $(this).val(oldstatus);
      $('#changeStatus-status').val(status);
      $('#changestatusform textarea').val('');
      $('#changestatus').modal('show');
    });

      $(document).ready(function(){

      $(".services").eq(0).addClass("active");
    });
    $(document).on('click','.comment',function(){
      var id = $(this).attr('data-id');
      $('#deal_id').val(id);
      $('#comment_title').val('');
      $('#comment').val('');

    });
    $(document).on('submit','#changestatusform',function(e){
        e.preventDefault();
        var data = new FormData(this);
      $.ajax({
          type:'post',
          url:"{{route('projects-change-status')}}",
          dataType: "JSON",
         xhr: function() {
               myXhr = $.ajaxSettings.xhr();
               return myXhr;
         },
         cache: false,
         contentType: false,
         processData: false,
          data:data,
          success:function(data){
              if(data.status == 0){
                  $('#changestatus').modal('hide');
                  Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: data.message,
                        showConfirmButton: false,
                        timer: 7000
                    })
                    return
              }else{
                  Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: "Status Changed successfully",
                        showConfirmButton: false,
                        timer: 2000
                    })
              }
              $('#changestatus').modal('hide');
              getCommentData(1,'');
          },
          error:function(data){
            $.each(data.responseJSON.errors, function(id,msg){
              $('#error_'+id).html(msg);
            })
          }
        });
  });

</script>
</div>
@endsection
