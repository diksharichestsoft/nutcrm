<!-- Add Servers Modal -->
<div class="modal fade" id="addAddonModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New Addon</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                    id="closeServerModalBtn">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div id="add_addon_modal_body" class="modal-body">
                <form id="add_addon_form" action="" method="POST">
                    @csrf
                    <input type="hidden" name="project_id" value="{{$project_id}}">
                    <div class="form-group row">
                        <label for="title" class="col-sm-2 col-form-label">Addon Title</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control"  name="title" >
                          <div class="error" id="error_title"></div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="description" class="col-sm-2 col-form-label">Description</label>
                        <div class="col-sm-10">
                            <textarea type="text" class="form-control"  name="description" ></textarea>
                            <div class="error" id="error_description"></div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="price" class="col-sm-2 col-form-label">Price</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="price" >
                          <div class="error" id="error_price"></div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="status" class="col-sm-2 col-form-label">Approved Status</label>
                        <div class="col-sm-10">
                          <select class="form-control" name="status">
                              <option value="1" selected>No Action</option>
                              <option value="2">Approved</option>
                          </select>
                          <div class="error" id="error_status"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary">
                                Save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@can ('project_addons_create')
<a href="#" id="addServerButton" class="btn btn-success  " data-toggle="modal" data-target="#addAddonModal">Add New Addon</a>
@endcan

<!-- Add Servers Modal End -->

<!-- Update Servers Modal -->
<div id="updateAddonModalBlock">
</div>
<!-- Update Servers Modal End -->

<!-- Servers -->
<div class="row mt-4">

    @forelse ( $project_addons as $project_addon )
        <div class="col-sm-12 mt-2" >

            <ul class="list-unstyled list-group">
                    <li class="list-group-item">

                        <div class="row">
                            <div class="col-sm-3">
                                <b>Status:</b>
                                &nbsp;
                                @if($project_addon->status == 1)
                                <button class="btn btn-info btn-sm" onclick="changeStatusWithPermissionCheck({{$project_addon->id}})">
                                    <i class="fas fa-pencil-alt fa-sm"></i>
                                    No Action
                                </button>
                                @endif
                                @if($project_addon->status == 2)
                                <button class="btn btn-success btn-sm" onclick="changeStatusWithPermissionCheck({{$project_addon->id}})">
                                    <i class="fas fa-pencil-alt fa-sm"></i>
                                    Approved
                                </button>
                                @endif
                                @if($project_addon->status == 3)
                                <button class="btn btn-warning btn-sm" onclick="changeStatusWithPermissionCheck({{$project_addon->id}})">
                                    <i class="fas fa-pencil-alt fa-sm"></i>
                                    Rejected
                                </button>
                                @endif
                            </div>
                            <div class="col-sm-1">
                                @can ('project_servers_create')
                                <div class="col-sm-3">
                                    <div class="btn-group text-center ml-3">
                                        @can('project_addons_edit')
                                        <button type="button" id="updateServerBtn" class="btn btn-outline-success btn-sm"
                                            data-id="{{ $project_addon->id }}" onclick="renderUpdateAddonModal( this )"> <i
                                            class="fas fa-pencil-alt"></i> </button>
                                        @endcan

                                        @can('project_addons_delete')
                                        <button data-id="{{$project_addon->id}}" onclick="deleteProjectAddon(this)" class="btn btn-danger btn-xs  remove">
                                            <i class="fas fa-times"></i>
                                        </button>
                                        @endcan
                                    </div>
                                </div>
                            @endcan

                            </div>
                            @if ($project_addon->price)
                                <div class="col-sm-2">
                                    <b>Price:</b> {{$project_addon->price}}
                                </div>
                            @endif

                            <div class="col-sm-3 smaller-text">
                                <b>Created By:</b> {{ $project_addon->user->name }}
                            </div>
                            <div class="col-sm-3 smaller-text">
                                <b>Created At:</b> {{ $project_addon->created_at->format('d M Y  H:i:s') }}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-1">
                                <b>Title:</b>
                            </div>
                            <div class="col-3">
                                {{$project_addon->title}}
                            </div>
                            <div class="col-2">
                                <b> Description: </b>
                            </div>
                            <div class="col-6">
                                {{$project_addon->description ?? ''}}
                            </div>
                        </div>

                    </li>
            </ul>

        </div>
    @empty
        <center class="mx-auto">
            <h3>No Data Available!</h3>
        </center>
    @endforelse
</div>
<!-- Servers End -->
<script>
    $("#add_addon_form").on('submit', function(e) {
        e.preventDefault();
        $("#page-loader").show();
        let form_data = new FormData(this);
        $.ajax({
            url: "{{ route('project-addons-store') }}",
            type: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            success: function(data) {
                Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Added Successfully',
                showConfirmButton: false,
                timer: 1500
                })
                $('#addAddonModal').modal('hide');

                $("#page-loader").hide();
                renderProjectAddonsView();
                $('.modal-backdrop').remove();
                $('body').removeClass('modal-open');
                getCommentData();
            },
            error: function(data) {

                console.log('this is error');
                console.log(data);
                $("#page-loader").hide();

                $.each(data.responseJSON.errors, function(id, msg) {
                    $('#error_' + id).html(msg);
                })
            }
        });
    })
    function deleteProjectAddon(deleteProjectAddonBtn) {
        var projectAddonId = deleteProjectAddonBtn.getAttribute('data-id');

        swal({
            title: "Oops....",
            text: "Are You Sure You want to delete!",
            icon: "error",
            buttons: ['NO', 'YES'],
        }).then(function(isConfirm) {
            if (isConfirm) {
                console.log("is confirmed");
                $("#page-loader").show();
                $.ajax({
                    url: "{{ route('project-addons-delete') }}",
                    type: "post",
                    data: {
                        'id': projectAddonId
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Remove Successfully',
                            showConfirmButton: false,
                            timer: 1500
                            })
                            renderProjectAddonsView();
                        getCommentData();

                    },
                    complete: function() {
                        $("#page-loader").hide();
                    }
                });
            } else {
                console.log("not confirmed");
            }
        });

    }

    function renderUpdateAddonModal(updateBtn) {
        var addon_id = updateBtn.getAttribute("data-id");
        $.ajax({

            type: 'get',
            url: "{{ route('project-addons-edit') }}",
            data: {
                addon_id
            },
            xhr: function() {
                myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },

            beforeSend: function() {
                $("#page-loader").show();
            },

            complete: function() {
                $("#page-loader").hide();
            },

            success: function(data) {
                $("#updateAddonModalBlock").empty().html(data);
                $("#updateAddonModal").modal('show');
            }

        })

    }

    function changeStatusWithPermissionCheck(id){
        let havePermission = false;
        @can('project_addons_change_status')
        havePermission = true;
        @endcan
        if(havePermission){
           changeAddonStatus(id);
        }else{
            Swal.fire("You don't have permission to change the status of addon", '', 'error')
        }
    }

    function changeAddonStatus(id){
        Swal.fire({
        title: 'Choose the Action',
        showDenyButton: true,
        showCancelButton: true,
        confirmButtonText: 'Approve',
        confirmButtonColor: '#00bc8c',
        denyButtonColor: '#f39c12',
        denyButtonText: `Reject`,
        }).then((result) => {
        if (result.isConfirmed) {
            status = 2
            msg = "Approved"
        } else if (result.isDenied) {
            status = 3
            msg = "Rejected"
        }
        if(result.isConfirmed || result.isDenied){
        $("#page-loader").show();
        $.ajax({
            url: "{{ route('project-addons-change-status') }}",
            type: "post",
            data: {id, status},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                $("#page-loader").hide();
                Swal.fire(msg, '', 'success')
                renderProjectAddonsView();
                getCommentData();
            },
            error: function(error){
                $("#page-loader").hide();
                console.log(error)
                Swal.fire("Opps.. Something went wrong, please try again", '', 'error')
            },
            complete: function() {
            }
        });
        }


    })

    }

</script>
