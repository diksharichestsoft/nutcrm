<form action="" name="updateCompany" id="updateCompany">
          @csrf
        <input type="hidden" name="id" id="update_id" value="{{$getProjectData->id}}">
        <div class="value">
          <div class="form-group row">
            <label for="project_title" class="col-sm-2 col-form-label">Title</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="title"  name="project_title" value="{{$getProjectData->title}}" >
              <div class="error" id="error_project_title"></div>
              </div>
          </div>
          <div class="form-group row">
            <label for="company" class="col-sm-2 col-form-label">Company</label>
            <div class="col-sm-10">
              <select name="company" id="company" class="selectpicker" data-live-search="true" >
                <option value="">Select Company</option>
                @foreach($companies as $company)
                  <option value="{{ $company->id }}" {{ $getProjectData->company_id == $company->id ? 'selected' : '' }}>{{ $company->name }}</option>
                @endforeach

              </select>
              <div class="error" id="error_company"></div>
            </div>
          </div>
          <div class="form-group row">
            <label for="url" class="col-sm-2 col-form-label">Url*</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="url" name="url" value="{{$getProjectData->url??''}}" >
              <div class="error" id="error_url"></div>
            </div>
          </div>
          <div class="form-group row">
            <label for="price" class="col-sm-2 col-form-label">Project Price*</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="price" name="price" value="{{$getProjectData->budget??''}}"  >
              <div class="error" id="error_price"></div>
            </div>
          </div>
          <div class="form-group row">
            <label for="platform" class="col-sm-2 col-form-label">Platform</label>
            <div class="col-sm-10">
              <select name="platform" id="platform" class="selectpicker" data-live-search="true">
                <option value="">Select Platform</option>
                @foreach($platform as $platform)
                <option value="{{ $platform->id }}" {{ $getProjectData->platform == $platform->id ? 'selected' : '' }}>{{ $platform->name }}</option>
                @endforeach
              </select>
              <div class="error" id="error_platform"></div>
            </div>
          </div>
          <div class="form-group row">
            <label for="platform_id" class="col-sm-2 col-form-label">Upwork ID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="platform_id" name="platform_id" value="{{$getProjectData->platform_id??''}}" >
              <div class="error" id="error_platform_id"></div>
            </div>
          </div>

          <div class="form-group row">
            <label for="team_lead" class="col-sm-2 col-form-label">Department</label>
            <select name="department_id[]" id="department" class="selectpicker" data-live-search="true" multiple required>
              <option value="">Select Departement</option>
              @foreach($project_type as $department)
              <option value="{{$department->id}}" {{array_key_exists($department->id,$getProjectData->projectTypes->keyby('id')->toarray()) ? "selected" : ''}}>{{$department->name}}</option>
              @endforeach
            </select>
            @error('team_lead')
                <div class="error error-msg-red"></div>
            @enderror
        </div>
          <div class="form-group row">
            <label for="client_name" class="col-sm-2 col-form-label">Client Name</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="client_name" name="client_name"  value="{{$getProjectData->client_name??''}}" >
              <div class="error" id="error_client_name"></div>
            </div>
          </div>

          <div class="form-group row">
            <label for="description" class="col-sm-2 col-form-label">Client Shipping Address</label>
              <div class="col-sm-10">
                  <input type="hidden" name="client_shipping_address"  id="client_shipping_address">
                  <div id="clientShippingAddressUpdate" style="border: 1px solid #ffffff42;">{!!$getProjectData->client_shipping_address ?? '' !!}</div>
              </div>
          </div>


          {{-- <div class="form-group row">
            <label for="client_shipping_address" class="col-sm-2 col-form-label">Client Shipping Address</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="client_shipping_address" name="client_shipping_address"  value="{{$getProjectData->client_shipping_address??''}}" >
              <div class="error" id="error_client_shipping_address"></div>
            </div>
          </div> --}}
          <div class="form-group row">
            <label for="client_email" class="col-sm-2 col-form-label">Client Email</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="client_email" name="client_email"  value="{{$getProjectData->client_email??''}}" >
              <div class="error" id="error_client_email"></div>
            </div>
          </div>
          <div class="form-group row">
            <label for="client_phone" class="col-sm-2 col-form-label">Client Phone</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="client_phone" name="client_phone"  value="{{$getProjectData->client_phone??''}}" >
              <div class="error" id="error_client_phone"></div>
            </div>
          </div>
          <div class="form-group row">
            <label for="client_whatsapp_number" class="col-sm-2 col-form-label">Client Whatsapp Number</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="client_whatsapp_number" value="{{$getProjectData->client_whatsapp_number ?? ''}}" name="client_whatsapp_number" >
              <div class="error" id="error_client_whatsapp_number"></div>
            </div>
          </div>

          <div class="form-group row">
            <label for="client_skype" class="col-sm-2 col-form-label">Client Skype</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="client_skype" value="{{$getProjectData->client_skype ?? ''}}" name="client_skype" >
              <div class="error" id="error_client_skype"></div>
            </div>
          </div>

          <div class="form-group row">
            <label for="client_slack_link" class="col-sm-2 col-form-label">Client Slack link</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="client_slack_link" value="{{$getProjectData->client_slack_link ?? ''}}" name="client_slack_link" >
              <div class="error" id="error_client_slack_link"></div>
            </div>
          </div>

          <div class="form-group row">
            <label for="client_slack_username" class="col-sm-2 col-form-label">Client Slack Username</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="client_slack_username" value="{{$getProjectData->client_slack_username ?? ''}}" name="client_slack_username" >
              <div class="error" id="error_client_slack_username"></div>
            </div>
          </div>

          <div class="form-group row">
            <label for="client_slack_password" class="col-sm-2 col-form-label">Client Slack Password</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="client_slack_password" value="{{$getProjectData->client_slack_password ?? ''}}" name="client_slack_password" >
              <div class="error" id="error_client_slack_password"></div>
            </div>
          </div>

          <div class="form-group row">
            <label for="client_telegram_number" class="col-sm-2 col-form-label">Client Telegram Number</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="client_telegram_number" value="{{$getProjectData->client_telegram_number ?? ''}}" name="client_telegram_number" >
              <div class="error" id="error_client_telegram_number"></div>
            </div>
          </div>

          <div class="form-group row">
            <label for="job" class="col-sm-2 col-form-label">Job Description</label>
            <div class="col-sm-10">
              <textarea name="job" id="job_desc" cols="30" rows="10" class="form-control">{{$getProjectData->job_descriprion??''}}</textarea>
              <div class="error" id="error_job"></div>
            </div>
          </div>
          <div class="form-group row">
            <div class="offset-sm-2 col-sm-10">
              <button type="submit" class="btn btn-success">Update Deal</button>
              <button type="reset" class="btn btn-danger">Reset</button>
            </div>
          </div>
        </div>
      </form>
      <script>
          $(".selectpicker").selectpicker({
            "title": "Select Options"
        }).selectpicker("render");
        var clientShippingAddressUpdateEditor;

        InlineEditor
        .create( document.querySelector( '#clientShippingAddressUpdate' ) )
        .then( editor => {
            console.log( 'Editor was initialized', editor );
            clientShippingAddressUpdateEditor = editor;
        } )
        .catch( err => {
            console.error( err.stack );
        } );
        $(document).ready(function (){
        $(".ck-file-dialog-button").hide();
        $(".ck-dropdown__button").hide();
        })

    </script>
