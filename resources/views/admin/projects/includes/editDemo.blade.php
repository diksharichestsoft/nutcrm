<?php
use Illuminate\Support\Facades\Crypt;
?>
<!-- Update Demos Modal -->
<div class="modal fade" id="updateDemoModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Demo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeDemoModalBtn">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div id="update_demo_modal_body" class="modal-body">
                <form id="update_demo_form" action="" method="POST">
                    <div id="update_demo_item_container" class="mb-3"> 
		    @csrf

		    @foreach ( $project_demo->credentials as $project_demo_credential )
			       <div class="update_demo_items row"><div class="col-sm-12 my-2 update_demo_items_subsection">
			@if ( $loop->iteration > 1 )
	<button onclick="$(this).parent('.update_demo_items_subsection').parent('.update_demo_items').remove()" style="float:right" class="btn btn-danger remove_demo_item mt-4 mb-2"><i class="fa fa-minus" aria-hidden="true"></i></button>
			@endif
</div>

        	               <div class="form-group col-sm-3">
				<select class="form-select form-control" aria-label="Default select example" name="type" required>
					<option value="" selected disabled>Select a credential type</option>
				@foreach ( $project_credential_types as $type => $display_name)
				    	<option value="{{ $type }}" {{ $type == $project_demo_credential->type ? 'selected' : '' }}>{{ $display_name }}</option>
				@endforeach

                         	</select>
		            </div> 
                            <div class="form-group col-sm-3">
                                <input required class="form-control" name="url" placeholder="URL" value="{{ $project_demo_credential->url }}">
			    </div>

                            <div class="form-group col-sm-3">
                                <input required class="form-control" name="username" placeholder="Username" value="{{ $project_demo_credential->username }}">
                            </div>

                            <div class="form-group col-sm-3">
                                <input required class="form-control" type="password" name="password" placeholder="Password" value="{{Crypt::decryptString($project_demo_credential->password)}}">
			    </div>


			</div>
		@endforeach
                    </div>
                            <div class="row form-group mx-1">
                                <textarea  class="form-control" name="description" placeholder="Description" id="update_demos_description">{{ $project_demo->description }}</textarea>
			    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <a href="javascript:void(0);" class="btn btn-success mr-2" onclick="appendUpdateDemoForm(this)" id="update_more_demos_child">
                                <i class="fa fa-plus" aria-hidden="true"></i> Add Demo
                            </a>

                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<!-- Update Demos Modal End -->

<script>
function appendUpdateDemoForm( updateDemoFormBtn )
{


	var demoFormHtml = `<div class="update_demo_items row"><div class="col-sm-12 my-2 update_demo_items_subsection"><button onclick="$(this).parent(&quot;.update_demo_items_subsection&quot;).parent(&quot;.update_demo_items&quot;).remove()" style="float:right" class="btn btn-danger remove_demo_item mt-4 mb-2"><i class="fa fa-minus" aria-hidden="true"></i></button></div><div class="form-group col-sm-3"><select class="form-select form-control" aria-label="Default select example" name="type" required><option value="" selected disabled>Select a credential type</option>@foreach ($project_credential_types as $type=> $display_name)<option value="{{$type}}">{{$display_name}}</option>@endforeach</select></div><div class="form-group col-sm-3"><input class="form-control" name="url" placeholder="URL" required></div><div class="form-group col-sm-3"><input class="form-control" name="username" placeholder="Username" required></div><div class="form-group col-sm-3"><input class="form-control" name="password" placeholder="Password" required></div></div>`;
    $("#update_demo_item_container").append(demoFormHtml);

}

    $("#update_demo_form").on('submit', function (e){
	    e.preventDefault();
	var project_id = "{{ $project_id }}";
        let projectDemosFormData = [];
        let objectToStoreValues  = {};
        $(".update_demo_items").each(function (){
            objectToStoreValues={};
                let eachInput= $(this).find(".form-control");
                eachInput.each( (e)=>{
                    if(e==0){
                    	objectToStoreValues[`${eachInput[e].name}`]=eachInput[e].value;
	                projectDemosFormData.push(objectToStoreValues);
                    }else{
                        objectToStoreValues[`${eachInput[e].name}`]=eachInput[e].value;
                    }
                })
            });
	console.log( projectDemosFormData );
	var demos_description = $("#update_demos_description").val();

	$("#page-loader").show();

        $.ajax({
		type:'post',
		url:"{{route('project-demos-update')}}",
		dataType: "JSON",
		xhr: function() {
			myXhr = $.ajaxSettings.xhr();
			return myXhr;
		},
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},

			data: { 'demos': projectDemosFormData, 'project_id': project_id, 'description': demos_description,'demo_id': "{{ $project_demo->id }}" },
            	success:function(data){
			$('#updateDemoModal').modal('hide');

			$("#page-loader").hide();
			renderProjectDemosView();
			$('.modal-backdrop').remove();
			$('body').removeClass('modal-open');
			getCommentData();
            	},
		error:function(data){
			console.log ('this is error' );
			console.log( data );
			$("#page-loader").hide();
              $.each(data.responseJSON.errors, function(id,msg){
                $('#error_'+id).html(msg);

              })
            }
	});
        })
</script>
