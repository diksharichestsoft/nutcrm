<section class="content">
    <div class="container-fluid">
      <div class="row">
                <!-- /.col -->
        <div class="col-md-9">
          <div class="card">
            <div class="card-header p-2">
              <ul class="nav nav-pills">

                  <li class="nav-item"><a class="{{$branch_id == 1 ? 'active' :''}} nav-link dev-timeline" onclick="getCommentData(1, 1)" href="#devtimeline" data-toggle="tab">Timeline</a></li>
                  @can('projects_show_sales_tab')
                  <li class="nav-item"><a class="{{$branch_id == 0 ? 'active' :''}} nav-link sales-timeline" onclick="getCommentData(1, 0)" href="#seotimeline" data-toggle="tab">Sales Timeline</a></li>
                  @endcan
                  @can('projects_comment')
                  <li class="nav-item"><a data-id="{{$getProjectData->id}}" class="btn-success nav-link ml-2" data-toggle="modal" data-target="#addComment">Add Comment</a>
                @endcan


              </ul>
            </div><!-- /.card-header -->
            <div class="card-body">
              <div class="tab-content">
               <!-- /.tab-pane -->
                <div class=" active tab-pane" id="devtimeline">
                  <!-- The timeline -->
                  <div class="timeline timeline-inverse">

                    @foreach($devcomment as $key=>$comment)

                    <?php $ctrtime = date('d M Y', strtotime($comment->created_at));
                    if(isset($devcomment[($key-1)]->created_at)){
                      $pretime = date('d M Y', strtotime($devcomment[($key-1)]->created_at));
                      $ctrtime = ($ctrtime!=$pretime)?$ctrtime:'';
                    }
                   ?>
                    @if(!empty($ctrtime))
                    <div class="time-label">
                      <span class="bg-danger">
                        {{$ctrtime}}
                      </span>
                    </div>
                    @endif
                    <div>
                        <i class="fas {{($comment->type!=1)?'fa-user bg-info':'fa-comments bg-warning'}}"></i>
                      <div class="timeline-item">
                          <span class="time"><i class="far fa-clock"></i> {{date('h.i A', strtotime($comment->created_at))}}</span>
                        <h3 class="timeline-header border-0"><a href="#">{{$comment->name }}</a> {{($comment->type!=1)?$comment->title:' commented on Project'}}
                        </h3>
                        <div class="timeline-body">
                            @if($comment->projects_closed_reason_id != null)
                            Reason: {{$comment->reasons->name}}
                            @endif
                          {!!$comment->comment!!}
                        </div>
                      </div>
                    </div>
                    <!-- END timeline item -->
                    <!-- timeline item -->
                    <!-- <div>  -->
                    @endforeach

                    {{$devcomment->appends(["id"=>$getProjectData->id, "branch_id"=>$branch_id ?? 0])->links()}}



                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>

  <script>
$('ul.pagination').hide();
  $(function() {
      $('.timeline-inverse').jscroll({
          autoTrigger: true,
          padding: 0,
          nextSelector: '.pagination li.active + li a',
          contentSelector: 'div.timeline-inverse',
          callback: function() {
              $('ul.pagination').remove();
          }
      });
  });
  </script>
