<style>
    select#selectsatus {
      width: 100%;
  }
  </style>
   <div class="card">
    <div class="card-header ">
      <h3 class="card-title">Your Projects</h3>
      <h6 class="count text-right">Total Projects : {{$data->total()}}</h6>
    </div>
                <!-- /.card-header -->
               <div class="card-body">
                  <table class="table table-bordered">
                    @if(count($data)>0)
                    <thead>
                      <tr>
                        <th style="width: 10px">Action</th>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Client Name</th>
                        <th>Other Info</th>
                        <th>BDE/BDM</th>
                        <th>Project Type</th>
                        <th>Time</th>
                        @if(isset($active) && $active!=1)
                        @can('projects_change_status')

                        <th>Action</th>
                        @endcan
                        @endif
                      </tr>
                    </thead>
                    @endif
                    <tbody>
                          @forelse($data as $key=>$value)
                             <tr>
                                  <td>
                                    <div class="btn-group">
                                        @can('projects_accept')

                                        @if($value->deal_status==1)
                                         <button data-id="{{$value->id}}" onclick="accept(this)" class="btn btn-primary btn-xs remove">Accept</button>
                                        @endif
                                        @endcan
                                        @can('tasks_view')
                                            <a href="{{route('tasks',$value->id)}}" target="_blank"  class="btn btn-primary btn-xs">Tasks</a>
                                        @endcan
                                        @can('projects_view')
                                            <a href="{{route('projects-view',$value->id)}}" target="_blank" class="btn btn-outline-success btn-xs  view"><i class="fas fa-eye"></i></a>
                                        @endcan
                                        @can('projects_edit')
                                            <button data-id="{{$value->id}}" onclick="updateCompanyProject(this)" class="btn btn-outline-success btn-xs  update" data-toggle="modal" data-target="#createDeal" ><i class="fas fa-pencil-alt"></i></button>
                                        @endcan
                                        @can('projects_comment')
                                            <button data-id="{{$value->id}}" class="btn btn-outline-success btn-xs comment" data-toggle="modal" data-target="#addComment"><i class="far fa-comment"></i></i></button>
                                        @endcan
                                        @can('projects_delete')
                                            <button data-id="{{$value->id}}" onclick="removeCompanyProject(this)" class="btn btn-danger btn-xs  remove"><i class="fas fa-times"></i></button>
                                        @endcan

                                        @can('project_invoices_view')
                                        <a href="{{route('project-transaction', $value->id)}}" class="btn btn-success btn-xs" target="_blank">Payments</a>

                                        @endcan
                                    </div>
                                  </td>
                                  <td>{{$value->id??''}}</td>
                                  <td style="word-break: break-all;">{{$value->title??''}}</td>
                                  <td>{{$value->client_name??''}}</td>
                                  <td>
                                    <b>Platform: </b><p class="smaller-text" >{{$value->platform_name ?? ''}}</p>
                                    <b>Upwork Id:</b><p class="smaller-text" > {{$value->platform_id??''}}</p>
                                   <b> Company: </b><p class="smaller-text" >{{$value->companyData->name??''}}</p>
                                   <b> Status: </b><p class="smaller-text  {{dealsBadgeClass($value->deal_status)}}" >{{$value->project_status_name??''}}</p>
                                </td>
                                  <td>
                                    <b> Created By :</b> <p class="smaller-text" >{{$value->created_by_user_name ?? ''}}</p>
                                    @if($value->updated_by_user_name!=0)
                                    <b> Updated By :</b> <p class="smaller-text" >{{$value->updated_by_user_name ?? ''}}</p>
                                    @endif
                                </td>
                                  <td>
                                    @foreach ($value->projectTypes as $thisProjectType)
                                        {{$thisProjectType->name}}<br>
                                    @endforeach

                                  </td>
                                  <td>
                                    <b> Created At :</b> <p class="smaller-text" >{{($value->created_at)?? ''}}</p>
                                    <b> Updated At :</b> <p class="smaller-text" >{{($value->updated_at)}}</p>
                                 </td>                                  @if($value->deal_status!=1)
                                  @can('projects_change_status')

                                  <td class="d-flex">
                                      <select name="sel" id="selectsatus" class="form-control deals_status" data-status="{{$value->deal_status}}" data-id="{{ $value->id }}" >
                                        <option value="0">Select Status</option>
                                        @foreach($ProjectStatus as $status)
                                        <option value="{{$status->id}}"  {{($value->deal_status==$status->id)?"selected":""}}>{{$status->dealname}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                @endcan
                                @endif
                             </tr>
                          @empty
                            <center> <h3> No Data Available </h3> </center>
                          @endforelse
                    </tbody>
                  </table>
                </div>
                {{$data->links()}}
                {{--
                @if($data->previousPageUrl() != null)
                   <a href="{{$data->previousPageUrl()}}" class="pagination prev_btn pull-left"><i class="fa fa-chevron-left"></i> Previous</a>
                @endif

                @if($data->nextPageUrl() != null)
                    <a href="{{$data->nextPageUrl()}}" class="pagination prev_btn pull-right">Next <i class="fa fa-chevron-right"></i> </a>
                @endif
                --}}
                <!-- /.card-body -->
              </div>



  {{-- All in one modal --}}
  <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Project Status</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
             <div class="form-group">
                 <label for="">Comment</label>
             <textarea name="" id="inproc1" cols="30" rows="10" class="form-control"></textarea>
             </div>

          </div>
          <div class="modal-footer">
            <button type="button" id="save_deal1" class="btn btn-primary">Save Project Status</button>
          </div>
        </div>
      </div>
    </div>



  {{-- \ All in one modal --}}



  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Project Status</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <label for="">Comment</label>
         <textarea name="" id="inproc" cols="30" rows="10" class="form-control"></textarea>
        </div>
        <div class="modal-footer">
          <button type="button" id="save_deal" onclick="save_deal_data()" class="btn btn-primary">Save Project Status</button>
        </div>
      </div>
    </div>
  </div>


  <!-- Meeting -->

  <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Project Status</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
           <div class="form-group">
               <label for="">Comment</label>
           <textarea name="" id="inproc1" cols="30" rows="10" class="form-control"></textarea>
           </div>

        </div>
        <div class="modal-footer">
          <button type="button" id="save_deal1" class="btn btn-primary">Save Project Status</button>
        </div>
      </div>
    </div>
  </div>

  <!-- FRD -->

  <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Deal Status</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

           <div class="form-group">
               <label for="">Comment</label>
           <textarea name="" id="inproc2" cols="30" rows="10" class="form-control"></textarea>
           </div>

        </div>
        <div class="modal-footer">
          <button type="button" id="save_deal2" class="btn btn-primary">Save Deal Status</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Favorite -->

  <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Deal Status</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

           <div class="form-group">
               <label for="">Comment</label>
           <textarea name="" id="inproc3" cols="30" rows="10" class="form-control"></textarea>
           </div>

        </div>
        <div class="modal-footer">
          <button type="button" id="save_deal3" class="btn btn-primary">Save Deal Status</button>
        </div>
      </div>
    </div>
  </div>

  <!-- WON -->

  <div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Deal Status</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

           <div class="form-group">
               <label for="">Comment</label>
           <textarea name="" id="inproc4" cols="30" rows="10" class="form-control"></textarea>
           </div>

        </div>
        <div class="modal-footer">
          <button type="button" id="save_deal4" class="btn btn-primary">Save Deal Status</button>
        </div>
      </div>
    </div>
  </div>
  <!-- LOST -->

  <div class="modal fade" id="exampleModal5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Deal Status</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

           <div class="form-group">
               <label for="">Comment</label>
           <textarea name="" id="inproc5" cols="30" rows="10" class="form-control"></textarea>
           </div>

        </div>
        <div class="modal-footer">
          <button type="button" id="save_deal5" class="btn btn-primary">Save Project Status</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="exampleModal6" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Project Status</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

           <div class="form-group">
               <label for="">Comment</label>
           <textarea name="" id="inproc6" cols="30" rows="10" class="form-control"></textarea>
           </div>

        </div>
        <div class="modal-footer">
          <button type="button" id="save_deal6" class="btn btn-primary">Save Project Status</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="exampleModal7" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Project Status</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

           <div class="form-group">
               <label for="">Comment</label>
           <textarea name="" id="inproc7" cols="30" rows="10" class="form-control"></textarea>
           </div>

        </div>
        <div class="modal-footer">
          <button type="button" id="save_deal7" class="btn btn-primary">Save Project Status</button>
        </div>
      </div>
    </div>
  </div>
  <script>

