
<!-- Update Demos Modal -->
<div class="modal fade" id="updateSheetModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Sheet</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeDemoModalBtn">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div id="update_demo_modal_body" class="modal-body">
                <form id="update_sheet_form" action="" method="POST">
                    <div id="update_sheet_item_container" class="mb-3">
		    @csrf

		    @foreach ( $project_sheet->credentials as $project_sheet_credential )
			       <div class="update_sheet_items row"><div class="col-sm-12 my-2 update_sheet_items_subsection">
			@if ( $loop->iteration > 1 )
	<button onclick="$(this).parent('.update_sheet_items_subsection').parent('.update_sheet_items').remove()" style="float:right" class="btn btn-danger remove_demo_item mt-4 mb-2"><i class="fa fa-minus" aria-hidden="true"></i></button>
			@endif
</div>

        	               <div class="form-group col-sm-3">
                            <input type="text" required name="type" placeholder="title" value="{{$project_sheet_credential->type}}" class="form-control">

		            </div>
                            <div class="form-group col-sm-3">
                                <input required class="form-control" name="url" placeholder="URL" value="{{ $project_sheet_credential->url }}">
			    </div>

                            <div class="form-group col-sm-3">
                                <input required class="form-control" name="username" type="hidden" placeholder="Username" value="0">
                            </div>

                            <div class="form-group col-sm-3">
                                <input required class="form-control" name="password" type="hidden" placeholder="Password" value="0">
			    </div>


			</div>
		@endforeach
                    </div>
                            <div class="row form-group mx-1">
                                <textarea  class="form-control" name="description" placeholder="Description" id="update_sheets_description">{{ $project_sheet->description }}</textarea>
			    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <a href="javascript:void(0);" class="btn btn-success mr-2" onclick="appenUpdateSheetFrom(this)" id="update_more_demos_child">
                                <i class="fa fa-plus" aria-hidden="true"></i> Add Sheet
                            </a>

                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<!-- Update Demos Modal End -->

<script>
function appenUpdateSheetFrom( updateDemoFormBtn )
{


	var demoFormHtml = `
    <div class="update_sheet_items row">
        <div class="col-sm-12 my-2 update_sheet_items_subsection">
            <button onclick="$(this).parent(&quot;.update_sheet_items_subsection&quot;).parent(&quot;.demo_items&quot;).remove()" style="float:right" class="btn btn-danger remove_demo_item mt-4 mb-2"><i class="fa fa-minus" aria-hidden="true"></i></button>
        </div>
        <div class="form-group col-sm-3">
            <input class="form-control" name="title" placeholder="title" required>
        </div>
        <div class="form-group col-sm-3">
            <input class="form-control" name="url" placeholder="URL" required>
        </div>
        <div class="form-group col-sm-3">
            <input required class="form-control" type="hidden" name="username" value="0" placeholder="Username">
        </div>

        <div class="form-group col-sm-3">
            <input required class="form-control" type="hidden" name="password" value="0" placeholder="Password">
        </div>

    </div>`;
    $("#update_sheet_item_container").append(demoFormHtml);

}

    $("#update_sheet_form").on('submit', function (e){
	    e.preventDefault();
	var project_id = "{{ $project_id }}";
        let projectSheetsFormData = [];
        let objectToStoreValues  = {};
        $(".update_sheet_items").each(function (){
            objectToStoreValues={};
                let eachInput= $(this).find(".form-control");
                eachInput.each( (e)=>{
                    if(e==0){
                    	objectToStoreValues[`${eachInput[e].name}`]=eachInput[e].value;
	                projectSheetsFormData.push(objectToStoreValues);
                    }else{
                        objectToStoreValues[`${eachInput[e].name}`]=eachInput[e].value;
                    }
                })
            });
	console.log( projectSheetsFormData );
	var sheets_description = $("#update_sheets_description").val();

	$("#page-loader").show();

        $.ajax({
		type:'post',
		url:"{{route('project-sheets-update')}}",
		dataType: "JSON",
		xhr: function() {
			myXhr = $.ajaxSettings.xhr();
			return myXhr;
		},
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},

			data: { 'demos': projectSheetsFormData, 'project_id': project_id, 'description': sheets_description,'demo_id': "{{ $project_sheet->id }}" },
            	success:function(data){
			$('#updateSheetModal').modal('hide');

			$("#page-loader").hide();
            renderProjectSheetsView();
			$('.modal-backdrop').remove();
			$('body').removeClass('modal-open');
			getCommentData();
            	},
		error:function(data){
			console.log ('this is error' );
			console.log( data );
			$("#page-loader").hide();
              $.each(data.responseJSON.errors, function(id,msg){
                $('#error_'+id).html(msg);

              })
            }
	});
        })
</script>
