@can('projects_show_subscription_tab')

                    <div class="card card">
                        <div class="card-header">
                            <h3 class="card-title">Subscription</h3>

                        <label class="label-switch switch-success">
                            <input type="checkbox" id="toggle_check" data-id="{{$getProjectData->id}}" value="0"  class="switch-square switch-bootstrap status toggle_user_login" data-id="" name="" id="status" {{($getProjectData->subscription_status == 1) ? 'checked' : ''}}>
                            <span class="lable"></span></label>
                        </div>
                        <form method="POST" action="" id="subscription_submit_form" class="col-md-8">
                            @csrf
                            <div class="form-group">
                              <label for="subscriptionDate">Date</label>
                              <input type="hidden" class="form-control" name="subscriptionProjectId" id="subscriptionProjectId" value="{{$getProjectData->id}}">
                              <input type="date" value="{{$getProjectData->subscription_date}}" name="subscription_date" class="form-control" id="subscriptionDate" placeholder="Select a Data" required>
                            </div>
                            <div class="form-group">
                                <label for="subscriptionAmount">Amount</label>
                                <input type="number" value="{{$getProjectData->subscription_amount}}" name="subscription_amount" class="form-control" id="subscriptionAmount" placeholder="Amount" required>
                            </div>
                            <div class="form-group">
                                <label for="subscription_payment_type">Payment type</label>
                                <select class="w-100 form-select form-control" value="{{$getProjectData->subscription_payment_type}}" name="subscription_payment_type" id="paymentType" aria-label="Default payment type" required>
                                    <option value=" ">Select payment type</option>
                                    <option value="+2 week" {{($getProjectData->subscription_payment_type == '+2 week') ? "selected" : " "}}>Half month</option>
                                    <option value="+1 month" {{($getProjectData->subscription_payment_type == '+1 month') ? "selected" : " "}}>Monthly</option>
                                    <option value="+3 month" {{($getProjectData->subscription_payment_type == '+3 month') ? "selected" : " "}}>3 month</option>
                                    <option value="+6 month" {{($getProjectData->subscription_payment_type == '+6 month') ? "selected" : " "}}>6 month</option>
                                    <option value="+1 year" {{($getProjectData->subscription_payment_type == '+1 year') ? "selected" : " "}}>1 year</option>
                                  </select>
                            </div>
                            <div class="form-group">
                                <label for="subscriptionMethod">Subscription method</label>
                                <select class="w-100 form-select form-control" name="subscription_payment_method" id="subscriptionMethod" aria-label="Default method" required>
                                    <option selected value="">Select method</option>
                                    <option value="Paypal" {{($getProjectData->subscription_payment_method == "Paypal") ? "selected" : ""}}>Paypal</option>
                                    <option value="Payoneer" {{($getProjectData->subscription_payment_method == "Payoneer") ? "selected" : ""}}>Payoneer</option>
                                    <option value="Bank" {{($getProjectData->subscription_payment_method == "Bank") ? "selected" : ""}}>Bank</option>
                                    <option value="Others" {{($getProjectData->subscription_payment_method == "Others") ? "selected" : ""}}>Others</option>
                                  </select>
                            </div>
                            <div class="form-group">
                                <label for="subscription_details">Details</label>
                                <input type="text" class="form-control" value="{{$getProjectData->subscription_details}}" name="subscription_details" id="subscription_details" placeholder="Details" required>
                            </div>
                            <button type="submit" class="btn btn-success">Submit</button>
                            <a class="btn btn-info" href="{{route('view-project-subscription',$getProjectData->id)}}">Subscriptions</a>

                          </form>
                    </div>

                    @endcan
                    <script>
                    $(".toggle_user_login").on('click', function (e){
                        e.preventDefault();
                        var id = $(this).attr('data-id');
                        $.ajax({
                            type: "post",
                            url: "{{route('subscriptions-status')}}",
                            headers:
                            {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {id:id},
                            dataType: "JSON",
                            success: function (response) {
                                    document.querySelector(`#${e.target.id}`).checked= response.success;
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: 'Updated Successfully',
                                        showConfirmButton: false,
                                        timer: 1500
                                        })
                                },
                                error:function(data){
                                    $.each(data.responseJSON.errors, function(id,msg){
                                    $('#error_'+id).html(msg);
                                    });
                                }
                            });


                        });

                    </script>
