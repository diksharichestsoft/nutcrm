<!-- Update Servers Modal -->
<div class="modal fade" id="updateAddonModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Addon</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeServerModalBtn">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div id="update_addon_modal_body" class="modal-body">
                <form id="edit_addon_form" action="" method="POST">
                    @csrf
                    <input type="hidden" name="project_id" value="{{$project_id}}">
                    <input type="hidden" name="id" value="{{$thisAddon->id}}">
                    <div class="form-group row">
                        <label for="title" class="col-sm-2 col-form-label">Addon Title</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" value="{{$thisAddon->title ?? ""}}"  name="title" >
                          <div class="error" id="error_title"></div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="description" class="col-sm-2 col-form-label">Description</label>
                        <div class="col-sm-10">
                            <textarea type="text" class="form-control"  name="description" >{{$thisAddon->description ?? ""}}</textarea>
                            <div class="error" id="error_description"></div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="price" class="col-sm-2 col-form-label">Price</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" value="{{$thisAddon->price ?? ""}}" name="price" >
                          <div class="error" id="error_price"></div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="status" class="col-sm-2 col-form-label">Approved Status</label>
                        <div class="col-sm-10">
                          <select class="form-control" name="status">
                              <option value="1" {{$thisAddon->status == 1 ? 'selected' : ''}} >No Action</option>
                              <option value="2" {{$thisAddon->status == 2 ? 'selected' : ''}}>Approved</option>
                              <option value="3" {{$thisAddon->status == 3 ? 'selected' : ''}}>Reject</option>
                          </select>
                          <div class="error" id="error_status"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary">
                                Save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Update Servers Modal End -->

<script>

$("#edit_addon_form").on('submit', function(e) {
        e.preventDefault();
        $("#page-loader").show();
        let form_data = new FormData(this);
        $.ajax({
            url: "{{ route('project-addons-store') }}",
            type: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            success: function(data) {
                Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Updated Successfully',
                showConfirmButton: false,
                timer: 1500
                })
                $('#updateAddonModal').modal('hide');

                $("#page-loader").hide();
                renderProjectAddonsView();
                $('.modal-backdrop').remove();
                $('body').removeClass('modal-open');
                getCommentData();
            },
            error: function(data) {

                console.log('this is error');
                console.log(data);
                $("#page-loader").hide();

                $.each(data.responseJSON.errors, function(id, msg) {
                    $('#error_' + id).html(msg);
                })
            }
        });
    })
</script>
