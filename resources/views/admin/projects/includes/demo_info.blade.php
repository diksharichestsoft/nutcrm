<?php
use Illuminate\Support\Facades\Crypt;
?>
<!-- Add Demos Modal -->
<div class="modal fade" id="addDemoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Demo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                    id="closeDemoModalBtn">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div id="add_demo_modal_body" class="modal-body">
                <form id="add_demo_form" action="" method="POST">
                    <div id="demo_item_container" class="mb-3">
                        @csrf
                        <div class="demo_items row">

        	               <div class="form-group col-sm-3">
				<select class="form-select form-control" aria-label="Default select example" name="type" required>
					<option value="" selected disabled>Select a credential type</option>
				@foreach ($project_credential_types as $type => $display_name)
				    	<option value="{{ $type }}" >{{ $display_name }}</option>
				@endforeach

                         	</select>
		            </div>
                            <div class="form-group col-sm-3">
                                <input required class="form-control" name="url" placeholder="URL">
			    </div>

                            <div class="form-group col-sm-3">
                                <input required class="form-control" name="username" placeholder="Username">
                            </div>

                            <div class="form-group col-sm-3">
                                <input required class="form-control" type="password" name="password" placeholder="Password">
                            </div>


			</div>

                    </div>
                            <div class="row form-group mx-1">
                                <textarea  class="form-control" name="description" placeholder="Description" id="demos_description"></textarea>
			    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <a href="javascript:void(0);" class="btn btn-success mr-2" onclick="appendDemoForm(this)" id="add_more_demos_child">
                                <i class="fa fa-plus" aria-hidden="true"></i> Add Demo
                            </a>

			    {{-- <button type="button" class="btn btn-primary" onclick="$('#add_demo_form').submit()">
                                Save</button> --}}
                            <button type="submit" class="btn btn-primary">
                                Save</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

@can ('project_demos_create')
<a href="#" id="addDemoButton" class="btn btn-success  " onclick=" console.log('clicked jsn'); " data-toggle="modal"
    data-target="#addDemoModal">Add New Demo</a>
@endcan
<!-- Add Demos Modal End -->

<!-- Update Demos Modal -->
<div id="updateDemoModalBlock">
</div>
<!-- Update Demos Modal End -->

<!-- Demos -->
<div class="row mt-4">

@forelse ( $project_demos as $project_demo )
    <div class="col-sm-12 mt-2">

                        <ul class="list-unstyled list-group">
                            <li class="list-group-item">
                                <div class="row">
    <div class="col-sm-4">
        <b>Created By:</b> {{ $project_demo->user->name }}
    </div>
    <div class="col-sm-4">
        <b>Created At:</b>  {{ $project_demo->created_at->format("d M Y  H:i:s") }}
    </div>

@canany (['project_demos_edit', 'project_demos_delete'])
    <div class="col-sm-4">
	<b>Actions:</b><div class="btn-group text-center ml-3">

@can ('project_demos_edit')
	<button type="button" id="updateDemoBtn" class="btn btn-outline-success btn-sm" data-id="{{ $project_demo->id }}"  onclick="renderUpdateDemoModal( this )"> <i class="fas fa-pencil-alt"></i> </button>

@endcan

@can ('project_demos_delete')

<button type="button" id="deleteDemo" data-id="{{ $project_demo->id }}" class="btn btn-danger btn-sm" onclick="deleteProjectDemo( this )"> <i class="fas fa-times"></i> </button>
@endcan
                                                                                                        </div>
    </div>

</div>
@endcanany
</li>












@foreach ( $project_demo->credentials as $project_demo_credential )

<li class="list-group-item">

<div class="row">
    <div class="col-sm-4">
        <b>Credentials Type:</b> {{ getDisplayNameOfProjectCredentailsType( $project_demo_credential->type ) }}
    </div>
    <div class="col-sm-2">
        <b>Link: <a href="{{ str_starts_with( $project_demo_credential->url, 'http' ) ? $project_demo_credential->url : 'https://'.$project_demo_credential->url }}" class="text-primary" target="_blank">Open link</a></b>
    </div>
    <div class="col-sm-3">
        <b>Username:</b> {{ $project_demo_credential->username }}
    </div>
    <div class="col-sm-3">
        <b>Password:</b> {{Crypt::decryptString($project_demo_credential->password)}}
    </div>
</div>
</li>
@endforeach

@if ( $project_demo->description )
<li class="list-group-item">
<b>Description:</b> {{ $project_demo->description }}
</li>
@endif
</ul>

                    </div>
		    @empty
		    <center class="mx-auto"><h3>No demo Data Available!</h3></center>
@endforelse
</div>
<!-- Demos End -->
<script>
function appendDemoForm( addDemoFormBtn )
{

	/* var demoFormHtml = `<div class="demo_items row"> <div class="col-sm-12 my-2 demo_items_subsection"><button onclick="$(this).parent('.demo_items_subsection').parent('.demo_items').remove()" style="float: right" class="btn btn-danger remove_demo_item mt-4 mb-2"><i class="fa fa-minus" aria-hidden="true"></i></button></div><div class="form-group col-sm-3"> <select class="form-select form-control" aria-label="Default select example" name="type" required> <option value="" selected disabled>Select a credential type</option> @foreach ($project_credential_types as $type=> $display_name) <option value="{{$type}}" >{{$display_name}}</option> @endforeach </select> </div><div class="form-group col-sm-3"> <input class="form-control" name="url" placeholder="URL"> </div><div class="form-group col-sm-3"> <input class="form-control" name="username" placeholder="Username"> </div><div class="form-group col-sm-3"> <input class="form-control" name="password" placeholder="Password"> </div></div>`; */

	var demoFormHtml = `<div class="demo_items row"><div class="col-sm-12 my-2 demo_items_subsection"><button onclick="$(this).parent(&quot;.demo_items_subsection&quot;).parent(&quot;.demo_items&quot;).remove()" style="float:right" class="btn btn-danger remove_demo_item mt-4 mb-2"><i class="fa fa-minus" aria-hidden="true"></i></button></div><div class="form-group col-sm-3"><select class="form-select form-control" aria-label="Default select example" name="type" required><option value="" selected disabled>Select a credential type</option>@foreach ($project_credential_types as $type=> $display_name)<option value="{{$type}}">{{$display_name}}</option>@endforeach</select></div><div class="form-group col-sm-3"><input class="form-control" name="url" placeholder="URL" required></div><div class="form-group col-sm-3"><input class="form-control" name="username" placeholder="Username" required></div><div class="form-group col-sm-3"><input class="form-control" name="password" placeholder="Password" required></div></div>`;
    $("#demo_item_container").append(demoFormHtml);

}

    $("#add_demo_form").on('submit', function (e){
	    e.preventDefault();
	var project_id = "{{ $project_id }}";
        let projectDemosFormData = [];
        let objectToStoreValues  = {};
        $(".demo_items").each(function (){
            objectToStoreValues={};
                let eachInput= $(this).find(".form-control");
                eachInput.each( (e)=>{
                    if(e==0){
                    	objectToStoreValues[`${eachInput[e].name}`]=eachInput[e].value;
	                projectDemosFormData.push(objectToStoreValues);
                    }else{
                        objectToStoreValues[`${eachInput[e].name}`]=eachInput[e].value;
                    }
                })
            });
	console.log( projectDemosFormData );
	var demos_description = $("#demos_description").val();

	$("#page-loader").show();

        $.ajax({
		type:'post',
		url:"{{route('project-demos-store')}}",
		dataType: "JSON",
		xhr: function() {
			myXhr = $.ajaxSettings.xhr();
			return myXhr;
		},
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},

			data: { 'demos': projectDemosFormData, 'project_id': project_id, 'description': demos_description },
            	success:function(data){
			$('#addDemoModal').modal('hide');

			$("#page-loader").hide();
			renderProjectDemosView();
			$('.modal-backdrop').remove();
			$('body').removeClass('modal-open');
			getCommentData();
            	},
		error:function(data){
			console.log ('this is error' );
			console.log( data );
			$("#page-loader").hide();
              $.each(data.responseJSON.errors, function(id,msg){
                $('#error_'+id).html(msg);

              })
            }
	});
        })

function deleteProjectDemo( deleteProjectDemoBtn )
{
  var projectDemoId= deleteProjectDemoBtn.getAttribute('data-id');

  swal({
	title: "Oops....",
	text: "Are You Sure You want to delete!",
	icon: "error",
	buttons: [ 'NO', 'YES' ],
}).then( function(isConfirm) {
	if (isConfirm) {
		$("#page-loader").show();
		 $.ajax({
				url:"{{ route('project-demos-delete') }}",
				type:"post",
				data:{ 'demo_id': projectDemoId },
				headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
				success: function(data){
					console.log("demo deleted");
					renderProjectDemosView();
					$("#page-loader").hide();
					getCommentData();
				}
		});
	} else {
		console.log("not confirmed");
	}
});

}


function renderUpdateDemoModal( updateBtn )
{
        var demo_id = updateBtn.getAttribute("data-id");
        $.ajax({

                type: 'get',
                url:  "{{ route('project-demos-edit') }}",
                data: { demo_id },
                xhr: function() {
                        myXhr = $.ajaxSettings.xhr();
                        return myXhr;
                },

                beforeSend: function(){
                        $("#page-loader").show();
                },

                complete: function(){
                        $("#page-loader").hide();
                },

                success:function(data){
                        console.log("update modal fetched");
                        $("#updateDemoModalBlock").empty().html(data);
                        $("#updateDemoModal").modal('show');
                        window.jsnn = data;
                }

        })

}
</script>
