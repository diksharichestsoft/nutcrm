@extends('admin.layout.template')
@section('contents')
<div id="permissions_data">

    <h5>Task</h5>
    <div class="accordion" id="accordionExample">
        <div class="card">
          <div class="card-header" id="headingOne">
            <h2 class="mb-0">
              <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                Task
              </button>
            </h2>
          </div>
          <div id="collapse1" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body">
                <table class="table">
                    <tbody>
                        <tr>
                            <th scope="row">Task Title</th>
                            <td>
                                {{$thisTask->title}}
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">Summary</th>
                            <td>{{$thisTask->summary}}</td>
                        </tr>
                        <tr>
                            <th scope="row">Description</th>
                            <td>{!!$thisTask->description!!}</td>
                        </tr>
                        <tr>
                            <th scope="row">Project</th>
                            <td>
                                @if($thisTask->project != null)
                                <a href="{{route('projects-view',$thisTask->project->id)}}" target="_blank">{{$thisTask->project->title}}</a>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">Time</th>
                            <td>{{$thisTask->time}}</td>
                        </tr>
                        <tr>
                            <th scope="row">Current Status</th>
                            <td>
                            <div class="d-flex col-6">
                                <select name="sel" id="selectsatus" class="form-control deals_status" data-id="{{ $thisTask->id }}" >
                                <option value="0">Select Status</option>
                              @foreach($quickTaskStatus as $status)
                                <option value="{{$status->id}}"  {{($thisTask->task_status_id==$status->id)?"selected":""}}>{{$status->title}}</option>
                                @endforeach

                            </select>
                        </div>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">Priority Level</th>
                            <td>
                                @if ($thisTask->priority_level == 1)
                                    Highest
                                @endif
                                @if ($thisTask->priority_level == 2)
                                    Medium
                                @endif
                                @if ($thisTask->priority_level == 3)
                                    Low
                                @endif
                                @if ($thisTask->priority_level == 4)
                                    Lowest
                                @endif

                            </td>
                        </tr>
                        <tr>
                            <th scope="row">Assigned By</th>
                            <td>{{$thisTask->assignedBy->name}}</td>
                        </tr>
                        <tr>
                            <th scope="row">Start Date</th>
                            <td>{{$thisTask->start_date}}</td>
                        </tr>
                        <tr>
                            <th scope="row">End Date</th>
                            <td>{{$thisTask->end_date}}</td>
                        </tr>
                    </tbody>
                </table>
                        </div>
          </div>
        </div>
      </div>

<!-- Content Wrapper. Contains page content -->
<!-- <div class="content-wrapper"> -->
    <!-- Content Header (Page header) -->

    <!-- Main content -->
</div>
<div class="modal fade" id="changestatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel1">Assign Task</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="changeStatuSbody">
          <form action="" name="changestatusform" id="changestatusform">
              @csrf
              <input type="hidden" name='status' id="changeStatus-status" value="">
              <input type="hidden" name='deal_id' id="changeStatus-deal_id" value="{{$thisTask->id}}">
              <div class="value">
              <div class="form-group row">
                <label for="comment" class="col-sm-2 col-form-label">Comment</label>
                <div class="col-sm-10">
                  <textarea name="comment"  cols="30" rows="10" class="form-control"></textarea>
                  <div class="error" id="error_comment"></div>
                </div>
              </div>
              <div class="form-group row">
                <div class="offset-sm-2 col-sm-10">
                  <button type="submit" class="btn btn-success">Submit</button>
                </div>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
<script>
        $(document).on('change','#selectsatus',function(){
      var status =$(this).val();
      $('#changeStatus-status').val(status);
      $('#changestatusform textarea').val('');
      $('#changestatus').modal('show');
    });

      $(document).ready(function(){
      $(".services").eq(0).addClass("active");
    });
    $(document).on('click','.comment',function(){
      var id = $(this).attr('data-id');
      $('#deal_id').val(id);
      $('#comment_title').val('');
      $('#comment').val('');

    });
    $(document).on('submit','#changestatusform',function(e){
        e.preventDefault();
        var data = new FormData(this);
      $.ajax({
          type:'post',
          url:"{{route('manageTasks-change-status')}}",
          dataType: "JSON",
         xhr: function() {
               myXhr = $.ajaxSettings.xhr();
               return myXhr;
         },
         cache: false,
         contentType: false,
         processData: false,
          data:data,
          success:function(data){
              if(data.status == 0){
                  $('#changestatus').modal('hide');
                  Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: data.message,
                        showConfirmButton: false,
                        timer: 7000
                    })
                    return
              }else{
                  Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: "Status Changed successfully",
                        showConfirmButton: false,
                        timer: 2000
                    })
              }
              $('#changestatus').modal('hide');
              getCommentData(1,'');
          },
          error:function(data){
            $.each(data.responseJSON.errors, function(id,msg){
              $('#error_'+id).html(msg);
            })
          }
        });
  });
    </script>
@endsection
