<style>

    .announcement-banner {
        text-align: center;
        width: 100%;
        margin: 0 0 3rem;
        position: relative;

    }
    .popup {
        position: absolute;
        border: none;
        border-radius: 0px;
        width: 35px;
        height: 100%;
        font-size: 20px !important;
        z-index: 2;
        border-radius: 50px 45px 45px 50px;
    }
    .popup-excalimation{
        animation: leaves 1s ease-in-out infinite alternate;
    }
    .notify{
        position: absolute;
        border: none;
        border-radius: 0px;
        width: 35px;
        height: 100%;
        font-size: 20px !important;
        z-index: 2;
        border-radius: 50px 45px 45px 50px;
    }
    .marquee-container{
      width: 100%  !important;
    }

    .fa-question-circle{
        animation: leaves 1s ease-in-out infinite alternate;

    }
    @keyframes leaves {
         0% {
            transform: scale(1.0);
        }
        20% {
            transform: scale(1.2);
        }
        40% {
            transform: scale(1.0);
        }
        100% {
            transform: scale(1.0);
        }
    }
    .announcement-marquee {
        color: black;
        background-color: white;
        text-transform: uppercase;
        font-size: 12px;
        font-family: 'Anton', sans-serif;
        height: 30px;
        display: flex;
        align-items: center;
        border-radius: 50px;
    }

    .announcement-marquee span{
      color: #F04524;
    }
    </style>

    {{-- Start Birthday --}}
    <div>

    </div> {{--End birthday--}}

    {{-- Annoucement Modal --}}
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Announcements</h5>
                  <button class="btn btn-info" id="fa-question-circle-button" onclick="Swal.fire('The Excalimation mark will pop whenever there is new announcement within 24 hours','','info')">
                      <i class="fa fa-question-circle" aria-hidden="true"></i>
                  </button>
              </div>
              <div class="modal-body">
                  <div id="accordion">
                    @if(!empty($usersWithBirthdayToday))
                    @if(count($usersWithBirthdayToday) > 0)
                    <div class="card">
                        <div class="card-header" style="background-color:#3f6791">
                          <h4 class="card-title w-100">
                            <a class="d-block w-100 h4" data-toggle="collapse" style="color:white"}};filter: invert(100%);" href="#collapse00" aria-expanded="true">
                                <span class="h6" style="letter-spacing: 4px">🎉Happy Birthday 🎉</span>
                            </a>
                          </h4>
                        </div>
                        <div id="collapse00" class="collapse show" style="">
                          <div class="card-body">
                            @foreach ($usersWithBirthdayToday as $key=>$eachUserWithBday)
                            {{$eachUserWithBday->name}} {{($key == count($usersWithBirthdayToday) - 1) ? "" :", "}}
                            @endforeach
                        </div>

                    </div>
                    </div>
                    @endif
                    @endif
                    @if(!empty($getAnnouncements) AND count($getAnnouncements)>0)

                      @foreach ($getAnnouncements as $key=>$eachAnnouncement )

                      <div class="card">
                        <div class="card-header" style="background-color: {{$eachAnnouncement->color ?? "#3f6791"}}">
                          <h4 class="card-title w-100">
                            <a class="d-block w-100 h4" data-toggle="collapse" style="color: {{$eachAnnouncement->color ?? "#3f6791"}};filter: invert(100%);" href="#collapse{{$key}}" aria-expanded="true">
                              {{$eachAnnouncement->title}}
                            </a>
                          </h4>
                        </div>
                        <div id="collapse{{$key}}" class="collapse show" style="">
                          <div class="card-body">
                          {{$eachAnnouncement->description}}
                          </div>
                        </div>
                      </div>

                      @endforeach
                      @endif
                    </div>
              </div>
          </div>
        </div>
      </div> {{--End Announcement Modal--}}


        <section class="announcement-banner row">
          @if($currentAnnouncement > 0)
            <div style="width:max-content" id="treeLeaves">
                <button class="btn btn-danger btn-xs popup" data-toggle="modal" data-target=".bd-example-modal-lg"><b class="popup-excalimation">!</b></button>
            </div>
            @else
            <div style="width:max-content">
              <button class="btn btn-danger btn-xs notify" data-toggle="modal" data-target=".bd-example-modal-lg"><b>!</b></button>
          </div>
          @endif
            <div class="marquee-container" style="width: 94%">
                <marquee class="announcement-marquee" behavior="scroll" onmouseover="this.stop()" onmouseout="this.start();"  direction="left" scrollamount="10" loop="infinite">
                    @if(!empty($usersWithBirthdayToday))
                    @if(count($usersWithBirthdayToday) > 0)
                            <span class="h6" style="letter-spacing: 4px">🎉Happy Birthday 🎉</span>
                            @foreach ($usersWithBirthdayToday as $key=>$eachUserWithBday)
                            {{$eachUserWithBday->name}} {{($key == count($usersWithBirthdayToday) - 1) ? "" :", "}}
                            @endforeach
                    @endif
                    @endif
                    @if(!empty($getAnnouncements) AND count($getAnnouncements) > 0)
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    <span class="text-dark h6" style="letter-spacing: 4px"><i class="fas fa-bullhorn"></i>Announcements<i class="fas fa-bullhorn fa-flip-horizontal"></i></span>
                    @foreach ($getAnnouncements as $eachAnnouncement)
                        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                        <b>{{$eachAnnouncement->title}}</b> ~ {!!$eachAnnouncement->description!!}
                        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>   &nbsp;
                        &nbsp;
                        &nbsp;
                    @endforeach
                    @endif
                </marquee>
            </div>
        </section>



