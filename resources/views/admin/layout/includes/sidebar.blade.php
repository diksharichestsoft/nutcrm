  <!-- Sidebar Menu -->
  <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item menu-open">
            <a href="#" class="nav-link ">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">

              <li class="nav-item">
                <a href="{{url('/admin/home')}}" class="nav-link">
                  <i class="fa fa-home nav-icon"></i>
                  <p>Home</p>
                </a>
              </li>


              <li class="nav-item">
                <a href="{{url('/admin/profile')}}" class="nav-link">
                  <i class="fas fa-id-badge nav-icon"></i>
                  <p>Profile</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{route('tasks', 'all')}}" class="nav-link">
                <i class="fa fa-tasks nav-icon text-success"></i>
                <p>All Tasks</p>
            </a>
        </li>

            <li class="nav-item">
                <a href="{{route('todo')}}" class="nav-link">
                    <i class="fa fa-user-md nav-icon"></i>
                    <p>My Notes</p>
                </a>
            </li>

            @can('leaves_view')

            <li class="nav-item">
                <a href="{{route('leaves')}}" class="nav-link">
                    <i class="fa fa-edit nav-icon"></i>
                    <p>leaves</p>
                </a>
            </li>
            @endcan

            @can('tickets_view')

            <li class="nav-item">
                <a href="{{route('tickets')}}" class="nav-link">
                    <i class="fa fa-indent nav-icon"></i>
                    <p>Tickets</p>
                </a>
            </li>
            @endcan

            @can('invoices_view')
            <li class="nav-item">
                <a href="javascript:void(0)" class="nav-link">
                    <i class="nav-icon fas fa-users"></i>
                    <p>
                        Invoice Management
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{route('invoices', 'purchase')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Purchase Invoices</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('invoices', 'sales')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Sales Invoices</p>
                        </a>
                    </li>

                </ul>
            </li>
            @endcan

            @can('announcements_view')

            <li class="nav-item">
                <a href="{{route('announcements')}}" class="nav-link">
                    <i class="fas fa-bullhorn nav-icon"></i>
                    <p>Announcements</p>
                </a>
            </li>
            @endcan

            @can('deals_view')
                <li class="nav-item">
                    <a href="{{url('/admin/companydeal')}}" class="nav-link">
                        <i class="fa fa-handshake nav-icon"></i>
                        <p>Deals</p>
                    </a>
                </li>
            @endcan

            @can('projects_view')
            <li class="nav-item">
                <a href="{{route('projects')}}" class="nav-link">
                    <i class="fa fa-briefcase nav-icon"></i>
                    <p>Projects</p>
                </a>
            </li>
            @endcan

        
            <li class="nav-item">
                <a href="javascript:void(0)" class="nav-link">
                    <i class="nav-icon fas fa-users"></i>
                    <p>
                      IT Management
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{route('servers')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Server</p>
                        </a>
                    </li>
                    

                </ul>
            </li>
            
 

            @can('training_session_view')
            <li class="nav-item">
                <a href="{{route('training-sessions')}}" class="nav-link">
                    <i class="fa fa-briefcase nav-icon"></i>
                    <p>Training Session</p>
                </a>
            </li>
            @endcan

            @can('targets_view')
            <li class="nav-item">
                <a href="{{route('targets')}}" class="nav-link">
                    <i class="fa fa-bullseye nav-icon"></i>
                    <p>Targets</p>
                </a>
            </li>
            @endcan

            <li class="nav-item">
                <a href="javascript:void(0)" class="nav-link">
                    <i class="fa fa-bullseye nav-icon"></i>
                    <p>
                        Dynamic Form
                        <i class="right fas fa-angle-left"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('dynamic-form-index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Listing</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('dynamic-form')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Create</p>
                            </a>
                        </li>
                    </ul>
                </li>

            @can('performance_view')
            <li class="nav-item">
                <a href="{{route('performance')}}" class="nav-link">
                    <i class="fa fa-briefcase nav-icon"></i>
                    <p>Performance</p>
                </a>
            </li>
            @endcan

            @can('seo_tasks_view')

            <li class="nav-item">
                <a href="{{route('seo.index')}}" class="nav-link">
                    <i class="fa fa-briefcase nav-icon"></i>
                    <p>Seo Tasks</p>
                </a>
            </li>
            @endcan


            @canany(['direct_leads_development_view','direct_leads_digital_view'])
                <li class="nav-item">
                    <a href="{{route('directLeads')}}" class="nav-link">
                    <i class="fa fa-user nav-icon"></i>
                    <p>Direct Leads</p>
                </a>
                </li>
            @endcanany

            @can('direct_lead_local_view')

            <li class="nav-item">
                <a href="{{route('DirectLeadLocal')}}" class="nav-link">
                    <i class="fa fa-user nav-icon"></i>
                    <p>Direct Leads Local</p>
                </a>
            </li>

            @endcan

            <li class="nav-item">
                <a href="{{route('manageTasks')}}" class="nav-link">
                    <i class="fa fa-tasks nav-icon"></i>
                    <p>Quick Tasks</p>
                </a>
            </li>
            @can('master_password_view')

            <li class="nav-item">
                <a href="{{route('masterPswd')}}" class="nav-link">
                    <i class="fa fa-tasks nav-icon"></i>
                    <p>Master Passwords</p>
                </a>
            </li>
            @endcan
            @can('attendance_view')
            <li class="nav-item">
                <a href="{{url('/admin/attendance')}}" class="nav-link">
                    <i class="fa fa-calendar nav-icon"></i>
                    <p>Attendance</p>
                </a>
            </li>
            @endcan
            @can('subscriptions_view')

            <li class="nav-item">
                <a href="{{route('subscriptions')}}" class="nav-link">
                    <i class="fa fa-credit-card nav-icon"></i>
                    <p>Subscriptions</p>
                </a>
            </li>
            @endcan

            @can('portfolio_view')

            <li class="nav-item">
              <a href="{{route('portfolio')}}" class="nav-link">
                <i class="fa fa-laptop nav-icon"></i>
                <p>Portfolio</p>
            </a>
            </li>
            @endcan

            @can('announcements_view')

            <li class="nav-item">
                <a href="{{route('announcements')}}" class="nav-link">
                    <i class="fas fa-bullhorn nav-icon"></i>
                    <p>Announcements</p>
                </a>
            </li>
            @endcan

          @can('user_management')
          <li class="nav-item">
              <a href="javascript:void(0)" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                  User Management
                  <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{route('premissions')}}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Permissons</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('roles')}}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Roles</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('roles-permissions')}}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Role Permissons</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('manage-users')}}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Assign Roles</p>
                    </a>
                </li>
            </ul>
        </li>
        @endcan

        @can('notification_users_view')
        <li class="nav-item">
            <a href="{{route('notification-users')}}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Notificaton Users</p>
            </a>
        </li>
        @endcan

            @can('employees_view')

            <li class="nav-item">
                <a href="{{url('/admin/employees')}}" class="nav-link">
                    <i class="fa fa-user-circle nav-icon"></i>
                    <p>Employees</p>
                </a>
            </li>
	    @endcan

            <li class="nav-item">
		        <a href="{{url('/admin/dsr')}}" class="nav-link">
                    <i class="fa fa-address-card nav-icon"></i>
                    <p>Daily Status Report</p>
                </a>
            </li>

        @canany ( ['departments_view','companies_view','platforms_view'] )
            <!-- Tables Management -->
            <li class="nav-item">
                <a href="javascript:void(0)" class="nav-link">
                    <i class="nav-icon fas fa-users"></i>
                    <p>
                        Tables Management
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    @can ( 'departments_view')
                    <li class="nav-item">
                        <a href="{{route('departments')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Departments</p>
                        </a>
                    </li>
                    @endcan

                    @can ( 'companies_view' )
                        <li class="nav-item">
                            <a href="{{ route('companies') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Companies</p>
                            </a>
                        </li>
		            @endcan

		            @can ('platforms_view')
                        <li class="nav-item">
                            <a href="{{ route('platforms') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Platforms</p>
                            </a>
                        </li>
            		@endcan
                </ul>
            </li>
            @endcanany
            
<!-- Tables Management End -->
            {{-- @can('workload_view')

            <li class="nav-item">
                <a href="{{route('workload', 1)}}" class="nav-link">
                    <i class="fa fa-user-md nav-icon"></i>
                    <p>Work Load</p>
                </a>
            </li>
            @endcan --}}

            @can('candidate_view')

            <li class="nav-item">
                <a href="javascript:void(0)" class="nav-link">
                    <i class="nav-icon fas fa-users"></i>
                    <p>
                        Candidate Management
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{route('candidates')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Cadidates</p>
                        </a>
                    </li>

                </ul>
            </li>
            @endcan


            @can('workload_view')

            <li class="nav-item">
                <a href="javascript:void(0)" class="nav-link">
                    <i class="nav-icon fas fa-users"></i>
                    <p>
                        WorkLoad
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{route('workload', 1)}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>WorkLoad Dev</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('workload', 2)}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>WorkLoad SEO</p>
                        </a>
                    </li>

                </ul>
            </li>
            @endcan
            @can('dynamic_form_view')
            <li class="nav-item">
                <a href="{{route('dynamic-form')}}" class="nav-link">
                    <i class="fa fa-newspaper-o nav-icon"></i>
                    <p>Dynamic Forms Creation</p>
                </a>
            </li>
            @endcan

        <li class="nav-item">
            <a href="{{url('/admin/report')}}" class="nav-link">
                <i class="fa fa-exclamation-circle nav-icon"></i>
                <p>Reports/Bugs</p>
            </a>
        </li>
          <li class="nav-item">
            <a href="{{url('/admin/logout')}}" class="nav-link bg-danger">
              <i class="fas fa-sign-out-alt nav-icon"></i>
              <p>
               Logout
              </p>
            </a>
          </li>


        </ul>
      </nav>
      <!-- /.sidebar-menu -->
      <script>
     $(document).ready(function() {
    var fullpath = window.location.pathname;
    var last = "{{url('/')}}/admin/" + fullpath.split('/')[4];
    var currentLink = $('a[href="' + last + '"]'); //Selects the proper a tag
    currentLink.addClass("active");
    });
    </script>
