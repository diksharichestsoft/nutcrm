@extends('admin.layout.template')

@section('contents')
<?php
$i = 1;
?>
<h5>Daily Status Report of</h5>
<h5> {{$employeeName->name}}</h5>
@foreach ($thisDsr->childDsrs as $childDsr)


<div class="accordion" id="accordionExample">
    <div class="card">
      <div class="card-header" id="headingOne">
        <h2 class="mb-0">
          <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{$i}}" aria-expanded="true" aria-controls="collapse{{$i}}">
            Project/Task #{{$i}}
          </button>
        </h2>
      </div>
      <div id="collapse{{$i}}" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
        <div class="card-body">
            <table class="table">
                <tbody>
                    <tr>
                        <th scope="row">Project Name</th>

                        <td>
                            @if ($childDsr->dsrProject)
                            <a href="{{route('projects-view',$childDsr->dsrProject->first('id')->id)}}" target="_blank">
                            {{$childDsr->dsrProject->where('projects.id', $childDsr->project_id)->first('title')->title}}
                            </a>
                            @else
                            Others
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Work Hours</th>
                        <td>{{$childDsr->time}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Description</th>
                        <td>{!!$childDsr->description!!}</td>
                    </tr>
                </tbody>
            </table>
                    </div>
      </div>
    </div>
  </div>

<?php $i++ ?>
@endforeach

  @endsection
