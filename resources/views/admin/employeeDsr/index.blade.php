@section('contents')
<style>
    .daterangepicker .ranges li {font-size: 12px;padding: 8px 12px;cursor: pointer;background: #000000b0;}
  </style>
<div class="card" id="data">
    <div class="card-header p-2" id="card_head">
      <ul class="nav nav-pills">
        <div id="reportrange" style="background: transparent; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
            <i class="fa fa-calendar"></i>&nbsp;
            <span class="ab"></span> <i class="fa fa-caret-down"></i>
           </div>
           <input type="hidden" id="start_date_range" >
           <input type="hidden" id="end_date_range">
            <li class="nav-item search-right">
             <div class="search_bar">
                <div class="input-group" data-widget="sidebar-search">
                 <input class="form-control form-control-sidebar" id="search" type="search" placeholder="Search" aria-label="Search">
                </div>
             </div>
            </li>
      </ul>
    </div><!-- /.card-header -->

    <div class="card-body">

        <div id="pagination_employee">
            {{-- @include('admin.employeeDsr.includes.view') --}}
        </div>

      <!-- /.tab-content -->
    </div><!-- /.card-body -->
</div>
<script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/inline/ckeditor.js"></script>

<script>


function refreshDsrData (change_html_id){
    $('#page-loader').show();
    $.ajax({
    url:"{{route('employee-dsr-refresh')}}",
    success:function(data)
    {
        $(`#${change_html_id}`).empty().html(data);
      $('#page-loader').hide();

    },
    error:function(error){
      $('#page-loader').hide();

    }
  });
}
$(document).on('click', '.pagination a', function(event){
    event.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    fetch_data__data(page,'','');

  });

document.querySelector("#search").addEventListener("keyup",(e)=>{
        var search = $("#search").val();
      filterEmpData(search);
  });

  function filterEmpData(search){
    $('#page-loader').show();
    var data={};
  var make_url= "{{url('/')}}/admin/dsr/search?search="+search;
  $.ajax({
    url:make_url,
    data:data,
    success:function(data)
    {

        $('#pagination_employee').empty().html(data);
      $('#page-loader').hide();

    },
    error:function(error){
      $('#page-loader').hide();

    }
  });
  }

  $(function() {
  var start = moment().subtract(29, 'days');
  var end = moment();
  function cb(start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      $('#start_date_range').val(start.format('YYYY-MM-DD'));
      $('#end_date_range').val(end.format('YYYY-MM-DD'));
      fetch_data__data(1,'');
  }

  $('#reportrange').daterangepicker({
      startDate: start,
      endDate: end,
      ranges: {
         'Today': [moment(), moment()],
         'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
         'Last 7 Days': [moment().subtract(6, 'days'), moment()],
         'Last 30 Days': [moment().subtract(29, 'days'), moment()],
         'This Month': [moment().startOf('month'), moment().endOf('month')],
         'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
  }, cb);
  cb(start, end);

  });



  function fetch_data__data(page)
  {
    $('#page-loader').show();

    var start_date=document.querySelector('#start_date_range').value;
    var end_date=document.querySelector('#end_date_range').value;

    var search=document.querySelector("#search").value;
    var data={search:search,start_date:start_date,end_date:end_date};
    var make_url= "{{url('/')}}/admin/dsr/search?page="+page;
    $.ajax({
      url:make_url,
      data:data,
      success:function(data)
      {
          $('#pagination_employee').empty().html(data);
        $('#page-loader').hide();

      },
      error:function(error){
        $('#page-loader').hide();
      }
    });
    }


</script>
