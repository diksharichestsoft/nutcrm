
<!-- Main content -->
@if (Auth::user()->isSeoEmployee() >= 1)
@include('admin.employeeDsr.includes.create')
@else
@include('admin.employeeDsr.includes.create2')
@endif

    @if ($isDsrExist > 0)
    <span style="color: rgb(1, 185, 1)">
        You Have Submitted your Daily Status Report For today
    </span>
    @else
    @if (Auth::user()->haveTimerOnTask()[0])
    <span style="color: red">
        You have timer running on a task, please stop it before adding dsr
    </span>
    @else
    <a href="#" id="addDsrButton" class="btn btn-outline-success btn-lg" data-toggle="modal" data-target="#addDsrModal">Add Daily Status Report</a>
    @endif
    @endif

</div>
    <div class="col-sm-12">
        <h6 class="count text-right">Total Dsrs : {{$dsrs->total()}}</h6>
    </div>
</div>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card-body">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width: 10px">#</th>
                          @can('employee_development_dsr_review')
                            <th>Review</th>
                            @else
                            @can('employee_digital_marketing_dsr_review')
                            <th>Review</th>
                            @endcan
                          @endcan

                          @can('employee_dsr_view')
                          <th>Action</th>
                          @endcan
                          <th>Employee Name</th>
                          <th>Projects</th>
                          <th>Total Items</th>
                          <th>Total Hours</th>
                          <th>Created At</th>
                        </tr>
                      </thead>
                      <tbody>
                          @forelse( $dsrs as $key=>$dsr )
                          <tr>
                            <td>{{$dsr->id}}</td>
                            @can('employee_digital_marketing_dsr_review')
                            <td>
                                @if (($dsr->status == 1))
                                Approved By {{$dsr->approveByUser->where('id', $dsr->approve_by)->first('name')->name}}
                                @else
                                <div class="approve_by_button">
                                    <button id="toggle_check_{{$key}}" onclick="approveThisDsr(this)"  class="btn btn-warning text-white toggle_dsr_review" data-id="{{$dsr->id}}">Approve</button>
                                </div>
                                @endif
                            </td>
                            @else
                            @can('employee_development_dsr_review')
                            <td>
                                @if (($dsr->status == 1))
                                Approved By {{$dsr->approveByUser->where('id', $dsr->approve_by)->first('name')->name}}
                                @else
                                <div class="approve_by_button">
                                    <button id="toggle_check_{{$key}}" onclick="approveThisDsr(this)"  class="btn btn-warning text-white toggle_dsr_review" data-id="{{$dsr->id}}">Approve</button>
                                </div>
                                @endif
                            </td>
                            @endcan

                            @endcan
                            @can('employee_dsr_view')
                            <td>
                                <a href="{{route('employee-dsr-view',$dsr->id)}}" target="_blank" class="btn btn-outline-success btn-xs  view"><i class="fas fa-eye"></i></a>
                            </td>
                            @endcan
                            <td>
                                {{$dsr->employee->whereId($dsr->user_id)->first('name')->name}}
                            </td>
                            <td>
                                @foreach ($dsr->childDsrs as $child_dsr)
                                @if ($child_dsr->dsrProject)
                                <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                <a href="{{route('projects-view',$child_dsr->dsrProject->first('id')->id)}}" target="_blank">
                                {{($child_dsr->dsrProject->where('id', $child_dsr->project_id)->first('title')->title)}} <br>
                                </a>
                                @endif
                                @endforeach
                            </td>
                            <td>
                                {{$dsr->childDsrs()->count()}}
                            </td>
                            <td>
                                @php
                                    $time = '00:00';

                                    foreach ($dsr->childDsrs as $key => $child_dsr) {

                                        $temp = explode(":",$child_dsr->time);
                                        if(empty($temp[0])){$temp[0] = "00";}
                                        $time = date("H:i", strtotime("+".$temp[0]."hour +".$temp[1]."minutes", strtotime($time)));
                                    }
                                    echo "<script>console.log('One ender')</script>";
                                @endphp
                                {{$time}}
                            </td>
                            <td>
                                {{$dsr->created_at}}
                            </td>
                        </tr>

                        @empty
                        <tr>
                            <td colspan="9">
                                <h4  align="center">No Data Found</h4>
                            </td>
                        </tr>
                        @endforelse
                      </tbody>
                    </table>
                </div>
                <!--  -->
            </div>
        </div>
        {{$dsrs->links()}}
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
    <script>
$("#addDsrButton").on('click', function (){
    $("#pagination_employee").html($("#add_dsr_modal_body").html())
    initializeCreateForm()
$("#add_dsr_form").on('submit', function (e){

    e.preventDefault();
    $('#page-loader').show();
    for (let index = 1; index <= $('.dsr_items').length; index++) {
        $(`#description${index}`).val($(`#ckeditor${index}`).html());
    }
    let dsrFormData=[];
    let objectToStoreValues={};
    $(".dsr_items").each(function (){
        objectToStoreValues={};
            let eachInput=$(this).find(".form-control");
            eachInput.each((e)=>{
                if(e==0){
                objectToStoreValues[`${eachInput[e].name}`]=eachInput[e].value;
                dsrFormData.push(objectToStoreValues);
                }else{
                    objectToStoreValues[`${eachInput[e].name}`]=eachInput[e].value;
                }
            })
        });

    if(dsrFormData.length < 1){
        Swal.fire({
                icon: 'error',
                title: 'Minimum One Dsr item is required',
                showConfirmButton: true,
                })
    }

    $.ajax({
        type:'post',
        url:"{{route('employee-dsr-store')}}",
        dataType: "JSON",
       xhr: function() {
             myXhr = $.ajaxSettings.xhr();
             return myXhr;
       },
       headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        data:{data: dsrFormData},
        success:function(data){
            $('#page-loader').hide();
            if(data.status){
            $('#addDsrModal').modal('hide');
            refreshDsrData("pagination_employee");

            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Added Successfully',
                showConfirmButton: false,
                timer: 1500
                })
            }else{
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: data.message,
                    showConfirmButton: true,
                    // timer: 1500
                    })
            }
        },
        error:function(data){
            $('#page-loader').hide();
          $.each(data.responseJSON.errors, function(id,msg){
            $('#error_'+id).html(msg);

          })
        }
      });
    })
})



            function approveThisDsr(e){
            var id = $(e).attr('data-id');
            $.ajax({
                type: "post",
                url: "{{route('employee-dsr-review')}}",
                headers:
                {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {id:id},
                dataType: "JSON",
                success: function (response) {
                    $(e).hide();
                    $(e).parent(".approve_by_button").empty().html('<button  class="btn btn-success text-white">Approved</button>')
                }
            });
            }
    </script>

<script>
    $(document).on('click', '#post_pagination .pagination a', function(event){
 event.preventDefault();
 var page = $(this).attr('href').split('page=')[1];
 var filter=document.querySelector("#tabs_filter").value;
 ajax('get',`{{Route("employee-pagination")}}`,data={page:page},'#pagination_employee');
});


</script>
