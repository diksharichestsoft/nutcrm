<!-- Button trigger modal -->
  <!-- Modal -->

  <?php
use App\Models\Project;

  ?>
  <script>

    // function leadingZeros(input) {
    //     let timeVal2=0;
    //     if(!isNaN(input.value) && input.value.length === 1) {
    //         input.value = '0' + input.value;

    //         if($(input).data('type') == "hours"){

    //         timeVal2= moment(timeVal, 'HH:mm:ss').add(input.value, 'H').format("HH:mm:ss");
    //         }else{
    //             timeVal2= moment(timeVal, 'HH:mm:ss').add(input.value, 'm').format("HH:mm:ss");
    //         }
    //         // totalTime = parseInt(timeVal));
    //         console.log(timeVal2);
    //         $("#totalTime").empty().append(`Total ${timeVal2}`);
    //     }
    //     if($(input).val() == "" ||$(input).val() == null  ){
    //         $(input).val("00")
    //     }
    // }
    const showTotalTimeValue =( $("#totalTime").html()).split(":")
    const initialHours = Number(showTotalTimeValue[0])
    const initialMinutes = Number(showTotalTimeValue[1])
    const initialSeconds = Number(showTotalTimeValue[2])
    function leadingZeros(input) {

        // Getting the pervious value which was on page load first time
        let hours = initialHours;
        let secs = initialSeconds;
        let mins = initialMinutes;

        //iterating throught each hours input and adding to total hours
        $(".hours-input").each((index,eachInput)=>{
            hours+= Number($(eachInput).val())
        })

        //iterating throught each minuts input and adding to total minuts
        $(".minutes-input").each((index,eachInput)=>{
            mins+= Number($(eachInput).val())
        })

        // if minute are more than 60, add  to hours how many times its greater than 60
        hours += (mins > 60) ? ~~(mins/60) : 0
        mins = mins % 60

        hours = (hours < 10) ? "0"+hours : hours
        mins = (mins < 10) ? "0"+mins : mins
        secs = (secs < 10) ? "0"+secs : secs

        $("#totalTime").html(`${hours}:${mins}:${secs}`)
      }

  </script>
  <div class="modal fade" id="addDsrModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add DSR</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="add_dsr_modal_body" class="modal-body">
            <form id="add_dsr_form" action="" method="POST">

                {{-- Main container for dsr items --}}
            <div id="dsr_item_container">
                @csrf
                @php
                    $totalTimeOfToday = 0;
                @endphp
                @foreach ($taskTimerData as $key=>$eachTimer) {{--Looping through all project timers--}}

                @php
                   $taskIds =  $eachTimer->getProject->getTodayActiveTask()->get();
                   $totalTimeOfToday += $eachTimer->today_total_time;
                @endphp
                <div class="dsr_items"> {{--Each dsr item container inside foreach loop--}}
                    <div class="">
                        <b>Project :</b> {{$eachTimer->getProject->title}}
                        <br>
                        <b>Total Time:</b> {{gmdate("H:i:s",$eachTimer->today_total_time)}}

                    </div>
                    <br>
                    <div class="form-group d-none">
                        <label for="project_id">Select a Project</label>
                        <select class="form-select form-control" aria-label="Default select example" name="project_id" required>
                    <option value="{{$eachTimer->getProject->id}}" selected></option>
                        </select>
                    </div>
                    <div class="form-group d-none">
                        <input type="hidden" class="form-control" name="time_hours" value="{{gmdate("H", $eachTimer->today_total_time)}}">
                        <input type="hidden" class="form-control" name="time_min" value="{{gmdate("i", $eachTimer->today_total_time)}}" >
                        <input type="hidden" class="form-control" name="user_id" value="{{Auth::user()->id}}">
                    </div>
                    <br>
                    <div class="form-group row">
                        <label for="description" class="col-sm-2 col-form-label">Description</label>
                          <div class="col-sm-10">
                              <input type="hidden" id="description{{$key+1}}" class="form-control" name="description">
                              <div id="ckeditor{{$key+1}}" style="border: 1px solid #ffffff42;">

                                @foreach ($taskIds as $task) {{--Looping through all project timers--}}
                                @php
                                    $total_count_per_task = 0;
                                @endphp
                                <a href="{{route('task-detial-view', $task->task_id ?? 0)}}" >{{route('task-detial-view', $task->task_id ?? 0)}} </a>
                                @php
                                    for($i = 0; $i < count($taskDsrTimerData); $i++)
                                    if($taskDsrTimerData[$i]->task_id == $task->task_id){
                                        $total_count_per_task += (int)($taskDsrTimerData[$i]->total_time);
                                    }
                                @endphp
                                {{gmdate("H:i:s",$total_count_per_task)}}
                                <br>
                            @endforeach
                                </div>
                          </div>
                      </div>
                </div>
                @endforeach
            </div>
            {{-- @if(!auth()->user()->can('cannot_add_dsr_item')) --}}
            <a href="javascript:void(0);" class="btn btn-success" id="add_more_dsr_child"><i class="fa fa-plus" aria-hidden="true"></i> Add Items</a>
            {{-- @endif --}}
            <br>
            {{-- <button type="submit" class="btn btn-primary">Save</button> --}}
            <br>
            <a class="btn btn-warning" href="#">Total <span id = "totalTime">{{gmdate("H:i:s",$totalTimeOfToday)}}</span></a>
            <button type="button" class="btn btn-primary" onclick="$('#add_dsr_form').submit()"> Save</button>
            <br>

            @if(auth()->user()->can('cannot_add_dsr_item'))
                <p><b>Note:</b> Add DSR Items is not longer available, Your DSR time will be calculated on the basis of your task timer </p>
            @endif
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="$('#add_dsr_form').submit()"> Save</button>
        </div>
      </div>
    </div>
  </div>


  @if(!auth()->user()->can('can_add_dsr_item'))
  <script>
    $("#add_more_dsr_child").hide()
</script>
@endif

<script>
function initializeCreateForm(){
    // This function will add ck editor for the form when loaded first time
    var dsr_item_counter = 0;
    @foreach ($taskTimerData as $key=>$eachDataForCounter)
    ckEditorInit('ckeditor'+{{$key+1}})
     dsr_item_counter++;
    @endforeach


// Function for appending the items in dsr form
    $("#add_more_dsr_child").on("click", function(){
        dsr_item_counter = dsr_item_counter + 1;
        $("#dsr_item_container").append(`
        <div class="dsr_items">
        <br>
        <hr class="rounded" style="  border-top: 8px solid">
                <a href="#" style="float: right" class="btn btn-danger remove_dsr_item"><i class="fa fa-minus" aria-hidden="true"></i> Remove Item</a>
                <br>
                    <div class="form-group">
                            <label for="project_id">Select a Project</label>
                            <select class="form-select form-control" aria-label="Default select example" name="project_id">
                        @foreach ($projects as $project)
                        <option value="{{$project->id}}">{{$project->title}}</option>
                        @endforeach
                        <option value="133" selected>Others(Development)</option>
                        <option value="134">Others(SEO)</option>
                        </select>
                        </div>
                        <div class="form-group">
                            <label for="project_id">Select Time (Hours : minutes)</label>
                            <br>
                            <input type="number" max="59" class="form-control hours-input" data-type="hours" id="time" onchange="leadingZeros(this)" name="time_hours" style="width: 150px; display: initial" min="0" value="00">
                            <input type="number" max="59" class="form-control minutes-input" style="width: 150px; display: initial" id="time"  onchange="leadingZeros(this)" data-type="minutes" name="time_min" min="0" value="00">
                            <input type="hidden" class="form-control" name="user_id" value="{{Auth::user()->id}}">
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-sm-2 col-form-label">Description</label>
                            <div class="col-sm-10">
                                <input type="hidden" class="form-control" id="description${dsr_item_counter}" name="description">
                                <div id="ckeditor${dsr_item_counter}"  style="border: 1px solid #ffffff42;"></div>
                            </div>
                        </div>
                    </div>
        `);
        // Initializing ck editor in all itmes added
        ckEditorInit('ckeditor'+dsr_item_counter);

        //Remove itmes function on each item added
        $(".remove_dsr_item").on("click", function(e){
            e.preventDefault();
            $(this).closest('div').remove();
        })
    })

// Function for adding zero in html input if its less than 10


// This function initialize ck editor on given div, with its id
function ckEditorInit(id) {
    var myEditor;
    InlineEditor
    .create( document.querySelector( `#${id}` ) )
    .then( editor => {
        console.log( 'Editor was initialized', editor );
        myEditor = editor;
    } )
    .catch( err => {
        console.error( err.stack );
    } );
    $(document).ready(function (){
    // Hiding default extrack file and dropdown buttons
    $(".ck-file-dialog-button").hide();
    $(".ck-dropdown__button").hide();
    })

 }
}
</script>

