<!-- Button trigger modal -->
  <!-- Modal -->
  <script>
      function leadingZeros(input) {
    if(!isNaN(input.value) && input.value.length === 1) {
      input.value = '0' + input.value;
    }
    if($(input).val() == "" ||$(input).val() == null  ){
        $(input).val("00")
    }
  }

  </script>
  <div class="modal fade" id="addDsrModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add DSR</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="add_dsr_modal_body" class="modal-body">
            <form id="add_dsr_form" action="" method="POST">
            <div id="dsr_item_container">
                @csrf
                <div class="dsr_items">
                 <div class="form-group">
                        <label for="project_id">Select a Project</label>
                        <select class="form-select form-control" aria-label="Default select example" name="project_id" required>
                    @foreach ($projects as $project)
                    <option value="{{$project->id}}">{{$project->title}}</option>
                    @endforeach
                    <option value="133" selected>Others(Development)</option>
                    <option value="134">Others(SEO)</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="project_id">Select Time (Hours : minutes)</label>
                        <br>
                        <input type="number" class="form-control" value="00" id="time" name="time_hours" onfocusout="leadingZeros(this)" style="width: 150px; display: initial"  onchange="leadingZeros(this)" min="0">
                        <input type="number" class="form-control" style="width: 150px; display: initial" min="0"  onchange="leadingZeros(this)" onfocusout="leadingZeros(this)" id="time" name="time_min" value="00">
                        <input type="hidden" class="form-control" name="user_id" value="{{Auth::user()->id}}">
                    </div>
                    <div class="form-group row">
                        <label for="description" class="col-sm-2 col-form-label">Description</label>
                          <div class="col-sm-10">
                              <input type="hidden" id="description1" class="form-control" name="description">
                              <div id="ckeditor1" style="border: 1px solid #ffffff42;"></div>
                          </div>
                      </div>

                </div>
            </div>
            <a href="javascript:void(0);" class="btn btn-success" id="add_more_dsr_child"><i class="fa fa-plus" aria-hidden="true"></i> Add Items</a>
            <br>
                {{-- <button type="submit" class="btn btn-primary">Save</button> --}}
                <button type="button" class="btn btn-primary" onclick="$('#add_dsr_form').submit()"> Save</button>

            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="$('#add_dsr_form').submit()"> Save</button>
        </div>
      </div>
    </div>
  </div>

  <script>

</script>


<script>


function initializeCreateForm(){
    // This function will add ck editor for the form when loaded first time
    ckEditorInit('ckeditor1')
    var dsr_item_counter = 1;

// Function for appending the items in dsr form
    $("#add_more_dsr_child").on("click", function(){
        dsr_item_counter = dsr_item_counter + 1;
        $("#dsr_item_container").append(`
        <div class="dsr_items">
        <br>
        <hr class="rounded" style="  border-top: 8px solid">
                <a href="#" style="float: right" class="btn btn-danger remove_dsr_item"><i class="fa fa-minus" aria-hidden="true"></i> Remove Item</a>
                <br>
                    <div class="form-group">
                            <label for="project_id">Select a Project</label>
                            <select class="form-select form-control" aria-label="Default select example" name="project_id">
                        @foreach ($projects as $project)
                        <option value="{{$project->id}}">{{$project->title}}</option>
                        @endforeach
                        <option value="133" selected>Others(Development)</option>
                        <option value="134">Others(SEO)</option>
                        </select>
                        </div>
                        <div class="form-group">
                            <label for="project_id">Select Time (Hours : minutes)</label>
                            <br>
                            <input type="number" class="form-control" id="time" onchange="leadingZeros(this)" name="time_hours" style="width: 150px; display: initial" min="0" value="00"><input type="number" class="form-control" style="width: 150px; display: initial" id="time"  onchange="leadingZeros(this)" name="time_min" min="0" value="00">
                            <input type="hidden" class="form-control" name="user_id" value="{{Auth::user()->id}}">
                        </div>
                        <div class="form-group row">
                            <label for="description" class="col-sm-2 col-form-label">Description</label>
                            <div class="col-sm-10">
                                <input type="hidden" class="form-control" id="description${dsr_item_counter}" name="description">
                                <div id="ckeditor${dsr_item_counter}"  style="border: 1px solid #ffffff42;"></div>
                            </div>
                        </div>
                    </div>
        `);
        // Initializing ck editor in all itmes added
        ckEditorInit('ckeditor'+dsr_item_counter);

        //Remove itmes function on each item added
        $(".remove_dsr_item").on("click", function(e){
            e.preventDefault();
            $(this).closest('div').remove();
        })
    })

// Function for adding zero in html input if its less than 10


// This function initialize ck editor on given div, with its id
function ckEditorInit(id) {
    var myEditor;
    InlineEditor
    .create( document.querySelector( `#${id}` ) )
    .then( editor => {
        console.log( 'Editor was initialized', editor );
        myEditor = editor;
    } )
    .catch( err => {
        console.error( err.stack );
    } );
    $(document).ready(function (){
    // Hiding default extrack file and dropdown buttons
    $(".ck-file-dialog-button").hide();
    $(".ck-dropdown__button").hide();
    })

 }
}
</script>

