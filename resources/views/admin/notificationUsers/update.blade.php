@extends('admin.layout.template')
@section('contents')
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!--  -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Assign Notification</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form method="post" action="{{route('notification-users-update')}}" >
                        @csrf
                        <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Select Roles</label>
                            <select name="notification_id" id="notification_id" class="form-control">
                              <option value="{{$thisNotification->id}}">{{$thisNotification->title ?? ''}}</option>
                            </select>
                            @error('notification_id')
                                <div class="error error-msg-red">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Select Users</label>
                            <!-- <input type="text" class="form-control" name="role" id="role" placeholder="Enter Role Name"> -->
                            <select name="users[]" id="permissions" class="form-control select2" multiple>
                              <option value="">Select Users</option>
                              @foreach($allUsers as $eahcUser)
                              <option value="{{$eahcUser->id}}" {{in_array ( $eahcUser->id, $thisNotificationUsers)?'selected':''}}>{{$eahcUser->name}}</option>
                              @endforeach
                            </select>
                            @error('permissions')
                                <div class="error error-msg-red">{{ $message }}</div>
                            @enderror
                        </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!--  -->
            </div>
        </div>

      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
@endsection
