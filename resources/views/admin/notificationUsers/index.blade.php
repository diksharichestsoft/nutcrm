@extends('admin.layout.template')
@section('contents')
      <a href="{{route('notification-users-create', 0)}}" class="btn btn-outline-success btn-lg">Create New Notification/Event</a>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!--  -->
                <div class="card-body">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width: 10px">#</th>
                          <th>Event/Notification</th>
                          <th>Users</th>
                          <th>Action</th>
                          <!-- <th style="width: 40px">Label</th> -->
                        </tr>
                      </thead>
                      <tbody>
                        <?php $i =0 ?>
                        @foreach($notifications as $eachNotification)
                          <tr>
                            <td><?php  $i++; echo $i; ?> </td>
                            <td>{{$eachNotification->title}} <a class="btn btn-success btn-sm" href="{{route('notification-users-create', $eachNotification->id)}}"><i class="fas fa-pencil-alt"></i></button></td>
                            <td>
                              @foreach($eachNotification->users as $eachUser)
                              <span class="badge bg-info">{{$eachUser->name}}</span>
                              @endforeach
                            </td>

                            <td>
                            <a href="{{route('notification-users-edit',$eachNotification->id)}}" class="btn btn-outline-success btn-sm rounded-pill">Edit</a>
                             </td>
                        </tr>
                        @endforeach

                      </tbody>
                    </table>
                </div>
                <!--  -->
            </div>
        </div>

      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
@endsection
