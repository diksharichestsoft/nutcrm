@extends('admin.layout.template')
@section('contents')
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!--  -->
                <div class="card card-primary">
                    <div class="card-header">
                        @if ($thisNotification)
                        <h3 class="card-title">Update Notification</h3>
                        @else
                        <h3 class="card-title">Create Notification</h3>
                        @endif
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form method="post" action="{{route('notification-users-store')}}" >
                        @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <br>
                                @if ($thisNotification)
                                    <input type="hidden" name="id" value="{{$thisNotification->id}}">
                                @endif
                                <input type="text" required value="{{$thisNotification->title ?? ''}}" class="form-control" name="title" id="subject">
                                @if($errors->has('title'))
                                    <div class="error">{{ $errors->first('tite') }}</div>
                                @endif
                            </div>

                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!--  -->
            </div>
        </div>

      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
    <script>
    </script>
@endsection
