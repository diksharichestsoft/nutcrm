@section('contents')
<style>
    .switch{
    opacity: 0;
    position: absolute;
    z-index: 1;
    width: 18px;
    height: 18px;
    cursor: pointer;
  }
  .switch + .lable{
    position: relative;
    display: inline-block;
    margin: 0;
    line-height: 20px;
    min-height: 18px;
    min-width: 18px;
    font-weight: normal;
    cursor: pointer;
  }
  .switch + .lable::before{
    cursor: pointer;
    font-weight: normal;
    font-size: 12px;
    color: #32a3ce;
    content: "\a0";
    background-color: #FAFAFA;
    border: 1px solid #c8c8c8;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
    border-radius: 0;
    display: inline-block;
    text-align: center;
    height: 16px;
    line-height: 14px;
    min-width: 16px;
    margin-right: 1px;
    position: relative;
    top: -1px;
  }
  .switch:checked + .lable::before {
    display: inline-block;
    content: '\f00c';
    background-color: #F5F8FC;
    border-color: #adb8c0;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
  }
  /* CSS3 on/off switches */
  .switch + .lable {
    margin: 0 4px;
    min-height: 24px;
  }
  .switch + .lable::before {
    font-weight: normal;
    font-size: 11px;
    line-height: 17px;
    height: 20px;
    overflow: hidden;
    border-radius: 12px;
    background-color: #F5F5F5;
    -webkit-box-shadow: inset 0 1px 1px 0 rgba(0, 0, 0, 0.15);
    box-shadow: inset 0 1px 1px 0 rgba(0, 0, 0, 0.15);
    border: 1px solid #CCC;
    text-align: left;
    float: left;
    padding: 0;
    width: 52px;
    text-indent: -21px;
    margin-right: 0;
    -webkit-transition: text-indent .3s ease;
    -o-transition: text-indent .3s ease;
    transition: text-indent .3s ease;
    top: auto;
  }
  .switch.switch-bootstrap + .lable::before {
    font-family: FontAwesome;
    content: "\f00d";
    box-shadow: none;
    border-width: 0;
    font-size: 16px;
    background-color: #a9a9a9;
    color: #F2F2F2;
    width: 52px;
    height: 22px;
    line-height: 21px;
    text-indent: 32px;
    -webkit-transition: background 0.1s ease;
    -o-transition: background 0.1s ease;
    transition: background 0.1s ease;
  }
  .switch.switch-bootstrap + .lable::after {
    content: '';
    position: absolute;
    top: 2px;
    left: 3px;
    border-radius: 12px;
    box-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
    width: 18px;
    height: 18px;
    text-align: center;
    background-color: #F2F2F2;
    border: 4px solid #F2F2F2;
    -webkit-transition: left 0.2s ease;
    -o-transition: left 0.2s ease;
    transition: left 0.2s ease;
  }
  .switch.switch-bootstrap:checked + .lable::before {
    content: "\f00c";
    text-indent: 6px;
    color: #FFF;
    border-color: #b7d3e5;

  }
  .switch-primary >.switch.switch-bootstrap:checked + .lable::before {
      background-color: #337ab7;
  }
  .switch-success >.switch.switch-bootstrap:checked + .lable::before {
      background-color: #5cb85c;
  }
  .switch-danger >.switch.switch-bootstrap:checked + .lable::before {
      background-color: #d9534f;
  }
  .switch-info >.switch.switch-bootstrap:checked + .lable::before {
      background-color: #5bc0de;
  }
  .switch-warning >.switch.switch-bootstrap:checked + .lable::before {
      background-color: #f0ad4e;
  }
  .switch.switch-bootstrap:checked + .lable::after {
    left: 32px;
    background-color: #FFF;
    border: 4px solid #FFF;
    text-shadow: 0 -1px 0 rgba(0, 200, 0, 0.25);
  }
  /* square */
  .switch-square{
    opacity: 0;
    position: absolute;
    z-index: 1;
    width: 18px;
    height: 18px;
    cursor: pointer;
  }
  .switch-square + .lable{
    position: relative;
    display: inline-block;
    margin: 0;
    line-height: 20px;
    min-height: 18px;
    min-width: 18px;
    font-weight: normal;
    cursor: pointer;
  }
  .switch-square + .lable::before{
    cursor: pointer;
    font-family: fontAwesome;
    font-weight: normal;
    font-size: 12px;
    color: #32a3ce;
    content: "\a0";
    background-color: #FAFAFA;
    border: 1px solid #c8c8c8;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
    border-radius: 0;
    display: inline-block;
    text-align: center;
    height: 16px;
    line-height: 14px;
    min-width: 16px;
    margin-right: 1px;
    position: relative;
    top: -1px;
  }
  .switch-square:checked + .lable::before {
    display: inline-block;
    content: '\f00c';
    background-color: #F5F8FC;
    border-color: #adb8c0;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
  }
  /* CSS3 on/off switches */
  .switch-square + .lable {
    margin: 0 4px;
    min-height: 24px;
  }
  .switch-square + .lable::before {
    font-weight: normal;
    font-size: 11px;
    line-height: 17px;
    height: 20px;
    overflow: hidden;
    border-radius: 2px;
    background-color: #F5F5F5;
    -webkit-box-shadow: inset 0 1px 1px 0 rgba(0, 0, 0, 0.15);
    box-shadow: inset 0 1px 1px 0 rgba(0, 0, 0, 0.15);
    border: 1px solid #CCC;
    text-align: left;
    float: left;
    padding: 0;
    width: 52px;
    text-indent: -21px;
    margin-right: 0;
    -webkit-transition: text-indent .3s ease;
    -o-transition: text-indent .3s ease;
    transition: text-indent .3s ease;
    top: auto;
  }
  .switch-square.switch-bootstrap + .lable::before {
    font-family: FontAwesome;
    content: "\f00d";
    box-shadow: none;
    border-width: 0;
    font-size: 16px;
    background-color: #a9a9a9;
    color: #F2F2F2;
    width: 52px;
    height: 22px;
    line-height: 21px;
    text-indent: 32px;
    -webkit-transition: background 0.1s ease;
    -o-transition: background 0.1s ease;
    transition: background 0.1s ease;
  }
  .switch-square.switch-bootstrap + .lable::after {
    content: '';
    position: absolute;
    top: 2px;
    left: 3px;
    border-radius: 12px;
    box-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
    width: 18px;
    height: 18px;
    text-align: center;
    background-color: #F2F2F2;
    border: 4px solid #F2F2F2;
    -webkit-transition: left 0.2s ease;
    -o-transition: left 0.2s ease;
    transition: left 0.2s ease;
  }
  .switch-square.switch-bootstrap:checked + .lable::before {
    content: "\f00c";
    text-indent: 6px;
    color: #FFF;
    border-color: #b7d3e5;

  }
  .switch-primary >.switch-square.switch-bootstrap:checked + .lable::before {
      background-color: #337ab7;
  }
  .switch-success >.switch-square.switch-bootstrap:checked + .lable::before {
      background-color: #5cb85c;
  }
  .switch-danger >.switch-square.switch-bootstrap:checked + .lable::before {
      background-color: #d9534f;
  }
  .switch-info >.switch-square.switch-bootstrap:checked + .lable::before {
      background-color: #5bc0de;
  }
  .switch-warning >.switch-square.switch-bootstrap:checked + .lable::before {
      background-color: #f0ad4e;
  }
  .switch-square.switch-bootstrap:checked + .lable::after {
    left: 32px;
    background-color: #FFF;
    border: 4px solid #FFF;
    text-shadow: 0 -1px 0 rgba(0, 200, 0, 0.25);
  }
  .switch-square.switch-bootstrap + .lable::after {
      border-radius: 2px;
  }
    .daterangepicker .ranges li {font-size: 12px;padding: 8px 12px;cursor: pointer;background: #000000b0;}
  </style>
  <div class="modal fade" id="attendanceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Attendance Data</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <span class="h5">User Name: </span> <span id="modal_user_name"></span>
            <br>
            <span class="h5">Date: </span> <span id="modal_this_date"></span>
            <br>
            <span class="h5">In At: </span> <span id="modal_in_at_data"></span>
            <br>
            <span class="h5">Out At: </span> <span id="modal_out_at_data"></span>
            <br>
            <span class="h5">In Ip: </span> <span id="modal_in_ip"></span>
            <br>
            <span class="h5">Out Ip: </span> <span id="modal_out_ip"></span>
            <br>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div>


<div class="card" id="data">
    <div class="card-header p-2" id="card_head">
      <ul class="nav nav-pills">
              <li class="nav-item"><a class="nav-link services present-absent-tab active" href="#view" onclick="fetch_data__data(1,this.getAttribute('data-id'))" data-id="1" data-toggle="tab">Present</a></li>&nbsp;
              <li class="nav-item"><a class="nav-link services present-absent-tab" href="#view" onclick="fetch_data__data(1,this.getAttribute('data-id'))" data-id="2" data-toggle="tab">Absent</a></li>&nbsp;
              <li class="nav-item"><a class="nav-link services present-absent-tab" href="#view" onclick="fetch_data__data(1,this.getAttribute('data-id'))" data-id="3" data-toggle="tab">All</a></li>&nbsp;


        <div id="reportrange" style="background: transparent; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
            <i class="fa fa-calendar"></i>&nbsp;
            <span class="ab"></span> <i class="fa fa-caret-down"></i>
           </div>
           <input type="hidden" id="start_date_range">
           <input type="hidden" id="end_date_range">
            <li class="nav-item search-right">
             <div class="search_bar">
                <div class="input-group" data-widget="sidebar-search">
                 <input class="form-control form-control-sidebar" id="search" type="search" placeholder="Search" aria-label="Search">
                </div>
             </div>
            </li>

      </ul>
    </div><!-- /.card-header -->

    <div class="card-body">
        <label class="label-switch switch-success">
            <input type="checkbox" id="toggle_check" value="0"  class="switch-square switch-bootstrap status toggle_user_login" data-id="" name="">
            <span class="lable">Show All Users</span></label>
        <div id="pagination_employee">

            {{-- @include('admin.employeeDsr.includes.view') --}}
        </div>

      <!-- /.tab-content -->
    </div><!-- /.card-body -->
</div>
<script>
    $("#toggle_check").on('click', (e)=>{
   if($("#toggle_check").val() == 1){
            $("#toggle_check").val(0)
        }else{
            $("#toggle_check").val(1)
        }
        fetch_data__data();
    })

function refreshDsrData(change_html_id){
    $('#page-loader').show();
    $.ajax({
    url:"{{route('attendance-filter')}}",
    success:function(data)
    {
        $(`#${change_html_id}`).empty().html(data);
      $('#page-loader').hide();
    },
    error:function(error){
      $('#page-loader').hide();

    }
  });
}
$(document).on('click', '.pagination a', function(event){
    event.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    fetch_data__data(page,'','');

  });

document.querySelector("#search").addEventListener("keyup",(e)=>{
        var search = $("#search").val();
        fetch_data__data();
  });

  function filterEmpData(search){
    $('#page-loader').show();
    var data={};
  var make_url= "{{url('/')}}/admin/attendance/search?search="+search;
  $.ajax({
    url:make_url,
    data:data,
    success:function(data)
    {

        $('#pagination_employee').empty().html(data);
      $('#page-loader').hide();

    },
    error:function(error){
      $('#page-loader').hide();

    }
  });
  }

  $(function() {
  var start = moment();
  var end = moment();
  function cb(start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      $('#start_date_range').val(start.format('YYYY-MM-DD'));
      $('#end_date_range').val(end.format('YYYY-MM-DD'));
      fetch_data__data(1,'');
  }

  $('#reportrange').daterangepicker({
      startDate: start,
      endDate: end,
      ranges: {
         'Today': [moment(), moment()],
         'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        //  'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        //  'Last 30 Days': [moment().subtract(29, 'days'), moment()],
         'This Month': [moment().startOf('month'), moment().endOf('month')],
         'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
  }, cb);
  cb(start, end);

  });



  function fetch_data__data(page, active = "")
  {
    $('#page-loader').show();
    if(active==''){
    var active=document.getElementsByClassName('services active')[0].getAttribute('data-id');
    }
    // active = (active)? active : 1
    var active_users = $("#toggle_check").val();
    var start_date=document.querySelector('#start_date_range').value;
    var end_date=document.querySelector('#end_date_range').value;
      $(".present-absent-tab").hide()
    if(start_date == end_date){
      $(".present-absent-tab").show()
  }
    var search=document.querySelector("#search").value;
    var data={search:search,start_date:start_date,end_date:end_date, active_users: active_users};
    var make_url= "{{url('/')}}/admin/attendance/search?active="+active+"&page="+page;
    $.ajax({
      url:make_url,
      data:data,
      success:function(data)
      {
          $('#pagination_employee').empty().html(data);
        $('#page-loader').hide();

      },
      error:function(error){
        $('#page-loader').hide();
      }
    });
    }


</script>

