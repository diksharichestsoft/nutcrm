
@php
$month = date("m");
$year = date("Y");
//
$day = date('w');
$week_start = date('d', strtotime('-'.$day.' days'));
$week_end = date('d', strtotime('+'.(6-$day).' days'));
//
$start_date = $week_start."-" . $month . "-" . $year;
$start_time = strtotime($start_date);


$end_time = strtotime("+7 day", $start_time);
for ($i = strtotime($att_start); $i <= strtotime($att_end); $i += 86400) {
  $thisMonthArray[] = date('Y-m-d', $i);
}
@endphp
<section class="content">
    <div class="container-fluid">
      <div class="row">
          <div class="col-md-12">

              <div class="card-body">
          <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th style="width: 10px">ID</th>
                        <th>Name</th>
                        <th>Other Info</th>
                        <th>This month Attendance</th>
                        @foreach ($thisMonthArray as $item)
                        <th>
                            {{$item}}
                        </th>
                            @endforeach
                      </tr>
                    </thead>
            <tbody>
              @forelse ( $data as $user)
              <tr>
                  <td> {{ $user->id }}</td>
                  <td> {{ $user->name }}</td>
                  <td> <a data-id="{{$user->id}}" class="btn btn-outline-success btn-xs" href="{{route('attendence.viewdetails').'?id='.$user->id}}"  target="_blank">Detail</a></td>

                  <td>{{$user->attendanceForCount->count()}}/{{date('t',strtotime($att_start))}}</td>

                  @for($i = $att_start ;$i <= $att_end; $i++)
                    <td>
                        @php
                            $thisUserAttendance = $user->attendanceForCount->where('date', strval($i))->first();
                        @endphp
                        @if ($thisUserAttendance)
                        <span class='btn btn-xs {{$thisUserAttendance->total_time > 30420 ? 'btn-success' : 'btn-info'}} toggle-attendance-modal'
                            data-punch_in_at="{{$thisUserAttendance->in_at}}"
                            data-punch_out_at="{{$thisUserAttendance->out_at}}"
                            data-in-ip = "{{$user->punchTiming->where('attendance_id', $thisUserAttendance->id)->first()->in_ip_address}}"
                            data-out-ip = "{{$user->punchTiming->where('attendance_id', $thisUserAttendance->id)->last()->out_ip_address}}"
                            
                            data-user-name = "{{$user->name}}"
                            data-this-date = "{{date('d-m-Y', strtotime($thisUserAttendance->date))}}"
                            >Present</span>
                        </br>
                        {{secToHoursHelper($thisUserAttendance->total_time) }}
                        @if($user->punchTiming->where('attendance_id', $thisUserAttendance->id)->last()->out_ip_address)
                        @if($user->punchTiming->where('attendance_id', $thisUserAttendance->id)->first()->in_ip_address != $user->punchTiming->where('attendance_id', $thisUserAttendance->id)->last()->out_ip_address)
                        <span style="color: red">
                            IP change
                        </span>
                        @endif
                        @endif
                        @else
                        <span class='btn btn-xs btn-danger'>Absent</span> </br>
                        @endif
                    </td>
                  @endfor

              </tr>
              @empty
                  <center> <h3> No Data Available! </h3></center>
              @endforelse
                    </tbody>
                  </table>
              </div>
              <!--  -->
          </div>
      </div>
       <div class="ml-4" id="departments_pagination" data-page="{{ $data->currentPage() }}"> {{  $data->links() }} </div>

    </div><!--/. container-fluid -->
  </section>
<script>


    $(".toggle-attendance-modal").on('click', function (e){
        e.preventDefault();

        $("#modal_in_at_data").empty();
        $("#modal_out_at_data").empty();
        $("#modal_in_ip").empty();
        $("#modal_out_ip").empty();
        $("#modal_user_name").empty();
        $("#modal_this_date").empty();

        $("#attendanceModal").modal('show');
        in_at = $(e.target).data('punch_in_at');
        out_at = $(e.target).data('punch_out_at');
        in_ip = $(e.target).data('in-ip');
        out_ip = $(e.target).data('out-ip');
        user_name = $(e.target).data('user-name');
        this_date = $(e.target).data('this-date');


        function time12Hours(timeString){
        var timeString = new Date(timeString)
        timeString = timeString.toLocaleString()
        timeString = timeString.split(",");
        return timeString[1];

        }

        $("#modal_in_at_data").html(time12Hours(in_at));
        $("#modal_out_at_data").html(time12Hours(out_at));
        $("#modal_in_ip").html(in_ip);
        $("#modal_out_ip").html(out_ip);
        $("#modal_user_name").html(user_name);
        $("#modal_this_date").html(this_date);
    })
</script>
