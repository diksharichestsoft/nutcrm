@extends('admin.layout.template')

@section('contents')

<h3>Details</h3>
<table class="table">
    <tbody>
        <tr>
            <th scope="row">Missing DSR</th>
            <td><button type="button" class="btn btn-primary modelDsr" data-toggle="modal" data-target="#changePassword"> @if($thisUser->missedDsrsAll() > 0)
                    {{$thisUser->missedDsrsAll()}}
                @endif</button>
            </td>
        </tr>
        <tr>
            <th scope="row">IP Changes</th>
            <td>{{$thisUser->countChangedIp()}}</td>
        </tr>
        <tr>
            <th scope="row">Short Leave</th>
            <td>
                <span data-toggle="tooltip" title="3 New Messages" class="badge badge-warning">
                    {{$thisUser->leaveTypes(1)}}
                </span>
            </td>
        </tr>

        <tr>
            <th scope="row">Half Leaves</th>
            <td>
                <span data-toggle="tooltip" title="3 New Messages" class="badge badge-primary">
                    {{$thisUser->leaveTypes(2)}}
                </span>
            </td>
        </tr>
        <tr>
            <th scope="row">Full Leaves</th>
            <td>
                <span data-toggle="tooltip" title="3 New Messages" class="badge badge-danger">
                    {{$thisUser->leaveTypes(3)}}
                </span>
            </td>
        </tr>
    </tbody>
</table>
<div class="modal fade" id="attendanceDsrModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Missing DSR Date</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            @foreach ($thisUser->missedDsrsAllDates() as $item)
            <span class="h5">Date:</span> <span class="dsr" style="
            margin-left: 138px;">{{$item }}</span><br> <span id="modal_user_name"></span>
            @endforeach
            <br>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
<script>
      $(".modelDsr").on('click', function (e){
        e.preventDefault();
        $("#modal_in_at_data").empty();
        $("#attendanceDsrModal").modal('show');
        function time12Hours(timeString){
        var timeString = new Date(timeString)
        timeString = timeString.toLocaleString()
        timeString = timeString.split(",");
        return timeString[1];
        }
    })
    </script>
@endsection
