<style>
    .daterangepicker .ranges li {font-size: 12px;padding: 8px 12px;cursor: pointer;background: #000000b0;}
  </style>
  <div class="card" id="data">
                <div class="card-header p-2" id="card_head">
                  <ul class="nav nav-pills">
                   <li style="align-self: center;">
                       <h3>
                       </h3>
                   </li>
                    <li class="nav-item search-right">
                        <div>
                            @can('master_password_add')
                            <a  id="addMasterPswdButton" class="btn btn-success" data-toggle="modal" data-target="#addMasterPswdModal">
                                <i class="fa fa-plus" aria-hidden="true"></i>Add Master Password
                            </a>
                            @endcan
                        </div>
                     <div class="search_bar">
                        <div class="input-group" data-widget="sidebar-search">
                         <input class="form-control form-control-sidebar" id="search" type="search" placeholder="Search" aria-label="Search">
                        </div>
                     </div>
                    </li>
                  </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                  <div class="tab-content">
                    <div class="tab-pane" id="profile">
                       {{-- @include('admin.permissions.includes.addform') --}}
                    </div>

                    <div class="active tab-pane" id="view">
                      {{-- @include('admin.announcements.includes.view') --}}
                      </div>
                    <!-- /.tab-pane -->
                  </div>
                  <!-- /.tab-content -->
                </div><!-- /.card-body -->
        </div>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> --}}

<script>
fetchMasterPswdData(1);

$("#search").on("keyup",(e)=>{
  fetchMasterPswdData(1);
  });

    $(document).on('click', '.pagination a', function(event){
  event.preventDefault();
  var page = $(this).attr('href').split('page=')[1];
  var sendurl=$(this).attr('href');
  fetchMasterPswdData(page);
});

function fetchMasterPswdData(page)
{
  $('#page-loader').show();
  var search=document.querySelector("#search").value;
//   page = (page='')?'1':page;
  var data={search};
  var make_url= "{{url('/')}}/admin/masterPswds/search?page="+page;
  $.ajax({
    url:make_url,
    data:data,
    success:function(data)
    {
    $('#view').empty().html(data);
    $('#page-loader').hide();
    },
    error:function(error){
      $('#page-loader').hide();

    }
  });
  }
  $("#addMasterPswdButton").on('click', function (){
      document.querySelector("#error_title").innerText="";
      document.querySelector("#error_password").innerText="";
    $("#add_masterPswd_form").on('submit', function (e){
        e.preventDefault();
        // var descData = ($("#editor").html());
        // $("#description").val(descData);
        var data = new FormData(this);
        console.log(data);
        $.ajax({
            type:'post',
            url:"{{route('masterPswds-store')}}",
            cache: false,
                contentType: false,
                processData: false,
                dataType: "JSON",
            data : {data: data},
        headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data:data,
            success:function(data){
                $('#addMasterPswdModal').modal('hide');
                $(".modal-backdrop").hide();
                $("body").removeClass("modal-open");
                fetchMasterPswdData(1);
                Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Added Successfully',
                showConfirmButton: false,
                timer: 1500
                })

            },
            error:function(data){
            $.each(data.responseJSON.errors, function(id,msg){
                $('#error_'+id).html(msg);

            })
            }
        });
        })

})

function removeMasterPswd(e)
{

  var id=e.getAttribute('data-id');
  swal({
  title: "Oops....",
  text: "Are You Sure You want to delete!",
  icon: "error",
  buttons: [
    'NO',
    'YES'
  ],
}).then(function(isConfirm) {
  if (isConfirm) {
    $.ajax({
      url:"{{route('remove-masterPswds')}}",
      type:"post",
      data:{id:id},
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
      success:function(data){
        Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Remove Successfully',
                showConfirmButton: false,
                timer: 1500
                })
                fetchMasterPswdData(1);
        }
    })
  } else {

  }
});
}


function updateMasterPswd(e)
{
  $('#page-loader').show();

  var id=e.getAttribute('data-id');
  $.ajax({
    url:"{{route('masterPswds-edit')}}",
    type:"get",
    data:{id:id},
    success:function(data)
    {
       // toastr.success(response.message)
      $('#page-loader').hide();
      $('#edit_dsr_modal_body').empty().html(data);
    $("#editMasterPswdModal").modal('show');
      intitializeUpdate();
    },
    error:function(){
      $('#page-loader').hide();
    }
  })
}
function intitializeUpdate(){

// initializeCreateForm()
    $("#update_masterPswd_form").on('submit', function (e){
        e.preventDefault();
        // var descData = ($("#editorupdate").html());
        // console.log(descData)
        // $("#update-description").val(descData);

        var data = new FormData(this);
        console.log(data);
        $.ajax({
            type:'post',
            url:"{{route('masterPswds-store')}}",
            cache: false,
                contentType: false,
                processData: false,
                dataType: "JSON",
            data : {data: data},
        headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data:data,
            success:function(data){
                $('#addMasterPswdModal').modal('hide');
                $(".modal-backdrop").hide();
                fetchMasterPswdData(1);
                Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Updated Successfully',
                showConfirmButton: false,
                timer: 1500
                })
            },
            error:function(data){
            $.each(data.responseJSON.errors, function(id,msg){
                $('#error_'+id).html(msg);

            })
            }
        });
        })

}
  function toggleDisableEnable(e)
{
  var id=e.getAttribute('data-id');
  swal({
  title: "Hey....",
  text: "Are You Sure You want to change status of this Password!",
  icon: "warning",
  buttons: [
    'NO',
    'YES'
  ],
}).then(function(isConfirm) {
  if (isConfirm) {
    var id=e.getAttribute('data-id');
       $.ajax({
       type: "post",
       url: "{{route('masterPswd-toggle')}}",
       headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
       data:{id:id},
       success: function(data){
        fetchMasterPswdData(1)
       },
       error: function(error){
         alert('error');
       }
       });
  } else {

  }
});
}

</script>
