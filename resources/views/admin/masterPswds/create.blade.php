<!-- Button trigger modal -->
  <!-- Modal -->
  <style>
    #user + span {
      width: 100% !important;
    }
    </style>
  <form id="add_masterPswd_form" action="" method="POST">
    <div id="dsr_item_container">
        @csrf
        <div class="dsr_items">
            <div class="form-group">
                <label for="title">Title</label>
                <br>
                <input type="text" required class="form-control" name="title">
                <input type="hidden" class="form-control" name="created_by" value="{{Auth::user()->id}}">
                <input type="hidden" name="reqType" value="0">
                <div class="error" id="error_title"></div>
            </div>
           
            <div class="form-group">
              <label for="password">Password</label>
              <br>
              <input type="password" required class="form-control" name="password">
              <div class="error" id="error_password"></div>
          </div>
        </div>
    </div>
    <br>
        {{-- <button type="submit" class="btn btn-primary">Save</button> --}}
        <button type="button" class="btn btn-primary" onclick="$('#add_masterPswd_form').submit()"> Save</button>

    </form>
   
    
