   <!-- Main content -->
   <div class="modal fade" id="addMasterPswdModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Master Password</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="add_dsr_modal_body" class="modal-body">
            @include('admin.masterPswds.create')
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

   <div class="modal fade" id="editMasterPswdModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Update Master Password</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="edit_dsr_modal_body" class="modal-body">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>



    {{-- <a href="#" id="addTodoButton" class="btn btn-outline-success btn-lg" data-toggle="modal" data-target="#addTodoModal">
        <i class="fa fa-plus" aria-hidden="true"></i>New Todo
    </a> --}}
    <div class="modal fade" id="createDeal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Todos</h5>
              <button type="button" class="close" id='createDealHide' data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body " id="todoBody">
            </div>
          </div>
        </div>
      </div>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!--  -->
                <div class="card-body">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width: 10px">#</th>
                          <th>Action</th>
                          <th>Title</th>
                          <th>Password status</th>
                          <th>Created At</th>
                          <th>Updated At</th>
                        </tr>
                      </thead>
                      <tbody>
                          @forelse ($masterPswds as $key=> $value )
                          <tr>
                              <td>
                                {{$key+1}}
                              </td>
                              <td>
                                <div class="btn-group">
                                    @can('master_password_edit')
                                    <button data-id="{{$value->id}}" onclick="updateMasterPswd(this)" class="btn btn-outline-success btn-xs  update" ><i class="fas fa-pencil-alt"></i></button>
                                    @endcan

                                    {{-- @can('announcements_view')
                                    <button data-id="{{$value->id}}" class="btn btn-outline-success btn-xs view" onclick="viewAnnouncementDetail(this)"><i class="fas fa-eye"></i></button>
                                    @endcan --}}

                                    @can('master_password_delete')
                                    <button data-id="{{$value->id}}" onclick="removeMasterPswd(this)" class="btn btn-danger btn-xs  remove"><i class="fas fa-times"></i></button>
                                    @endcan

                                </div>
                              </td>
                              <td>
                                  {{$value->title}}
                                </td>
                                @can('master_password_toggle_status')

                                <td>@if($value->status==1)
                                    <button data-id="{{$value->id}}" class="disable_enable btn btn-success btn-xs" onclick="toggleDisableEnable(this)">Enable</button>
                                    @else
                                    <button data-id="{{$value->id}}" class="disable_enable btn btn-danger btn-xs" onclick="toggleDisableEnable(this)">Disable</button>
                                    @endif
                                </td>
                              @endcan
                          <td>{{ $value->created_at->format('d/m/Y')}}</td>
                          <td>{{ $value->updated_at->format('d/m/Y')}}</td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="9">
                                <h4  align="center">No Data Found</h4>
                            </td>
                        </tr>
                        @endforelse
                      </tbody>
                    </table>
                </div>

                <!--  -->
            </div>
        </div>
        {{$masterPswds->links()}}
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
    <script>




   

// function viewAnnouncementDetail (e){
//     let id = $(e).data('id')
//     $.ajax({
//     url:"{{route('announcement-detail-view')}}",
//     type:"get",
//     data:{id:id},
//     success:function(data)
//     {
//        // toastr.success(response.message)
//       $('#page-loader').hide();
//       $('#edit_dsr_modal_body').empty().html(data);
//     $("#editAnnouncementModal").modal('show');
//       intitializeUpdate();
//     },
//     error:function(){
//       $('#page-loader').hide();
//     }
//   })
// }

</script>
