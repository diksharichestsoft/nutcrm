<section class="content">
    <div class="container-fluid">
        <div class="row">
                <!-- /.col -->
        <div class="col-md-9">
            <div class="card">
            <div class="card-header p-2">
                <ul class="nav nav-pills">

                <li class="nav-item"><a class=" active nav-link" href="#timeline" data-toggle="tab">Timeline</a></li>
                <li class="nav-item"><a data-id="{{$getProjectData->id}}" class="btn-success nav-link" data-toggle="modal" data-target="#addComment">Add Comment</a>
                </li>
                </ul>
            </div><!-- /.card-header -->
            <div class="card-body">
                <div class="tab-content">
                <!-- /.tab-pane -->
                <div class=" active tab-pane" id="timeline">
                    <!-- The timeline -->
                    <div class="timeline timeline-inverse">
                    <!-- timeline item -->
                    @foreach($getcomment as $key=>$comment)
                    <?php $ctrtime = date('d M Y', strtotime($comment->created_at));
                    if(isset($getcomment[($key-1)]->created_at)){
                        $pretime = date('d M Y', strtotime($getcomment[($key-1)]->created_at));
                        $ctrtime = ($ctrtime!=$pretime)?$ctrtime:'';
                    }
                    ?>
                    @if(!empty($ctrtime))
                    <div class="time-label">
                        <span class="bg-danger">
                        {{$ctrtime}}
                        </span>
                    </div>
                    @endif
                    <div>
                        <i class="fas {{($comment->type!=1)?'fa-user bg-info':'fa-comments bg-warning'}}"></i>
                        <div class="timeline-item">
                            <span class="time"><i class="far fa-clock"></i> {{date('h.i A', strtotime($comment->created_at))}}</span>
                        <h3 class="timeline-header border-0"><a href="#">{{$comment->name }}</a> {{($comment->type!=1)?$comment->title:' commented on Deal'}}
                        </h3>
                        <div class="timeline-body">
                            {!!$comment->comment!!}
                        </div>

                        </div>
                    </div>

                    @endforeach
                    {{$getcomment->links()}}

                <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
    </section>
