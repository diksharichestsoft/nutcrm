<style>
  select#selectsatus {
    width: 100%;
}
</style>
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Deals</h3>
    <h6 class="results text-right">Total Deals : {{$data->total()}}</h6>
  </div>
              <!-- /.card-header -->
             <div class="card-body">
                <table class="table table-bordered">
                  @if(count($data)>0)
                  <thead>
                    <tr>
                      <th style="width: 10px">Action</th>
                      <th>ID</th>
                      <th>Title</th>
                      <th>Client Details</th>
                      <th>Project Type</th>
                      <th>Other Info</th>
                      <th>Dates</th>
                      <th>BDE/BDM</th>
                      @if(isset($active) && $active!=1)
                      @can('deals_change_status')
                      <th>Action</th>
                      @endcan
                      @endif
                    </tr>
                  </thead>
                  @endif
                  <tbody>
                        @forelse($data as $key=>$value)
                           <tr>
                                <td>
                                  <div class="btn-group">
                                      @can('deals_assign')

                                      <button data-id="{{$value->id}}" class="btn btn-primary btn-xs assignButton">Assign</button>
                                      @endcan
                                      @can('deals_accept')
                                        @if($value->deal_status==1)

                                        @if(isset($active) && $active!=-1)
                                        <button data-id="{{$value->id}}" onclick="accept(this)" class="btn btn-primary btn-xs remove">Accept</button>
                                        @endif

                                        @endif
                                      @endcan
                                    @can('deals_view')
                                    <a href="{{route('companyDeal-view',$value->id)}}" target="_blank" class="btn btn-outline-success btn-xs  view"><i class="fas fa-eye"></i></a>
                                    @endcan

                                    @can('deals_edit')
                                    <button data-id="{{$value->id}}" onclick="updateCompanyProject(this)" class="btn btn-outline-success btn-xs  update" data-toggle="modal" data-target="#createDeal" ><i class="fas fa-pencil-alt"></i></button>
                                    @endcan

                                    @can('deals_comment')
                                    <button data-id="{{$value->id}}" class="btn btn-outline-success btn-xs comment" data-toggle="modal" data-target="#addComment"><i class="far fa-comment"></i></i></button>

                                    @endcan
                                    @can('deals_delete')
                                    <button data-id="{{$value->id}}" onclick="removeCompanyProject(this)" class="btn btn-danger btn-xs  remove"><i class="fas fa-times"></i></button>
                                    @endcan
                                  </div>
                                </td>
                                <td>{{$value->id??''}}</td>
                                <td style="word-break: break-all;">{{$value->title??''}}</td>
                                <td style="word-break: break-all;">
                                    <b>Name:</b> {{$value->client_name??''}}<br/>
                                    <b>Email:</b> {{$value->client_email??''}}<br/>
                                    <b>Phone:</b> {{$value->client_phone??''}}
                                </td>
                                <td>
                                    {{$value->projectTypeData->name}}
                                </td>
                                <td>
                                    <b>Platform: </b><p class="smaller-text" >{{$value->platformData->name??''}}</p>
                                    <b>Upwork Id:</b><p class="smaller-text" > {{$value->platform_id??''}}</p>
                                   <b> Company: </b><p class="smaller-text" >{{$value->companyData->name??''}}</p>
                                   <b> Status: </b><p class="smaller-text {{dealsBadgeClass($value->deal_status)}}" >{{$value->deal_status_title??''}}</p>
                                </td>
                                <td>
                                   <b> Created At :</b> <p class="smaller-text" >{{($value->created_at)?? ''}}</p>
                                   <b> Updated At :</b> <p class="smaller-text" >{{($value->updated_at)}}</p>
                                </td>
                                <td>
                                   <b> Created By :</b> <p class="smaller-text" >{{$value->created_by_user_name ?? ""}}</p>
                                   @if($value->updated_by_user_name)
                                   <b> Updated By :</b> <p class="smaller-text" >{{$value->updated_by_user_name ?? ""}}</p>
                                   @endif
                                   <b> Assign To :</b> <p class="smaller-text" >{{$value->assigned_to_user_name ?? ""}}</p>
                                </td>
                                @if($value->deal_status!=1)
                                @can('deals_change_status')

                                <td class="d-flex">
                                    <select name="sel" id="selectsatus" class="form-control deals_status" data-status="{{$value->deal_status}}" data-id="{{ $value->id }}" >
                                        <option value="0">Select Status</option>
                                        @foreach($Dealstatus as $status)
                                        <option value="{{$status->id}}"  {{($value->deal_status==$status->id)?"selected":""}}>{{$status->dealname}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                @endcan
                                @endif
                           </tr>
                        @empty
                          <center> <h3> No Data Available </h3> </center>
                        @endforelse
                  </tbody>
                </table>
              </div>
              {{$data->links()}}
            </div>
