<form action="" name="updateCompany" id="updateCompany">
          @csrf
        <input type="hidden" name="id" id="update_id" value="{{$getProjectData->id}}">
        <div class="value">   
          <div class="form-group row">
            <label for="deal_title" class="col-sm-2 col-form-label">Title</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="title"  name="deal_title" value="{{$getProjectData->title}}" >
              <div class="error" id="error_deal_title"></div>
              </div>
          </div>
          <div class="form-group row">
            <label for="company" class="col-sm-2 col-form-label">Company</label>
            <div class="col-sm-10">
              <select name="company" id="company" class="form-control select2" >
                <option value="">Select Company</option>
                @foreach($companies as $company)
                  <option value="{{ $company->id }}" {{ $getProjectData->company_id == $company->id ? 'selected' : '' }}>{{ $company->name }}</option>
                @endforeach

              </select>
              <div class="error" id="error_company"></div>
            </div>
          </div>
          <div class="form-group row">
            <label for="url" class="col-sm-2 col-form-label">Url*</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="url" name="url" value="{{$getProjectData->url??''}}" >
              <div class="error" id="error_url"></div>
            </div>
          </div>
          <div class="form-group row">
            <label for="price" class="col-sm-2 col-form-label">Project Price*</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="price" name="price" value="{{$getProjectData->budget??''}}"  >
              <div class="error" id="error_price"></div>
            </div>
          </div>
          <div class="form-group row">
            <label for="platform" class="col-sm-2 col-form-label">Platform</label>
            <div class="col-sm-10">
              <select name="platform" id="platform" class="form-control select2">
                <option value="">Select Platform</option>
                @foreach($platform as $platform)
                <option value="{{ $platform->id }}" {{ $getProjectData->platform == $platform->id ? 'selected' : '' }}>{{ $platform->name }}</option>
                @endforeach
              </select>
              <div class="error" id="error_platform"></div>
            </div>
          </div>
          <div class="form-group row">
            <label for="platform_id" class="col-sm-2 col-form-label">Upwork ID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="platform_id" name="platform_id" value="{{$getProjectData->platform_id??''}}" >
              <div class="error" id="error_platform_id"></div>
            </div>
          </div>
          <div class="form-group row">
            <label for="department" class="col-sm-2 col-form-label">Department</label>
            <div class="col-sm-10">
              <select name="department" id="department" class="form-control select2">
                <option value="">Select Department</option>
                @foreach($project_type as $project_type)
                <option value="{{ $project_type->id }}" {{ $getProjectData->project_type == $project_type->id ? 'selected' : '' }}>{{ $project_type->name }}</option>
                @endforeach
              </select>
              <div class="error" id="error_department"></div>
            </div>
          </div>
          <div class="form-group row">
            <label for="client_name" class="col-sm-2 col-form-label">Client Name</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="client_name" name="client_name"  value="{{$getProjectData->client_name??''}}" >
              <div class="error" id="error_client_name"></div>
            </div>
          </div>
          <div class="form-group row">
            <label for="client_email" class="col-sm-2 col-form-label">Client Email</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="client_email" name="client_email"  value="{{$getProjectData->client_email??''}}" >
              <div class="error" id="error_client_email"></div>
            </div>
          </div>
          <div class="form-group row">
            <label for="client_phone" class="col-sm-2 col-form-label">Client Phone</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="client_phone" name="client_phone"  value="{{$getProjectData->client_phone??''}}" >
              <div class="error" id="error_client_phone"></div>
            </div>
          </div>
          <div class="form-group row">
            <label for="job" class="col-sm-2 col-form-label">Job Description</label>
            <div class="col-sm-10">
              <textarea name="job" id="job_desc" cols="30" rows="10" class="form-control">{{$getProjectData->job_descriprion??''}}</textarea>
              <div class="error" id="error_job"></div>
            </div>
          </div>
          <div class="form-group row">
            <div class="offset-sm-2 col-sm-10">
              <button type="submit" class="btn btn-success">Update Deal</button>
              <button type="reset" class="btn btn-danger">Reset</button>
            </div>
          </div>
        </div>
      </form>