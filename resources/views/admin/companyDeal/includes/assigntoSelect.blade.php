<select name="assign_to" required id="employees_select"  class="selectpicker" data-live-search="true">
    <option value="" disabled selected>Select Employee</option>
    @foreach ($users as $eachUser)
    <option value="{{$eachUser->id}}">{{$eachUser->name}}</option>
    @endforeach
  </select>
  <script type="text/javascript">
    $('#employees_select').selectpicker({
      });
</script>
