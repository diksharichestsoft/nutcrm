@extends('admin.layout.template')
@section('contents')
    <style>
        /* .dark-mode .list-group-item {
        border-color: #454d55;
        background-color: #343a40;
      } */
        .icon {
            color: #007bff;
            margin-right: 10px;
        }

        .card-header {
            border-bottom: 1px solid #454d55;
        }

        .dark-mode .list-group-item {
            border-color: #343a40;
            background-color: #454d55;
        }

    </style>
    {{-- Add comment Modal --}}
    <div class="modal fade" id="createDeal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Deals</h5>
                    <button type="button" class="close" id='createDealHide' data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body " id="createDealBody">
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addComment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Comment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body " id="addCommentBody">
                    <form action="" name="addCommentForm" id="addCommentForm">
                        @csrf
                        <input type="hidden" name='deal_id' id="deal_id" value="{{ $getProjectData->id }}">
                        <div class="value">
                            <div class="form-group row">
                                <label for="comment" class="col-sm-2 col-form-label">Comment</label>
                                <div class="col-sm-10">
                                    {{-- <textarea name="comment" id="comment" cols="30" rows="10" class="form-control"></textarea> --}}
                                    <input type="hidden" id="comment" class="form-control" name="comment">
                                    <div id="editor" style="border: 1px solid #ffffff42;"></div>

                                    <div class="error" id="error_comment"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="offset-sm-2 col-sm-10">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="changestatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel1">Assign Task</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="changeStatuSbody">
                    <form action="" name="changestatusform" id="changestatusform">
                        @csrf
                        <input type="hidden" name='status' id="changeStatus-status" value="">
                        <input type="hidden" name='deal_id' id="changeStatus-deal_id" value="{{ $getProjectData->id }}">
                        <div class="value">
                            <div class="form-group row">
                                <label for="comment" class="col-sm-2 col-form-label">Comment</label>
                                <div class="col-sm-10">
                                    <textarea name="comment" cols="30" rows="10" class="form-control"></textarea>
                                    <div class="error" id="error_comment"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="offset-sm-2 col-sm-10">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="permissions_data">

        <section class="content">
            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Projects Detail</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>


                <div class="card-body">
                    <div class="row pb-4">
                        <div class="col-4">
                            <h5 class="mt-5 ">Project Details</h5>
                            <ul class="list-unstyled list-group">
                                <li class="list-group-item">
                                    <i class="far fa-fw fa-file-word icon"></i>Deal Name:- {{ $getProjectData->title ?? '' }}
                                </li>
                                <li class="list-group-item">
                                    <i class="far fa-fw fa-file-pdf icon"></i> Deal Type:-
                                    {{ $getProjectData->project_name ?? '' }}
                                </li>
                                <li class="list-group-item">
                                    <i class="far fa-fw fa-envelope icon"></i>Reffred By:-
                                    {{ $getProjectData->user_name ?? '' }}
                                </li>
                                <li class="list-group-item">
                                    <i class="far fa-fw fa-image icon"></i>Platform:-
                                    {{ $getProjectData->platform_name ?? '' }}
                                </li>
                                <li class="list-group-item">
                                    <i class="far fa-fw fa-image icon"></i>Platform ID:-
                                    {{ $getProjectData->platform_id ?? '' }}
                                </li>
                                <li>
                                    <div class="row mt-4">
                                        <div class="col-md-3">
                                            <h6>Actions</h6>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="btn-group">
                                                @can('deals_edit')
                                                    <button data-id="{{ $getProjectData->id }}"
                                                        onclick="updateCompanyProject(this)"
                                                        class="btn btn-outline-success btn-xs  update" data-toggle="modal"
                                                        data-target="#createDeal"><i class="fas fa-pencil-alt"></i></button>
                                                @endcan
                                                @can('deals_delete')
                                                    <button data-id="{{ $getProjectData->id }}"
                                                        onclick="removeCompanyProject(this)"
                                                        class="btn btn-danger btn-xs  remove"><i
                                                            class="fas fa-times"></i></button>
                                                @endcan
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        @can('deal_details_clientinfo_hide')
                        <div class="col-4">
                            <h5 class="mt-5 text">Client Infromation</h5>
                            <ul class="list-unstyled">
                                <li class="list-group-item">
                                    <i class="far fa-user-circle icon"></i>&nbsp;Client Name:-
                                    {{ $getProjectData->client_name ?? '' }}
                                </li>
                                <li class="list-group-item">
                                    <i class="fas fa-envelope-square icon"></i>&nbsp;Client Email:-
                                    {{ $getProjectData->client_email ?? '' }}
                                </li>
                                <li class="list-group-item">
                                    <i class="fas fa-phone-square-alt icon"></i>&nbsp;Client Phone:-
                                    {{ $getProjectData->client_phone ?? '' }}
                                </li>

                                <?php
                                    if($getProjectData->contact_email != null){
                                ?>
                                <li class="list-group-item">
                                    <i class="fas fa-phone-square-alt icon"></i>&nbsp;Contact Email:-
                                    {{ $getProjectData->contact_email ?? '' }}
                                </li>
                                <?php
                                    }
                                ?>
                                <?php
                                    if($getProjectData->contact_phone != null){
                                ?>
                                <li class="list-group-item">
                                    <i class="fas fa-phone-square-alt icon"></i>&nbsp;Contact Phone:-
                                    {{ $getProjectData->contact_phone ?? '' }}
                                </li>
                                <?php
                                    }
                                ?>
                                <?php
                                    if($getProjectData->contact_descriprion != null){
                                ?>
                                <li class="list-group-item">
                                    <i class="fas fa-phone-square-alt icon"></i>&nbsp;Contact Descriprion:-
                                    {{ $getProjectData->contact_descriprion ?? '' }}
                                </li>
                                <?php
                                    }
                                ?>
                            </ul>
                        </div>
                        @endcan
                        <div class="col-4">
                            <h5 class="mt-5 text">Project Times Details</h5>
                            <ul class="list-unstyled">
                                @can('deals_change_status')
                                    <li class="list-group-item">
                                        <div scope="row">Status</div>
                                        <div class="d-flex">
                                            <select name="sel" id="selectsatus" class="form-control deals_status" data-status="{{$getProjectData->deal_status}}"
                                                data-id="{{ $getProjectData->id }}">
                                                <option value="0">Select Status</option>
                                                @foreach ($dealStatus as $status)
                                                    <option value="{{ $status->id }}"
                                                        {{ $getProjectData->deal_status == $status->id ? 'selected' : '' }}>
                                                        {{ $status->dealname }}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </li>
                                @endcan
                                @if ($getProjectData->deal_status == 3)
                                    <li class="list-group-item">
                                        <i class="fas fa-time icon"></i>&nbsp;<b>Meeting Time:-
                                        </b>{{ date('d-m-Y h:i a', strtotime($getProjectData->meeting_time)) ?? '' }}
                                    </li>
                                @endif
                                <li class="list-group-item">
                                    <i class="fas fa-stopwatch icon"></i>&nbsp;Planned Start Date:-
                                    {{ $getProjectData->planned_start_date ?? '' }}
                                </li>
                                <li class="list-group-item">
                                    <i class="fas fa-business-time icon"></i>&nbsp;Planned End Date:-
                                    {{ $getProjectData->planned_start_date ?? '' }}
                                </li>
                                <li class="list-group-item">
                                    <i class="fas fa-hourglass-half icon"></i>&nbsp;Estimated Hour:-
                                    {{ $getProjectData->estimated_hours ?? '' }}
                                </li>
                                <li class="list-group-item">
                                    <i class="fas fa-link icon"></i>&nbsp;Url:- <a
                                        href="{{ $getProjectData->url ?? '' }}">{{ $getProjectData->url ?? '' }}</a>
                                </li>
                            </ul>
                        </div>

                    </div>



                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
                            <div class="row">
                                <div class="col-12">
                                    <br>


                                </div>
                                <div class="row">
                                    <div class="col-12 mt-3 mb-3">

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <h3>Description</h3>
                                        <p>
                                            {{ $getProjectData->job_descriprion }}
                                        </p>
                                        @foreach ($comments as $comment)
                                            <div class="post">
                                                <div class="user-block">
                                                    <img class="img-circle img-bordered-sm"
                                                        src="../../dist/img/user1-128x128.jpg" alt="user image">
                                                    <span class="username">
                                                        <a href="#">{{ $comment->name }}</a>
                                                    </span>
                                                    <span class="description">{{ $comment->title }} - 7:45 PM
                                                        today</span>
                                                </div>
                                                <!-- /.user-block -->
                                                <p>
                                                    {{ $comment->comment }}
                                                </p>

                                                <p>
                                                    <a href="#" class="link-black text-sm"><i
                                                            class="fas fa-link mr-1"></i> Demo File 1 v2</a>
                                                </p>
                                            </div>
                                        @endforeach






                                    </div>

                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
        </section>
        <!-- Content Wrapper. Contains page content -->
        <!-- <div class="content-wrapper"> -->
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">

                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>TimeLine</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">

                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <div id="comment-section-view">
            @include('admin.companyDeal.includes.timeline')
        </div>
        <!--End comment-section-view -->
        <!-- /.content -->
        <!-- </div> -->
        <!-- /.content-wrapper -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jscroll/2.4.1/jquery.jscroll.min.js"></script>
        <script>
            $('ul.pagination').hide();
            $(function() {
                $('.timeline-inverse').jscroll({
                    autoTrigger: true,
                    padding: 0,
                    nextSelector: '.pagination li.active + li a',
                    contentSelector: 'div.timeline-inverse',
                    callback: function() {
                        $('ul.pagination').remove();
                    }
                });
            });

            function updateCompanyProject(e) {
                id = $(e).data('id');
                console.log(id)
                $('#page-loader').show();

                $.ajax({
                    url: "{{ route('deal-edit') }}",
                    type: "get",
                    data: {
                        id
                    },
                    success: function(data) {
                        $('#page-loader').hide();
                        $('#createDealBody').empty().html(data);
                    },
                    error: function(error) {
                        $('#page-loader').hide();
                    }
                })
            }

            $(document).on('submit', '#updateCompany', function(e) {
                e.preventDefault();
                var data = new FormData(this);
                $.ajax({
                    type: 'post',
                    // url:"{{ url('/') }}/admin/companyDeal/UpdateDealProjectData",
                    url: "{{ route('deals-add-company') }}",
                    dataType: "JSON",
                    xhr: function() {
                        myXhr = $.ajaxSettings.xhr();
                        return myXhr;
                    },
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function(data) {
                        $(".services:first").trigger("click");
                        $('#createDeal').modal('hide');
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Added Successfully, Reload the page to see the changes',
                            showConfirmButton: true
                        })
                    },
                    error: function(data) {
                        $.each(data.responseJSON.errors, function(id, msg) {
                            $('#error_' + id).html(msg);
                        });
                    }
                });
            });

            function removeCompanyProject(e) {
                var id = e.getAttribute('data-id');
                swal({
                    title: "Oops....",
                    text: "Are You Sure You want to delete!",
                    icon: "error",
                    buttons: [
                        'NO',
                        'YES'
                    ],
                }).then(function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: "{{ route('deal-remove-data') }}",
                            type: "post",
                            data: {
                                id: id
                            },
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(data) {
                                $('#view').empty().html(data);
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'Remove Successfully',
                                    showConfirmButton: true
                                }).then(() => {
                                    window.location.replace("{{ route('deals', 'all') }}");
                                })
                            }
                        })
                    } else {

                    }
                });
            }

            $(document).on('submit', '#addCommentForm', function(e) {
                $('#page-loader').show();

                e.preventDefault();
                var commentData = ($("#editor").html());
                $("#comment").val(commentData);

                var data = new FormData(this);
                $.ajax({
                    type: 'post',
                    url: "{{ route('deals-add-Comment') }}",
                    dataType: "JSON",
                    xhr: function() {
                        myXhr = $.ajaxSettings.xhr();
                        return myXhr;
                    },
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function(data) {
                        $('#addComment').modal('hide');
                        getCommentData();
                        $('#page-loader').hide();
                        myEditor.setData('');
                    },
                    error: function(data) {
                        $.each(data.responseJSON.errors, function(id, msg) {
                            $('.error#error_' + id).html(msg);
                            $('#page-loader').hide();

                        })
                    }
                });
            });

            function getCommentData() {
                $('#page-loader').show();
                $.ajax({
                    url: "{{ route('deals-get-timeline') }}",
                    data: {
                        id: "{{ $getProjectData->id }}"
                    },
                    success: function(data) {
                        $('#comment-section-view').empty().html(data);
                        $('#page-loader').hide();

                    }
                });
            }

            $(document).on('change', '#selectsatus', function() {
                oldstatus = $(this).data('status')
                var status = $(this).val();
                $(this).val(oldstatus);
                $('#changeStatus-status').val(status);
                $('#changestatusform textarea').val('');
                $('#changestatus').modal('show');
            });

            $(document).ready(function() {
                $(".services").eq(0).addClass("active");
            });
            $(document).on('click', '.comment', function() {
                var id = $(this).attr('data-id');
                $('#deal_id').val(id);
                $('#comment_title').val('');
                $('#comment').val('');

            });
            $(document).on('submit', '#changestatusform', function(e) {
                e.preventDefault();
                var data = new FormData(this);
                $.ajax({
                    type: 'post',
                    url: "{{ route('deals-change-status') }}",
                    dataType: "JSON",
                    xhr: function() {
                        myXhr = $.ajaxSettings.xhr();
                        return myXhr;
                    },
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function(data) {
                        if (data.status == 0) {
                            $('#changestatus').modal('hide');
                            Swal.fire({
                                position: 'top-end',
                                icon: 'error',
                                title: data.message,
                                showConfirmButton: false,
                                timer: 7000
                            })
                            return
                        } else {
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: "Status Changed successfully",
                                showConfirmButton: false,
                                timer: 2000
                            })
                        }
                        $('#changestatus').modal('hide');
                        getCommentData(1, '');
                    },
                    error: function(data) {
                        $.each(data.responseJSON.errors, function(id, msg) {
                            $('#error_' + id).html(msg);
                        })
                    }
                });
            });
        </script>
        <script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/inline/ckeditor.js"></script>
        <script>
            var myEditor;
            InlineEditor
                .create(document.querySelector(`#editor`))
                .then(editor => {
                    console.log('Editor was initialized', editor);
                    myEditor = editor;
                })
                .catch(err => {
                    console.error(err.stack);
                });
            $(document).ready(function() {
                // Hiding default extrack file and dropdown buttons
                $(".ck-file-dialog-button").hide();
                $(".ck-dropdown__button").hide();
            })
        </script>
    </div>
@endsection
