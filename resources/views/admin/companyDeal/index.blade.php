<style>
    .daterangepicker .ranges li {
        font-size: 12px;
        padding: 8px 12px;
        cursor: pointer;
        background: #000000b0;
    }

</style>
<style>
    .switch{
  opacity: 0;
  position: absolute;
  z-index: 1;
  width: 18px;
  height: 18px;
  cursor: pointer;
}
.switch + .lable{
  position: relative;
  display: inline-block;
  margin: 0;
  line-height: 20px;
  min-height: 18px;
  min-width: 18px;
  font-weight: normal;
  cursor: pointer;
}
.switch + .lable::before{
  cursor: pointer;
  font-weight: normal;
  font-size: 12px;
  color: #32a3ce;
  content: "\a0";
  background-color: #FAFAFA;
  border: 1px solid #c8c8c8;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
  border-radius: 0;
  display: inline-block;
  text-align: center;
  height: 16px;
  line-height: 14px;
  min-width: 16px;
  margin-right: 1px;
  position: relative;
  top: -1px;
}
.switch:checked + .lable::before {
  display: inline-block;
  content: '\f00c';
  background-color: #F5F8FC;
  border-color: #adb8c0;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
}
/* CSS3 on/off switches */
.switch + .lable {
  margin: 0 4px;
  min-height: 24px;
}
.switch + .lable::before {
  font-weight: normal;
  font-size: 11px;
  line-height: 17px;
  height: 20px;
  overflow: hidden;
  border-radius: 12px;
  background-color: #F5F5F5;
  -webkit-box-shadow: inset 0 1px 1px 0 rgba(0, 0, 0, 0.15);
  box-shadow: inset 0 1px 1px 0 rgba(0, 0, 0, 0.15);
  border: 1px solid #CCC;
  text-align: left;
  float: left;
  padding: 0;
  width: 52px;
  text-indent: -21px;
  margin-right: 0;
  -webkit-transition: text-indent .3s ease;
  -o-transition: text-indent .3s ease;
  transition: text-indent .3s ease;
  top: auto;
}
.switch.switch-bootstrap + .lable::before {
  font-family: FontAwesome;
  content: "\f00d";
  box-shadow: none;
  border-width: 0;
  font-size: 16px;
  background-color: #a9a9a9;
  color: #F2F2F2;
  width: 52px;
  height: 22px;
  line-height: 21px;
  text-indent: 32px;
  -webkit-transition: background 0.1s ease;
  -o-transition: background 0.1s ease;
  transition: background 0.1s ease;
}
.switch.switch-bootstrap + .lable::after {
  content: '';
  position: absolute;
  top: 2px;
  left: 3px;
  border-radius: 12px;
  box-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  width: 18px;
  height: 18px;
  text-align: center;
  background-color: #F2F2F2;
  border: 4px solid #F2F2F2;
  -webkit-transition: left 0.2s ease;
  -o-transition: left 0.2s ease;
  transition: left 0.2s ease;
}
.switch.switch-bootstrap:checked + .lable::before {
  content: "\f00c";
  text-indent: 6px;
  color: #FFF;
  border-color: #b7d3e5;

}
.switch-primary >.switch.switch-bootstrap:checked + .lable::before {
    background-color: #337ab7;
}
.switch-success >.switch.switch-bootstrap:checked + .lable::before {
    background-color: #5cb85c;
}
.switch-danger >.switch.switch-bootstrap:checked + .lable::before {
    background-color: #d9534f;
}
.switch-info >.switch.switch-bootstrap:checked + .lable::before {
    background-color: #5bc0de;
}
.switch-warning >.switch.switch-bootstrap:checked + .lable::before {
    background-color: #f0ad4e;
}
.switch.switch-bootstrap:checked + .lable::after {
  left: 32px;
  background-color: #FFF;
  border: 4px solid #FFF;
  text-shadow: 0 -1px 0 rgba(0, 200, 0, 0.25);
}
/* square */
.switch-square{
  opacity: 0;
  position: absolute;
  z-index: 1;
  width: 18px;
  height: 18px;
  cursor: pointer;
}
.switch-square + .lable{
  position: relative;
  display: inline-block;
  margin: 0;
  line-height: 20px;
  min-height: 18px;
  min-width: 18px;
  font-weight: normal;
  cursor: pointer;
}
.switch-square + .lable::before{
  cursor: pointer;
  font-family: fontAwesome;
  font-weight: normal;
  font-size: 12px;
  color: #32a3ce;
  content: "\a0";
  background-color: #FAFAFA;
  border: 1px solid #c8c8c8;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
  border-radius: 0;
  display: inline-block;
  text-align: center;
  height: 16px;
  line-height: 14px;
  min-width: 16px;
  margin-right: 1px;
  position: relative;
  top: -1px;
}
.switch-square:checked + .lable::before {
  display: inline-block;
  content: '\f00c';
  background-color: #F5F8FC;
  border-color: #adb8c0;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
}
/* CSS3 on/off switches */
.switch-square + .lable {
  margin: 0 4px;
  min-height: 24px;
}
.switch-square + .lable::before {
  font-weight: normal;
  font-size: 11px;
  line-height: 17px;
  height: 20px;
  overflow: hidden;
  border-radius: 2px;
  background-color: #F5F5F5;
  -webkit-box-shadow: inset 0 1px 1px 0 rgba(0, 0, 0, 0.15);
  box-shadow: inset 0 1px 1px 0 rgba(0, 0, 0, 0.15);
  border: 1px solid #CCC;
  text-align: left;
  float: left;
  padding: 0;
  width: 52px;
  text-indent: -21px;
  margin-right: 0;
  -webkit-transition: text-indent .3s ease;
  -o-transition: text-indent .3s ease;
  transition: text-indent .3s ease;
  top: auto;
}
.switch-square.switch-bootstrap + .lable::before {
  font-family: FontAwesome;
  content: "\f00d";
  box-shadow: none;
  border-width: 0;
  font-size: 16px;
  background-color: #a9a9a9;
  color: #F2F2F2;
  width: 52px;
  height: 22px;
  line-height: 21px;
  text-indent: 32px;
  -webkit-transition: background 0.1s ease;
  -o-transition: background 0.1s ease;
  transition: background 0.1s ease;
}
.switch-square.switch-bootstrap + .lable::after {
  content: '';
  position: absolute;
  top: 2px;
  left: 3px;
  border-radius: 12px;
  box-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  width: 18px;
  height: 18px;
  text-align: center;
  background-color: #F2F2F2;
  border: 4px solid #F2F2F2;
  -webkit-transition: left 0.2s ease;
  -o-transition: left 0.2s ease;
  transition: left 0.2s ease;
}
.switch-square.switch-bootstrap:checked + .lable::before {
  content: "\f00c";
  text-indent: 6px;
  color: #FFF;
  border-color: #b7d3e5;

}
.switch-primary >.switch-square.switch-bootstrap:checked + .lable::before {
    background-color: #337ab7;
}
.switch-success >.switch-square.switch-bootstrap:checked + .lable::before {
    background-color: #5cb85c;
}
.switch-danger >.switch-square.switch-bootstrap:checked + .lable::before {
    background-color: #d9534f;
}
.switch-info >.switch-square.switch-bootstrap:checked + .lable::before {
    background-color: #5bc0de;
}
.switch-warning >.switch-square.switch-bootstrap:checked + .lable::before {
    background-color: #f0ad4e;
}
.switch-square.switch-bootstrap:checked + .lable::after {
  left: 32px;
  background-color: #FFF;
  border: 4px solid #FFF;
  text-shadow: 0 -1px 0 rgba(0, 200, 0, 0.25);
}
.switch-square.switch-bootstrap + .lable::after {
    border-radius: 2px;
}
  .daterangepicker .ranges li {font-size: 12px;padding: 8px 12px;cursor: pointer;background: #000000b0;}
</style>



<div class="modal fade" id="assignDealModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel1">Assign Deal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="changeStatuSbody">
                <form action="" name="assignDealForm" id="assignDealForm">
                    @csrf
                    <input type="hidden" name='deal_id' id="assignDeal-deal_id" value="">
                    <div class="value">
                        <div class="form-group row">
                            <label for="comment" class="col-sm-2 col-form-label">Assign Deal To</label>
                            <div class="col-sm-10" id="selectContainer">
                                <div class="error" id="error_assign_to"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="comment" class="col-sm-2 col-form-label">Comment</label>
                            <div class="col-sm-10">
                                <textarea name="comment" cols="30" rows="10" class="form-control"></textarea>
                                <div class="error" id="error_comment"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-2 col-sm-10">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>



<div class="card" id="data">
    <div class="card-header p-2" id="card_head">
        <ul class="nav nav-pills">
            @foreach ($Dealstatus as $dealstatus)
                <li class="nav-item"><a class="nav-link services" href="#view"
                        style="background-color: {{dealsStatusColors($dealstatus->id)}}"
                        onclick="fetch_data(1,this.getAttribute('data-id'))" data-id="{{ $dealstatus->id }}"
                        data-toggle="tab">{{ $dealstatus->dealname }}({{ $dealsCount[$dealstatus->id] ?? 0 }})</a>
                </li>
                &nbsp;
            @endforeach
            @can('deals_all_leads_tab')
                <li class="nav-item"><a class="nav-link services" href="#view"
                        onclick="fetch_data(1,this.getAttribute('data-id'))" data-id="-1" data-toggle="tab">All
                        Leads({{ array_sum($dealsCount) }})</a></li>&nbsp;
            @endcan
            <div id="reportrange"
                style="background: transparent; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                <i class="fa fa-calendar"></i>&nbsp;
                <span class="ab"></span> <i class="fa fa-caret-down"></i>
            </div>
            <input type="hidden" id="start_date_range">
            <input type="hidden" id="end_date_range">
            <li class="nav-item search-right">

                <div>
                    @can('deals_add')

                        <a href="javascript:void(0)" onclick="addcompanydeals()" class="btn btn-primary" data-toggle="modal"
                            data-target="#createDeal">ADD DEALS</a>
                    @endcan
                </div>
                <div class="search_bar">
                    <div class="input-group" data-widget="sidebar-search">
                        <input class="form-control form-control-sidebar" id="search" type="search" placeholder="Search"
                            aria-label="Search">
                    </div>
                </div>
            </li>
        </ul>
        @can('only_assigned_deals_toggle')

        <label class="label-switch switch-success">
            <input type="checkbox" id="toggle_check" onclick="toggleSwitchDeals()" value="0"  class="switch-square switch-bootstrap status toggle_user_login" data-id="" name="" id="status">
            <span class="lable">Only Assigned to me</span></label>
            @else

            <input type="checkbox" style="display: none" id="toggle_check" onclick="toggleSwitchDeals()" value="0"  class="switch-square switch-bootstrap status toggle_user_login" data-id="" name="" id="status">
        @endcan
    </div><!-- /.card-header -->
    <div class="card-body">
        <div class="tab-content">
            <div class="tab-pane" id="profile">
                @include('admin.permissions.includes.addform')
            </div>

            <div>
            </div>
            <div class="active tab-pane" id="view">
                {{-- @include('admin.companyDeal.includes.view') --}}
            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
    </div><!-- /.card-body -->
</div>


<div class="modal fade" id="changestatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel1">Comment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="changeStatuSbody">
                <form action="" name="changestatusform" id="changestatusform" autocomplete="off">
                    @csrf

                    <input type="hidden" name='status' id="changeStatus-status" value="">
                    <input type="hidden" name='deal_id' id="changeStatus-deal_id" value="">

                    <div class="value" id="changeStatusFormFieldsContainer">


                        <div class="form-group row" id="meeting_time">
                            <label for="meeting_time" class="col-sm-2 col-form-label">Schedule Meeting Time</label>
                            <div class="col-sm-10">
                                <input type="datetime-local" class="form-control" name='meeting_time' id="meet_time">
                                <div class="error" id="error_meeting_time"></div>
                            </div>
                        </div>

                        <div class="form-group row" id="email_description">
                            <label for="contact_email" class="col-sm-4 col-form-label">Email</label>
                            <div class="col-sm-12">
                                <input type="email" class="form-control" name="contact_email" id="contact_email">
                                <div class="error" id="error_contact_email"></div>
                            </div>

                            <label for="contact_phone" class="col-sm-4 col-form-label">Phone Number</label>
                            <div class="col-sm-12">
                                <input type="phone" class="form-control" name="contact_phone" id="contact_phone">
                                <div class="error" id="error_contact_phone"></div>
                            </div>

                            <label for="contact_description" class="col-sm-4 col-form-label">Description</label>
                            <div class="col-sm-12">
                            <textarea id="contact_description" rows="3" class="form-control" name="contact_description"></textarea>
                            </div>
                        </div>
                        </div>
                        <div class="form-group row">
                            <label for="comment" class="col-sm-4 col-form-label">Comment</label>
                            <div class="col-sm-12">
                                <textarea name="comment" cols="30" rows="10" class="form-control"></textarea>
                                <div class="error" id="error_comment"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-2 col-sm-10">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="createDeal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Deals</h5>
                <button type="button" class="close" id='createDealHide' data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body " id="createDealBody">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="addComment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Comment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body " id="addCommentBody">
                <form action="" name="addCommentForm" id="addCommentForm">
                    @csrf
                    <input type="hidden" name='deal_id' id="deal_id" value="">
                    <div class="value">
                        <div class="form-group row">
                            <label for="comment" class="col-sm-2 col-form-label">Comment</label>
                            <div class="col-sm-10">
                                {{-- <textarea name="comment" id="comment" cols="30" rows="10" class="form-control"></textarea> --}}
                                <input type="hidden" id="comment" class="form-control" name="comment">
                                <div id="editor" style="border: 1px solid #ffffff42;"></div>

                                <div class="error" id="error_comment"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="offset-sm-2 col-sm-10">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"
integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA=="
crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $(document).ready(function() {
        $(".services").eq(0).addClass("active");
    });
    $(document).on('click', '.comment', function() {
        var id = $(this).attr('data-id');
        // alert(id);
        $('#deal_id').val(id);
        $('#comment_title').val('');
        $('#comment').val('');
    });

    $(document).on('submit', '#addCompany', function(e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            type: 'post',
            url: "{{ route('deals-add-company') }}",
            dataType: "JSON",
            xhr: function() {
                myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(data) {
                $(".services:first").trigger("click");
                $('#createDeal').modal('hide');

            },
            error: function(data) {
                console.log(data.responseJSON.errors);
                $.each(data.responseJSON.errors, function(id, msg) {
                    $('#error_' + id).html(msg);
                })
            }
        });
    });


    $(document).on('submit', '#addCommentForm', function(e) {
        e.preventDefault();
        var commentData = ($("#editor").html());
        $("#comment").val(commentData);
        var data = new FormData(this);
        $.ajax({
            type: 'post',
            url: "{{ route('deals-add-Comment') }}",
            dataType: "JSON",
            xhr: function() {
                myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(data) {
                $('#addComment').modal('hide');
                fetch_data(1, '');
                myEditor.setData('');
            },
            error: function(data) {
                // console.log(data.responseJSON.errors.comment);
                $.each(data.responseJSON.errors, function(id, msg) {
                    // console.log();
                    $('.error#error_' + id).html(msg);
                })
            }
        });
    });


    $(document).on('change', '#selectsatus', function() {
        oldstatus = $(this).data('status')
        $("#meeting_time").hide();
        $("#email_description").hide();

        var status = $(this).val();
        $(this).val(oldstatus);
        var deal_id = $(this).attr('data-id');
        $('#changeStatus-status').val(status);
        $('#changeStatus-deal_id').val(deal_id);
        $('#changestatusform textarea').val('');
        $('#changestatus').modal('show');
        if (status == 3) {
            $("#meeting_time").show();
            $("#sched_time").attr('name', 'meeting_time');
        }
        status = parseInt(status);
        if ([9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19].includes(status)) {
            $("#email_description").show();
            $("#sched_time").attr('name', 'email_description');
        }
        if (status == '7') {
            var reasonInputHtml =
                '<div class="form-group row" id="dealLostReasonInputContainer"> <label for="reason" class="col-sm-2 col-form-label">Reason</label> <div class="col-sm-10"><select name="deal_lost_reason_id" class="form-control" required>@foreach ($dealLostReasons as $dealLostReason)<option value="{{ $dealLostReason->id }}">{{ $dealLostReason->title }}</option>@endforeach</select> <div class="error" id="error_reason"></div></div></div>';
            $("#changeStatusFormFieldsContainer").prepend(reasonInputHtml);
        }

    });
    $(document).on('submit', '#changestatusform', function(e) {
        e.preventDefault();
        Swal.showLoading();
        var data = new FormData(this);
        $.ajax({
            type: 'post',
            url: "{{ route('deals-change-status') }}",
            dataType: "JSON",
            xhr: function() {
                myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(data) {
                $('#changestatus').modal('hide');
                $("#dealLostReasonInputContainer").remove();
                fetch_data(1, '');
                Swal.close();
                Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Change Status Successfully',
                            showConfirmButton: false,
                            timer: 1500
                        })
                        $('#changeStatusFormFieldsContainer').empty().html(data);
            },
            error: function(data) {
                Swal.close();
                Swal.fire('error', '', 'error')
                console.log(data.responseJSON.errors);
                $.each(data.responseJSON.errors, function(id, msg) {
                    $('#error_' + id).html(msg);
                })
            }
        });
    });

    function removeCompanyProject(e) {
        var id = e.getAttribute('data-id');
        swal({
            title: "Oops....",
            text: "Are You Sure You want to delete!",
            icon: "error",
            buttons: [
                'NO',
                'YES'
            ],
        }).then(function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: "{{ route('deal-remove-data') }}",
                    type: "post",
                    data: {
                        id: id
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Remove Successfully',
                            showConfirmButton: false,
                            timer: 1500
                        })
                        // $('#view').empty().html(data);
                        fetch_data(1, '');
                    }
                })
            } else {

            }
        });
    }

    function accept(e) {

        var id = e.getAttribute('data-id');
        $.ajax({
            url: "{{ route('deal-accept') }}",
            type: "post",
            data: {
                id: id
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                fetch_data(1, '');
            }
        })
    }

    $(document).on('submit', '#updateCompany', function(e) {
        e.preventDefault();
        Swal.showLoading();
        var data = new FormData(this);
        $.ajax({
            type: 'post',
            // url:"{{ url('/') }}/admin/companyDeal/UpdateDealProjectData",
            url: "{{ route('deals-add-company') }}",
            dataType: "JSON",
            xhr: function() {
                myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(data) {
                Swal.close()
                fetch_data(1, '');
                $('#createDeal').modal('hide');
            },
            error: function(data) {
                Swal.close()
                Swal.fire('error', '', 'error')
                console.log(data.responseJSON.errors);
                $.each(data.responseJSON.errors, function(id, msg) {
                    $('#error_' + id).html(msg);
                });
            }
        });
    });

    function addcompanydeals() {
        $('#page-loader').show();

        $.ajax({
            url: "{{ route('deal-add') }}",
            type: "get",
            success: function(data) {
                $('#page-loader').hide();
                $('#createDealBody').empty().html(data);
            },
            error: function(error) {
                $('#page-loader').hide();
            }
        })
    }

    function updateCompanyProject(e) {
        id = $(e).data('id');
        console.log(id)
        $('#page-loader').show();

        $.ajax({
            url: "{{ route('deal-edit') }}",
            type: "get",
            data: {
                id
            },
            success: function(data) {
                $('#page-loader').hide();
                $('#createDealBody').empty().html(data);
            },
            error: function(error) {
                $('#page-loader').hide();
            }
        })
    }



    // run on every event to refresh the page
    // function filter(start_date,end_date,active,search){
    //     // var search=document.querySelector("#search").value;
    //   $.ajax({
    //     url:"{{ url('/') }}/admin/companyDeal/GetDatewiseData",
    //     type:'get',
    //     data:{start_date:start_date,end_date:end_date,active:active,search:search},
    //     success:function(data)
    //     {
    //       $('#view').empty().html(data);
    //     }
    //   })
    // }
    // run on  change date start to end

    function toggleSwitchDeals(){
        if($("#toggle_check").val() == 1){
                    $("#toggle_check").val(0)
                }else{
                    $("#toggle_check").val(1)
                }
                fetch_data(1, '')
    }

    $(function() {
        var start = moment().subtract(moment().year() - 1947, 'years');
        var end = moment();

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            $('#start_date_range').val(start.format('YYYY-MM-DD'));
            $('#end_date_range').val(end.format('YYYY-MM-DD'));
            fetch_data(1, '');
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                    'month').endOf('month')]
            }
        }, cb);
        cb(start, end);

    });

    $(document).on('click', '.pagination a', function(event) {
        event.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        var sendurl = document.getElementsByClassName('services active')[0].getAttribute('data-id');
        fetch_data(page, sendurl, '');

    });
    document.querySelector("#search").addEventListener("keyup", (e) => {
        // var active=$('a.active.services').attr('data-id');
        fetch_data(1, '');
    });

    function fetch_data(page, active) {
        $('#page-loader').show();

        var start_date = document.querySelector('#start_date_range').value;
        var end_date = document.querySelector('#end_date_range').value;
        var active_toggle = $("#toggle_check").val();
        if (active == '') {
            var active = document.getElementsByClassName('services active')[0].getAttribute('data-id');
        }

        // page = (page='')?'1':page;
        // console.log(page)
        var search = document.querySelector("#search").value;
        // let make_url="";
        var data = {
            search: search,
            active: active,
            start_date: start_date,
            end_date: end_date,
            active_toggle: active_toggle
        };
        var make_url = "{{ url('/') }}/admin/companyDeal/search?page=" + page;
        $.ajax({
            url: make_url,
            data: data,
            success: function(data) {
                $('#view').empty().html(data);
                $('#page-loader').hide();

            },
            error: function(error) {
                $('#page-loader').hide();
            }
        });
    }

    // $(".selectpicker").selectpicker({
    //     "title": "Select Options"
    // }).selectpicker("render");

    $(document).on('click', '.assignButton', function() {
        var deal_id = $(this).attr('data-id');
        var project_id = $(this).attr('data-project-id');
        $('#assignDealModal-status').val(status);
        $('#assignDeal-deal_id').val(deal_id);
        $('#assignDealModal textarea').val('');
        $.ajax({
            type: 'get',
            url: `{{ route('get-deals-employee-list') }}?projectId=${project_id}`,
            data: {
                "status": status
            },
            xhr: function() {
                myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                console.log(data)
                $('#selectContainer').empty().html(data);
            },
            error: function(data) {
                // Swal.fire("error", '', 'Opps Something went wrong')
                console.log(data)
                $.each(data.responseJSON.errors, function(id, msg) {
                    $('#error_' + id).html(msg);
                })
            }
        });
        $('#assignDealModal').modal('show');
    });


    $(document).on('submit', '#assignDealForm', function(e) {
        e.preventDefault();
        Swal.showLoading()
        var data = new FormData(this);
        $.ajax({
            type: 'post',
            url: "{{ route('deals-assign') }}",
            dataType: "JSON",
            xhr: function() {
                myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(data) {
                Swal.close()
                Swal.fire({
                    icon: 'success',
                    title: 'Assigned Successfully',
                })

                $('#assignDealModal').modal('hide');
                fetch_data(1, '');
            },
            error: function(data) {
                Swal.close()
                $.each(data.responseJSON.errors, function(id, msg) {
                    $('#error_' + id).html(msg);
                })
            }
        });
    // $("#toggle_check").on('click', (e)=>{

    //     fetch_data(1,'');
    // })
    });

</script>

<script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/inline/ckeditor.js"></script>
<script>
    var myEditor;
    InlineEditor
        .create(document.querySelector(`#editor`))
        .then(editor => {
            console.log('Editor was initialized', editor);
            myEditor = editor;
        })
        .catch(err => {
            console.error(err.stack);
        });
    $(document).ready(function() {
        // Hiding default extrack file and dropdown buttons
        $(".ck-file-dialog-button").hide();
        $(".ck-dropdown__button").hide();
    })
</script>
