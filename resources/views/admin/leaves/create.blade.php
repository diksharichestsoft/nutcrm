<!-- Button trigger modal -->
  <!-- Modal -->

  <form id="add_leave_form"  action="" method="POST" enctype="multipart/form-data">
    <div id="dsr_item_container">
        @csrf
        <div class="form-group">
            <label for="project_id">Title</label>
            <br>
            <input type="text" class="form-control" name="subject" id="subject">
       <input type="hidden" name="reqType" value="0">
            <div class="error" id="error_subject"></div>
        </div>
        <div class="form-group">
            <label for="description" class="col col-form-label">Description</label>
              <div class="col-sm-10">
                  <input type="hidden" id="description" class="form-control" name="description">
                  <div id="editor" style="border: 1px solid #ffffff42;"></div>
              </div>
          </div>
        <div class="leave_items">

              <div class="form-group">
                <label for="leave_type" class="col col-form-label">Leave Type</label>
                <div class="col-sm-10">
                  <select name="leave_type" id="leave_type" class="form-control">
                    <option disabled selected >Select Leave type </option>
                      <option value="1">Short</option>
                      <option value="2">Half</option>
                      <option value="3">Full</option>
                  </select>
                  <div class="error" id="error_leave_type"></div>
                </div>
              </div>

              <div class="form-group">
                <label for="leave_date" class="col col-form-label">Leave Date</label>
                  <div class="col-sm-10">
                      <input type="date" id="leave_date" class="form-control bg-light" name="leave_date">
                      <div class="error" id="error_leave_date"></div>
                  </div>
              </div>

        </div>
    </div>
    <br>
        <div class="row">
          <div class="col-sm-12">
              <a href="javascript:void(0);" class="btn btn-success mr-2" onclick="appendLeaveForm(this)"
                  id="add_more_servers_child">
                  <i class="fa fa-plus" aria-hidden="true"></i> Add Leave
              </a>

              <button type="submit" class="btn btn-primary">
                  Save</button>
          </div>
      </div>

    </form>

<script>

$("#addleaveButton").on('click', function (){
  $("#addleaveModal").modal('show');
})

function appendLeaveForm(addServerFormBtn) {


var LeaveFormHtml =
    `<div class="leave_items row"><div class="col-sm-12 my-2 leave_items_subsection">
    <button onclick="$(this).parent(&quot;.leave_items_subsection&quot;).parent(&quot;.leave_items&quot;)
    .remove()" style="float:right" class="btn btn-danger remove_leave_items mt-4 mb-2"><i class="fa fa-minus"
    aria-hidden="true"></i></button></div>
               <div class="col-sm-10">
                  <select name="leave_type[]" id="leave_type" class="form-control">
                    <option disabled selected >Select Leave type </option>
                      <option value="1">Short</option>
                      <option value="2">Half</option>
                      <option value="3">Full</option>
                  </select>
                  <div class="error" id="error_leave_type"></div>
                </div><br><br><br>
                <div class="col-sm-10">
                      <input type="date" id="leave_date" class="form-control bg-light" name="leave_date[]">
                      <div class="error" id="error_leave_date"></div>
                  </div>
                </div>`;
$("#dsr_item_container").append(LeaveFormHtml);

}
var myEditor;
InlineEditor
.create( document.querySelector( '#editor' ) )
.then( editor => {
console.log( 'Editor was initialized', editor );
myEditor = editor;
} )
.catch( err => {
console.error( err.stack );
} );
$(document).ready(function (){
// Hiding default extrack file and dropdown buttons
$(".ck-file-dialog-button").hide();
$(".ck-dropdown__button").hide();
})

var objectToFormData = function(obj, form, namespace) {

var fd = form || new FormData();
var formKey;

for (var property in obj) {
    if (obj.hasOwnProperty(property)) {

        if (namespace) {
            formKey = namespace + '[' + property + ']';
        } else {
            formKey = property;
        }

        // if the property is an object, but not a File,
        // use recursivity.
        if (typeof obj[property] === 'object' && !(obj[property] instanceof File)) {

            objectToFormData(obj[property], fd, property);

        } else {

            // if it's a string or a File object
            fd.append(formKey, obj[property]);
        }

    }
}

return fd;

};
$("#add_leave_form").on('submit', function(e) {
e.preventDefault();
Swal.showLoading()
let projectServersFormData = [];
let objectToStoreValues = {};
$(".leave_items").each(function() {
    objectToStoreValues = {};
    let eachInput = $(this).find(".form-control");
    eachInput.each((e) => {
        if (e == 0) {
            objectToStoreValues[`${eachInput[e].name}`] = eachInput[e].value;
            projectServersFormData.push(objectToStoreValues);
        } else {
            objectToStoreValues[`${eachInput[e].name}`] = eachInput[e].value;
        }
    })
});
console.log(projectServersFormData);
var leaveDescription = $("#editor").html();

var data_to_post = {
    'leave_items': projectServersFormData,
    'subject': $("#subject").val(),
    'description': leaveDescription,
};
var form_data_instance = new FormData();

var form_data = objectToFormData(data_to_post, form_data_instance, 'leaves');

$("#page-loader").show();

$.ajax({
    url: "{{ route('leave-store') }}",
    type: 'post',
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    cache: false,
    contentType: false,
    processData: false,
    data: form_data,
    success: function(data) {
        Swal.close()
        Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Added Successfully',
        showConfirmButton: false,
        timer: 1500
        })
        $('#addleaveModal').modal('hide');

        $("#page-loader").hide();
        fetchLeavesData(1);
        $('.modal-backdrop').remove();
        $('body').removeClass('modal-open');
    },
    error: function(data) {
        Swal.close()
        if(data.status == 400){

            Swal.fire('error', data.responseJSON.message, 'error')
        }
        console.log('this is error');
        console.log(data);
        $("#page-loader").hide();

        $.each(data.responseJSON.errors, function(id, msg) {
            $('#error_' + id).html(msg);
        })
    }
});
})






</script>

