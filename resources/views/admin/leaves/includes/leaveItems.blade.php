<table class="table">
    <tbody>
        <tr class="bg-warning">
            <td scope="row" colspan="2" style="text-align: center">
                <h3 style="margin-bottom: 0;">
                    Leave Items({{count($thisLeave->leaveItems)}})
                </h3>
            </td>
        </tr>
    <tr>
        <td class="card-td">
        {{-- <th scope="row">Leave Items</th> --}}
        <div class="row">
            <div class="col-md-3 col-sm-6 col-12">
                <button class="btn btn-outline-success" onclick="leaveAction(0,2, {{$thisLeave->id}})" class="far fa-flag"><i class="fa fa-check"></i>Approve All</button>
                <button class="btn btn-outline-danger" onclick="leaveAction(0,3, {{$thisLeave->id}})" class="far fa-flag"><i class="fa fa-ban"></i>Reject All</button>
            </div>
        </div>
        <hr>
            <div class="row">
                @foreach ($thisLeave->leaveItems as $eachLeaveItem)
                    <div class="col-md-3 col-sm-6 col-12">
                        <div class="info-box {{($eachLeaveItem->status == 2) ? 'bg-success' : (($eachLeaveItem->status == 3) ? 'bg-danger' : '')}}">
                        <span class="info-box-icon">
                            @if ($eachLeaveItem->status != 2)
                            <button class="btn btn-success btn-sm" onclick="leaveAction({{$eachLeaveItem->id}},2)" class="far fa-flag"><i class="fa fa-check"></i></button>
                            @endif

                            @if ($eachLeaveItem->status != 3)
                            <button class="btn btn-danger btn-sm" onclick="leaveAction({{$eachLeaveItem->id}},3)" class="far fa-flag"><i class="fa fa-ban"></i></button>
                            @endif
                        </span>

                        <div class="info-box-content">
                            <span class="info-box-text"> {{$leaveTypes[$eachLeaveItem->leave_type] ?? ''}}</span>
                            <span class="info-box-number">                    {{date('d-M-Y',strtotime($eachLeaveItem->leave_date)) ?? ''}}
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                @endforeach
            </div>
        </td>
    </tr>

    </tbody>
  </table>
