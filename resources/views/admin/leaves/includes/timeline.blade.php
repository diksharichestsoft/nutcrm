
<section class="content">
    <div class="container-fluid">
      <div class="row">
                <!-- /.col -->
        <div class="col">
          <div class="card">
            <div class="card-header p-2">
              <ul class="nav nav-pills">
                  <li class="nav-item"><a class="active nav-link" href="#seotimeline" data-toggle="tab">TimeLine</a></li>
              </ul>
            <div class="card-body">
              <div class="tab-content">
               <!-- /.tab-pane -->

                <div class="tab-pane active" id="seotimeline">
                  <!-- The timeline -->
                  <div class="timeline timeline-inverse">
                    <!-- END timeline item -->
                    <!-- timeline item -->
                    @foreach($getcomment as $key=>$comment)

                    <?php $ctrtime = date('d M Y', strtotime($comment->created_at));
                    if(isset($getcomment[($key-1)]->created_at)){
                      $pretime = date('d M Y', strtotime($getcomment[($key-1)]->created_at));
                      $ctrtime = ($ctrtime!=$pretime)?$ctrtime:'';
                    }
                   ?>
                    @if(!empty($ctrtime))
                    <div class="time-label">
                      <span class="bg-danger">
                        {{$ctrtime}}
                      </span>
                    </div>
                    @endif
                    <div>
                        {{-- <i class="fas {{($comment->type!=1)?($comment->type=2)'fa-user bg-info':'fa-comments bg-warning'}}"></i> --}}
                        @if($comment->type == 1)
                        <i class="fas fa-comments bg-warning"></i>
                        @else
                            @if ($comment->type == 0)
                            <i class="fas fa-user bg-info"></i>
                            @else
                            <i class="fas fa-clock bg-success"></i>

                            @endif

                        @endif

                      <div class="timeline-item">
                          <span class="time"><i class="far fa-clock"></i> {{date('h.i A', strtotime($comment->created_at))}}</span>
                        <h3 class="timeline-header border-0"><a href="#">{{$comment->name }}</a> {{($comment->type!=1)?$comment->title:' commented on Project'}}
                        </h3>
                        <div class="timeline-body">
                          {!!$comment->comment!!}
                        </div>

                      </div>
                    </div>

                    @endforeach
                    {{$getcomment->links()}}

              </div>
              <!-- /.tab-content -->
            </div>
                <div class="tab-pane" id="devtimeline">
                  <!-- The timeline -->
                  <div class="timeline timeline-inverse">

                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>

