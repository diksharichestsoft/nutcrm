@extends('admin.layout.template')

@section('contents')
<style>
table.table.new-table-wrapper tr > th , table.table.new-table-wrapper tr > td {
    width: 50% !important;
}
td.card-td {
    width: 100% !important;
}
.table-card {
    background: #343a40;
    border-radius: 5px;
}
.table-card-box {
    text-align: center;
    padding: 10px;
}
.table-card-box p {
    margin-bottom: 0px;
}
</style>
<table class="table new-table-wrapper">
    <tbody>
      <tr>
        <th scope="row">Title</th>
        <td>{{$thisLeave->subject}}</td>
      </tr>
      <tr>
        <th scope="row">Description</th>
        <td>{!!$thisLeave->description!!}</td>
      </tr>
      <tr>
        <th scope="row">Created By</th>
        <td>{{$thisLeave->createdByUser->name}}</td>
      </tr>
      <tr>
        <th scope="row">Created At</th>
        <td>{{timestampToDateAndTimeObj($thisLeave->created_at)->time}}</td>
      </tr>
      <tr>
        <th scope="row">Created On</th>
        <td>{{timestampToDateAndTimeObj($thisLeave->created_at)->date}}</td>
    </tr>

</tbody>
</table>
<div id="leaveItemContainer">
    @include('admin.leaves.includes.leaveItems')
</div>
<h3>Comments/Feed</h3>
                    {{-- Start commment box --}}
                    <div class="container card">
                        <form action="" name="addCommentForm" id="addCommentForm">
                          @csrf
                          <input type="hidden" name='leave_id' id="leave_id" value="{{$thisLeave->id}}">
                          <div class="value">
                              <div class="form-group row"  style="border: 3px solid #ffffff42;">
                                  <div class="col">
                                      <input type="hidden" id="comment" class="form-control" name="comment">
                                      <div id="editor"></div>
                                      <div class="error" id="error_comment"></div>
                                  </div>
                              </div>
                              <div class="form-group align-items-end row">
                                  <div class="col">
                                      <button type="submit" class="btn btn-success float-right">Post Comment</button>
                                  </div>
                              </div>
                          </div>
                      </form>
                    </div> {{--End comment box--}}

                    {{-- Start comment section --}}
                    <div id="comment-section-view">
                        @include('admin.leaves.includes.timeline')
                    </div> {{--End Comment Section--}}


  <script>
      function leaveAction(leave_item_id, status, leave_id = 0){
            if(status == 2){
            message="Are you sure you want to Approve this Leave";
            title = "Approve"
            }
            else if(status == 3){
            message="Are you sure you want to Reject this Leave";
            title = "Reject"
            }
            swal({
                title: title,
                text: message,
                icon: "warning",
                buttons: [
                    'NO',
                    'YES'
                ],
}).then(function(isConfirm) {
    if (isConfirm) {
        Swal.showLoading()
        $.ajax({
            type: "post",
            url: "{{route('leave-review')}}",
            headers:
            {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {id:leave_item_id,status:status, leave_id: leave_id},
            success: function (response) {
                Swal.close()
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: response.message,
                    showConfirmButton: false,
                    timer: 1500
                })
                getLeaveItems()
            },
            error: function (){
                Swal.close()
                Swa.fire('error', response.message ?? '', 'error')
            }
        });
    }
});

}
function getLeaveItems(){
    Swal.showLoading()
    $.ajax({
            type: "post",
            url: "{{route('leave-item-refresh', $thisLeave->id)}}",
            headers:
            {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                Swal.close()
                $("#leaveItemContainer").empty().html(response)
            },
            error: function (){
                Swal.close()
                Swal.fire('error', response.message ?? '', 'error')
            }
        });


}



$(document).on('submit','#addCommentForm',function(e){
    $('#page-loader').show();
    var commentData = ($("#editor").html());
    $("#comment").val(commentData);
    e.preventDefault();
    var data = new FormData(this);
    $.ajax({
    type:'post',
    url:"{{route('leave-add-Comment')}}",
    dataType: "JSON",
    xhr: function() {
            myXhr = $.ajaxSettings.xhr();
            return myXhr;
    },
    cache: false,
    contentType: false,
    processData: false,
    data:data,
        success:function(data){
            $('#addComment').modal('hide');
            getCommentData();
            $('#page-loader').hide();
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Added Successfully',
                showConfirmButton: false,
                timer: 1500
	          })
        myEditor.setData('');
        },
        error:function(data){
        $.each(data.responseJSON.errors, function(id,msg){
            $('.error#error_'+id).html(msg);
            $('#page-loader').hide();

        })
        }
    });
});
function getCommentData(page = 1){
    $('#page-loader').show();
    $.ajax({
          url:"{{route('leave-get-timeline')}}",
          data:{id:"{{$thisLeave->id}}", page: page},
          success:function(data)
          {
              $('#comment-section-view').empty().html(data);
              $('#page-loader').hide();

          }
        });
}
</script>

<script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/inline/ckeditor.js"></script>
<script>
    var myEditor;
    InlineEditor
    .create( document.querySelector( `#editor` ) )
    .then( editor => {
        console.log( 'Editor was initialized', editor );
        myEditor = editor;
    } )
    .catch( err => {
        console.error( err.stack );
    } );
    $(document).ready(function (){
    // Hiding default extrack file and dropdown buttons
    $(".ck-file-dialog-button").hide();
    $(".ck-dropdown__button").hide();
    })

  </script>
  @endsection

