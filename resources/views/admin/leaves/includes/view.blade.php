   <!-- Main content -->


  </head>

   <div class="modal fade" id="addleaveModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add leave</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="add_dsr_modal_body" class="modal-body">
            @include('admin.leaves.create')
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

   <div class="modal fade" id="leaveDetailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Leave Detail</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="leaveDetailBody" class="modal-body">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>



    {{-- <a href="#" id="addTodoButton" class="btn btn-outline-success btn-lg" data-toggle="modal" data-target="#addleaveModal">
        <i class="fa fa-plus" aria-hidden="true"></i>New Todo
    </a> --}}
    <div class="modal fade" id="editLeaveModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel"> Edit Leave</h5>
              <button type="button" class="close" id='createDealHide' data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body " id="editLeaveBody">
            </div>
          </div>
        </div>
      </div>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!--  -->
                <div class="card-body">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width: 10px">#</th>
                          <th>Leave Review</th>
                          @can('leaves_approve_reject')
                          <th>Leave Action</th>
                          @endcan
                          <th>Subject</th>
                          <th>Employee Name</th>
                          <th>Description</th>
                        </tr>
                      </thead>
                      <tbody>
                          @forelse ($leaves as $key=> $value )
                          <tr>
                              <td>
                                {{$key+1}}
                              </td>
                              <td>
                                @if (($value->status == 2))
                                Approved By {{$value->approveByUser->where('id', $value->approved_by_user_id)->first('name')->name}}
                                @elseif(($value->status == 3))
                                Rejected By {{$value->approveByUser->where('id', $value->approved_by_user_id)->first('name')->name}}
                                @else
                                No Action
                                @endif
                              </td>
                              <td>
                              <div class="approve_by_button">
                                @can('leaves_approve_reject')
                                    @if($value->status ==  1)
                                        <button id="toggle_check_{{$key}}" onclick="approveThisLeave(this,'2')"  class="btn btn-success btn-xs text-white toggle_dsr_review" data-id="{{$value->id}}">Approve</button>
                                        <button id="toggle_check_{{$key}}" onclick="approveThisLeave(this,'3')"  class="btn btn-danger btn-xs text-white toggle_dsr_review" data-id="{{$value->id}}">Reject</button>
                                    @elseif($value->status == 2)
                                        <button id="toggle_check_{{$key}}" onclick="approveThisLeave(this,'3')"  class="btn btn-danger btn-xs text-white toggle_dsr_review" data-id="{{$value->id}}">Reject</button>
                                    @elseif($value->status == 3)
                                        <button id="toggle_check_{{$key}}" onclick="approveThisLeave(this,'2')"  class="btn btn-success btn-xs text-white toggle_dsr_review" data-id="{{$value->id}}">Approve</button>
                                    @endif
                               @endcan
                                @can('leaves_edit')
                                <button data-id="{{$value->id}}" onclick="updateleave(this)" class="btn btn-outline-success btn-xs  update" ><i class="fas fa-pencil-alt"></i></button>
                                @endcan

                                @can('leaves_view')
                                <a href=" {{route('leave-detail-view',$value->id)}}" target="_blank" class="btn btn-outline-success btn-xs  view"><i class="fas fa-eye"></i></a>
                                @endcan

                                @can('leaves_delete ')
                                <button data-id="{{$value->id}}" onclick="removeLeave(this)" class="btn btn-danger btn-xs  remove"><i class="fas fa-times"></i></button>
                                @endcan

                            </div>
                              </td>
                              <td>
                                  {{$value->subject}}
                                </td>
                              <td>
                                  {{$value->createdByUser->name}}
                                </td>
                                <td>
                                {!!$value->description!!}
                            </td>

                        </tr>
                        @empty
                        <tr>
                            <td colspan="9">
                                <h4  align="center">No Data Found</h4>
                            </td>
                        </tr>
                        @endforelse

                      </tbody>
                    </table>
                </div>

                <!--  -->
            </div>
        </div>
        {{$leaves->links()}}
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->

    <script>


    // $("#addleaveButton").on('click', function (){
    //   $("#addleaveModal").modal('show');
    //   document.querySelector("#error_subject").innerText="";
    //   document.querySelector("#error_leave_type").innerText="";
    //   document.querySelector("#error_leave_date").innerText="";
    // })
    // $("#view").html($("#add_dsr_modal_body").html())
    // initializeCreateForm()
//     $("#add_leave_form").on('submit', function (e){
//         e.preventDefault();
//         Swal.showLoading()
//         var descData = ($("#editor").html());
//         $("#description").val(descData);
//         var data = new FormData(this);
//         console.log(data);
//         $.ajax({
//             type:'post',
//             url:"{{route('leave-store')}}",
//             cache: false,
//                 contentType: false,
//                 processData: false,
//                 dataType: "JSON",
//             data : {data: data},
//         headers: {
//                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                 },
//             data:data,
//             success:function(data){
//               Swal.close()
//                 $('#addleaveModal').modal('hide');
//                 $(".modal-backdrop").hide();
//                 $("body").removeClass("modal-open");
//                 fetchLeavesData(1);
//                 Swal.fire({
//                 position: 'top-end',
//                 icon: 'success',
//                 title: 'Added Successfully',
//                 showConfirmButton: false,
//                 timer: 1500
//                 })
//             },
//             error:function(data){
//               Swal.close()
//             $.each(data.responseJSON.errors, function(id,msg){
//                 $('#error_'+id).html(msg);

//             })
//             }
//         });
//         })

$(document).on('click', '#post_pagination .pagination a', function(event){
 event.preventDefault();
 var page = $(this).attr('href').split('page=')[1];
 var filter=document.querySelector("#tabs_filter").value;
 ajax('get',`{{Route("employee-pagination")}}`,data={page:page},'#pagination_employee');
});


function approveThisLeave(e,status){
            var id = $(e).attr('data-id');
            if(status == 2){
            message="Are you sure you want to Approve this Leave";
            }
            else if(status == 3){
            message="Are you sure you want to Reject this Leave";
            }
            swal({
  title: "Oops....",
  text: message,
  icon: "warning",
  buttons: [
    'NO',
    'YES'
  ],
}).then(function(isConfirm) {
  if (isConfirm) {
    $.ajax({
                type: "post",
                url: "{{route('leave-review')}}",
                headers:
                {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {id:id,status:status},
                success: function (response) {
                  Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Action Performed Successfully',
                showConfirmButton: false,
                timer: 1500
                })
        fetchLeavesData(1);
                }
            });
  } else {

  }
});
}

function removeLeave(e)
{

  var id=e.getAttribute('data-id');
  swal({
  title: "Oops....",
  text: "Are You Sure You want to delete Leave!",
  icon: "error",
  buttons: [
    'NO',
    'YES'
  ],
}).then(function(isConfirm) {
  if (isConfirm) {
    $.ajax({
      url:"{{route('remove-leave')}}",
      type:"post",
      data:{id:id},
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
      success:function(data){
        Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Remove Successfully',
                showConfirmButton: false,
                timer: 1500
                })
        fetchLeavesData(1);
        }
    })
  } else {

  }
});
}


function updateleave(e)
{
  $('#page-loader').show();

  var id=e.getAttribute('data-id');
  $.ajax({
    url:"{{route('leave-edit')}}",
    type:"get",
    data:{id:id},
    success:function(data)
    {
       // toastr.success(response.message)
      $('#page-loader').hide();
      $('#editLeaveBody').empty().html(data);
    $("#editLeaveModal").modal('show');
      intitializeUpdate();
    },
    error:function(){
      $('#page-loader').hide();
    }
  })
}

function intitializeUpdate(){

// initializeCreateForm()

}
</script>
