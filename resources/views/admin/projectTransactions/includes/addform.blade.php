<form id="addTransaction"  method="POST" >
    @csrf
    <div class="form-group">
         <input type="hidden" class="form-control" name="parent_id" id="parent_id">
    <div class="form-group">
        <label for="exampleInputEmail1">Image</label>
        <div class="input-group">
          <div class="custom-file">
            <input type="file" class="custom-file-input" name="image"  id="exampleInputFile">
            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
          </div>
        </div>
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Payment Method</label>
        <select class="w-100 form-select form-control" name="payment_method" id="subscriptionMethod" aria-label="Default method" required="">
            <option selected="" value="">Select method</option>
            <option value="Paypal">Paypal</option>
            <option value="Payoneer">Payoneer</option>
            <option value="Bank">Bank</option>
            <option value="Others">Others</option>
        </select>
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Comment</label>
        <textarea type="number" name="comment" class="form-control" id="comment" ></textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
