<div class="card">
    <div class="card-header">
        <h3 class="card-title">Payments</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Action</th>
                    <th>Toal Amount</th>
                    <th>Project</th>
                    <th>Client Name</th>
                    <th>Company</th>
                    <th>Tax Type</th>
                    <th>Invoice</th>
                    <th>Date</th>
                    <th>Added by</th>
                    <th>Created at</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($transactions as $transaction)
                <tr>
                    <td>{{$transaction->id}}</td>
                    <td>
                        <div class="btn-group">
                          <a href="{{route('project-transaction-edit', $transaction->id)}}" target="_blank" class="btn btn-outline-success btn-xs"><i class="fas fa-pencil-alt"></i></a>
                        </div>
                  </td>
                    <td>{{$transaction->total_amount}}</td>
                    <td><a href="{{route('projects-view',$transaction->project_id)}}" target="_blank">{{$transaction->project->title}}</a></td>
                    <td>{{$transaction->client_name}}</td>
                    <td>{{$transaction->company->name}}</td>
                    <td>{{$transaction->taxType->name}}</td>
                    <td><a href="{{route('project-transaction-invoice', $transaction->id)}}" class="btn btn-success"><i class="fa fa-download"></i>Invoice</a></td>
                    <td>{{$transaction->date}}</td>
                    <td>{{$transaction->created_by}}</td>
                    <td>{{$transaction->created_at}}</td>

                </tr>
                @empty
                <tr>
                    <td colspan="9">
                        <h4  align="center">No Data Found</h4>
                    </td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
</div>
<!-- Modal -->



<script>
    $(document).on('click','.payment-modal-toggle',function(e){
        e.preventDefault();
        $("#parent_id").val($(this).data('id'));
    });
$(document).on('submit','#addSubscription',function(e){
    e.preventDefault();

    var data = new FormData(this);
        $.ajax({
            type:'post',
            url:"{{route('add-subscription-payment')}}",
            dataType: "JSON",
        xhr: function() {
                myXhr = $.ajaxSettings.xhr();
                return myXhr;
        },
        cache: false,
        contentType: false,
        processData: false,
        data:data,
            success:function(data){
                $('.active.services').trigger('click');
                // window.location.reload();


            },
            error:function(data){
            }
        });
    });


</script>

