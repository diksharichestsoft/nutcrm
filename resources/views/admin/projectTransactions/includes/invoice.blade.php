<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rs Receipt</title>
 </head>
 <style>
@font-face {
  font-family: 'Droid';
  font-style: normal;
  font-weight: normal;
  src: url('dompdf/lib/fonts/DroidSansFallback.ttf') format('truetype');
}

* {
	margin: 0;
	box-sizing: border-box;
}

html {
	padding-top: 10px;
	background: #e6e6e6;
	text-align: center;
}

body {
	width: 730px;
	font-family: 'Droid', 'Helvetica';
	font-size: 14px;
	padding: 30px 40px;
	text-align: left;
	margin: 0 auto;
	background-color: #FFF;
}

.page_break { page-break-before: always; }

h2 {
	margin-top: 8px;
	margin-bottom: 15px;
}

table {
	margin: 8px 0;
}

table th, td {
	padding: 8px 14px 8px 14px;
}

.text-center {
	text-align: center;
}

img {
	max-width: 100%;
}
 </style>
 <body>
    <html>
        <body style="background-color:#e2e1e0;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;">
          <table style="max-width:670px;margin:50px auto 0px auto;background-color:#fff;padding:50px 50px 0px 50px;">
            <thead>
              <tr>
                <th style="text-align:left;">
                    {{-- <a href="javascript:void(0)">
                        <img style="max-width: 150px;" src="https://richestsoft.com/wp-content/uploads/2018/02/logo.png" alt="logo">
                        </a> --}}
                    </th>
                <th style="text-align:right;font-weight:700;text-transform:uppercase;">Original for recipient</th>
              </tr>
              <tr>
                  <th></th>
                <th style="text-align:right;font-weight:700;text-transform:uppercase;">export invoice</th>
              </tr>
              <tr>
                <th></th>
              <th style="text-align:right;font-weight:400;">Invoice no 104100</th>
            </tr>
            </thead>
            <!--  -->
            <tbody>
                <tr>
                    <td style="height:35px;"></td>
                  </tr>
                <tr>
                    <td style="text-align:left;font-weight:700;text-transform:uppercase;">gstin:Static</td>
                    <td style="text-align:right;font-weight:400;">Invoice Date: {{date('d-m-Y', strtotime($created_at))}}</td>
                  </tr>
                  <tr>
                    <td style="text-align:left;font-weight:700;">{{$company["name"]}}</td>
                    <td style="text-align:right;font-weight:400;">Place of Supply Outside India</td>
                  </tr>
                  <!--  -->
                  <tr>
                    <td style="height:30px;"></td>
                  </tr>
                  <tr>
                    <tr>
                        <td style="text-align:left;font-weight:400;">{{$company["name"]}}</td>
                      </tr>
                      <tr>
                        <td style="text-align:left;font-weight:400;"><p>{!!($from_address !=="") ? $from_address : "F-34, 2nd Floor, Phase 8, Industrial Area, Shaibzada Ajit Singh Nagar, Punjab 160071, India"!!}</p></td>
                      </tr>
                  </tr>
                  <!--  -->
                  <tr>
                    <td style="height:30px;"></td>
                </tr>
              <!--  -->
              <tr>
                <td style="width:50%;text-align:left;font-weight:400;text-transform:uppercase;color:#a3a3a3;">bill to:</td>
                <td style="width:50%;text-align:left;font-weight:400;text-transform:uppercase;color:#a3a3a3;">ship no:</td>
              </tr>
              <tr>
                  <td>{!!str_replace("&nbsp;", "", $bill_to_address)!!}</td>
                  <td>{!!str_replace("&nbsp;", "", $ship_to_address)!!}</td>
              </tr>
               <!--  -->
               <tr>
                <td style="height:30px;"></td>
              </tr>
            </tbody>
          </table>
        <table style="max-width:670px;margin:0px auto 10px;width:100%;background-color:#fff;padding:50px;">
            <thead>
                <tr>
                    <th style="text-align:left;background: #000;color: #fff;padding: 10px;width:25%">Description</th>
                    <th style="text-align:center;background: #000;color: #fff;padding: 10px;width:25%">Qty</th>
                    <th style="text-align:center;background: #000;color: #fff;padding: 10px;width:25%">Rate</th>
                    <th style="text-align:right;background: #000;color: #fff;padding: 10px;width:25%">Amount</th>
                </tr>
            </thead>
            <tbody>
                <!--  -->
                @foreach ($items as $item)
                <tr>
                    <td style="text-align:left;">{{$item["description"]}}</td>
                    <td style="text-align:center;">{{$item["quantity"]}}</td>
                    <td style="text-align:center;">{{getCurrencySymbol($currency)}} {{$item["rate"]}}</td>
                    <td style="text-align:right;">{{getCurrencySymbol($currency)}} {{$item["amount"]}}</td>
                </tr>
                @endforeach
                <!--  -->
                <tr>
                    <td style="height:30px;"></td>
                  </tr>
                <!--  -->
                <tr>
                    <td></td>
                    <td></td>
                    <td style="color:#474747;text-align:right;">Sub Total</td>
                    <td style="text-align:right;">{{getCurrencySymbol($currency)}} {{$total_amount - $total_tax_amount - $total_discount_amount}}</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td style="color:#474747;text-align:right;">Discount</td>
                    <td style="text-align:right;">{{getCurrencySymbol($currency)}} {{$total_discount_amount}}</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td style="color:#474747;text-align:right;">Tax</td>
                    <td style="text-align:right;">{{getCurrencySymbol($currency)}} {{$total_tax_amount}}</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td style="color:#474747;text-align:right;">Total</td>
                    <td style="text-align:right;">{{getCurrencySymbol($currency)}} {{$total_amount}}</td>
                </tr>
                {{-- <tfooter> --}}
                    <tr>
                        <td style="height:30px;"></td>
                      </tr>
                    <tr>
                      <td colspan="6">
                        <p style="line-height:10px;padding-bottom:0px;color:#474747;display:block;margin:0 0 30px 0;">Bank Details</p>
                        <p style="font-size: 15px;line-height:25px;margin:0px;">A/C Name : RICHESTSOFT</p>
                        <P style="font-size: 15px;line-height:25px;margin:0px;">A/C number:</P>
                        <p style="font-size: 15px;line-height:25px;margin:0px;text-transform: uppercase;">swift:kkbkinbb</p>
                        <p style="font-size: 15px;line-height:25px;margin:0px;">IFSC Code:</p>
                        <p style="font-size: 15px;margin:0px;">Bank Name:Kotak Mahindra Bank Branch name: S.C.F 58,Phase 11, Shaibzada Ajit Sigh Nagar, Punjab</p>
                        <p style="font-size: 15px;line-height:25px;margin:0px;">160062 City name : Mohali</p>
                      </td>
                    </tr>
                    <tr>
                        <td colspan="6"><p style="font-size: 15px;margin:0px;margin-top: 20px;">Note: Supply meant for export of services without payment of tax under LUT.</p></td>
                    </tr>
                  {{-- </tfooter> --}}
            </tbody>
        </table>
        </body>
        </html>
</body>
</html>
