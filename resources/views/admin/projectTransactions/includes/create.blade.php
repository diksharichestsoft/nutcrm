@extends('admin.layout.template')
@section('contents')
<style>
    .ck-editor__editable_inline{
        min-height: 150px
    }
</style>

<div class="card">
    <div class="card-header">New Payment</div>
    <div class="card-body">
        <form id="add_payment_form" action="" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-sm-3">
                    <label for="company_id" class="col-form-label d-block">Company</label>

                    <select name="company_id" required id="company_id" class="selectpicker" data-live-search="true" onchange="fillChangeToAddress()" required>
                        <option selected disabled>Select Company</option>
                        @foreach ($companies as $key=>$company)
                            <option value="{{$company->id}}" {{$company->id == 1 ? 'selected' : ''}}>{{$company->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-3">
                    <label for="project_id" class="col-form-label d-block">Project </label>

                    <select name="project_id" id="project_id" onchange="$('#client_name').val($(this).find(':selected').data('client_name'))" class="selectpicker" data-live-search="true" required>
                        @foreach ($allProjects as $key=>$eachProject)
                            <option {{($eachProject->id != $thisProject->id) ?: 'selected' }} data-client_name ="{{$eachProject->client_name ?? ''}}" value="{{$eachProject->id}}">{{$eachProject->title}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-3">
                    <label for="client_name" class="col-form-label d-block">Client Name</label>

                    <input type="text" required class="form-control" value="{{$thisProject->client_name ?? ''}}" id="client_name" name="client_name"></input>
                    <div class="error_client_name" id="error_date"></div>

                </div>

                <div class="col-sm-3">
                    <label for="date" class="col-form-label d-block">Select Date</label>

                    <input type="date" id="date" class="form-control" required="" value="{{date('Y-m-d')}}" name="date">
                    <div class="error" id="error_date"></div>
                </div>
            </div>
            <br>
            <hr>
            <br>
            <div class="row">
                <div class="col-md-4">
                    <label for="terms" class="col-form-label">From</label>
                    <div style="color: black !important; min-height: 100px">
                        <div id="fromAddress"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="terms" class="col-form-label">Bill To</label>
                    <div style="color: black !important; min-height: 100px">
                        <div id="billToAddress"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="terms" class="col-form-label">Ship To</label>
                    <div style="color: black !important; min-height: 100px">
                        <div id="shipToAddress"></div>
                    </div>
                </div>
            </div>

            <br>
            <br>

                <div>
                    <div class="row items-label bg-white my-2">
                        <div class="col-sm-3 p-1 text-bold">
                            <span class="text-dark pl-2">Item Description</span>
                        </div>

                        <div class="col-sm-3 p-1 text-bold">
                            <span class="text-dark pl-2">Quantity</span>
                        </div>

                        <div class="col-sm-3 p-1 text-bold">
                            <span class="text-dark pl-2">Rate</span>
                        </div>


                        <div class="col-sm-3 p-1 text-bold">
                            <span class="text-dark pl-2">Amount</span>
                        </div>
                    </div>
                </div>

                {{-- Payment Items --}}
                <div id="payment_item_container" class="mb-3">
                    <div class="payment_items row">
                        <div class="form-group col-sm-3">
                            <input required type="text" name="description" class="form-control" id="description" placeholder="description">

                        </div>
                        <div class="form-group col-sm-3">
                            <input required class="form-control quantity" onchange="calculateAmount(this)" min="0" value="0" type="number" name="quantity" placeholder="Enter Quantity">
                        </div>

                        <div class="form-group col-sm-3">
                            <input required class="form-control rate" onchange="calculateAmount(this)" min="0" value="0"  type="number" name="rate" placeholder="Rate">
                        </div>

                        <div class="form-group col-sm-3">
                            <input required class="form-control amount" value="0" onchange="calculateAmount(this)" type="number" min="0" name="amount" placeholder="amount">
                        </div>
                    </div>
                </div> {{--End Payment Items--}}

                {{-- Add Payment Button --}}
                <div class="row">
                    <a href="javascript:void(0);" class="btn btn-success mr-2" onclick="addItemInPaymentForm(this)"
                        id="add_more_servers_child">
                        <i class="fa fa-plus" aria-hidden="true"></i> Add Item
                    </a>
                </div> {{--End Add Payment Button--}}

            {{-- <div class="row form-group">
                <a class="btn btn-success" href="javascript:void(0);" onclick="appendDiscount(this)" ><i class="fa fa-plus" aria-hidden="true"></i>Discount</a>
            </div> --}}


            <div class="row">

                <div class="col-md-6 p-4">

                    {{-- Discounts --}}
                    <div id="payment_discount_container" class="mb-3 row">

                        <div class="w-100">
                            <div class="form-group row">
                                <label for="currency" class="form-label">Currency</label>
                                <select  name="currency" id="currency"  class="selectpicker form-control" data-live-search="true" required>
                                    @foreach ($currencies as $key=>$currency)
                                        <option {{($key != "USD") ?: 'selected' }} value="{{$key}}">{{$key}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group row">
                                <label for="tax_type_id" class="form-label">Tax Type</label>
                                <select name="tax_type_id" onchange="changeTotalAmount()" id="tax_type_id"  class="selectpicker form-control" data-live-search="true" required>
                                    <option selected disabled>Select Tax</option>
                                    @foreach ($taxTypes as $key=>$eachTaxType)
                                        <option {{($key !=0) ?: 'selected' }}  value="{{$eachTaxType->id}},{{$eachTaxType->value}}">{{$eachTaxType->name}} ({{$eachTaxType->value}}%)</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="payment_discounts">
                                <div class="form-group row">
                                    <label for="terms" class="form-label">Discount</label>
                                    <select onclick="hideShowValueOfDiscount(this)" required name="type" class="form-control" id="discount_select">
                                        <option selected value="0">No Discount</option>
                                        <option value="FLAT">Flat</option>
                                        <option value="%">Percent(%)</option>
                                    </select>
                                </div>

                                <div class="form-group row discount-value-row" id="discount_row_first" style="display:none;">
                                    <label for="terms" class="form-label">Discount Value</label>
                                    <input required class="form-control discount-input" min="00" value="0" onchange="calculateAmount(this)" id="discount_input"  type="number" name="value" placeholder="Value">
                                </div>
                            </div>
                        </div>
                    </div> {{--End Discounts--}}


                    {{-- Total SubTotal Table --}}
                    <div class="form-group">
                        <table class="table">
                            <thead>
                            </thead>
                            <tbody>
                              <tr>
                                <th>Subtotal</th>
                                <td><span id="subtotal_amount_by_calculation">0</span></td>
                              </tr>
                              <tr>
                                <th>Discount</th>
                                <td><span id="discount_amount_by_calculation">0</span></td>
                              </tr>
                              <tr>
                                <th>Total Tax</th>
                                <td><span id="tax_amount_by_calculation">0</span></td>
                              </tr>
                              <tr>
                                <th>Total</th>
                                <td><span id="total_amount_by_calculation">0</span></td>
                              </tr>
                            </tbody>
                          </table>
                    </div> {{--End Total SubTotal Table--}}

                </div>

            {{-- Right side Notes,Terms,Details  --}}
                <div class="col-md-6 p-4">
                    <div class="row">
                        <div class="col-md-12">

                            <label for="terms" class="col-form-label">Notes</label>
                            <div style="color: black !important">
                                <div id="notes"></div>
                            </div>
                          </div>

                          <div class="col-md-12 form-group">
                            <label for="details" class="col-form-label">Details</label>
                            <div  style="color: black !important">
                                <div id="details"></div>
                            </div>
                        </div>

                        <div class="col-md-12 form-group">
                            <label for="details" class="col-form-label">Terms</label>
                            <div  style="color: black !important">
                                <div id="terms"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> {{--End Right Side Notes,terms,details--}}

            <button type="submit" class="btn btn-primary">
                Save</button>
        </form>
    </div>
</div>

<script>
</script>

<script>
    function removeLeadingZeros(str){
        // Regex to remove leading
            // zeros from a string
            const regex = new RegExp("^0+(?!$)",'g');

            // Replaces the matched
            // value with given string
            return str.replaceAll(regex, "");


    }

    function addItemInPaymentForm(button) {
        var paymentFromHTML =
            `<div class="payment_items row">
                <div class="form-group col-sm-3">
                    <input required type="text" class="form-control" name="description" id="description" placeholder="description">

                </div>
                <div class="form-group col-sm-3">
                    <input  type="number" value="0" min="0"  onchange="calculateAmount(this)" required class="form-control quantity" name="quantity" placeholder="Enter Quantity">
                </div>

                <div class="form-group col-sm-3">
                    <input type="number" value="0" min="0"  onchange="calculateAmount(this)" required class="form-control rate" name="rate"  placeholder="Rate">
                </div>

                <div class="form-group col-sm-2">
                    <input  type="number" value="0" min="0" onchange="calculateAmount(this)"  required class="form-control amount" name="amount" placeholder="amount">
                </div>

                <div class="form-group col-sm-1">
                    <button style="float:right" onclick="$(this).parent().parent('.payment_items').remove()" class="btn btn-danger remove_payment_item"><i class="fa fa-trash" aria-hidden="true"></i></button>
                </div>

            </div>`;
        $("#payment_item_container").append(paymentFromHTML);
}

    // If in future there is need for multiple discounts, uncomment this and add button to add

    // function appendDiscount(button) {
    //     var discountHtml =
    //             `<div class="payment_discounts row col-sm-4">
    //                 <div class="form-group row">
    //                     <label for="terms" class="form-label col">Title/Desc</label>
    //                     <input required type="text" name="title" class="form-control col" placeholder="title">
    //                 </div>
    //                 <div class="form-group row">
    //                     <label for="terms" class="form-label col">Discount Type</label>
    //                     <select onchange="hideShowValueOfDiscount(this)" required name="type" class="form-control col">
    //                         <option selected disabled>Discount Type</option>
    //                         <option value="FLAT">Flat</option>
    //                         <option value="%">Percent(%)</option>
    //                     </select>
    //                 </div>

    //                 <div class="form-group row discount-value-row">
    //                     <label for="terms" class="form-label col">Value</label>
    //                     <input required class="form-control col" min="0" value="0"  type="number" name="value" placeholder="Value">
    //                 </div>
    //             </div>`;
    //     $("#payment_discount_container").append(discountHtml);

    // }
$("#add_payment_form").on("submit", function (e) {
        e.preventDefault();

        // Get items
        let paymentItemsFormData = [];
        let objectToStoreValues = {};
        $(".payment_items").each(function () {
            objectToStoreValues = {};
            let eachInput = $(this).find(".form-control");
            eachInput.each((e) => {
                if (e == 0) {
                    objectToStoreValues[`${eachInput[e].name}`] =
                        eachInput[e].value;
                    paymentItemsFormData.push(objectToStoreValues);
                } else {
                    objectToStoreValues[`${eachInput[e].name}`] =
                        eachInput[e].value;
                }
            });
        });

        // Get Discount
        let paymentDiscountFormData = [];
        $(".payment_discounts").each(function () {
            objectToStoreValues = {};
            let eachInput = $(this).find(".form-control");
            eachInput.each((e) => {
                if (e == 0) {
                    objectToStoreValues[`${eachInput[e].name}`] =
                        eachInput[e].value;
                    paymentDiscountFormData.push(objectToStoreValues);
                } else {
                    objectToStoreValues[`${eachInput[e].name}`] =
                        eachInput[e].value;
                }
            });
        });


        $("#page-loader").show();

        $.ajax({
            type: "post",
            url: "{{route('project-transaction-store')}}",
            dataType: "JSON",
            xhr: function () {
                myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },

            data: {
                items: paymentItemsFormData,
                discounts: paymentDiscountFormData,
                project_id: $("#project_id").val(),
                client_name: $("#client_name").val(),
                from_address: fromAddressEditor.getData(),
                bill_to_address: billToAddressEditor.getData(),
                ship_to_address: shipToAddressEditor.getData(),
                notes: notesEditor.getData(),
                terms: termsEditor.getData(),
                details: detailsEditor.getData(),
                date: $("#date").val(),
                currency: $("#currency").val(),
                tax_type_id: $("#tax_type_id").val(),
                company_id: $("#company_id").val(),
                discount_type: $("#discount_select").val(),
                discount_value: $("#discount_input").val(),
            },
            success: function (data) {
                $("#addPaymentModal").modal("hide");
                $("#page-loader").hide();
                Swal.fire({
                icon: 'success',
                title: 'Added Successfully',
                confirmButtonText: 'View/Download',
                showConfirmButton: true,
                }).then(()=>{
                    window.location.href = `{{url('/admin/transaction/invoice/download')}}/${data.id}`;
                })
            },
            error: function (data) {
                console.log("this is error");
                console.log(data);
                if(data.status == 422){
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'Check the missed form fields',
                    showConfirmButton: true,
                })
                }
                if(data.status != 422){
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'Error encountered',
                    showConfirmButton: true,
                })
                }
                $("#page-loader").hide();
                $.each(data.responseJSON.errors, function (id, msg) {
                    $("#error_" + id).html(msg);
                });
            },
        });
    });

function calculateAmount(input){
    $(input).val(removeLeadingZeros($(input).val()))
    let thisInputValue = parseFloat($(input).val()).toFixed(2);
    if(thisInputValue== "") $(input).val(0);

    // if(!isNaN(input.value) && input.value.length === 1) {
    //   input.value = '0' + input.value;
    // }
    // var quantity = $(input).parent(".payment_items").children('.quantity').val();
    // var rate = $(input).parent(".payment_items").children('.rate').val();

    let quantityInput = $($(input).parent().parent().children()[1]).children();;
    let rateInput = $($(input).parent().parent().children()[2]).children();
    let amountInput = $($(input).parent().parent().children()[3]).children();

    let quantity = $(quantityInput).val();
    let rate = $(rateInput).val();
    amount = parseFloat(quantity).toFixed(2) * parseFloat(rate).toFixed(2);
    $(amountInput).val(amount);
    changeTotalAmount();
}

// $(".discount-input").on('change', (this)=>{
//     changeTotalAmount()
// })

function changeTotalAmount(){
    /*  Function is made assuming
        there might be need for multiple disounts too,
        for just uncomment the disoucnt
        function and the add discount button
    */

    // All inputs of total amount in itmes
    var amountInputs = $(".amount");
    let subTotal = 0;
    subTotal = + subTotal;
    let discount = 0;
    let taxAmount = 0;

    // Looping throught all input of item amount to get the subTotal
    for(var i = 0; i < amountInputs.length; i++){
        subTotal= subTotal+ Number($(amountInputs[i]).val());
    }
    subTotal = parseFloat(subTotal).toFixed(2)

    //Setting Sub Total
    $("#subtotal_amount_by_calculation").html(subTotal)

    // Starting code for calculating disocunts
    var discountInputs = $(".discount-input")
    allDiscountContainer = $("#payment_discount_container")

    // Array container all containers of disounts
    discountContainerArr = $(allDiscountContainer).children().children(".payment_discounts")
    let discountTypeValueArr = [];

    // Making array of objects of each discount with its type and value
    for(var i = 0; i < discountContainerArr.length; i++){
        typeOfThisDiscount = $($($(discountContainerArr[i]).children(".row")[0]).children(".form-control")[0]).val();
        valueOfThisDiscount = $($($(discountContainerArr[i]).children(".row")[1]).children(".form-control")[0]).val();
        discountTypeValueArr.push({typeOfThisDiscount, valueOfThisDiscount});
    }
    // Substrating discount  from subTotal in case of Flat and percentage disocunt accordingly
    discountToSubtract = 0;
    discountTypeValueArr.forEach(element => {
        if(element.typeOfThisDiscount == "FLAT"){
            // Flat will be subtracted normall
            discountToSubtract+= parseFloat(element.valueOfThisDiscount).toFixed(2)
        }
        if(element.typeOfThisDiscount == "%"){
            // % will be subtracted on whole percentage
            percentageOfDiscount = parseFloat(element.valueOfThisDiscount).toFixed(2)
            discountToSubtract += subTotal * (percentageOfDiscount/100);
        }
        total = subTotal - discountToSubtract
    });

    // Calculating Tax
    let taxTypeVar = $("#tax_type_id").val();
    let taxPercentage = parseInt((taxTypeVar.split(","))[1]);
    let taxTypeId = parseInt((taxTypeVar.split(","))[0]);

    // if the tax is cgst
    if(taxTypeId == 6){taxPercentage *= 2}
    //

    taxAmount = total * (taxPercentage / 100);
    total = total + taxAmount.toFixed(2);

    $("#discount_amount_by_calculation").html(`${parseFloat(discountToSubtract).toFixed(2)}`)
    $("#tax_amount_by_calculation").html(`${taxPercentage}%/  ${parseFloat(taxAmount).toFixed(2)}`)
    $("#total_amount_by_calculation").html(parseFloat(total).toFixed(2))
}
</script>


{{-- CK Editor --}}
<script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/classic/ckeditor.js"></script>
<script>
    var notesEditor;
    ClassicEditor
    .create( document.querySelector( `#notes` ) )
    .then( editor => {
        console.log( 'Editor was initialized', editor );
        notesEditor = editor;
    } )
    .catch( err => {
        console.error( err.stack );
    } );
    var termsEditor;
    ClassicEditor
    .create( document.querySelector( `#terms` ) )
    .then( editor => {
        console.log( 'Editor was initialized', editor );
        termsEditor = editor;
    } )
    .catch( err => {
        console.error( err.stack );
    } );
    var detailsEditor;
    ClassicEditor
    .create( document.querySelector( `#details` ) )
    .then( editor => {
        console.log( 'Editor was initialized', editor );
        detailsEditor = editor;
    } )
    .catch( err => {
        console.error( err.stack );
    } );
    var fromAddressEditor;
    ClassicEditor
    .create( document.querySelector( `#fromAddress` ) )
    .then( editor => {
        console.log( 'Editor was initialized', editor );
        fromAddressEditor = editor;
    } )
    .catch( err => {
        console.error( err.stack );
    } );
    var billToAddressEditor;
    ClassicEditor
    .create( document.querySelector( `#billToAddress` ) )
    .then( editor => {
        console.log( 'Editor was initialized', editor );
        billToAddressEditor = editor;
    } )
    .catch( err => {
        console.error( err.stack );
    } );
    var shipToAddressEditor;
    ClassicEditor
    .create( document.querySelector( `#shipToAddress` ) )
    .then( editor => {
        console.log( 'Editor was initialized', editor );
        shipToAddressEditor = editor;
    } )
    .catch( err => {
        console.error( err.stack );
    } );


    $(document).ready(function (){
    // Hiding default extrack file and dropdown buttons
    $(".ck-file-dialog-button").hide();
    $(".ck-dropdown__button").hide();
    })

    function hideShowValueOfDiscount(e){
            if($(e).val()==0){
            $($(e).parent().parent().children(".discount-value-row")[0]).css("display", "none")
                $(e).closest(".discount-value-row").css("display", "none")
            }else{
            $($(e).parent().parent().children(".discount-value-row")[0]).css("display", "block")
                $(e).closest(".discount-value-row").css("display", "block")
            }
        }


    $(document).ready(()=>{

        let company_address_obj = {};
        @foreach ($companies as $eachCompany)
                company_address_obj = {
                    ...company_address_obj,
                    {{$eachCompany->id}} : `{!!$eachCompany->company_address ?? ''!!}`,
                }
        @endforeach
        function fillChangeToAddress(){
            companyId = $("#company_id").val();
            fromAddressEditor.setData(company_address_obj[companyId])
        }
        fillChangeToAddress();


        let project_ship_to_address_obj = {};
        @foreach ($allProjects as $eachProject)
                project_ship_to_address_obj = {
                    ...project_ship_to_address_obj,
                    {{$eachProject->id}} : `{!!$eachProject->client_shipping_address ?? ''!!}`,
                }
        @endforeach
        console.log(project_ship_to_address_obj)

        function fillBillToAddress(){
            project_id = $("#project_id").val();
            shipToAddressEditor.setData(project_ship_to_address_obj[project_id])
            billToAddressEditor.setData(project_ship_to_address_obj[project_id])
            console.log(project_ship_to_address_obj[project_id])
        }
        fillBillToAddress();

        $("#company_id").on('change', ()=>{
            fillChangeToAddress();
        })
        $("#project_id").on('change', ()=>{
            fillBillToAddress();
        })

    })
</script>

@endsection
