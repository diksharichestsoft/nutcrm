<style>
    .daterangepicker .ranges li {font-size: 12px;padding: 8px 12px;cursor: pointer;background: #000000b0;}
  </style>
  <div class="card" id="data">
    <div class="card-header p-2" id="card_head">
      <ul class="nav nav-pills">
          <div id="reportrange" style="background: transparent; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
            <i class="fa fa-calendar"></i>&nbsp;
        <span class="ab"></span> <i class="fa fa-caret-down"></i>
    </div>
    <input type="hidden" id="start_date_range" >
    <input type="hidden" id="end_date_range">
    @if ($thisProject != null)
    <li style="align-self: center;">
        <h3>
            Payments Of <a href="{{route('projects-view', $thisProject->id)}}"  target="_blank">{{$thisProject->title}}</a>
        </h3>
    </li>
    @endif
        <li class="nav-item search-right">
          <div>
              <a href="{{route('project-transaction-create', $thisProject->id)}}" target="_blank" class="btn btn-primary" >New Payment</a>
          </div>
         <div class="search_bar">
            <div class="input-group" data-widget="sidebar-search">
             <input class="form-control form-control-sidebar" id="search" type="search" placeholder="Search" aria-label="Search">
            </div>
         </div>
        </li>
      </ul>
    </div><!-- /.card-header -->
    <div class="card-body">
      <div class="tab-content">
        <div class="tab-pane" id="profile">
        </div>

        <div class="active tab-pane" id="view">
           {{-- @include('admin.projectTransactions.includes.view') --}}
          </div>
        <!-- /.tab-pane -->
      </div>
      <!-- /.tab-content -->
    </div><!-- /.card-body -->
</div>

{{-- Created Modal --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/inline/ckeditor.js"></script>

<script>
      const addPayments = ()=>{
    $("#addPaymentModal").modal('show')
  }



$(document).ready(()=>{
    $(function() {
        var start = moment().subtract(moment().year()- 1947, 'years');
    var end = moment();
    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $('#start_date_range').val(start.format('YYYY-MM-DD'));
        $('#end_date_range').val(end.format('YYYY-MM-DD'));
        fetch_transaction_data(1,'');
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);
    cb(start, end);

    });
    // fetch_transaction_data()
    $(document).on('click','.services',function(){
        const id = $(this).data('id')
        console.log(id)
        fetch_transaction_data(id)
    });
    document.querySelector("#search").addEventListener("keyup",(e)=>{
      fetch_transaction_data(1,'');
  });
    function fetch_transaction_data(id){
        var start_date=document.querySelector('#start_date_range').value;
        var end_date=document.querySelector('#end_date_range').value;
        var search=document.querySelector("#search").value;

        $("#page-loader").show();
        var id = {{$thisProject->id ?? "all"}};
        data = {start_date, end_date, id, search}
        $.ajax({
                type:'get',
                url:"{{route('project-transaction-search')}}",
                data:data,
                success:function(data){
                    $('#view').html(data);
                    $("#page-loader").hide();
                },
                error:function(data){

                    $("#page-loader").hide();
                }
            });
    }




})
</script>
