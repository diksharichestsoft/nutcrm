   <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!--  -->
                <div class="card-body">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                            <th>Action</th>
                        <th>Id</th>
                          <th>CompanyId</th>
                          <th>Fname</th>
                          <th>website_url</th>
                          <th>Phone</th>
                          <th>Email</th>
                          <th>Budget</th>
                          <th>CompanyName</th>
                          <th>Message</th>
                          <th>link</th>
                          <th>Interested</th>
                          <th>Skype_whatsapp</th>
                          <th>token</th>
                          <th>status</th>
                          <th>Country</th>
                          <th>city</th>
                          <th>Ip</th>
                          <th>Utm_campaign</th>
                          <th>Utm_medium</th>
                          <th>Utm_source</th>
                          <th>Utm_term</th>
                          <th>created_at</th>
                          <th>updated_at</th>
                        </tr>
                      </thead>
                      <tbody>
                          @forelse ($leaves as $data )
                          <tr>
                            <td><a class="btn btn-success" href="{{route('DirectLeadLocal-delete', $data["id"])}}">Delete</a></td>
                            <td>{{$data['id']}}</td>
                              <td>{{$data['company_id']}}</td>
                              <td>{{$data['fname']}}</td>
                              <td>{{$data['website_url']}}</td>
                              <td>{{$data['phone']}}</td>
                              <td>{{$data['email']}}</td>
                              <td>{{$data['budget']}}</td>
                              <td>{{$data['company_name']}}</td>
                              <td>{{$data['message']}}</td>
                              <td>{{$data['link']}}</td>
                              <td>{{$data['interested']}}</td>
                              <td>{{$data['skype_whatsapp']}}</td>
                              <td>{{$data['token']}}</td>
                              <td>{{$data['status']}}</td>
                              <td>{{$data['country']}}</td>
                              <td>{{$data['city']}}</td>
                              <td>{{$data['ip']}}</td>
                              <td>{{$data['utm_campaign']}}</td>
                              <td>{{$data['utm_medium']}}</td>
                              <td>{{$data['utm_source']}}</td>
                              <td>{{$data['utm_term']}}</td>
                              <td>{{$data['created_at']}}</td>
                              <td>{{$data['updated_at']}}</td>




                        </tr>
                        @empty
                        <tr>
                            <td colspan="9">
                                <h4  align="center">No Data Found</h4>
                            </td>
                        </tr>
                        @endforelse

                      </tbody>
                    </table>
                </div>

                <!--  -->
            </div>
        </div>
        {{$leaves->links()}}
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->

    <script>


    $("#addleaveButton").on('click', function (){
      $("#addleaveModal").modal('show');
      document.querySelector("#error_subject").innerText="";
      document.querySelector("#error_leave_type").innerText="";
      document.querySelector("#error_leave_date").innerText="";
    })


$(document).on('click', '#post_pagination .pagination a', function(event){
 event.preventDefault();
 var page = $(this).attr('href').split('page=')[1];
 var filter=document.querySelector("#tabs_filter").value;
 ajax('get',`{{Route("employee-pagination")}}`,data={page:page},'#pagination_employee');
});

</script>
