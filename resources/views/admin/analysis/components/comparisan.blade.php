<div class="col-md-6">
    <div class="card">
        <div class="card-header">
            <div
                id="reportrange_comparisan"
                style="
                    background: transparent;
                    cursor: pointer;
                    padding: 5px 10px;
                    border: 1px solid #ccc;
                    width: 100%;
                "
            >
                <i class="fa fa-calendar"></i>&nbsp; <span class="ab"></span>
                <i class="fa fa-caret-down"></i>
                <input type="hidden" id="start_date_range" />
                <input type="hidden" id="end_date_range" />
            </div>
        </div>
        <div class="card-body">
            <p class="text-center">
                <strong>Projects Comparisan</strong>
            </p>

            @foreach ($statusCounts as $eachStatus)

            <div class="progress-group">
                {{$eachStatus->dealname}}
                <span class="float-right"><b>160</b>/200</span>
                <div class="progress progress-sm">
                    <div
                        class="progress-bar bg-success"
                        style="width: 10%"
                    ></div>
                </div>
            </div>
            <!-- /.progress-group -->
            @endforeach
        </div>
    </div>
</div>
<script>
    $(function () {
        var start = moment().subtract(moment().year() - 1947, "years");
        var end = moment();
        function cb(start, end) {
            $("#reportrange_comparisan span").html(
                start.format("MMMM D, YYYY") +
                    " - " +
                    end.format("MMMM D, YYYY")
            );
            $("#start_date_range").val(start.format("YYYY-MM-DD"));
            $("#end_date_range").val(end.format("YYYY-MM-DD"));
        }

        $("#reportrange_comparisan").daterangepicker(
            {
                startDate: start,
                endDate: end,
                ranges: {
                    Today: [moment(), moment()],
                    Yesterday: [
                        moment().subtract(1, "days"),
                        moment().subtract(1, "days"),
                    ],
                    "Last 7 Days": [moment().subtract(6, "days"), moment()],
                    "Last 30 Days": [moment().subtract(29, "days"), moment()],
                    "This Month": [
                        moment().startOf("month"),
                        moment().endOf("month"),
                    ],
                    "Last Month": [
                        moment().subtract(1, "month").startOf("month"),
                        moment().subtract(1, "month").endOf("month"),
                    ],
                },
            },
            cb
        );
        cb(start, end);
    });
</script>
