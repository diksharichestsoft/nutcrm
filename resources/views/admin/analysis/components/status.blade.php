@php
    $iconsArr = array("fa-project-diagram", "fa-wrench", "fa-handshake", "fa-exclamation", "fa-pause-circle", "fa-stop-circle", "fa-thumbs-up", "fa-thumbs-down");
    $colorArr = array("","primary" , "info", "danger", "info", "warning", "secondary", "success", "danger");
@endphp
<div id="reportrange_status" style="background: transparent; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
    <i class="fa fa-calendar"></i>&nbsp;
    <span class="ab"></span> <i class="fa fa-caret-down"></i>
    <input type="hidden" id="start_date_range" >
    <input type="hidden" id="end_date_range">
</div>

<div class="container-fluid">
    <div class="row">
        @foreach ($statusCounts as $key=>$eachStatusCount)
        <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box">
            <span class="info-box-icon bg-{{$colorArr[$key]}} elevation-1"><i class="fas fa-cog"></i></span>

            <div class="info-box-content">
            <span class="info-box-text">{{$eachStatusCount->dealname}}</span>
            <span class="info-box-number">
                {{$eachStatusCount->count}}
            </span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
        </div>
        @endforeach
        <!-- /.col -->
    </div>
</div>
<script>

$(function() {
    var start = moment().subtract(moment().month()- 1, 'months');
  var end = moment();
$('#reportrange_status span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
  function cb(start, end) {
      $('#reportrange_status span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      $('#start_date_range').val(start.format('YYYY-MM-DD'));
      $('#end_date_range').val(end.format('YYYY-MM-DD'));
      fetch_status_data(1,"")
  }

  $('#reportrange_status').daterangepicker({
      startDate: start,
      endDate: end,
      ranges: {
         'Today': [moment(), moment()],
         'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
         'Last 7 Days': [moment().subtract(6, 'days'), moment()],
         'Last 30 Days': [moment().subtract(29, 'days'), moment()],
         'This Month': [moment().startOf('month'), moment().endOf('month')],
         'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
  }, cb);
//   cb(start, end);

  });
</script>
