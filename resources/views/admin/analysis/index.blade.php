<section>
    <div id="statusCounts">
        @include('admin.analysis.components.status')
    </div>
    <div id="monthComparisan">
        @include('admin.analysis.components.comparisan')
    </div>
</section>
<script>
  function fetch_status_data()
  {
    $('#page-loader').show();

    var start_date=document.querySelector('#start_date_range').value;
    var end_date=document.querySelector('#end_date_range').value;
    var data={start_date:start_date,end_date:end_date};
    var make_url= "{{route('project-analysis-status-filter')}}";
    $.ajax({
      url:make_url,
      data:data,
      success:function(data)
      {
          $('#statusCounts').empty().html(data);
        $('#page-loader').hide();

      },
      error:function(error){
        $('#page-loader').hide();
      }
    });
    }
</script>
