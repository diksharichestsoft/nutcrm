<!-- Button trigger modal -->
  <!-- Modal -->

  <form id="add_ticket_form"  action="" method="POST">
    <div id="dsr_item_container">
        @csrf
        <div class="dsr_items">
            <div class="form-group">
                <label for="project_id">Title</label>
                <br>
                <input type="text" class="form-control" name="title" id="title"> 
{{--
                <input type="hidden" class="form-control" name="created_by" value="{{Auth::user()->id}}">
--}}                <input type="hidden" name="reqType" value="0">
                <div class="error" id="error_title"></div>
            </div>
            <div class="form-group">
                <label for="description" class="col col-form-label">Description</label>
                  <div class="col-sm-10">
                      <input type="hidden" id="description" class="form-control" name="description">
                      <div id="editor" style="border: 1px solid #ffffff42;"></div>
                  </div>
                  <div class="error" id="error_description"></div>
              </div>
              <div class="form-group">
                <label class="col col-form-label">Select Department</label>
                <select class="form-control" id="department_id" name="department_id"  >
                <option value="N/A" disabled selected="true">--Select Department--</option>
                @foreach($departments as $department)
                    <option value="{{$department->id}}">{{$department->name}}</option>
                @endforeach
                </select>
                <div class="error" id="error_department_id"></div>
                </div>
                <div class="form-group row">
                    <label for="priority" class="col-sm-3 col-form-label">Priority</label>
                    <div class="col-sm-10">
                      <select name="priority" id="priority" class="form-control ">
                        <option disabled selected >Select Priority</option>
                          <option value="1">Highest</option>
                          <option value="2">Medium</option>
                          <option value="3">Low</option>
                          <option value="4">Lowest</option>
                      </select>
                    </div>
                    <div class="error" id="error_priority"></div>
                  </div>
        </div>
    </div>
    <br>
        {{-- <button type="submit" class="btn btn-primary">Save</button> --}}
        <button type="button" class="btn btn-primary"   onclick="$('#add_ticket_form').submit()"> Save</button>

    </form>


<script>

</script>


<script>
var myEditor;
InlineEditor
.create( document.querySelector( `#editor` ) )
.then( editor => {
console.log( 'Editor was initialized', editor );
myEditor = editor;
} )
.catch( err => {
console.error( err.stack );
} );
$(document).ready(function (){
// Hiding default extrack file and dropdown buttons
$(".ck-file-dialog-button").hide();
$(".ck-dropdown__button").hide();
})
</script>

