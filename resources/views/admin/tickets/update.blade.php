<!-- Button trigger modal -->
  <!-- Modal -->

            <form id="update_ticket_form" action="" method="POST">
            <div id="dsr_item_container">
                @csrf
                <div class="dsr_items">
                    <div class="form-group">
                        <label for="project_id">Title</label>
                        <br>
                        <input type="text" class="form-control" name="title" value="{{$thisTicket->title}}">
                        {{-- <input type="hidden" class="form-control" name="user_id" value="{{Auth::user()->id}}"> --}}
                        <input type="hidden" name="reqType" value="1">
                        <input type="hidden" name="id" value="{{$thisTicket->id}}">
                        <div class="error" id="error_title"></div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col col-form-label">Description</label>
                          <div class="col-sm-10">
                              <input type="hidden" id="update-description" name="description">
                              <div id="editorupdate" style="border: 1px solid #ffffff42;">{!!$thisTicket->description!!}</div>
                          </div>
                      </div>
                      <div class="form-group row">
                        <label for="department_id" class="col-sm-3 col-form-label">Department</label>
                        <div class="col-sm-10">
                          <select name="department_id" id="department_id" class="form-control">
                            <option value="N/A" disabled selected="true">--Select Department--</option>
                            @foreach($departments as $department)
                              <option value="{{ $department->id }}" {{($department->id == $thisTicket->department_id) ? 'selected' : ''}}>{{ $department->name }}</option>
                            @endforeach
                          </select>
                          <div class="error" id="error_department_id"></div>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="priority" class="col-sm-3 col-form-label">Priority</label>
                        <div class="col-sm-10">
                          <select name="priority" id="priority" class="form-control">
                            <option disabled selected>Select Priority</option>
                              <option value="1" {{($thisTicket->priority == 1 ? 'selected' : '')}}>Highest</option>
                              <option value="2"  {{($thisTicket->priority == 2 ? 'selected' : '')}}>Medium</option>
                              <option value="3"  {{($thisTicket->priority == 3 ? 'selected' : '')}}>Low</option>
                              <option value="4"  {{($thisTicket->priority == 4 ? 'selected' : '')}}>Lowest</option>
                          </select>
                          <div class="error" id="error_priority"></div>
                        </div>
                      </div>
                </div>
            </div>
            <br>
                {{-- <button type="submit" class="btn btn-primary">Save</button> --}}
                <button type="button" class="btn btn-primary" onclick="$('#update_ticket_form').submit()"> Save</button>

            </form>

  <script>

</script>


<script>
    var myEditor;
    InlineEditor
    .create( document.querySelector( `#editorupdate` ) )
    .then( editor => {
        console.log( 'Editor was initialized', editor );
        myEditor = editor;
    } )
    .catch( err => {
        console.error( err.stack );
    } );
    $(document).ready(function (){
    // Hiding default extrack file and dropdown buttons
    $(".ck-file-dialog-button").hide();
    $(".ck-dropdown__button").hide();
    })
</script>
