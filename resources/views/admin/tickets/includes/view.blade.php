   <!-- Main content -->


  </head>

   <div class="modal fade" id="addticketModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Ticket</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="add_ticket_modal_body" class="modal-body">
            @include('admin.tickets.create')
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

   <div class="modal fade" id="ticketDetailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ticket Detail</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="ticketDetailBody" class="modal-body">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>



    {{-- <a href="#" id="addTodoButton" class="btn btn-outline-success btn-lg" data-toggle="modal" data-target="#addleaveModal">
        <i class="fa fa-plus" aria-hidden="true"></i>New Todo
    </a> --}}
    <div class="modal fade" id="editTicketModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel"> Edit Ticket</h5>
              <button type="button" class="close" id='createDealHide' data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body " id="editTicketBody">
            </div>
          </div>
        </div>
      </div>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!--  -->
                <div class="card-body">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width: 10px">#</th>
                          <th>Action</th>
                          <th>Title</th>
                          <th>Department</th>
                          <th>Priority</th>
                        </tr>
                      </thead>
                      <tbody>
                          @forelse ($tickets as $key=> $value )
                          <tr>
                              <td>
                                {{$key+1}}
                              </td>
                              <td>
                                <div class="btn-group">
                                    @can('tickets_edit')
                                    <button data-id="{{$value->id}}" onclick="updateticket(this)" class="btn btn-outline-success btn-xs  update" ><i class="fas fa-pencil-alt"></i></button>
                                    @endcan

                                    @can('tickets_view')
                                    <a href=" {{route('ticket-detail-view',$value->id)}}" target="_blank" class="btn btn-outline-success btn-xs  view"><i class="fas fa-eye"></i></a>
                                    @endcan

                                    @can('tickets_delete')
                                    <button data-id="{{$value->id}}" onclick="removeticket(this)" class="btn btn-danger btn-xs  remove"><i class="fas fa-times"></i></button>
                                    @endcan

                                </div>
                              </td>
                              <td>
                                  {{$value->title}}
                                </td>
                              <td>
                                {{$value->departments->name}}
                            </td>
                            <td>
                              @if($value->priority == 1)
                                Highest
                              @elseif($value->priority == 2)
                                Medium
                              @elseif($value->priority == 3)
                                Low
                              @elseif($value->priority == 4)
                                Lowest
                              @endif
                          </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="9">
                                <h4  align="center">No Data Found</h4>
                            </td>
                        </tr>
                        @endforelse

                      </tbody>
                    </table>
                </div>

                <!--  -->
            </div>
        </div>
        {{$tickets->links()}}
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->

    <script>


    $("#addticketButton").on('click', function (){
      $("#addticketModal").modal('show');
      document.querySelector("#error_title").innerText="";
      document.querySelector("#error_department_id").innerText="";
      document.querySelector("#error_priority").innerText="";
    })
    // $("#view").html($("#add_dsr_modal_body").html())
    // initializeCreateForm()
    $("#add_ticket_form").on('submit', function (e){
        e.preventDefault();
        var descData = ($("#editor").html());
        $("#description").val(descData);
        var data = new FormData(this);
        console.log(data);
        $.ajax({
            type:'post',
            url:"{{route('ticket-store')}}",
            cache: false,
                contentType: false,
                processData: false,
                dataType: "JSON",
            data : {data: data},
        headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data:data,
            success:function(data){
                $('#addticketModal').modal('hide');
                $(".modal-backdrop").hide();
                $("body").removeClass("modal-open");
                fetchTicketsData(1);
                Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Added Successfully',
                showConfirmButton: false,
                timer: 1500
                })
            },
            error:function(data){
            $.each(data.responseJSON.errors, function(id,msg){
                $('#error_'+id).html(msg);

            })
            }
        });
        })

$(document).on('click', '#post_pagination .pagination a', function(event){
 event.preventDefault();
 var page = $(this).attr('href').split('page=')[1];
 var filter=document.querySelector("#tabs_filter").value;
 ajax('get',`{{Route("employee-pagination")}}`,data={page:page},'#pagination_employee');
});

function removeticket(e)
{

  var id=e.getAttribute('data-id');
  swal({
  title: "Oops....",
  text: "Are You Sure You want to delete Ticket!",
  icon: "error",
  buttons: [
    'NO',
    'YES'
  ],
}).then(function(isConfirm) {
  if (isConfirm) {
    $.ajax({
      url:"{{route('remove-ticket')}}",
      type:"post",
      data:{id:id},
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
      success:function(data){
        Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Remove Successfully',
                showConfirmButton: false,
                timer: 1500
                })
                fetchTicketsData(1);
        }
    })
  } else {

  }
});
}


function updateticket(e)
{
  $('#page-loader').show();

  var id=e.getAttribute('data-id');
  $.ajax({
    url:"{{route('ticket-edit')}}",
    type:"get",
    data:{id:id},
    success:function(data)
    {
       // toastr.success(response.message)
      $('#page-loader').hide();
      $('#editTicketBody').empty().html(data);
    $("#editTicketModal").modal('show');
      intitializeUpdate();
    },
    error:function(){
      $('#page-loader').hide();
    }
  })
}

function intitializeUpdate(){

// initializeCreateForm()
    $("#update_ticket_form").on('submit', function (e){
        e.preventDefault();
        var descData = ($("#editorupdate").html());
        console.log(descData)
        $("#update-description").val(descData);

        var data = new FormData(this);
        console.log(data);
        $.ajax({
            type:'post',
            url:"{{route('ticket-store')}}",
            cache: false,
                contentType: false,
                processData: false,
                dataType: "JSON",
            data : {data: data},
        headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data:data,
            success:function(data){
                $('#addticketModal').modal('hide');
                $(".modal-backdrop").hide();
                fetchTicketsData(1);
                Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Updated Successfully',
                showConfirmButton: false,
                timer: 1500
                })
            },
            error:function(data){
            $.each(data.responseJSON.errors, function(id,msg){
                $('#error_'+id).html(msg);

            })
            }
        });
        })

}
</script>
