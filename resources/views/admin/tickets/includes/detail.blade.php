@extends('admin.layout.template')
@section('contents')
<table class="table">
    <tbody>
      <tr>
        <th scope="row">Title</th>
        <td>{{$thisTicket->title}}</td>
      </tr>
      <tr>
        <th scope="row">Description</th>
        <td>{!!$thisTicket->description!!}</td>
      </tr>
      <tr>
        <th scope="row">Priority</th>
        <td>
        @if($thisTicket->priority == 1)
          Highest
        @elseif($thisTicket->priority == 2)
          Medium
        @elseif($thisTicket->priority == 3)
          Low
        @elseif($thisTicket->priority == 4)
          Lowest
        @endif
        </td>
    </tr>
      <tr>
        <th scope="row">Created By</th>
        <td>{{$thisTicket->createdByUser->name}}</td>
      </tr>
      <tr>
        <th scope="row">Created At</th>
        <td>{{timestampToDateAndTimeObj($thisTicket->created_at)->time}}</td>
      </tr>
      <tr>
        <th scope="row">Created On</th>
        <td>{{timestampToDateAndTimeObj($thisTicket->created_at)->date}}</td>
      </tr>
      <tr>
        <th scope="row">Department</th>
        <td>{{$thisTicket->departments->name}}</td>
      </tr>
    </tbody>
  </table>
  @endsection

