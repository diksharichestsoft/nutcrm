@extends('admin.layout.template')
@section('contents')
 <!-- Info boxes (optional) -->
 <!-- include('admin.layout.includes.info_box') -->

    <!-- /.content-header -->
    <!-- Main content -->
    <a href="{{route('premissions-create')}}" class="btn btn-outline-success btn-lg">Create Permission</a>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!--  -->
                <div class="card-body"> 
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width: 10px">#</th>
                          <th>Name</th>
                          <th>Action</th>
                          <!-- <th style="width: 40px">Label</th> -->
                        </tr>
                      </thead>
                      <tbody>
                        <?php $i =0 ?>
                        @foreach($permissions as $permission)
                          <tr>
                            <td><?php  $i++; echo $i; ?> </td>
                            <td>{{$permission->name}}</td>
                            <td>
                            <a href="{{route('premissions-edit',$permission->id)}}" class="btn btn-outline-success btn-sm rounded-pill">Edit</a>
                            <a href="{{route('premissions-destroy',$permission->id)}}" class="btn btn-outline-danger btn-sm rounded-pill">Delete</a>
                            </td>
                        </tr>
                        @endforeach
                        
                      </tbody>
                    </table>
                </div>
                <!--  -->
            </div>
        </div>
        
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->

 @endsection