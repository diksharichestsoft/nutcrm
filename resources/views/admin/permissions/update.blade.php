@extends('admin.layout.template')
@section('contents')
 <!-- Info boxes (optional) -->
 <!-- include('admin.layout.includes.info_box') -->

    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!--  -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Update Permission</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form method="post" action="{{route('premissions-update',$permission->id)}}" >
                        @csrf
                        <div class="card-body">
                        
                        <div class="form-group">
                            <label for="exampleInputEmail1">Permission Name</label>
                            <input type="text" class="form-control" name="permission" value="{{$permission->name??''}}" id="permission" placeholder="Enter Permission Name">
                            @error('permission')
                                <div class="error error-msg-red">{{ $message }}</div>
                            @enderror
                        </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!--  -->
            </div>
        </div>
      </div><!--/. container-fluid -->
    </section>

@endsection
