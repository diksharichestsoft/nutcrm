@extends('admin.layout.template')

@section('contents')

<table class="table">
    <tbody>
      <tr>
        <th scope="row">Name</th>
        <td>{{$thisPerformance->name ?? ""}}</td>
      </tr>

      <tr>

        <th scope="row">Departments</th>
        <td>
            @foreach ($thisPerformance->departments as $eachDepartment)
            {{$eachDepartment->name ?? ""}},
            @endforeach
        </td>
      </tr>
      <tr>
        <th scope="row">Total Assigned Projects</th>
        <td>{{$thisPerformance->projects_count ?? ""}}</td>
      </tr>
      <tr>
        <th scope="row">+ve Completed</th>
        <td>{{$thisPerformance->project_positive_completed_count ?? ""}}</td>
      </tr>
      <tr>
        <th scope="row">-ve completed</th>
        <td>{!!$thisPerformance->project_negtive_completed_count!!}</td>
      </tr>
      <tr>
        <th scope="row">Moved in</th>
        <td>{{$thisPerformance->project_moved_in_count ?? ""}}</td>
      </tr>
      <tr>
        <th scope="row">Moved Out</th>
        <td>{{$thisPerformance->project_moved_out_count ?? ""}}</td>
      </tr>


    </tbody>
  </table>@endsection
