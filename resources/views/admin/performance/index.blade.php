@section('contents')
<style>
      .switch{
    opacity: 0;
    position: absolute;
    z-index: 1;
    width: 18px;
    height: 18px;
    cursor: pointer;
  }
  .switch + .lable{
    position: relative;
    display: inline-block;
    margin: 0;
    line-height: 20px;
    min-height: 18px;
    min-width: 18px;
    font-weight: normal;
    cursor: pointer;
  }
  .switch + .lable::before{
    cursor: pointer;
    font-weight: normal;
    font-size: 12px;
    color: #32a3ce;
    content: "\a0";
    background-color: #FAFAFA;
    border: 1px solid #c8c8c8;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
    border-radius: 0;
    display: inline-block;
    text-align: center;
    height: 16px;
    line-height: 14px;
    min-width: 16px;
    margin-right: 1px;
    position: relative;
    top: -1px;
  }
  .switch:checked + .lable::before {
    display: inline-block;
    content: '\f00c';
    background-color: #F5F8FC;
    border-color: #adb8c0;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
  }
  /* CSS3 on/off switches */
  .switch + .lable {
    margin: 0 4px;
    min-height: 24px;
  }
  .switch + .lable::before {
    font-weight: normal;
    font-size: 11px;
    line-height: 17px;
    height: 20px;
    overflow: hidden;
    border-radius: 12px;
    background-color: #F5F5F5;
    -webkit-box-shadow: inset 0 1px 1px 0 rgba(0, 0, 0, 0.15);
    box-shadow: inset 0 1px 1px 0 rgba(0, 0, 0, 0.15);
    border: 1px solid #CCC;
    text-align: left;
    float: left;
    padding: 0;
    width: 52px;
    text-indent: -21px;
    margin-right: 0;
    -webkit-transition: text-indent .3s ease;
    -o-transition: text-indent .3s ease;
    transition: text-indent .3s ease;
    top: auto;
  }
  .switch.switch-bootstrap + .lable::before {
    font-family: FontAwesome;
    content: "\f00d";
    box-shadow: none;
    border-width: 0;
    font-size: 16px;
    background-color: #a9a9a9;
    color: #F2F2F2;
    width: 52px;
    height: 22px;
    line-height: 21px;
    text-indent: 32px;
    -webkit-transition: background 0.1s ease;
    -o-transition: background 0.1s ease;
    transition: background 0.1s ease;
  }
  .switch.switch-bootstrap + .lable::after {
    content: '';
    position: absolute;
    top: 2px;
    left: 3px;
    border-radius: 12px;
    box-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
    width: 18px;
    height: 18px;
    text-align: center;
    background-color: #F2F2F2;
    border: 4px solid #F2F2F2;
    -webkit-transition: left 0.2s ease;
    -o-transition: left 0.2s ease;
    transition: left 0.2s ease;
  }
  .switch.switch-bootstrap:checked + .lable::before {
    content: "\f00c";
    text-indent: 6px;
    color: #FFF;
    border-color: #b7d3e5;

  }
  .switch-primary >.switch.switch-bootstrap:checked + .lable::before {
      background-color: #337ab7;
  }
  .switch-success >.switch.switch-bootstrap:checked + .lable::before {
      background-color: #5cb85c;
  }
  .switch-danger >.switch.switch-bootstrap:checked + .lable::before {
      background-color: #d9534f;
  }
  .switch-info >.switch.switch-bootstrap:checked + .lable::before {
      background-color: #5bc0de;
  }
  .switch-warning >.switch.switch-bootstrap:checked + .lable::before {
      background-color: #f0ad4e;
  }
  .switch.switch-bootstrap:checked + .lable::after {
    left: 32px;
    background-color: #FFF;
    border: 4px solid #FFF;
    text-shadow: 0 -1px 0 rgba(0, 200, 0, 0.25);
  }
  /* square */
  .switch-square{
    opacity: 0;
    position: absolute;
    z-index: 1;
    width: 18px;
    height: 18px;
    cursor: pointer;
  }
  .switch-square + .lable{
    position: relative;
    display: inline-block;
    margin: 0;
    line-height: 20px;
    min-height: 18px;
    min-width: 18px;
    font-weight: normal;
    cursor: pointer;
  }
  .switch-square + .lable::before{
    cursor: pointer;
    font-family: fontAwesome;
    font-weight: normal;
    font-size: 12px;
    color: #32a3ce;
    content: "\a0";
    background-color: #FAFAFA;
    border: 1px solid #c8c8c8;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
    border-radius: 0;
    display: inline-block;
    text-align: center;
    height: 16px;
    line-height: 14px;
    min-width: 16px;
    margin-right: 1px;
    position: relative;
    top: -1px;
  }
  .switch-square:checked + .lable::before {
    display: inline-block;
    content: '\f00c';
    background-color: #F5F8FC;
    border-color: #adb8c0;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
  }
  /* CSS3 on/off switches */
  .switch-square + .lable {
    margin: 0 4px;
    min-height: 24px;
  }
  .switch-square + .lable::before {
    font-weight: normal;
    font-size: 11px;
    line-height: 17px;
    height: 20px;
    overflow: hidden;
    border-radius: 2px;
    background-color: #F5F5F5;
    -webkit-box-shadow: inset 0 1px 1px 0 rgba(0, 0, 0, 0.15);
    box-shadow: inset 0 1px 1px 0 rgba(0, 0, 0, 0.15);
    border: 1px solid #CCC;
    text-align: left;
    float: left;
    padding: 0;
    width: 52px;
    text-indent: -21px;
    margin-right: 0;
    -webkit-transition: text-indent .3s ease;
    -o-transition: text-indent .3s ease;
    transition: text-indent .3s ease;
    top: auto;
  }
  .switch-square.switch-bootstrap + .lable::before {
    font-family: FontAwesome;
    content: "\f00d";
    box-shadow: none;
    border-width: 0;
    font-size: 16px;
    background-color: #a9a9a9;
    color: #F2F2F2;
    width: 52px;
    height: 22px;
    line-height: 21px;
    text-indent: 32px;
    -webkit-transition: background 0.1s ease;
    -o-transition: background 0.1s ease;
    transition: background 0.1s ease;
  }
  .switch-square.switch-bootstrap + .lable::after {
    content: '';
    position: absolute;
    top: 2px;
    left: 3px;
    border-radius: 12px;
    box-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
    width: 18px;
    height: 18px;
    text-align: center;
    background-color: #F2F2F2;
    border: 4px solid #F2F2F2;
    -webkit-transition: left 0.2s ease;
    -o-transition: left 0.2s ease;
    transition: left 0.2s ease;
  }
  .switch-square.switch-bootstrap:checked + .lable::before {
    content: "\f00c";
    text-indent: 6px;
    color: #FFF;
    border-color: #b7d3e5;

  }
  .switch-primary >.switch-square.switch-bootstrap:checked + .lable::before {
      background-color: #337ab7;
  }
  .switch-success >.switch-square.switch-bootstrap:checked + .lable::before {
      background-color: #5cb85c;
  }
  .switch-danger >.switch-square.switch-bootstrap:checked + .lable::before {
      background-color: #d9534f;
  }
  .switch-info >.switch-square.switch-bootstrap:checked + .lable::before {
      background-color: #5bc0de;
  }
  .switch-warning >.switch-square.switch-bootstrap:checked + .lable::before {
      background-color: #f0ad4e;
  }
  .switch-square.switch-bootstrap:checked + .lable::after {
    left: 32px;
    background-color: #FFF;
    border: 4px solid #FFF;
    text-shadow: 0 -1px 0 rgba(0, 200, 0, 0.25);
  }
  .switch-square.switch-bootstrap + .lable::after {
      border-radius: 2px;
  }
    .daterangepicker .ranges li {font-size: 12px;padding: 8px 12px;cursor: pointer;background: #000000b0;}
  </style>
<div class="card" id="data">
    <div class="card-header p-2" id="card_head">
      <ul class="nav nav-pills">
        <div id="reportrange" style="background: transparent; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
        <i class="fa fa-calendar"></i>&nbsp;
        <span class="ab"></span> <i class="fa fa-caret-down"></i>
       </div>
       <input type="hidden" id="start_date_range" >
       <input type="hidden" id="end_date_range">
       <li style="align-self: center;">
           <h3>
           </h3>
       </li>
        <li class="nav-item search-right">
            <div>
                @can('targets_add')
                {{-- <a href="javascript:void(0)" onclick="addTarget()"  class="btn btn-primary" ><i class="fa fa-plus"></i> Add Targetttttt</a> --}}
                @endcan
            </div>
 <div class="search_bar">
            <div class="input-group" data-widget="sidebar-search">
             <input class="form-control form-control-sidebar" id="search" type="search" placeholder="Search" aria-label="Search">
            </div>
         </div>
        </li>
      </ul>
      <label class="label-switch switch-success">
        <input type="checkbox" id="toggle_check" value="0"  class="switch-square switch-bootstrap status toggle_user_login" data-id="" name="" id="status">
        <span class="lable">Show All Users</span></label>
    </div>
</div>



<div id="view">

    {{-- @include('admin.targets.includes.view') --}}
</div>






{{-- Created Modal --}}
<div class="modal fade" id="targetModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Projects</h5>
          <button type="button" class="close" id='createDealHide' data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body " id="targetModalBody">
        </div>
      </div>
    </div>
  </div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/inline/ckeditor.js"></script>

<script>
      $("#toggle_check").on('click', (e)=>{
   if($("#toggle_check").val() == 1){
            $("#toggle_check").val(0)
        }else{
            $("#toggle_check").val(1)
        }
        getTargetData();
    })
  function addTarget()
  {
    $('#page-loader').show();

    $.ajax({
      url:"{{route('targets-add')}}",
      type:"get",
      success:function(data)
      {
        $("#targetModal").modal('show')
        $('#page-loader').hide();
        $('#targetModalBody').empty().html(data);
      },
      error:function(error){
        $('#page-loader').hide();
      }
    })
  }
  function editTarget(e)
  {
    $('#page-loader').show();
    const id = $(e).data('id');
    $.ajax({
      url:"{{route('targets-edit')}}",
      type:"get",
      data: {id},
      success:function(data)
      {
        $("#targetModal").modal('show')
        $('#page-loader').hide();
        $('#targetModalBody').empty().html(data);
      },
      error:function(error){
        $('#page-loader').hide();
      }
    })
  }
//   function removeTarget(e)
//   {
//     $('#page-loader').show();
//     const id = $(e).data('id');
//     $.ajax({
//       url:"{{route('targets-remove')}}",
//       type:"post",
//       data: {id},
//       success:function(data)
//       {
//         $("#targetModal").modal('show')
//         $('#page-loader').hide();
//         $('#targetModalBody').empty().html(data);
//         Swal.fire({
//                 position: 'top-end',
//                 icon: 'success',
//                 title: data.message,
//                 showConfirmButton: false,
//                 timer: 1500
// 	          })
//       },
//       error:function(error){
//         $('#page-loader').hide();
//         Swal.fire({
//                 position: 'top-end',
//                 icon: 'error',
//                 title: data.message ?? "Failed to remove",
//                 showConfirmButton: false,
//                 timer: 1500
// 	          })
//       }
//     })
//   }
function removeTarget(e)
  {
    var id=e.getAttribute('data-id');
    swal({
    title: "Oops....",
    text: "Are You Sure You want to delete!",
    icon: "error",
    buttons: [
      'NO',
      'YES'
    ],
  }).then(function(isConfirm) {
    if (isConfirm) {
      $.ajax({
        url:"{{route('targets-remove')}}",
        type:"post",
        data:{id:id},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        success:function(data){
          getTargetData(1);
          Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Remove Successfully',
                showConfirmButton: false,
                timer: 1500
                })
        },
        error:function(error){
        $('#page-loader').hide();
        Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: error.message ?? "Failed to remove",
                showConfirmButton: false,
                timer: 1500
	          })
        }
      })
    } else {

    }
  });
  }





$(function() {
    var start = moment().subtract(moment().year()- 1947, 'years');
  var end = moment();
  function cb(start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      $('#start_date_range').val(start.format('YYYY-MM-DD'));
      $('#end_date_range').val(end.format('YYYY-MM-DD'));
      getTargetData(1);
  }

  $('#reportrange').daterangepicker({
      startDate: start,
      endDate: end,
      ranges: {
         'Today': [moment(), moment()],
         'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
         'Last 7 Days': [moment().subtract(6, 'days'), moment()],
         'Last 30 Days': [moment().subtract(29, 'days'), moment()],
         'This Month': [moment().startOf('month'), moment().endOf('month')],
         'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
  }, cb);
  cb(start, end);

  });
  document.querySelector("#search").addEventListener("keyup",(e)=>{
      getTargetData(1);
  });

    $(document).on('click', '.pagination a', function(event){
  event.preventDefault();
  var page = $(this).attr('href').split('page=')[1];
  var sendurl=$(this).attr('href');
  getTargetData(page);
});
function getTargetData(page)
{
  $('#page-loader').show();
//   page = (page='')?'1':page;
// var id = $(e).attr('data-id');
var active_users = $("#toggle_check").val();
    var start_date=document.querySelector('#start_date_range').value;
    var end_date=document.querySelector('#end_date_range').value;
    var search=document.querySelector("#search").value;
    var data={search:search,start_date:start_date,end_date:end_date,active_users: active_users};
  var make_url= "{{url('/')}}/admin/performance/search?page="+page;
  $.ajax({
    url:make_url,
    data:data,
    success:function(data)
    {
    $('#view').empty().html(data);
    $('#page-loader').hide();
    },
    error:function(error){
      $('#page-loader').hide();

    }
  });
  }
  //   Function to submit Target
$(document).on('submit','#addTargetForm',function(e){
    e.preventDefault();
    var descData = (myEditor.getData());
    $("#description").val(descData);
    var data = new FormData(this);
      $.ajax({
          type:'post',
          url:"{{route('targets-store')}}",
          dataType: "JSON",
         xhr: function() {
               myXhr = $.ajaxSettings.xhr();
               return myXhr;
         },
         cache: false,
         contentType: false,
         processData: false,
          data:data,
          success:function(data){
              $(".services:first").trigger( "click" );
              $('#targetModal').modal('hide');
              getTargetData(1);
              Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Added Successfully',
                showConfirmButton: false,
                timer: 1500
	          })
          },
          error:function(data){
            $.each(data.responseJSON.errors, function(id,msg){
              $('#error_'+id).html(msg);
            })
          }
        });
      });


</script>
@endsection
