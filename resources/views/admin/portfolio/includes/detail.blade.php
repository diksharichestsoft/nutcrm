@extends('admin.layout.template')

@section('contents')

    <table class="table">
        <tbody>
            <tr>
                <th scope="row">Project Name</th>
                <td>{{ $thisPortfolio->project_name }}</td>
            </tr>

            <tr>
                <th scope="row">Web URL</th>
                <td>{{ $thisPortfolio->web_url }}</td>
            </tr>
            <tr>
                <th scope="row">Android URL</th>
                <td>{{ $thisPortfolio->android_url }}</td>
            </tr>
            <tr>
                <th scope="row">Ios URL</th>
                <td>{{ $thisPortfolio->ios_url }}</td>
            </tr>
            <tr>
                <th scope="row">Description</th>
                <td>{!! $thisPortfolio->description !!}</td>
            </tr>
            <tr>
                <th scope="row">Attachment</th>
                <td><img src="{{ $thisPortfolio->attachment }}" alt=""></td>
            </tr>
            @if($thisPortfolio->createdByUser)
            <tr>
                <th scope="row">Created By</th>
                <td>{{ $thisPortfolio->createdByUser->name}}</td>
            </tr>
            @endif
            @if($thisPortfolio->updatedByUser)
            <tr>
                <th scope="row">Updated By</th>
                <td>{{ $thisPortfolio->updatedByUser->name}}</td>
            </tr>
            @endif
            <tr>
                <th scope="row">Actions</th>
                <td>
                    <div class="btn-group">
                        <button data-id="{{ $thisPortfolio->id }}" onclick="updateCandidateProject(this)"
                            class="btn btn-outline-success btn-xs  update" data-toggle="modal" data-target=""><i
                                class="fas fa-pencil-alt"></i></button>
                        <button data-id="{{ $thisPortfolio->id }}" onclick="removeCompanyProject(this)"
                            class="btn btn-danger btn-xs  remove"><i class="fas fa-times"></i></button>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="modal fade" id="EditCandidates" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> Edit Candidates</h5>
                    <button type="button" class="close" id='EditCandidatesHide' data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body " id="EditCandidatesBody">
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/inline/ckeditor.js"></script>
    <script>
        function updateCandidateProject(e) {
            $('#page-loader').show();

            var id = e.getAttribute('data-id');
            $.ajax({
                url: "{{ route('portfolio-edit') }}",
                type: "get",
                data: {
                    id: id
                },
                success: function(data) {
                    $('#page-loader').hide();
                    $('#EditCandidatesBody').empty().html(data);
                    $("#EditCandidates").modal("show");
                },
                error: function() {
                    $('#page-loader').hide();
                }
            })
        }
        $("#edit_portfolio_form").on('submit', function(e) {
            e.preventDefault();
            var detail = true;
            var data = getFormWithImage("edit_portfolio_form");
            $.ajax({
                url: "{{ route('portfolio-store') }}",
                type: "post",
                cache: false,
                contentType: false,
                processData: false,
                dataType: "JSON",
                data: data,
                xhr: function() {
                    myXhr = $.ajaxSettings.xhr();
                    return myXhr;
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data,detail) {
                    $(".services:first").trigger("click");
                    $('#EditCandidates').modal('hide');
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Added Successfully, Reload the page to see the changes',
                        showConfirmButton: true
                    })
                },
                error: function(data) {
                    $.each(data.responseJSON.errors, function(id, msg) {
                        $('#error_' + id).html(msg);
                    });
                }
            });
        });
        $(".ck-file-dialog-button").hide();
    $(".ck-dropdown__button").hide();
        function removeCompanyProject(e) {

            var id = e.getAttribute('data-id');
            swal({
                title: "Oops....",
                text: "Are You Sure You want to delete!",
                icon: "error",
                buttons: [
                    'NO',
                    'YES'
                ],
            }).then(function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: "{{ route('portfolio-remove-data') }}",
                        type: "post",
                        data: {
                            id: id
                        },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            $('#view').empty().html(data);
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Removed Successfully',
                                showConfirmButton: true
                            }).then(() => {
                                window.location.replace("{{ route('portfolio', 'all') }}");
                            })
                        }
                    })
                } else {

                }
            });
        }
    </script>
@endsection
