{{-- <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script> --}}
{{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> --}}
<link rel="stylesheet" type="text/css" href="{{asset('css/amsify.suggestags.css')}}">

<!-- Amsify Plugin -->
{{-- <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"> --}}
<script type="text/javascript" src="{{asset('js/jquery.amsify.suggestags.js')}}"></script>
<style>
    .amsify-suggestags-list{
        background: black !important;
    }
</style>

    <div class="form-group">
        <label for="description">Teck Stack</label>
        @php
            $values = "";
        @endphp
        @if ($default_value != null)
            @foreach ($default_value as $deTag)
                @php
                $values = $values.",".$deTag->name;
                @endphp
            @endforeach
        @endif
        <input type="text" class="form-control" id="protfolioTags" name="tags" value="{{$values ?? ''}}"/>
    </div>


<script type="text/javascript">
 $(document).on("keydown", "form", function(event) {
    return event.key != "Enter";
});
function decodeHtml(html) {
    let areaElement = document.createElement("textarea");
    areaElement.innerHTML = html;
    return areaElement.value;
}
    data = "{{json_encode($array_value)}}";
    suggestions_arr = decodeHtml(data);
    var suggestions_arr = JSON.parse("[" + suggestions_arr + "]");
	$('input[name="tags"]').amsifySuggestags({
		suggestions: suggestions_arr[0],
        values: ["shivam"],
		whiteList: false
	});
</script>
