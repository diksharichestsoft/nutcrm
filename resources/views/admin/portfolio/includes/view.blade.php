   <!-- Main content -->
   <div class="row">
     <div class="col-md-10"> 
   <a href="{{route('portfolio-create')}}" class="btn btn-outline-success btn-lg">Add Portfolio</a>
     </div>
     <div class="col-md-2"> 
    <h6 class="count text-right">Total Portfolios : {{$portfolios->total()}}</h6>
     </div>
   </div>
    <div class="modal fade" id="createDeal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Candidates</h5>
              <button type="button" class="close" id='createDealHide' data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body " id="createDealBody">
            </div>
          </div>
        </div>
      </div>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card-body">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width: 10px">#</th>
                          <th>Action</th>
                          <th>Project Name</th>
                          <th>Tags</th>
                          <th>Website Url</th>
                          <th>Android Url</th>
                          <th>Ios Url</th>
                          <th>Admin Url</th>
                          <th>Description</th>
                        </tr>
                      </thead>
                      <tbody>
                        @php
                            $i =0;
                        @endphp

                        @foreach($portfolios as $key=>$portfolio)
                          <tr>
                              <td><?php  $i++; echo $i; ?></td>
                              <td>
                                    <div class="btn-group">
                                      <a href="{{route('portfolio-view',$portfolio->id)}}" target="" class="btn btn-outline-success btn-xs  view"><i class="fas fa-eye"></i></a>
                                      <button data-id="{{$portfolio->id}}" onclick="updateCompanyProject(this)" class="btn btn-outline-success btn-xs  update" data-toggle="modal" data-target="" ><i class="fas fa-pencil-alt"></i></button>
                                      <button data-id="{{$portfolio->id}}" onclick="removeCompanyProject(this)" class="btn btn-danger btn-xs  remove"><i class="fas fa-times"></i></button>
                                    </div>
                              </td>
                            <td>
                                <p>{{$portfolio->project_name ?? 'NaN'}} </p>
                            </td>
                            <td>
                                {{-- {{$portfolio->tags}} --}}
                                @forelse ($portfolio->tags as $tag)
                                {{$tag->name}},
                                @empty
                                ''
                                @endforelse

                            </td>

                            <?php

                            $base="https://"; ?>
                            <td>
                                  <a href="{{(str_starts_with($portfolio->web_url, 'http')) ? $portfolio->web_url : $base.$portfolio->web_url}}" target="_blank">Website Url</a>
                            </td>
                            <td>
                                  <a href="{{(str_starts_with($portfolio->android_url, 'http')) ? $portfolio->android_url : $base.$portfolio->android_url}}" target="_blank">Android Url</a>
                            </td>
                            <td>
                                  <a href="{{(str_starts_with($portfolio->ios_url, 'http')) ? $portfolio->ios_url : $base.$portfolio->ios_url}}" target="_blank">Ios url</a>
                            </td>
                            <td>
                                  <a href="{{(str_starts_with($portfolio->admin_url, 'http')) ? $portfolio->admin_url : $base.$portfolio->admin_url}}" target="_blank">Admin Url</a>
                            </td>
                            <td id="portfolio_description">
                                {!!$portfolio->description!!}
                            </td>

                        </tr>
                        @endforeach

                      </tbody>
                    </table>
                </div>

                <!--  -->
            </div>
        </div>
        {{$portfolios->links()}}
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
    <script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/inline/ckeditor.js"></script>

    <script>


        $(".toggle_user_login").on('click', function (e){
            e.preventDefault();
            var id = $(this).attr('data-id');
            $.ajax({
                type: "post",
                url: "{{route('employee-login-toggle')}}",
                headers:
                {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {id:id},
                dataType: "JSON",
                success: function (response) {

                      document.querySelector(`#${e.target.id}`).checked= response.success;
                }
            });
        })
    $(document).on('click', '#post_pagination .pagination a', function(event){
 event.preventDefault();
 var page = $(this).attr('href').split('page=')[1];
 var filter=document.querySelector("#tabs_filter").value;
 ajax('get',`{{Route("employee-pagination")}}`,data={page:page},'#pagination_employee');
});


function removeCompanyProject(e)
{

  var id=e.getAttribute('data-id');
  swal({
  title: "Oops....",
  text: "Are You Sure You want to delete!",
  icon: "error",
  buttons: [
    'NO',
    'YES'
  ],
}).then(function(isConfirm) {
  if (isConfirm) {
    $.ajax({
      url:"{{route('portfolio-remove-data')}}",
      type:"post",
      data:{id:id},
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
      success:function(data){
        getEmployeeData({{$portfolios->currentPage()}});
        Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Remove Successfully',
                showConfirmButton: false,
                timer: 1500
                })
        }
    })
  } else {

  }
});
}
</script>
