
    <section class="content">
        <div class="container-fluid">
<form method="post" id="edit_portfolio_form" action="" enctype="multipart/form-data">

    @csrf
    <div class="card-body">
        <div class="form-group">
            <label for="prject_name">Project Name</label>
            <input type="text" name="project_name" class="form-control" value="{{$thisPortfolio->project_name}}">
            <div class="error" id="error_project_name"></div>

        </div>
        <input type="hidden" name="reqType" value="1">
        <input type="hidden" name="id" value="{{$thisPortfolio->id}}">
        <div class="form-group">
            <label for="country" class="col-sm-2 col-form-label">Country</label>
            <div class="col-sm-10">
              <select name="country" id="country" class="form-control">
                <option value="">Select Country</option>
                @foreach($countries as $country)
                <option value="{{ $country->id }}" {{ $thisPortfolio->country == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                @endforeach
              </select>
              <div class="error" id="error_country"></div>
            </div>
        </div>
        <div class="form-group">
            <label for="department" class="col-sm-2 col-form-label">Department</label>
            <div class="col-sm-10">
              <select name="department" id="department" class="form-control">
                <option value="">Select Department</option>
                @foreach($departments as $department)
                <option value="{{ $department->id }}" {{ $thisPortfolio->department == $department->id ? 'selected' : '' }}>{{ $department->name }}</option>
                @endforeach
              </select>
              <div class="error" id="error_department"></div>
            </div>
        </div>
        <div class="form-group">
            <label for="web_url">Web URL</label>
            <input type="text" class="form-control" name="web_url" value="{{$thisPortfolio->web_url}}">
        </div>
        <div class="form-group">
            <label for="android_url">Android URL</label>
            <input type="text" class="form-control" name="android_url" value="{{$thisPortfolio->android_url}}">
        </div>
        <div class="form-group">
            <label for="ios_url">Ios URL</label>
            <input type="text" class="form-control" name="ios_url" value="{{$thisPortfolio->ios_url}}">
        </div>
        <div class="form-group">
            <label for="ios_url">Admin URL</label>
            <input type="text" class="form-control" name="admin_url" value="{{$thisPortfolio->admin_url}}">
        </div>

        <div class="form-group">
            <label for="description">Description</label>
            <input type="hidden" name="description"  id="description">
            <div id="editor2" style="border: 1px solid #ffffff42;">{!!$thisPortfolio->description!!}</div>
        </div>
        <div class="form-group">
            <label for="business_model">Business Model</label>
            <textarea type="text" class="form-control" name="business_model" >{{$thisPortfolio->business_model}}</textarea>
        </div>
        @include('admin.portfolio.includes.tags')

        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>

</form>
        </div>
    </section>

    {{-- <script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/inline/ckeditor.js"></script> --}}
<script>
    var myEditor2;
    InlineEditor
    .create( document.querySelector( '#editor2' ) )
    .then( editor => {
        console.log( 'Editor was initialized', editor );
        myEditor2 = editor;
    } )
    .catch( err => {
        console.error( err.stack );
    } );


</script>
<script>
    function getFormWithImage(id) {
        let form = document.querySelector(`#${id}`);
        let data = new FormData(form);
        return data;
    }
    $("#edit_portfolio_form").on('submit', function (e) {
        e.preventDefault();
        var detail = false;
        var descData = (myEditor2.getData());
        $("#description").val(descData);

        var data = getFormWithImage("edit_portfolio_form");

        $.ajax({
            url:"{{route('portfolio-store')}}",
            type:"post",
            cache: false,
            contentType: false,
            processData: false,
            dataType: "JSON",
            data:data,
            xhr: function() {
                myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            success:function(data,detail){
                if(detail == false){
                $("#createDeal").modal('hide');
                // getEmployeeData(1);
                Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Updated Successfully',
                showConfirmButton: false,
                timer: 1500
                })
            }else{
                $(".services:first").trigger("click");
                    $('#EditCandidates').modal('hide');
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Added Successfully, Reload the page to see the changes',
                        showConfirmButton: true
                    })
            }
            },
            error:function(data){
          $.each(data.responseJSON.errors, function(id,msg){
            $('#error_'+id).html(msg);
          });
            }

            })
     })
     $(".ck-file-dialog-button").hide();
    $(".ck-dropdown__button").hide();

</script>
