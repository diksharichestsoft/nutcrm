@section('contents')
<style>
    .daterangepicker .ranges li {font-size: 12px;padding: 8px 12px;cursor: pointer;background: #000000b0;}
  </style>
<div class="card" id="data">
    <div class="card-header p-2" id="card_head">
      <ul class="nav nav-pills">
        <div id="reportrange" style="background: transparent; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
        <i class="fa fa-calendar"></i>&nbsp;
        <span class="ab"></span> <i class="fa fa-caret-down"></i>
       </div>
       <input type="hidden" id="start_date_range" >
       <input type="hidden" id="end_date_range">
       <li style="align-self: center;">
           <h3>
           </h3>
       </li>
        <li class="nav-item search-right">
          <div>
          </div>
         <div class="search_bar">
            <div class="input-group" data-widget="sidebar-search">
             <input class="form-control form-control-sidebar" id="search" type="search" placeholder="Search" aria-label="Search">
            </div>
         </div>
        </li>
      </ul>
    </div>
</div>


<div id="pagination_employee">
    @include('admin.portfolio.includes.view')
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
{{-- <script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/inline/ckeditor.js"></script> --}}

<script>
function updateCompanyProject(e)
{
  $('#page-loader').show();

  var id=e.getAttribute('data-id');
  $.ajax({
    url:"{{route('portfolio-edit')}}",
    type:"get",
    data:{id:id},
    success:function(data)
    {
      $('#page-loader').hide();
      $('#pagination_employee').empty().html(data);
    },
    error:function(){
      $('#page-loader').hide();
    }
  })
}

$(function() {
    var start = moment().subtract(moment().year()- 1947, 'years');
  var end = moment();
  function cb(start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      $('#start_date_range').val(start.format('YYYY-MM-DD'));
      $('#end_date_range').val(end.format('YYYY-MM-DD'));
      getEmployeeData(1);
  }

  $('#reportrange').daterangepicker({
      startDate: start,
      endDate: end,
      ranges: {
         'Today': [moment(), moment()],
         'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
         'Last 7 Days': [moment().subtract(6, 'days'), moment()],
         'Last 30 Days': [moment().subtract(29, 'days'), moment()],
         'This Month': [moment().startOf('month'), moment().endOf('month')],
         'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
  }, cb);
  cb(start, end);

  });
  document.querySelector("#search").addEventListener("keyup",(e)=>{
      getEmployeeData(1);
  });

    $(document).on('click', '.pagination a', function(event){
  event.preventDefault();
  var page = $(this).attr('href').split('page=')[1];
  var sendurl=$(this).attr('href');
  getEmployeeData(page);
});
function getEmployeeData(page)
{
  $('#page-loader').show();
//   page = (page='')?'1':page;
    var start_date=document.querySelector('#start_date_range').value;
    var end_date=document.querySelector('#end_date_range').value;
    var search=document.querySelector("#search").value;
    var data={search:search,start_date:start_date,end_date:end_date};
  var make_url= "{{url('/')}}/admin/portfolio/page?page="+page;
  $.ajax({
    url:make_url,
    data:data,
    success:function(data)
    {
    $('#pagination_employee').empty().html(data);
    $('#page-loader').hide();
    },
    error:function(error){
      $('#page-loader').hide();

    }
  });
  }


</script>
@endsection
