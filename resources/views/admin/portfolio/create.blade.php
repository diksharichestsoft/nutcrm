@extends('admin.layout.template')
@section('contents')
{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css"> --}}

<style>
     .select2-selection{
         background-color:#343a40 !important;
     }

.my_tag_input .accordion {
    margin-bottom:-3px;
}

.my_tag_input .accordion-group {
    border: none;
}

.my_tag_input .twitter-typeahead .tt-query,
.my_tag_input .twitter-typeahead .tt-hint {
    margin-bottom: 0;
}

.my_tag_input .twitter-typeahead .tt-hint
{
    display: none;
}

.my_tag_input .tt-menu {
    position: absolute;
    top: 100%;
    left: 0;
    z-index: 1000;
    display: none;
    float: left;
    min-width: 160px;
    padding: 5px 0;
    margin: 2px 0 0;
    list-style: none;
    font-size: 14px;
    border: 1px solid rgba(0, 0, 0, 0.15);
    border-radius: 4px;
    -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, 0.175);
    box-shadow: 0 6px 12px rgba(0, 0, 0, 0.175);
    background-clip: padding-box;
    cursor: pointer;
}

.my_tag_input .tt-suggestion {
    display: block;
    padding: 3px 20px;
    clear: both;
    font-weight: normal;
    line-height: 1.428571429;
    color: #333333;
    white-space: nowrap;
}

.my_tag_input .tt-suggestion:hover,
.my_tag_input .tt-suggestion:focus {
  color: #ffffff;
  text-decoration: none;
  outline: 0;
  background-color: #428bca;
}

.my_tag_input .bootstrap-tagsinput{
    background-color: #333333 !important;
}
</style>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Add PortFolio</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <div id="validation-errors"></div>
                    <form method="post" id="add_user_form" action="" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                        <div class="form-group">
                            <label for="prject_name">Project Name</label>
                            <input type="text" name="project_name" class="form-control">

                            @error('company_id')
                                <div class="error error-msg-red">{{ $message }}</div>
                            @enderror
                        </div>

                        <input type="hidden" name="reqType" value="0">
                        <div class="form-group">
                            <label for="country" class="col-sm-2 col-form-label">Country</label>
                            <div class="col-sm-10">
                              <select name="country" id="country" class="selectpicker" data-live-search="true">
                                <option value="">Select Country</option>
                                @foreach($countries as $country)
                                <option value="{{ $country->id }}" {{ old('project_type') == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                                @endforeach
                              </select>
                              <div class="error" id="error_country"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="department" class="col-sm-2 col-form-label">Department</label>
                            <div class="col-sm-10">
                              <select name="department" id="department" class="selectpicker" data-live-search="true">
                                <option value="">Select Department</option>
                                @foreach($departments as $departments)
                                <option value="{{ $departments->id }}" {{ old('project_type') == $departments->id ? 'selected' : '' }}>{{ $departments->name }}</option>
                                @endforeach
                              </select>
                              <div class="error" id="error_department"></div>
                            </div>

                        </div>

                        <div class="form-group">
                            <label for="web_url">Web URL</label>
                            <input type="text" class="form-control" name="web_url" >
                        </div>
                        <div class="form-group">
                            <label for="android_url">Android URL</label>
                            <input type="text" class="form-control" name="android_url" >
                        </div>
                        <div class="form-group">
                            <label for="ios_url">Ios URL</label>
                            <input type="text" class="form-control" name="ios_url">
                        </div>
                        <div class="form-group">
                            <label for="ios_url">Admin URL(Add access of admin in description if any)</label>
                            <input type="text" class="form-control" name="admin_url">
                        </div>

                        <div class="form-group">
                            <label for="description">Description</label>
                            <input type="hidden" name="description"  id="description">
                            <div id="editor" style="border: 1px solid #ffffff42;"></div>

                        </div>
                        <div class="form-group">
                            <label for="description">Business Model</label>
                            <textarea type="text" class="form-control" name="busniness_model"> </textarea>
                        </div>
                        @include('admin.portfolio.includes.tags')

                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!--  -->
            </div>
        </div>

    </div><!--/. container-fluid -->


</section>
<script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/inline/ckeditor.js"></script>
<script>
    var myEditor;

    InlineEditor
    .create( document.querySelector( '#editor' ) )
    .then( editor => {
        console.log( 'Editor was initialized', editor );
        myEditor = editor;
    } )
    .catch( err => {
        console.error( err.stack );
    } );

</script>
    <!-- /.content -->
    <script>
function getFormWithImage(id) {
            let form = document.querySelector(`#${id}`);
            let data = new FormData(form);
            return data;
        }
        $("#add_user_form").on('submit', function (e) {
            e.preventDefault();
            var descData = (myEditor.getData());
            $("#description").val(descData);
            var data = getFormWithImage("add_user_form");
            console.log(data);
            $.ajax({
                url:"{{route('portfolio-store')}}",
                type:"post",
                cache: false,
                contentType: false,
                processData: false,
                dataType: "JSON",
                data:data,
                xhr: function() {
                    myXhr = $.ajaxSettings.xhr();
                    return myXhr;
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                success:function(data){
                    window.location.href = "{{route('portfolio')}}"
                },
                error:function(data){
                    data =data.responseJSON.errors;
                    for (const [key, value] of Object.entries(data)) {
                        $('#validation-errors').append('<div class="alert alert-warning alert-dismissible fade show" role="alert"><strong>Error: </strong>'+value+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                        }
                        $("html, body").animate({ scrollTop: 0 }, "slow");
                }

            });
        });
// Hiding some Ckediotr buttons
    $(".ck-file-dialog-button").hide();
    $(".ck-dropdown__button").hide();


    </script>

<script>
    $(".selectpicker").selectpicker({
            "title": "Select Options"
        }).selectpicker("render");

    $(document).ready(function (){
    $(".ck-file-dialog-button").hide();
    $(".ck-dropdown__button").hide();
    })
</script>
@endsection

