<style>
    .daterangepicker .ranges li {font-size: 12px;padding: 8px 12px;cursor: pointer;background: #000000b0;}
  </style>
  <div class="card" id="data">
                <div class="card-header p-2" id="card_head">
                  <ul class="nav nav-pills">

                    @foreach($companies as $eachCompany)
                    <li class="nav-item"><a class="nav-link services" href="#view" onclick="fetch_purchase_invoices(1,this.getAttribute('data-id'))" data-id="{{$eachCompany->id}}" data-toggle="tab">{{$eachCompany->name}}</a></li>&nbsp;
                    @endforeach
                    <li class="nav-item"><a class="nav-link services" href="#view" onclick="fetch_purchase_invoices(1,this.getAttribute('data-id'))" data-id="-1" data-toggle="tab">All Companies</a></li>&nbsp;
                    {{-- <li class="nav-item"><a class="nav-link services" href="#view" onclick="fetch_purchase_invoices(1,this.getAttribute('data-id'))" data-id="-1" data-toggle="tab">All Tasks</a></li>&nbsp; --}}
                    <div id="reportrange" style="background: transparent; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                    <i class="fa fa-calendar"></i>&nbsp;
                    <span class="ab"></span> <i class="fa fa-caret-down"></i>
                   </div>
                   <input type="hidden" id="start_date_range">
                   <input type="hidden" id="end_date_range">
                   <li style="align-self: center;">
                       <h3>
                       </h3>
                   </li>
                    <li class="nav-item search-right">
                        @can('invoices_purchase_create')

                        <div>
                            <a href="javascript:void(0)" id="addInvoiceButton"  class="btn btn-primary" >New Invoice</a>
                        </div>
                        @endcan
                     <div class="search_bar">
                        <div class="input-group" data-widget="sidebar-search">
                         <input class="form-control form-control-sidebar" id="search" type="search" placeholder="Search" aria-label="Search">
                        </div>
                     </div>
                    </li>
                  </ul>
                <ul class="nav nav-pills">
                    <li class="nav-item"><a class="nav-link statusCheck" href="#view" onclick="fetch_purchase_invoices(1,'',this.getAttribute('data-id'))" data-id="1" data-toggle="tab">Unchecked</a></li>&nbsp;
                    <li class="nav-item"><a class="nav-link statusCheck" href="#view" onclick="fetch_purchase_invoices(1,'',this.getAttribute('data-id'))" data-id="2" data-toggle="tab">Checked</a></li>&nbsp;
                    <li class="nav-item"><a class="nav-link statusCheck" href="#view" onclick="fetch_purchase_invoices(1,'',this.getAttribute('data-id'))" data-id="all" data-toggle="tab">All</a></li>&nbsp;
                </ul>

                </div><!-- /.card-header -->
                <div class="card-body">
                  <div class="tab-content">
                    <div class="tab-pane" id="profile">
                       {{-- @include('admin.permissions.includes.addform') --}}
                    </div>

                    <div class="active tab-pane" id="view">
                      </div>
                    <!-- /.tab-pane -->
                  </div>
                  <!-- /.tab-content -->
                </div><!-- /.card-body -->
        </div>
  <div class="modal fade" id="createDeal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Invoice</h5>
          <button type="button" class="close" id='createDealHide' data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body " id="createDealBody">
        </div>
      </div>
    </div>
  </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script>
    $(document).ready(function(){
      $(".services").eq(0).addClass("active");
      $(".statusCheck").eq(0).addClass("active");
    });
    $(document).on('click','.comment',function(){
      var id = $(this).attr('data-id');
      // alert(id);
      $('#deal_id').val(id);
      $('#comment_title').val('');
      $('#comment').val('');

    });

  $(document).on('submit','#addPurchaseInvoice',function(e){
    e.preventDefault();
    Swal.showLoading()
    var descData = (detailsEditor.getData());
    $("#details").val(descData);
    var data = new FormData(this);
      $.ajax({
          type:'post',
          url:"{{route('invoices-purchase-store')}}",
          dataType: "JSON",
         xhr: function() {
               myXhr = $.ajaxSettings.xhr();
               return myXhr;
         },
         cache: false,
         contentType: false,
         processData: false,
          data:data,
          success:function(data){
            Swal.close()
              $('#createDeal').modal('hide');
              fetch_purchase_invoices(1, '');
              Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Added Successfully',
                showConfirmButton: false,
                timer: 1500
                })

          },
          error:function(data){
            Swal.close()
            Swal.fire({
                icon: 'error',
                title: 'Oops, Something went wrong',
                showConfirmButton: true,
                })
            $.each(data.responseJSON.errors, function(id,msg){
              $('#error_'+id).html(msg);
            })
          }
        });
      });
  $(document).on('submit','#updatePurchaseInvoice',function(e){
    e.preventDefault();
    Swal.showLoading()
    var descData = (detailsEditor.getData());
    $("#details").val(descData);
    var data = new FormData(this);
      $.ajax({
          type:'post',
          url:"{{route('invoices-purchase-update')}}",
          dataType: "JSON",
         xhr: function() {
               myXhr = $.ajaxSettings.xhr();
               return myXhr;
         },
         cache: false,
         contentType: false,
         processData: false,
          data:data,
          success:function(data){
            Swal.close()
              $('#createDeal').modal('hide');
              fetch_purchase_invoices(1, '');
              Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Added Successfully',
                showConfirmButton: false,
                timer: 1500
                })

          },
          error:function(data){
            Swal.close()
            $.each(data.responseJSON.errors, function(id,msg){
              $('#error_'+id).html(msg);
            })
          }
        });
      });

    $(document).on('change','#selectsatus',function(){
        Swal.fire({
                title: `Change Status to ${$(this).val() == 1 ? 'Unchecked' : 'Checked'}` ,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes!'
                }).then((result) => {
                if (result.isConfirmed) {
                    data = {id: $(this).data('id'), status: $(this).val()}
                    fetch(`{{route('invoices-purchase-change-status')}}`,
                    {
                        method: 'POST', // or 'PUT'
                        headers: {
                            'Content-Type': 'application/json',
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        body: JSON.stringify(data),
                    }).then((result)=>{
                        console.log(result)
                        result.json().then((json)=>{
                            fetch_purchase_invoices(1,'')
                            if(json.success){
                                Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Changed Successfully',
                                showConfirmButton: false,
                                timer: 1500
                                })
                            }else{
                                Swal.fire('error', '', 'error')
                            }
                        })
                    }).catch((error)=>{
                        Swal.fire('error', '', 'error')
                        console.log(error)
                    })
                }
                })

    });

  function removeTask(e)
  {
    var id=e.getAttribute('data-id');
    swal({
    title: "Oops....",
    text: "Are You Sure You want to delete!",
    icon: "error",
    buttons: [
      'NO',
      'YES'
    ],
  }).then(function(isConfirm) {
    if (isConfirm) {
      $.ajax({
        url:"{{route('manageTasks-remove-data')}}",
        type:"post",
        data:{id:id},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        success:function(data){
          $('#view').empty().html(data);
          fetch_purchase_invoices(1,'');
        }
      })
    } else {

    }
  });
  }

//   function addPurchaseInvoice()
$("#addInvoiceButton").on('click', ()=>{
    $('#page-loader').show();

    $.ajax({
      url:"{{route('invoices-purchase-add')}}",
      type:"get",
      data: {},
      success:function(data)
      {
          $("#createDeal").modal('show')
        $('#page-loader').hide();
        $('#createDealBody').empty().html(data);
      },
      error:function(error){
        $('#page-loader').hide();
      }
    })
})
function updatePurchaseInvoice(button){
    $('#page-loader').show();
    id = $(button).data('id')
    $.ajax({
      url:"{{route('invoices-purchase-edit')}}",
      type:"get",
      data: {id},
      success:function(data)
      {
          $("#createDeal").modal('show')
        $('#page-loader').hide();
        $('#createDealBody').empty().html(data);
      },
      error:function(error){
        $('#page-loader').hide();
      }
    })
}


  // run on  change date start to end

  $(function() {
  var start = moment().subtract(29, 'days');
  var end = moment();
  function cb(start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      $('#start_date_range').val(start.format('YYYY-MM-DD'));
      $('#end_date_range').val(end.format('YYYY-MM-DD'));
      fetch_purchase_invoices(1,'');
  }

  $('#reportrange').daterangepicker({
      startDate: start,
      endDate: end,
      ranges: {
         'Today': [moment(), moment()],
         'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
         'Last 7 Days': [moment().subtract(6, 'days'), moment()],
         'Last 30 Days': [moment().subtract(29, 'days'), moment()],
         'This Month': [moment().startOf('month'), moment().endOf('month')],
         'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
  }, cb);
  cb(start, end);

  });

  $(document).on('click', '.pagination a', function(event){
    event.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    var sendurl=document.getElementsByClassName('services active')[0].getAttribute('data-id');
    fetch_purchase_invoices(page,sendurl,'');

  });
  document.querySelector("#search").addEventListener("keyup",(e)=>{
      // var active=$('a.active.services').attr('data-id');
      fetch_purchase_invoices(1,'');
  });

  function fetch_purchase_invoices(page,active, status = '')
  {
    $('#page-loader').show();

    var start_date=document.querySelector('#start_date_range').value;
    var end_date=document.querySelector('#end_date_range').value;
    if(active==''){
      var active=document.getElementsByClassName('services active')[0].getAttribute('data-id');
    }
    if(status ==''){
      var status=document.getElementsByClassName('statusCheck active')[0].getAttribute('data-id');
    }

    var search=document.querySelector("#search").value;
    var data={search:search,active:active,start_date:start_date,end_date:end_date, status: status};
    var make_url= "{{route('invoices-purchase-filter')}}"+'?page='+page;
    $.ajax({
      url:make_url,
      data:data,
      success:function(data)
      {
          $('#view').empty().html(data);
        $('#page-loader').hide();

      },
      error:function(error){
        $('#page-loader').hide();
      }
    });
    }

    function leadingZeros(input) {
    if(!isNaN(input.value) && input.value.length === 1) {
      input.value = '0' + input.value;
    }
  }



  </script>
  <script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/inline/ckeditor.js"></script>

