<style>
    select#selectsatus {
      width: 100%;
  }
  </style>

  <div class="card">
    <div class="card-header ">
      <h3 class="card-title">Sales Invoices</h3>
      <h6 class="count text-right">Total Invoices : {{$invoices->total()}}</h6>
    </div>
                <!-- /.card-header -->
               <div class="card-body">
                  <table class="table table-bordered">
                    @if(count($invoices)>0)
                    <thead>
                      <tr>
                        <th style="width: 10px">Action</th>
                        <th>ID</th>
                        <th>Title</th>
                        <th>GSt Amount</th>
                        <th>Total_amount</th>
                        <th>Warranty</th>
                        <th>Date</th>
                        <th>Created By</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    @endif
                    <tbody>
                          @forelse($invoices as $key=>$eachInvoice)
                             <tr>
                                  <td>
                                    <div class="btn-group">
                                        @can('task_management_view')
                                        <a href="{{route('invoices-purchase-detial-view',$eachInvoice->id)}}" target="_blank" class="btn btn-outline-success btn-xs  view"><i class="fas fa-eye"></i></a>
                                        @endcan
                                        <button data-id="{{$eachInvoice->id}}" onclick="updatePurchaseInvoice(this)" class="btn btn-outline-success btn-xs  update" ><i class="fas fa-pencil-alt"></i></button>
                                            <a target="_blank" data-id="{{$eachInvoice->id}}" data-type="0" href="{{ asset('purchaseInvoices/'. $eachInvoice->file)}}" download class="btn btn-success btn-sm">
                                                <i class="fa fa-download" aria-hidden="true"></i>
                                            </a>
                                    </div>
                                  </td>
                                <td>{{$eachInvoice->id??''}}</td>
                                <td>{{$eachInvoice->title??''}}</td>
                                <td>{{getCurrencySymbol($eachInvoice->currency)}}{{$eachInvoice->gst_amount??''}}</td>
                                <td>{{getCurrencySymbol($eachInvoice->currency)}}{{$eachInvoice->total_amount??''}}</td>
                                <td>{{($eachInvoice->warranty == 1) ? 'Yes' : 'No'}}</td>
                                <td>{{date('d-M-Y',strtotime($eachInvoice->date))??''}}</td>
                                <td>{{$eachInvoice->createdBy->name ?? ''}}</td>
                                <td class="d-flex">
                                    <select name="sel" id="selectsatus" class="form-control deals_status" data-id="{{ $eachInvoice->id }}" >
                                      <option value="1" {{($eachInvoice->status==1)?"selected":""}}>Unchecked</option>
                                      <option value="2" {{($eachInvoice->status==2)?"selected":""}}>Checked</option>
                                  </select>
                              </td>

                             </tr>
                          @empty
                            <center> <h3> No Data Available </h3> </center>
                          @endforelse
                    </tbody>
                  </table>
                </div>
                {{$invoices->links()}}
                <!-- /.card-body -->
              </div>







    <script>

      function save_deal_data()
      {
          var active=$('a.active.services').attr('data-id');
          var start_date=document.querySelector('#start_date_range').value;
          var end_date=document.querySelector('#end_date_range').value;
      }
    </script>
