<form action="" name="" id="updatePurchaseInvoice" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="id" value="{{$thisInvoice->id}}">
  <div class="value">
      <div class="form-group row">
          <label for="company_id" class="col-sm-2 col-form-label">Select Company</label>
          <div class="col-sm-10">
            <select required name="company_id" id="company_id"  class="selectpicker" data-live-search="true">
              <option value="">Select Company</option>
              @foreach($companies as $eachCompany)
                <option value="{{ $eachCompany->id }}" {{($thisInvoice->company_id == $eachCompany->id) ? 'selected' : ''}}>{{ $eachCompany->name}}</option>
              @endforeach
            </select>
              <div class="error" id="error_company_id"></div>
          </div>
      </div>
      <div class="form-group row">
        <label for="title" class="col-sm-2 col-form-label">Invoice Title</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="title" value="{{$thisInvoice->title ?? ''}}" required name="title" >
          <div class="error" id="error_title"></div>
          </div>
      </div>
      <div class="form-group row">
          <label for="total_amount" class="col-sm-2 col-form-label">Total Amount</label>
          <div class="col-sm-3">
              <select name="currency" required class="form-control" id="">
                  @foreach ($currencies as $key=>$eachCurrency)
                  <option {{($key != $thisInvoice->currency) ?: 'selected' }} value="{{$key}}">{{$key}}({{getCurrencySymbol($key)}})</option>
                  @endforeach
              </select>
              <div class="error" id="error_total_amount"></div>
          </div>
          <div class="col-sm-5">
              <input name="total_amount" required type="number" value="{{$thisInvoice->total_amount ?? ''}}" class="form-control"/>
              <div class="error" id="error_total_amount"></div>
          </div>
      </div>
      <div class="form-group row">
          <label for="gst_amount" class="col-sm-2 col-form-label">GST Amount</label>
            <div class="col-sm-10">
                <input type="number" class="form-control" name="gst_amount" value="{{$thisInvoice->total_amount ?? ''}}" id="gst_amount">
            </div>
        </div>
    <div class="form-group row form-switch">
      <label for="warranty" class="col-sm-2 col-form-label">Warranty?</label>
      <div class="col-sm-10">
          <input type="radio" id="html" name="warranty" value="1" {{$thisInvoice->warranty == 1 ? "checked" : '' }}>
          <label for="warranty" class="col-sm-2 col-form-label">Yes</label>
          <input type="radio" id="html" name="warranty" value="0" {{$thisInvoice->warranty == 0 ? "checked" : '' }}>
          <label for="warranty" class="col-sm-2 col-form-label" >No</label>
        <div class="error" id="error_warranty"></div>
        </div>
    </div>
    <div class="form-group row">
      <label for="file" class="col-sm-2 col-form-label">File</label>
      <div class="col-sm-10">
          <input type="file" id="file" class="form-control" name="file">
        <div class="error" id="error_file"></div>
        </div>
    </div>


    <div class="form-group row">
      <label for="details" class="col-sm-2 col-form-label">Details</label>
        <div class="col-sm-10">
            <input type="hidden" name="details"  id="details">
            <div id="detailsEditor" style="border: 1px solid #ffffff42;">{!!$thisInvoice->details ?? '' !!}</div>
        </div>
    </div>


  <div class="form-group row">
      <label for="date" class="col-sm-2 col-form-label">Select Date</label>
      <div class="col-sm-10">
          <input type="date" id="date" class="form-control" required value="{{$thisInvoice->date ?? ''}}" name="date">
          <div class="error" id="error_date"></div>
        </div>
    </div>

    <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-success">Add Task</button>
        <button type="reset" class="btn btn-danger">Reset</button>
      </div>
    </div>
  </div>
  </form>
  <script>
      $(".selectpicker").selectpicker({
              "title": "Select Options"
          }).selectpicker("render");
      var detailsEditor;

      InlineEditor
      .create( document.querySelector( '#detailsEditor' ) )
      .then( editor => {
          console.log( 'Editor was initialized', editor );
          detailsEditor = editor;
      } )
      .catch( err => {
          console.error( err.stack );
      } );
      $(document).ready(function (){
      $(".ck-file-dialog-button").hide();
      $(".ck-dropdown__button").hide();
      })
  </script>
