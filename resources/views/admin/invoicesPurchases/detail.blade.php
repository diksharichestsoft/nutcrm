@extends('admin.layout.template')
@section('contents')
<div id="permissions_data">

    <h5>Invoice</h5>
    <div class="accordion" id="accordionExample">
        <div class="card">
          <div class="card-header" id="headingOne">
            <h2 class="mb-0">
              <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                Invoice
              </button>
            </h2>
          </div>
          <div id="collapse1" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body">
                <table class="table">
                    <tbody>
                        <tr>
                            <th scope="row">Invoice Title</th>
                            <td>
                                {{$thisInvoice->title ?? ''}}
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">Company</th>
                            <td>
                                {{$thisInvoice->company->name ?? ''}}
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">Amount</th>
                            <td>
                                {{getCurrencySymbol($thisInvoice->currency)}}{{$thisInvoice->total_amount ?? ''}}
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">GST Amount</th>
                            <td>
                                {{getCurrencySymbol($thisInvoice->currency)}}{{$thisInvoice->gst_amount ?? ''}}
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">Invoice Date</th>
                            <td>
                                {{date('d-M-Y', strtotime($thisInvoice->date)) ?? ''}}
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">Warranty</th>
                            <td>
                                {{($thisInvoice->warranty == 0 ? "No" : "Yes" ) ?? ''}}
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">File Download</th>
                            <td>
                                <a class="btn btn-success" href="{{asset('purchaseInvoices/'.$thisInvoice->file)}}" download>
                                    Download
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">Details</th>
                            <td>
                                {!!$thisInvoice->details ?? ''!!}
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">Created</th>
                            <td>
                                {{timestampToDateAndTimeObj($thisInvoice->created_at)->date ?? ''}}
                                <br>
                                {{timestampToDateAndTimeObj($thisInvoice->created_at)->time ?? ''}}
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">Created By</th>
                            <td>
                                {{$thisInvoice->createdBy->name?? ''}}
                            </td>
                        </tr>

                    </tbody>
                </table>
                        </div>
          </div>
        </div>
      </div>

<!-- Content Wrapper. Contains page content -->
<!-- <div class="content-wrapper"> -->
    <!-- Content Header (Page header) -->

    <!-- Main content -->
</div>

@endsection
