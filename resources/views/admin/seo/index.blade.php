@section('contents')
<style>
    .daterangepicker .ranges li {font-size: 12px;padding: 8px 12px;cursor: pointer;background: #000000b0;}
  </style>
<div class="card" id="data">
    <div class="card-header p-2" id="card_head">
      <ul class="nav nav-pills">
        <div id="reportrange" style="background: transparent; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
        <i class="fa fa-calendar"></i>&nbsp;
        <span class="ab"></span> <i class="fa fa-caret-down"></i>
       </div>
       <input type="hidden" id="start_date_range" >
       <input type="hidden" id="end_date_range">
       <li style="align-self: center;">
           <h3>
           </h3>
       </li>
        <li class="nav-item search-right">
            <div>
                {{-- @can('targets_add') --}}
                <a href="javascript:void(0)" onclick="addSeo()"  class="btn btn-primary" ><i class="fa fa-plus"></i> Add Task</a>
                {{-- @endcan --}}
            </div>
 <div class="search_bar">
            <div class="input-group" data-widget="sidebar-search">
             <input class="form-control form-control-sidebar" id="search" type="search" placeholder="Search" aria-label="Search">
            </div>
         </div>
        </li>
      </ul>
    </div>
</div>



<div id="view">
    {{-- @include('admin.targets.includes.view') --}}
</div>






{{-- Created Modal --}}
<div class="modal fade" id="targetModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Projects</h5>
          <button type="button" class="close" id='createDealHide' data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body " id="targetModalBody">
        </div>
      </div>
    </div>
  </div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/inline/ckeditor.js"></script>

<script>
  function addSeo()
  {
    $('#page-loader').show();

    $.ajax({
      url:"{{route('seo-add')}}",
      type:"get",
      success:function(data)
      {
        $("#targetModal").modal('show')
        $('#page-loader').hide();
        $('#targetModalBody').empty().html(data);
      },
      error:function(error){
        $('#page-loader').hide();
      }
    })
  }
  function editTarget(e)
  {
    $('#page-loader').show();
    const id = $(e).data('id');
    $.ajax({
      url:"{{route('seo-edit')}}",
      type:"get",
      data: {id},
      success:function(data)
      {
        $("#targetModal").modal('show')
        $('#page-loader').hide();
        $('#targetModalBody').empty().html(data);
      },
      error:function(error){
        $('#page-loader').hide();
      }
    })
  }
//   function removeTarget(e)
//   {
//     $('#page-loader').show();
//     const id = $(e).data('id');
//     $.ajax({
//       url:"{{route('targets-remove')}}",
//       type:"post",
//       data: {id},
//       success:function(data)
//       {
//         $("#targetModal").modal('show')
//         $('#page-loader').hide();
//         $('#targetModalBody').empty().html(data);
//         Swal.fire({
//                 position: 'top-end',
//                 icon: 'success',
//                 title: data.message,
//                 showConfirmButton: false,
//                 timer: 1500
// 	          })
//       },
//       error:function(error){
//         $('#page-loader').hide();
//         Swal.fire({
//                 position: 'top-end',
//                 icon: 'error',
//                 title: data.message ?? "Failed to remove",
//                 showConfirmButton: false,
//                 timer: 1500
// 	          })
//       }
//     })
//   }
function removeTarget(e)
  {
    var id=e.getAttribute('data-id');
    swal({
    title: "Oops....",
    text: "Are You Sure You want to delete!",
    icon: "error",
    buttons: [
      'NO',
      'YES'
    ],
  }).then(function(isConfirm) {
    if (isConfirm) {
      $.ajax({
        url:"{{route('seo-remove')}}",
        type:"post",
        data:{id:id},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        success:function(data){
          getTargetData(1);
          Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Remove Successfully',
                showConfirmButton: false,
                timer: 1500
                })
        },
        error:function(error){
        $('#page-loader').hide();
        Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: error.message ?? "Failed to remove",
                showConfirmButton: false,
                timer: 1500
	          })
        }
      })
    } else {

    }
  });
  }





$(function() {
    var start = moment().subtract(moment().year()- 1947, 'years');
  var end = moment();
  function cb(start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      $('#start_date_range').val(start.format('YYYY-MM-DD'));
      $('#end_date_range').val(end.format('YYYY-MM-DD'));
      getTargetData(1);
  }

  $('#reportrange').daterangepicker({
      startDate: start,
      endDate: end,
      ranges: {
         'Today': [moment(), moment()],
         'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
         'Last 7 Days': [moment().subtract(6, 'days'), moment()],
         'Last 30 Days': [moment().subtract(29, 'days'), moment()],
         'This Month': [moment().startOf('month'), moment().endOf('month')],
         'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
  }, cb);
  cb(start, end);

  });
  document.querySelector("#search").addEventListener("keyup",(e)=>{
      getTargetData(1);
  });

    $(document).on('click', '.pagination a', function(event){
  event.preventDefault();
  var page = $(this).attr('href').split('page=')[1];
  var sendurl=$(this).attr('href');
  getTargetData(page);
});
function getTargetData(page)
{
  $('#page-loader').show();
//   page = (page='')?'1':page;
    var start_date=document.querySelector('#start_date_range').value;
    var end_date=document.querySelector('#end_date_range').value;
    var search=document.querySelector("#search").value;
    var data={search:search,start_date:start_date,end_date:end_date};
  var make_url= "{{url('/')}}/admin/seo/search?page="+page;
  $.ajax({
    url:make_url,
    data:data,
    success:function(data)
    {
    $('#view').empty().html(data);
    $('#page-loader').hide();
    },
    error:function(error){
      $('#page-loader').hide();

    }
  });
  }
  //   Function to submit Target
$(document).on('submit','#addTargetForm',function(e){
    e.preventDefault();
    var descData = (myEditor.getData());
    $("#description").val(descData);
    var data = new FormData(this);
      $.ajax({
          type:'post',
          url:"{{route('seo-store')}}",
          dataType: "JSON",
         xhr: function() {
               myXhr = $.ajaxSettings.xhr();
               return myXhr;
         },
         cache: false,
         contentType: false,
         processData: false,
          data:data,
          success:function(data){
              $(".services:first").trigger( "click" );
              $('#targetModal').modal('hide');
              getTargetData(1);
              Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Added Successfully',
                showConfirmButton: false,
                timer: 1500
	          })
          },
          error:function(data){
            $.each(data.responseJSON.errors, function(id,msg){
              $('#error_'+id).html(msg);
            })
          }
        });
      });


</script>
@endsection
