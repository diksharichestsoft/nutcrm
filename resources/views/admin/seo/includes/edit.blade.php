<form action="" id="addTargetForm" enctype="multipart/form-data">
    @csrf
  <div class="value">
      <input type="hidden" name="id" value="{{$thisTask->id}}">
      <div class="form-group row">
      <label for="title" class="col-sm-2 col-form-label">Title *</label>
      <div class="col-sm-10">
          <input type="text" name="title" value="{{$thisTask->title ?? ''}}" class="form-control">
          <div class="error" id="error_title"></div>
      </div>
      </div>

      <div class="form-group row">
        <label for="description" class="col-sm-2 col-form-label">Description</label>
          <div class="col-sm-10">
              <input type="hidden" name="description"  id="description">
              <div id="editor2" style="border: 1px solid #ffffff42;">{!!$thisTask->description!!}</div>
          </div>
      </div>
      <div class="form-group row">
          <label for="task_type_id" class="col-sm-2 col-form-label">Type</label>
          <div class="col-sm-10">
              <select name="type" id="type" class="form-control select2">
              <option value="">Select Type</option>
              @foreach ($taskTypes as $item)
                  <option value="{{$item->id}}" {{$thisTask->type == $item->id ? "selected" : ""}}>{{$item->title}}</option>
              @endforeach
              </select>
              <div class="error" id="error_type"></div>
          </div>
      </div>



  </div>

    <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-success">Add Target</button>
        <button type="reset" class="btn btn-danger">Reset</button>
      </div>
    </div>
  </div>

  </form>
  <script>
      var myEditor;

      InlineEditor
      .create( document.querySelector( '#editor2' ) )
      .then( editor => {
          console.log( 'Editor was initialized', editor );
          myEditor = editor;
      } )
      .catch( err => {
          console.error( err.stack );
      } );
      $(document).ready(function (){
      $(".ck-file-dialog-button").hide();
      $(".ck-dropdown__button").hide();
      })
  </script>
<script>
  $(".selectpicker").selectpicker({
          "title": "Select Options"
      }).selectpicker("render");
  var myEditor;

  InlineEditor
  .create( document.querySelector( '#editor' ) )
  .then( editor => {
      console.log( 'Editor was initialized', editor );
      myEditor = editor;
  } )
  .catch( err => {
      console.error( err.stack );
  } );
  $(document).ready(function (){
  $(".ck-file-dialog-button").hide();
  $(".ck-dropdown__button").hide();
  })
</script>
