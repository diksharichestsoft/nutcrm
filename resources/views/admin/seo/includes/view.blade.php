   <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card-body">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width: 10px">#</th>
                          <th>Action</th>
                          <th>Title</th>
                          <th>Description</th>
                          <th>Type</th>

                        </tr>
                      </thead>
                      <tbody>

                        @forelse($seoEmployees as $key=>$seoEmployee)
                          <tr>
                              <td>{{$key +1}}</td>
                              <td>
                                    <div class="btn-group">
                                        <a href="{{route('seo-detail',$seoEmployee->id)}}" target="_blank" class="btn btn-outline-success btn-xs  view"><i class="fas fa-eye"></i></a>
                                      <button data-id="{{$seoEmployee->id}}" onclick="editTarget(this)" class="btn btn-outline-success btn-xs  update"><i class="fas fa-pencil-alt"></i></button>
                                      <button data-id="{{$seoEmployee->id}}" onclick="removeTarget(this)" class="btn btn-danger btn-xs  remove"><i class="fas fa-times"></i></button>
                                    </div>
                              </td>
                            <td>
                                <p>{{$seoEmployee->title  ?? ''}} </p>
                            </td>
                            <td>
                                {!! $seoEmployee->description !!}
                            </td>


                            <td>
                                <p>{{$seoEmployee->seo_task_type_title}} </p>
                        </td>




                        </tr>
                        @empty
                        <tr>
                            <td colspan="9">
                                <p class="text-center h4">
                                    No Data Found
                                </p>
                            </td>
                        </tr>
                        @endforelse

                      </tbody>
                    </table>
                </div>

                <!--  -->
            </div>
        </div>
        {{$seoEmployees->links()}}
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
    <script>


        $(".toggle_user_login").on('click', function (e){
            e.preventDefault();
            var id = $(this).attr('data-id');
            $.ajax({
                type: "post",
                url: "{{route('employee-login-toggle')}}",
                headers:
                {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {id:id},
                dataType: "JSON",
                success: function (response) {

                      document.querySelector(`#${e.target.id}`).checked= response.success;
                }
            });
        })
    $(document).on('click', '#post_pagination .pagination a', function(event){
 event.preventDefault();
 var page = $(this).attr('href').split('page=')[1];
 var filter=document.querySelector("#tabs_filter").value;
 ajax('get',`{{Route("employee-pagination")}}`,data={page:page},'#pagination_employee');
});


function removeCompanyProject(e)
{

  var id=e.getAttribute('data-id');
  swal({
  title: "Oops....",
  text: "Are You Sure You want to delete!",
  icon: "error",
  buttons: [
    'NO',
    'YES'
  ],
}).then(function(isConfirm) {
  if (isConfirm) {
    $.ajax({
      url:"{{route('portfolio-remove-data')}}",
      type:"post",
      data:{id:id},
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
      success:function(data){
        Swal.fire({
            getEmployeeData({{$seoEmployees->currentPage()}});
                position: 'top-end',
                icon: 'success',
                title: 'Remove Successfully',
                showConfirmButton: false,
                timer: 1500
                })
        }
    })
  } else {

  }
});
}
</script>
