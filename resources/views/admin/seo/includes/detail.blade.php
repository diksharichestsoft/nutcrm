@extends('admin.layout.template')

@section('contents')

<table class="table">
    <tbody>
      <tr>
        <th scope="row">Title</th>
        <td>{{$thisTask->title ?? ""}}</td>
      </tr>
      <tr>
        <th scope="row">Description</th>
        <td>{!!$thisTask->description!!}</td>
      </tr>
      <tr>
        <th scope="row">Type</th>
        <td>{{$thisTask->type ?? ""}}</td>
      </tr>
    </tbody>
  </table>@endsection
