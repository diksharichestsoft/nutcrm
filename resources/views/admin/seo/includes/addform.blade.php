        <form action="" id="addTargetForm" enctype="multipart/form-data">
          @csrf
        <div class="value">
            {{-- <div class="form-group row">
                <label for="project_id" class="col-sm-2 col-form-label">Select Project</label>
                <div class="col-sm-10">
                <select name="project_id" id="project_id" class="selectpicker" data-live-search="true" required>
                    <option value="">Select Project</option>
                    @foreach($projects as $eachProject)
                    <option value="{{ $eachProject->id }}">{{ $eachProject->title }}</option>
                    @endforeach
                </select>
                <div class="error" id="error_project_id"></div>
                </div>
            </div> --}}
            <div class="form-group row">
            <label for="title" class="col-sm-2 col-form-label">Title *</label>
            <div class="col-sm-10">
                <input type="text" name="title" class="form-control">
                <div class="error" id="error_title"></div>
            </div>
            </div>
            <div class="form-group row">
                <label for="description" class="col-sm-2 col-form-label">Description</label>
                  <div class="col-sm-10">
                      <input type="hidden" name="description"  id="description">
                      <div id="editor3" style="border: 1px solid #ffffff42;"></div>
                  </div>
              </div>

            <div class="form-group row">
                <label for="task_type_id" class="col-sm-2 col-form-label">Type</label>
                <div class="col-sm-10">
                    <select name="type" id="type" class="form-control select2">
                    <option value="">Select Type</option>
                    @foreach ($taskTypes as $item)
                        <option value="{{$item->id}}">{{$item->title}}</option>
                    @endforeach
                    </select>
                    <div class="error" id="error_type"></div>
                </div>
            </div>
            {{-- <div class="form-group row">
                <label for="target_date" class="col-sm-2 col-form-label">Select Target Date</label>
                <div class="col-sm-10">
                    <input type="date" class="form-control" name="target_date">
                    <div class="error" id="error_target_date"></div>
                  </div>
              </div> --}}

            {{-- <div class="form-group row">
                <label for="converted_by" class="col-sm-2 col-form-label">Who Converted Lead</label>
                <div class="col-sm-10">
                <select name="converted_by" id="converted_by" class="selectpicker" data-live-search="true" required>
                    <option value="">Select User</option>
                    @foreach ($users as $eachUser)
                    <option value="{{ $eachUser->id }}">{{ $eachUser->name }}</option>
                    @endforeach
                </select>
                <div class="error" id="error_converted_by"></div>
                </div>
            </div>


            <div class="form-group row">
              <label for="description" class="col-sm-2 col-form-label">Description</label>
                <div class="col-sm-10">
                    <input type="hidden" name="description"  id="description">
                    <div id="editor3" style="border: 1px solid #ffffff42;"></div>
                </div>
            </div> --}}


        </div>

          <div class="form-group row">
            <div class="offset-sm-2 col-sm-10">
              <button type="submit" class="btn btn-success">Add Task</button>
              <button type="reset" class="btn btn-danger">Reset</button>
            </div>
          </div>
        </div>

        </form>
        <script>
            var myEditor;

            InlineEditor
            .create( document.querySelector( '#editor3' ) )
            .then( editor => {
                console.log( 'Editor was initialized', editor );
                myEditor = editor;
            } )
            .catch( err => {
                console.error( err.stack );
            } );
            $(document).ready(function (){
            $(".ck-file-dialog-button").hide();
            $(".ck-dropdown__button").hide();
            })
        </script>
    <script>
        $(".selectpicker").selectpicker({
                "title": "Select Options"
            }).selectpicker("render");
        var myEditor;

        InlineEditor
        .create( document.querySelector( '#editor' ) )
        .then( editor => {
            console.log( 'Editor was initialized', editor );
            myEditor = editor;
        } )
        .catch( err => {
            console.error( err.stack );
        } );
        $(document).ready(function (){
        $(".ck-file-dialog-button").hide();
        $(".ck-dropdown__button").hide();
        })
    </script>
