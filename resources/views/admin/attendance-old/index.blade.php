<style>
  .daterangepicker .ranges li {font-size: 12px;padding: 8px 12px;cursor: pointer;background: #000000b0;}
</style>
<div class="card" id="data">
              <div class="card-header p-2" id="card_head">
                <ul class="nav nav-pills">

                  @foreach($ProjectStatus as $projectstatus)
                  <li class="nav-item"><a class="nav-link services" href="#view" onclick="fetch_data(1,this.getAttribute('data-id'))" data-id="{{$projectstatus->id}}" data-toggle="tab">{{$projectstatus->dealname}}</a></li>&nbsp;
                  @endforeach
                  <li class="nav-item"><a class="nav-link services" href="#view" onclick="fetch_data(1,this.getAttribute('data-id'))" data-id="-1" data-toggle="tab">All Leads</a></li>&nbsp;
                  <div id="reportrange" style="background: transparent; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                  <i class="fa fa-calendar"></i>&nbsp;
                  <span class="ab"></span> <i class="fa fa-caret-down"></i>
                 </div>
                 <input type="hidden" id="start_date_range" >
                 <input type="hidden" id="end_date_range">
                  <li class="nav-item search-right">
                    <div>
                      <a href="javascript:void(0)" onclick="addcompanydeals()"  class="btn btn-primary"  data-toggle="modal" data-target="#createDeal" >ADD PROJECTS</a>
                    </div>
                   <div class="search_bar">
                      <div class="input-group" data-widget="sidebar-search">
                       <input class="form-control form-control-sidebar" id="search" type="search" placeholder="Search" aria-label="Search">
                      </div>
                   </div>
                  </li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="tab-pane" id="profile">
                    {{--
                      @include('admin.attendance.includes.addform')
                      --}}
                  </div>

                  <div>
                    @if(!isset($data))
                    <h5>Add Deals</h5>
                    @endif
                  </div>
                  <div class="active tab-pane" id="view">
                     @include('admin.attendance.includes.view')
                    </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
      </div>


<div class="modal fade" id="changestatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel1">Comment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="changeStatuSbody">
        <form action="" name="changestatusform" id="changestatusform">
            @csrf

            <input type="hidden" name='status' id="changeStatus-status" value="">
            <input type="hidden" name='deal_id' id="changeStatus-deal_id" value="">
            <div class="value">

            <div class="form-group row">
                <label for="comment" class="col-sm-2 col-form-label">Seleact a reason</label>
                <div class="col-sm-10">
                    <select class="form-select" id="reasons_select" aria-label="Default select example">
                        <option selected>Open this select menu</option>
                    </select>
                    <div class="error" id="error_comment"></div>
                </div>
            </div>
            <div class="form-group row">
              <label for="comment" class="col-sm-2 col-form-label">Comment</label>
              <div class="col-sm-10">
                <textarea name="comment"  cols="30" rows="10" class="form-control"></textarea>
                <div class="error" id="error_comment"></div>
              </div>
            </div>
            <div class="form-group row">
              <div class="offset-sm-2 col-sm-10">
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="createDeal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Projects</h5>
        <button type="button" class="close" id='createDealHide' data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body " id="createDealBody">
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="addComment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Comment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body " id="addCommentBody">
      <form action="" name="addCommentForm" id="addCommentForm">
          @csrf
          <input type="hidden" name='deal_id' id="deal_id" value="">
        <div class="value">
          <div class="form-group row">
            <label for="comment" class="col-sm-2 col-form-label">Comment</label>
            <div class="col-sm-10">
              <textarea name="comment" id="comment" cols="30" rows="10" class="form-control"></textarea>
              <div class="error" id="error_comment"></div>
            </div>
          </div>
          <div class="form-group row">
            <div class="offset-sm-2 col-sm-10">
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
          </div>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
  $(document).ready(function(){
    $(".services").eq(0).addClass("active");
  });
  $(document).on('click','.comment',function(){
    var id = $(this).attr('data-id');
    $('#deal_id').val(id);
    $('#comment_title').val('');
    $('#comment').val('');
  });


function updateCompanyProject(e)
{
  $('#page-loader').show();

  var id=e.getAttribute('data-id');
  $.ajax({
    url:"{{route('projects-edit')}}",
    type:"get",
    data:{id:id},
    success:function(data)
    {

      $('#page-loader').hide();
      $('#createDealBody').empty().html(data);
    },
    error:function(){
      $('#page-loader').hide();
    }
  })
}

$(document).on('submit','#addCompany',function(e){
  e.preventDefault();
  var data = new FormData(this);
    $.ajax({
        type:'post',
        url:"{{route('projects-store')}}",
        dataType: "JSON",
       xhr: function() {
             myXhr = $.ajaxSettings.xhr();
             return myXhr;
       },
       cache: false,
       contentType: false,
       processData: false,
        data:data,
        success:function(data){
            $(".services:first").trigger( "click" );
            $('#createDeal').modal('hide');
        },
        error:function(data){
          console.log(data.responseJSON.errors);
          $.each(data.responseJSON.errors, function(id,msg){
            $('#error_'+id).html(msg);
          })
        }
      });
    });


$(document).on('submit','#addCommentForm',function(e){
  e.preventDefault();
  var data = new FormData(this);
    $.ajax({
        type:'post',
        url:"{{route('deals-add-Comment')}}",
        dataType: "JSON",
       xhr: function() {
             myXhr = $.ajaxSettings.xhr();
             return myXhr;
       },
       cache: false,
       contentType: false,
       processData: false,
        data:data,
        success:function(data){
            $('#addComment').modal('hide');
            fetch_data(1,'');
        },
        error:function(data){
          // console.log(data.responseJSON.errors.comment);
          $.each(data.responseJSON.errors, function(id,msg){
            // console.log();
            $('.error#error_'+id).html(msg);
          })
        }
      });
    });


  $(document).on('change','#selectsatus',function(){
    var status =$(this).val();
    var deal_id = $(this).attr('data-id');
    $('#changeStatus-status').val(status);
    $('#changeStatus-deal_id').val(deal_id);
    $('#changestatusform textarea').val('');
    console.log(status);
    $.ajax({
        type:'get',
        url:"{{route('get-status-reason')}}",
        data: {"status":status},
        dataType: "JSON",
       xhr: function() {
             myXhr = $.ajaxSettings.xhr();
             return myXhr;
       },
       cache: false,
       contentType: false,
       processData: false,
        success:function(data){
            console.log(data.reasons)

            $('#reasons_select').empty()
            $.each(data.reasons, function(index, item) {
            console.log()
           $('#reasons_select').append(`<option value="${item.id}">${item.name}</option>`)
        });
        },
        error:function(data){
        //   console.log(data.responseJSON.errors);
        //   $.each(data.responseJSON.errors, function(id,msg){
        //     $('#error_'+id).html(msg);
        //   })
        }
      });


    $('#changestatus').modal('show');
  });
  $(document).on('submit','#changestatusform',function(e){
      e.preventDefault();
        var data = new FormData(this);
    $.ajax({
        type:'post',
        url:"{{route('projects-change-status')}}",
        dataType: "JSON",
       xhr: function() {
             myXhr = $.ajaxSettings.xhr();
             return myXhr;
       },
       cache: false,
       contentType: false,
       processData: false,
        data:data,
        success:function(data){
            $('#changestatus').modal('hide');
            fetch_data(1,'');
        },
        error:function(data){
          console.log(data.responseJSON.errors);
          $.each(data.responseJSON.errors, function(id,msg){
            $('#error_'+id).html(msg);
          })
        }
      });
});

function removeCompanyProject(e)
{
  var id=e.getAttribute('data-id');
  swal({
  title: "Oops....",
  text: "Are You Sure You want to delete!",
  icon: "error",
  buttons: [
    'NO',
    'YES'
  ],
}).then(function(isConfirm) {
  if (isConfirm) {
    $.ajax({
      url:"{{route('projects-remove-data')}}",
      type:"post",
      data:{id:id},
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
      success:function(data){
        $('#view').empty().html(data);
        fetch_data(1,'');
      }
    })
  } else {

  }
});
}

function accept(e)
{

  var id=e.getAttribute('data-id');
  $.ajax({
    url:"{{route('projects-accept')}}",
    type:"post",
    data:{id:id},
    headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
    success:function(data)
    {
    fetch_data(1,'');
    }
  })
}

  $(document).on('submit','#updateCompany',function(e){
    e.preventDefault();
    var data = new FormData(this);
    $.ajax({
        type:'post',
        url:"{{route('projects-store')}}",
        dataType: "JSON",
       xhr: function() {
             myXhr = $.ajaxSettings.xhr();
             return myXhr;
       },
       cache: false,
       contentType: false,
       processData: false,
        data:data,
        success:function(data){
          fetch_data(1,'');
          $('#createDeal').modal('hide');
        },
        error:function(data){
          console.log(data.responseJSON.errors);
          $.each(data.responseJSON.errors, function(id,msg){
            $('#error_'+id).html(msg);
          });
        }
    });
});
function addcompanydeals()
{
  $('#page-loader').show();

  $.ajax({
    url:"{{route('projects-add')}}",
    type:"get",
    success:function(data)
    {
      $('#page-loader').hide();
      $('#createDealBody').empty().html(data);
    },
    error:function(error){
      $('#page-loader').hide();
    }
  })
}

$(function() {
var start = moment().subtract(29, 'days');
var end = moment();
function cb(start, end) {
    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    $('#start_date_range').val(start.format('YYYY-MM-DD'));
    $('#end_date_range').val(end.format('YYYY-MM-DD'));
    fetch_data(1,'');
}

$('#reportrange').daterangepicker({
    startDate: start,
    endDate: end,
    ranges: {
       'Today': [moment(), moment()],
       'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
       'Last 7 Days': [moment().subtract(6, 'days'), moment()],
       'Last 30 Days': [moment().subtract(29, 'days'), moment()],
       'This Month': [moment().startOf('month'), moment().endOf('month')],
       'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
}, cb);
cb(start, end);

});

$(document).on('click', '.pagination a', function(event){
  event.preventDefault();
  var page = $(this).attr('href').split('page=')[1];
  var sendurl=$(this).attr('href');
  fetch_data(page,sendurl,'');
});
document.querySelector("#search").addEventListener("keyup",(e)=>{
    // var active=$('a.active.services').attr('data-id');
    fetch_data(1,'');
});

function fetch_data(page,active)
{
  $('#page-loader').show();

  var start_date=document.querySelector('#start_date_range').value;
  var end_date=document.querySelector('#end_date_range').value;
  if(active==''){
    var active=document.getElementsByClassName('services active')[0].getAttribute('data-id');
  }
  page = (page='')?'1':page;
  var search=document.querySelector("#search").value;
  // let make_url="";
  var data={search:search,active:active,start_date:start_date,end_date:end_date};
  var make_url= "{{url('/')}}/admin/attendence/search?page="+page;
  console.log(active);
  $.ajax({
    url:make_url,
    data:data,
    success:function(data)
    {
        $('#view').empty().html(data);
      $('#page-loader').hide();

    },
    error:function(error){
      $('#page-loader').hide();

    }
  });
  }
</script>

