@extends('admin.layout.template')
@section('contents')
  <div class="modal fade" id="attendanceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Attendance Data</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <span class="h5">In At: </span> <span id="modal_in_at_data"></span>
            <br>
            <span class="h5">Out At: </span> <span id="modal_out_at_data"></span>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div>
    <select class="form-select" aria-label="Default select example">
        <option selected>Select Month</option>
        <option value="1">January</option>
        <option value="2">February</option>
        <option value="3">March</option>
        <option value="4">April</option>
        <option value="5">May</option>
        <option value="6">June</option>
        <option value="7">July</option>
        <option value="8">August</option>
        <option value="9">September</option>
        <option value="10">October</option>
        <option value="11">November</option>
        <option value="12">December</option>
      </select>
  </div>
  <div id="attendenceTableView">
      @include('admin.attendance.attendance')
  </div>
<script>


    $(".toggle-attendance-modal").on('click', function (e){
        e.preventDefault();
        $("#attendanceModal").modal('show');
        in_at = $(e.target).data('punch_in_at').split(" ");
        out_at = $(e.target).data('punch_out_at').split(" ");
        // var in_at = new Date(in_at);
        // var out_at = new Date(out_at);


        function time12Hours(timeString){
        var timeString = new Date(timeString)
        timeString = timeString.toLocaleString()
        timeString = timeString.split(",");
        return timeString[1];

        }

        $("#modal_in_at_data").html(time12Hours(in_at));
        $("#modal_out_at_data").html(time12Hours(out_at));
    })
</script>
@endsection
