<?php

use App\Models\Attendance;
use App\Models\User;


$month = date("m");
$year = date("Y");
$start_date = "01-" . $month . "-" . $year;
$start_time = strtotime($start_date);

$end_time = strtotime("+1 month", $start_time);

for ($i = $start_time; $i < $end_time; $i += 86400) {
  $thisMonthArray[] = date('Y-m-d', $i);
}


echo '<table class="table table-striped table-bordered table-responsive" id="table_content">';
echo "<thead>";
echo "<tr>";
echo "<th>S/No</th>";
echo "<th>Em.ID</th>";
echo "<th>Name</th>";
foreach ($thisMonthArray as $thisMonth) {
  echo "<th>" . $thisMonth . "</th>";
}
echo "</tr>";
echo "</thead>";

$users = User::get();
$i = 1;
foreach ($users as $user) {

  echo "<tr>";
  echo "<td>" . $i . "</td>";
  echo "<td>" . $user->id . "</td>";
  echo "<td>" . $user->name . "</td>";

  foreach ($thisMonthArray as $thisMonth) {

    echo "<td>";
    $checkAtt = Attendance::where(["date" => $thisMonth, "created_by" => $user->id])->first();
    if (!empty($checkAtt)) {
      $time = gmdate("H:i:s", $checkAtt->total_time);
      echo "<span class='btn btn-xs btn-success toggle-attendance-modal' data-punch_in_at='".$checkAtt->in_at."' data-punch_out_at='".$checkAtt->out_at."'>P</span> </br>";
      echo $time;
      echo "<br>";
      echo "In Ip:".$user->punchTiming()->get()->last()->in_ip_address;
      echo "<br>";
      echo "Out Ip:".$user->punchTiming()->get()->last()->out_ip_address;
    } else {
      //$time = gmdate("H:i:s", 0);
      echo "<span class='btn btn-xs btn-danger'>A </span></br>";
      //echo $time;
    }
    echo "</td>";
  }
  echo "</tr>";

  $i++;
}
echo "</table>";
