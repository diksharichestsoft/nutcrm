<style>
  select#selectsatus {
    width: 60%;
}
</style>
<!-- {{--dd('hiiiiiiiiiiiiiii')--}} -->
<div class="card">
              <div class="card-header">
                <h3 class="card-title">In Queue</h3>
              </div>
              <!-- /.card-header -->
             <div class="card-body">
                <table class="table table-bordered">
                  @if(count($data)>0)
                  @endif
                  <tbody>
                        @forelse($data as $user)
                        @if(count($user->punchTiming)>0)
                          <tr>
                            <td colspan="5" align="center">{{$user->name}}</td>
                          </tr>
                          <?php $totalTime = 0; ?>
                        @forelse($user->punchTiming as $key=>$value)
                        <?php $totalTime = $totalTime+$value->total_time; ?>
                            <tr>
                              <td>in</td>
                              <td>{{$value->punch_in??''}}</td>
                              <td>{{$value->punch_in??''}}</td>
                              <td>{{$value->punch_in??''}}</td>
                              <td>{{$value->punch_in??''}}</td>
                            </tr>
                            <tr>
                              <td>out</td>
                              <td>{{$value->punch_out??''}}</td>
                              <td>{{$value->punch_out??''}}</td>
                              <td>{{$value->punch_out??''}}</td>
                              <td>{{$value->punch_out??''}}</td>
                            </tr>
                            <tr>
                              <td>time</td>
                              <td>{{($value->total_time!='')?date('H:i', mktime(0,$value->total_time)):'0:00'}}</td>
                              <td>{{($value->total_time!='')?date('H:i', mktime(0,$value->total_time)):'0:00'}}</td>
                              <td>{{($value->total_time!='')?date('H:i', mktime(0,$value->total_time)):'0:00'}}</td>
                              <td>{{($value->total_time!='')?date('H:i', mktime(0,$value->total_time)):'0:00'}}</td>
                            </tr>
                        @empty
                        @endforelse
                        <!-- <tr>
                          <td colspan="4" align="center">Total Time</td>
                          <td>{{date('H:i', mktime(0,$totalTime))}}</td>
                        </tr> -->
                        @endif
                        @empty
                        @endforelse
                  </tbody>
                </table>
              </div>
              {{$data->links()}}
              {{--
              @if($data->previousPageUrl() != null)
                 <a href="{{$data->previousPageUrl()}}" class="pagination prev_btn pull-left"><i class="fa fa-chevron-left"></i> Previous</a>
              @endif

              @if($data->nextPageUrl() != null)
                  <a href="{{$data->nextPageUrl()}}" class="pagination prev_btn pull-right">Next <i class="fa fa-chevron-right"></i> </a>
              @endif
              --}}
              <!-- /.card-body -->
            </div>



{{-- All in one modal --}}
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Project Status</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
           <div class="form-group">
               <label for="">Comment</label>
           <textarea name="" id="inproc1" cols="30" rows="10" class="form-control"></textarea>
           </div>

        </div>
        <div class="modal-footer">
          <button type="button" id="save_deal1" class="btn btn-primary">Save Project Status</button>
        </div>
      </div>
    </div>
  </div>



{{-- \ All in one modal --}}



<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Project Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <label for="">Comment</label>
       <textarea name="" id="inproc" cols="30" rows="10" class="form-control"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" id="save_deal" onclick="save_deal_data()" class="btn btn-primary">Save Project Status</button>
      </div>
    </div>
  </div>
</div>


<!-- Meeting -->

<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Project Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <div class="form-group">
             <label for="">Comment</label>
         <textarea name="" id="inproc1" cols="30" rows="10" class="form-control"></textarea>
         </div>

      </div>
      <div class="modal-footer">
        <button type="button" id="save_deal1" class="btn btn-primary">Save Project Status</button>
      </div>
    </div>
  </div>
</div>

<!-- FRD -->

<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Deal Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

         <div class="form-group">
             <label for="">Comment</label>
         <textarea name="" id="inproc2" cols="30" rows="10" class="form-control"></textarea>
         </div>

      </div>
      <div class="modal-footer">
        <button type="button" id="save_deal2" class="btn btn-primary">Save Deal Status</button>
      </div>
    </div>
  </div>
</div>

<!-- Favorite -->

<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Deal Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

         <div class="form-group">
             <label for="">Comment</label>
         <textarea name="" id="inproc3" cols="30" rows="10" class="form-control"></textarea>
         </div>

      </div>
      <div class="modal-footer">
        <button type="button" id="save_deal3" class="btn btn-primary">Save Deal Status</button>
      </div>
    </div>
  </div>
</div>

<!-- WON -->

<div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Deal Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

         <div class="form-group">
             <label for="">Comment</label>
         <textarea name="" id="inproc4" cols="30" rows="10" class="form-control"></textarea>
         </div>

      </div>
      <div class="modal-footer">
        <button type="button" id="save_deal4" class="btn btn-primary">Save Deal Status</button>
      </div>
    </div>
  </div>
</div>
<!-- LOST -->

<div class="modal fade" id="exampleModal5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Deal Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

         <div class="form-group">
             <label for="">Comment</label>
         <textarea name="" id="inproc5" cols="30" rows="10" class="form-control"></textarea>
         </div>

      </div>
      <div class="modal-footer">
        <button type="button" id="save_deal5" class="btn btn-primary">Save Project Status</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModal6" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Project Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

         <div class="form-group">
             <label for="">Comment</label>
         <textarea name="" id="inproc6" cols="30" rows="10" class="form-control"></textarea>
         </div>

      </div>
      <div class="modal-footer">
        <button type="button" id="save_deal6" class="btn btn-primary">Save Project Status</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModal7" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Project Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

         <div class="form-group">
             <label for="">Comment</label>
         <textarea name="" id="inproc7" cols="30" rows="10" class="form-control"></textarea>
         </div>

      </div>
      <div class="modal-footer">
        <button type="button" id="save_deal7" class="btn btn-primary">Save Project Status</button>
      </div>
    </div>
  </div>
</div>
<script>
$('.deals_status').change(function(){
        var active=$('a.active.services').attr('data-id');
        var start_date=document.querySelector('#start_date_range').value;
        var end_date=document.querySelector('#end_date_range').value;
        var status = $(this).val();
        var candidate_id = $(this).find(':selected').attr('data-id');
          if(candidate_id==1)
          {
            $('#exampleModal').modal('show');
            status(status);
            candidate(candidate_id);
          //  save_data(status,candidate_id,active,start_date,end_date);
          }
          else if(candidate_id==2)
          {
            $('#exampleModal1').modal('show');
            save_data1(status,candidate_id,active,start_date,end_date);
          }
          else if(candidate_id==3)
          {
            $('#exampleModal2').modal('show');
            save_data2(status,candidate_id,active,start_date,end_date);
          }
          else if(candidate_id==4)
          {
            $('#exampleModal3').modal('show');
            save_data3(status,candidate_id,active,start_date,end_date);
          }
          else if(candidate_id==5)
          {
            $('#exampleModal4').modal('show');
            save_data4(status,candidate_id,active,start_date,end_date);
          }
          else if(candidate_id==6)
          {
            $('#exampleModal5').modal('show');
            save_data5(status,candidate_id,active,start_date,end_date);
          }
          else if(candidate_id==7)
          {
            $('#exampleModal6').modal('show');
            save_data6(status,candidate_id,active,start_date,end_date);
          }
          else if(candidate_id==8)
          {
            $('#exampleModal7').modal('show');
            save_data7(status,candidate_id,active,start_date,end_date);
          }
      })
      function save_data(status,candidate_id,active,start_date,end_date){
      $('#save_deal').click(function(){
        alert
          let comment=$('#inproc').val();
                $.ajax({
                   url:"{{url('/')}}/admin/companyDeal/saveInproccess",
                   type:"post",
                   data:{comment:comment,status:status,candidate_id:candidate_id,active:active,start_date:start_date,end_date:end_date},
                   headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
                   success:function(data){
                    if(active==2)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(1).addClass("active");
                    }
                    if(active==3)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(2).addClass("active");
                    }
                    if(active==4)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(3).addClass("active");
                    }
                    if(active==5)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(4).addClass("active");
                    }
                    if(active==6)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(5).addClass("active");
                    }
                    if(active==7)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(6).addClass("active");
                    }
                    if(active==8)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(7).addClass("active");
                    }




                   // $('#exampleModal1').hide();
                    //$('#exampleModal1').modal('hide');
                        // location.reload();
                   }
                });
            })
        }
        function save_data1(status,candidate_id,active){
           $('#save_deal1').click(function(){
              let comment=$('#inproc1').val();

                    $.ajax({
                       url:"{{url('/')}}/admin/companyDeal/saveInproccess",
                       type:"post",
                       data:{comment:comment,status:status,candidate_id:candidate_id,active:active},
                       headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
                       success:function(data){
                          //  location.reload();
                          if(active==2)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(1).addClass("active");
                    }
                    if(active==3)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(2).addClass("active");
                    }
                    if(active==4)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(3).addClass("active");
                    }
                    if(active==5)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(4).addClass("active");
                    }
                    if(active==6)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(5).addClass("active");
                    }
                    if(active==7)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(6).addClass("active");
                    }
                    if(active==8)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(7).addClass("active");
                    }
                       }
                    });
                })
            }
            function save_data2(status,candidate_id,active){
          $('#save_deal2').click(function(){
              let comment=$('#inproc2').val();

                    $.ajax({
                       url:"{{url('/')}}/admin/companyDeal/saveInproccess",
                       type:"post",
                       data:{comment:comment,status:status,candidate_id:candidate_id,active:active},
                       headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
                       success:function(data){
                          //  location.reload();
                          if(active==2)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(1).addClass("active");
                    }
                    if(active==3)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(2).addClass("active");
                    }
                    if(active==4)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(3).addClass("active");
                    }
                    if(active==5)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(4).addClass("active");
                    }
                    if(active==6)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(5).addClass("active");
                    }
                    if(active==7)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(6).addClass("active");
                    }
                    if(active==8)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(7).addClass("active");
                    }
                       }
                    });
                })
            }
            function save_data3(status,candidate_id,active){

          $('#save_deal3').click(function(){
              let comment=$('#inproc3').val();

                    $.ajax({
                       url:"{{url('/')}}/admin/companyDeal/saveInproccess",
                       type:"post",
                       data:{comment:comment,status:status,candidate_id:candidate_id,active:active},
                       headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
                       success:function(data){
                          //  location.reload();
                          if(active==2)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(1).addClass("active");
                    }
                    if(active==3)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(2).addClass("active");
                    }
                    if(active==4)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(3).addClass("active");
                    }
                    if(active==5)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(4).addClass("active");
                    }
                    if(active==6)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(5).addClass("active");
                    }
                    if(active==7)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(6).addClass("active");
                    }
                    if(active==8)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(7).addClass("active");
                    }
                       }
                    });
                })
            }
            function save_data4(status,candidate_id,active){

          $('#save_deal4').click(function(){
              let comment=$('#inproc4').val();

                    $.ajax({
                       url:"{{url('/')}}/admin/companyDeal/saveInproccess",
                       type:"post",
                       data:{comment:comment,status:status,candidate_id:candidate_id,active:active},
                       headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
                       success:function(data){
                          //  location.reload();
                          if(active==2)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(1).addClass("active");
                    }
                    if(active==3)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(2).addClass("active");
                    }
                    if(active==4)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(3).addClass("active");
                    }
                    if(active==5)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(4).addClass("active");
                    }
                    if(active==6)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(5).addClass("active");
                    }
                    if(active==7)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(6).addClass("active");
                    }
                    if(active==8)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(7).addClass("active");
                    }
                       }
                    });
                })
            }
            function save_data5(status,candidate_id,active){
           $('#save_deal5').click(function(){
              let comment=$('#inproc5').val();
                    $.ajax({
                       url:"{{url('/')}}/admin/companyDeal/saveInproccess",
                       type:"post",
                       data:{comment:comment,status:status,candidate_id:candidate_id,active:active},
                       headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
                       success:function(data){
                          //  location.reload();
                          if(active==2)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(1).addClass("active");
                    }
                    if(active==3)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(2).addClass("active");
                    }
                    if(active==4)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(3).addClass("active");
                    }
                    if(active==5)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(4).addClass("active");
                    }
                    if(active==6)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(5).addClass("active");
                    }
                    if(active==7)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(6).addClass("active");
                    }
                    if(active==8)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(7).addClass("active");
                    }
                       }
                    });
                })
            }
            function save_data6(status,candidate_id,active){
           $('#save_deal6').click(function(){
              let comment=$('#inproc6').val();
                    $.ajax({
                       url:"{{url('/')}}/admin/companyDeal/saveInproccess",
                       type:"post",
                       data:{comment:comment,status:status,candidate_id:candidate_id,active:active},
                       headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
                       success:function(data){
                          //  location.reload();
                          if(active==2)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(1).addClass("active");
                    }
                    if(active==3)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(2).addClass("active");
                    }
                    if(active==4)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(3).addClass("active");
                    }
                    if(active==5)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(4).addClass("active");
                    }
                    if(active==6)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(5).addClass("active");
                    }
                    if(active==7)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(6).addClass("active");
                    }
                    if(active==8)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(7).addClass("active");
                    }
                       }
                    });
                })
            }
            function save_data7(status,candidate_id,active){
           $('#save_deal7').click(function(){
              let comment=$('#inproc7').val();
                    $.ajax({
                       url:"{{url('/')}}/admin/companyDeal/saveInproccess",
                       type:"post",
                       data:{comment:comment,status:status,candidate_id:candidate_id,active:active},
                       headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
                       success:function(data){
                          //  location.reload();
                          if(active==2)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(1).addClass("active");
                    }
                    if(active==3)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(2).addClass("active");
                    }
                    if(active==4)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(3).addClass("active");
                    }
                    if(active==5)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(4).addClass("active");
                    }
                    if(active==6)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(5).addClass("active");
                    }
                    if(active==7)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(6).addClass("active");
                    }
                    if(active==8)
                    {
                      $('#view').empty().html(data);
                    $(".modal-backdrop").hide();
                    $(".services").eq(7).addClass("active");
                    }
                       }
                    });
                })
            }

  </script>
  <script>
    // status(status)
    // {
    //   return status;
    // }
    function save_deal_data()
    {
        var active=$('a.active.services').attr('data-id');
        var start_date=document.querySelector('#start_date_range').value;
        var end_date=document.querySelector('#end_date_range').value;
    }
  </script>
