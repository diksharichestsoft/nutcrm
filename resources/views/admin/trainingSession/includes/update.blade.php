<!-- Button trigger modal -->
  <!-- Modal -->
  <style>
    #users + span {
      width: 100% !important;
    }
    </style>
  <form id="update_training_session_form" action="" method="POST">
    <div id="dsr_item_container">
        @csrf
        <div class="dsr_items">
            <div class="form-group">
                <label for="title">Title</label>
                <br>
                <input type="text" required class="form-control" name="title" value="{{$thisTrainingSession->title}}">
                <input type="hidden" class="form-control" name="created_by" value="{{Auth::user()->id}}">
                <input type="hidden" name="reqType" value="1">
                <input type="hidden" name="id" value="{{$thisTrainingSession->id}}">
                <div class="error" id="error_title"></div>
            </div>
            <div class="form-group">
              <label for="description" class="col col-form-label">Description</label>
                <div class="col-sm-10">
                    <input type="hidden" id="update-description" name="description">
                    <div id="editorupdate" style="border: 1px solid #ffffff42;">{!!$thisTrainingSession->description!!}</div>
                </div>
            </div>
            <div class="form-group">
              <label for="training_type" class="col col-form-label">Training Type</label>
              <div class="col-sm-10">
                <select name="type" id="type" class="form-control ">
                  <option disabled selected >Select Training Type</option>
                  @foreach($training_types as $eachType)
                    <option value="{{$eachType}}" {{($eachType == $thisTrainingSession->type) ? 'selected' : ''}}>{{$eachType}}</option>
                    @endforeach
                </select>
                <div class="error" id="error_type"></div>
              </div>
            </div>
            <div class="form-group">
              <label for="user" class="col col-form-label">Users</label><br>
              <div class="col-sm-10">
                  <select name="user_id[]" id="users" class="form-control select2" multiple required>
                    <option value="" {{count($users_name) < 1 ? "selected" : ""}}>All Users</option>
                    @foreach($users as $key=>$user)
                      <option value="{{$user->id}}" {{(in_array($user->id,$users_name))? 'selected':''}} >{{$user->name}}</option>
                    @endforeach
                  </select>
              </div>
              @error('user_id')
                  <div class="error error-msg-red"></div>
              @enderror
          </div>
        </div>
    </div>
    <br>
        {{-- <button type="submit" class="btn btn-primary">Save</button> --}}
        <button type="button" class="btn btn-primary" onclick="$('#update_training_session_form').submit()"> Save</button>

    </form>
    <script>
      $('#users').select2({
        dropdownParent: $('#editTrainingSessionModal')
    });
    </script>

<script>
var myEditor;
InlineEditor
.create( document.querySelector( `#editorupdate` ) )
.then( editor => {
console.log( 'Editor was initialized', editor );
myEditor = editor;
} )
.catch( err => {
console.error( err.stack );
} );
$(document).ready(function (){
// Hiding default extrack file and dropdown buttons
$(".ck-file-dialog-button").hide();
$(".ck-dropdown__button").hide();
})
</script>
