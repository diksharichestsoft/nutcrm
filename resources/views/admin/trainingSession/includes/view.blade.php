   <!-- Main content -->
   <div class="modal fade" id="addTrainingSessionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Training Session</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="add_dsr_modal_body" class="modal-body">
            @include('admin.trainingSession.create')
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

   <div class="modal fade" id="editTrainingSessionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Training Session</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="edit_dsr_modal_body" class="modal-body">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>



    {{-- <a href="#" id="addTodoButton" class="btn btn-outline-success btn-lg" data-toggle="modal" data-target="#addTodoModal">
        <i class="fa fa-plus" aria-hidden="true"></i>New Todo
    </a> --}}
    <div class="modal fade" id="createDeal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Todos</h5>
              <button type="button" class="close" id='createDealHide' data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body " id="todoBody">
            </div>
          </div>
        </div>
      </div>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!--  -->
                <div class="card-body">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width: 10px">#</th>
                          <th>Action</th>
                          <th>Title</th>
                          <th>Description</th>
                          <th>Status</th>
                          <th>Training Type</th>
                          <th>Created At</th>
                          <th>Updated At</th>
                        </tr>
                      </thead>
                      <tbody>
                          @forelse ($trainingSession as $key=> $value )
                          <tr>
                              <td>
                                {{$key+1}}
                              </td>
                              <td>
                                <div class="btn-group">
                                    @can('training_session_edit')
                                    <button data-id="{{$value->id}}" onclick="updateTrainingSession(this)" class="btn btn-outline-success btn-xs  update" ><i class="fas fa-pencil-alt"></i></button>
                                    @endcan

                                    @can('training_session_view')
                                    <a href=" {{route('training-sessions-detail',$value->id)}}" target="_blank" class="btn btn-outline-success btn-xs  view"><i class="fas fa-eye"></i></a>
                                    @endcan

                                    @can('training_session_delete')
                                    <button data-id="{{$value->id}}" onclick="removeTrainingSession(this)" class="btn btn-danger btn-xs  remove"><i class="fas fa-times"></i></button>
                                    @endcan

                                </div>
                              </td>
                              <td>
                                  {{$value->title}}
                                </td>
                                <td>
                                {!!$value->description!!}
                            </td>
                          @can('training_session_toggle_status')

                            <td>@if($value->status==1)
                                <button data-id="{{$value->id}}" class="disable_enable btn btn-success btn-xs" onclick="toggleDisableEnable(this)">Approved</button>
                                @else
                                <button data-id="{{$value->id}}" class="disable_enable btn btn-danger btn-xs" onclick="toggleDisableEnable(this)">Not Approved</button>
                                @endif
                            </td>
                          @endcan
                          <td>{{$value->type}}</td>

                          <td>{{ $value->created_at->format('d/m/Y')}}</td>
                          <td>{{ $value->updated_at->format('d/m/Y')}}</td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="9">
                                <h4  align="center">No Data Found</h4>
                            </td>
                        </tr>
                        @endforelse
                      </tbody>
                    </table>
                </div>

                <!--  -->
            </div>
        </div>
        {{$trainingSession->links()}}
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
    <script>




    $("#addTrainingSessionButton").on('click', function (){
      document.querySelector("#error_title").innerText="";


    $("#add_training_session_form").on('submit', function (e){
        e.preventDefault();
        var descData = ($("#editor").html());
        $("#description").val(descData);
        var data = new FormData(this);
        console.log(data);
        $.ajax({
            type:'post',
            url:"{{route('training-sessions-store')}}",
            cache: false,
                contentType: false,
                processData: false,
                dataType: "JSON",
            data : {data: data},
        headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data:data,
            success:function(data){
                $('#addTrainingSessionModal').modal('hide');
                $(".modal-backdrop").hide();
                $("body").removeClass("modal-open");
                fetchTrainingSessionData(1);
                Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Added Successfully',
                showConfirmButton: false,
                timer: 1500
                })

            },
            error:function(data){
            $.each(data.responseJSON.errors, function(id,msg){
                $('#error_'+id).html(msg);

            })
            }
        });
        })

})

function removeTrainingSession(e)
{

  var id=e.getAttribute('data-id');
  swal({
  title: "Oops....",
  text: "Are You Sure You want to delete!",
  icon: "error",
  buttons: [
    'NO',
    'YES'
  ],
}).then(function(isConfirm) {
  if (isConfirm) {
    $.ajax({
      url:"{{route('remove-training-sessions')}}",
      type:"post",
      data:{id:id},
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
      success:function(data){
        Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Remove Successfully',
                showConfirmButton: false,
                timer: 1500
                })
                fetchTrainingSessionData(1);
        }
    })
  } else {

  }
});
}


function updateTrainingSession(e)
{
  $('#page-loader').show();

  var id=e.getAttribute('data-id');
  $.ajax({
    url:"{{route('training-sessions-edit')}}",
    type:"get",
    data:{id:id},
    success:function(data)
    {
       // toastr.success(response.message)
      $('#page-loader').hide();
      $('#edit_dsr_modal_body').empty().html(data);
    $("#editTrainingSessionModal").modal('show');
      intitializeUpdate();
    },
    error:function(){
      $('#page-loader').hide();
    }
  })
}
function intitializeUpdate(){

// initializeCreateForm()
    $("#update_training_session_form").on('submit', function (e){
        e.preventDefault();
        var descData = ($("#editorupdate").html());
        console.log(descData)
        $("#update-description").val(descData);

        var data = new FormData(this);
        console.log(data);
        $.ajax({
            type:'post',
            url:"{{route('training-sessions-store')}}",
            cache: false,
                contentType: false,
                processData: false,
                dataType: "JSON",
            data : {data: data},
        headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data:data,
            success:function(data){
                $('#addTrainingSessionModal').modal('hide');
                $(".modal-backdrop").hide();
                fetchTrainingSessionData(1);
                Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Updated Successfully',
                showConfirmButton: false,
                timer: 1500
                })
            },
            error:function(data){
            $.each(data.responseJSON.errors, function(id,msg){
                $('#error_'+id).html(msg);

            })
            }
        });
        })

}

</script>
