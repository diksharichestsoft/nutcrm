@extends('admin.layout.template')

@section('contents')
<table class="table">
    <tbody>
      <tr>
        <th scope="row">Title</th>
        <td>{{$thisTrainingSession->title}}</td>
      </tr>
      <tr>
        <th scope="row">Description</th>
        <td>{!!$thisTrainingSession->description!!}</td>
      </tr>
      <tr>
        <th scope="row">Training Type</th>
        <td>{{$thisTrainingSession->type}}</td>
      </tr>
      <tr>
        <th scope="row">Created At</th>
        <td>{{timestampToDateAndTimeObj($thisTrainingSession->created_at)->time}}</td>
      </tr>
      <tr>
        <th scope="row">Created On</th>
        <td>{{timestampToDateAndTimeObj($thisTrainingSession->created_at)->date}}</td>
      </tr>
      <tr>
        <th scope="row">Users</th>
        <td>
          @foreach($thisTrainingSession->users as $user)
          {{$user->name}}
          <br>
          @endforeach
        </td>
      </tr>
    </tbody>
  </table>
  @endsection

