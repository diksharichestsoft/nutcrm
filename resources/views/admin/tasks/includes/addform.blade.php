        <form action="" name="addTask" id="addTask" enctype="multipart/form-data">
          @csrf
        <div class="value">
            <div class="form-group row">
                <label for="title" class="col-sm-2 col-form-label">Title</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="title" required name="title" >
                <div class="error" id="error_title"></div>
                </div>
            </div>
            <div class="form-group row">
            <label for="file" class="col-sm-2 col-form-label">File</label>
            <div class="col-sm-10">
                <input type="file" id="file" class="form-control" name="file" accept="">
                <div class="error" id="error_file"></div>
                </div>
            </div>
            <div class="form-group row">
            <label for="task_type_id" class="col-sm-2 col-form-label">Task Type</label>
            <div class="col-sm-10">
                <select name="task_type_id" id="task_type_id" class="form-control select2">
                <option value="">Select Task Type</option>
                @foreach($taskTypes as $taskType)
                    <option value="{{ $taskType->id }}")>{{ $taskType->title }}</option>
                @endforeach
                </select>
                <div class="error" id="error_task_type_id"></div>
            </div>
            </div>

        @if($project_id == null )
        <input type="hidden" name="assign_myself" value="true">
            <div class="form-group row">
                <label for="project_id" class="col-sm-2 col-form-label">Select Project</label>
                <div class="col-sm-10">
                <select name="project_id" id="project_id" class="selectpicker" data-live-search="true" required>
                    <option value="">Select Project</option>
                    @foreach(Auth::user()->projects as $eachProject)
                    <option value="{{ $eachProject->id }}">{{ $eachProject->title }}</option>
                    @endforeach
                </select>
                <div class="error" id="error_project_id"></div>
                </div>
            </div>
            @else
            <input type="hidden" name="project_id" value="{{$project_id}}">
        @endif

          <div class="form-group row">
            <label for="summary" class="col-sm-2 col-form-label">Summary</label>
            <div class="col-sm-10">
                <textarea name="summary" id="job_desc" cols="30" rows="3" class="form-control"></textarea>
                <div class="error" id="error_summary"></div>
            </div>
        </div>

            <div class="form-group row">
              <label for="description" class="col-sm-2 col-form-label">Description</label>
                <div class="col-sm-10">
                    <input type="hidden" name="description"  id="description">
                    <div id="editor" style="border: 1px solid #ffffff42;"></div>
                </div>
            </div>
          <div class="form-group row">
            <label for="task_time" class="col-sm-2 col-form-label">Select Hours</label>
            <div class="col-sm-10">
                <input type="number" onchange="leadingZeros(this)" required min="00" value="00" class="form-control" id="project_hours" name="time_hours" style="width: 150px; display: initial">
                <input type="number" onchange="leadingZeros(this)" required min="00" value="30" id="project_minutes" class="form-control" style="width: 150px; display: initial" name="time_min">
              <div class="error" id="error_project_title"></div>
              </div>
          </div>
          <div class="form-group row">
            <label for="task_time" class="col-sm-2 col-form-label">Select Start Date</label>
            <div class="col-sm-10">
                <input type="date" id="start_date" class="form-control" required value="{{date('d-m-Y h:i')}}" name="start_date">
                <div class="error" id="error_start_date"></div>
              </div>
          </div>
          <div class="form-group row">
            <label for="task_time" class="col-sm-2 col-form-label">Select End Date</label>
            <div class="col-sm-10">
                <input type="date" id="end_date" class="form-control" name="end_date">
                <div class="error" id="error_start_date"></div>
              </div>
          </div>
          <div class="form-group row">
            <label for="priority_level" class="col-sm-2 col-form-label">Priority</label>
            <div class="col-sm-10">
              <select name="priority_level" id="priority_level" class="form-control select2">
                <option disabled selected >Select Priority</option>
                  <option value="1">Highest</option>
                  <option value="2" selected>Medium</option>
                  <option value="3">Low</option>
                  <option value="4">Lowest</option>
              </select>
              <div class="error" id="error_priority_level"></div>
            </div>
          </div>
          <div class="form-group row">
            <label for="priority_points" class="col-sm-2 col-form-label">Priority Points</label>
            <div class="col-sm-10">
                <input type="number" required value="0" onload="leadingZeros(this)" min="00" class="form-control" id="priority_points" name="priority_points" style="width: 150px; display: initial">
                <div class="error" id="error_priority_points"></div>
            </div>
          </div>


        </div>

          <div class="form-group row">
            <div class="offset-sm-2 col-sm-10">
              <button type="submit" class="btn btn-success">Add Task</button>
              <button type="reset" class="btn btn-danger">Reset</button>
            </div>
          </div>
        </div>

        </form>
        <script>
            var myEditor;

            InlineEditor
            .create( document.querySelector( '#editor' ) )
            .then( editor => {
                console.log( 'Editor was initialized', editor );
                myEditor = editor;
            } )
            .catch( err => {
                console.error( err.stack );
            } );
            $(document).ready(function (){
            $(".ck-file-dialog-button").hide();
            $(".ck-dropdown__button").hide();
            })
        </script>
    <script>

        $(document).ready(()=>{
           const time =  moment(new Date).format('DD-MM-YYYY HH:MM')
           $("#start_date").val(time)
           console.log(time)
        })
        $(".selectpicker").selectpicker({
                "title": "Select Options"
            }).selectpicker("render");
        var myEditor;

        InlineEditor
        .create( document.querySelector( '#editor' ) )
        .then( editor => {
            console.log( 'Editor was initialized', editor );
            myEditor = editor;
        } )
        .catch( err => {
            console.error( err.stack );
        } );
        $(document).ready(function (){
        $(".ck-file-dialog-button").hide();
        $(".ck-dropdown__button").hide();
        })

    </script>
