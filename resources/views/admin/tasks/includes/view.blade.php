<style>
    select#selectsatus {
      width: 100%;
  }
  </style>

<!-- Button trigger modal -->
{{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-lg">
    Launch demo modal
  </button> --}}


  <div class="modal fade" id="taskTypeModal" tabindex="-1" role="dialog" aria-labelledby="taskTypeModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Task Type</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="value">
                <div class="form-group row">
                <label for="task_type_id" class="col-sm-2 col-form-label">Task Type</label>
                <div class="col-sm-10">
                    <select name="task_type0" id="task_type0" class="form-control" onchange ="addTasks(this)">
                    <option value=""  >Select Task Type</option>
                    @foreach($taskTypes as $taskType)
                        <option value="{{ $taskType->id }}")>{{ $taskType->title }}</option>
                    @endforeach
                    </select>
                    <div class="error" id="error_task_type_id"></div>
                </div>
                </div>
            </div>
        </div>
      </div>
    </div>
</div>


  <div class="card">
                <div class="card-header ">
                  <h3 class="card-title">Your Tasks</h3>
                  <h6 class="count text-right">Total Tasks : {{$data->total()}}</h6>
                </div>
                <!-- /.card-header -->
               <div class="card-body">
                  <table class="table table-bordered">

                    @if(count($data)>0)
                    <thead>
                      <tr>
                        <th style="width: 10px">Action</th>
                        <th>ID</th>

                        @if(isset($active) && $active==3 && $project_id == null)
                        <th>Timer</th>
                        @endif
                        <th>Title</th>
                        <th>Summary</th>
                        <th>Info</th>
                        <th>Team</th>
                        <th>Time</th>
                        <th>Created At</th>
                        @if(isset($active) && $active!=1)
                        @can('tasks_change_status')
                        <th>Action</th>
                        @endcan
                        @endif
                      </tr>
                    </thead>
                    @endif
                    <tbody>
                        @forelse($data as $key=>$value)
                            <tr>
                                <td> <!-- Start ButtonGroup -->
                                <div class="btn-group">
                                    @can('tasks_assign')
                                    @if($value->task_status_id==1)
                                    <button data-id="{{$value->id}}" data-project-id="{{$project_id ?? $value->project_id}}" class="btn btn-primary btn-xs assignButton">Assign</button>
                                    @endif
                                    @if($value->task_status_id != 1)

                                    <button data-id="{{$value->id}}" data-project-id="{{$project_id ?? $value->project_id}}" class="btn btn-primary btn-xs assignButton">ReAssign</button>
                                    @endif
                                    @endcan
                                    @can('tasks_view')
                                    <a href="{{route('task-detial-view',$value->id)}}" target="_blank" class="btn btn-outline-success btn-xs  view"><i class="fas fa-eye"></i></a>
                                    @endcan
                                    @can('tasks_view')
                                    @endcan
                                    @can('tasks_edit')
                                    <button data-id="{{$value->id}}" onclick="editTask(this)" class="btn btn-outline-success btn-xs  update" ><i class="fas fa-pencil-alt"></i></button>
                                    @endcan
                                    <div class="modal fade" id="changestatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel1">Assign Task</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="changeStatuSbody">
          <form action="" name="changestatusform" id="changestatusform">
              @csrf
              <input type="hidden" name='status' id="changeStatus-status" value="">
              <input type="hidden" name='deal_id' id="changeStatus-deal_id" value="">
              <div class="value">
              <div class="form-group row">
                <label for="comment" class="col-sm-2 col-form-label">Comment</label>
                <div class="col-sm-10">
                  <textarea name="comment"  cols="30" rows="10" class="form-control"></textarea>
                  <div class="error" id="error_comment"></div>
                </div>
              </div>
              <div class="form-group row">
                <div class="offset-sm-2 col-sm-10">
                  <button type="submit" class="btn btn-success">Submit</button>
                </div>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>

<div class="modal fade" id="changestatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel1">Assign Tasks</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="changeStatuSbody">
          <form action="" name="changestatusform" id="changestatusform">
              @csrf
              <input type="hidden" name='status' id="changeStatus-status" value="">
              <input type="hidden" name='deal_id' id="changeStatus-deal_id" value="">
              <div class="value">
              <div class="form-group row">
                <label for="comment" class="col-sm-2 col-form-label">Comment</label>
                <div class="col-sm-10">
                  <textarea name="comment"  cols="30" rows="10" class="form-control"></textarea>
                  <div class="error" id="error_comment"></div>
                </div>
              </div>
              <div class="form-group row">
                <div class="offset-sm-2 col-sm-10">
                  <button type="submit" class="btn btn-success">Submit</button>
                </div>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>


                                    @can('tasks_delete')
                                    <button data-id="{{$value->id}}" onclick="removeTask(this)" class="btn btn-danger btn-xs  remove"><i class="fas fa-times"></i></button>
                                    @endcan
                                </div>
                                </td> <!-- send button group -->
                                <td>{{$value->id??''}}</td>
                                @if(isset($active) && $active==3 && $project_id == null)
                                <td>
                                    @php
                                        $secs = (int)$value->authUserTimerOnThisTask();
                                        $diffMinSec = gmdate("i:s", $secs);
                                        $diffHours = (gmdate("d", $secs)-1)*24 + gmdate("H", $secs);
                                    @endphp

                                    <p class = "text-center time-output" id="time_output_{{$value->id}}">{{$diffHours.":".$diffMinSec}}</p>
                                    <div id = "controls" class = "text-center">
                                      <button class="btn {{($value->haveTimerRunning()) ?  "btn-danger" : "btn-success"}}" id="time_button_{{$value->id}}" data-seconds="{{$secs}}" data-id="{{$value->id}}" onclick = "startTimerAjax(this,'#time_output_{{$value->id}}')">{{($value->haveTimerRunning()) ?  "PAUSE" : "START"}}</button>
                                    </div>
                                    @php
                                        if($value->haveTimerRunning()){
                                            $diffTimer = Carbon\Carbon::parse($value->latestTimer[0]->start_time)->diffInSeconds(now());
                                            echo "<script>myCounter('".($secs+$diffTimer)."', true, '#time_output_".$value->id."')</script>";
                                        }
                                    @endphp
                                </td>
                                @endif
                                <td style="word-break: break-all;">{{$value->title??''}}</td>
                                <td style="word-break: break-all;">{{$value->summary??''}}</td>
                                <td>
                                    <p class="smaller-text" >{{$value->project->title ?? ''}}</p>
                                    <b> Status :</b> <p class="smaller-text" >{{($value->thisTaskStatus->title ?? '')}}</p>
                                    <b> Added By :</b> <p class="smaller-text" >{{($value->createdBy->name ?? '')}}</p>
                                    <b> Updated By :</b> <p class="smaller-text" >{{($value->updatedBy->name ?? '')}}</p>
                                    <b> Total Time spent:</b> <p class="smaller-text" >(Hours:minutes){{secToHoursHelper($value->total_timer_time)}}</p>
                                </td>
                                <td>
                                    @foreach ($value->employees()->get() as $assignedEmployee )
                                        {{$assignedEmployee->name}}<br>
                                    @endforeach
                                    {{-- {{DB::table('users')->where(['id' => $value->user_id])->pluck('name')->first() ?? '' }} --}}
                                </td>
                                <td>{{$value->time}}</td>
                                <td>{{(!empty($value->created_at))?date('d-m-Y h:m:s a',strtotime($value->created_at)):''}}</td>
                                @if($value->deal_status!=1)
                                @can('tasks_change_status')

                            @if(isset($active) && $active!=1)

                                <td class="d-flex">
                                    <select name="sel" id="selectsatus" class="form-control deals_status" data-status="{{$value->task_status_id}}" data-id="{{ $value->id }}" >
                                    <option value="0">Select Status</option>
                                    @foreach($taskStatus as $status)
                                    <option value="{{$status->id}}"  {{($value->task_status_id==$status->id)?"selected":""}}>{{$status->title}}</option>
                                    @endforeach
                                </select>
                            </td>
                            @endif
                            @endcan
                            @endif
                            </tr>
                        @empty
                        <center> <h3> No Data Available </h3> </center>
                        @endforelse
                    </tbody>
                  </table>
                </div>
                {{$data->links()}}
                <!-- /.card-body -->
              </div>


  {{-- All in one modal --}}
  <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Project Status</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
             <div class="form-group">
                 <label for="">Comment</label>
             <textarea name="" id="inproc1" cols="30" rows="10" class="form-control"></textarea>
             </div>

          </div>
          <div class="modal-footer">
            <button type="button" id="save_deal1" class="btn btn-primary">Save Project Status</button>
          </div>
        </div>
      </div>
    </div>



  {{-- \ All in one modal --}}



  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Project Status</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <label for="">Comment</label>
         <textarea name="" id="inproc" cols="30" rows="10" class="form-control"></textarea>
        </div>
        <div class="modal-footer">
          <button type="button" id="save_deal" onclick="save_deal_data()" class="btn btn-primary">Save Project Status</button>
        </div>
      </div>
    </div>
  </div>
<script>

</script>
