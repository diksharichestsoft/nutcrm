<style>
    .daterangepicker .ranges li {font-size: 12px;padding: 8px 12px;cursor: pointer;background: #000000b0;}

  </style>
  <div class="card" id="data">
                <div class="card-header p-2" id="card_head">
                  <ul class="nav nav-pills">

                    @foreach($taskStatus as $eachTaskStatus)
                    <li class="nav-item"><a class="nav-link services" href="#view" onclick="fetch_data__data(1,this.getAttribute('data-id'))" data-id="{{$eachTaskStatus->id}}" data-toggle="tab">{{$eachTaskStatus->title}}({{$tasksCount[$eachTaskStatus->id] ?? 0}})</a></li>&nbsp;
                    @endforeach
                    @can('projects_all_leads_tab')
                    <li class="nav-item"><a class="nav-link services" href="#view" onclick="fetch_data__data(1,this.getAttribute('data-id'))" data-id="-1" data-toggle="tab">All Tasks({{array_sum($tasksCount)}})</a></li>&nbsp;
                    @endcan
                    @can('all_user_all_task_tab')
                    <li class="nav-item"><a class="nav-link services" href="#view" onclick="fetch_data__data(1,this.getAttribute('data-id'))" data-id="-2" data-toggle="tab">All User All Tasks</a></li>&nbsp;
                    @endcan
                    <div id="reportrange" style="background: transparent; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                    <i class="fa fa-calendar"></i>&nbsp;
                    <span class="ab"></span> <i class="fa fa-caret-down"></i>
                   </div>
                   <input type="hidden" id="start_date_range" >
                   <input type="hidden" id="end_date_range">
                   @if ($project_id != null)
                   <li style="align-self: center;">
                       <h3>
                           Tasks Of <a href="{{route('projects-view', $project_id)}}"  target="_blank">{{$thisProjectTitleOnly->title}}</a>
                       </h3>
                   </li>
                   @endif
                    <li class="nav-item search-right">
                      <div>
                        @if($project_id != null)
                            <a href="javascript:void(0)"  data-toggle="modal" data-target="#taskTypeModal"  class="btn btn-primary" >ADD Tasks</a>
                        @else
                            {{-- <a href="javascript:void(0)"  data-toggle="modal" data-target="#taskTypeModal" class="btn btn-primary" >Self Assign Tasks</a> --}}
                        <a class="btn btn-info" href="javascript:void(0)" id="fa-question-circle-button" onclick="Swal.fire('Ask Project Coordinator to assign tasks, you cannot assign tasks yourself anymore','','info')">
                          Self Assign Tasks
                          <i class="fa fa-question-circle" aria-hidden="true"></i>
                        </a>
                            {{-- <a href="javascript:void(0)" onclick="addTasks()"  class="btn btn-primary" >Self Assign Tasks</a> --}}
                        @endif
                      </div>
                     <div class="search_bar">
                        <div class="input-group" data-widget="sidebar-search">
                         <input class="form-control form-control-sidebar" id="search" type="search" placeholder="Search" aria-label="Search">
                        </div>
                     </div>
                    </li>
                  </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                  <div class="tab-content">
                    <div class="tab-pane" id="profile">
                       {{-- @include('admin.permissions.includes.addform') --}}
                    </div>

                    <div class="active tab-pane" id="view">
                       {{-- @include('admin.projects.includes.view') --}}
                      </div>
                    <!-- /.tab-pane -->
                  </div>
                  <!-- /.tab-content -->
                </div><!-- /.card-body -->
        </div>

  <div class="modal fade" id="changestatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel1">Assign Task</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="changeStatuSbody">
          <form action="" name="changestatusform" id="changestatusform">
              @csrf
              <input type="hidden" name='status' id="changeStatus-status" value="">
              <input type="hidden" name='deal_id' id="changeStatus-deal_id" value="">
              <div class="value">
              <div class="form-group row">
                <label for="comment" class="col-sm-2 col-form-label">Comment</label>
                <div class="col-sm-10">
                  <textarea name="comment"  cols="30" rows="10" class="form-control"></textarea>
                  <div class="error" id="error_comment"></div>
                </div>
              </div>
              <div class="form-group row">
                <div class="offset-sm-2 col-sm-10">
                  <button type="submit" class="btn btn-success">Submit</button>
                </div>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="assingTaskModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel1">Assign Task</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="changeStatuSbody">
          <form action="" name="assignTaskForm" id="assignTaskForm">
              @csrf
              <input type="hidden" name='task_id' id="assignTask-task_id" value="">
              <div class="value">
                <div class="form-group row">
                    <label for="comment" class="col-sm-2 col-form-label">Assign Task To</label>
                    <div class="col-sm-10">
                        <select name="user_id[]" class="form-select select2" id="employees_select" aria-label="Default select example" multiple>
                            <option selected>Open this select menu</option>
                        </select>
                        <div class="error" id="error_user_id"></div>
                    </div>
                </div>
              <div class="form-group row">
                <label for="comment" class="col-sm-2 col-form-label">Comment</label>
                <div class="col-sm-10">
                  <textarea name="comment"  cols="30" rows="10" class="form-control"></textarea>
                  <div class="error" id="error_comment"></div>
                </div>
              </div>
              <div class="form-group row">
                <div class="offset-sm-2 col-sm-10">
                  <button type="submit" class="btn btn-success">Submit</button>
                </div>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="createDeal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Projects</h5>
          <button type="button" class="close" id='createDealHide' data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body " id="createDealBody">
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="addComment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Comment</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body " id="addCommentBody">
        <form action="" name="addCommentForm" id="addCommentForm">
            @csrf
            <input type="hidden" name='deal_id' id="deal_id" value="">
          <div class="value">
            <div class="form-group row">
              <label for="comment" class="col-sm-2 col-form-label">Comment</label>
              <div class="col-sm-10">
                {{-- <textarea name="comment" id="comment" cols="30" rows="10" class="form-control"></textarea> --}}
                <input type="hidden" id="comment" class="form-control" name="comment">
                <div id="editor2" style="border: 1px solid #ffffff42;"></div>

                <div class="error" id="error_comment"></div>
              </div>
            </div>
            <div class="form-group row">
              <div class="offset-sm-2 col-sm-10">
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </div>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script>
    $(document).ready(function(){
      $(".services").eq(0).addClass("active");
    });
    $(document).on('click','.comment',function(){
      var id = $(this).attr('data-id');
      $('#deal_id').val(id);
      $('#comment_title').val('');
      $('#comment').val('');

    });

// Function to get form for update task
  function editTask(e)
  {
    $('#page-loader').show();

    var id=e.getAttribute('data-id');
    $.ajax({
      url:"{{route('tasks-edit')}}",
      type:"get",
      data:{id:id},
      success:function(data)
      {
        $("#createDeal").modal('show')
        $('#page-loader').hide();
        $('#createDealBody').empty().html(data);

      },
      error:function(){
        $('#page-loader').hide();
      }
    })
  }



  var objectToFormData = function(obj, form, namespace) {

var fd = form || new FormData();
var formKey;

for (var property in obj) {
    if (obj.hasOwnProperty(property)) {

        if (namespace) {
            formKey = namespace + '[' + property + ']';
        } else {
            formKey = property;
        }

        // if the property is an object, but not a File,
        // use recursivity.
        if (typeof obj[property] === 'object' && !(obj[property] instanceof File)) {

            objectToFormData(obj[property], fd, property);

        } else {

            // if it's a string or a File object
            fd.append(formKey, obj[property]);
        }

    }
}

return fd;

};
//   Function to submit task
  $(document).on('submit','#addSeoTask',function(e){
    e.preventDefault();
    Swal.showLoading()

    let projectServersFormData = [];
        let objectToStoreValues = {};
        $(".seotaskdata").each(function() {
            objectToStoreValues = {};
            let eachInput = $(this).find(".form-control");
            eachInput.each((e) => {
                if (e == 0) {
                    objectToStoreValues[`${eachInput[e].name}`] = eachInput[e].value;
                    projectServersFormData.push(objectToStoreValues);
                } else {
                    objectToStoreValues[`${eachInput[e].name}`] = eachInput[e].value;
                }
            })
        });
    console.log(projectServersFormData);
    var data_to_post = {
            'servers': projectServersFormData,

        };

    var descData = (myEditor.getData());
    $("#description").val(descData);
    //var data = new FormData(this);
    var form_data_instance = new FormData();
    var start_date = $("#start_date").val();
    var end_date = $("#end_date").val();
    data_to_post.servers.push({start_date:start_date, end_date: end_date})

var form_data = objectToFormData(data_to_post, form_data_instance, 'formKey');
    $("#page-loader").show();

      $.ajax({
          type:'post',
          url:"{{route('seoTasks-store')}}",
          headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          dataType: "JSON",

         xhr: function() {
               myXhr = $.ajaxSettings.xhr();
               return myXhr;
         },
         cache: false,
         contentType: false,
         processData: false,
          data:form_data,
          success:function(data){
            Swal.close()
              $(".services:first").trigger( "click" );
              $('#createDeal').modal('hide');
              $(".modal-backdrop").hide();
              Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Added Successfully',
                showConfirmButton: false,
                timer: 1500
	          })
          },
          error:function(data){
            $("#page-loader").hide();
            Swal.close()
            $.each(data.responseJSON.errors, function(id,msg){
              $('#error_'+id).html(msg);
            })
          }
        });
      });

     // Function to submit task
  $(document).on('submit','#addTask',function(e){
    e.preventDefault();
    Swal.showLoading()
    var descData = (myEditor.getData());
    $("#description").val(descData);
    var data = new FormData(this);
      $.ajax({
          type:'post',
          url:"{{route('tasks-store')}}",
          dataType: "JSON",
         xhr: function() {
               myXhr = $.ajaxSettings.xhr();
               return myXhr;
         },
         cache: false,
         contentType: false,
         processData: false,
          data:data,
          success:function(data){
            Swal.close()
              $(".services:first").trigger( "click" );
              $('#createDeal').modal('hide');
              $(".modal-backdrop").hide();
              Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Added Successfully',
                showConfirmButton: false,
                timer: 1500
            })
          },
          error:function(data){
            Swal.close()
            $.each(data.responseJSON.errors, function(id,msg){
              $('#error_'+id).html(msg);
            })
          }
        });
      });





  $(document).on('submit','#addCommentForm',function(e){
    e.preventDefault();
    var commentData = ($("#editor2").html());
    $("#comment").val(commentData);

    var data = new FormData(this);
      $.ajax({
        type:'post',
        url:"{{route('tasks-add-Comment')}}",
        dataType: "JSON",
        xhr: function() {
              myXhr = $.ajaxSettings.xhr();
              return myXhr;
        },
        cache: false,
        contentType: false,
        processData: false,
        data:data,
          success:function(data){
              $('#addComment').modal('hide');
              fetch_data__data(1,'');
              Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Added Successfully',
                showConfirmButton: false,
                timer: 1500
	      });
	      	      myEditor.setData('');

          },
          error:function(data){
            $.each(data.responseJSON.errors, function(id,msg){
              $('.error#error_'+id).html(msg);
            })
          }
        });
      });


    $(document).on('change','#selectsatus',function(){
      oldstatus = $(this).data('status')
      var status =$(this).val();
        $(this).val(oldstatus);
      var deal_id = $(this).attr('data-id');
      $('#changeStatus-status').val(status);
      $('#changeStatus-deal_id').val(deal_id);
      $('#changestatusform textarea').val('');
      $('#changestatus').modal('show');
    });

        $(document).on('click','.assignButton',function(){
        var task_id = $(this).attr('data-id');
        var project_id = $(this).attr('data-project-id');
        $('#assingTaskModal-status').val(status);
        $('#assignTask-task_id').val(task_id);
        $('#assingTaskModal textarea').val('');
        $
        $.ajax({
            type:'get',
            url:`{{route('get-employee-list')}}?projectId=${project_id}`,
            data: {"status":status},
            dataType: "JSON",
            xhr: function() {
                myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },
            cache: false,
            contentType: false,
            processData: false,
            success:function(data){

                $('#employees_select').empty();
                $.each(data.reasons, function(index, item) {
                $('#employees_select').append(`<option value="${item.id}">${item.name}</option>`)
            });
            },
            error:function(data){
            //   $.each(data.responseJSON.errors, function(id,msg){
            //     $('#error_'+id).html(msg);
            //   })
            }
            });
        $('#assingTaskModal').modal('show');
        });

    $(document).on('submit','#changestatusform',function(e){
        e.preventDefault();
          var data = new FormData(this);
      $.ajax({
          type:'post',
          url:"{{route('tasks-change-status')}}",
          dataType: "JSON",
         xhr: function() {
               myXhr = $.ajaxSettings.xhr();
               return myXhr;
         },
         cache: false,
         contentType: false,
         processData: false,
          data:data,
          success:function(data){
              if(data.status == 0){
                  $('#changestatus').modal('hide');
                  Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: data.message,
                        showConfirmButton: false,
                        timer: 7000
                    })
                    return
              }
              $('#changestatus').modal('hide');
              fetch_data__data(1,'');
          },
          error:function(data){
            $.each(data.responseJSON.errors, function(id,msg){
              $('#error_'+id).html(msg);
            })
          }
        });
  });
    $(document).on('submit','#assignTaskForm',function(e){
        e.preventDefault();
        $('#page-loader').show();
          var data = new FormData(this);

      $.ajax({
          type:'post',
          url:"{{route('tasks-assign-to-employee')}}",
          dataType: "JSON",
         xhr: function() {
               myXhr = $.ajaxSettings.xhr();
               return myXhr;
         },
         cache: false,
         contentType: false,
         processData: false,
          data:data,
          success:function(data){
              $('#assingTaskModal').modal('hide');
              $('#page-loader').hide();

              fetch_data__data(1,'');
          },
          error:function(data){
            $.each(data.responseJSON.errors, function(id,msg){
              $('#error_'+id).html(msg);
            })
          }
        });
  });

  function removeTask(e)
  {
    var id=e.getAttribute('data-id');
    swal({
    title: "Oops....",
    text: "Are You Sure You want to delete!",
    icon: "error",
    buttons: [
      'NO',
      'YES'
    ],
  }).then(function(isConfirm) {
    if (isConfirm) {
      $.ajax({
        url:"{{route('tasks-remove-data')}}",
        type:"post",
        data:{id:id},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        success:function(data){
        //   $('#view').empty().html(data);
        console.log(data);
            if(data.status == 0){
                Swal.fire({
                        icon: 'warning',
                        title: data.message,
                        showConfirmButton: true,
                        });
                        return
            }
          Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Remove Successfully',
                showConfirmButton: false,
                timer: 1500
                })
          fetch_data__data(1,'');
        }
      })
    } else {

    }
  });
  }


  function addTasks(e)
  {
    $('#page-loader').show();

    $.ajax({
      url:"{{route('tasks-add')}}",
      type:"get",
      data: {id: "{{$project_id}}", type: $(e).val()},
      success:function(data)
      {
        $("#createDeal").modal('show')
        $('#page-loader').hide();
        $('#createDealBody').empty().html(data);
      },
      error:function(error){
        $('#page-loader').hide();
      }
    })
  }


  // run on  change date start to end

  $(function() {
    var start = moment().subtract(moment().year()- 1947, 'years');
  var end = moment();
  function cb(start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      $('#start_date_range').val(start.format('YYYY-MM-DD'));
      $('#end_date_range').val(end.format('YYYY-MM-DD'));
      fetch_data__data(1,'');
  }

  $('#reportrange').daterangepicker({
      startDate: start,
      endDate: end,
      ranges: {
         'Today': [moment(), moment()],
         'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
         'Last 7 Days': [moment().subtract(6, 'days'), moment()],
         'Last 30 Days': [moment().subtract(29, 'days'), moment()],
         'This Month': [moment().startOf('month'), moment().endOf('month')],
         'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
  }, cb);
  cb(start, end);

  });

  $(document).on('click', '.pagination a', function(event){
    event.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    var sendurl=document.getElementsByClassName('services active')[0].getAttribute('data-id');
    fetch_data__data(page,sendurl,'');

  });
  document.querySelector("#search").addEventListener("keyup",(e)=>{
      // var active=$('a.active.services').attr('data-id');
      fetch_data__data(1,'');
  });

  function fetch_data__data(page,active)
  {

    $('#page-loader').show();
    try {
        if(timer){
              clearInterval(timer);
          }
    } catch (error) {

    }
    var start_date=document.querySelector('#start_date_range').value;
    var end_date=document.querySelector('#end_date_range').value;
    if(active==''){
      var active=document.getElementsByClassName('services active')[0].getAttribute('data-id');
    }

    var search=document.querySelector("#search").value;
    var project_id = "{{$project_id}}";
    var data={search:search,active:active,start_date:start_date,end_date:end_date, project_id: project_id};
    var make_url= "{{url('/')}}/admin/tasks/search?page="+page;
    $.ajax({
      url:make_url,
      data:data,
      success:function(data)
      {
          $('#view').empty().html(data);
        $('#page-loader').hide();

      },
      error:function(error){
        $('#page-loader').hide();
      }
    });
    }

    function leadingZeros(input) {
    if(!isNaN(input.value) && input.value.length === 1) {
      input.value = '0' + input.value;
    }
  }

  </script>
  <script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/inline/ckeditor.js"></script>

  <script>
      var myEditor;
      InlineEditor
      .create( document.querySelector( `#editor2` ) )
      .then( editor => {
          console.log( 'Editor was initialized', editor );
          myEditor = editor;
      } )
      .catch( err => {
          console.error( err.stack );
      } );
      $(document).ready(function (){
      // Hiding default extrack file and dropdown buttons
      $(".ck-file-dialog-button").hide();
      $(".ck-dropdown__button").hide();
      })

      $(".swalDefaultWarning").on('click', function () {
    swal({
  title: "How to add Tasks",
  text: "To add tasks, you'll have to go to Project Section, then go to tasks of that specific project, then add tasks ",
  icon: "warning",
  dangerMode: true,
})
 })

//  Task timer funcitons
function startTimerAjax (thisButton,selector) {
    id = $(thisButton).data('id');
    seconds = $(thisButton).data('seconds')
    $.ajax({
      url:"{{route('task-timer-start')}}",
      type: 'post',
      headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
      data:{id},
      success:function(data)
      {
        if(data.status == 0){
                  $('#changestatus').modal('hide');
                  Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: data.message,
                        showConfirmButton: false,
                        timer: 7000
                    })
                    return
              }else{
                  if(data.isRunning == 0){
                        Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: "Timer Stoped",
                                showConfirmButton: false,
                                timer: 3000
                            })
                        $(thisButton).html('START')
                        $(thisButton).addClass('btn-success')
                        $(thisButton).removeClass('btn-danger')
                    }else{
                        Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: "Timer Started",
                                showConfirmButton: false,
                                timer: 2000
                        });
                        $(thisButton).html('PAUSE')
                        $(thisButton).removeClass('btn-success')
                        $(thisButton).addClass('btn-danger')

                  }
              }
        running = (data.isRunning) ? 1 : 0
        myCounter(data.seconds,running,selector)
        // startPause(thisButton, data.seconds);
      },
      error:function(error){
        alert(error)
        console.log(error)
      }
    });

 }
let timer;
function myCounter(seconds,OnOff,selector) {
    timeHMS = seconds;
  if(OnOff){
    timer=setInterval(()=>{
    // document.querySelector(`${selector}`).innerHTML=new Date(seconds * 1000).toISOString().substr(11, 8);
    document.querySelector(`${selector}`).innerHTML= toHHMMSS(seconds);
    ++seconds;
  },1000);
  }else{
    clearInterval(timer);
    document.querySelector(`${selector}`).innerHTML=toHHMMSS(seconds);
    // document.querySelector(`${selector}`).innerHTML=new Date(seconds * 1000).toISOString().substr(11, 8);
    return ;
  }
}

const toHHMMSS = (secs) => {
    var sec_num = parseInt(secs, 10)
    var hours   = Math.floor(sec_num / 3600)
    var minutes = Math.floor(sec_num / 60) % 60
    var seconds = sec_num % 60

    return [hours,minutes,seconds]
        .map(v => v < 10 ? "0" + v : v)
        .filter((v,i) => v !== "00" || i > 0)
        .join(":")
}

  </script>
