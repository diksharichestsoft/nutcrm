@extends('admin.layout.template')
@section('contents')
@if($thisProjectTitleOnly)
@section('title')
{{$thisProjectTitleOnly->title}}
@stop
@endif
<div id="permissions_data">
@include('admin.tasks.index')
</div>
@endsection
