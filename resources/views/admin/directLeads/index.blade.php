@section('contents')
<div class="modal fade" id="createDeal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Deals</h5>
        <button type="button" class="close" id='createDealHide' data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body " id="createDealBody">
      </div>
    </div>
  </div>
</div>


<div class="card" id="data">
  <div class="card-header p-2" id="card_head">

  </div><!-- /.card-header -->

  <div class="card-body">

    <div id="pagination_employee">
      @include('admin.directLeads.includes.view ')
    </div>

    <!-- /.tab-content -->
  </div><!-- /.card-body -->
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>

    // Function for fetching the page data
function fetchDirectLeadData (){
      data = "{{json_encode($companies)}}";
      company_arr = decodeHtml(data);
      company_arr = JSON.parse(company_arr);
      $('#page-loader').show();
      $.ajax({
        url: "https://sagarwatertanks.com/public/api/getdetails",
        success: function (data) {
          $.each(data.data.data, function (key, val) {
            $("#directLeadsTbody").append(`<tr>
            <td>${key + 1}</td>
            <td>
                <div class="btn-group">
                    @can('direct_leads_add')
                    <button data-id="`+val.id+`" onclick="addcompanydeals(this)" class="btn btn-outline-success btn-xs  update" data-toggle="modal" data-target="#createDeal">Add</button>
                    @endcan
                    @can('direct_leads_delete')
                    <button data-id="`+val.id+`" onclick="removeDirectLeadData(this)"  class="btn btn-danger btn-xs  remove"><i class="fas fa-times"></i></button>
                    @endcan
                </div>
                <b> Created At :</b> <p class="smaller-text" >${moment(val.created_at).format("DD-MM-YYYY HH:MM a")}</p>
                <b> Updated At :</b> <p class="smaller-text" >${moment(val.updated_at).format("DD-MM-YYYY HH:MM a")}</p>
            </td>
            <td>${val.full_name}</td>
            <td>`+ getCompanyName(val.company_id) + `</td>
            <td>${val.email}</td>
            <td>${val.budget}</td>
            <td>${val.message}</td>
            <td>${val.phone}</td>
            <td>${val.city}</td>
            <td>${val.country}</td>
            <td>${val.ip}</td>
            <td>${val.status}</td>
            <td>${val.website_url}</td>
            <td>${val.utm_campaign}</td>
            <td>${val.utm_medium}</td>
            <td>${val.utm_source}</td>
            <td>${val.utm_term}</td>
            <td>${val.link}</td>
            </tr>`);
          });
          $('#page-loader').hide();

        },
        error: function (error) {
          $('#page-loader').hide();

        }
      });
    };

    //   Fetching data for view page
    fetchDirectLeadData();
    function decodeHtml(html) {
      let areaElement = document.createElement("textarea");
      areaElement.innerHTML = html;
      return areaElement.value;
    }

    function getCompanyName(id) {
      var result = company_arr.filter(obj => {
        return obj.id === 4;
      });
      return result[0].name
    }





// Function to fetch add deals form, input filled with current DirectLead Data
  function addcompanydeals(e)
{
  $('#page-loader').show();
  $.ajax({
    url:"{{route('directlead-deal-add')}}",
    type:"get",
    success:function(data)
    {
        // On success, fetching data particulare deal
        $('#createDealBody').empty().html(data);
        lead_id = $(e).attr('data-id');
        $.ajax({
        url: "https://sagarwatertanks.com/public/api/getdetails?id="+lead_id,
        success: function (singleLeadData) {
            console.log(singleLeadData.data.website_url);
                $("#url").val(singleLeadData.data.website_url);
                $("#client_email").val(singleLeadData.data.email);
                $("#client_phone").val(singleLeadData.data.phone);
                $("#price").val(singleLeadData.data.budget);
                $("#company").val(singleLeadData.data.company_id);

        },
        error: function (error) {
            alert("cannot connect to server, Please Try again later")
          $('#page-loader').hide();
        }
      });



        $('#page-loader').hide();
    },
    error:function(error){
        alert("cannot connect to server, Please Try again later")
      $('#page-loader').hide();
    }
  })
}

$(document).on('submit','#addCompany',function(e){
  e.preventDefault();
  var data = new FormData(this);
    $.ajax({
        type:'post',
        url:"{{route('deals-add-company')}}",
        dataType: "JSON",
       xhr: function() {
             myXhr = $.ajaxSettings.xhr();
             return myXhr;
       },
       cache: false,
       contentType: false,
       processData: false,
        data:data,
        success:function(data){
            $(".services:first").trigger( "click" );
            $('#createDeal').modal('hide');
        },
        error:function(data){
          console.log(data.responseJSON.errors);
          $.each(data.responseJSON.errors, function(id,msg){
            $('#error_'+id).html(msg);
          })
        }
      });


    });
    function removeDirectLeadData(e)
  {
      var id=e.getAttribute('data-id');
      swal({
          title: "Oops....",
          text: "Are You Sure You want to delete!",
          icon: "error",
          buttons: [
              'NO',
              'YES'
          ],
      }).then(function(isConfirm) {
          if (isConfirm) {
              $.ajax({
                  url:"https://sagarwatertanks.com/public/api/deletedetails?id="+id,
                  type:"delete",
                  success:function(data){
                      // $('#view').empty().html(data);
                      window.location.reload();
                  },
                  error: function(error){
                      alert(error.responseJSON.message);
                  }
              })
          } else {

          }
      });
  }


</script>
@endsection
