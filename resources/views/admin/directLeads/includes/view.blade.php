<section class="content">
    <div class="container-fluid">
      <div class="row">
          <div class="col-md-12">
              <!--  -->
              <div class="card-body">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th style="width: 10px">#</th>
                        <th>Action</th>
                        <th>Full Name</th>
                        <th>Company</th>
                        <th>Email</th>
                        <th>Budget</th>
                        <th>Message</th>
                        <th>Phone</th>
                        <th>City</th>
                        <th>Country</th>
                        <th>IP</th>
                        <th>Status</th>
                        <th>Website Url</th>
                        <th>Api Hitted From</th>
                        <th>UTM_CAMPAIGN</th>
                        <th>UTM_MEDIUM</th>
                        <th>UTM_SOURCE</th>
                        <th>UTM_TERM</th>
                        {{-- <th>Time Details</th> --}}
                      </tr>
                    </thead>
                    <tbody id="directLeadsTbody">

                    </tbody>
                  </table>
              </div>
              <!--  -->
          </div>
      </div>
    </div><!--/. container-fluid -->
  </section>
