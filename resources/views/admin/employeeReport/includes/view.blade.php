

    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!--  -->
                <div class="card-body">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width: 10px">Id</th>
                          <th>Name</th>
                          <th>Title</th>
                          <th>Date</th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach ($qry as $eachTask)
                              <tr>
                                  <td>{{$eachTask->id}}</td>
                                  <td>{{$eachTask->name}}</td>
                                  <td>{{$eachTask->title}}</td>
                                  <td>{{$eachTask->date}}</td>
                              </tr>
                          @endforeach

                      </tbody>
                    </table>
                </div>

                <!--  -->
            </div>
        </div>

      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->

    <script>



$(document).on('click', '.pagination a', function(event){
 event.preventDefault();
 var page = $(this).attr('href').split('page=')[1];
 var filter=document.querySelector("#tabs_filter").value;
 ajax('get',`{{Route("employee-full-report-search")}}`,data={page:page},'#pagination_employee');
});
</script>
