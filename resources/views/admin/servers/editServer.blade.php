<?php
use Illuminate\Support\Facades\Crypt;
?>
<!-- Update Servers Modal -->
<div class="modal fade" id="updateServerModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Server</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeServerModalBtn">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div id="update_server_modal_body" class="modal-body">
                <form id="update_server_form" action="" method="POST" enctype="multipart/form-data">
                    <div id="update_server_item_container" class="mb-3">
		    @csrf

		    @foreach ( $project_server->credentials as $project_server_credential )
			       <div class="update_server_items row"><div class="col-sm-12 my-2 update_server_items_subsection">
			@if ( $loop->iteration > 1 )
	<button onclick="$(this).parent('.update_server_items_subsection').parent('.update_server_items').remove()" style="float:right" class="btn btn-danger remove_server_item mt-4 mb-2"><i class="fa fa-minus" aria-hidden="true"></i></button>
			@endif
</div>

        	               <div class="form-group col-sm-3">
				<select class="form-select form-control" aria-label="Default select example" name="type" required>
					<option value="" selected disabled>Select a credential type</option>
				@foreach ( $server_credential_types as $type => $display_name)
				    	<option value="{{ $type }}" {{ $type == $project_server_credential->type ? 'selected' : '' }}>{{ $display_name }}</option>
				@endforeach

                         	</select>
		            </div>
                            <div class="form-group col-sm-3">
                                <input required class="form-control" name="url" placeholder="URL" value="{{ $project_server_credential->url }}">
			    </div>
                            <div class="form-group col-sm-3">
                                <input required class="form-control" name="username" placeholder="Username" value="{{ $project_server_credential->username }}">
                            </div>

                            <div class="form-group col-sm-3">
                                <input required class="form-control" type="password" name="password" placeholder="Password" value="{{Crypt::decryptString($project_server_credential->password)}}">
			    </div>
			</div>
		@endforeach
		    </div>

		   <div class="row form-group ">
                        <div class="col-sm-12">
                            <textarea class="form-control" name="description" placeholder="Description" id="update_servers_description">{{$project_server->description ?? ""}}</textarea>
                        </div>

                        <div class="col-sm-12 my-4">
                            <b class="mr-3">Initial Form:</b> <input type="file" name="update_pem_keys" id="update_pem_keys">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <a href="javascript:void(0);" class="btn btn-success mr-2" onclick="appendUpdateServerForm(this)" id="update_more_servers_child">
                                <i class="fa fa-plus" aria-hidden="true"></i> Add Server
                            </a>

                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<!-- Update Servers Modal End -->

<script>
function appendUpdateServerForm( updateServerFormBtn )
{


	var serverFormHtml = `<div class="update_server_items row"><div class="col-sm-12 my-2 update_server_items_subsection"><button onclick="$(this).parent(&quot;.update_server_items_subsection&quot;).parent(&quot;.update_server_items&quot;).remove()" style="float:right" class="btn btn-danger remove_server_item mt-4 mb-2"><i class="fa fa-minus" aria-hidden="true"></i></button></div><div class="form-group col-sm-3"><select class="form-select form-control" aria-label="Default select example" name="type" required><option value="" selected disabled>Select a credential type</option>@foreach ($server_credential_types as $type=> $display_name)<option value="{{$type}}">{{$display_name}}</option>@endforeach</select></div><div class="form-group col-sm-3"><input class="form-control" name="url" placeholder="URL" required></div><div class="form-group col-sm-3"><input class="form-control" name="username" placeholder="Username" required></div><div class="form-group col-sm-3"><input class="form-control" name="password" placeholder="Password" required></div></div>`;
    $("#update_server_item_container").append(serverFormHtml);

}

    $("#update_server_form").on('submit', function (e){
	    e.preventDefault();
        Swal.showLoading()
	var project_id = "{{ $project_id }}";
	var server_id = "{{$project_server->id}}";
        let projectServersFormData = [];
        let objectToStoreValues  = {};
        $(".update_server_items").each(function (){
            objectToStoreValues={};
                let eachInput= $(this).find(".form-control");
                eachInput.each( (e)=>{
                    if(e==0){
                    	objectToStoreValues[`${eachInput[e].name}`]=eachInput[e].value;
	                projectServersFormData.push(objectToStoreValues);
                    }else{
                        objectToStoreValues[`${eachInput[e].name}`]=eachInput[e].value;
                    }
                })
            });
	console.log( projectServersFormData );
	var servers_description = $("#update_servers_description").val();

	var data_to_post = {
		'servers'    : projectServersFormData,
		'project_id' : project_id,
		'description': servers_description,
		'pem_keys'   : null,
		'server_id'   : server_id,
	};

	if ($("#update_pem_keys")[0].files.length > 0) {
            data_to_post['pem_keys'] = $("#update_pem_keys")[0].files[0];
        }

	var form_data_instance = new FormData();
	var form_data          = objectToFormData(data_to_post, form_data_instance, 'formKey');

	$("#page-loader").show();

        $.ajax({
            url: "{{ route('project-servers-update') }}",
            type: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
	    success:function(data){
            Swal.close()
			$('#updateServerModal').modal('hide');
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Updated Successfully',
                showConfirmButton: false,
                timer: 1500
                })

			$("#page-loader").hide();
			renderProjectServersView();
			$('.modal-backdrop').remove();
			$('body').removeClass('modal-open');
			getCommentData();
		},
		error:function(data){
            Swal.close()
			console.log ('this is error' );
			console.log( data );
			$("#page-loader").hide();
	      $.each(data.responseJSON.errors, function(id,msg){
		$('#error_'+id).html(msg);

	      })
	    }
	});
        })
</script>
