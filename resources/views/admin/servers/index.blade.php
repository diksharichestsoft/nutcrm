<!-- Add Servers Modal -->
<div class="modal fade" id="addServerModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Server</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                    id="closeServerModalBtn">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div id="add_server_modal_body" class="modal-body">
                <form id="add_server_form" action="" method="POST" enctype="multipart/form-data">
                    <div id="server_item_container" class="mb-3">
                        @csrf
                        <div class="server_items row">

                            <div class="form-group col-sm-3">
                                <select class="form-select form-control" aria-label="Default select example" name="type"
                                    required>
                                    <option value="" selected disabled>Select a credential type</option>
                                    @foreach ($server_credential_types as $type => $display_name)
                                        <option value="{{ $type }}">{{ $display_name }}</option>
                                    @endforeach

                                </select>
                            </div>
                            <div class="form-group col-sm-3">
                                <input required class="form-control" name="url" placeholder="URL">
                            </div>

                            <div class="form-group col-sm-3">
                                <input required class="form-control" name="username" placeholder="Username">
                            </div>

                            <div class="form-group col-sm-3">
                                <input required class="form-control" type="password" name="password" placeholder="Password">
                            </div>


                        </div>

                    </div>
                    <div class="row form-group ">
                        <div class="col-sm-12">
                            <textarea class="form-control" name="description" placeholder="Description"
                                id="servers_description"></textarea>
                        </div>

                        <div class="col-sm-12 my-4">
                            <b class="mr-3">Pem Keys:</b> <input type="file" name="pem_keys" id="pem_keys" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <a href="javascript:void(0);" class="btn btn-success mr-2" onclick="appendServerForm(this)"
                                id="add_more_servers_child">
                                <i class="fa fa-plus" aria-hidden="true"></i> Add Server
                            </a>

                            <button type="submit" class="btn btn-primary">
                                Save</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addPswdModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form id="add_master_pswd" action="" method="POST">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Server Password</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="add_dsr_modal_body" class="modal-body">
            <div class="form-group">
            <input type="text" class="form-control" name="masterPswd" id="masterPswd" placeholder="Enter Master Password">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="intitializeUpdate()"> Save</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
    </form>
  </div>

@can ('project_servers_create')
<a href="#" id="addServerButton" class="btn btn-success  " data-toggle="modal" data-target="#addServerModal">Add New Server</a>
@endcan

<!-- Add Servers Modal End -->

<!-- Update Servers Modal -->
<div id="updateServerModalBlock">
</div>
<!-- Update Servers Modal End -->

<!-- Servers -->
<div class="row mt-4">

  
        <div class="col-sm-12 mt-2" >

            <ul class="list-unstyled list-group">
                {{-- <li class="list-group-item">
                    <div class="row">
                        <div class="col-sm-4">
                            <b>Created By:</b> {{ $project_server->user->name }}
                        </div>
                        <div class="col-sm-5">
                            <b>Created At:</b> {{ $project_server->created_at->format('d M Y  H:i:s') }}
			        </div>
                    @can ('project_servers_create')
                        <div class="col-sm-3">

                            @if($loop->iteration == 1)
                                <b>Actions:</b>
                                <div class="btn-group text-center ml-3">
                                    <button type="button" id="updateServerBtn" class="btn btn-outline-success btn-sm"
                                        data-id="{{ $project_server->id }}" onclick="renderUpdateServerModal( this )"> <i
                                        class="fas fa-pencil-alt"></i> </button>
                                </div>
                            @endif
                        </div>
                    @endcan
                    </div>
                </li> --}}

                

                <table class="table table-bordered">
                      <thead>
                        <tr>
                                <th>#</th>
                                <th>Credentials Type</th>
                                <th>Link</th>
                                <th>Username</th>
                                <th>Password  </th>
                                <th>Description</th>
                                <th>Actions</th>
                                <th>Created By</th>
                                <th>Created At</th> 
                                <th>Initial Form</th></tr>
                </thead>
                <tr>  @forelse ( $project_servers as $project_server )
                                 
                                 @foreach ($project_server->credentials as $project_server_credential)
                                <td>{{$project_server_credential->id}}</td>
                                 <td>{{ getDisplayNameOfProjectCredentailsType($project_server_credential->type) }}</td>
                                 <td><a
                                        href="{{ str_starts_with($project_server_credential->url, 'http') ? $project_server_credential->url : 'https://' . $project_server_credential->url }}"
                                        class="text-primary" target="_blank">Open link</a></td>

                                 <td width="10%">{{ $project_server_credential->username }}</td>
                                 {{-- {{$project_server_credential->password}} --}}
                                 <td><center><button data-id="{{$project_server_credential->id}}" class="btn btn-outline-success btn-xs view" onclick="viewMasterPswd(this)"><i class="fas fa-eye"></i></button></center></td>
            

                   

                     
                            
                                
                            <td>{{$project_server->description}}</td>
                                        
                                    @can ('project_servers_create')
                                    

                                            <td>
                                                <button type="button" id="updateServerBtn" class="btn btn-outline-success btn-sm"
                                                    data-id="{{ $project_server->id }}" onclick="renderUpdateServerModal( this )"> <i
                                                    class="fas fa-pencil-alt"></i> </button>
                                                    <button data-id="{{$project_server->id}}" onclick="deleteProjectServer(this)" class="btn btn-danger btn-xs  remove"><i class="fas fa-times"></i></button>
                                            </td>
                                  
                                @endcan

                                <td>
                                     {{ $project_server->user->name }} </td>
                                
                               
                                    <td>{{ $project_server->created_at->format('d M Y  H:i:s') }}</td>
                             <td>   
                            @if ($project_server->keys)
                                
                                    {{-- <b>Initial Form:</b> <a class="btn btn-success btn-sm" href="{{ asset($project_server->keys) }}" download> Download</a> --}}
                                    <a class="btn btn-success btn-sm" href="{{ asset($project_server->keys) }}" download="{{$thisProject->title}}.{{explode('.',$project_server->keys)[1]}}"> Download</a>
                              
                            @endif
                </td>

                </tr>
            
            @endforeach
    
    @empty
        <center class="mx-auto">
            <h3>No Data Available!</h3>
        </center>
    @endforelse
                </table>


              
<!-- Servers End -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    function appendServerForm(addServerFormBtn) {


        var serverFormHtml =
            `<div class="server_items row"><div class="col-sm-12 my-2 server_items_subsection"><button onclick="$(this).parent(&quot;.server_items_subsection&quot;).parent(&quot;.server_items&quot;).remove()" style="float:right" class="btn btn-danger remove_server_item mt-4 mb-2"><i class="fa fa-minus" aria-hidden="true"></i></button></div><div class="form-group col-sm-3"><select class="form-select form-control" aria-label="Default select example" name="type" required><option value="" selected disabled>Select a credential type</option>@foreach ($server_credential_types as $type => $display_name)<option value="{{ $type }}">{{ $display_name }}</option>@endforeach</select></div><div class="form-group col-sm-3"><input class="form-control" name="url" placeholder="URL" required></div><div class="form-group col-sm-3"><input class="form-control" name="username" placeholder="Username" required></div><div class="form-group col-sm-3"><input class="form-control" name="password" placeholder="Password" required></div></div>`;
        $("#server_item_container").append(serverFormHtml);

    }

    var objectToFormData = function(obj, form, namespace) {

        var fd = form || new FormData();
        var formKey;

        for (var property in obj) {
            if (obj.hasOwnProperty(property)) {

                if (namespace) {
                    formKey = namespace + '[' + property + ']';
                } else {
                    formKey = property;
                }

                // if the property is an object, but not a File,
                // use recursivity.
                if (typeof obj[property] === 'object' && !(obj[property] instanceof File)) {

                    objectToFormData(obj[property], fd, property);

                } else {

                    // if it's a string or a File object
                    fd.append(formKey, obj[property]);
                }

            }
        }

        return fd;

    };
    $("#add_server_form").on('submit', function(e) {
        e.preventDefault();
        Swal.showLoading()
        var project_id = "{{ 1 }}";
        let projectServersFormData = [];
        let objectToStoreValues = {};
        $(".server_items").each(function() {
            objectToStoreValues = {};
            let eachInput = $(this).find(".form-control");
            eachInput.each((e) => {
                if (e == 0) {
                    objectToStoreValues[`${eachInput[e].name}`] = eachInput[e].value;
                    projectServersFormData.push(objectToStoreValues);
                } else {
                    objectToStoreValues[`${eachInput[e].name}`] = eachInput[e].value;
                }
            })
        });
        console.log(projectServersFormData);
        var servers_description = $("#servers_description").val();

        var data_to_post = {
            'servers': projectServersFormData,
            'project_id': project_id,
            'description': servers_description,
            'pem_keys': null
        };
        if ($("#pem_keys")[0].files.length > 0) {

            data_to_post['pem_keys'] = $("#pem_keys")[0].files[0];
        }

        var form_data_instance = new FormData();

        var form_data = objectToFormData(data_to_post, form_data_instance, 'formKey');

        $("#page-loader").show();

        $.ajax({
            url: "{{ route('project-servers-store') }}",
            type: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            success: function(data) {
                Swal.close()
                Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Added Successfully',
                showConfirmButton: false,
                timer: 1500
                })
                $('#addServerModal').modal('hide');

                $("#page-loader").hide();
             
                $('.modal-backdrop').remove();
                $('body').removeClass('modal-open');
                getCommentData();
            },
            error: function(data) {
                Swal.close()
                console.log('this is error');
                console.log(data);
                $("#page-loader").hide();

                $.each(data.responseJSON.errors, function(id, msg) {
                    $('#error_' + id).html(msg);
                })
            }
        });
    })
    function deleteProjectServer(deleteProjectServerBtn) {
        var projectServerId = deleteProjectServerBtn.getAttribute('data-id');

        swal({
            title: "Oops....",
            text: "Are You Sure You want to delete!",
            icon: "error",
            buttons: ['NO', 'YES'],
        }).then(function(isConfirm) {
            if (isConfirm) {
                console.log("is confirmed");
                $("#page-loader").show();
                $.ajax({
                    url: "{{ route('project-servers-delete') }}",
                    type: "post",
                    data: {
                        'server_id': projectServerId
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(data) {
                        console.log("server deleted");
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Remove Successfully',
                            showConfirmButton: false,
                            timer: 1500
                            })
                        //renderProjectServersView();
                        //getCommentData();

                    },
                    complete: function() {
                        $("#page-loader").hide();
                    }
                });
            } else {
                console.log("not confirmed");
            }
        });

    }

    function renderUpdateServerModal(updateBtn) {
        var server_id = updateBtn.getAttribute("data-id");
        $.ajax({

            type: 'get',
            url: "{{ route('project-servers-edit') }}",
            data: {
                server_id
            },
            xhr: function() {
                myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },

            beforeSend: function() {
                $("#page-loader").show();
            },

            complete: function() {
                $("#page-loader").hide();
            },

            success: function(data) {
                $("#updateServerModalBlock").empty().html(data);
                $("#updateServerModal").modal('show');
            }

        })

    }

    function viewMasterPswd(e) {
        var id=e.getAttribute('data-id');
    Swal.fire({
  title: 'Enter Master Password',
  input: 'password',
  inputAttributes: {
    autocapitalize: 'off'
  },
  showCancelButton: true,
  confirmButtonText: 'Submit',
  showLoaderOnConfirm: true,
  preConfirm: (masterPassword) => {
    return  fetch("{{route('view-server-pswd')}}",{
            method:'post',
                dataType: "JSON",
                body :JSON.stringify({id, masterPassword}) ,
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
    }).then(response => {
        if (!response.ok) {
          throw new Error(response.statusText)
        }
        return response.json()
      })
      .catch(error => {
        Swal.showValidationMessage(
          `Request failed: Password didn't matched or something went wrong`
        )
      })
  },
  allowOutsideClick: () => !Swal.isLoading()
}).then((result) => {
  if (result.isConfirmed) {
      if(result.value.isValidated == true){
            Swal.fire({
            title: `<button id="copy" onclick="copyToClipboard('#serverpassword')" class="btn btn-success copy-link"><i class="far fa-copy"></i></button><br>
            <div class="box"  id="serverpassword" class="box" name="serverpassword" readonly>${result.value.password}</div>`,
            })
        }else{
            Swal.fire(`Password didn't matched`, '', `error`);
        }
  }
})
    }

function intitializeUpdate(){

// initializeCreateForm()

        e.preventDefault();

        var data = new FormData(this);
        console.log(data);
        $.ajax({
            type:'post',
            url:"{{route('view-server-pswd')}}",
            cache: false,
                contentType: false,
                processData: false,
                dataType: "JSON",
            data : {data: data},
        headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data:data,
            success:function(data){
                console.log(data);
                $('#add_master_pswd').modal('hide');
                // fetchServerPswd(1);
                Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Updated Successfully',
                showConfirmButton: false,
                timer: 1500
                })
            },
            error:function(data){
            $.each(data.responseJSON.errors, function(id,msg){
                $('#error_'+id).html(msg);

            })
            }
        });


}
function copyToClipboard(element) {
         var $temp = $("<input>");
         $("body").append($temp);
         $temp.val($(element).text()).select();
         document.execCommand("copy");
         $temp.remove();
         Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Copied',
                showConfirmButton: false,
                timer: 1500
                })
         }
</script>
