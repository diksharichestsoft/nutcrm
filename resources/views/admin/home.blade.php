
@extends('admin.layout.template')
@section('contents')
    @include('admin.layout.includes.announcement')
    @include('admin.layout.includes.info_box')

<style>
    .dashboard-table{
        table-layout: fixed;
    }

    .dashboard-table td{
        white-space: normal;
    }
.dashboard-table thead tr th:first-child, .dashboard-table tbody tr td:first-child{
    width: 10px;
}

.dashboard-table thead tr th:nth-child(2), .dashboard-table tbody tr td:nth-child(2){
    width: 80px;
}

.dashboard-table thead tr th:nth-child(3), .dashboard-table tbody tr td:nth-child(3){
    width: 70px;
}

.dashboard-table thead tr th:nth-child(4), .dashboard-table tbody tr td:nth-child(4){
    width: 57px;
}
    </style>


@php
        $priorities = ['Highest', 'Medium', 'Low', 'Lowest', 'None'];
    @endphp

    <input type="hidden" name="user_id" id="user_id" value="{{$id??''}}">

    @if(Auth::user()->haveBirthdayToday())
    {{--Birthday Theme--}}
    <div class="confetti">
        <div class="confetti-piece"></div>
        <div class="confetti-piece"></div>
        <div class="confetti-piece"></div>
        <div class="confetti-piece"></div>
        <div class="confetti-piece"></div>
        <div class="confetti-piece"></div>
        <div class="confetti-piece"></div>
        <div class="confetti-piece"></div>
        <div class="confetti-piece"></div>
        <div class="confetti-piece"></div>
        <div class="confetti-piece"></div>
        <div class="confetti-piece"></div>
        <div class="confetti-piece"></div>
    </div>{{--End birthday theme--}}
    @endif



    {{-- First row Start --}}
    <div class="row">
        <div class="col-3 time">
            <div class="total_hour">
                <h4>Total Hour</h4>
                <div id="timer">
                    <div class="row">
                        <div class="col-md-2">
                            <div id="main-time" style="font-size: 52px">
                                {{$data['punchings_sum']??''}}
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div id="minutes"></div>
                        </div>
                        <div class="col-md-2">
                            <div id="seconds"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div class="btn">
                        <form action="" id="punchin">
                            @csrf
                            <!-- <input > -->
                            <style>
                                .btn-punch-button {
                                    background-color: #dc3545;
                                    color: white;
                                }
                            </style>
                            <button
                                type="submit"
                                name="punch"
                                id="punch-btn"
                                value="Punch In"
                                class="btn btn-{{(isset($data['punched_in']) && $data['punched_in']>0)?'punch-button':'primary'}} "
                            >
                                {{(isset($data['punched_in']) &&
                                $data['punched_in']>0)?'Punch Out':'Punch In'}}
                            </button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">

            </div>
        </div>
        <div class="col-3">
            <h4>Today Attendence</h4>
            <br>
            <b>Note:</b><br>
            <h6>Please use your office system to Punch In/Out but if you forgot then E-mail at <a href="mailto:hr@richestsoft.in">hr@richestsoft.in</a> for Punch In/Out timing.</h6>
            @if(Auth::user()->missedDsrs() > 0)
            <h6>You have missed <span style="color: red"> {{Auth::user()->missedDsrs()}} </span> daily status reports(DSR) this month. We'll count your day as absent for missing daily status reports(DSRs). Make sure you submit Daily status report before leaving</h6>
            @endif
        </div>

        @if(Auth::user()->isBusinessSales())
        {{-- Business Sales row --}}
            <div class="col-6">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Deals</h3>

                            <div class="card-tools">
                                <a href="{{route('deals')}}" target="_blank">View All</a>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0" style="height:300px">
                            <table class="table table-head-fixed text-nowrap dashboard-table">
                            <thead>
                                <tr>
                                <th style="width: 33%" scope="col">Meeting(Today)</th>
                                <th  style="width: 33%" scope="col">Hot Deals</th>
                                <th  style="width: 33%" scope="col">In queue</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $inMeeting = App\Models\Deals::where('deal_status', 3)->where('meeting_time', '!=', null)->whereDate('meeting_time', today())->get(['id', 'client_name','meeting_time' ]);
                                    $hotDeals = App\Models\Deals::where('deal_status', 5)->get(['id', 'client_name']);
                                    $inQueue = App\Models\Deals::where('deal_status', 1)->get(['id', 'client_name']);
                                @endphp
                                    <tr>
                                        <td class="col-4">
                                            <table style="width: 100%">
                                                @foreach ($inMeeting as $eachInMeeting)

                                                <tr>
                                                    <td><a target="_blank" href="{{route('companyDeal-view', $eachInMeeting->id)}}"> {{$eachInMeeting->client_name}} </a> at {{date('H:i a', strtotime($eachInMeeting->meeting_time))}}</td>
                                                </tr>
                                                @endforeach
                                            </table>
                                        </td>
                                        <td>
                                            <table style="width: 100%">
                                                @foreach ($hotDeals as $eachHotDeals)

                                                <tr>
                                                    <td><a target="_blank" href="{{route('companyDeal-view', $eachHotDeals->id)}}"> {{$eachHotDeals->client_name}}</a></td>
                                                </tr>
                                                @endforeach
                                            </table>
                                        </td>
                                        <td>
                                            <table style="width: 100%">
                                                @foreach ($inQueue as $eachInQueue)

                                                <tr>
                                                    <td><a target="_blank" href="{{route('companyDeal-view', $eachInQueue->id)}}"> {{$eachInQueue->client_name}}</a></td>
                                                </tr>
                                                @endforeach
                                            </table>
                                        </td>
                                    </tr>
                            </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
            </div>
        {{-- Business sales Row End --}}
        @endif

        <div class="col-6">
            <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Quick Tasks</h3>

                  <div class="card-tools">
                      <a href="{{route('manageTasks')}}" target="_blank">View All</a>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0" style="height:300px">
                    <table class="table table-head-fixed text-nowrap dashboard-table">
                    <thead>
                      <tr>
                        <th style="width: 10px">#</th>
                        <th>Task</th>
                        <th>Send By</th>
                        <th><i class="fa fa-clock"></i></th>
                        <th style="width: 40px">Priority</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($currentUser->quickTasks as $key=>$thisUserQuickTasks)
                            <tr>
                                <td>{{$key+1}}.</td>
                                <td>
                                    <a href="{{route('manageTasks-detial-view',$thisUserQuickTasks->id)}}" target="_blank"> {{$thisUserQuickTasks->title}}</a>
                                </td>
                                <td>
                                    {{$thisUserQuickTasks->assignedBy->name}}
                                </td>
                                <td>
                                    {{$thisUserQuickTasks->created_at}}
                                </td>
                                <td><span class="badge bg-info">
                                    {{$priorities[$thisUserQuickTasks->priority_level-1]}}
                                    </span></td>
                            </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
        </div>

    </div>
    {{-- First row End --}}

    @if(Auth::user()->isHr())
    {{-- HR row --}}
    <div class="row">
        <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Leaves</h3>

                        <div class="card-tools">
                            <a href="{{route('leaves')}}" target="_blank">View All</a>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0" style="height:300px">
                        <table class="table table-head-fixed text-nowrap dashboard-table">
                        <thead>
                            <tr>
                            <th style="width: 10px">#</th>
                            <th>Subject</th>
                            <th>Name</th>
                            <th><i class="fa fa-clock"></i>Created At</th>
                            <th style="width: 40px">Leave Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $leaves = App\Models\Leave::with(['createdByUser:id,name'])->where('status','!=',0 )->orderBy('updated_At', 'DESC')->get();
                            @endphp
                            @foreach ($leaves as $key=>$eachLeave)
                                <tr>
                                    <td>{{$key+1}}.</td>
                                    <td>
                                        <a href="{{route('leaves',$eachLeave->id)}}" target="_blank"> {{$eachLeave->subject}}</a>
                                    </td>
                                    <td>
                                        {{$eachLeave->createdByUser->name ?? ''}}
                                    </td>
                                    <td>{{date('d-m-Y H:i a',strtotime($eachLeave->created_at)) ?? ''}}</td>
                                    <td>
                                        {{date('d-M-Y',strtotime($eachLeave->leave_date)) ?? ''}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
        </div>
        <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Candidates Lined Up today</h3>

                        <div class="card-tools">
                            <a href="{{route('candidates')}}" target="_blank">View All</a>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0" style="height:300px">
                        <table class="table table-head-fixed text-nowrap dashboard-table">
                        <thead>
                            <tr>
                            <th style="width: 10px">#</th>
                            <th>Name</th>
                            <th><i class="fa fa-clock"></i>Date/Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $qry =  App\Models\ManageCandidate::whereDate('time_schedule', today());
                            if(Auth::user()->can('candidate_all_candidate_of_any_user')){
                                $candidateQry = $qry;
                            }else{
                                $candidateQry = $qry->where('hr_id', Auth::user()->id);
                            }
                            $candidatesToday = $candidateQry->get(['id', 'name', 'time_schedule']);
                            @endphp
                            @foreach ($candidatesToday as $key=> $eachCandidate)
                                <tr>
                                    <td>{{$key+1}}.</td>
                                    <td>
                                        {{$eachCandidate->name}}
                                    </td>

                                    <td>{{date('d-m-Y H:i a',strtotime($eachCandidate->time_schedule))}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
        </div>
    </div>
    {{-- Hr Row End --}}
    @endif
    {{-- Second row Start --}}
    <div class="row">
        <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Your Project Tasks</h3>

                        <div class="card-tools">
                            <a href="{{route('tasks', 'all')}}" target="_blank">View All</a>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0" style="height:300px">
                        <table class="table table-head-fixed text-nowrap dashboard-table">
                        <thead>
                            <tr>
                            <th style="width: 10px">#</th>
                            <th>Task</th>
                            <th>Project</th>
                            <th><i class="fa fa-clock"></i></th>
                            <th style="width: 40px">Priority</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($currentUser->tasksAssigned as $key=> $thisUserTasks)
                                <tr>
                                    <td>{{$key+1}}.</td>
                                    <td>
                                        <a href="{{route('task-detial-view',$thisUserTasks->id)}}" target="_blank"> {{$thisUserTasks->title}}</a>
                                    </td>
                                    <td>
                                        <a href="{{route('projects-view',$thisUserTasks->project->id)}}" target="_blank"> {{$thisUserTasks->project->title}} </a>
                                    </td>
                                    <td>{{$thisUserTasks->created_at}}</td>
                                    <td><span class="badge bg-info">
                                        {{$priorities[(($thisUserTasks->priority_level) ?? 5)-1]}}
                                        </span></td>
                                </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
        </div>
        <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Your To do</h3>

                        <div class="card-tools">
                            <a href="{{route('todo')}}" target="_blank">View All</a>
                        {{-- <ul class="pagination pagination-sm float-right">
                            <li class="page-item"><a class="page-link" href="#">«</a></li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">»</a></li>
                        </ul> --}}
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0" style="height:300px">
                        <table class="table table-head-fixed text-nowrap dashboard-table">
                        <thead>
                            <tr>
                            <th style="width: 10px">#</th>
                            <th>Task</th>
                            <th><i class="fa fa-clock"></i></th>
                            <th>Priority</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($currentUser->todos as $key=> $thisUserTodo)
                                <tr>
                                    <td>{{$key+1}}.</td>
                                    <td>
                                        {{$thisUserTodo->title}}
                                    </td>

                                    <td>{{$thisUserTodo->created_at}}</td>
                                    <td><span class="badge bg-info">
                                        {{$priorities[(($thisUserTodo->priority_level) ?? 5)-1]}}
                                        </span></td>
                                </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
        </div>
    </div>
    {{-- Second row End --}}




 <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
 <script>
   var counter = '';
   function secToTime(time){
         return new Date(time * 1000).toISOString().substr(11, 8);
     }
     $("#main-time").html(secToTime('{{$data["diffTime"] ??  $data["in_minuts"]}}'));


 function myFunction (time = null){
     counter = setInterval( function (){
     time++;
     newTime = secToTime(time);
     $("#main-time").html(newTime)
     }, 1000)
 }

 $(document).on('submit','#punchin',function(e){
     e.preventDefault();
     var data = new FormData(this);
     $.ajax({
         type:'post',
         url:"{{route('punchin')}}",
         dataType: "JSON",
         cache: false,
         contentType: false,
         processData: false,
         data:data,
         success:function(data){
           if(parseInt(data.punch)>0){
             $('#punch-btn').removeClass('btn-primary');
             $('#punch-btn').addClass('btn-danger');
             $('#punch-btn').html('Punch Out');
             myFunction(data.in_minuts);
           }
           

           else{
             $('#punch-btn').removeClass('btn-danger');
             $('#punch-btn').addClass('btn-primary');
             $('#punch-btn').html('Punch In');
             if(data.isTimerStopped)
             Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: "Punched out and stoped your all active timers on tasks",
                        showConfirmButton: true,
                        // timer: 7000
                    })
             clearInterval(counter);
           }
           console.log(data.in_minuts);
           stopedTime =secToTime(data.in_minuts);
           $('#main-time').html(stopedTime);
         },
         error:function(data){
           console.log(data.responseJSON.errors);
         }
     });
 });


 </script>
 <script>
     @if(isset($data['punched_in']) && $data['punched_in']>0)
     in_minuts = {{$data["diffTime"] ??  $data["in_minuts"]}};
     myFunction(in_minuts);
     setInterval( function () {
     myFunction(in_minuts);
     in_minuts = in_minuts + 30000
     clearInterval(counter);
   },30000);
     @endif

 </script>
 @endsection
