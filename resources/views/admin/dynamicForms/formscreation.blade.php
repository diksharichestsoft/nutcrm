@extends('admin.layout.template')
@section('contents')
<div class="card">
  <div class="card-body">
      <form class="form" id="generate_dynamic_form" method="post">
          <div class="form-group">
             <input type="text" placeholder="Domain Name" name="domain" class="form-control"/>
             <div id="error_domain"></div>
          </div>
          <div class="form-group">
              <select class="form-control"  name="company">
                  {{--Please check for the constant secret keys in  config/formstype.php--}}
                 @foreach($companies as $value)
                    <option value="<?php echo Config::get('formstype.secret.'.$value->id); ?>">{{$value->name}}</option>
                 @endforeach
              </select>
              <div id="error_company"></div>
          </div>
          <div class="form-group">
              <select class="form-control" name="form_type" onload="getFormView(this)" onchange="getFormView(this)"  id="select_form">
                  @foreach(Config::get('formstype.forms') as $key=>$value)
                  <option value="{{$key}}">{{$value}}</option>
                  @endforeach
              </select>
              <div id="error_form_type"></div>
          </div>
          <div class="form-group">
             <input type="text" placeholder="Redirect to page after success" name="redirected_to_success" class="form-control"/>
          </div>
          <div class="form-group">
              <button type="submit" class="btn btn-sm btn-success">Generate Dynamic Form</button>
          </div>
      </form>
  </div>
</div>
<script>
$("#generate_dynamic_form").on("submit",(e)=>{
    e.preventDefault();
    const data=getformdata("generate_dynamic_form");
     $.ajax({
         type:'post',
         url:"{{route('dynamic-form.add')}}",
         dataType: "JSON",
         xhr: function(){
                   myXhr = $.ajaxSettings.xhr();
                   return myXhr;
         },
         headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
          data:data,
          success:function(data){
            Swal.fire({
             title:`Copy Script <button id='copy' onclick='copyToClipboard("#copyScript")' class='btn btn-success copy-link'><i class='far fa-copy'></i></button><br>
                     <input type='hidden' value='${data}' id='copyScript'>`,
                 text: `${data}`,
                 icon: "success",
                 buttons: [
                   'YES'
                 ],
                }).then(function(isConfirm) {
                       window.location.reload();
                });
          },
          error:function(data){
                   console.log(data);
                // $.each(data.responseJSON.errors, function(id,msg){
                //   $('#error_'+id).html(msg);
                //  })
          }
 });

});

function getformdata(id){
     let form = document.querySelector(`#${id}`);
     let data = new FormData(form);
     var object={};
     for (let [key, value] of data) {
         object[key]=value;
     }
     var FormValuedata=object;
     return FormValuedata;
}

function copyToClipboard(element) {
 var $temp = $("<input>");
 $("body").append($temp);
 $temp.val($(element).val()).select();
 document.execCommand("copy");
 $temp.remove();
 Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Copied',
        showConfirmButton: false,
        timer: 1500
    })
}
</script>
@endsection
