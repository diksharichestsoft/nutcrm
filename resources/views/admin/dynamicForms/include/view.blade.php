   <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card-body">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width: 10px">#</th>
                          <th>Action</th>
                          <th>Id</th>
                          <th>Company Id</th>
                          <th>Form Type</th>
                          <th>Domain</th>
                        </tr>
                      </thead>
                      <tbody>

                        @forelse($dynamicforms as $key=>$dynamicform)
                          <tr>
                            <td>{{$key +1}}</td>
                              <td>
                            <a href="{{route('dynamicform-detail',$dynamicform->id)}}" target="_blank" class="btn btn-outline-success btn-xs  view"><i class="fas fa-eye"></i></a></td>

                              <td>{{$dynamicform->id}}</td>

                            {{-- <td>
                                <p>{{$target->project->title  ?? 'NaN'}} </p>
                            </td> --}}
                          <td>
                                {{$dynamicform->company_id	}}
                            </td>
                              <td>
                                {{Config::get('formstype.forms.'.$dynamicform->form_type)}}
                            </td>
                            <td>
                                {{$dynamicform->domain}}
                            </td>

                            {{-- <td>
                                {{date_format(date_create($target->target_date), "d-m-Y") ?? ""}}
                            </td>
                            <td>
                                {{timestampToDateAndTimeObj($target->created_at)->date ?? ""}}
                            </td> --}}

                        </tr>
                        @empty
                        <tr>
                            <td colspan="9">
                                <p class="text-center h4">
                                    No Data Found
                                </p>
                            </td>
                        </tr>
                        @endforelse

                      </tbody>
                    </table>
                </div>
                {{$dynamicforms->links()}}
                <!--  -->
            </div>
        </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
    <script>


        $(".toggle_user_login").on('click', function (e){
            e.preventDefault();
            var id = $(this).attr('data-id');
            $.ajax({
                type: "post",
                url: "{{route('employee-login-toggle')}}",
                headers:
                {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {id:id},
                dataType: "JSON",
                success: function (response) {

                      document.querySelector(`#${e.target.id}`).checked= response.success;
                }
            });
        })
    $(document).on('click', '#post_pagination .pagination a', function(event){
 event.preventDefault();
 var page = $(this).attr('href').split('page=')[1];
 var filter=document.querySelector("#tabs_filter").value;
 ajax('get',`{{Route("employee-pagination")}}`,data={page:page},'#pagination_employee');
});


function removeCompanyProject(e)
{

  var id=e.getAttribute('data-id');
  swal({
  title: "Oops....",
  text: "Are You Sure You want to delete!",
  icon: "error",
  buttons: [
    'NO',
    'YES'
  ],
}).then(function(isConfirm) {
  if (isConfirm) {
    $.ajax({
      url:"{{route('portfolio-remove-data')}}",
      type:"post",
      data:{id:id},
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
      success:function(data){

        Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Remove Successfully',
                showConfirmButton: false,
                timer: 1500
                })
        }
    })
  } else {

  }
});
}
</script>
