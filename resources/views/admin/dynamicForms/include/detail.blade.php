@extends('admin.layout.template')

@section('contents')

<table class="table">
    <tbody>
      <tr>
        <th scope="row">ID</th>
        <td>{{$thisDetails->id?? ""}}</td>
      </tr>
      <tr>
        <th scope="row">Company Id</th>
        <td>{{$thisDetails->company_id?? ""}}</td>
      </tr>
      <tr>
        <th scope="row">Form Type</th>
        <td>{{Config::get('formstype.forms.'.$thisDetails->form_type)}}</td>
      </tr>
      <tr>
        <th scope="row">Domain</th>
        <td>{{$thisDetails->domain?? ""}}</td>
      </tr>
      <tr>
        <th scope="row">Script</th>
        <td style="word-break: break-all;">{{$thisDetails->script?? ""}}</td>
      </tr>
    </tbody>
  </table>@endsection
