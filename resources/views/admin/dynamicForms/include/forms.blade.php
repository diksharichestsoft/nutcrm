<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<link rel="stylesheet" href="http://localhost/portal/public/css/adminlte.min.css">

<style>
.errors{
    color:#ff0000;
}

</style>
@if(Config::get('formstype.forms.'.$formType)=="quote")
@php 
$Id=Str::random(32);
@endphp
<div class="container">
          <div class="section-free-quote-form">
            <form id="{{$Id}}"  method="post" accept-charset="utf-8" novalidate="novalidate" >
                <div class="row">
                    <div class="col-md-6 mb-3">
                        <label>Full Name</label>
                        <input class="form-control" id="{{$Id}}fname" name="firstname" type="text" placeholder="Full Name" value="">
                        <div id="{{$Id}}_fname" class="errors"></div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label>Email</label>
                        <input class="form-control" id="{{$Id}}email" name="email" type="email" placeholder="Email" value="">
                        <div id="{{$Id}}_email" class="errors"></div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label>Phone Number</label>
                        <input class="form-control" id="{{$Id}}phone" name="mobilephone" type="text" placeholder="Country Code + Phone Number" value="">
                        <div id="{{$Id}}_phone" class="errors"></div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label>Estimated Budget</label>
                         <input class="form-control" id="{{$Id}}estimated_budget" name="budget" type="text" placeholder="Estimated Budget" value="">
                         <div id="{{$Id}}_estimated_budget" class="errors"></div>
                    </div>

                    <div class="col-md-12 mb-3">
                        <label>Company Name</label>
                        <input class="form-control" id="{{$Id}}company_name" name="company_name" type="text" placeholder="Company Name" value="">
                        <div id="{{$Id}}_company_name" class="errors"></div>
                    </div>

                    <div class="col-md-12 mb-3">
                        <label>Message</label>
                        <textarea placeholder="Enter Your Messaage"  id="{{$Id}}message" name="message" style="resize:none;" class="form-control"></textarea>
                        <div id="{{$Id}}_message" class="errors"></div>
                    </div>

                  
                    <div class="col-md-12 text-center mt-4">
                        <input type="hidden" name="status" id="0" >
                        <button type="submit" class="btn btn-primary" id="{{$Id}}-button">Request a Quote</button>
                    </div>
                </div>
            </form>
          </div>
</div>
<script>
if(document.querySelector("#{{$Id}}")){
document.querySelector("#{{$Id}}").addEventListener("submit",(e)=>{
    e.preventDefault();
    var data=getformdata1("#{{$Id}}");
    data["company_id"]="{{$company}}";
    $.ajax({
        type: "post",
        url: "https://sagarwatertanks.com/public/api/contactus",
        data: data,
        success: function(data){
            if(data.message){
                window.location.href="{{$redirection}}";
            }
        } ,
         error: function(data) {
             console.log(data);
            errorhighlight('{{$Id}}fname',data.responseJSON.message.firstname);
            errormessage('{{$Id}}_fname',data.responseJSON.message.firstname);
            errorhighlight('{{$Id}}phone',data.responseJSON.message.mobilephone);
            errormessage('{{$Id}}_phone',data.responseJSON.message.mobilephone);
            errorhighlight('{{$Id}}email',data.responseJSON.message.email);
            errormessage('{{$Id}}_email',data.responseJSON.message.email);
            errorhighlight('{{$Id}}company_name',data.responseJSON.message.company_name);
            errormessage('{{$Id}}_company_name',data.responseJSON.message.company_name);
            errorhighlight('{{$Id}}estimated_budget',data.responseJSON.message.estimated_budget);
            errormessage('{{$Id}}_estimated_budget',data.responseJSON.message.estimated_budget);
            errorhighlight('{{$Id}}message',data.responseJSON.message.message);
            errormessage('{{$Id}}_message',data.responseJSON.message.message);
         },
         beforeSend: function(){

              $("#{{$Id}}-button").html("Loading...");
          },
          complete: function(){
            $("#{{$Id}}-button").html("Request a Quote");
          },
       });
});
}
</script>
@endif
@if(Config::get('formstype.forms.'.$formType)=="common" || Config::get('formstype.forms.'.$formType)=="popup")
@php 
$Id=Str::random(32);
@endphp
<div class="container">
            <div class="row align-items-center gy-5">
                <div class="col-md-12 hero-img">
                    <!-- <img src="images/solution-details-banner.png" alt="On-Demand Apps" /> -->
                    <div class="cons-form-wrapper01 position-relative">
                        <!-- <div class="ready-to-blk">
						    <h5>Ready To Start Your Business?</h5>
							<p>Leave Your Details Below</p>
						</div>-->
                        <form  id="{{$Id}}" method="post"  accept-charset="utf-8" novalidate="novalidate">
                            <div class="row">
                                <div class="col-md-12 mb-3">
                                    <input class="form-control" id="{{$Id}}consName" name="firstname" type="text" placeholder="Full Name">
                                    <div id="{{$Id}}_cs_fullname" class="errors"></div>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <input class="form-control" id="{{$Id}}consEmail" name="email" type="email" placeholder="Email">
                                    <div id="{{$Id}}_cs_email" class="errors"></div>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <input class="form-control" id="{{$Id}}consNumber" name="mobilephone" type="tel" placeholder="Country Code + Phone">
                                    <div id="{{$Id}}_cs_mobilephone" class="errors"></div>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <textarea class="form-control" id="{{$Id}}consMessage" name="message" style="resize:none;" placeholder="Message"></textarea>
                                    <div id="{{$Id}}_cs_message" class="errors"></div>
                                </div>
                                <input type="hidden" name="status" value="2">
                                <input type="hidden" name="recaptcha" id="recaptcha">
                                <div class="col-md-12 text-center mt-3">
                                    <button type="submit" class="btn btn-primary w-100" id="{{$Id}}-button">Submit</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
</div>
<script>
if(document.querySelector("#{{$Id}}")){
    document.querySelector("#{{$Id}}").addEventListener("submit",(e)=>{
        e.preventDefault();
        var data=getformdata1("#{{$Id}}");
        data["company_id"]="{{$company}}";
        $.ajax({
        type: "post",
        url: "https://sagarwatertanks.com/public/api/contactus",
        data: data,
        success: function(data){
            if(data.message){
                window.location.href="{{$redirection}}";
            }
        } ,
         error: function(data) {
            errorhighlight('#{{$Id}}consName',data.responseJSON.message.firstname);
            errormessage('#{{$Id}}_cs_fullname',data.responseJSON.message.firstname);
            errorhighlight('#{{$Id}}consNumber',data.responseJSON.message.mobilephone);
            errormessage('#{{$Id}}_cs_mobilephone',data.responseJSON.message.mobilephone);
            errorhighlight('#{{$Id}}consEmail',data.responseJSON.message.email);
            errormessage('#{{$Id}}_cs_email',data.responseJSON.message.email);
            errorhighlight('#{{$Id}}consMessage',data.responseJSON.message.message);
            errormessage('#{{$Id}}_cs_message',data.responseJSON.message.message);
         },
         beforeSend: function(){
            $("#{{$Id}}-button").html("Loading...");
          },
          complete: function(){
            $("#{{$Id}}-button").html("Submit");
          },
       });
    });
}
</script>
@endif

@if(Config::get('formstype.forms.'.$formType)=="newsletter")
@php 
$Id=Str::random(32);
@endphp
<div class="container">
   <div class="col-md-12">
      <form action="thanks.html"  id="{{$Id}}">
          <div class="input-group">
              <input type="email" name="email" class="form-control" style="resize:none;" placeholder="Enter Your Email ID" aria-label="Recipient's username" aria-describedby="button-addon2">
              <div class="{{$Id}}_news_email"></div>
              <input type="hidden" name="status" value=3 />
              <button class="btn btn-primary" style="border-top-left-radius: 0;
    border-bottom-left-radius: 0; min-width:150px;" type="submit" id="button-addon2">Submit</button>
          </div>
      </form>
   </div>
</div>
<script>
if(document.querySelector("#{{$Id}}")){
    document.querySelector("#{{$Id}}").addEventListener("submit",(e)=>{
        
        
        e.preventDefault();
        var data=getformdata1("#{{$Id}}");
        data['link']=window.location.href;
        data["company_id"]="{{$company}}";
        $.ajax({
        type: "post",
        url: "https://sagarwatertanks.com/public/api/contactus",
        data:data ,
        success: function(data){
            if(data.message){
                window.location.href="{{$redirection}}";
            }else{
                window.location.reload();
            }
        } ,
         error: function(data) {
            errorhighlight('{{$Id}}_news_email',data.responseJSON.message.email);
         },
         beforeSend: function(){
              document.querySelector("#{{$Id}} button[type=submit]").innerText="Loading...";
            //   $(".form").append($html);
          },
          complete: function(){
            //   $("#loader").remove();
              document.querySelector("#{{$Id}} button[type=submit]").innerText="Submit";
          },
       });
    });
}
</script>
@endif

<script>
function getformdata1(id){
let form = document.querySelector(`${id}`);
let data = new FormData(form);
var object={};
for (let [key, value] of data) {
    object[key]=value;
}
var FormValuedata=object;
return FormValuedata;
}
function errorhighlight(id,value){
    if(value){
        $(`#${id}`).addClass('has-warning');
      }else{
        $(`#${id}`).removeClass('has-warning');
      }
}
function errormessage(id,value){
    if(value && value!=null){
        document.querySelector(`#${id}`).innerText=value;
      }else{
        document.querySelector(`#${id}`).innerText="";
      }
}

</script>