<style>
    .daterangepicker .ranges li {font-size: 12px;padding: 8px 12px;cursor: pointer;background: #000000b0;}
  </style>
  <div class="card" id="data">
                <div class="card-header p-2" id="card_head">
                  <ul class="nav nav-pills">
                   <li style="align-self: center;">
                    <li class="nav-item"><a class="nav-link services active" data-id="1" href="#view" onclick="getDsrData(1, $(this).data('id'))" data-toggle="tab">DEV</a></li>&nbsp;
                    <li class="nav-item"><a class="nav-link services" data-id="2" href="#view" onclick="getDsrData(1, $(this).data('id'))" data-toggle="tab">SEO</a></li>&nbsp;
                   </li>
                    <li class="nav-item search-right">
                        <div>
                            @can('leaves_add')
                            <a  id="addleaveButton" class="btn btn-success" data-toggle="modal" data-target="#addLeaveModal">
                                <i class="fa fa-plus" aria-hidden="true"></i>Add Leave
                            </a>
                            @endcan
                        </div>
                     <div class="search_bar">
                        <div class="input-group" data-widget="sidebar-search">
                         <input class="form-control form-control-sidebar" id="search" type="search" placeholder="Search" aria-label="Search">
                        </div>
                     </div>
                    </li>
                  </ul>
                </div><!-- /.card-header -->
                <div class="card-body">
                  <div class="tab-content">
                    <div class="tab-pane" id="profile">
                       {{-- @include('admin.permissions.includes.addform') --}}
                    </div>

                    <div class="active tab-pane" id="view">
                      </div>
                    <!-- /.tab-pane -->
                  </div>
                  <!-- /.tab-content -->
                </div><!-- /.card-body -->
        </div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn..com/ckeditor5/30.0.0/inline/ckeditor.js"></script>

<script>
      $(document).ready(function(){
    $(".services").eq(0).addClass("active");
  });
getDsrData(1);

$("#search").on("keyup",(e)=>{
    getDsrData();
  });


$(document).on('click', '.pagination a', function(event){
  event.preventDefault();
  var page = $(this).attr('href').split('page=')[1];
  var sendurl=$(this).attr('href');
  getDsrData(page);
});
function getDsrData(page=1, active=1)
{
    $('#page-loader').show();
    var search=document.querySelector("#search").value;
    var make_url= "{{url('/')}}/admin/allDsrs/search?page="+page;
    // var active=document.getElementsByClassName('services active')[0].getAttribute('data-id');
    var data={search, active};
    $.ajax({
        url:make_url,
        data:data,
        success:function(data)
        {
        $('#view').empty().html(data);
        $('#page-loader').hide();
        },
        error:function(error){
        $('#page-loader').hide();

        }
    });
  }

</script>
