

    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!--  -->
                <div class="card-body">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width: 10px">Id</th>
                          <th>Actions</th>
                          <th>Name</th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach ($users as $eachUser)
                              <tr>
                                  <td>{{$eachUser->id}}</td>
                                  <td>
                                    <div class="btn-group text-center">
                                        <a target="_blank" href="{{ route('employee-full-report',$eachUser->id)}}" id="viewCompanyDetailsBtn" class="btn btn-outline-success btn-sm" data-id="{{ $eachUser->id }}" > <i class="fas fa-eye"></i> </a>
                                    </div>
                                  </td>
                                  <td>{{$eachUser->name}}</td>
                              </tr>
                          @endforeach

                      </tbody>
                    </table>
                </div>

                <!--  -->
            </div>
        </div>
        {{$users->links()}}
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->

    <script>



$(document).on('click', '.pagination a', function(event){
 event.preventDefault();
 var page = $(this).attr('href').split('page=')[1];
 var filter=document.querySelector("#tabs_filter").value;
 ajax('get',`{{Route("employee-pagination")}}`,data={page:page},'#pagination_employee');
});






</script>
