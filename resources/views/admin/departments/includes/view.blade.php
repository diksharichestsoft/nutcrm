
<!-- Main content -->

@can ('departments_create')
	    <a href="#" id="addDepartmentButton" class="btn btn-success ml-4 " data-toggle="modal" data-target="#addDepartmentModal">Add New Department</a>
@endcan

    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!--  -->
                <div class="card-body">
		    <table class="table table-bordered">
			@if ( count( $departments ) > 0 )
                      <thead>
                        <tr>
                          <th style="width: 10px">ID</th>
                          <th>Name</th>
			  @canany ( ['departments_edit', 'departments_delete'] )                          
				<th>Actions</th>
			  @endcanany

                        </tr>
		      </thead>
			@endif
		      <tbody>
				@forelse ( $departments as $department )
				<tr>
					<td> {{ $department->id }}</td>
					<td> {{ $department->name }}</td>
					@canany ( ['departments_edit', 'departments_delete'] )
					<td>
						<div class="row">
							<div class="col-sm-6">
								@can ('departments_edit')
								<button type="button" id="updateDepartmentBtn" class="btn btn-info btn-sm" data-id="{{ $department->id }}" onclick=" renderUpdateDepartmentModal( this )">Update</button>
								@endcan

								@can ('departments_delete')
								<button type="button" id="deleteDepartment" data-id="{{ $department->id }}" class="btn btn-danger btn-sm" onclick=" deleteDepartment( this )">Delete</button>
								@endcan
							</div>
						</div>
					</td>
					@endcanany
				</tr>
				@empty
					<center> <h3> No Data Available! </h3></center>
				@endforelse
                      </tbody>
                    </table>
                </div>
                <!--  -->
            </div>
        </div>
     	<div class="ml-4" id="departments_pagination" data-page="{{ $departments->currentPage() }}"> {{  $departments->links() }} </div>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
