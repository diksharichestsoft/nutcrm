<!-- Button trigger modal -->
  <!-- Modal -->
  <div class="modal fade" id="updateDepartmentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Update Department</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeUpdateDepartmentModalBtn">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div id="update_department_modal_body" class="modal-body">
	    <form id="update_department_form" action="#" method="POST">

		@csrf
			<input type="text" class="form-control" maxlength="255" required name="department_id" value="{{  $department->id }}" hidden>
		<div class="form-group">
			<input type="text" class="form-control" id="departmentName" placeholder="Name" maxlength="255" required name="name" value="{{ $department->name }}">
			<small id="errors_update_name" class="form-text font-weight-bold text-danger"></small>
		</div>
                <button type="submit" class="btn btn-success">Update</button>
            </form>
	</div>
      </div>
    </div>
  </div>
