@section('contents')
<style>
    .daterangepicker .ranges li {font-size: 12px;padding: 8px 12px;cursor: pointer;background: #000000b0;}
  </style>
<div class="card" id="data">
    <div class="card-header p-2" id="card_head">
      <ul class="nav nav-pills">
            <li class="nav-item search-right">
             <div class="search_bar">
                <div class="input-group" data-widget="sidebar-search">
                 <input class="form-control form-control-sidebar" id="search" type="search" placeholder="Search" aria-label="Search">
                </div>
             </div>
            </li>
      </ul>
    </div><!-- /.card-header -->

    <div class="card-body">

	<div id="createDepartmentModalBlock">
		@include('admin.departments.includes.create')
	</div>

	<div id="departments_view">
        </div>

	<div id="updateDepartmentModalBlock">

	</div>
      <!-- /.tab-content -->
    </div><!-- /.card-body -->
</div>
<script>

function refreshDepartmentsView( userRequestedPage = '' ){

	var searched_keyword = $("#search").val();
	console.log( `user requested page: ${userRequestedPage}` );
	if ( userRequestedPage != '' ){
		var page = userRequestedPage;
	} else {
		var page = $('#departments_pagination').data('page');
	}
	$.ajax({

		url: '{{ route("search-departments") }}' +`?page=${page}`,
		type:'get',
		data: { searched_keyword },
		xhr: function() {
			myXhr = $.ajaxSettings.xhr();
			return myXhr;
		},

		beforeSend:  function(){
			$("#page-loader").show();
		},


		success: function( departmentsHtml ){
			console.log( "page refreshed ");
			window.jsnn = departmentsHtml;
			$("#departments_view").empty().html( departmentsHtml );
		},

		error: function( errorResponse ){
			console.log("page not refreshed");
		},

		complete: function(){
			$("#page-loader").hide();
		},
});

}

function saveNewDepartment( departmentData ){

	console.log("save new department");
	$('#page-loader').show();

	$.ajax({

		url:'{{ route("store-department") }}',
		type:'post',
		dataType: "JSON",
		xhr: function() {
			myXhr = $.ajaxSettings.xhr();
			return myXhr;
		},

		cache: false,
		contentType: false,
		processData: false,
		data: departmentData,
		success:  function( successResponse ){
			$("#addDepartmentModal").modal('hide');;
			$('#page-loader').hide();
			refreshDepartmentsView();
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Added Successfully',
                showConfirmButton: false,
                timer: 1500
                })
		},

		error: function( errorResponse ){

			console.log("next is errors");
			console.log( errorResponse );
			for (const [key, value] of Object.entries( errorResponse.responseJSON.errors)) {
				console.log( `key: ${key}, value: ${value}` );
				$(`#errors_${key}`).html( value );			}
	  		$('#page-loader').hide();

		}
		});
  }

/* Save new department's helper */
  $(document).on('submit', '#add_department_form', function(e) {
	  e.preventDefault();

	  var departmentData = new FormData(this);
	  saveNewDepartment( departmentData );
  });


  function deleteDepartment( deleteDepartmentBtn )
  {
	  var departmentId= deleteDepartmentBtn.getAttribute('data-id');

	  swal({
	  	title: "Oops....",
		text: "Are You Sure You want to delete!",
		icon: "error",
		buttons: [ 'NO', 'YES' ],
	}).then( function(isConfirm) {
		if (isConfirm) {
			console.log("is confirmed");
			 $.ajax({
					url:"{{ route('delete-department') }}",
					type:"post",
					data:{ departmentId },
					headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
					success: function(data){
						refreshDepartmentsView();
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Remove Successfully',
                            showConfirmButton: false,
                            timer: 1500
                            })
					    }
			});
		} else {
			console.log("not confirmed");
		}
	});

  }



  function renderUpdateDepartmentModal( updateDepartmentBtn )
  {
	$('#page-loader').show();

	var departmentId = updateDepartmentBtn.getAttribute('data-id');
	console.log("this is department id: "+ departmentId );
	$.ajax({
		url: "{{ route('edit-department') }}",
		type: "get",
		data: { departmentId },
		success: function( successResponse ){
			$('#page-loader').hide();
			$('#updateDepartmentModalBlock').empty().html( successResponse );
			$("#updateDepartmentModal").modal('show');
		},
		error: function(){
			$('#page-loader').hide();
		}
	})
  }

function updateDepartment( departmentData ){

	console.log("update new department");
	$('#page-loader').show();

	$.ajax({

		url:'{{ route("update-department") }}',
		type:'post',
		dataType: "JSON",
		xhr: function() {
			myXhr = $.ajaxSettings.xhr();
			return myXhr;
		},

		cache: false,
		contentType: false,
		processData: false,
		data: departmentData,
		success:  function( successResponse ){
			$("#updateDepartmentModal").modal('hide');
		//	$('.modal-backdrop').hide();
			$('#page-loader').hide();
			refreshDepartmentsView();
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Updated Successfully',
                showConfirmButton: false,
                timer: 1500
                })
		},

		error: function( errorResponse ){

			console.log("next is errors");
			console.log( errorResponse );
			for (const [key, value] of Object.entries( errorResponse.responseJSON.errors)) {
				console.log( `key: ${key}, value: ${value}` );
				$(`#errors_update_${key}`).html( value );
				$('#page-loader').hide();
			}
		}
	});
  }

  $(document).on('submit', '#update_department_form', function(e) {
	  e.preventDefault();

	  var departmentData = new FormData(this);
	  updateDepartment( departmentData );
  });

document.querySelector("#search").addEventListener("keyup",(e)=>{
	refreshDepartmentsView( 1);
  });

  /* Pagination */

  $(document).on('click', '.pagination a', function(event){

	          event.preventDefault();
		  console.log('ok');
		  var page = $(this).attr('href').split('page=')[1];
		  var sendurl=$(this).attr('href');

		  console.log( `page number: ${page}` );
		  refreshDepartmentsView(page);
  });
  refreshDepartmentsView();
</script>
